﻿using Domain.DomainClasses.Feature;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Interfaces
{
    public interface IFeatureReplyTranslationRepository
    {
        Task<IEnumerable<FeatureReplyTranslation>> GetAllAsyn(Expression<Func<FeatureReplyTranslation, bool>> where = null, Func<IQueryable<FeatureReplyTranslation>, IOrderedQueryable<FeatureReplyTranslation>> orderBy = null
          , Func<IQueryable<FeatureReplyTranslation>, IIncludableQueryable<FeatureReplyTranslation, object>> include = null, bool tracking = false);

        Task<FeatureReplyTranslation> GetAsync(Expression<Func<FeatureReplyTranslation, bool>> where = null, Func<IQueryable<FeatureReplyTranslation>
            , IOrderedQueryable<FeatureReplyTranslation>> orderBy = null, Func<IQueryable<FeatureReplyTranslation>, IIncludableQueryable<FeatureReplyTranslation, object>> include = null, bool tracking = false);

        Task Insert(FeatureReplyTranslation featureReplyTranslation);
        Task<bool> ExistsAsync(Expression<Func<FeatureReplyTranslation, bool>> where = null, Func<IQueryable<FeatureReplyTranslation>,
            IIncludableQueryable<FeatureReplyTranslation, object>> include = null);

        Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<FeatureReplyTranslation, TResult>> selector = null,
                                          Expression<Func<FeatureReplyTranslation, bool>> where = null,
                                          Func<IQueryable<FeatureReplyTranslation>, IOrderedQueryable<FeatureReplyTranslation>> orderBy = null,
                                          Func<IQueryable<FeatureReplyTranslation>, IIncludableQueryable<FeatureReplyTranslation, object>> include = null,
                                          bool disableTracking = true);
        void Update(FeatureReplyTranslation featureReplyTranslation);

        void Delete(FeatureReplyTranslation featureReplyTranslation);
        void DeleteAll(IEnumerable<FeatureReplyTranslation> featureReplyTranslations);
        Task Save();
    }
}
