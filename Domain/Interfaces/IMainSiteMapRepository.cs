﻿
using Domain.DomainClasses.Seo;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Interfaces
{
    public interface IMainSiteMapRepository
    {
        Task<IEnumerable<MainSiteMap>> GetAllAsyn(Expression<Func<MainSiteMap, bool>> where = null, Func<IQueryable<MainSiteMap>, IOrderedQueryable<MainSiteMap>> orderBy = null
           , Func<IQueryable<MainSiteMap>, IIncludableQueryable<MainSiteMap, object>> include = null, bool tracking = false);

        Task<MainSiteMap> GetAsync(Expression<Func<MainSiteMap, bool>> where = null, Func<IQueryable<MainSiteMap>
            , IOrderedQueryable<MainSiteMap>> orderBy = null, Func<IQueryable<MainSiteMap>, IIncludableQueryable<MainSiteMap, object>> include = null, bool tracking = false);

        Task Insert(MainSiteMap MainSiteMap);
        Task<bool> ExistsAsync(Expression<Func<MainSiteMap, bool>> where = null, Func<IQueryable<MainSiteMap>,
            IIncludableQueryable<MainSiteMap, object>> include = null);

        Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<MainSiteMap, TResult>> selector = null,
                                          Expression<Func<MainSiteMap, bool>> where = null,
                                          Func<IQueryable<MainSiteMap>, IOrderedQueryable<MainSiteMap>> orderBy = null,
                                          Func<IQueryable<MainSiteMap>, IIncludableQueryable<MainSiteMap, object>> include = null,
                                          bool disableTracking = true);
        void Update(MainSiteMap MainSiteMap);

        void DeleteAsync(MainSiteMap MainSiteMap);
        Task Save();
    }
}
