﻿using Domain.DomainClasses.Gallery;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Interfaces
{
    public interface IGalleryGroupRepository
    {
        Task<IEnumerable<GalleryGroup>> GetAllAsyn(Expression<Func<GalleryGroup, bool>> where = null, Func<IQueryable<GalleryGroup>, IOrderedQueryable<GalleryGroup>> orderBy = null
          , Func<IQueryable<GalleryGroup>, IIncludableQueryable<GalleryGroup, object>> include = null, bool tracking = false);

        Task<GalleryGroup> GetAsync(Expression<Func<GalleryGroup, bool>> where = null, Func<IQueryable<GalleryGroup>
            , IOrderedQueryable<GalleryGroup>> orderBy = null, Func<IQueryable<GalleryGroup>, IIncludableQueryable<GalleryGroup, object>> include = null, bool tracking = false);

        Task<bool> ExistsAsync(Expression<Func<GalleryGroup, bool>> where = null, Func<IQueryable<GalleryGroup>,
            IIncludableQueryable<GalleryGroup, object>> include = null);

        Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<GalleryGroup, TResult>> selector = null,
                                          Expression<Func<GalleryGroup, bool>> where = null,
                                          Func<IQueryable<GalleryGroup>, IOrderedQueryable<GalleryGroup>> orderBy = null,
                                          Func<IQueryable<GalleryGroup>, IIncludableQueryable<GalleryGroup, object>> include = null,
                                          bool disableTracking = true);

        Task Insert(GalleryGroup galleryGroup);
        Task Update(GalleryGroup galleryGroup);

        void DeleteAsync(GalleryGroup galleryGroup);
        Task Save();
    }
}
