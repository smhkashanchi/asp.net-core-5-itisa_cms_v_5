﻿using Domain.DomainClasses.Connection;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Interfaces
{
    public interface IConnectionTranslationRepository
    {
        Task<ConnectionTranslation> GetAsync(Expression<Func<ConnectionTranslation, bool>> where = null, Func<IQueryable<ConnectionTranslation>
          , IOrderedQueryable<ConnectionTranslation>> orderBy = null, Func<IQueryable<ConnectionTranslation>, IIncludableQueryable<ConnectionTranslation, object>> include = null, bool tracking = false);

        Task<Dictionary<string, int>> GetAllToDictionary(int Id);
        Task<bool> ExistsAsync(Expression<Func<ConnectionTranslation, bool>> where = null, Func<IQueryable<ConnectionTranslation>,
         IIncludableQueryable<ConnectionTranslation, object>> include = null);
        Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<ConnectionTranslation, TResult>> selector = null,
                                          Expression<Func<ConnectionTranslation, bool>> where = null,
                                          Func<IQueryable<ConnectionTranslation>, IOrderedQueryable<ConnectionTranslation>> orderBy = null,
                                          Func<IQueryable<ConnectionTranslation>, IIncludableQueryable<ConnectionTranslation, object>> include = null,
                                          bool disableTracking = true);

        Task Insert(ConnectionTranslation connectionTranslation);
        void Update(ConnectionTranslation connectionTranslation);
        void Delete(ConnectionTranslation connectionTranslation);
        void DeleteAll(IEnumerable<ConnectionTranslation> connectionTranslation);
        Task Save();
    }
}
