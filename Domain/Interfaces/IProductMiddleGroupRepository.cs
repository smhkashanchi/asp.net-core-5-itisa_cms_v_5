﻿using Domain.DomainClasses.Product;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Interfaces
{
    public interface IProductMiddleGroupRepository
    {
        Task<IEnumerable<ProductMiddleGroup>> GetAllAsyn(Expression<Func<ProductMiddleGroup, bool>> where = null, Func<IQueryable<ProductMiddleGroup>, IOrderedQueryable<ProductMiddleGroup>> orderBy = null
         , Func<IQueryable<ProductMiddleGroup>, IIncludableQueryable<ProductMiddleGroup, object>> include = null, bool tracking = false);

        Task<ProductMiddleGroup> GetAsync(Expression<Func<ProductMiddleGroup, bool>> where = null, Func<IQueryable<ProductMiddleGroup>
            , IOrderedQueryable<ProductMiddleGroup>> orderBy = null, Func<IQueryable<ProductMiddleGroup>, IIncludableQueryable<ProductMiddleGroup, object>> include = null, bool tracking = false);

        Task Insert(ProductMiddleGroup productMiddleGroup);
        Task InsertRange(IEnumerable<ProductMiddleGroup> productMiddleGroups);
        Task<bool> ExistsAsync(Expression<Func<ProductMiddleGroup, bool>> where = null, Func<IQueryable<ProductMiddleGroup>,
            IIncludableQueryable<ProductMiddleGroup, object>> include = null);

        Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<ProductMiddleGroup, TResult>> selector = null,
                                          Expression<Func<ProductMiddleGroup, bool>> where = null,
                                          Func<IQueryable<ProductMiddleGroup>, IOrderedQueryable<ProductMiddleGroup>> orderBy = null,
                                          Func<IQueryable<ProductMiddleGroup>, IIncludableQueryable<ProductMiddleGroup, object>> include = null,
                                          bool disableTracking = true);
        void Update(ProductMiddleGroup productMiddleGroup);

        void Delete(ProductMiddleGroup productMiddleGroup);
        void DeleteAll(IEnumerable<ProductMiddleGroup> productMiddleGroups);
        Task Save();
    }
}
