﻿using Domain.DomainClasses.Gallery;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Interfaces
{
    public interface IGalleryPictureRepository
    {
        Task<IEnumerable<GalleryPicture>> GetAllAsyn(Expression<Func<GalleryPicture, bool>> where = null, Func<IQueryable<GalleryPicture>, IOrderedQueryable<GalleryPicture>> orderBy = null
          , Func<IQueryable<GalleryPicture>, IIncludableQueryable<GalleryPicture, object>> include = null, bool tracking = false);

        Task<GalleryPicture> GetAsync(Expression<Func<GalleryPicture, bool>> where = null, Func<IQueryable<GalleryPicture>
            , IOrderedQueryable<GalleryPicture>> orderBy = null, Func<IQueryable<GalleryPicture>, IIncludableQueryable<GalleryPicture, object>> include = null, bool tracking = false);

        Task<bool> ExistsAsync(Expression<Func<GalleryPicture, bool>> where = null, Func<IQueryable<GalleryPicture>,
            IIncludableQueryable<GalleryPicture, object>> include = null);

        Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<GalleryPicture, TResult>> selector = null,
                                          Expression<Func<GalleryPicture, bool>> where = null,
                                          Func<IQueryable<GalleryPicture>, IOrderedQueryable<GalleryPicture>> orderBy = null,
                                          Func<IQueryable<GalleryPicture>, IIncludableQueryable<GalleryPicture, object>> include = null,
                                          bool disableTracking = true);

        Task Insert(GalleryPicture galleryPicture);
        void Update(GalleryPicture galleryPicture);

        void DeleteAsync(GalleryPicture galleryPicture);
        Task Save();
    }
}
