﻿using Domain.DomainClasses.FAQ;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Interfaces
{
    public interface IFAQRepository
    {
        Task<IEnumerable<FAQ>> GetAllAsyn(Expression<Func<FAQ, bool>> where = null, Func<IQueryable<FAQ>, IOrderedQueryable<FAQ>> orderBy = null
               , Func<IQueryable<FAQ>, IIncludableQueryable<FAQ, object>> include = null, bool tracking = false);

        Task<FAQ> GetAsync(Expression<Func<FAQ, bool>> where = null, Func<IQueryable<FAQ>
            , IOrderedQueryable<FAQ>> orderBy = null, Func<IQueryable<FAQ>, IIncludableQueryable<FAQ, object>> include = null, bool tracking = false);

        Task Insert(FAQ fAQ);
        Task<bool> ExistsAsync(Expression<Func<FAQ, bool>> where = null, Func<IQueryable<FAQ>,
            IIncludableQueryable<FAQ, object>> include = null);

        Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<FAQ, TResult>> selector = null,
                                          Expression<Func<FAQ, bool>> where = null,
                                          Func<IQueryable<FAQ>, IOrderedQueryable<FAQ>> orderBy = null,
                                          Func<IQueryable<FAQ>, IIncludableQueryable<FAQ, object>> include = null,
                                          bool disableTracking = true);
        Task Update(FAQ fAQ);

        void Delete(FAQ fAQ);
        void DeleteAll(IEnumerable<FAQ> fAQs);
        Task Save();
    }
}
