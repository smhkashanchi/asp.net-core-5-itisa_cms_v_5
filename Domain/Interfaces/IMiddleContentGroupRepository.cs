﻿using Domain.DomainClasses.Content;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Interfaces
{
    public interface IMiddleContentGroupRepository
    {
         Task<IEnumerable<ContentMiddleGroup>> GetAllAsyn(Expression<Func<ContentMiddleGroup, bool>> where = null, Func<IQueryable<ContentMiddleGroup>, IOrderedQueryable<ContentMiddleGroup>> orderBy = null
           , Func<IQueryable<ContentMiddleGroup>, IIncludableQueryable<ContentMiddleGroup, object>> include = null, bool tracking = false);

        Task<ContentMiddleGroup> GetAsync(Expression<Func<ContentMiddleGroup, bool>> where = null, Func<IQueryable<ContentMiddleGroup>
            , IOrderedQueryable<ContentMiddleGroup>> orderBy = null, Func<IQueryable<ContentMiddleGroup>, IIncludableQueryable<ContentMiddleGroup, object>> include = null, bool tracking = false);

        Task Insert(ContentMiddleGroup ContentMiddleGroup);
        Task<bool> ExistsAsync(Expression<Func<ContentMiddleGroup, bool>> where = null, Func<IQueryable<ContentMiddleGroup>,
            IIncludableQueryable<ContentMiddleGroup, object>> include = null);

        Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<ContentMiddleGroup, TResult>> selector = null,
                                          Expression<Func<ContentMiddleGroup, bool>> where = null,
                                          Func<IQueryable<ContentMiddleGroup>, IOrderedQueryable<ContentMiddleGroup>> orderBy = null,
                                          Func<IQueryable<ContentMiddleGroup>, IIncludableQueryable<ContentMiddleGroup, object>> include = null,
                                          bool disableTracking = true);

        Task Update(ContentMiddleGroup contentMiddleGroup);

        void Delete(ContentMiddleGroup ContentMiddleGroup);
        void DeleteAll(IEnumerable<ContentMiddleGroup> ContentMiddleGroups);
        Task Save();
    }
}
