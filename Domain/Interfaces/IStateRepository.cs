﻿using Domain.DomainClasses.Region;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Interfaces
{
    public interface IStateRepository
    {
        Task<IEnumerable<State>> GetAllAsyn(Expression<Func<State, bool>> where = null, Func<IQueryable<State>, IOrderedQueryable<State>> orderBy = null
              , Func<IQueryable<State>, IIncludableQueryable<State, object>> include = null, bool tracking = false);

        Task<State> GetAsync(Expression<Func<State, bool>> where = null, Func<IQueryable<State>
            , IOrderedQueryable<State>> orderBy = null, Func<IQueryable<State>, IIncludableQueryable<State, object>> include = null, bool tracking = false);

        Task Insert(State state);
        Task<bool> ExistsAsync(Expression<Func<State, bool>> where = null, Func<IQueryable<State>,
            IIncludableQueryable<State, object>> include = null);

        Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<State, TResult>> selector = null,
                                          Expression<Func<State, bool>> where = null,
                                          Func<IQueryable<State>, IOrderedQueryable<State>> orderBy = null,
                                          Func<IQueryable<State>, IIncludableQueryable<State, object>> include = null,
                                          bool disableTracking = true);
        Task Update(State state);

        void Delete(State state);
        void DeleteAll(IEnumerable<State> states);
        Task Save();
    }
}
