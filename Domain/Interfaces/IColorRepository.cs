﻿using Domain.DomainClasses.Color;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Interfaces
{
    public interface IColorRepository
    {
        Task<Color> GetAsync(Expression<Func<Color, bool>> where = null, Func<IQueryable<Color>
             , IOrderedQueryable<Color>> orderBy = null, Func<IQueryable<Color>, IIncludableQueryable<Color, object>> include = null, bool tracking = false);
        Task InsertColor(Color color);
        void UpdateColor(Color color);
        void Delete(Color color);
        Task Save();
        
    }
}
