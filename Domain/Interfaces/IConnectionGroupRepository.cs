﻿using Domain.DomainClasses.Connection;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Interfaces
{
    public interface IConnectionGroupRepository
    {
        Task<IEnumerable<ConnectionGroup>> GetAllAsyn(Expression<Func<ConnectionGroup, bool>> where = null, Func<IQueryable<ConnectionGroup>, IOrderedQueryable<ConnectionGroup>> orderBy = null
           , Func<IQueryable<ConnectionGroup>, IIncludableQueryable<ConnectionGroup, object>> include = null, bool tracking = false);

        Task<ConnectionGroup> GetAsync(Expression<Func<ConnectionGroup, bool>> where = null, Func<IQueryable<ConnectionGroup>
            , IOrderedQueryable<ConnectionGroup>> orderBy = null, Func<IQueryable<ConnectionGroup>, IIncludableQueryable<ConnectionGroup, object>> include = null, bool tracking = false);
        
        Task<bool> ExistsAsync(Expression<Func<ConnectionGroup, bool>> where = null, Func<IQueryable<ConnectionGroup>,
            IIncludableQueryable<ConnectionGroup, object>> include = null);

        Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<ConnectionGroup, TResult>> selector = null,
                                          Expression<Func<ConnectionGroup, bool>> where = null,
                                          Func<IQueryable<ConnectionGroup>, IOrderedQueryable<ConnectionGroup>> orderBy = null,
                                          Func<IQueryable<ConnectionGroup>, IIncludableQueryable<ConnectionGroup, object>> include = null,
                                          bool disableTracking = true);

        Task Insert(ConnectionGroup ConnectionGroup);
        void Update(ConnectionGroup ConnectionGroup);

        void DeleteAsync(ConnectionGroup ConnectionGroup);
        Task Save();
    }
}
