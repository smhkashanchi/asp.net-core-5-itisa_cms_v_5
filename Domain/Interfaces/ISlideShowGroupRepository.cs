﻿using Domain.DomainClasses.SlideShow;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Interfaces
{
    public interface ISlideShowGroupRepository
    {
        Task<IEnumerable<SlideShowGroup>> GetAllAsyn(Expression<Func<SlideShowGroup, bool>> where = null, Func<IQueryable<SlideShowGroup>, IOrderedQueryable<SlideShowGroup>> orderBy = null
          , Func<IQueryable<SlideShowGroup>, IIncludableQueryable<SlideShowGroup, object>> include = null, bool tracking = false);

        Task<SlideShowGroup> GetAsync(Expression<Func<SlideShowGroup, bool>> where = null, Func<IQueryable<SlideShowGroup>
            , IOrderedQueryable<SlideShowGroup>> orderBy = null, Func<IQueryable<SlideShowGroup>, IIncludableQueryable<SlideShowGroup, object>> include = null, bool tracking = false);

        Task<bool> ExistsAsync(Expression<Func<SlideShowGroup, bool>> where = null, Func<IQueryable<SlideShowGroup>,
            IIncludableQueryable<SlideShowGroup, object>> include = null);

        Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<SlideShowGroup, TResult>> selector = null,
                                          Expression<Func<SlideShowGroup, bool>> where = null,
                                          Func<IQueryable<SlideShowGroup>, IOrderedQueryable<SlideShowGroup>> orderBy = null,
                                          Func<IQueryable<SlideShowGroup>, IIncludableQueryable<SlideShowGroup, object>> include = null,
                                          bool disableTracking = true);

        Task Insert(SlideShowGroup slideShowGroup);
        Task Update(SlideShowGroup slideShowGroup);

        void DeleteAsync(SlideShowGroup slideShowGroup);
        Task Save();
    }
}
