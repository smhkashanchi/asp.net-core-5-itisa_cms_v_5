﻿using Domain.DomainClasses.SlideShow;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Interfaces
{
    public interface ISlideShowRepository
    {
        Task<IEnumerable<SlideShow>> GetAllAsyn(Expression<Func<SlideShow, bool>> where = null, Func<IQueryable<SlideShow>, IOrderedQueryable<SlideShow>> orderBy = null
         , Func<IQueryable<SlideShow>, IIncludableQueryable<SlideShow, object>> include = null, bool tracking = false);

        Task<SlideShow> GetAsync(Expression<Func<SlideShow, bool>> where = null, Func<IQueryable<SlideShow>
            , IOrderedQueryable<SlideShow>> orderBy = null, Func<IQueryable<SlideShow>, IIncludableQueryable<SlideShow, object>> include = null, bool tracking = false);

        Task<bool> ExistsAsync(Expression<Func<SlideShow, bool>> where = null, Func<IQueryable<SlideShow>,
            IIncludableQueryable<SlideShow, object>> include = null);

        Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<SlideShow, TResult>> selector = null,
                                          Expression<Func<SlideShow, bool>> where = null,
                                          Func<IQueryable<SlideShow>, IOrderedQueryable<SlideShow>> orderBy = null,
                                          Func<IQueryable<SlideShow>, IIncludableQueryable<SlideShow, object>> include = null,
                                          bool disableTracking = true);

        Task Insert(SlideShow slideShow);
        Task Update(SlideShow slideShow);

        void DeleteAsync(SlideShow slideShow);
        Task Save();
    }
}
