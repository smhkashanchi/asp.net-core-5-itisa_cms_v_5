﻿using Domain.DomainClasses.FilesManage;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Interfaces
{
    public interface IFileManageRepository
    {
        Task<IEnumerable<FileManage>> GetAllAsyn(Expression<Func<FileManage, bool>> where = null, Func<IQueryable<FileManage>, IOrderedQueryable<FileManage>> orderBy = null
           , Func<IQueryable<FileManage>, IIncludableQueryable<FileManage, object>> include = null, bool tracking = false);

        Task<FileManage> GetAsync(Expression<Func<FileManage, bool>> where = null, Func<IQueryable<FileManage>
            , IOrderedQueryable<FileManage>> orderBy = null, Func<IQueryable<FileManage>, IIncludableQueryable<FileManage, object>> include = null, bool tracking = false);

        Task Insert(FileManage fileManage);
        Task<bool> ExistsAsync(Expression<Func<FileManage, bool>> where = null, Func<IQueryable<FileManage>,
            IIncludableQueryable<FileManage, object>> include = null);

        Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<FileManage, TResult>> selector = null,
                                          Expression<Func<FileManage, bool>> where = null,
                                          Func<IQueryable<FileManage>, IOrderedQueryable<FileManage>> orderBy = null,
                                          Func<IQueryable<FileManage>, IIncludableQueryable<FileManage, object>> include = null,
                                          bool disableTracking = true);
        void Update(FileManage fileManage);

        void Delete(FileManage fileManage);
        Task Save();
    }
}
