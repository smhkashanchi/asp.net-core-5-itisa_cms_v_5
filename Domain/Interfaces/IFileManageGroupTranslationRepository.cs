﻿using Domain.DomainClasses.FilesManage;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Interfaces
{
    public interface IFileManageGroupTranslationRepository
    {
        Task<IEnumerable<FileManageGroupTranslation>> GetAllAsyn(Expression<Func<FileManageGroupTranslation, bool>> where = null, Func<IQueryable<FileManageGroupTranslation>, IOrderedQueryable<FileManageGroupTranslation>> orderBy = null
           , Func<IQueryable<FileManageGroupTranslation>, IIncludableQueryable<FileManageGroupTranslation, object>> include = null, bool tracking = false);

        Task<FileManageGroupTranslation> GetAsync(Expression<Func<FileManageGroupTranslation, bool>> where = null, Func<IQueryable<FileManageGroupTranslation>
            , IOrderedQueryable<FileManageGroupTranslation>> orderBy = null, Func<IQueryable<FileManageGroupTranslation>, IIncludableQueryable<FileManageGroupTranslation, object>> include = null, bool tracking = false);

        Task Insert(FileManageGroupTranslation fileManageGroupTranslation);
        Task<bool> ExistsAsync(Expression<Func<FileManageGroupTranslation, bool>> where = null, Func<IQueryable<FileManageGroupTranslation>,
            IIncludableQueryable<FileManageGroupTranslation, object>> include = null);

        Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<FileManageGroupTranslation, TResult>> selector = null,
                                          Expression<Func<FileManageGroupTranslation, bool>> where = null,
                                          Func<IQueryable<FileManageGroupTranslation>, IOrderedQueryable<FileManageGroupTranslation>> orderBy = null,
                                          Func<IQueryable<FileManageGroupTranslation>, IIncludableQueryable<FileManageGroupTranslation, object>> include = null,
                                          bool disableTracking = true);
        void Update(FileManageGroupTranslation fileManageGroupTranslation);

        void Delete(FileManageGroupTranslation fileManageGroupTranslation);
        Task Save();
    }
}
