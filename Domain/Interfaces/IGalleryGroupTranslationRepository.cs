﻿using Domain.DomainClasses.Gallery;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Interfaces
{
    public interface IGalleryGroupTranslationRepository
    {
        Task<IEnumerable<GalleryGroupTranslation>> GetAllAsyn(Expression<Func<GalleryGroupTranslation, bool>> where = null, Func<IQueryable<GalleryGroupTranslation>, IOrderedQueryable<GalleryGroupTranslation>> orderBy = null
         , Func<IQueryable<GalleryGroupTranslation>, IIncludableQueryable<GalleryGroupTranslation, object>> include = null, bool tracking = false);

        Task<GalleryGroupTranslation> GetAsync(Expression<Func<GalleryGroupTranslation, bool>> where = null, Func<IQueryable<GalleryGroupTranslation>
            , IOrderedQueryable<GalleryGroupTranslation>> orderBy = null, Func<IQueryable<GalleryGroupTranslation>, IIncludableQueryable<GalleryGroupTranslation, object>> include = null, bool tracking = false);

        Task<bool> ExistsAsync(Expression<Func<GalleryGroupTranslation, bool>> where = null, Func<IQueryable<GalleryGroupTranslation>,
            IIncludableQueryable<GalleryGroupTranslation, object>> include = null);

        Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<GalleryGroupTranslation, TResult>> selector = null,
                                          Expression<Func<GalleryGroupTranslation, bool>> where = null,
                                          Func<IQueryable<GalleryGroupTranslation>, IOrderedQueryable<GalleryGroupTranslation>> orderBy = null,
                                          Func<IQueryable<GalleryGroupTranslation>, IIncludableQueryable<GalleryGroupTranslation, object>> include = null,
                                          bool disableTracking = true);

        Task Insert(GalleryGroupTranslation galleryGroupTranslation);
        Task Update(GalleryGroupTranslation galleryGroupTranslation);

        void Delete(GalleryGroupTranslation galleryGroupTranslation);
        Task Save();
    }
}
