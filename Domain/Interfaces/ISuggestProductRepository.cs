﻿using Domain.DomainClasses.Product;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Interfaces
{
    public interface ISuggestProductRepository
    {
        Task<IEnumerable<SuggestProduct>> GetAllAsyn(Expression<Func<SuggestProduct, bool>> where = null, Func<IQueryable<SuggestProduct>, IOrderedQueryable<SuggestProduct>> orderBy = null
              , Func<IQueryable<SuggestProduct>, IIncludableQueryable<SuggestProduct, object>> include = null, bool tracking = false);

        Task<SuggestProduct> GetAsync(Expression<Func<SuggestProduct, bool>> where = null, Func<IQueryable<SuggestProduct>
            , IOrderedQueryable<SuggestProduct>> orderBy = null, Func<IQueryable<SuggestProduct>, IIncludableQueryable<SuggestProduct, object>> include = null, bool tracking = false);

        Task Insert(SuggestProduct suggestProduct);
        Task<bool> ExistsAsync(Expression<Func<SuggestProduct, bool>> where = null, Func<IQueryable<SuggestProduct>,
            IIncludableQueryable<SuggestProduct, object>> include = null);

        Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<SuggestProduct, TResult>> selector = null,
                                          Expression<Func<SuggestProduct, bool>> where = null,
                                          Func<IQueryable<SuggestProduct>, IOrderedQueryable<SuggestProduct>> orderBy = null,
                                          Func<IQueryable<SuggestProduct>, IIncludableQueryable<SuggestProduct, object>> include = null,
                                          bool disableTracking = true);
        Task Update(SuggestProduct suggestProduct);

        void Delete(SuggestProduct suggestProduct);
        void DeleteAll(IEnumerable<SuggestProduct> suggestProducts);
        Task Save();
    }
}
