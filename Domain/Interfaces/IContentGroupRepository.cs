﻿using Domain.DomainClasses.Content;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Interfaces
{
    public interface IContentGroupRepository
    {
        Task<IEnumerable<ContentGroup>> GetAllAsyn(Expression<Func<ContentGroup, bool>> where = null, Func<IQueryable<ContentGroup>, IOrderedQueryable<ContentGroup>> orderBy = null
           , Func<IQueryable<ContentGroup>, IIncludableQueryable<ContentGroup, object>> include = null, bool tracking = false);

        Task<ContentGroup> GetAsync(Expression<Func<ContentGroup, bool>> where = null, Func<IQueryable<ContentGroup>
            , IOrderedQueryable<ContentGroup>> orderBy = null, Func<IQueryable<ContentGroup>, IIncludableQueryable<ContentGroup, object>> include = null, bool tracking = false);

        Task Insert(ContentGroup contentGroup);
        Task<bool> ExistsAsync(Expression<Func<ContentGroup, bool>> where = null, Func<IQueryable<ContentGroup>,
            IIncludableQueryable<ContentGroup, object>> include = null);

        Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<ContentGroup, TResult>> selector = null,
                                          Expression<Func<ContentGroup, bool>> where = null,
                                          Func<IQueryable<ContentGroup>, IOrderedQueryable<ContentGroup>> orderBy = null,
                                          Func<IQueryable<ContentGroup>, IIncludableQueryable<ContentGroup, object>> include = null,
                                          bool disableTracking = true);
        void Update(ContentGroup contentGroup);

        void DeleteAsync(ContentGroup contentGroup);
        Task Save();
    }
}
