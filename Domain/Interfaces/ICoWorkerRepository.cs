﻿using Domain.DomainClasses.CoWorker;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Interfaces
{
    public interface ICoWorkerRepository
    {
        Task<IEnumerable<CoWorker>> GetAllAsyn(Expression<Func<CoWorker, bool>> where = null, Func<IQueryable<CoWorker>, IOrderedQueryable<CoWorker>> orderBy = null
         , Func<IQueryable<CoWorker>, IIncludableQueryable<CoWorker, object>> include = null, bool tracking = false);

        Task<CoWorker> GetAsync(Expression<Func<CoWorker, bool>> where = null, Func<IQueryable<CoWorker>
            , IOrderedQueryable<CoWorker>> orderBy = null, Func<IQueryable<CoWorker>, IIncludableQueryable<CoWorker, object>> include = null, bool tracking = false);

        Task<bool> ExistsAsync(Expression<Func<CoWorker, bool>> where = null, Func<IQueryable<CoWorker>,
            IIncludableQueryable<CoWorker, object>> include = null);

        Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<CoWorker, TResult>> selector = null,
                                          Expression<Func<CoWorker, bool>> where = null,
                                          Func<IQueryable<CoWorker>, IOrderedQueryable<CoWorker>> orderBy = null,
                                          Func<IQueryable<CoWorker>, IIncludableQueryable<CoWorker, object>> include = null,
                                          bool disableTracking = true);

        Task Insert(CoWorker coWorker);
        Task Update(CoWorker coWorker);

        void DeleteAsync(CoWorker coWorker);
        Task Save();
    }
}
