﻿using Domain.DomainClasses.Product;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Interfaces
{
    public interface ISimilarProductRepository
    {
        Task<IEnumerable<SimilarProduct>> GetAllAsyn(Expression<Func<SimilarProduct, bool>> where = null, Func<IQueryable<SimilarProduct>, IOrderedQueryable<SimilarProduct>> orderBy = null
              , Func<IQueryable<SimilarProduct>, IIncludableQueryable<SimilarProduct, object>> include = null, bool tracking = false);

        Task<SimilarProduct> GetAsync(Expression<Func<SimilarProduct, bool>> where = null, Func<IQueryable<SimilarProduct>
            , IOrderedQueryable<SimilarProduct>> orderBy = null, Func<IQueryable<SimilarProduct>, IIncludableQueryable<SimilarProduct, object>> include = null, bool tracking = false);

        Task Insert(SimilarProduct similarProduct);
        Task<bool> ExistsAsync(Expression<Func<SimilarProduct, bool>> where = null, Func<IQueryable<SimilarProduct>,
            IIncludableQueryable<SimilarProduct, object>> include = null);

        Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<SimilarProduct, TResult>> selector = null,
                                          Expression<Func<SimilarProduct, bool>> where = null,
                                          Func<IQueryable<SimilarProduct>, IOrderedQueryable<SimilarProduct>> orderBy = null,
                                          Func<IQueryable<SimilarProduct>, IIncludableQueryable<SimilarProduct, object>> include = null,
                                          bool disableTracking = true);
        Task Update(SimilarProduct similarProduct);

        void Delete(SimilarProduct similarProduct);
        void DeleteAll(IEnumerable<SimilarProduct> similarProducts);
        Task Save();
    }
}
