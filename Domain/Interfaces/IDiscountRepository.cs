﻿using Domain.DomainClasses.Product;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Interfaces
{
    public interface IDiscountRepository
    {
        Task<IEnumerable<Discount>> GetAllAsyn(Expression<Func<Discount, bool>> where = null, Func<IQueryable<Discount>, IOrderedQueryable<Discount>> orderBy = null
               , Func<IQueryable<Discount>, IIncludableQueryable<Discount, object>> include = null, bool tracking = false);

        Task<Discount> GetAsync(Expression<Func<Discount, bool>> where = null, Func<IQueryable<Discount>
            , IOrderedQueryable<Discount>> orderBy = null, Func<IQueryable<Discount>, IIncludableQueryable<Discount, object>> include = null, bool tracking = false);

        Task Insert(Discount discount);
        Task<bool> ExistsAsync(Expression<Func<Discount, bool>> where = null, Func<IQueryable<Discount>,
            IIncludableQueryable<Discount, object>> include = null);

        Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<Discount, TResult>> selector = null,
                                          Expression<Func<Discount, bool>> where = null,
                                          Func<IQueryable<Discount>, IOrderedQueryable<Discount>> orderBy = null,
                                          Func<IQueryable<Discount>, IIncludableQueryable<Discount, object>> include = null,
                                          bool disableTracking = true);
        Task Update(Discount discount);

        void Delete(Discount discount);
        void DeleteAll(IEnumerable<Discount> discounts);
        Task Save();
    }
}
