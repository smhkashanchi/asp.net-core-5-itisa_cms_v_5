﻿using Domain.DomainClasses.Content;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Interfaces
{
    public interface IContentTranslationRepository
    {
        Task<IEnumerable<ContentTranslation>> GetAllAsyn(Expression<Func<ContentTranslation, bool>> where = null, Func<IQueryable<ContentTranslation>, IOrderedQueryable<ContentTranslation>> orderBy = null
        , Func<IQueryable<ContentTranslation>, IIncludableQueryable<ContentTranslation, object>> include = null, bool tracking = false);

        Task<ContentTranslation> GetAsync(Expression<Func<ContentTranslation, bool>> where = null, Func<IQueryable<ContentTranslation>
            , IOrderedQueryable<ContentTranslation>> orderBy = null, Func<IQueryable<ContentTranslation>, IIncludableQueryable<ContentTranslation, object>> include = null, bool tracking = false);

        Task Insert(ContentTranslation contentTranslation);
        Task<bool> ExistsAsync(Expression<Func<ContentTranslation, bool>> where = null, Func<IQueryable<ContentTranslation>,
            IIncludableQueryable<ContentTranslation, object>> include = null);

        Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<ContentTranslation, TResult>> selector = null,
                                          Expression<Func<ContentTranslation, bool>> where = null,
                                          Func<IQueryable<ContentTranslation>, IOrderedQueryable<ContentTranslation>> orderBy = null,
                                          Func<IQueryable<ContentTranslation>, IIncludableQueryable<ContentTranslation, object>> include = null,
                                          bool disableTracking = true);
        Task Update(ContentTranslation contentTranslation);
        Task Detach(ContentTranslation contentTranslation);
        void DeleteAsync(ContentTranslation contentTranslation);
        void DeleteAllAsync(IEnumerable<ContentTranslation> contentTranslations);
        Task Save();
    }
}
