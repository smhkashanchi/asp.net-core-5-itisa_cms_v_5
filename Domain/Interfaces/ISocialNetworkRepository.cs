﻿using Domain.DomainClasses.SocialNetwork;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Interfaces
{
    public interface ISocialNetworkRepository
    {
        Task<IEnumerable<SocialNetwork>> GetAllAsyn(Expression<Func<SocialNetwork, bool>> where = null, Func<IQueryable<SocialNetwork>, IOrderedQueryable<SocialNetwork>> orderBy = null
           , Func<IQueryable<SocialNetwork>, IIncludableQueryable<SocialNetwork, object>> include = null, bool tracking = false);

        Task<SocialNetwork> GetAsync(Expression<Func<SocialNetwork, bool>> where = null, Func<IQueryable<SocialNetwork>
            , IOrderedQueryable<SocialNetwork>> orderBy = null, Func<IQueryable<SocialNetwork>, IIncludableQueryable<SocialNetwork, object>> include = null, bool tracking = false);

        Task Insert(SocialNetwork socialNetwork);
        Task<bool> ExistsAsync(Expression<Func<SocialNetwork, bool>> where = null, Func<IQueryable<SocialNetwork>,
            IIncludableQueryable<SocialNetwork, object>> include = null);

        Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<SocialNetwork, TResult>> selector = null,
                                          Expression<Func<SocialNetwork, bool>> where = null,
                                          Func<IQueryable<SocialNetwork>, IOrderedQueryable<SocialNetwork>> orderBy = null,
                                          Func<IQueryable<SocialNetwork>, IIncludableQueryable<SocialNetwork, object>> include = null,
                                          bool disableTracking = true);
        Task Update(SocialNetwork socialNetwork);

        void DeleteAsync(SocialNetwork socialNetwork);
        Task Save();
    }
}
