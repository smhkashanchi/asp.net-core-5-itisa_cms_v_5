﻿using Domain.DomainClasses.Product;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Interfaces
{
    public interface IProductGroupFeatureRepository
    {
        Task<IEnumerable<ProductGroupFeature>> GetAllAsyn(Expression<Func<ProductGroupFeature, bool>> where = null, Func<IQueryable<ProductGroupFeature>, IOrderedQueryable<ProductGroupFeature>> orderBy = null
         , Func<IQueryable<ProductGroupFeature>, IIncludableQueryable<ProductGroupFeature, object>> include = null, bool tracking = false);
        Task InsertRange(List<ProductGroupFeature> productGroupFeatures);
        Task<bool> ExistsAsync(Expression<Func<ProductGroupFeature, bool>> where = null, Func<IQueryable<ProductGroupFeature>,
            IIncludableQueryable<ProductGroupFeature, object>> include = null);

        Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<ProductGroupFeature, TResult>> selector = null,
                                          Expression<Func<ProductGroupFeature, bool>> where = null,
                                          Func<IQueryable<ProductGroupFeature>, IOrderedQueryable<ProductGroupFeature>> orderBy = null,
                                          Func<IQueryable<ProductGroupFeature>, IIncludableQueryable<ProductGroupFeature, object>> include = null,
                                          bool disableTracking = true);

        Task<ProductGroupFeature> GetAsync(Expression<Func<ProductGroupFeature, bool>> where = null, Func<IQueryable<ProductGroupFeature>
           , IOrderedQueryable<ProductGroupFeature>> orderBy = null, Func<IQueryable<ProductGroupFeature>, IIncludableQueryable<ProductGroupFeature, object>> include = null, bool tracking = false);

        void DeleteRange(List<ProductGroupFeature> productGroupFeatures);
        Task Save();
    }
}
