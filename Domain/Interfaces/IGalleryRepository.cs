﻿using Domain.DomainClasses.Gallery;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Interfaces
{
    public interface IGalleryRepository
    {
        Task<Gallery> GetAsync(Expression<Func<Gallery, bool>> where = null, Func<IQueryable<Gallery>
         , IOrderedQueryable<Gallery>> orderBy = null, Func<IQueryable<Gallery>, IIncludableQueryable<Gallery, object>> include = null, bool tracking = false);

        Task<bool> ExistsAsync(Expression<Func<Gallery, bool>> where = null, Func<IQueryable<Gallery>,
          IIncludableQueryable<Gallery, object>> include = null);

        Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<Gallery, TResult>> selector = null,
                                        Expression<Func<Gallery, bool>> where = null,
                                        Func<IQueryable<Gallery>, IOrderedQueryable<Gallery>> orderBy = null,
                                        Func<IQueryable<Gallery>, IIncludableQueryable<Gallery, object>> include = null,
                                        bool disableTracking = true);

        Task InsertGallery(Gallery gallery);
        Task UpdateGallery(Gallery gallery);
        void Delete(Gallery gallery);
        Task Save();
    }
}
