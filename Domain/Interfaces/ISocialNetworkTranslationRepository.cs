﻿using Domain.DomainClasses.SocialNetwork;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Interfaces
{
    public interface ISocialNetworkTranslationRepository
    {
        Task<IEnumerable<SocialNetworkTranslation>> GetAllAsyn(Expression<Func<SocialNetworkTranslation, bool>> where = null, Func<IQueryable<SocialNetworkTranslation>, IOrderedQueryable<SocialNetworkTranslation>> orderBy = null
          , Func<IQueryable<SocialNetworkTranslation>, IIncludableQueryable<SocialNetworkTranslation, object>> include = null, bool tracking = false);

        Task<SocialNetworkTranslation> GetAsync(Expression<Func<SocialNetworkTranslation, bool>> where = null, Func<IQueryable<SocialNetworkTranslation>
            , IOrderedQueryable<SocialNetworkTranslation>> orderBy = null, Func<IQueryable<SocialNetworkTranslation>, IIncludableQueryable<SocialNetworkTranslation, object>> include = null, bool tracking = false);

        Task Insert(SocialNetworkTranslation SocialNetworkTranslation);
        Task<bool> ExistsAsync(Expression<Func<SocialNetworkTranslation, bool>> where = null, Func<IQueryable<SocialNetworkTranslation>,
            IIncludableQueryable<SocialNetworkTranslation, object>> include = null);

        Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<SocialNetworkTranslation, TResult>> selector = null,
                                          Expression<Func<SocialNetworkTranslation, bool>> where = null,
                                          Func<IQueryable<SocialNetworkTranslation>, IOrderedQueryable<SocialNetworkTranslation>> orderBy = null,
                                          Func<IQueryable<SocialNetworkTranslation>, IIncludableQueryable<SocialNetworkTranslation, object>> include = null,
                                          bool disableTracking = true);
        Task Update(SocialNetworkTranslation SocialNetworkTranslation);

        void DeleteAsync(SocialNetworkTranslation SocialNetworkTranslation);
        void DeleteAllAsync(IEnumerable<SocialNetworkTranslation> SocialNetworkTranslations);
        Task Save();
    }
}
