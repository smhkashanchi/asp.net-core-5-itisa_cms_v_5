﻿using Domain.DomainClasses;

using FluentValidation;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Validators
{
    public class LanguageValidator: AbstractValidator<Language>
    {
        public LanguageValidator()
        {
            RuleFor(x => x.Id)
                .NotNull().WithMessage("لطفا شناسه زبان را وارد نمایید")
                //.MaximumLength(2).WithMessage("شناسه زبان بیش از 2 کاراکتر مجاز نمی باشد")
                .Matches(@"^[a-zA-Z]+$").WithMessage("شناسه نا معتبر است");

            RuleFor(x => x.Name)
                .NotNull().WithMessage("لطفا نام زبان را وارد نمایید")
                .MaximumLength(30).WithMessage("شناسه زبان بیش از 30 کاراکتر مجاز نمی باشد");



        }
    }
}
