﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.DomainClasses.SocialNetwork
{
    public enum SocialNetworkType { Telegram, Instagram, Twitter, Linekdin,Facebook }
    public static class EnumSocialNetworkType
    {
        public static IEnumerable<T> GetValues<T>()
        {
            return Enum.GetValues(typeof(T)).Cast<T>();
        }
    }
    public class SocialNetwork 
    {
        [Key]
        public Int16 SocialNetwork_ID { get; set; }
        [DisplayName("لینک")]
        [Required(ErrorMessage ="لطفا {0} را وارد نمایید")]
        [DataType(DataType.Url,ErrorMessage ="لطفا {0} را بصورت صحیح وارد نمایید")]
        [MaxLength(200)]
        public string Link { get; set; }
        [DisplayName("نوع شبکه اجتماعی")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(50)]
        public string Type { get; set; }
    }
}
