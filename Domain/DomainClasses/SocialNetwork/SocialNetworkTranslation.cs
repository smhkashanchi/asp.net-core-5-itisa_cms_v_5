﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.DomainClasses.SocialNetwork
{
    public class SocialNetworkTranslation
    {
        [Key]
        public Int16 SocialNetworkTranslation_ID { get; set; }
        [ForeignKey("SocialNetwork")]
        public Int16 SocialNetwork_Id { get; set; }
        [DisplayName("عنوان")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(50)]
        public string Title { get; set; }
        [DisplayName("وضعیت")]
        public bool IsActive { get; set; }
        [ForeignKey("Language")]
        public string Language_Id { get; set; }

        public virtual SocialNetwork SocialNetwork { get; set; }
        public virtual Language Language{ get; set; }
    }
}
