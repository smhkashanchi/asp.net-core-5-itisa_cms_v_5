﻿using Domain.DomainClasses.Seo;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.DomainClasses.Product
{
    public class ProductGroup
    {
        public ProductGroup()
        {
        }
        [Key]
        public Int16 ProductGroup_ID { get; set; }
        [ForeignKey("ProductGroupParent")]
        public Int16? ParentId { get; set; }
        [DisplayName("تصویر")]
        [MaxLength(900)]
        public string Image { get; set; }
        [DisplayName("عرض عکس")]
        [MaxLength(5)]
        public string ImageWidth { get; set; }
        [DisplayName("ارتفاع عکس")]
        [MaxLength(5)]
        public string ImageHeight { get; set; }
        [DisplayName("عرض عکس کوچک")]
        [MaxLength(5)]
        public string SmallImageWidth { get; set; }
        [DisplayName("ارتفاع عکس کوچک")]
        [MaxLength(5)]
        public string SmallImageHeight { get; set; }
        [ForeignKey("MainSiteMap")]
        public int? SiteMap_Id { get; set; }


        public virtual MainSiteMap MainSiteMap { get; set; }


        public virtual ProductGroup ProductGroupParent { get; set; }
        public List<ProductGroupTranslation> ProductGroupTranslations { get; set; }
        public List<ProductGroupFeature> ProductGroupFeatures { get; set; }

        public List<ProductMiddleGroup> ProductMiddleGroups { get; set; }
    }
}
