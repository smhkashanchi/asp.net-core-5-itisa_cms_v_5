﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.DomainClasses.Product
{
    public class Product
    {
        public Product()
        {
        }
        [Key]
        public int Product_ID { get; set; }
        [DisplayName("کد محصول")]
        [MaxLength(50)]
        public string Code { get; set; }
        [DisplayName("موجود؟")]
        public bool IsExist { get; set; }
        //[DisplayName("نمایش در دکور")]
        //public bool IsFirstPageDecor { get; set; }
        [DisplayName("پر بازدید ترین")]
        public bool MostVisited { get; set; }
        [DisplayName("پر فروش ترین")]
        public bool BestSelling { get; set; }
        [DisplayName("تاریخ ثبت")]
        public DateTime CreateDate { get; set; }
        [DisplayName("گالری")]
        [ForeignKey("Gallery")]
        public Int16? Gallery_Id { get; set; }
        [DisplayName("پیش نمایش")]
        public bool IsPreview { get; set; }

        public virtual Gallery.Gallery Gallery { get; set; }
        public virtual List<ProductTranslation> ProductTranslations { get; set; }
        public List<ProductMiddleGroup> ProductMiddleGroups { get; set; }
        public virtual List<ProductFeature> ProductFeatures { get; set; }
        [InverseProperty("Product")]
        public virtual IEnumerable<SimilarProduct> SimilarProduct { get; set; }
        [InverseProperty("Similar_Product")]
        public virtual IEnumerable<SimilarProduct> SimilarProduct2 { get; set; }

        [InverseProperty("Product")]
        public virtual IEnumerable<SuggestProduct> SuggestProducts { get; set; }
        [InverseProperty("Suggest_Product")]
        public virtual IEnumerable<SuggestProduct> SuggestProduct { get; set; }

    }
}
