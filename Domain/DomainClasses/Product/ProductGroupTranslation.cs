﻿using Domain.DomainClasses.Feature;
using Domain.DomainClasses.Seo;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.DomainClasses.Product
{
    public class ProductGroupTranslation
    {
        public ProductGroupTranslation()
        {
        }
        [Key]
        public Int16 ProductGroupTranslation_ID { get; set; }
        [ForeignKey("ProductGroup")]
        public Int16 ProductGroup_Id { get; set; }
       
        [DisplayName("تخفیف")]
        [ForeignKey("Discount")]
        public Int16? Discount_Id { get; set; }
        [DisplayName("عنوان")]
        [Required(ErrorMessage ="لطفا {0} را وارد نمایید")]
        [MaxLength(200)]
        public string Title { get; set; }
        [DisplayName("اسلاگ")]
        [MaxLength(200)]
        public string Slug { get; set; }
        [DisplayName("توضیحات")]
        public string Description { get; set; }
        [DisplayName("وضعیت")]
        public bool IsActive { get; set; }
        [DisplayName("زبان")]
        [ForeignKey("Language")]
        public string Language_Id { get; set; }


        public virtual Language Language { get; set; }
        public virtual ProductGroup ProductGroup { get; set; }
        public virtual Discount Discount { get; set; }
       

    }
}
