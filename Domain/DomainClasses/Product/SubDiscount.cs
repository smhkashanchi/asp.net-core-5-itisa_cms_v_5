﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.DomainClasses.Product
{
    public class SubDiscount
    {
        public SubDiscount()
        {
        }
        [Key]
        public int SubDiscount_ID { get; set; }
        [ForeignKey("Discount")]
        public Int16 Discount_Id { get; set; }
        [DisplayName("عنوان")]
        [Required]
        [MaxLength(200)]
        public string Title { get; set; }
        public bool IsUse { get; set; } = false;

        public virtual Discount Discount { get; set; }
    }
}
