﻿using Domain.DomainClasses.Feature;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.DomainClasses.Product
{
    public class ProductFeature
    {
        public ProductFeature()
        {
        }
        [Key]
        [MaxLength(450)]
        public string ProductFeature_ID { get; set; }
        [ForeignKey("Feature")]
        [DisplayName("ویژگی")]
        public int Feature_Id { get; set; }
        [ForeignKey("Product")]
        [DisplayName("محصول")]
        public int Product_Id { get; set; }
        [ForeignKey("FeatureReply")]
        [DisplayName("جواب ویژگی")]
        public int FeatureReply_Id { get; set; }
        public bool IsVariable { get; set; }

        public virtual Product Product { get; set; }
        public virtual FeatureReply FeatureReply { get; set; }
        public virtual Feature.Feature Feature { get; set; }
    }
}
