﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.DomainClasses.Product
{
    public class PriceList
    {
        public PriceList()
        {
        }
        [Key]
        public int PriceList_ID { get; set; }
        [DisplayName("واحد پول")]
        [ForeignKey("Currency")]
        public byte Currency_Id { get; set; }
        [DisplayName("عنوان")]
        [MaxLength(50)]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        public string Title { get; set; }

        public virtual Currency Currency { get; set; }

        public virtual List<ProductPriceList> ProductPriceLists { get; set; }
        public virtual List<ProductPriceListHistory> ProductPriceListHistories { get; set; }

    }
}
