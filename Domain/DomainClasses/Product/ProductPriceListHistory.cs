﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.DomainClasses.Product
{
    public class ProductPriceListHistory
    {
        public ProductPriceListHistory()
        {
        }
        [Key]
        public int ProductPriceListHistory_ID { get; set; }
        [DisplayName("لیست قیمت محصول")]
        [ForeignKey("ProductPriceList")]
        public int ProductPriceList_ID { get; set; }
        [DisplayName("لیست قیمت")]
        [ForeignKey("PriceList")]
        public int PriceList_Id { get; set; }
        [DisplayName("محصول")]
        [ForeignKey("ProductTranslation")]
        public int ProductTranslation_Id { get; set; }
        [DisplayName("اختلاف")]
        [ForeignKey("Variation")]
        public int Variation_Id { get; set; }

        public virtual ProductPriceList ProductPriceList { get; set; }
        public virtual PriceList PriceList { get; set; }
        public virtual ProductTranslation ProductTranslation { get; set; }
        public virtual Variation.Variation Variation { get; set; }
    }
}
