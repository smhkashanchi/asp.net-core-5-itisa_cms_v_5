﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.DomainClasses.Product
{
    public class SuggestProduct
    {
        public SuggestProduct()
        {

        }
        [Key]
        [MaxLength(450)]
        public string ID { get; set; }

        [ForeignKey("Product_Id")]
        public Product Product { get; set; }
        public int? Product_Id { get; set; }


        [ForeignKey("ProductSuggest_Id")]
        public Product Suggest_Product { get; set; }
        public int? ProductSuggest_Id { get; set; }

    }
}
