﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.DomainClasses.Product
{
    public class ProductTranslation
    {
        public ProductTranslation()
        {
        }
        [Key] 
        public int ProductTranslation_ID { get; set; }
        [ForeignKey("Product")]
        public int Product_Id { get; set; }
        [DisplayName("تخفیف")]
        [ForeignKey("Discount")]
        public Int16? Discount_Id { get; set; }
        [DisplayName("عنوان")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(200)]
        public string Title { get; set; }
        [DisplayName("اسلاگ")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(200)]
        public string Slug { get; set; }
        [DisplayName("توضیحات")]
        public string Description { get; set; }
        [MaxLength(200)]
        public string MetaTitle { get; set; }
        [MaxLength(500)]
        public string MetaKeword { get; set; }
        [MaxLength(5000)]
        public string MetaDescription { get; set; }
        [MaxLength(5000)]
        public string MetaOther { get; set; }
        [DisplayName("کلمات کلیدی")]
        [MaxLength(1000)]
        public string Tag { get; set; }
        [DisplayName("زبان")]
        [ForeignKey("Language")]
        public string Language_Id { get; set; }
        public bool IsActive { get; set; }

        public virtual Language Language { get; set; }
        public virtual Product Product { get; set; }
        public virtual Discount Discount { get; set; }

        
        public virtual List<Variation.Variation> Variations { get; set; }
        public virtual List<ProductPriceList> ProductPriceLists { get; set; }
        public virtual List<ProductPriceListHistory> ProductPriceListHistories { get; set; }

       
    }
}
