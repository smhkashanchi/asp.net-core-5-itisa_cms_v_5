﻿using Microsoft.EntityFrameworkCore.Infrastructure;

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.DomainClasses.Product
{
    public class SimilarProduct
    {
        public SimilarProduct()
        {

        }
        [Key]
        [MaxLength(450)]
        public string ID { get; set; }

        [ForeignKey("Product_Id")]
        public Product Product { get; set; }
        public int? Product_Id { get; set; }


        [ForeignKey("ProductSimilar_Id")]
        public Product Similar_Product { get; set; }
        public int? ProductSimilar_Id { get; set; }



    }
}
