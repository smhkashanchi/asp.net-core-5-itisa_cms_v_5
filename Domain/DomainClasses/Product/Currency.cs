﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.DomainClasses.Product
{
    public class Currency
    {
        public Currency()
        {
        }
        [Key,DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public byte Currency_ID { get; set; }
        [DisplayName("عنوان")]
        [MaxLength(50)]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        public string Title { get; set; }

        public List<PriceList> PriceLists { get; set; }
    }
}
