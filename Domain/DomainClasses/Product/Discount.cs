﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.DomainClasses.Product
{
    public class Discount
    {
        public Discount()
        {
        }
        [Key]
        public Int16 Discount_ID { get; set; }
        [DisplayName("عنوان")]
        [MaxLength(200)]
        [Required(ErrorMessage ="لطفا {0} را وارد نمایید")]
        public string Title { get; set; }
        [DisplayName("درصد")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        public byte Percent { get; set; }
        [DisplayName("وضعیت")]
        public bool IsActive { get; set; }
        [DisplayName("تاریخ شروع")]
        [MaxLength(20)]
        public string StartDate { get; set; }
        [DisplayName("تاریخ پایان")]
        [MaxLength(20)]
        public string EndDate { get; set; }
        [DisplayName("نامحدود زمانی ؟")]
        public bool UnLimitTime { get; set; }
        [DisplayName("نامحدود استفاده ؟")]
        public bool UnLimitUse { get; set; }
        [DisplayName("کد تصادفی ؟")]
        public bool IsRandom { get; set; }
        [DisplayName("تعداد تخفیف")]
        public Int16? CountOff { get; set; } = 0;
        [DisplayName("تعداد استفاده")]
        public Int16 CountUse { get; set; } = 0;
        [ForeignKey("Language")]
        public string Language_Id { get; set; }

        public virtual Language Language { get; set; }
        public virtual List<SubDiscount> SubDiscounts { get; set; }
        public List<ProductGroupTranslation> ProductGroupTranslations { get; set; }
        public virtual List<ProductTranslation> ProductTranslations { get; set; }

        [NotMapped]
        public string RandomType { get; set; }
        [NotMapped]
        public byte? ExtraKeyCharCount { get; set; }
    }
}
