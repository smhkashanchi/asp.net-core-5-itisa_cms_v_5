﻿using Domain.DomainClasses.Seo;

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.DomainClasses.Product
{
    public class ProductMiddleGroup
    {
        [Key]
        [MaxLength(450)]
        public string ID { get; set; }
        [ForeignKey("ProductGroup")]
        public Int16 ProductGroup_Id { get; set; }
        [ForeignKey("Product")]
        public int Product_Id { get; set; }
        [ForeignKey("MainSiteMap")]
        public int SiteMap_Id { get; set; }

        public virtual ProductGroup ProductGroup { get; set; }
        public virtual MainSiteMap MainSiteMap { get; set; }
        public virtual Product Product { get; set; }
        
    }
}
