﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.DomainClasses.Product
{
    public class ProductGroupFeature
    {
        [Key]
        [MaxLength(450)]
        public string ProductGroupFeature_ID { get; set; }
        [ForeignKey("Feature")]
        [DisplayName("ویژگی")]
        public int Feature_Id { get; set; }
        [ForeignKey("ProductGroup")]
        [DisplayName("گروه محصول")]
        public Int16 ProductGroup_Id { get; set; }
        public virtual ProductGroup ProductGroup { get; set; }
        public virtual Feature.Feature Feature { get; set; }
    }
}
