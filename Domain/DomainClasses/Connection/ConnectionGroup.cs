﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.DomainClasses.Connection
{
    public class ConnectionGroup
    {
        [Key,DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public byte Id { get; set; }
        [MaxLength(200)]
        [Required(ErrorMessage ="لطفا عنوان گروه را وارد نمایید")]
        public string Title { get; set; }
        [MaxLength(300)]
        [Required]
        public string Slug { get; set; }
        public bool IsActive { get; set; }

        public virtual List<Connection> Connections { get; set; }
    }
}
