﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.DomainClasses.Connection
{
    public enum ConnectionType{Mobile,Email,Phone,Map}
    public static class EnumConnectionType
    {
        public static IEnumerable<T> GetValues<T>()
        {
            return Enum.GetValues(typeof(T)).Cast<T>();
        }
    }
    public class Connection
    {
        [Key,DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public byte Id { get; set; }
        [ForeignKey("ConnectionGroup")]
        [Required(ErrorMessage = "لطفا گروه ارتباط را وارد نمایید")]
        public byte GroupId { get; set; }
        [Required(ErrorMessage ="لطفا نوع ارتباط را وارد نمایید")]
        [MaxLength(100)]
        public string Type { get; set; }
        [Required(ErrorMessage = "لطفا تصویر راه ارتباطی را وارد نمایید")]
        [MaxLength(100)]
        public string Image { get; set; }
        public virtual ConnectionGroup ConnectionGroup { get; set; }
        public virtual List<ConnectionTranslation> ConnectionTranslations { get; set; }
    }
}
