﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.DomainClasses.Connection
{
    public class ConnectionTranslation
    {
        [Key]
        public Int16 Id { get; set; }
        [ForeignKey("Connection")]
        public byte ConnectionId { get; set; }
        [Required(ErrorMessage ="لطفا نام راه ارتباطی را وارد نمایید")]
        [MaxLength(100)]
        public string Name { get; set; }
        [Required(ErrorMessage = "لطفا مقدار راه ارتباطی را وارد نمایید")]
        [MaxLength(100)]
        public string Value { get; set; }
        [ForeignKey("Language")]
        public string LanguageId { get; set; }

        public virtual Language Language { get; set; }
        public virtual Connection Connection{ get; set; }

    }
}
