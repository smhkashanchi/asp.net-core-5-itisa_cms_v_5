﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.DomainClasses.CoWorker
{
    public class CoWorkerGroupTranslation
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public byte CoWorkerGroupTranslation_ID { get; set; }
        [ForeignKey("CoWorkerGroup")]
        public byte CoWorkerGroup_Id { get; set; }
        [DisplayName("عنوان")]
        [Required(ErrorMessage = "لطفا {0}  را وارد نمایید")]
        [MaxLength(50)]
        public string Title { get; set; }
        [DisplayName("زبان")]
        [ForeignKey("Language")]
        public string Language_Id { get; set; }

        public Language Language { get; set; }
        public virtual CoWorkerGroup CoWorkerGroup { get; set; } 
        public List<CoWorkerTranslation> CoWorkerTranslations { get; set; }
    }
}
