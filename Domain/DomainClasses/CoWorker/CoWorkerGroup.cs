﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.DomainClasses.CoWorker
{
    public class CoWorkerGroup
    {
        [Key,DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public byte CoWorkerGroup_ID { get; set; }
       
        [DisplayName("عرض تصویر (px)")]
        [Required(ErrorMessage = "لطفا {0}  را وارد نمایید")]
        [MaxLength(5)]
        public string ImageWidth { get; set; } = "420";
        [DisplayName("ارتفاع تصویر(px)")]
        [Required(ErrorMessage = "لطفا {0}  را وارد نمایید")]
        [MaxLength(5)]
        public string ImageHeight { get; set; } = "400";
        public bool IsActive { get; set; }

        public List<CoWorkerGroupTranslation> CoWorkerGroupTranslations { get; set; }

    }
}
