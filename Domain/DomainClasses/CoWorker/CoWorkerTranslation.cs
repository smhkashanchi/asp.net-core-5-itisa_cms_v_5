﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.DomainClasses.CoWorker
{
    public class CoWorkerTranslation
    {
        [Key]
        public Int16 CoWorkerTranslation_ID { get; set; }
        [ForeignKey("CoWorker")]
        public Int16 CoWorker_Id { get; set; }
        [DisplayName("گروه")]
        [ForeignKey("CoWorkerGroupTranslation")]
        public byte TranslationGroup_Id { get; set; }
        [DisplayName("عنوان")]
        [MaxLength(200)]
        public string Title { get; set; }
        [DisplayName("لینک")]
        [MaxLength(500)]
        public string Link { get; set; }
        [DisplayName("وضعیت")]
        public bool IsActive { get; set; }
        [DisplayName("زبان")]
        [ForeignKey("Language")]
        public string Language_Id { get; set; }

        public Language Language { get; set; }
        public virtual CoWorker CoWorker { get; set; }
        public virtual CoWorkerGroupTranslation CoWorkerGroupTranslation{ get; set; }
    }
}
