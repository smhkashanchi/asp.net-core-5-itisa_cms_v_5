﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.DomainClasses.CoWorker
{
    public class CoWorker
    {
        [Key]
        public Int16 CoWorker_ID { get; set; }
        [DisplayName("تصویر")]
        [MaxLength(500)]
        public string Image { get; set; }
        public List<CoWorkerTranslation> CoWorkerTranslations { get; set; }
    }
}
