﻿using Domain.DomainClasses.FilesManage;
using Domain.DomainClasses.Product;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.DomainClasses.Seo
{
    public class MainSiteMap
    {
        public MainSiteMap()
        {

        }
        [Key]
        public int SiteMap_ID { get; set; }

        [DisplayName("نوع نقشه سایت")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        public Int16 Type { get; set; }

        [DisplayName("آدرس")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        public string Location { get; set; }


        [DisplayName("عنوان نقشه سایت")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        public string Title { get; set; }

        [DisplayName("تاریخ")]
        public DateTime CreateDate { get; set; }

        public virtual List<Content.ContentGroupTranslation> ContentGroupTranslations { get; set; }
        public virtual List<Content.ContentMiddleGroup> ContentMiddleGroups{ get; set; }
        public virtual List<FileManageGroupTranslation> FileManageGroupTranslations { get; set; }
        public virtual List<FileManageTranslation> FileManageTranslations { get; set; }
        public List<ProductGroupTranslation> ProductGroupTranslations { get; set; }

    }
}
