﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.DomainClasses.Seo
{
    public class SeoPage
    {
        [Key]
        public Int16 SeoPage_ID { get; set; }
        [DataType(DataType.Url)]
        [MaxLength(5000)]
        [Required(ErrorMessage ="لطفا {0} را وارد نمایید")]
        public string PageUrl { get; set; }
        [MaxLength(200)]
        public string MetaTitle { get; set; }
        [MaxLength(500)]
        public string MetaKeyword { get; set; }
        [MaxLength(5000)]
        public string MetaDescription { get; set; }
        [MaxLength(5000)]
        public string MetaOther { get; set; }
    }
}
