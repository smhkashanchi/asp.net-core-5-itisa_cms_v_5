﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.DomainClasses.Region
{
    public class Country
    {
        public Country()
        {

        }
        [Key]
        public int Country_ID { get; set; }
        [DisplayName("نام کشور")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(50)]
        public string Name { get; set; }
        [DisplayName("کد کشور")]
        [Column(TypeName = "varchar(10)")]
        public string Code { get; set; }
        [DisplayName("وضعیت")]
        public bool IsActive { get; set; }
       
        public List<State> States { get; set; }
    }
}
