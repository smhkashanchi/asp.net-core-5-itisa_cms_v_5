﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Domain.DomainClasses.Region
{
    public class State
    {
        public State()
        {

        }

        [Key]
        public int State_ID { get; set; }
        [DisplayName("نام استان")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(50)]
        public string Name { get; set; }
        [DisplayName("کد استان")]
        [Column(TypeName = "varchar(10)")]
        public string Code { get; set; }
        [DisplayName("وضعیت")]
        public bool IsActive { get; set; }

        [DisplayName("کشور")]
        [ForeignKey("Country")]
        public int CountryId { get; set; }

        public virtual Country Country { get; set; }
        //[JsonIgnore]
        public virtual List<City> Cities { get; set; }
    }
}
