﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.DomainClasses.Region
{
    public class City
    {
        public City()
        {
        }

        [Key]
        public int City_ID { get; set; }
        [DisplayName("نام شهر")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(50)]
        public string Name { get; set; }
        [DisplayName("کد شهر")]
        [Column(TypeName = "varchar(10)")]
        public string Code { get; set; }
        [DisplayName("وضعیت")]
        public bool IsActive { get; set; }
        [ForeignKey("State")]
        [DisplayName("استان")]
        public int State_Id { get; set; }
        public virtual State State { get; set; }
        
    }
}
