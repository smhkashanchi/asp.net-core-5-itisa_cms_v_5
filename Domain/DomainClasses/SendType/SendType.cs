﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.DomainClasses.SendType
{

    public class SendType
    {
        [Key,DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public byte SendType_ID { get; set; }
        [DisplayName("نوع روش ارسال")]
        [Required(ErrorMessage ="لطفا {0} را وارد نمایید")]
        [MaxLength(200)]
        public string Type { get; set; }

        public List<SendTypeTranslation>  SendTypeTranslations { get; set; }
    }
}
