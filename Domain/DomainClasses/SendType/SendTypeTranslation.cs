﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.DomainClasses.SendType
{
    public class SendTypeTranslation
    {
        [Key]
        public Int16 SendTypeTranslation_ID { get; set; }
        [ForeignKey("SendType")]
        public byte SendType_Id { get; set; }
        [DisplayName("عنوان")]
        [Required(ErrorMessage ="لطفا {0} را وارد نمایید")]
        [MaxLength(100)]
        public string Title { get; set; }
        [DisplayName("توضیحات")]
        [MaxLength(9000)]
        public string Description { get; set; }
        [DisplayName("قیمت")]
        public int? Price { get; set; }
        [ForeignKey("Language")]
        public string Language_Id { get; set; }

        public virtual Language Language { get; set; }
        public virtual SendType SendType { get; set; }
    }
}
