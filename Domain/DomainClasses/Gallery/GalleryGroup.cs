﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.DomainClasses.Gallery
{
    public class GalleryGroup 
    {
        [Key,DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public byte GalleryGroup_ID { get; set; }
       
        [MaxLength(5)]
        [DisplayName("عرض عکس (px)")]
        public string LargPicWidth { get; set; } = "0";
        [MaxLength(5)]
        [DisplayName("  عرض عکس متوسط (px)")]
        public string MediumPicWidth { get; set; } = "0";
        [MaxLength(5)]
        [DisplayName("عرض عکس کوچک (px)")]
        public string SmallPicWidth { get; set; } = "0";
        [MaxLength(5)]
        [DisplayName("ارتفاع عکس (px)")]
        public string LargPicHeight { get; set; } = "0";
        [MaxLength(5)]
        [DisplayName("ارتفاع عکس متوسط (px)")]
        public string MediumPicHeight { get; set; } = "0";
        [MaxLength(5)]
        [DisplayName("ارتفاع عکس کوچک (px)")]
        public string SmallPicHeight { get; set; } = "0";

        public virtual List<GalleryGroupTranslation> GalleryGroupTranslations{ get; set; }
    }
}
