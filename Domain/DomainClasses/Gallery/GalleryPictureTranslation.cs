﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.DomainClasses.Gallery
{
    public class GalleryPictureTranslation
    {
        [Key]
        [MaxLength(450)]
        public string GalleryPicTranslation_ID { get; set; }
        [MaxLength(450)]
        [ForeignKey("GalleryPicture")]
        public string GalleryPic_Id { get; set; }
        [MaxLength(200)]
        public string Title { get; set; }

        public virtual GalleryPicture GalleryPicture { get; set; }
    }
}
