﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.DomainClasses.Gallery
{
    public class Gallery
    {
        public Gallery()
        {

        }
        [Key]
        public Int16 Gallery_ID { get; set; }
       
        [DisplayName("تصویر")]
        [MaxLength(500)]
        public string Image { get; set; }

       
        public virtual List<GalleryTranslation> GalleryTranslations{ get; set; }
        public virtual List<GalleryPicture> GalleryPictures{ get; set; }
        public virtual List<Product.Product> Products { get; set; }
        public virtual List<Content.Content> Contents{ get; set; }
    }
}
