﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.DomainClasses.Gallery
{
    public class GalleryTranslation
    {
        [Key]
        public int GalleryTranslation_ID { get; set; }
        [ForeignKey("Gallery")]
        public Int16 Gallery_Id { get; set; }
        [ForeignKey("GalleryGroupTranslation")]
        [DisplayName("گروه گالری")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        public Int16 GroupTranslation_Id { get; set; }
        [DisplayName("عنوان")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(100)]
        public string Title { get; set; }
        [DisplayName("اسلاگ")]
        [MaxLength(100)]
        public string Slug { get; set; }
        [DisplayName("وضعیت")]
        public bool IsActive { get; set; }
        [ForeignKey("Language")]
        public string Language_Id { get; set; }

        public virtual Language Language { get; set; }
        public virtual Gallery Gallery { get; set; }
        public virtual GalleryGroupTranslation GalleryGroupTranslation { get; set; }

    }
}
