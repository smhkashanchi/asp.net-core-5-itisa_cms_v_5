﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.DomainClasses.Gallery
{
    public class GalleryPicture
    {
        [Key]
        [MaxLength(450)]
        public string GalleryPic_ID { get; set; }
        [ForeignKey("Gallery")]
        public Int16 Gallery_Id { get; set; }
        [MaxLength(500)]
        [Required]
        public string Image { get; set; }

        public virtual Gallery Gallery { get; set; }
        public virtual List<GalleryPictureTranslation> GalleryPictureTranslations { get; set; }
    }
}
