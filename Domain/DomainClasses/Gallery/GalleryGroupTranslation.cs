﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.DomainClasses.Gallery
{
    public class GalleryGroupTranslation
    {
        [Key]
        public Int16 GalleryGroupTranslation_ID { get; set; }
        [ForeignKey("GalleryGroup")]
        public byte GalleryGroup_Id { get; set; }
        [DisplayName("عنوان")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(100)]
        public string Title { get; set; }
        [ForeignKey("Language")]
        public string Language_Id { get; set; }

        public virtual Language Language { get; set; }
        public virtual GalleryGroup GalleryGroup { get; set; }
        public virtual List<GalleryTranslation> GalleryTranslations { get; set; }
    }
}
