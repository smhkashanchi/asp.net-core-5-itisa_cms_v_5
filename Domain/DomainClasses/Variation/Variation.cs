﻿using Domain.DomainClasses.Product;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.DomainClasses.Variation
{
    public class Variation
    {
        public Variation()
        {
        }
        [Key]
        public int Variation_ID { get; set; }
        [ForeignKey("ProductTranslation")]
        [DisplayName("محصول")]
        public int ProductTranslation_Id { get; set; }
        [DisplayName("عنوان")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(100)]
        public string Title { get; set; }

        public virtual ProductTranslation ProductTranslation { get; set; }
        public virtual List<FeatureReplyVariation> FeatureReplyVariations { get; set; }
        public virtual List<ProductPriceList> ProductPriceLists { get; set; }
        public virtual List<ProductPriceListHistory> ProductPriceListHistories { get; set; }
    }
}
