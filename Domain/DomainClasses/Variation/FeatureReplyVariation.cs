﻿using Domain.DomainClasses.Feature;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.DomainClasses.Variation
{
    public class FeatureReplyVariation
    {
        public FeatureReplyVariation()
        {
        }
        [Key]
        public int FeatureReplyVariation_ID { get; set; }
        [ForeignKey("FeatureReply")]
        [DisplayName("جواب ویژگی")]
        public int FeatureReply_Id { get; set; }
        [ForeignKey("Variation")]
        [DisplayName("اختلاف")]
        public int Variation_Id { get; set; }
        [ForeignKey("Feature")]
        [DisplayName("ویژگی")]
        public int Feature_Id { get; set; }

        public virtual FeatureReply FeatureReply { get; set; }
        public virtual Feature.Feature Feature { get; set; }
        public virtual Variation Variation { get; set; }
    }
}
