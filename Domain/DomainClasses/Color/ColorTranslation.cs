﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.DomainClasses.Color
{
    public class ColorTranslation
    {
        [Key]
        public int Id { get; set; }
        [ForeignKey("Color")]
        [Required]
        public Int16 ColorId { get; set; }
        [Required(ErrorMessage ="لطفا عنوان رنگ را وارد نمایید")]
        [MaxLength(100)]
        public string Title { get; set; }
        [ForeignKey("Language")]
        [Required]
        public string LanguageId { get; set; }

        public virtual Color Color { get; set; }
        public virtual Language Language { get; set; }
    }
}
