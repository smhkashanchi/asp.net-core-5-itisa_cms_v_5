﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.DomainClasses.Color
{
    public class Color
    {
        [Key]
        public Int16 ColorId { get; set; }
        [Required(ErrorMessage ="لطفا کد رنگ را وارد نمایید")]
        [MaxLength(20)]
        public string ColorCode { get; set; }

        public virtual List<ColorTranslation> ColorTranslations { get; set; }
    }
}
