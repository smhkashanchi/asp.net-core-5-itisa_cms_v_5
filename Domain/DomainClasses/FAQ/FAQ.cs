﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.DomainClasses.FAQ
{
    public class FAQ
    {
        public FAQ()
        {
        }
        [Key]
        public Int16 FAQ_ID { get; set; }
        [DisplayName("سوال")]
        [Required(ErrorMessage ="لطفا {0} را وارد نمایید")]
        [MaxLength(500)]
        public string Question { get; set; }
        [DisplayName("پاسخ")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(9000)]
        public string Answer { get; set; }
        [DisplayName("وضعیت")]
        public bool IsActive { get; set; }
        [DisplayName("زبان")]
        [ForeignKey("Language")]
        public string Language_Id { get; set; }

        public Language Language { get; set; }
    }
}
