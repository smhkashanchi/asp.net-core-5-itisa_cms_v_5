﻿using Domain.DomainClasses.Connection;
using Domain.DomainClasses.Content;
using Domain.DomainClasses.CoWorker;
using Domain.DomainClasses.Feature;
using Domain.DomainClasses.FilesManage;
using Domain.DomainClasses.Gallery;
using Domain.DomainClasses.Product;
using Domain.DomainClasses.Region;
using Domain.DomainClasses.SendType;
using Domain.DomainClasses.SlideShow;
using Domain.DomainClasses.SocialNetwork;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.DomainClasses
{
    public class Language
    {
        [Key]
        [DisplayName("شناسه")]
        [Required(ErrorMessage = "لطفا شناسه زبان را وارد نمایید")]
        [MaxLength(10,ErrorMessage = "شناسه زبان بیش از 10 کاراکتر مجاز نمی باشد")]
        //[RegularExpression(@"^[a-zA-Z]+$",ErrorMessage = "شناسه نا معتبر است")]
        public string Id { get; set; }

        [DisplayName("نام")]
        [Required(ErrorMessage = "لطفا نام زبان را وارد نمایید")]
        [MaxLength(30, ErrorMessage = "شناسه زبان بیش از 30 کاراکتر مجاز نمی باشد")]
        public string Name { get; set; }

        [DisplayName("وضعیت")]
        public bool IsActive { get; set; }


        public virtual List<Color.ColorTranslation> ColorTranslations { get; set; }
        public virtual List<ConnectionTranslation> ConnectionTranslations { get; set; }
        public virtual List<ContentGroupTranslation> ContentGroupTranslations { get; set; }
        public virtual List<ContentTranslation> ContentTranslations { get; set; }
        public virtual List<GalleryTranslation> GalleryTranslations { get; set; }
        public List<GalleryGroupTranslation> GalleryGroupTranslations { get; set; }
        public virtual List<SocialNetworkTranslation> SocialNetworkTranslations { get; set; }
        public List<SlideShowTranslation> SlideShowTranslations { get; set; }
        public List<SlideShowGroupTranslation> SlideShowGroupTranslations { get; set; }
        public List<FileManageGroupTranslation> FileManageGroupTranslations{ get; set; }
        public List<FileManageTranslation> FileManageTranslations { get; set; }
        public List<SendTypeTranslation> SendTypeTranslations { get; set; }
        public List<CoWorkerTranslation> CoWorkerTranslations { get; set; }
        public List<CoWorkerGroupTranslation> CoWorkerGroupTranslations { get; set; }
        public List<Discount> Discounts { get; set; }
        public List<ProductGroupTranslation> ProductGroupTranslations { get; set; }
        public List<ProductTranslation> ProductTranslations { get; set; }
        public List<FeatureGroupTranslation> FeatureGroupTranslations{ get; set; }
        public List<FeatureTranslation> FeatureTranslations{ get; set; }
        public List<FeatureReplyTranslation> FeatureReplyTranslations { get; set; }
        public List<FAQ.FAQ> FAQs { get; set; }
       

    }
}
