﻿using Domain.DomainClasses.Product;
using Domain.DomainClasses.Variation;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.DomainClasses.Feature
{
    public class FeatureReply
    {
        [Key]
        public int FeatureReply_ID { get; set; }
        [ForeignKey("Feature")]
        [DisplayName("ویژگی")]
        public int Feature_Id { get; set; }
        [DisplayName("مقدار پیش فرض")]
        public bool IsDefault { get; set; }
        [DisplayName("کد رنگ")]
        [MaxLength(20)]
        public string ColorCode { get; set; }
        public virtual Feature Feature { get; set; }
        public List<FeatureReplyTranslation> FeatureReplyTranslations { get; set; }
        public virtual List<ProductFeature> ProductFeatures { get; set; }
        public virtual List<FeatureReplyVariation> FeatureReplyVariations { get; set; }
    }
}
