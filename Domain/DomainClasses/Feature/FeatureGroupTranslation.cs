﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.DomainClasses.Feature
{
    public class FeatureGroupTranslation
    {
        [Key]
        public Int16 FeatureGroupTranslation_ID { get; set; }
        [ForeignKey("FeatureGroup")]
        public Int16 FeatureGroup_Id { get; set; }
        [DisplayName("عنوان")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(100)]
        public string Title { get; set; }
        [ForeignKey("Language")]
        [Required]
        [DisplayName("زبان")]
        public string Language_Id { get; set; }

        public virtual Language Language { get; set; }
        public virtual FeatureGroup FeatureGroup { get; set; }
    }
}
