﻿using Domain.DomainClasses.Product;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.DomainClasses.Feature
{
    public class FeatureGroup
    {
        [Key]
        public Int16 FeatureGroup_ID { get; set; }
        public virtual List<FeatureGroupTranslation> FeatureGroupTranslations { get; set; }
    }
}
