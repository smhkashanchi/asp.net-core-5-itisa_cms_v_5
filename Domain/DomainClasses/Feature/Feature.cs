﻿using Domain.DomainClasses.Product;
using Domain.DomainClasses.Variation;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.DomainClasses.Feature
{
    public class Feature
    {
        [Key]
        public int Feature_ID { get; set; }
        [ForeignKey("FeatureGroup")]
        [DisplayName("گروه ویژگی")]
        public Int16 FeatureGroup_Id { get; set; }
       
        [DisplayName("استفاده در فیلتر")]
        public bool UseFilter { get; set; }
        [DisplayName("")]
        public bool IsVariable { get; set; }

        public virtual FeatureGroup FeatureGroup { get; set; }
        public virtual List<FeatureTranslation> FeatureTranslations { get; set; }
        public virtual List<FeatureReply> FeatureReplies { get; set; }
        public virtual List<ProductGroupFeature> ProductGroupFeatures { get; set; }
        //public virtual List<FeatureReplyVariation> FeatureReplyVariations { get; set; }

    }
}
