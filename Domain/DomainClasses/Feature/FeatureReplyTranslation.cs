﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.DomainClasses.Feature
{
    public class FeatureReplyTranslation
    {
        [Key]
        public int FeatureReplyTranslation_ID { get; set; }
        [ForeignKey("FeatureReply")]
        public int FeatureReply_Id { get; set; }
        [DisplayName("عنوان")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(200)]
        public string Title { get; set; }
        [ForeignKey("Language")]
        [Required]
        [DisplayName("زبان")]
        public string Language_Id { get; set; }

        public virtual Language Language { get; set; }
        public virtual FeatureReply FeatureReply { get; set; }
    }
}
