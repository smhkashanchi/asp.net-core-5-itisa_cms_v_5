﻿using Domain.DomainClasses.Seo;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.DomainClasses.Content
{
    public class ContentTranslation 
    {
        [Key]
        public int ContentTranslation_ID { get; set; }
        [ForeignKey("Content")]
        public int Content_Id { get; set; }
        [DisplayName("عنوان")]
        [Required(ErrorMessage ="لطفا {0} را وارد نمایید")]
        [MaxLength(200)]
        public string Title { get; set; }
        [DisplayName("اسلاگ")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(200)]
        public string Slug { get; set; }
        [DisplayName("خلاصه محتوا")]
        [MaxLength(200)]
        public string Summery { get; set; }
        [DisplayName("متن محتوا")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        public string ContentText { get; set; }
        [MaxLength(200)]
        public string MetaTitle { get; set; }
        [MaxLength(500)]
        public string MetaKeword { get; set;}
        [MaxLength(5000)]
        public string MetaDescription { get; set; }
        [MaxLength(5000)]
        public string MetaOther { get; set; }
        [DisplayName("کلمات کلیدی")]
        [MaxLength(1000)]
        public string Tag { get; set; }
        [DisplayName("تعداد بازدید")]
        public Int16 VisitCount { get; set; } = 0;
        [DisplayName("تعداد لایک")]
        public Int16 LikeCount { get; set; } = 0;
        [DisplayName("نویسنده")]
        [MaxLength(100)]
        public string Author { get; set; }
        [DisplayName("امتیازات")]
        public Int16 Votes { get; set; } = 0;
        [DisplayName("جمع امتیازات")]
        public Int16 SumVotes { get; set; } = 0;
        [DisplayName("میانگین امتیازات")]
        public Int16 AvgVotes { get; set; } = 0;
        [DisplayName("زبان")]
        [ForeignKey("Language")]
        public string Language_Id { get; set; }
       

        [NotMapped]
        public List<short> selectedGroupIds { get; set; }
        public  virtual Content Content { get; set; }
        public virtual Language Language { get; set; }
       
        public virtual List<ContentMiddleGroup> ContentMiddleGroups { get; set; }
    }
}
