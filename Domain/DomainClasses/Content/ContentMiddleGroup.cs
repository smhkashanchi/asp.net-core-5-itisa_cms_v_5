﻿using Domain.DomainClasses.Seo;

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.DomainClasses.Content
{
    public class ContentMiddleGroup
    {
        [Key]
        [MaxLength(450)]
        public string ID { get; set; }
        [ForeignKey("ContentGroupTranslation")]
        public Int16 ContentGroupTranslation_Id { get; set; }
        [ForeignKey("ContentTranslation")]
        public int ContentTranslation_Id { get; set; }
        [ForeignKey("MainSiteMap")]
        public int SiteMap_Id { get; set; }

        public virtual ContentGroupTranslation ContentGroupTranslation { get; set; }
        public virtual MainSiteMap MainSiteMap { get; set; }
        public virtual ContentTranslation ContentTranslation{ get; set; }

    }
}
