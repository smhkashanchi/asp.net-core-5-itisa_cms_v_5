﻿using Domain.DomainClasses.Seo;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.DomainClasses.Content
{
    public class ContentGroup
    {
        [Key,DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public byte ContentGroup_ID { get; set; }
        [ForeignKey("ParentContentGroup")]
        public byte? ParentId { get; set; }
        [DisplayName("وضعیت")]
        public bool IsActive { get; set; }
       
        [DisplayName("عرض عکس")]
        [MaxLength(5)]
        public string ContentImageWidth { get; set; } = "450";
        [DisplayName("ارتفاع عکس")]
        [MaxLength(5)]
        public string ContentImageHeight { get; set; } = "360";

        public virtual ContentGroup ParentContentGroup { get; set; }
        
        public virtual List<ContentGroupTranslation> ContentGroupTranslations { get; set; }

    }
}
