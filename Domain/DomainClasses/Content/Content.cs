﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.DomainClasses.Content
{
    public class Content
    {
        [Key]
        public int Content_ID { get; set; }
        [DisplayName("تصویر")]
        public string Image { get; set; }
        [DisplayName("تصویر")]
        public string SmallImage { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ReleaseDate { get; set; }
        [DisplayName("وضعیت")]
        public bool IsActive { get; set; } = true;
        [DisplayName("گالری")]
        [ForeignKey("Gallery")]
        public Int16? Gallery_Id { get; set; }

        public virtual List<ContentTranslation> ContentTranslations{ get; set; }
        public Gallery.Gallery Gallery{ get; set; }
    }
}
