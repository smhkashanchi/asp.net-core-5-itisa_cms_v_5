﻿using Domain.DomainClasses.Seo;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.DomainClasses.Content
{
    public class ContentGroupTranslation
    {
        [Key]
        public Int16 ContentGroupTranslation_ID { get; set; }
        [ForeignKey("ContentGroup")]
        [DisplayName("نام گروه")]
        public byte ContentGroup_Id { get; set; }
        [DisplayName("عنوان")]
        [Required(ErrorMessage ="لطفا {0} را وارد نمایید")]
        [MaxLength(200)]
        public string Title { get; set; }
        [DisplayName("اسلاگ")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(300)]
        public string Slug { get; set; }
        [DisplayName("توضیحات")]
        [MaxLength(2000)]
        public string Description { get; set; }
        [ForeignKey("Language")]
        public string Language_Id { get; set; }
        [ForeignKey("MainSiteMap")]
        public int? SiteMap_Id { get; set; }

        [NotMapped]
        public byte ParentId { get; set; }

        public virtual MainSiteMap MainSiteMap { get; set; }
        public virtual Language Language { get; set; }
        public virtual ContentGroup ContentGroup{ get; set; }
        public virtual List<ContentMiddleGroup> ContentMiddleGroups { get; set; }
    }
}
