﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.DomainClasses.SlideShow
{
    public class SlideShowGroupTranslation
    {
        [Key,DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public byte SlideShowGroupTranslation_ID { get; set; }
        [ForeignKey("SlideShowGroup")]
        public byte SlideShowGroup_Id { get; set; }
        [DisplayName("عنوان")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(100)]
        public string Title { get; set; }

        [DisplayName("زبان")]
        [ForeignKey("Language")]
        public string Language_Id { get; set; }

        public Language Language { get; set; }
        public virtual SlideShowGroup SlideShowGroup { get; set; }
        public virtual List<SlideShowTranslation> SlideShowTranslations { get; set; }
    }
}
