﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.DomainClasses.SlideShow
{
    public class SlideShowGroup
    {
        [Key,DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public byte SlideShowGroup_ID { get; set; }
        [DisplayName("وضعیت")]
        public bool IsActive { get; set; }
        [DisplayName("عرض عکس (px)")]
        [MaxLength(5)]
        public string ImageWidth { get; set; } = "0";
        [DisplayName("ارتفاع عکس (px)")]
        [MaxLength(5)]
        public string ImageHeight { get; set; } = "0";
        [DisplayName("عرض عکس کوچک (px)")]
        [MaxLength(5)]
        public string SmallPicWidth { get; set; } = "0";
        [DisplayName("ارتفاع عکس کوچک (px)")]
        [MaxLength(5)]
        public string SmallPicHeight { get; set; } = "0";
        [DisplayName("ارتفاع عکس متوسط (px)")]
        [MaxLength(5)]
        public string MediumPicHeight { get; set; } = "0";
        [DisplayName("  عرض عکس متوسط (px)")]
        [MaxLength(5)]
        public string MediumPicWidth { get; set; } = "0";
        public List<SlideShowGroupTranslation> SlideShowGroupTranslations { get; set; }

    }
}
