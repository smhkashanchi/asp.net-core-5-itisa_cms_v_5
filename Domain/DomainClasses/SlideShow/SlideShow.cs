﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.DomainClasses.SlideShow
{
    public class SlideShow
    {
        [Key]
        public Int16 SlideShow_ID { get; set; }
        [DisplayName("تصویر")]
        [MaxLength(5000)]
        public string Image { get; set; }
        [MaxLength(5000)]
        public string MediumImage { get; set; }
        [MaxLength(5000)]
        public string SmallImage { get; set; }
        [DisplayName("روش آپلود")] //اگر ترو باشد بصورت تغییر سایز آپلود شده
        public bool SimpleUpload { get; set; }

        [NotMapped]
        public byte GroupTranslationId { get; set; }
    }
}
