﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.DomainClasses.SlideShow
{
    public class SlideShowTranslation
    {
        [Key]
        public int SlideShowTranslation_ID { get; set; }
        [ForeignKey("SlideShow")]
        public Int16 SlideShow_Id { get; set; }
        [ForeignKey("SlideShowGroupTranslation")]

        [DisplayName("گروه")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        public byte? TranslationGroup_Id { get; set; }
        [DisplayName("عنوان")]
        [MaxLength(100)]
        public string Title{ get; set; }
        [DisplayName("متن")]
        [MaxLength(900)]
        public string Text{ get; set; }
        [DisplayName("لینک")]
        [MaxLength(500)]
        public string Link{ get; set; }
        [DisplayName("وضعیت")]
        public bool IsActive { get; set; }
        [DisplayName("زبان")]
        [ForeignKey("Language")]
        public string Language_Id { get; set; }

        public SlideShow SlideShow { get; set; }
        public SlideShowGroupTranslation SlideShowGroupTranslation { get; set; }
        public Language Language { get; set; }

    }
}
