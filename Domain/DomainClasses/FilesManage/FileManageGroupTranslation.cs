﻿using Domain.DomainClasses.Seo;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.DomainClasses.FilesManage
{
    public class FileManageGroupTranslation
    {
        [Key]
        public int FileManageGroupTranslation_ID { get; set; }
        [ForeignKey("FileManageGroup")]
        public Int16 FileManageGroup_Id { get; set; }
        [Required(ErrorMessage ="لطفا {0} را وارد نمایید")]
        [DisplayName("عنوان")]
        [MaxLength(100)]
        public string Title { get; set; }
        [DisplayName("اسلاگ")]
        [MaxLength(100)]
        public string Slug { get; set; }
        [ForeignKey("Language")]
        public string Language_Id { get; set; }
        [ForeignKey("MainSiteMap")]
        public int? SiteMap_Id { get; set; }
        public virtual MainSiteMap MainSiteMap { get; set; }

        public virtual FileManageGroup FileManageGroup { get; set; }
        public virtual Language Language { get; set; }
        public List<FileManageTranslation> FileManageTranslations { get; set; }

        [NotMapped]
        public Int16 ParentId { get; set; }
    }
}
