﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.DomainClasses.FilesManage
{
    public enum UploadType { File, Link, Script}
    public static class EnumUploadType
    {
        public static IEnumerable<T> GetValues<T>()
        {
            return Enum.GetValues(typeof(T)).Cast<T>();
        }
    }
    public class FileManage
    {
        [Key]
        public int FileManage_ID { get; set; }
        [DisplayName("نوع آپلود")]
        [Required(ErrorMessage ="لطفا {0} را وارد نمایید")]
        [MaxLength(10)]
        public string Type { get; set; }
        [DisplayName("فایل")]
        [MaxLength(3000)]
        public string FileData { get; set; }
        [DisplayName("اندازه فایل")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(20)]
        public string FileSize { get; set; } = "0";
        [DisplayName("تعداد دانلود")]
        public Int16 DownloadCount { get; set; } = 0;
        public DateTime CreateDate { get; set; }
        public bool IsActive { get; set; }

        public List<FileManageTranslation> FileManageTranslations { get; set; }

        [NotMapped]
        public string OldFileData { get; set; }
    }
}
