﻿using Domain.DomainClasses.Seo;

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.DomainClasses.FilesManage
{
    public class FileManageGroup
    {
        [Key]
        public Int16 FileManageGroup_ID { get; set; }
        [ForeignKey("FileManageGroupParent")]
        public Int16? Parent_Id { get; set; }
        public bool IsActive { get; set; }

        public virtual FileManageGroup FileManageGroupParent { get; set; }
        public virtual List<FileManageGroupTranslation> FileManageGroupTranslations{ get; set; }
    }
}
