﻿using Domain.DomainClasses.Seo;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.DomainClasses.FilesManage
{
    public class FileManageTranslation
    {
        [Key]
        public int FileManageTranslation_ID { get; set; }
        [ForeignKey("FileManage")]
        public int FileManage_Id { get; set; }
        [ForeignKey("FileManageGroupTranslation")]
        public int TranslationGroup_Id { get; set; }
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [DisplayName("عنوان")]
        [MaxLength(100)]
        public string Title { get; set; }
        [DisplayName("اسلاگ")]
        [MaxLength(100)]
        public string Slug { get; set; }
        [DisplayName("توضیحات")]
        //[MaxLength(9000)]
        public string Description { get; set; }
        [MaxLength(200)]
        public string MetaTitle { get; set; }
        [MaxLength(500)]
        public string MetaKeword { get; set; }
        [MaxLength(5000)]
        public string MetaDescription { get; set; }
        [MaxLength(5000)]
        public string MetaOther { get; set; }
        [ForeignKey("Language")]
        public string Language_Id { get; set; }
        [ForeignKey("MainSiteMap")]
        public int? SiteMap_Id { get; set; }
        public virtual MainSiteMap MainSiteMap { get; set; }
        public virtual FileManageGroupTranslation FileManageGroupTranslation { get; set; }
        public virtual FileManage FileManage { get; set; }
        public virtual Language Language { get; set; }
    }
}
