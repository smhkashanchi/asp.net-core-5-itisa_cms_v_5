﻿var isDevelopment = true;

function createSlideShow() {
    $.get("/Admin/SlideShows?handler=CreateSlideShow", function (result) {
        if (result.status == "error") {
            if (isDevelopment) {
                alert(result.data);
            } else {
                ToastMessage("خطایی رخ داده است", "info", "خطا");
            }
        }
        $(".modal-dialog").css('max-width', '65%');
        $(".modal-header").css('background-color', '#008000b8');
        $(".headerModalColor").text("افزودن اسلایدشو");
        $("#myModal").modal();
        $("#myModalBody").html(result);
    });
}

function insertSlideShow() {
    var frm = $("#slideShowFrm").valid();
    if (frm == true) {
        var slideShow = new FormData();
        var files = $("#Image").get(0).files;
        var orginalImage = $("#Image1").get(0).files;
        var mediumImage = $("#Image2").get(0).files;
        var thumbImage = $("#Image3").get(0).files;

        $("#btnInsertSlideShow").children(".fa-spinner").removeClass('hide');
        $("#btnInsertSlideShow").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');

        slideShow.append("GroupTranslationId", $("#slideShowGroup").val()),
            slideShow.append("imgSlideShow", files[0]),
            slideShow.append("imgOrginal", orginalImage[0]),
            slideShow.append("imgMedium", mediumImage[0]),
            slideShow.append("imgThumb", thumbImage[0])

        $.ajax({
            method: 'POST',
            url: '/Admin/SlideShows?handler=CreateSlideShow',
            contentType: false,
            processData: false,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            data: slideShow,

            success: function (result) {
                $("#btnInsertSlideShow").children(".fa-spinner").addClass('hide');
                $("#btnInsertSlideShow").children(".fa-spinner").siblings('span').text('ویرایش');

                if (result.status == "ok") {

                    $(".insert-div").addClass('hide');
                    $(".update-div").removeClass('hide');

                    $("#SlideShow_ID").val(result.slideShowId);
                    createSlideShowTranslation(result.slideShowId);
                }
                else if (result == "noImage") {
                    ToastMessage("تصویری برای اسلایدشو انتخاب نشده است", "warning", "هشدار");
                }
                else {
                    if (isDevelopment) {
                        alert(result.data);
                    } else {
                        ToastMessage("خطایی رخ داده است", "danger", "خطا");
                    }
                }
            },
            error: function (result) {
            }
        });
        //}
        //else {
        //    ToastMessage("لطفا یک تصویر انتخاب نمایید", "warning", "خطا");
        //}
    }
    else {
        return false;
    }
}

function createSlideShowTranslation(slideShowId) {
    $.get("/Admin/SlideShows?handler=CreateSlideShowTrans", { id: slideShowId }, function (result) {
        $("#slideShow_trans").html(result);
        $("#tab_1").removeClass("active");
        $("#slideShow_trans").addClass("active");

        $("a[href='#tab_1']").removeClass('active');
        $("a[href='#slideShow_trans']").addClass('active');
    });
}

function insertSlideShowTranslation() {
    var frm = $("#slideShowTransFrm").valid();
    if (frm == true) {
        if ($("#language_id").val() != "0") {

            $("#btnInsertSlideShowFinal").children(".fa-spinner").removeClass('hide');
            $("#btnInsertSlideShowFinal").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');

            var isactive = $('#IsActive:checked').length;
            var status = "";
            if (isactive === 1) { status = "true"; }
            else { status = "false"; }

            var slideShowTranslation = {
                SlideShow_Id: $("#SlideShow_Id").val(),
                Title: $("#Title").val(),
                Text: $("#Text").val(),
                Link: $("#Link").val(),
                TranslationGroup_Id: $("#TranslationGroup_Id").val(),
                Language_Id: $("#language_id").val(),
                IsActive: status
            }
            $.ajax({
                method: 'POST',
                url: '/Admin/SlideShows?handler=CreateSlideShowTrans',
                data: {
                    slideShowTranslation: slideShowTranslation,
                    __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
                },
                success: function (result) {
                    $(".fa-spinner").addClass('hide');
                    $(".fa-spinner").siblings('span').text('ثبت');

                    $(".insert-div").removeClass('hide');
                    $(".update-div").addClass('hide');

                    if (result == "ok") {
                        resetForm();
                        ToastMessage("اسلایدشو با موفقیت ثبت گردید", "success", "کاربر گرامی");
                        LoadDatatable("#slideShow_tb", true);
                    }
                    else if (result == "exsist") {
                        ToastMessage(" این گالری  برای این زبان قبلا ثبت شده است", "info", "کاربر گرامی");
                    }
                    else {
                        if (isDevelopment) {
                            alert(result.data);
                        } else {
                            ToastMessage("خطایی رخ داده است", "warning", "خطا");
                        }
                    }
                },
                error: function (result) {
                }
            });
        }
        else {
            ToastMessage(" لطفا یک زبان را انتخاب کنید", "info", "کاربر گرامی");
        }
    }
    else {
        return false;
    }
}

function getSlideShowGroupByLang(langId, status, selectedOption) {
    $.get("/Admin/SlideShows?handler=SlideShowGroupByLang", { id: langId }, function (result) {
        //var options = "";
        //if (status == "edit") {
        var options = "<option value=''>انتخاب کنید ...</option>";
        //}

        for (var i = 0; i < result.length; i++) {
            options += "<option value='" + result[i].slideShowGroupTranslation_ID + "'>" + result[i].title + "</option>";
        }
        $("#TranslationGroup_Id").html(options);
        $("#TranslationGroup_Id").val(selectedOption);
    });
}

function viewAllSlideShows(GroupId) {
    loadingPageShow();
    $("#slideShow_tb").DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.24/i18n/Persian.json"
        },
        "processing": true,
        "serverSide": true,
        "filter": true,
        "destroy": true,   /// allow reinitialize datattable
        "ajax": {
            "url": "/Admin/SlideShows?handler=ViewAllSlideShows",
            "type": "POST",
            "datatype": "json",
            "data": {
                groupId: GroupId,
                __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
            },
        },

        "columns": [
            {
                data: "slideShow.smallImage",
                render: function (data) {
                    return '<img width="60" height="60" src="/' + data + '">';
                },
            },
            {
                mRender: function (data, type, row) {
                    var link = "#";
                    if (row.link != null)
                        link = row.link;

                    return "<a href='" + row.link + "' target='_blank' class='tooltips'  title='" + link + "' data-toggle='tooltip' data-placement='top'>" + row.title + "</a>"
                },
                name: "title",
            },
            
            {
                data: "isActive",
                render: function (data) {
                    if (data) {
                        return '<span class="text-success">فعال</span >';
                    }
                    else {
                        return '<span class="text-danger">غیر فعال</span >';
                    }
                },
            },
            {
                mRender: function (data, type, row) {
                    return '<div class="btn-group" role="group">' +
                        '<span class="btn btn-outline-info" onclick="editSlideShow(' + row.slideShow_Id + ',' + row.slideShowTranslation_ID + ')" ><i class="fa fa-edit fa-sm"></i></span>' +
                        '<span class="btn btn-outline-danger" onclick="deleteSlideShow(' + row.slideShow_Id + ',' + row.slideShowTranslation_ID + ')"><i class="fa fa-trash fa-sm"></i></span></div>';
                }
            },
        ]

    });
    loadingPageHide();
    GroupTransId = 0;
}

function editSlideShow(slideShowId, translationId) {
    $.get("/Admin/SlideShows?handler=EditSlideShow", { id: slideShowId }, function (result) {
        if (result.status == "error") {
            if (isDevelopment) {
                alert(result.data);
            } else {
                ToastMessage("خطایی رخ داده است", "info", "خطا");
            }
        }
        else {
            $(".modal-dialog").css('max-width', '65%');
            $(".modal-header").css('background-color', '#008074b8');
            $(".headerModalColor").text("ویرایش اسلایدشو");
            $("#myModal").modal();
            $("#myModalBody").html(result);
            editSlideShowTranslation(translationId);
        }
    });
}

function editSlideShowTranslation(translationId) {
    $.get("/Admin/SlideShows?handler=EditSlideShowTrans", { id: translationId }, function (res) {
        $("#slideShow_trans").html(res);
    });
}

function getSlideShowTransInfoWithLanguage(langId, slideShowId) {

    $.get("/Admin/SlideShows?handler=SlideShowTransInfo", { id: langId, slideShowId: slideShowId }, function (res) {
        if (res.status == "ok") {
            $(".update_div").removeClass('hide');
            $(".insert_div").addClass('hide');

            getSlideShowGroupByLang($("#language_id").val(), "edit", res.groupId);

            $("#Title").val(res.title);
            $("#Text").val(res.text);
            $("#Link").val(res.link);
            //$("#Group_Id").val(res.groupId);
            $("#SlideShow_Id").val(res.slideShow_Id);
            $("#SlideShowTranslation_ID").val(res.id);
            if (res.isActive)
                $(".icheckbox_square-blue").addClass('checked');
            else
                $(".icheckbox_square-blue").removeClass('checked');
        }
        else {
            resetForm2();
            $(".update_div").addClass('hide');
            $(".insert_div").removeClass('hide');
        }
    });
}

function getSlideShowImageSize(groupId) {
    $.get("/Admin/SlideShows?handler=SlideShowImageSize", { groupId: groupId }, function (res) {
        $("#imgWidth").text(res.imageWidth + " * ");
        $("#imgHeight").text(res.imageHeight);
        $("#mediumImgWidth").text(res.mediumPicWidth + " * ");
        $("#mediumImgHeight").text(res.mediumPicHeight);
        $("#smallImgWidth").text(res.smallPicWidth + " * ");
        $("#smallImgHeight").text(res.smallPicHeight);
    });
}

function updateSlideShow() {
    var frm = $("#slideShowFrm").valid();
    if (frm == true) {

        var slideShow = new FormData();
        var files = $("#Image").get(0).files;
        var orginalImage = $("#Image1").get(0).files;
        var mediumImage = $("#Image2").get(0).files;
        var thumbImage = $("#Image3").get(0).files;


        $("#btnUpdateSlideShow").children(".fa-spinner").removeClass('hide');
        $("#btnUpdateSlideShow").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');


        slideShow.append("SimpleUpload", $("#SimpleUpload").val()),
            slideShow.append("Image", $("#oldImage").val()),
            slideShow.append("SmallImage", $("#thumbOldImage").val()),
            slideShow.append("MediumImage", $("#mediumOldImage").val()),
            slideShow.append("SlideShow_ID", $("#SlideShow_ID").val()),
            slideShow.append("GroupTranslationId", $("#slideShowGroup").val()),
            slideShow.append("imgOrginal", orginalImage[0]),
            slideShow.append("imgMedium", mediumImage[0]),
            slideShow.append("imgThumb", thumbImage[0]),
            slideShow.append("imgSlideShow", files[0])

        $.ajax({
            method: 'POST',
            url: '/Admin/SlideShows?handler=EditSlideShow',
            contentType: false,
            processData: false,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            data: slideShow,
            success: function (result) {
                $("#btnUpdateSlideShow").children(".fa-spinner").addClass('hide');
                $("#btnUpdateSlideShow").children(".fa-spinner").siblings('span').text('ویرایش');

                if (result == "ok") {
                    LoadDatatable("#slideShow_tb", false);
                    ToastMessage("اطلاعات مشترک اسلایدشو با موفقیت ویرایش گردید", "success", "کاربر گرامی");
                }
                else {
                    if (isDevelopment) {
                        alert(result.data);
                    } else {
                        ToastMessage("خطایی رخ داده است", "info", "خطا");
                    }
                }
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}

function updateSlideShowTranslation() {
    var frm = $("#slideShowTransFrm").valid();
    if (frm == true) {
        if ($("#language_id").val() != "0") {
            $("#btnUpdateSlideShowFinal").children(".fa-spinner").removeClass('hide');
            $("#btnUpdateSlideShowFinal").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');

            var isactive = $('#IsActive:checked').length;
            var status = "";
            if (isactive === 1) { status = "true"; }
            else { status = "false"; }

            var slideShowTranslation = {
                SlideShowTranslation_ID: $("#SlideShowTranslation_ID").val(),
                SlideShow_Id: $("#SlideShow_Id").val(),
                Title: $("#Title").val(),
                Text: $("#Text").val(),
                Link: $("#Link").val(),
                TranslationGroup_Id: $("#TranslationGroup_Id").val(),
                Language_Id: $("#language_id").val(),
                IsActive: status
            }

            $.ajax({
                method: 'POST',
                url: '/Admin/SlideShows?handler=EditSlideShowTrans',
                data: {
                    slideShowTranslation: slideShowTranslation,
                    __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
                },
                success: function (result) {
                    $(".fa-spinner").addClass('hide');
                    $(".fa-spinner").siblings('span').text('ویرایش');

                    if (result == "ok") {
                        resetForm();
                        ToastMessage("اسلایدشو با موفقیت ویرایش گردید", "success", "کاربر گرامی");
                        LoadDatatable("#slideShow_tb", false);
                    }
                    //else if (result == "exsist") {
                    //    ToastMessage(" این کد رنگ برای این زبان قبلا ثبت شده است", "info", "کاربر گرامی");
                    //}
                    else {
                        if (isDevelopment) {
                            alert(result.data);
                        } else {
                            ToastMessage("خطایی رخ داده است", "warning", "خطا");
                        }
                    }
                },
                error: function (result) {
                }
            });
        }
        else {
            ToastMessage(" لطفا یک زبان را انتخاب کنید", "info", "کاربر گرامی");
        }

    }
    else {
        return false;
    }
}

function deleteSlideShow(slideShowId, translationId) {
    $.confirm({
        title: 'آیا برای حذف اسلایدشو مطمئن هستید؟', icon: 'fa fa-warning', theme: "material", animation: 'scale', content: '',
        buttons: {
            confirm: {
                text: 'تایید',
                btnClass: 'btn-blue',
                action: function () {
                    $.get("/Admin/SlideShows?handler=DeleteSlideShow", { id: translationId, slideShowId: slideShowId, status: "one" },
                        function (result) {
                            if (result == "ok") {
                                ToastMessage("اسلایدشو با موفقیت حذف گردید", "success", "کاربر گرامی");
                                LoadDatatable("#slideShow_tb", false);
                            }
                            else {
                                if (isDevelopment) {
                                    alert(result.data);
                                } else {
                                    ToastMessage("خطایی رخ داده است", "warning", "خطا");
                                }
                            }
                        });
                }
            },
            deleteall: {
                text: 'حذف در تمام زبان ها',
                btnClass: 'btn-red',
                action: function () {

                    $.get("/Admin/SlideShows?handler=DeleteSlideShow", { id: translationId, slideShowId: slideShowId, status: "all" },
                        function (result) {
                            if (result == "ok") {
                                ToastMessage("اسلایدشو در تمام زبان ها با موفقیت حذف گردید", "success", "کاربر گرامی");
                                LoadDatatable("#slideShow_tb", false);
                            }
                            else {
                                if (isDevelopment) {
                                    alert(result.data);
                                } else {
                                    ToastMessage("خطایی رخ داده است", "warning", "خطا");
                                }
                            }
                        });
                }
            },
            cancel: {
                text: 'انصراف',

            }
        },
    });
}

function getSlideShowGroupByLang_Global(langId, selector) {
    $.get("/Admin/SlideShows?handler=SlideShowGroupByLang", { id: langId }, function (result) {
        var options = "<option value=''>گروه اسلایدشو ...</option>";
        for (var i = 0; i < result.length; i++) {
            options += "<option value='" + result[i].slideShowGroup_Id + "'>" + result[i].title + "</option>";
        }
        $(selector).html(options);
    });
}

function resetForm() {
    $("#language_id").val('0');
    $("#Group_Id").val('0');
    $("#Title").val('');
    $("#Link").val('');
    $("#Text").val('');
}
function resetForm2() {
    $("#Group_Id").val('0');
    $("#Title").val('');
    $("#Link").val('');
    $("#Text").val('');
}