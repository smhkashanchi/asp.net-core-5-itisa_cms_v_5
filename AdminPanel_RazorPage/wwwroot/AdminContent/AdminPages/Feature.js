﻿var isDevelopment = true;

function createFeature() {
    $.get("/Admin/Features?handler=CreateFeature", function (result) {
        if (result.status == "error") {
            if (isDevelopment) {
                alert(result.data);
            } else {
                ToastMessage("خطایی رخ داده است", "info", "خطا");
            }
        }
        $(".modal-dialog").css('max-width', '65%');
        $(".modal-header").css('background-color', '#008000b8');
        $(".headerModalColor").text("افزودن ویژگی");
        $("#myModal").modal();
        $("#myModalBody").html(result);
    });
}

function insertFeature() {
    var frm = $("#featureFrm").valid();
    if (frm == true) {
        $("#btnInsertFeature").children(".fa-spinner").removeClass('hide');
        $("#btnInsertFeature").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');


        var isVariable = $('#IsVariable:checked').length;
        var status_isVariable = "";
        if (isVariable === 1) { status_isVariable = "true"; }
        else { status_isVariable = "false"; }

        var useFilter = $('#UseFilter:checked').length;
        var status_useFilter = "";
        if (useFilter === 1) { status_useFilter = "true"; }
        else { status_useFilter = "false"; }

        var feature = {
            FeatureGroup_Id: $("#FeatureGroup_Id").val(),
            IsVariable: status_isVariable,
            UseFilter: status_useFilter
        }
        $.ajax({
            method: 'POST',
            url: '/Admin/Features?handler=CreateFeature',
            data: {
                feature: feature,
                __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
            },
            success: function (result) {
                $("#btnInsertFeature").children(".fa-spinner").addClass('hide');
                $("#btnInsertFeature").children(".fa-spinner").siblings('span').text('ثبت');


                if (result.status == "ok") {

                    $(".insert-div").addClass('hide');
                    $(".update-div").removeClass('hide');

                    $("#Feature_ID").val(result.featureId);
                    createFeatureTranslation(result.featureId);
                }
                else {
                    if (isDevelopment) {
                        alert(result.data);
                    } else {
                        ToastMessage("خطایی رخ داده است", "warning", "خطا");
                    }
                }
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}

function createFeatureTranslation(featureId) {
    $.get("/Admin/Features?handler=CreateFeatureTranslation", { id: featureId }, function (result) {
        $("#feature_trans").html(result);
        $("#tab_1").removeClass("active");
        $("#feature_trans").addClass("active");

        $("a[href='#tab_1']").removeClass('active');
        $("a[href='#feature_trans']").addClass('active');
    });
}

function insertFeatureTranslation() {
    var frm = $("#featureTransFrm").valid();
    if (frm == true) {
        if ($("#language_id").val() != "0") {

            $("#btnInsertFeatureFinal").children(".fa-spinner").removeClass('hide');
            $("#btnInsertFeatureFinal").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');

            var featureTranslation = {
                Feature_Id: $("#Feature_Id").val(),
                Title: $("#Title").val(),
                Language_Id: $("#language_id").val(),
            }
            $.ajax({
                method: 'POST',
                url: '/Admin/Features?handler=CreateFeatureTranslation',
                data: {
                    featureTranslation: featureTranslation,
                    __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
                },
                success: function (result) {
                    $(".fa-spinner").addClass('hide');
                    $(".fa-spinner").siblings('span').text('ثبت');

                    $(".insert-div").removeClass('hide');
                    $(".update-div").addClass('hide');

                    if (result == "ok") {
                        resetForm();
                        ToastMessage("ویژگی با موفقیت ثبت گردید", "success", "کاربر گرامی");
                        LoadDatatable("#feature_tb", true);
                        getFeaturesByLang_Global("#feature_ddl", "", "");
                    }
                    else if (result == "exsist") {
                        ToastMessage(" این ویژگی  برای این زبان قبلا ثبت شده است", "info", "کاربر گرامی");
                    }
                    else {
                        if (isDevelopment) {
                            alert(result.data);
                        } else {
                            ToastMessage("خطایی رخ داده است", "warning", "خطا");
                        }
                    }
                },
                error: function (result) {
                }
            });
        }
        else {
            ToastMessage(" لطفا یک زبان را انتخاب کنید", "info", "کاربر گرامی");
        }
    }
    else {
        return false;
    }
}

function viewAllFeatures(GroupId) {
    loadingPageShow();
    $("#feature_tb").DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.24/i18n/Persian.json"
        },
        "processing": true,
        "serverSide": true,
        "filter": true,
        "destroy": true,   /// allow reinitialize datattable
        "ajax": {
            "url": "/Admin/Features?handler=ViewAllFeatures",
            "type": "POST",
            //'contentType': 'application/json',
            "beforeSend": function (xhr) {
                loadingPageShow();
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            "data": { groupId: GroupId },
           
        },

        "columns": [
            
            { "data": "title", "name": "title", "autoWidth": true },
            {
                data: "feature.isVariable",
                render: function (data) {
                    if (data) {
                        return '<span class="badge badge-info">متغیر</span >';
                    }
                    else {
                        return '<span class="badge badge-secondary">ثابت</span >';
                    }
                }
            },
            {
                data: "feature.useFilter",
                render: function (data) {
                    if (data) {
                        return '<span class="badge badge-success">استفاده میشود</span >';
                    }
                    else {
                        return '<span class="badge badge-secondary">عدم استفاده</span >';
                    }
                }
            },
            {
                mRender: function (data, type, row) {
                    return '<div class="btn-group" role="group">' +
                        '<span class="btn btn-outline-info" onclick="editFeature(' + row.feature_Id + ',' + row.featureTranslation_ID + ')" ><i class="fa fa-edit fa-sm"></i></span>' +
                        '<span class="btn btn-outline-danger" onclick="deleteFeature(' + row.feature_Id + ',' + row.featureTranslation_ID + ')"><i class="fa fa-trash fa-sm"></i></span></div>';
                }
            },
        ]

    });
    loadingPageHide();
}

function editFeature(featureId, translationId) {
    $.get("/Admin/Features?handler=EditFeature", { id: featureId }, function (result) {
        if (result.status == "error") {
            if (isDevelopment) {
                alert(result.data);
            } else {
                ToastMessage("خطایی رخ داده است", "info", "خطا");
            }
        }
        else {
            $(".modal-dialog").css('max-width', '65%');
            $(".modal-header").css('background-color', '#008074b8');
            $(".headerModalColor").text("ویرایش ویژگی");
            $("#myModal").modal();
            $("#myModalBody").html(result);
            editFeatureTranslation(translationId);
        }
    });
}

function editFeatureTranslation(Id) {
    $.get("/Admin/Features?handler=EditFeatureTranslation", { id: Id }, function (res) {
        $("#feature_trans").html(res);
    });
}

function getFeatureInfoWithLanguage(langId, featureId) {

    $.get("/Admin/Features?handler=FeatureInfo", { id: langId, featureId: featureId }, function (res) {
        if (res.status == "ok") {
            $(".update_div").removeClass('hide');
            $(".insert_div").addClass('hide');

            $("#Title").val(res.title);

            $("#FeatureTranslation_ID").val(res.id);
        }
        else {
            $("#Title").val('');
            $("#FeatureTranslation_ID").val('');
            $(".update_div").addClass('hide');
            $(".insert_div").removeClass('hide');
        }
    });
}

function updateFeature() {
    var frm = $("#featureFrm").valid();
    if (frm == true) {

        $("#btnUpdateFeature").children(".fa-spinner").removeClass('hide');
        $("#btnUpdateFeature").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');

        var isVariable = $('#IsVariable:checked').length;
        var status_isVariable = "";
        if (isVariable === 1) { status_isVariable = "true"; }
        else { status_isVariable = "false"; }

        var useFilter = $('#UseFilter:checked').length;
        var status_useFilter = "";
        if (useFilter === 1) { status_useFilter = "true"; }
        else { status_useFilter = "false"; }

        var feature = {
            Feature_ID: $("#Feature_ID").val(),
            FeatureGroup_Id: $("#FeatureGroup_Id").val(),
            IsVariable: status_isVariable,
            UseFilter: status_useFilter
        }
        $.ajax({
            method: 'POST',
            url: '/Admin/Features?handler=EditFeature',
            data: {
                feature: feature,
                __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
            },
            success: function (result) {
                $("#btnUpdateFeature").children(".fa-spinner").addClass('hide');
                $("#btnUpdateFeature").children(".fa-spinner").siblings('span').text('ویرایش');

                if (result == "ok") {
                    LoadDatatable("#feature_tb", false);
                    ToastMessage("اطلاعات مشترک ویژگی با موفقیت ویرایش گردید", "success", "کاربر گرامی");
                }
                else {
                    if (isDevelopment) {
                        alert(result.data);
                    } else {
                        ToastMessage("خطایی رخ داده است", "info", "خطا");
                    }
                }
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}

function updateFeatureTranslation() {
    var frm = $("#featureTransFrm").valid();
    if (frm == true) {
        if ($("#language_id").val() != "0") {
            $("#btnUpdateFeatureFinal").children(".fa-spinner").removeClass('hide');
            $("#btnUpdateFeatureFinal").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');

            var featureTranslation = {
                FeatureTranslation_ID: $("#FeatureTranslation_ID").val(),
                Feature_Id: $("#Feature_Id").val(),
                Title: $("#Title").val(),
                Language_Id: $("#language_id").val(),
            }

            $.ajax({
                method: 'POST',
                url: '/Admin/Features?handler=EditFeatureTranslation',
                data: {
                    featureTranslation: featureTranslation,
                    __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
                },
                success: function (result) {
                    $("#btnUpdateFeatureFinal").children(".fa-spinner").addClass('hide');
                    $("#btnUpdateFeatureFinal").children(".fa-spinner").siblings('span').text('ویرایش');

                    if (result == "ok") {
                        resetForm();
                        ToastMessage("ویژگی با موفقیت ویرایش گردید", "success", "کاربر گرامی");
                        LoadDatatable("#feature_tb", false);
                        getFeaturesByLang_Global("#feature_ddl", "", "");
                    }
                 
                    else {
                        if (isDevelopment) {
                            alert(result.data);
                        } else {
                            ToastMessage("خطایی رخ داده است", "warning", "خطا");
                        }
                    }
                },
                error: function (result) {
                }
            });
        }
        else {
            ToastMessage(" لطفا یک زبان را انتخاب کنید", "info", "کاربر گرامی");
        }

    }
    else {
        return false;
    }
}

function deleteFeature(featureId, translationId) {
    $.confirm({
        title: 'آیا برای حذف ویژگی مطمئن هستید؟', icon: 'fa fa-warning', theme: "material", animation: 'scale', content: '',
        buttons: {
            confirm: {
                text: 'تایید',
                btnClass: 'btn-blue',
                action: function () {
                    $.get("/Admin/Features?handler=DeleteFeatureTranslation", { id: translationId, featureId: featureId, status: "one" },
                        function (result) {
                            if (result == "ok") {
                                ToastMessage("ویژگی با موفقیت حذف گردید", "success", "کاربر گرامی");
                                LoadDatatable("#feature_tb", false);
                            }
                            else if (result == "existInReply") {
                                ToastMessage("برای ویژگی مقدار تعریف شده است", "info", "کاربر گرامی");
                            }
                            else {
                                if (isDevelopment) {
                                    alert(result.data);
                                } else {
                                    ToastMessage("خطایی رخ داده است", "warning", "خطا");
                                }
                            }
                        });
                }
            },
            deleteall: {
                text: 'حذف در تمام زبان ها',
                btnClass: 'btn-red',
                action: function () {

                    $.get("/Admin/Features?handler=DeleteFeatureTranslation", { id: translationId, featureId: featureId, status: "all" },
                        function (result) {
                            if (result == "ok") {
                                ToastMessage("ویژگی با موفقیت حذف گردید", "success", "کاربر گرامی");
                                LoadDatatable("#feature_tb", false);
                            }
                            else if (result == "existInReply") {
                                ToastMessage("برای ویژگی مقدار تعریف شده است", "info", "کاربر گرامی");
                            }
                            else {
                                if (isDevelopment) {
                                    alert(result.data);
                                } else {
                                    ToastMessage("خطایی رخ داده است", "warning", "خطا");
                                }
                            }
                        });
                }
            },
            cancel: {
                text: 'انصراف',

            }
        },
    });
}

function getFeaturesByLang_Global(featureGroupId,selector, selectedItem, status) {
    $.get("/Admin/Features?handler=Features_Global", { featureGroupId: featureGroupId}, function (result) {
        var options = "<option value=''>ویژگی ...</option>";
        for (var i = 0; i < result.length; i++) {
            options += "<option value='" + result[i].feature_Id + "'>" + result[i].title + "</option>";
        }
        $(selector).html(options);
        if (status == "edit")
            $(selector).val(selectedItem);
    });
}

function resetForm() {
    $("#language_id").val('0');
    $("#Title").val('');
}