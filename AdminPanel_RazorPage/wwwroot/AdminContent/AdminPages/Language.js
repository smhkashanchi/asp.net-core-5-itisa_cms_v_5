﻿var isDevelopment = true;
///Toast Message
var ToastMessage = function (message, type, title) {
    var msgs = [message];
    var $toast = toastr[type](msgs, title);
    $toastlast = $toast;
    //return msgs;
};
toastr.options = {
    closeButton: 'true',
    progressBar: 'true',
    positionClass: 'toast-top-left',
};
$("#btnCreateLanguage").on('click', function (e) {
    e.preventDefault();
    $.get("/Admin/Languages?handler=Create", function (result) {
        $(".modal-dialog").css('max-width', '45%');
        $(".modal-header").css('background-color', '#008000b8');
        $(".headerModalColor").text("افزودن زبان");
        $("#myModal").modal();
        $("#myModalBody").html(result);
    });
});

$("#btnInsertLang").on('click', function (e) {
    e.preventDefault();
    var frm = $("#langFrm").valid();
    if (frm === true) {
        if ($("#Language_Id").val() != "0") {
            $(".fa-spinner").removeClass('hide');
            $(".fa-spinner").siblings('span').text('منتظر بمانید ...');


            var isactive = $('#Language_IsActive:checked').length;
            var status = "";
            if (isactive === 1) { status = "true"; }
            else { status = "false"; }

            var language = {
                Id: $("#Language_Id").val(),
                IsActive: status,
                Name: $("#Language_Name").val()
            }
            $.ajax({
                method: 'POST',
                url: '/Admin/Languages?handler=Create',
                data: {
                    language: language,
                    __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
                },
                success: function (result) {
                    $(".fa-spinner").addClass('hide');
                    $(".fa-spinner").siblings('span').text('ثبت');

                    if (result.status == "ok") {
                        var table = $('#language_tb').DataTable();
                        table.ajax.reload(null, false);

                        $("#myModal").modal("hide");

                        ToastMessage("زبان با موفقیت ثبت گردید", "success", "کاربر گرامی");
                    }
                    else if (result.status == "isExsist") {
                        ToastMessage(" شناسه زبان قبلا ثبت شده است", "info", "کاربر گرامی");
                    }
                    else {
                        if (isDevelopment) {
                            alert(result.data);
                        } else {
                            ToastMessage("خطایی رخ داده است", "info", "خطا");
                        }
                    }
                },
                error: function (result) {
                }
            });
        }
        else {
            ToastMessage(" لطفا یک شناسه زبان انتخاب کنید", "warning", "کاربر گرامی");
        }
    }
    else {
        return false;
    }
});

function viewAllLanguages() {
    $("#language_tb").DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.24/i18n/Persian.json"
        },
        "processing": true,
        "serverSide": true,
        "filter": true,
        "ajax": {
            "url": "/api/LanguageApi",
            "type": "POST",
            "datatype": "json"
        },

        "columns": [
            { "data": "id","name":"Id" },
            { "data": "name", "name": "Name"},
            {
                data: "isActive",
                render: function (data) {
                    if (data) {
                        return '<span class="text-success">فعال</span >';
                    }
                    else {
                        return '<span class="text-danger">غیر فعال</span >';
                    }
                },
            },
            {
                mRender: function (data, type, row) {
                    return '<div class="btn-group" role="group">' +
                        '<span class="btn btn-outline-info btnLanguageEdit" data-id="' + row.id + '"><i class="fa fa-edit fa-sm"></i></span>' +
                        '<span class="btn btn-outline-danger btnLanguageDelete" data-id="' + row.id + '"><i class="fa fa-trash fa-sm"></i></span></div>';
                }
            },
        ]

    });
    loadingPage();
}


$("#language_tb").on("click", " .btnLanguageEdit", function () {
    var Id = $(this).attr('data-id');
    $.get("/Admin/Languages?handler=Edit", { id: Id }, function (result) {
        $(".modal-dialog").css('max-width', '45%');
        $(".modal-header").css('background-color', '#008074b8');
        $(".headerModalColor").text("ویرایش زبان");
        $("#myModal").modal();
        $("#myModalBody").html(result);
    });
});

$("#btnUpdateLang").on('click', function () {
    var frm = $("#langFrmEdit").valid();
    if (frm == true) {
        $(".fa-spinner").removeClass('hide');
        $(".fa-spinner").siblings('span').text('منتظر بمانید ...');

        var isactive = $('#IsActive:checked').length;
        var status = "";
        if (isactive === 1) { status = "true"; }
        else { status = "false"; }

        var language = {
            Id: $("#Id").val(),
            IsActive: status,
            Name: $("#Name").val()
        }
        $.ajax({
            method: 'POST',
            url: '/Admin/Languages?handler=Edit',
            data: {
                language: language,
                __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
            },
            success: function (result) {
                $(".fa-spinner").addClass('hide');
                $(".fa-spinner").siblings('span').text('ویرایش');
                if (result == "ok") {
                    var table = $('#language_tb').DataTable();
                    table.ajax.reload(null, false);
                    $("#myModal").modal("hide");

                    ToastMessage("زبان با موفقیت ویرایش گردید", "success", "کاربر گرامی");
                }
                else if (result == "isExsist") {
                    ToastMessage(" شناسه زبان وارد شده قبلا ثبت شده است", "info", "کاربر گرامی");
                }
                else {
                    if (isDevelopment) {
                        alert(result.data);
                    } else {
                        ToastMessage("خطایی رخ داده است", "info", "خطا");
                    }
                }
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
});

$("#language_tb").on("click", " .btnLanguageDelete", function (e) {
    e.preventDefault();
    var Id = $(this).attr('data-id');
    $.confirm({
        title: 'آیا مطمئن هستید؟', icon: 'fa fa-warning', animation: 'scale', content: '',
        buttons: {
            confirm: {
                text: 'تایید',
                btnClass: 'btn-blue',
                action: function () {

                    $.get("/Admin/Languages?handler=Delete", { id: Id }, function (result) {
                        if (result == "ok") {
                            ToastMessage("زبان با موفقیت حذف گردید", "success", "کاربر گرامی");
                            var table = $('#language_tb').DataTable();
                            table.ajax.reload(null, false);
                        }
                        else {
                            if (isDevelopment) {
                                alert(result.data);
                            } else {
                                ToastMessage("خطایی رخ داده است", "info", "خطا");
                            }
                        }
                    });
                }
            },
            cancel: {
                text: 'انصراف',
            }
        },
    });

});

$('#excel').on('click', function () {
    tableToExcel('language_tb', "test");
});


$("#print").on('click', function () {
    printHtml('language_tb');
});








