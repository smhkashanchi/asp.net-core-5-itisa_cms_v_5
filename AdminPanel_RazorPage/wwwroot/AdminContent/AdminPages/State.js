﻿var isDevelopment = true;

function createState() {
    $.get("/Admin/Regions?handler=CreateState", function (result) {
        if (result.status == "error") {
            if (isDevelopment) {
                alert(result.msg);
            } else {
                ToastMessage("خطایی رخ داده است", "info", "خطا");
            }
        }
        $(".modal-dialog").css('max-width', '55%');
        $(".modal-header").css('background-color', '#008000b8');
        $(".headerModalColor").text("افزودن استان");
        $("#myModal").modal();
        $("#myModalBody").html(result);
    });
}

function insertState() {
    var frm = $("#stateFrm").valid();
    if (frm == true) {
        $("#btnInsertState").children(".fa-spinner").removeClass('hide');
        $("#btnInsertState").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');
        var isactive = $('#IsActive:checked').length;
        var status = "";
        if (isactive === 1) { status = "true"; }
        else { status = "false"; }

        var state = {
            CountryId: $("#CountryId").val(),
            Name: $("#Name").val(),
            Code: $("#Code").val(),
            IsActive: status,
        }
        $.ajax({
            method: 'POST',
            url: '/Admin/Regions?handler=CreateState',
            data: {
                state: state,
                __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
            },
            success: function (result) {
                $(".fa-spinner").addClass('hide');
                $(".fa-spinner").siblings('span').text('ثبت');

                if (result == "ok") {

                    LoadDatatable("#state_tb", false);
                    //$("#myModal").modal("hide");
                    ToastMessage("استان با موفقیت ثبت گردید", "success", "کاربر گرامی");
                    getStateByCountryId_Global("#state_ddl", "");
                    clearForm();
                }
                else if (result == "exists") {
                    ToastMessage(" این استان قبلا ثبت شده است", "info", "کاربر گرامی");
                }
                else {
                    if (isDevelopment) {
                        alert(result.data);
                    } else {
                        ToastMessage("خطایی رخ داده است", "info", "خطا");
                    }
                }
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}

function viewAllStates(countryId) {
    loadingPageShow();
    $("#state_tb").DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.24/i18n/Persian.json"
        },
        "processing": true,
        "serverSide": true,
        "filter": true,
        "destroy": true,
        "ajax": {
            "url": "/Admin/Regions?handler=ViewAllStates",
            "type": "POST",
            "datatype": "json",
            "data": {
                countryId: countryId,
                __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
            },
        },

        "columns": [
            { "data": "name", "name": "Name" },
            { "data": "code", "name": "Code" },
            {
                data: "isActive",
                render: function (data) {
                    if (data) {
                        return '<span class="text-success">فعال</span >';
                    }
                    else {
                        return '<span class="text-danger">غیر فعال</span >';
                    }
                },
            },
            {
                mRender: function (data, type, row) {
                    return '<div class="btn-group" role="group">' +
                        '<span class="btn btn-outline-info" onclick="editState(' + row.state_ID + ')"><i class="fa fa-edit fa-sm"></i></span>' +
                        '<span class="btn btn-outline-danger btnStateDelete" onclick="deleteState(' + row.state_ID + ')"><i class="fa fa-trash fa-sm"></i></span></div>';
                }
            },
        ]

    });
    
    loadingPageHide();
}

function editState(Id) {
    $.get("/Admin/Regions?handler=EditState", { id: Id }, function (result) {
        if (result.status == "error") {
            if (isDevelopment) {
                alert(result.msg);
            } else {
                ToastMessage("خطایی رخ داده است", "info", "خطا");
            }
        }
        $(".modal-dialog").css('max-width', '55%');
        $(".modal-header").css('background-color', '#008074b8');
        $(".headerModalColor").text("ویرایش استان");
        $("#myModal").modal();
        $("#myModalBody").html(result);
    });
}

function updateState() {
    var frm = $("#stateFrm").valid();
    if (frm == true) {
        $("#btnUpdateState").children(".fa-spinner").removeClass('hide');
        $("#btnUpdateState").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');
        var isactive = $('#IsActive:checked').length;
        var status = "";
        if (isactive === 1) { status = "true"; }
        else { status = "false"; }

        var state = {
            State_ID: $("#State_ID").val(),
            Name: $("#Name").val(),
            Code: $("#Code").val(),
            CountryId: $("#CountryId").val(),
            IsActive: status,
        }
        $.ajax({
            method: 'POST',
            url: '/Admin/Regions?handler=EditState',
            data: {
                state: state,
                __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
            },
            success: function (result) {
                $(".fa-spinner").addClass('hide');
                $(".fa-spinner").siblings('span').text('ویرایش');

                if (result == "ok") {
                    LoadDatatable("#state_tb", false);
                    $("#myModal").modal("hide");
                    ToastMessage("استان با موفقیت ویرایش گردید", "success", "کاربر گرامی");
                    getStateByCountryId_Global("#state_ddl", "");
                }
                else {
                    if (isDevelopment) {
                        alert(result.data);
                    } else {
                        ToastMessage("خطایی رخ داده است", "info", "خطا");
                    }
                }
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}

function deleteState(stateId) {
    $.confirm({
        title: 'آیا برای حذف این استان مطمئن هستید؟', icon: 'fa fa-warning', theme: 'material', animation: 'scale', content: '',
        buttons: {
            confirm: {
                text: 'تایید',
                btnClass: 'btn-blue',
                action: function () {

                    $.get("/Admin/Regions?handler=DeleteState", { id: stateId }, function (result) {
                        if (result == "ok") {
                            LoadDatatable("#state_tb", false);
                            ToastMessage("استان با موفقیت حذف گردید", "success", "کاربر گرامی");
                            getStateByCountryId_Global("#state_ddl", "");
                        }
                        else if (result == "exists") {
                            ToastMessage("برای این استان شهر تعریف شده است", "warning", "کاربر گرامی");
                        }
                        else if (result.status == "error") {
                            if (isDevelopment) {
                                alert(result.data);
                            } else {
                                ToastMessage("خطایی رخ داده است", "info", "خطا");
                            }
                        }
                    });
                }
            },
            cancel: {
                text: 'انصراف',
            }
        },
    });

}

function getStateByCountryId_Global(selector, CountryId,status,selectedValue) {
    $.get("/Admin/Regions?handler=StatesByCountryId", { countryId: CountryId }, function (result) {
        
        var options = "<option value=''>استان...</option>";
        for (var i = 0; i < result.length; i++) {
            options += "<option value='" + result[i].state_ID + "'>" + result[i].name + "</option>";
        }
        $(selector).html(options);
        if (status == "edit")
            $(selector).val(selectedValue);
    });
}

function clearForm() {
    $("#CountryId").val('');
    $("#Name").val('');
    $("#Code").val('');
}

