﻿var isDevelopment = true;
function createSeoPage() {
    $.get("/Admin/SeoPages?handler=CreateSeoPage", function (result) {
        $(".modal-dialog").css('max-width', '70%');
        $(".modal-header").css('background-color', '#008000b8');
        $(".headerModalColor").text("افزودن صفحات سئو");
        $("#myModal").modal();
        $("#myModalBody").html(result);
    });
}

function insertSeoPage() {
    var frm = $("#seoPageFrm").valid();
    if (frm == true) {

        $("#btnInsertSeoPage").children(".fa-spinner").removeClass('hide');
        $("#btnInsertSeoPage").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');

        var seoPage = {
            PageUrl: $("#PageUrl").val(),
            MetaTitle: $("#MetaTitle").val(),
            MetaKeyword: $("#MetaKeyword").val(),
            MetaDescription: $("#MetaDescription").val(),
            MetaOther: $("#MetaOther").val(),
        }
        $.ajax({
            method: 'POST',
            url: '/Admin/SeoPages?handler=CreateSeoPage',
            data: {
                seoPage: seoPage,
                __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
            },
            success: function (result) {
                $(".fa-spinner").addClass('hide');
                $(".fa-spinner").siblings('span').text('ثبت');

                if (result == "ok") {
                    LoadDatatable("#seoPage_tb", true);
                    $("#myModal").modal("hide");
                    ToastMessage("سئو صحفه با موفقیت ثبت گردید", "success", "کاربر گرامی");
                }
                else {
                    if (isDevelopment) {
                        alert(result.data);
                    } else {
                        ToastMessage("خطایی رخ داده است", "info", "خطا");
                    }
                }
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}

function viewAllSeoPages() {
    $("#seoPage_tb").DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.24/i18n/Persian.json"
        },
        "processing": true,
        "serverSide": true,
        "filter": true,
        "ajax": {
            "url": "/Admin/SeoPages?handler=ViewAllSeoPages",
            "type": "POST",
            "datatype": "json",
            "data": { __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val() },
        },
        "columns": [
            {
                data: "pageUrl",
                render: function (data) {
                    var page_url = data;
                    if (page_url.length > 50)
                        page_url = "..."+ page_url.substring(0, 49);

                    return '<a href="' + data + '" title="' + data + '" target="_blank" class="tooltips" data-toggle="tooltip" data-placement="top">' + page_url + '</a>';
                },
            },
            {
                mRender: function (data, type, row) {
                    return '<div class="btn-group" role="group">' +
                        '<span class="btn btn-outline-info" onclick="editSeoPage(' + row.seoPage_ID + ')"><i class="fa fa-edit fa-sm"></i></span>' +
                        '<span class="btn btn-outline-danger" onclick="deleteSeoPage(' + row.seoPage_ID + ')"><i class="fa fa-trash fa-sm"></i></span></div>';
                }
            },
        ]

    });
    loadingPage();
}

function editSeoPage(seoPageId) {
    $.get("/Admin/SeoPages?handler=EditSeoPage", { id: seoPageId }, function (result) {
        if (result.status == "error") {
            if (isDevelopment) {
                alert(result.data);
            } else {
                ToastMessage("خطایی رخ داده است", "info", "خطا");
            }
        }
        $(".modal-dialog").css('max-width', '70%');
        $(".modal-header").css('background-color', '#008074b8');
        $(".headerModalColor").text("ویرایش سئو صفحه");
        $("#myModal").modal();
        $("#myModalBody").html(result);
    });
}

function updateSeoPage() {
    var frm = $("#seoPageFrm").valid();
    if (frm == true) {

        $("#btnUpdateSeoPage").children(".fa-spinner").removeClass('hide');
        $("#btnUpdateSeoPage").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');

        var seoPage = {
            SeoPage_ID: $("#SeoPage_ID").val(),
            PageUrl: $("#PageUrl").val(),
            MetaTitle: $("#MetaTitle").val(),
            MetaKeyword: $("#MetaKeyword").val(),
            MetaDescription: $("#MetaDescription").val(),
            MetaOther: $("#MetaOther").val(),
        }
        $.ajax({
            method: 'POST',
            url: '/Admin/SeoPages?handler=EditSeoPage',
            data: {
                seoPage: seoPage,
                __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
            },
            success: function (result) {
                $(".fa-spinner").addClass('hide');
                $(".fa-spinner").siblings('span').text('ویرایش');

                if (result == "ok") {
                    LoadDatatable("#seoPage_tb", false);
                    $("#myModal").modal("hide");
                    ToastMessage("سئو صفحه با موفقیت ویرایش گردید", "success", "کاربر گرامی");
                }
                else {
                    if (isDevelopment) {
                        alert(result.data);
                    } else {
                        ToastMessage("خطایی رخ داده است", "info", "خطا");
                    }
                }
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}

function deleteSeoPage(seoPageId) {
    $.confirm({
        title: 'آیا برای حذف گروه مطمئن هستید؟', icon: 'fa fa-warning', theme: 'material', animation: 'scale', content: '',
        buttons: {
            confirm: {
                text: 'تایید',
                btnClass: 'btn-blue',
                action: function () {

                    $.get("/Admin/SeoPages?handler=DeleteSeoPage", { id: seoPageId }, function (result) {
                        if (result == "ok") {
                            LoadDatatable("#seoPage_tb", false);
                            ToastMessage("سئو صفحه با موفقیت حذف گردید", "success", "کاربر گرامی");
                        }
                        else if (result.status == "error") {
                            if (isDevelopment) {
                                alert(result.data);
                            } else {
                                ToastMessage("خطایی رخ داده است", "info", "خطا");
                            }
                        }
                    });
                }
            },
            cancel: {
                text: 'انصراف',
            }
        },
    });

}