﻿var isDevelopment = true;
function createProduct() {
    $.get("/Admin/Products?handler=CreateProduct", function (result) {
        if (result.status == "error") {
            if (isDevelopment) {
                alert(result.data);
            } else {
                ToastMessage("خطایی رخ داده است", "info", "خطا");
            }
        }
        $(".modal-dialog").css('max-width', '95%');
        $(".modal-header").css('background-color', '#008000b8');
        $(".headerModalColor").text("افزودن محصول");
        $("#myModal").modal({
            backdrop: 'static',
            keyboard: false
        });
        $('#myModal').modal({ backdrop: 'static', keyboard: false })
        $("#myModalBody").html(result);
    });
}

function createProductTranslation() {
    $.get("/Admin/Products?handler=CreateProducts", function (result) {
        $("#productGroup_trans").html(result);
        //switchTab();
    });
}

function createAnotherProductTranslation(Lang) {
    if ($("#card_" + Lang).length == 0) {
        $.get("/Admin/Products?handler=CreateAnotherProductTrans", { id: Lang }, function (result) {
            $("#accordion").append(result);
        });

        $("#myModal").animate({ scrollTop: $("#myModal").height() + 200 }, "slow");
    }
}

function switchTab(current, next) {
    $(current).removeClass("active");
    $(next).addClass("active");

    $("a[href='" + current + "']").removeClass('active');
    $("a[href='" + next + "']").addClass('active');
}

function insertProductTranslation() {
    var frm = $("#productTransFrm").valid();
    if (frm == true) {
        $("#btnInsertProductTrans").children(".fa-spinner").removeClass('hide');
        $("#btnInsertProductTrans").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');


        var model = [];
        /// برای ارسال لیستی از مدل به سمت سرور باید ابتدا آن را در آرایه ریخته سپس بصورت 
        /// جیسون استرینگ فای آن را میفرستیم و سمت سرور آن را بصورت فرم بادی میگیریم
        $("#accordion").children('.card').each(function (index) {

            var languageId = $(this).attr('data-id');

            var isactive = $("#IsActive_" + languageId + ":checked").length;
            var status = "";
            if (isactive === 1) { status = "true"; }
            else { status = "false"; }
            model.push({
                product_Id: $("#Product_ID").val(),
                title: $("#Title_" + languageId + "").val(),
                slug: $("#Slug_" + languageId + "").val(),
                tag: $("#Tag_" + languageId + "").val(),
                description: CKEDITOR.instances["Description_" + languageId].getData(),
                discount_Id: $("#DiscountId_" + languageId + "").val(),
                metaTitle: $("#MetaTitle_" + languageId + "").val(),
                metaKeword: $("#MetaKeword_" + languageId + "").val(),
                metaDescription: $("#MetaDescription_" + languageId + "").val(),
                metaOther: $("#MetaOther_" + languageId + "").val(),
                isActive: status,
                language_Id: languageId.trim(),
            });
        });

        $.ajax({
            type: "POST",
            contentType: "application/json",
            url: "/Admin/Products?handler=CreateProductTrans",
            //dataType: 'json',
            data: JSON.stringify(model),
            beforeSend: function (xhr) {
                loadingPageShow();
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            success: function (response) {
                $("#btnInsertProductTrans").children(".fa-spinner").addClass('hide');
                $("#btnInsertProductTrans").children(".fa-spinner").siblings('span').text('ادامه');

                if (response.status == "ok") {
                    success_ProductTranslation(response.productId, response.createDate);
                    viewAllProducts();
                    
                    //    console.log(response.productTranId); //TODO :FOR UPDATE PRODUCT
                }
                //else if (result == "exsist") {
                //    ToastMessage(" این گروه  برای این زبان قبلا ثبت شده است", "info", "کاربر گرامی");
                //}
                else {
                    if (isDevelopment) {
                        alert(response.data);
                    } else {
                        ToastMessage("خطایی رخ داده است", "warning", "خطا");
                    }
                }
            },
            error: function (xhr, resp, text) {
                console.log(xhr, resp, text);
                alert(xhr.statusText);
            }

        });
    }
    else {
        return false;
    }
}

function updateProductTranslation() {
    var frm = $("#productTransFrm").valid();
    if (frm == true) {
        $("#btnUpdateProductTrans").children(".fa-spinner").removeClass('hide');
        $("#btnUpdateProductTrans").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');


        var model = [];
        /// برای ارسال لیستی از مدل به سمت سرور باید ابتدا آن را در آرایه ریخته سپس بصورت 
        /// جیسون استرینگ فای آن را میفرستیم و سمت سرور آن را بصورت فرم بادی میگیریم
        $("#accordion").children('.card').each(function (index) {

            var languageId = $(this).attr('data-id');

            var isactive = $("#IsActive_" + languageId + ":checked").length;
            var status = "";
            if (isactive === 1) { status = "true"; }
            else { status = "false"; }
            model.push({
                product_Id: $("#Product_ID").val(),
                productTranslation_ID: $("#ProductTranslation_ID_" + languageId + "").val(),
                title: $("#Title_" + languageId + "").val(),
                slug: $("#Slug_" + languageId + "").val(),
                tag: $("#Tag_" + languageId + "").val(),
                description: CKEDITOR.instances["Description_" + languageId].getData(),
                discount_Id: $("#DiscountId_" + languageId + "").val(),
                metaTitle: $("#MetaTitle_" + languageId + "").val(),
                metaKeword: $("#MetaKeword_" + languageId + "").val(),
                metaDescription: $("#MetaDescription_" + languageId + "").val(),
                metaOther: $("#MetaOther_" + languageId + "").val(),
                isActive: status,
                language_Id: languageId.trim(),
            });
        });

        $.ajax({
            type: "POST",
            contentType: "application/json",
            url: "/Admin/Products?handler=UpdateProductTrans",
            //dataType: 'json',
            data: JSON.stringify(model),
            beforeSend: function (xhr) {
                loadingPageShow();
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            success: function (response) {
                $("#btnUpdateProductTrans").children(".fa-spinner").addClass('hide');
                $("#btnUpdateProductTrans").children(".fa-spinner").siblings('span').text('ادامه');

                if (response.status == "ok") {
                    createFileManager("#PictureProduct");
                    viewAllProducts();
                    switchTab('#general', '#productGroup');
                    $("#groupsOfProduct").removeClass('hide');
                }

                else {
                    if (isDevelopment) {
                        alert(response.data);
                    } else {
                        ToastMessage("خطایی رخ داده است", "warning", "خطا");
                    }
                }
            },
            error: function (xhr, resp, text) {
                console.log(xhr, resp, text);
                alert(xhr.statusText);
            }

        });
    }
    else {
        return false;
    }
}

function deleteProduct(productId, Id) {
    $.confirm({
        title: 'آیا برای حذف محصول مطمئن هستید؟', icon: 'fa fa-warning', theme: "material", animation: 'scale', content: '',
        buttons: {
            confirm: {
                text: 'تایید',
                btnClass: 'btn-blue',
                action: function () {
                    $.get("/Admin/Products?handler=DeleteProduct", { id: Id, productId: productId, status: "one" },
                        function (result) {
                            if (result == "ok") {
                                ToastMessage("محصول با موفقیت حذف گردید", "success", "کاربر گرامی");
                                viewAllProducts();
                            }
                            //else if (result == "exists") {
                            //    ToastMessage("برای این گروه محصول ثبت شده است", "warning", "کاربر گرامی");
                            //}
                            //else if (result == "existParent") {
                            //    ToastMessage("برای این گروه زیر گروه ثبت شده است", "warning", "کاربر گرامی");
                            //}
                            else {
                                if (isDevelopment) {
                                    alert(result.data);
                                } else {
                                    ToastMessage("خطایی رخ داده است", "warning", "خطا");
                                }
                            }
                        });
                }
            },
            deleteall: {
                text: 'حذف در تمام زبان ها',
                btnClass: 'btn-red',
                action: function () {

                    $.get("/Admin/Products?handler=DeleteProduct", { id: Id, productId: productId, status: "all" },
                        function (result) {
                            if (result == "ok") {
                                ToastMessage(" محصول در تمام زبان ها با موفقیت حذف گردید", "success", "کاربر گرامی");
                                viewAllProducts();
                            }
                            //else if (result == "exists") {
                            //    ToastMessage("برای گروه محصول ثبت شده است", "warning", "کاربر گرامی");
                            //}
                            //else if (result.status == "existParent") {
                            //    ToastMessage("برای گروه زیر گروه ثبت شده است", "warning", "کاربر گرامی");
                            //}
                            else {
                                if (isDevelopment) {
                                    alert(result.data);
                                } else {
                                    ToastMessage("خطایی رخ داده است", "warning", "خطا");
                                }
                            }
                        });
                }
            },
            cancel: {
                text: 'انصراف',

            }
        },
    });
}

function success_ProductTranslation(ProductId, CreateDate) {
    $("#Product_ID").val(ProductId);
    $("#CreateDate").val(CreateDate);
    switchTab('#general', '#productGroup');

    $(".group_treeView").removeClass('hide');
    $(".group_treeView_alert").addClass('hide');

    $("#productSettingFrm").removeClass('hide');
    $(".setting_alert").addClass('hide');

    $("#FeatureProduct").removeClass('hide');
    $(".feature_alert").addClass('hide');

    $("#productPicFrm").removeClass('hide');
    $(".picture_alert").addClass('hide');

    $("#SimilarProduct").removeClass('hide');
    $(".similar_alert").addClass('hide');
   
    getAllProductsGroupForTreeView();
   
    createSimilarProduct();
}

function updateProductSetting() {
    var frm = $("#productSettingFrm").valid();
    if (frm == true) {


        $("#btnUpdateProductSetting").children(".fa-spinner").removeClass('hide');
        $("#btnUpdateProductSetting").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');

        var isExist = $('#IsExist:checked').length;
        var status_isExist = "";
        if (isExist === 1) { status_isExist = "true"; }
        else { status_isExist = "false"; }

        var isPreview = $('#IsPreview:checked').length;
        var status_isPreview = "";
        if (isPreview === 1) { status_isPreview = "true"; }
        else { status_isPreview = "false"; }

        var bestSelling = $('#BestSelling:checked').length;
        var status_bestSelling = "";
        if (bestSelling === 1) { status_bestSelling = "true"; }
        else { status_bestSelling = "false"; }

        var product = {
            Product_ID: $("#Product_ID").val(),
            CreateDate: $("#CreateDate").val(),
            Gallery_Id: $("#Gallery_Id").val(),
            Code: $("#Code").val(),
            IsExist: status_isExist,
            IsPreview: status_isPreview,
            BestSelling: status_bestSelling
        }

        $.ajax({
            method: 'POST',
            url: '/Admin/Products?handler=UpdateProductSetting',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            data: product,
            success: function (result) {
                $("#btnUpdateProductSetting").children(".fa-spinner").addClass('hide');
                $("#btnUpdateProductSetting").children(".fa-spinner").siblings('span').text('ادامه');

                if (result == "ok") {
                    $("#Code").val('');
                    ToastMessage("محصول با موفقیت ثبت گردید", "success", "کاربر گرامی");
                }
                else {
                    if (isDevelopment) {
                        alert(result.data);
                    } else {
                        ToastMessage("خطایی رخ داده است", "info", "خطا");
                    }
                }
            },
            error: function (result) {
            }
        });
        //}
        //else {
        //    ToastMessage(" لطفا یک تصویر برای گروه انتخاب کنید", "info", "کاربر گرامی");
        //}
    }
    else {
        return false;
    }
}

function union_arrays(x,y) {   //Uniqe two array
    var obj = {};
    for (var i = x.length - 1; i >= 0; --i)
        obj[x[i]] = x[i];
    for (var i = y.length - 1; i >= 0; --i)
        obj[y[i]] = y[i];
    var res = []
    for (var k in obj) {
        if (obj.hasOwnProperty(k))  // <-- optional
            res.push(obj[k]);
    }
    return res;
}

function insertGroupsOfProduct() {
    debugger;
    var temp = union_arrays(groupsArrSelected, selectedProductGroupIds);
    if (selectedProductGroupIds.length >= 1) {
        $("#btnGroupOfProduct").children(".fa-spinner").removeClass('hide');
        $("#btnGroupOfProduct").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');
        $.ajax({
            method: 'POST',
            async: false,
            url: '/Admin/Products?handler=CreateGroupOfProduct',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            data: { SelectedProductGroupIds: temp, productId: $("#Product_ID").val() },
            success: function (result) {
                $("#btnGroupOfProduct").children(".fa-spinner").addClass('hide');
                $("#btnGroupOfProduct").children(".fa-spinner").siblings('span').text('ادامه');

                if (result == "ok") {
                    getAllProductGroupFeature('#FeatureGroup_ddl', temp);
                    viewAllGroupsOfProduct($("#Product_ID").val());
                    switchTab('#productGroup', '#feature');
                    
                }
                else {
                    if (isDevelopment) {
                        alert(result.data);
                    } else {
                        ToastMessage("خطایی رخ داده است", "info", "خطا");
                    }
                }
            },
            error: function (result) {
            }
        });
    }
    switchTab('#productGroup', '#feature');
    $("#FeatureProduct").removeClass('hide');
    getAllProductGroupFeature('#FeatureGroup_ddl', groupsArrSelected, "edit");   //groupsArrSelected is global variable
    getAllProductFeatures($("#Product_ID").val());
}

function viewAllGroupsOfProduct(ProductId) { /// For Edit
    $.get("/Admin/Products?handler=ViewAllGroupsOfProduct", { id: ProductId }, function (result) {
        $("#groupsOfProduct").html(result);
    });
}

$("a[href='#productGroup']").on('click', function () { /// For Edit
    $("#groupsOfProduct").removeClass('hide');
});

function viewAllProducts(productGroupId) {
    loadingPageShow();
    $("#product_tb").DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.24/i18n/Persian.json"
        },
        "processing": true,
        "serverSide": true,
        "filter": true,
        "destroy": true,   /// allow reinitialize datattable
        "ajax": {
            "url": "/Admin/Products?handler=ViewAllProducts",
            "type": "POST",
            "datatype": "json",
            "data":
            {
                productGroupId: productGroupId,
                __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
            },
        },
        "columns": [
            { "data": "title", "name": "title", "autoWidth": true },
            {
                data: "product.createDate",
                render: function (data) {
                    return new persianDate(new Date(data)).format("L");

                }, name: "product.createDate"
            },
            {
                data: "isActive",
                render: function (data) {
                    if (data) {
                        return '<span class="text-success">فعال</span >';
                    }
                    else {
                        return '<span class="text-danger">غیر فعال</span >';
                    }
                },
            },
            {
                data: "product.isExist",
                render: function (data) {
                    if (data) {
                        return '<span class="text-success">موجود</span >';
                    }
                    else {
                        return '<span class="text-danger">ناموجود</span >';
                    }
                },
            },
            {
                data: "product.isPreview",
                render: function (data) {
                    if (data) {
                        return '<span class="badge badge-warning">پیش نمایش</span >';
                    }
                    else {
                        return '<span class="badge badge-success">نمایش</span >';
                    }
                },
            },
            {
                mRender: function (data, type, row) {
                    return '<div class="btn-group" role="group">' +
                        '<span class="btn btn-outline-info" onclick="editProduct(' + row.product_Id + ')"><i class="fa fa-edit fa-sm"></i></span>' +
                        '<span class="btn btn-outline-danger" onclick="deleteProduct(' + row.product_Id + ',' + row.productTranslation_ID + ')"><i class="fa fa-trash fa-sm"></i></span></div>';
                }
            },
        ]

    });
    loadingPageHide();
}

function editProduct(productId) {
    $.get("/Admin/Products?handler=EditProduct", { id: productId }, function (result) {
        if (result.status == "error") {
            if (isDevelopment) {
                alert(result.data);
            } else {
                ToastMessage("خطایی رخ داده است", "info", "خطا");
            }
        }
        $(".modal-dialog").css('max-width', '95%');
        $(".modal-header").css('background-color', '#008074b8');
        $(".headerModalColor").text("ویرایش محصول");
        $("#myModal").modal({
            backdrop: 'static',
            keyboard: false
        });
        $('#myModal').modal({ backdrop: 'static', keyboard: false })
        $("#myModalBody").html(result);
    });
}

function editAnotherProductTranslation(Lang, productId) {
    if ($("#card_" + Lang).length == 0) {
        $.get("/Admin/Products?handler=EditAnotherProductTrans", { lang: Lang, productId: productId }, function (result) {
            $("#accordion").append(result);
        });

        $("#myModal").animate({ scrollTop: $("#myModal").height() + 200 }, "slow");
    }
}
////ویژگی های محصول
function getAllProductGroupFeature(selector, Ids, status) {
    if (status == "edit") {
        var temp = [];
        temp = Ids.splice(Ids.length - 1, 1);
    }
    jQuery.ajaxSettings.traditional = true;
    $.get("/Admin/Products?handler=ProductGroupFeaturesForProduct", { SelectedProductGroupIds: Ids }, function (result) {
        debugger;
        var options = "<option value=''>ویژگی های گروه محصول ...</option>";
        for (var i = 0; i < result.length; i++) {
            options += "<option value='" + result[i].feature_Id + "'>" + result[i].title + "</option>";
        }
        $(selector).html(options);
    });

}

function getAllFeatureReplies(FeatureId) {
    if ($("#accordion_" + FeatureId).length == 0) {
        $.get("/Admin/Products?handler=FeatureReplyOfFeature", { featureId: FeatureId }, function (res) {
            $("#FeatureProduct2").append(res);
        });
    }
}

function insertProductFeature(featureReplyTransId) {
    var featureReplies = [];
    var isVariable = $('#IsVariable_' + featureReplyTransId + ':checked').length;
    var status = "";
    if (isVariable === 1) { status = "true"; }
    else { status = "false"; }
    $(".featureReply_" + featureReplyTransId + ":checked").each(function () {
        featureReplies.push($(this).val());
    });
    if (featureReplies.length > 0) {
        var productFeatureVM = {
            ProductId: $("#Product_ID").val(),
            FeatureId: $("#accordion_" + featureReplyTransId).attr('data-id'),
            FeatureReplyIds: featureReplies,
            IsVariable: status,
        }
        $.ajax({
            method: 'POST',
            url: '/Admin/Products?handler=CreateProductFeatures',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            data: productFeatureVM,
            success: function (result) {
                if (result == "ok") {
                    ToastMessage("ویژگی محصول با موفقیت ثبت گردید", "success", "کاربر گرامی");
                    getAllProductFeatures($("#Product_ID").val());
                }
                else if (result == "exists") {
                    getAllProductFeatures($("#Product_ID").val());
                    ToastMessage("ویژگی انتخاب شده برای این محصول ثبت شده است", "info", "کاربر گرامی");
                }
                else {
                    if (isDevelopment) {
                        alert(result.data);
                    } else {
                        ToastMessage("خطایی رخ داده است", "info", "خطا");
                    }
                }
            },
            error: function (result) {
            }
        });
    }
    else {
        ToastMessage("برای ثبت ویژگی محصول یک آیتم را انتخاب نمایید", "info", "کاربر گرامی");
    }
}

function getAllProductFeatures(ProductId) {
    $.get("/Admin/Products?handler=ViewAllProductFeatures", { productId: ProductId }, function (res) {
        $("#FeatureProductList").html(res);
    });
}

function deleteProductFeatures() {
    $.confirm({
        title: 'آیا برای حذف ویژگی محصول مطمئن هستید؟', icon: 'fa fa-warning', theme: "material", animation: 'scale', content: '',
        buttons: {
            confirm: {
                text: 'تایید',
                btnClass: 'btn-blue',
                action: function () {
                    $(".btnDeleteFeature").children(".fa-spinner").removeClass('hide');
                    $(".btnDeleteFeature").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');

                    var checkboxValues = "";
                    $('.forRemove:checked').each(function () {  // گرفتن ولیو های چکس باکس هایی که تیک خورده باشند
                        checkboxValues += $(this).val() + "&";
                    });
                    var featureReplies = [];
                    featureReplies = checkboxValues.split("&");

                    jQuery.ajaxSettings.traditional = true; // جهت ارسال آرایه بصورت گت این پراپرتی باید تنظیم شود

                    $.ajax({
                        method: 'GET',
                        url: '/Admin/Products?handler=DeleteProductFeatures',
                        data: {
                            featureReplies: featureReplies,
                            productId: $("#Product_ID").val(),
                            __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
                        },
                        success: function (result) {
                            $(".btnDeleteFeature").children(".fa-spinner").addClass('hide');
                            $(".btnDeleteFeature").children(".fa-spinner").siblings('span').text("حذف ویژگی");

                            if (result == "ok") {
                                getAllProductFeatures($("#Product_ID").val());
                                ToastMessage("ویژگی با موفقیت از لیست ویژگی های محصول حذف گردید", "success", "کاربر گرامی");
                            }
                            else {
                                if (isDevelopment) {
                                    alert(result.data);
                                } else {
                                    ToastMessage("خطایی رخ داده است", "info", "خطا");
                                }
                            }
                        },
                        error: function (result) {
                        }
                    });
                }
            },

            cancel: {
                text: 'انصراف',

            }
        },
    });


}

$("a[href='#feature']").on('click', function () {  /// For Edit
    $("#FeatureProduct").removeClass('hide');
    getAllProductGroupFeature('#FeatureGroup_ddl', groupsArrSelected, "edit");   //groupsArrSelected is global variable
    getAllProductFeatures($("#Product_ID").val());
});

/// تصاویر محصول
function ProductPicturesUpload() {
    var imageArray = [];
    var frm = $("#productPicFrm").valid();
    if (frm === true) {
        $("#btnProductPic").children(".fa-spinner").removeClass('hide');
        $("#btnProductPic").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');

        var galleryId = $("#Gallery_Id").val();
        $("#imgPreviews").children('.myimage').children('img').each(function () {
            imageArray.push($(this).attr('id'));
        });


        $.ajax({
            data: {
                productId: $("#Product_ID").val(),
                galleryId: galleryId,
                images: imageArray
            },
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            url: '/Admin/Products?handler=CreateProductGallery',
            success: function (response) {
                $("#btnProductPic").children(".fa-spinner").addClass('hide');
                $("#btnProductPic").children(".fa-spinner").siblings('span').text('آپلود');
                imageArray = [];

                if (response.status == "ok") {
                    ToastMessage("تصاویر محصول با موفقیت ثبت گردید", "success", "آپلود تصاویر");
                    $("#btnProductPic").addClass('disabled');
                    $("#Gallery_Id").val(response.galleryId);
                    $(".galleryPicProduct_div").addClass('hide');
                    //ViewAllGalleryPic(galleryId);

                }

            },

        });
    }
    else {
        return false;
    }
}

function viewAllProductPictures() { /// For Edit
    $.get("/Admin/Products?handler=ViewAllProductPictures", { productId: $("#Product_ID").val() }, function (res) {
        if (res != "invalid") {
            $("#imgPreviews").html(res);
            $(".galleryPicProduct_div").addClass('hide');
        }
        else {
            $(".galleryPicProduct_div").removeClass('hide');
            $(".galleryPicProduct_div").html('برای این محصول تصویری ثبت نشده است !');
        }
    });
}

$("a[href='#picture']").on('click', function () {  /// For Edit
    createFileManager("#PictureProduct");
});

/// محصولات مشابه
function changeSimilarOperation(value) {
    getAllProductsGroupForTreeView($("#global_language_id").val());
    if (value == "1") {
        $(".similar_treeView").removeClass('hide');
        $(".suggest_treeView").addClass('hide');
        viewAllProductsForSimilarProduct();
        viewAllSimilarProducts($("#Product_ID").val());
        $(".deleteSimilar_div").removeClass('hide');
        $(".deleteSuggest_div").addClass('hide');
    }
    else if (value == "2") {
        $(".similar_treeView").addClass('hide');
        $(".suggest_treeView").removeClass('hide');
        viewAllProductsForSuggestProduct();
        viewAllSuggestProducts($("#Product_ID").val());
        $(".deleteSimilar_div").addClass('hide');
        $(".deleteSuggest_div").removeClass('hide');
    }
}

function createSimilarProduct() {
    $.get("/Admin/Products?handler=CreateSimilarProduct", function (res) {
        $("#SimilarProduct").html(res);
        getAllProductsGroupForTreeView($("#global_language_id").val());
        viewAllProductsForSimilarProduct();
    });
}

function viewAllProductsForSimilarProduct(productGroupId) {
    loadingPageShow();
    $("#productForSimilar_tb").removeClass('hide');
    $("#productForSimilar_tb").DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.24/i18n/Persian.json"
        },
        "processing": true,
        "serverSide": true,
        "filter": true,
        "destroy": true,   /// allow reinitialize datattable
        "ajax": {
            "url": "/Admin/Products?handler=ViewAllProductsForSimilarProduct",
            "type": "POST",
            "datatype": "json",
            "data":
            {
                productGroupId: productGroupId,
                productId: $("#Product_ID").val(),
                __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
            },
        },
        "columns": [
            { "data": "title", "name": "title", "autoWidth": true },
            //{
            //    data: "product.createDate",
            //    render: function (data) {
            //        return new persianDate(new Date(data)).format("L");

            //    }, name: "product.createDate"
            //},
            {
                data: "isActive",
                render: function (data) {
                    if (data) {
                        return '<span class="text-success">فعال</span >';
                    }
                    else {
                        return '<span class="text-danger">غیر فعال</span >';
                    }
                },
            },
            //{
            //    data: "product.isExist",
            //    render: function (data) {
            //        if (data) {
            //            return '<span class="text-success">موجود</span >';
            //        }
            //        else {
            //            return '<span class="text-danger">ناموجود</span >';
            //        }
            //    },
            //},
            {
                data: "product.isPreview",
                render: function (data) {
                    if (data) {
                        return '<span class="badge badge-warning">پیش نمایش</span >';
                    }
                    else {
                        return '<span class="badge badge-success">نمایش</span >';
                    }
                },
            },
            {
                mRender: function (data, type, row) {
                    return '<div class="btn-group" role="group">' +
                        '<span class="btn btn-outline-success tooltips btnInsertSimilarProduct_' + row.product_Id + '" data-placement="top" title="افزودن این محصول به عنوان محصول مشابه" onclick="insertSimilarProduct(' + row.product_Id + ')">' +
                        '<i class="fa fa-spinner fa-spin hide"></i><i class="fa fa-plus fa-sm"></i></span > ' +
                        '</div>';
                }
            },
        ]

    });
    loadingPageHide();
}

function insertSimilarProduct(SimilarProductId) {
    if ($("#Product_ID") != 0) {
        $(".btnInsertSimilarProduct_" + SimilarProductId).children(".fa-spinner").removeClass('hide');
        $(".btnInsertSimilarProduct_" + SimilarProductId).children(".fa-plus").addClass('hide');
        $(".btnInsertSimilarProduct_" + SimilarProductId).children(".fa-spinner").siblings('span').text('منتظر بمانید ...');

        $.ajax({
            data: {
                productId: $("#Product_ID").val(),
                similarProductId: SimilarProductId
            },
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            url: '/Admin/Products?handler=CreateSimilarProduct',
            success: function (response) {
                $(".btnInsertSimilarProduct_" + SimilarProductId).children(".fa-spinner").addClass('hide');
                $(".btnInsertSimilarProduct_" + SimilarProductId).children(".fa-plus").removeClass('hide');
                $(".btnInsertSimilarProduct_" + SimilarProductId).children(".fa-spinner").siblings('span').text('آپلود');

                if (response == "ok") {
                    ToastMessage("محصول مشابه با موفقیت ثبت گردید", "success", "کاربر گرامی");
                    viewAllSimilarProducts($("#Product_ID").val());
                }
                else if (response == "exists") {
                    ToastMessage("این آیتم در لیست محصولات مشابه محصول مورد نظر وجود دارد", "info", "کاربر گرامی");
                }
                else if (response == "invalid") {
                    ToastMessage("عملیات ناموفق", "error", "خطا");
                }
            },

        });
    }
    else {
        ToastMessage("محصولی ثبت نشده است !", "warning", "کاربر گرامی");
    }
}

function viewAllSimilarProducts(ProductId) {
    $.get("/Admin/Products?handler=ViewAllSimilarProducts", { productId: ProductId }, function (result) {
        $("#resultSimilarViewAll").html(result);
    });
}

function deleteSimilarProducts() {
    $.confirm({
        title: 'آیا برای حذف محصول مشابه مطمئن هستید؟', icon: 'fa fa-warning', theme: "material", animation: 'scale', content: '',
        buttons: {
            confirm: {
                text: 'تایید',
                btnClass: 'btn-blue',
                action: function () {
                    $(".btnDeleteSimilarProduct").children(".fa-spinner").removeClass('hide');
                    $(".btnDeleteSimilarProduct").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');

                    var checkboxValues = "";
                    $('.forRemove:checked').each(function () {  // گرفتن ولیو های چکس باکس هایی که تیک خورده باشند
                        checkboxValues += $(this).val() + "&";
                    });
                    var similarProducts = [];
                    similarProducts = checkboxValues.split("&");

                    jQuery.ajaxSettings.traditional = true; // جهت ارسال آرایه بصورت گت این پراپرتی باید تنظیم شود

                    $.ajax({
                        method: 'GET',
                        url: '/Admin/Products?handler=DeleteSimilarProducts',
                        data: {
                            similarProducts: similarProducts,
                            productId: $("#Product_ID").val(),
                            __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
                        },
                        success: function (result) {
                            $(".btnDeleteSimilarProduct").children(".fa-spinner").addClass('hide');
                            $(".btnDeleteSimilarProduct").children(".fa-spinner").siblings('span').text("حذف محصول مشابه");

                            if (result == "ok") {
                                viewAllSimilarProducts($("#Product_ID").val());
                                ToastMessage("محصول مشابه با موفقیت حذف گردید", "success", "کاربر گرامی");
                            }
                            else {
                                if (isDevelopment) {
                                    alert(result.data);
                                } else {
                                    ToastMessage("خطایی رخ داده است", "info", "خطا");
                                }
                            }
                        },
                        error: function (result) {
                        }
                    });
                }
            },

            cancel: {
                text: 'انصراف',

            }
        },
    });


}

$("a[href='#similar']").on('click', function () {  /// For Edit
    createSimilarProduct();
    viewAllSimilarProducts($("#Product_ID").val());
    viewAllProductsForSimilarProduct();
});
function gotoSimilarPart() {
    switchTab('#picture', '#similar');
    createSimilarProduct();
    viewAllSimilarProducts($("#Product_ID").val());
    viewAllProductsForSimilarProduct();
}
/// محصولات پیشنهادی
function viewAllProductsForSuggestProduct(productGroupId) {
    loadingPageShow();
    $("#productForSimilar_tb").removeClass('hide');
    $("#productForSimilar_tb").DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.24/i18n/Persian.json"
        },
        "processing": true,
        "serverSide": true,
        "filter": true,
        "destroy": true,   /// allow reinitialize datattable
        "ajax": {
            "url": "/Admin/Products?handler=ViewAllProductsForSimilarProduct",
            "type": "POST",
            "datatype": "json",
            "data":
            {
                productGroupId: productGroupId,
                productId: $("#Product_ID").val(),
                __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
            },
        },
        "columns": [
            { "data": "title", "name": "title", "autoWidth": true },
            //{
            //    data: "product.createDate",
            //    render: function (data) {
            //        return new persianDate(new Date(data)).format("L");

            //    }, name: "product.createDate"
            //},
            {
                data: "isActive",
                render: function (data) {
                    if (data) {
                        return '<span class="text-success">فعال</span >';
                    }
                    else {
                        return '<span class="text-danger">غیر فعال</span >';
                    }
                },
            },
            //{
            //    data: "product.isExist",
            //    render: function (data) {
            //        if (data) {
            //            return '<span class="text-success">موجود</span >';
            //        }
            //        else {
            //            return '<span class="text-danger">ناموجود</span >';
            //        }
            //    },
            //},
            {
                data: "product.isPreview",
                render: function (data) {
                    if (data) {
                        return '<span class="badge badge-warning">پیش نمایش</span >';
                    }
                    else {
                        return '<span class="badge badge-success">نمایش</span >';
                    }
                },
            },
            {
                mRender: function (data, type, row) {
                    return '<div class="btn-group" role="group">' +
                        '<span class="btn btn-outline-success tooltips btnInsertSuggestProduct_' + row.product_Id + '" data-placement="top" title="افزودن این محصول به عنوان محصول پیشنهادی" onclick="insertSuggestProduct(' + row.product_Id + ')">' +
                        '<i class="fa fa-spinner fa-spin hide"></i><i class="fa fa-plus fa-sm"></i></span > ' +
                        '</div>';
                }
            },
        ]

    });
    loadingPageHide();
}

function insertSuggestProduct(SuggestProductId) {
    if ($("#Product_ID") != 0) {
        $(".btnInsertSuggestProduct_" + SuggestProductId).children(".fa-spinner").removeClass('hide');
        $(".btnInsertSuggestProduct_" + SuggestProductId).children(".fa-plus").addClass('hide');
        $(".btnInsertSuggestProduct_" + SuggestProductId).children(".fa-spinner").siblings('span').text('منتظر بمانید ...');

        $.ajax({
            data: {
                productId: $("#Product_ID").val(),
                suggestProductId: SuggestProductId
            },
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            url: '/Admin/Products?handler=CreateSuggestProduct',
            success: function (response) {
                $(".btnInsertSuggestProduct_" + SuggestProductId).children(".fa-spinner").addClass('hide');
                $(".btnInsertSuggestProduct_" + SuggestProductId).children(".fa-plus").removeClass('hide');
                $(".btnInsertSuggestProduct_" + SuggestProductId).children(".fa-spinner").siblings('span').text('آپلود');

                if (response == "ok") {
                    ToastMessage("محصول پیشنهادی با موفقیت ثبت گردید", "success", "کاربر گرامی");
                    viewAllSuggestProducts($("#Product_ID").val());
                }
                else if (response == "exists") {
                    ToastMessage("این آیتم در لیست محصولات پیشنهادی محصول مورد نظر وجود دارد", "info", "کاربر گرامی");
                }
                else if (response == "invalid") {
                    ToastMessage("عملیات ناموفق", "error", "خطا");
                }
            },

        });
    }
    else {
        ToastMessage("محصولی ثبت نشده است !", "warning", "کاربر گرامی");
    }
}

function viewAllSuggestProducts(ProductId) {
    $.get("/Admin/Products?handler=ViewAllSuggestProducts", { productId: ProductId }, function (result) {
        $("#resultSimilarViewAll").html(result);
    });
}

function deleteSuggestProducts() {
    $.confirm({
        title: 'آیا برای حذف محصول پیشنهادی مطمئن هستید؟', icon: 'fa fa-warning', theme: "material", animation: 'scale', content: '',
        buttons: {
            confirm: {
                text: 'تایید',
                btnClass: 'btn-blue',
                action: function () {
                    $(".btnDeleteSuggestProduct").children(".fa-spinner").removeClass('hide');
                    $(".btnDeleteSuggestProduct").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');

                    var checkboxValues = "";
                    $('.forRemoveSuggest:checked').each(function () {  // گرفتن ولیو های چکس باکس هایی که تیک خورده باشند
                        checkboxValues += $(this).val() + "&";
                    });
                    var suggestProducts = [];
                    suggestProducts = checkboxValues.split("&");

                    jQuery.ajaxSettings.traditional = true; // جهت ارسال آرایه بصورت گت این پراپرتی باید تنظیم شود

                    $.ajax({
                        method: 'GET',
                        url: '/Admin/Products?handler=DeleteSuggestProducts',
                        data: {
                            suggestProducts: suggestProducts,
                            productId: $("#Product_ID").val(),
                            __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
                        },
                        success: function (result) {
                            $(".btnDeleteSuggestProduct").children(".fa-spinner").addClass('hide');
                            $(".btnDeleteSuggestProduct").children(".fa-spinner").siblings('span').text("حذف محصول مشابه");

                            if (result == "ok") {
                                viewAllSuggestProducts($("#Product_ID").val());
                                ToastMessage("محصول پیشنهادی با موفقیت حذف گردید", "success", "کاربر گرامی");
                            }
                            else {
                                if (isDevelopment) {
                                    alert(result.data);
                                } else {
                                    ToastMessage("خطایی رخ داده است", "info", "خطا");
                                }
                            }
                        },
                        error: function (result) {
                        }
                    });
                }
            },

            cancel: {
                text: 'انصراف',

            }
        },
    });


}