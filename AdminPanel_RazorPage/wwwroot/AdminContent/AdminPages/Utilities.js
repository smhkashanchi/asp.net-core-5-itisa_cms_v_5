﻿var groupsArrSelected = []; //Use In _ViewAllGroupsOfProduct For Feature (Edit)
var selectedGroupIds = [];
var selectedProductGroupIds = [];
var selectedGroupIdsEdit = [];
var selectedProductGroupIdsEdit = [];
///Image Preview
function readURL(input, element) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $(element).attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}
///Toast Message
var ToastMessage = function (message, type, title) {
    var msgs = [message];
    var $toast = toastr[type](msgs, title);
    $toastlast = $toast;
    //return msgs;
};
toastr.options = {
    closeButton: 'true',
    progressBar: 'true',
    positionClass: 'toast-top-left',
};

function loadingPage() {
    EasyLoading.show({
        type: EasyLoading.TYPE['LINE_SCALE'], text: 'در حال بارگذاری ...',
        timeout: true ? 1000 : null,
        callback: function (event, data) {
            $("body").append($("<div>" + event + "</div>"));
        }
    });
}


function loadingPageShow() {
    EasyLoading.show({
        type: EasyLoading.TYPE['LINE_SCALE'], text: 'در حال بارگذاری ...',
        timeout: true ? 1000 : null,
        callback: function (event, data) {
            $("body").append($("<div>" + event + "</div>"));
        }
    });
}

function loadingPageHide() {
    EasyLoading.hide();
}
//Change Culture
var result = "";
function changeLanguage(culture) {
    var controller = window.location.href;
    controller = controller.toLowerCase();
    controller = controller.substring(controller.indexOf("/admin/"), controller.length);

    $.get("/Admin/Languages?handler=ChangeLanguage", { culture: culture},
        function (res) {
            if (controller.includes("contents")) { //اگر در صفحه مطالب باشیم
                getAllContentsGroupForTreeView(res.trim());
            }
            else if (controller.includes("filesmanage")) { //اگر در صفحه فایل ها باشیم
                getAllFilesManageGroupForTreeView(res.trim());
            }
            else if (controller.includes("products")) {
                getAllProductsGroupForTreeView(res.trim());
                viewAllProducts();
            }
            else if (controller.includes("slideshows")) {
                getSlideShowGroupByLang_Global(res.trim(), "#slideshowGroup_ddl");
                dtRefresh();
            }
            else if (controller.includes("coworkers")) {
                getCoWorkerGroupByLang_Global(res.trim(), "#coWorkerGroup_ddl");
                dtRefresh();
            }
            else if (controller.includes("galleries")) {
                getGalleryGroupByLang_Global(res.trim(),"#galleryGroup_ddl", "", "");
                dtRefresh();
            }
            else if (controller.includes("features")) {
                getFeatureGroupByLang_Global("#featureGroup_ddl");
                getFeaturesByLang_Global("", "#feature_ddl", "", "");
                dtRefresh();
            }
            //else if (controller.includes("regions")) {
            //    dtRefresh();
            //}
            else {
                
                dtRefresh();
            }


            setDirectionWithLanguage_Global(res.trim());//تنظیم دایرکشن برای تکست باکس ها
        });
}

/// Datatable Reload[button refresh]
function dtRefresh() {
    $(".culture").children('div').each(function () {
        var tbName = $(this).attr('id');
        tbName = tbName.replace("_wrapper", "").trim();

        var table = $('#' + tbName + '').DataTable();
        table.ajax.reload();
    });
   
}
$("#btnDtRefresh").click(function () { 
    dtRefresh();
});
///Load Datatable
function LoadDatatable(tbId,resetPaging) {
    var table = $(tbId).DataTable();
    table.ajax.reload(null, resetPaging);
}
function setZeroValue(selector) {
    $(selector).val(0);
}
///Print
function printHtml(element) {
    var divToPrint = document.getElementById(element);
    var htmlToPrint = '' +
        '<style type="text/css">' +
        'table th, table td {' +
        'border:1px solid #000;' +
        'padding:0.5em;' +
        'direction:rtl;' +
        '}' +
        '</style>';
    htmlToPrint += divToPrint.outerHTML;
    newWin = window.open("");
    newWin.document.write(htmlToPrint);
    newWin.print();
    newWin.close();
}

/// Export Html To Excel
var tableToExcel = (function () {
    var uri = 'data:application/vnd.ms-excel;base64,'
        , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>'
        , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
        , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
    return function (table, name) {
        if (!table.nodeType) table = document.getElementById(table)
        var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }
        window.location.href = uri + base64(format(template, ctx))
    }
})()

///Set Slug
function setSlug(text, selector) {
    text = text.replace(/ /g, "-");
    $(selector).val(text);
}

function spaceEmpty(element) {
    var text = $(element).val();
    text = text.replace(/ /g, "");
    $(element).val(text);
}

function setDirectionWithLanguage_Global(language) {
    if (language == "fa-IR") {
        $("input").attr('dir', 'rtl');
    }
    else if (language == "en-US") {
        $("input").attr('dir', 'ltr');
    }
}

function setTooltip(tableElement) {
    $(tableElement).on("mouseover", ".tooltips", function () {
        $(this).tooltip();
    });
}

function createFileManager(element_view) {
    $.get("/Admin/Products?handler=FileManagerPartialView", function (res) {
        $(element_view).html(res);
    });
}