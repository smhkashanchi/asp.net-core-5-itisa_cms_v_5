﻿var isDevelopment = true;
function createContent() {
    $.get("/Admin/Contents?handler=CreateContent", function (result) {
        if (result.status == "error") {
            if (isDevelopment) {
                alert(result.data);
            } else {
                ToastMessage("خطایی رخ داده است", "info", "خطا");
            }
        }
        $(".modal-dialog").css('max-width', '85%');
        $(".modal-header").css('background-color', '#008000b8');
        $(".headerModalColor").text("افزودن محتوا");
        $("#myModal").modal();
        $("#myModalBody").html(result);
    });
}

function insertContent() {
    var frm = $("#contentFrm").valid();
    if (frm == true) {
        if ($("#Type").val() != "-1") {


            var content = new FormData();
            var files = $("#Image").get(0).files;
            if (files.length > 0) {
                var isactive = $('#IsActive:checked').length;
                var status = "";
                if (isactive === 1) { status = "true"; }
                else { status = "false"; }

                content.append("Gallery_Id", $("#Gallery_Id").val()),
                    content.append("IsActive", status),
                    content.append("imgContent", files[0])


                $("#btnInsertContent").children(".fa-spinner").removeClass('hide');
                $("#btnInsertContent").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');


                $.ajax({
                    method: 'POST',
                    url: '/Admin/Contents?handler=CreateContent',
                    contentType: false,
                    processData: false,
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader("XSRF-TOKEN",
                            $('input:hidden[name="__RequestVerificationToken"]').val());
                    },
                    data: content,
                    success: function (result) {
                        $("#btnInsertContent").children(".fa-spinner").addClass('hide');
                        $("#btnInsertContent").children(".fa-spinner").siblings('span').text('ویرایش');

                        if (result.status == "ok") {

                            $(".insert-div").addClass('hide');
                            $(".update-div").removeClass('hide');

                            $("#Content_ID").val(result.contentId);

                            createContentTranslation(result.contentId);
                        }
                        else {
                            if (isDevelopment) {
                                alert(result.data);
                            } else {
                                ToastMessage("خطایی رخ داده است", "info", "خطا");
                            }
                        }
                    },
                    error: function (result) {
                    }
                });
            }
            else {
                ToastMessage("لطفا یک تصویر برای محتوا انتخاب نمایید", "warning", "کاربر گرامی");
            }
        }
        else {
            ToastMessage(" لطفا یک نوع راه ارتباطی انتخاب کنید", "info", "کاربر گرامی");
        }
    }
    else {
        return false;
    }
}

function createContentTranslation(contentId) {
    $.get("/Admin/Contents?handler=CreateContentTrans", { id: contentId }, function (result) {
        $("#content_trans").html(result);
        $("#tab_1").removeClass("active");
        $("#content_trans").addClass("active");

        $("a[href='#tab_1']").removeClass('active');
        $("a[href='#content_trans']").addClass('active');
    });
}

function insertContentTranslation() {
    var frm = $("#contentTransFrm").valid();
    if (frm == true) {
        if ($("#language_id").val() != "0") {

            $("#btnInsertContentFinal").children(".fa-spinner").removeClass('hide');
            $("#btnInsertContentFinal").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');
            var contentTranslation = {
                Content_Id: $("#Content_Id").val(),
                Title: $("#Title").val(),
                Slug: $("#Slug").val(),
                Summery: $("#Summery").val(),
                Author: $("#Author").val(),
                ContentText: CKEDITOR.instances['ContentText'].getData(),
                MetaTitle: $("#MetaTitle").val(),
                MetaKeword: $("#MetaKeword").val(),
                MetaDescription: $("#MetaDescription").val(),
                MetaOther: $("#MetaOther").val(),
                Tag: $("#Tag").val(),
                Language_Id: $("#language_id").val(),
            }
            $.ajax({
                method: 'POST',
                url: '/Admin/Contents?handler=CreateContentTrans',
                data: {
                    contentTranslation: contentTranslation,
                    selectedGroupId: selectedGroupIds,
                    __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
                },
                success: function (result) {
                    $(".fa-spinner").addClass('hide');
                    $(".fa-spinner").siblings('span').text('ثبت');

                    $(".insert-div").removeClass('hide');
                    $(".update-div").addClass('hide');

                    if (result.status == "ok") {

                        ToastMessage("محتوا با موفقیت ثبت گردید", "success", "کاربر گرامی");

                        LoadDatatable("#content_tb", true);
                        viewAllContents(result.contentTrans_id);
                        resetForm();
                        viewAllContents();
                    }
                    else if (result == "exists") {
                        ToastMessage(" این اطلاعات برای این زبان قبلا ثبت شده است", "info", "کاربر گرامی");
                    }
                    else {
                        if (isDevelopment) {
                            alert(result.data);
                        } else {
                            ToastMessage("خطایی رخ داده است", "warning", "خطا");
                        }
                    }
                },
                error: function (result) {
                }
            });
        }
        else {
            ToastMessage(" لطفا یک زبان را انتخاب کنید", "info", "کاربر گرامی");
        }
    }
    else {
        return false;
    }
}

function editContent(contentId, translationId) {
    $.get("/Admin/Contents?handler=EditContent", { id: contentId }, function (result) {
        if (result.status == "error") {
            if (isDevelopment) {
                alert(result.data);
            } else {
                ToastMessage("خطایی رخ داده است", "info", "خطا");
            }
        }
        else {
            $(".modal-dialog").css('max-width', '85%');
            $(".modal-header").css('background-color', '#008074b8');
            $(".headerModalColor").text("ویرایش محتوا");
            $("#myModal").modal();
            $("#myModalBody").html(result);
            editContentTranslation(translationId);
        }
    });
}

function editContentTranslation(translationId) {
    $.get("/Admin/Contents?handler=EditContentTrans", { id: translationId }, function (res) {
      
        $("#content_trans").html(res);
        //getContentInfoWithLanguage('', translationId);
    });
}

function getContentInfoWithLanguage(langId, contentId) {

    $.get("/Admin/Contents?handler=ContentInfo", { id: langId, contentId: contentId }, function (res) {
        if (res.status == "ok") {
            var badgeHtml = "";
            var obj = res.data.contentMiddleGroups;
            obj.forEach(function (item) {
                badgeHtml += '<p class="badge badge-primary ml-2" >' + item.contentGroupTranslation.title + '</p>';
            });
          
            $(".contentGroups_badge").html(badgeHtml);

            $("#Title").val(res.data.title);
          
            $("#Slug").val(res.data.slug);
            $("#Slug").attr('disabled', true);
            $("#isChange").prop('checked', false);
            $("#Summery").val(res.data.summery);
            $("#Author").val(res.data.author);
            CKEDITOR.instances['ContentText'].setData(res.data.contentText);
            $("#MetaTitle").val(res.data.metaTitle);
            $("#MetaKeword").val(res.data.metaKeword);
            $("#MetaDescription").val(res.data.metaDescription);
            $("#MetaOther").val(res.data.metaOther);
            $("#Tag").tagsinput('removeAll');
            $("#Tag").tagsinput('add',res.data.tag);
            $("#Title").attr('data-id', res.data.contentTranslation_ID);
        }
        else {
            //ToastMessage(" عنوان رنگی یافت نشد!", "info", "کاربر گرامی");
        }
    });
}

function updateContent() {
    var frm = $("#contentFrm").valid();
    if (frm == true) {

        $("#btnUpdateContent").children(".fa-spinner").removeClass('hide');
        $("#btnUpdateContent").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');
        var content = new FormData();
        var files = $("#Image").get(0).files;

        var isactive = $('#IsActive:checked').length;
        var status = "";
        if (isactive === 1) { status = "true"; }
        else { status = "false"; }

        content.append("Content_ID", $("#Content_ID").val()),
        content.append("Gallery_Id", $("#Gallery_Id").val()),
            content.append("Image", $("#oldImage").val()),
            content.append("SmallImage", $("#SmallImage").val()),
            content.append("CreateDate", $("#CreateDate").val()),
            content.append("IsActive", status),
            content.append("imgContent", files[0])

        $.ajax({
            method: 'POST',
            url: '/Admin/Contents?handler=EditContent',
            contentType: false,
            processData: false,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            data: content,
            success: function (result) {
                $("#btnUpdateContent").children(".fa-spinner").addClass('hide');
                $("#btnUpdateContent").children(".fa-spinner").siblings('span').text('ویرایش');

                if (result.status == "ok") {
                    LoadDatatable("#content_tb", false);
                    ToastMessage("اطلاعات مشترک محتوا با موفقیت ویرایش گردید", "success", "کاربر گرامی");
                }
                else {
                    if (isDevelopment) {
                        alert(result.data);
                    } else {
                        ToastMessage("خطایی رخ داده است", "info", "خطا");
                    }
                }
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}

function updateContentTranslation() {
    var frm = $("#contentTransFrm").valid();
    if (frm == true) {
        if ($("#language_id").val() != "0") {
            $("#btnUpdateContentFinal").children(".fa-spinner").removeClass('hide');
            $("#btnUpdateContentFinal").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');

            var contentTranslation = {
                ContentTranslation_ID: $("#Title").attr('data-id'),
                Content_Id: $("#Content_Id").val(),
                Title: $("#Title").val(),
                Slug: $("#Slug").val(),
                Summery: $("#Summery").val(),
                Author: $("#Author").val(),
                ContentText: CKEDITOR.instances['ContentText'].getData(),
                MetaTitle: $("#MetaTitle").val(),
                MetaKeword: $("#MetaKeword").val(),
                MetaDescription: $("#MetaDescription").val(),
                MetaOther: $("#MetaOther").val(),
                Slug: $("#Slug").val(),
                AvgVotes: $("#AvgVotes").val(),
                LikeCount: $("#LikeCount").val(),
                VisitCount: $("#VisitCount").val(),
                SumVotes: $("#SumVotes").val(),
                Votes: $("#Votes").val(),
                Tag: $("#Tag").val(),
                Language_Id: $("#language_id").val(),
            }

            $.ajax({
                method: 'POST',
                url: '/Admin/Contents?handler=EditContentTrans',
                data: {
                    contentTranslation: contentTranslation,
                    selectedGroupId: selectedGroupIdsEdit,
                    __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
                },
                success: function (result) {
                    $("#btnUpdateContentFinal").children(".fa-spinner").addClass('hide');
                    $("#btnUpdateContentFinal").children(".fa-spinner").siblings('span').text('ویرایش');

                    if (result.status == "ok") {
                        resetForm();
                        ToastMessage(" محتوا با موفقیت ویرایش گردید", "success", "کاربر گرامی");
                        LoadDatatable("#content_tb", false);
                        viewAllContents(result.contentTrans_id);
                        $(".contentGroups_badge").html('');
                        viewAllContents();
                    }
                    //else if (result == "exsist") {
                    //    ToastMessage(" این کد رنگ برای این زبان قبلا ثبت شده است", "info", "کاربر گرامی");
                    //}
                    else {
                        if (isDevelopment) {
                            alert(result.data);
                        } else {
                            ToastMessage("خطایی رخ داده است", "warning", "خطا");
                        }
                    }
                },
                error: function (result) {
                }
            });
        }
        else {
            ToastMessage(" لطفا یک زبان را انتخاب کنید", "info", "کاربر گرامی");
        }

    }
    else {
        return false;
    }
}

function deleteContent(contentId, Id) {
    $.confirm({
        title: 'آیا برای حذف محتوا مطمئن هستید؟', icon: 'fa fa-warning', theme: "material", animation: 'scale', content: '',
        buttons: {
            confirm: {
                text: 'تایید',
                btnClass: 'btn-blue',
                action: function () {
                    $.get("/Admin/Contents?handler=DeleteContent", { id: Id, contentId: contentId, status: "one" },
                        function (result) {
                            if (result == "ok") {
                                ToastMessage(" محتوا با موفقیت حذف گردید", "success", "کاربر گرامی");
                                LoadDatatable("#content_tb", false);
                               
                            }
                            else {
                                if (isDevelopment) {
                                    alert(result.data);
                                } else {
                                    ToastMessage("خطایی رخ داده است", "warning", "خطا");
                                }
                            }
                        });
                }
            },
            cancel: {
                text: 'انصراف',

            }
        },
    });
}

function getTagInputContent() {
    var tagText = "";
    $(".tagify").children('.tagify__tag').each(function () {
        tagText += $(this).attr('title') + ",";
    });
    return tagText;
}

function getContentStatistics(translationId) {
    $.get("/Admin/Contents?handler=ContentStatistics", { id: translationId }, function (result) {
        $(".modal-dialog").css('max-width', '65%');
        $(".modal-header").css('background-color', 'rgb(2 80 2 / 72%)');
        $(".headerModalColor").text("آمار محتوا");
        $("#myModal").modal();
        $("#myModalBody").html(result);
    });
}

function resetForm() {
    $("#language_id").val('0');
    $("#Title").val('');
    $("#Tag").tagsinput('removeAll');
    $("#Slug").val('');
    $("#Summery").val('');
    $("#Author").val('');
    $("#MetaTitle").val('');
    $("#MetaKeword").val('');
    $("#MetaDescription").val('');
    $("#MetaOther").val('');
    CKEDITOR.instances['ContentText'].setData('');
    selectedGroupIds = [];
    selectedGroupIdsEdit = [];
}
