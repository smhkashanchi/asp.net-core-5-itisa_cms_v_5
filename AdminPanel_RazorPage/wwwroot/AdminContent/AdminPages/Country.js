﻿var isDevelopment = true;

function createCountry() {
    $.get("/Admin/Regions?handler=Index", function (result) {
        if (result.status == "error") {
            if (isDevelopment) {
                alert(result.msg);
            } else {
                ToastMessage("خطایی رخ داده است", "info", "خطا");
            }
        }
        $(".modal-dialog").css('max-width', '55%');
        $(".modal-header").css('background-color', '#008000b8');
        $(".headerModalColor").text("افزودن کشور");
        $("#myModal").modal();
        $("#myModalBody").html(result);
    });
}

function insertCountry() {
    var frm = $("#countryFrm").valid();
    if (frm == true) {
        $("#btnInsertCountry").children(".fa-spinner").removeClass('hide');
        $("#btnInsertCountry").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');
        var isactive = $('#IsActive:checked').length;
        var status = "";
        if (isactive === 1) { status = "true"; }
        else { status = "false"; }

        var country = {
            Name: $("#Name").val(),
            Code: $("#Code").val(),
            IsActive: status,
        }
        $.ajax({
            method: 'POST',
            url: '/Admin/Regions?handler=CreateCountry',
            data: {
                country: country,
                __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
            },
            success: function (result) {
                $(".fa-spinner").addClass('hide');
                $(".fa-spinner").siblings('span').text('ثبت');

                if (result == "ok") {

                    LoadDatatable("#country_tb", false);
                    $("#myModal").modal("hide");
                    ToastMessage("کشور با موفقیت ثبت گردید", "success", "کاربر گرامی");
                    getCountryByLang_Global("#country_ddl");
                }
                else if (result == "exists") {
                    ToastMessage(" این کشور قبلا ثبت شده است", "info", "کاربر گرامی");
                }
                else {
                    if (isDevelopment) {
                        alert(result.data);
                    } else {
                        ToastMessage("خطایی رخ داده است", "info", "خطا");
                    }
                }
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}

function viewAllCountries() {
    loadingPageShow();
    $("#country_tb").DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.24/i18n/Persian.json"
        },
        "processing": true,
        "serverSide": true,
        "filter": true,
        "ajax": {
            "url": "/Admin/Regions?handler=ViewAllCountries",
            "type": "POST",
            "datatype": "json",
            "data": { __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val() },
        },

        "columns": [
            { "data": "name", "name": "Name" },
            { "data": "code", "name": "Code" },
            {
                data: "isActive",
                render: function (data) {
                    if (data) {
                        return '<span class="text-success">فعال</span >';
                    }
                    else {
                        return '<span class="text-danger">غیر فعال</span >';
                    }
                },
            },
            {
                mRender: function (data, type, row) {
                    return '<div class="btn-group" role="group">' +
                        '<span class="btn btn-outline-info" onclick="editCountry(' + row.country_ID + ')"><i class="fa fa-edit fa-sm"></i></span>' +
                        '<span class="btn btn-outline-danger btnCountryDelete" onclick="deleteCountry(' + row.country_ID + ')"><i class="fa fa-trash fa-sm"></i></span></div>';
                }
            },
        ]

    });
    loadingPageHide();
}

function editCountry(Id) {
    $.get("/Admin/Regions?handler=EditCountry", { id: Id }, function (result) {
        if (result.status == "error") {
            if (isDevelopment) {
                alert(result.msg);
            } else {
                ToastMessage("خطایی رخ داده است", "info", "خطا");
            }
        }
        $(".modal-dialog").css('max-width', '55%');
        $(".modal-header").css('background-color', '#008074b8');
        $(".headerModalColor").text("ویرایش کشور");
        $("#myModal").modal();
        $("#myModalBody").html(result);
    });
}

function updateCountry() {
    var frm = $("#countryFrm").valid();
    if (frm == true) {
        $("#btnUpdateCountry").children(".fa-spinner").removeClass('hide');
        $("#btnUpdateCountry").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');
        var isactive = $('#IsActive:checked').length;
        var status = "";
        if (isactive === 1) { status = "true"; }
        else { status = "false"; }

        var country = {
            Country_ID: $("#Country_ID").val(),
            Name: $("#Name").val(),
            Code: $("#Code").val(),
            IsActive: status,
        }
        $.ajax({
            method: 'POST',
            url: '/Admin/Regions?handler=EditCountry',
            data: {
                country: country,
                __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
            },
            success: function (result) {
                $(".fa-spinner").addClass('hide');
                $(".fa-spinner").siblings('span').text('ویرایش');

                if (result == "ok") {
                    LoadDatatable("#country_tb", false);
                    $("#myModal").modal("hide");
                    ToastMessage("کشور با موفقیت ویرایش گردید", "success", "کاربر گرامی");
                    getCountryByLang_Global("#country_ddl");
                }
                else {
                    if (isDevelopment) {
                        alert(result.data);
                    } else {
                        ToastMessage("خطایی رخ داده است", "info", "خطا");
                    }
                }
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}

function deleteCountry(countryId) {
    $.confirm({
        title: 'آیا برای حذف این کشور مطمئن هستید؟', icon: 'fa fa-warning', theme: 'material', animation: 'scale', content: '',
        buttons: {
            confirm: {
                text: 'تایید',
                btnClass: 'btn-blue',
                action: function () {

                    $.get("/Admin/Regions?handler=DeleteCountry", { id: countryId }, function (result) {
                        if (result == "ok") {
                            LoadDatatable("#country_tb", false);
                            ToastMessage("کشور با موفقیت حذف گردید", "success", "کاربر گرامی");
                            getCountryByLang_Global("#country_ddl");
                        }
                        else if (result == "exists") {
                            ToastMessage("برای این کشور استان تعریف شده است", "warning", "کاربر گرامی");
                        }
                        else if (result.status == "error") {
                            if (isDevelopment) {
                                alert(result.data);
                            } else {
                                ToastMessage("خطایی رخ داده است", "info", "خطا");
                            }
                        }
                    });
                }
            },
            cancel: {
                text: 'انصراف',
            }
        },
    });

}

function getCountryByLang_Global(selector) {
    $.get("/Admin/Regions?handler=CountiesByLang", function (result) {
        var options = "<option value=''>کشور...</option>";
        for (var i = 0; i < result.length; i++) {
            options += "<option value='" + result[i].country_ID + "'>" + result[i].name + "</option>";
        }
        $(selector).html(options);
    });


}

function getCountryByLang_GlobalForCity(selector, status, selectedValue) {
    $.get("/Admin/Regions?handler=CountiesByLangForCity", function (result) {
        var options = "<option value=''>کشور...</option>";
        for (var i = 0; i < result.length; i++) {
            options += "<option value='" + result[i].country_ID + "'>" + result[i].name + "</option>";
        }
        $(selector).html(options);
        if (status == "edit")
            $(selector).val(selectedValue);
    });


}

function test2() {
    $.get("/Admin/Regions?handler=Test2", {id:-3}, function (result) {
       
    });
}
function testt() {
    var model = [];
    for (var i = 0; i < 5; i++) {
        model.push({ productoId: i, value: "test1" });
    }
    //[{ "productoId": 0, "value": "test1" }, { "productoId": 1, "value": "test1" }, { "productoId": 2, "value": "test1" }, { "productoId": 3, "value": "test1" }, { "productoId": 4, "value": "test1" }]
    var detalleVenta =
        [

            { productoId: 1, value: "test1" },
            { productoId: 2, value: "test2" },
            { productoId: -3, value: "test21" }
        ]

    console.log(JSON.stringify(detalleVenta));
    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/Admin/Regions?handler=PostTest",
        //dataType: 'json',
        data: JSON.stringify(detalleVenta),
        beforeSend: function (xhr) {
            loadingPageShow();
            xhr.setRequestHeader("XSRF-TOKEN",
                $('input:hidden[name="__RequestVerificationToken"]').val());
        },
        success: function (response) {
            loadingPageHide();
            console.log(response)
        },
        error: function (xhr, resp, text) {
            console.log(xhr, resp, text);
            alert(xhr.statusText);
        }

    });
}
