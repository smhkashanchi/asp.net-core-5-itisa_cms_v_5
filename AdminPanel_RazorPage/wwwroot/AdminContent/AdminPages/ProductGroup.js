﻿var isDevelopment = true;
var productGroupIdForParentId = 0;
var productGroupIdForParentIdEdit = 0;
var pgID = 1;
var Languages;

function getAllProductsGroupForTreeView(lang, status) {
    $.ajax({
        method: 'GET',
        url: '/Admin/Products?handler=CreateTreeView',
        data: { lang: lang },
        success: function (response) {

            $('#productsGroup_tw').treeview({  //Create Product

                data: response,
                expandIcon: 'fa fa-plus-square',
                collapseIcon: 'fa fa-minus-square',
                onNodeSelected: function (event, node) {
                    productGroupIdForParentId = node.tags;
                }

            });
            if (status != "dontShow") {
                $('#productsGroup2_tw').treeview({   //index Product
                    data: response,
                    expandIcon: 'fa fa-plus-square',
                    collapseIcon: 'fa fa-minus-square',
                    onNodeSelected: function (event, node) {
                        viewDetailsProductGroup(node.tags);
                    },
                    //
                });
            }

            $('#productsGroup3_tw').treeview({   //Edit ProductGroup
                data: response,
                expandIcon: 'fa fa-plus-square',
                collapseIcon: 'fa fa-minus-square',
                onNodeSelected: function (event, node) {
                    productGroupIdForParentIdEdit = node.tags;
                },
            });

            $('#productsGroup4_tw').treeview({   //add product
                data: response,
                expandIcon: 'fa fa-plus-square',
                collapseIcon: 'fa fa-minus-square',
                multiSelect: true,
                onNodeSelected: function (event, node) {
                    //var cgtId = node.href;
                    //cgtId = cgtId.replace("#", "");
                    selectedProductGroupIds.push(node.tags);
                },
                onNodeUnselected: function (event, node) {
                    //var cgtId = node.href;
                    //cgtId = cgtId.replace("#", "");
                    selectedProductGroupIds.splice(selectedProductGroupIds.indexOf(node.tags), 1);
                }
            });

            if (status != "dontShow") {
                $('#productsGroup5_tw').treeview({   //view all product
                    data: response,
                    expandIcon: 'fa fa-plus-square',
                    collapseIcon: 'fa fa-minus-square',
                    onNodeSelected: function (event, node) {
                        //var cgtId = node.href;
                        //cgtId = cgtId.replace("#", "");
                        viewAllProducts(node.tags);
                    },
                });
            }

            $('#productsGroup6_tw').treeview({   //edit product
                data: response,
                expandIcon: 'fa fa-plus-square',
                collapseIcon: 'fa fa-minus-square',
                multiSelect: true,
                onNodeSelected: function (event, node) {

                    selectedGroupIdsEdit.push(node.tags);
                },
                onNodeUnselected: function (event, node) {
                    var cgtId = node.href;
                    cgtId = cgtId.replace("#", "");
                    selectedGroupIdsEdit.splice(selectedGroupIdsEdit.indexOf(node.tags), 1);
                }
            });


            $('#productsGroup7_tw').treeview({   //view all product for similar
                data: response,
                expandIcon: 'fa fa-plus-square',
                collapseIcon: 'fa fa-minus-square',
                onNodeSelected: function (event, node) {
                  
                    viewAllProductsForSimilarProduct(node.tags);
                },
            });

            $('#productsGroup8_tw').treeview({   //view all product for suggest
                data: response,
                expandIcon: 'fa fa-plus-square',
                collapseIcon: 'fa fa-minus-square',
                onNodeSelected: function (event, node) {

                    viewAllProductsForSuggestProduct(node.tags);
                },
            });

            $('#productsGroup9_tw').treeview({   //edit product
                data: response,
                expandIcon: 'fa fa-plus-square',
                collapseIcon: 'fa fa-minus-square',
                multiSelect: true,
                onNodeSelected: function (event, node) {
                    selectedProductGroupIds.push(node.tags);
                },
                onNodeUnselected: function (event, node) {
                    selectedProductGroupIds.splice(selectedProductGroupIds.indexOf(node.tags), 1);
                }
            });

        },
        error: function (response) {

        }
    });
}

function createProductGroup(Group_Id) {
    //var Group_Id = $(this).parent('li').attr('data-pgid');


    //var ;
    $.get("/Admin/Products?handler=CreateProductGroup", { id: Group_Id }, function (result) {
        if (result.status == "error") {
            if (isDevelopment) {
                alert(result.data);
            } else {
                ToastMessage("خطایی رخ داده است", "info", "خطا");
            }
        }
        $(".modal-dialog").css('max-width', '80%');
        $(".modal-header").css('background-color', '#008000b8');
        $(".headerModalColor").text("افزودن گروه محصول");
        $("#myModal").modal();
        $("#myModalBody").html(result);
    });
}

function insertProductGroup() {
    var frm = $("#productGroupFrm").valid();
    if (frm == true) {


        $("#btnInsertProductGroup").children(".fa-spinner").removeClass('hide');
        $("#btnInsertProductGroup").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');

        var productGroup = new FormData();
        var files = $("#Image_temp").get(0).files;

        if (files.length > 0) {
            productGroup.append("imgProduct", files[0])
        }
        productGroup.append("ParentId", $("#ParentId").val()),
            productGroup.append("ImageWidth", $("#ImageWidth").val()),
            productGroup.append("ImageHeight", $("#ImageHeight").val()),
            productGroup.append("SmallImageWidth", $("#SmallImageWidth").val()),
            productGroup.append("SmallImageHeight", $("#SmallImageHeight").val())


        $.ajax({
            method: 'POST',
            url: '/Admin/Products?handler=CreateProductGroup',
            contentType: false,
            processData: false,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            data: productGroup,
            success: function (result) {
                $("#btnInsertProductGroup").children(".fa-spinner").addClass('hide');
                $("#btnInsertProductGroup").children(".fa-spinner").siblings('span').text('ویرایش');

                if (result.status == "ok") {

                    $(".insert-div").addClass('hide');
                    $(".update-div").removeClass('hide');

                    fillFormForEdit(result.groupId, result.image, result.parentId);
                    switchTab();
                }
                else {
                    if (isDevelopment) {
                        alert(result.data);
                    } else {
                        ToastMessage("خطایی رخ داده است", "info", "خطا");
                    }
                }
            },
            error: function (result) {
            }
        });
        //}
        //else {
        //    ToastMessage(" لطفا یک تصویر برای گروه انتخاب کنید", "info", "کاربر گرامی");
        //}
    }
    else {
        return false;
    }
}

function fillFormForEdit(GroupId, Image, ParentId) {
    // زمانی که گروه ثبت شد اجازه ویرایش داشته باشد به همین دلیل فرم باید پر شود
    $("#ProductGroup_ID").val(GroupId);
    $("#ProductGroup_Id").val(GroupId); // In Translation
    $("#Image").val(Image);
    $("#ParentId").val(ParentId);
}

function createProductGroupTranslation(Id) {
    $.get("/Admin/Products?handler=CreateProductGroupTrans", { id: Id }, function (result) {
        $("#productGroup_trans").html(result);
        //switchTab();
    });
}

function createAnotherProductGroupTranslation(Lang) {
    if ($("#card_" + Lang).length == 0) {
        $.get("/Admin/Products?handler=CreateAnotherProductGroupTrans", { id: Lang }, function (result) {
            $("#accordion").append(result);
        });
    }
}

function insertProductGroupTranslation() {
    var frm = $("#productGroupTransFrm").valid();
    if (frm == true) {

        if ($("#ProductGroup_Id").val() != 0) {
            $("#btnInsertProductGroupFinal").children(".fa-spinner").removeClass('hide');
            $("#btnInsertProductGroupFinal").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');

            var model = [];
            /// برای ارسال لیستی از مدل به سمت سرور باید ابتدا آن را در آرایه ریخته سپس بصورت 
            /// جیسون استرینگ فای آن را میفرستیم و سمت سرور آن را بصورت فرم بادی میگیریم
            $("#accordion").children('.card').each(function (index) {

                var languageId = $(this).attr('data-id');

                if ($("#Title_" + languageId + "").val() != "") {
                    var isactive = $("#IsActive_" + languageId + ":checked").length;
                    var status = "";
                    if (isactive === 1) { status = "true"; }
                    else { status = "false"; }
                    model.push({
                        productGroup_Id: $("#ProductGroup_Id").val(),
                        title: $("#Title_" + languageId + "").val(),
                        slug: $("#Slug_" + languageId + "").val(),
                        description: $("#Description_" + languageId + "").val(),
                        discount_ID: $("#DiscountId_" + languageId + "").val(),
                        isActive: status,
                        language_Id: languageId.trim(),
                    });
                }
            });

            $.ajax({
                type: "POST",
                contentType: "application/json",
                url: "/Admin/Products?handler=CreateProductGroupTrans",
                //dataType: 'json',
                data: JSON.stringify(model),
                beforeSend: function (xhr) {
                    loadingPageShow();
                    xhr.setRequestHeader("XSRF-TOKEN",
                        $('input:hidden[name="__RequestVerificationToken"]').val());
                },
                success: function (response) {
                    $("#btnInsertProductGroupFinal").children(".fa-spinner").addClass('hide');
                    $("#btnInsertProductGroupFinal").children(".fa-spinner").siblings('span').text('ثبت');

                    $(".insert-div").removeClass('hide');
                    $(".update-div").addClass('hide');

                    if (response.status == "ok") {
                        $("#myModal").modal('hide');
                        ToastMessage("گروه محصول با موفقیت ثبت گردید", "success", "کاربر گرامی");
                        getAllProductsGroupForTreeView($("#global_language_id").val());
                        viewDetailsProductGroup(response.group_id);

                    }
                    else if (result == "exsist") {
                        ToastMessage(" این گروه  برای این زبان قبلا ثبت شده است", "info", "کاربر گرامی");
                    }
                    else {
                        if (isDevelopment) {
                            alert(response.data);
                        } else {
                            ToastMessage("خطایی رخ داده است", "warning", "خطا");
                        }
                    }
                },
                error: function (xhr, resp, text) {
                    console.log(xhr, resp, text);
                    alert(xhr.statusText);
                }

            });
        }

        else {
            ToastMessage(" لطفا ابتدا اطلاعات مشترک گروه را ثبت کنید", "info", "کاربر گرامی");
        }
    }
    else {
        return false;
    }
}

function viewDetailsProductGroup(group_id) {
    $.get("/Admin/Products?handler=ViewDetailsProductGroup", { groupId: group_id }, function (response) {

        $("#productGroupDetails").html(response);
    });
}

function editProductGroup(productGroupId, Id) {
    $.get("/Admin/Products?handler=EditProductGroup", { id: productGroupId }, function (result) {
        if (result.status == "error") {
            if (isDevelopment) {
                alert(result.data);
            } else {
                ToastMessage("خطایی رخ داده است", "info", "خطا");
            }
        }
        else {

            $(".modal-dialog").css('max-width', '80%');
            $(".modal-header").css('background-color', '#008074b8');
            $(".headerModalColor").text("ویرایش گروه محصول");
            $("#myModal").modal();
            $("#myModalBody").html(result);
            editProductGroupTranslation(Id);

        }
    });
};

function editProductGroupTranslation(Id) {

    $.get("/Admin/Products?handler=EditProductGroupTrans", { id: Id }, function (res) {
        if (result.status == "error") {
            if (isDevelopment) {
                alert(result.data);
            } else {
                ToastMessage("خطایی رخ داده است", "info", "خطا");
            }
        }
        else {
            $("#productGroup_trans").html(res);
        }
    });
}

function editAnotherProductGroupTranslation(Lang, ProductGroupId) {
    if ($("#card_" + Lang).length == 0) {
        $.get("/Admin/Products?handler=EditAnotherProductGroupTrans", { id: Lang, productGroupId: ProductGroupId }, function (result) {
            $("#accordion").append(result);

        });
    }
}

function updateProductGroup() {
    var frm = $("#productGroupFrm").valid();
    if (frm == true) {
        if (productGroupIdForParentIdEdit != 0)
            parentId = productGroupIdForParentIdEdit;
        else
            parentId = $("#ParentId").val();

        if ($("#ProductGroup_ID").val() != parentId) {
            $("#btnUpdateProductGroup").children(".fa-spinner").removeClass('hide');
            $("#btnUpdateProductGroup").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');



            var productGroup = new FormData();
            var files = $("#Image_temp").get(0).files;

            if (files.length > 0) {
                productGroup.append("imgProduct", files[0])
            }
            productGroup.append("ParentId", parentId),
                productGroup.append("ProductGroup_ID", $("#ProductGroup_ID").val()),
                productGroup.append("ImageWidth", $("#ImageWidth").val()),
                productGroup.append("ImageHeight", $("#ImageHeight").val()),
                productGroup.append("SmallImageWidth", $("#SmallImageWidth").val()),
                productGroup.append("SmallImageHeight", $("#SmallImageHeight").val()),
                productGroup.append("SiteMap_Id", $("#SiteMap_Id").val()),
                productGroup.append("Image", $("#Image").val()),


                $.ajax({
                    method: 'POST',
                    url: '/Admin/Products?handler=EditProductGroup',
                    contentType: false,
                    processData: false,
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader("XSRF-TOKEN",
                            $('input:hidden[name="__RequestVerificationToken"]').val());
                    },
                    data: productGroup,
                    success: function (result) {
                        $("#btnUpdateProductGroup").children(".fa-spinner").addClass('hide');
                        $("#btnUpdateProductGroup").children(".fa-spinner").siblings('span').text('ویرایش');

                        if (result == "ok") {
                            viewDetailsProductGroup($("#ProductGroup_ID").val());
                            getAllProductsGroupForTreeView($("#global_language_id").val(), "edit");
                            switchTab();
                        }
                        else {
                            if (isDevelopment) {
                                alert(result.data);
                            } else {
                                ToastMessage("خطایی رخ داده است", "info", "خطا");
                            }
                        }
                    },
                    error: function (result) {
                    }
                });
        }
        else {
            ToastMessage("لطفا گروه مناسب را انتخاب نمایید", "info", "کاربر گرامی");
        }
    }
    else {
        return false;
    }
}

function updateProductGroupTranslation() {
    var frm = $("#productGroupTransFrm").valid();
    if (frm == true) {
        var model = [];

        $("#accordion").children('.card').each(function (index) {

            var languageId = $(this).attr('data-id');
            var isactive = $("#IsActive_" + languageId + ":checked").length;
            var status = "";
            if (isactive === 1) { status = "true"; }
            else { status = "false"; }
            model.push({
                productGroupTranslation_ID: $("#Title_" + languageId + "").attr('data-id'),
                productGroup_Id: $("#ProductGroup_Id").val(),
                title: $("#Title_" + languageId + "").val(),
                slug: $("#Slug_" + languageId + "").val(),
                description: $("#Description_" + languageId + "").val(),
                discount_ID: $("#DiscountId_" + languageId + "").val(),
                isActive: status,
                language_Id: languageId.trim(),
            });
        });

        $.ajax({
            type: "POST",
            contentType: "application/json",
            url: "/Admin/Products?handler=EditProductGroupTrans",
            //dataType: 'json',
            data: JSON.stringify(model),
            beforeSend: function (xhr) {
                loadingPageShow();
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            success: function (response) {
                $("#btnInsertProductGroupFinal").children(".fa-spinner").addClass('hide');
                $("#btnInsertProductGroupFinal").children(".fa-spinner").siblings('span').text('ویرایش');

                $(".insert-div").removeClass('hide');
                $(".update-div").addClass('hide');

                if (response.status == "ok") {
                    $("#myModal").modal('hide');
                    ToastMessage("گروه محصول با موفقیت ویرایش گردید", "success", "کاربر گرامی");
                    viewDetailsProductGroup(response.group_id);
                    getAllProductsGroupForTreeView($("#global_language_id").val());
                }
                else if (response == "exsist") {
                    ToastMessage(" این گروه  برای این زبان قبلا ثبت شده است", "info", "کاربر گرامی");
                }
                else {
                    if (isDevelopment) {
                        alert(result.data);
                    } else {
                        ToastMessage("خطایی رخ داده است", "warning", "خطا");
                    }
                }
            },
            error: function (xhr, resp, text) {
                console.log(xhr, resp, text);
                alert(xhr.statusText);
            }

        });

    }
    else {
        return false;
    }
}

function deleteProductGroup(productGroupId, Id) {
    $.confirm({
        title: 'آیا برای حذف گروه محصول مطمئن هستید؟', icon: 'fa fa-warning', theme: "material", animation: 'scale', content: '',
        buttons: {
            confirm: {
                text: 'تایید',
                btnClass: 'btn-blue',
                action: function () {
                    $.get("/Admin/Products?handler=DeleteProductGroup", { id: Id, productGroupId: productGroupId, status: "one" },
                        function (result) {
                            if (result == "ok") {
                                ToastMessage("گروه محصول با موفقیت حذف گردید", "success", "کاربر گرامی");
                                getAllProductsGroupForTreeView($("#global_language_id").val());
                                viewDetailsProductGroup(productGroupId);
                            }
                            else if (result == "exists") {
                                ToastMessage("برای این گروه محصول ثبت شده است", "warning", "کاربر گرامی");
                            }
                            else if (result == "existParent") {
                                ToastMessage("برای این گروه زیر گروه ثبت شده است", "warning", "کاربر گرامی");
                            }
                            else {
                                if (isDevelopment) {
                                    alert(result.data);
                                } else {
                                    ToastMessage("خطایی رخ داده است", "warning", "خطا");
                                }
                            }
                        });
                }
            },
            deleteall: {
                text: 'حذف در تمام زبان ها',
                btnClass: 'btn-red',
                action: function () {

                    $.get("/Admin/Products?handler=DeleteProductGroup", { id: Id, productGroupId: productGroupId, status: "all" },
                        function (result) {
                            if (result == "ok") {
                                ToastMessage("گروه محصول در تمام زبان ها با موفقیت حذف گردید", "success", "کاربر گرامی");
                                getAllProductsGroupForTreeView($("#global_language_id").val());
                                viewDetailsProductGroup(productGroupId);
                            }
                            else if (result == "exists") {
                                ToastMessage("برای گروه محصول ثبت شده است", "warning", "کاربر گرامی");
                            }
                            else if (result.status == "existParent") {
                                ToastMessage("برای گروه زیر گروه ثبت شده است", "warning", "کاربر گرامی");
                            }
                            else {
                                if (isDevelopment) {
                                    alert(result.data);
                                } else {
                                    ToastMessage("خطایی رخ داده است", "warning", "خطا");
                                }
                            }
                        });
                }
            },
            cancel: {
                text: 'انصراف',

            }
        },
    });
}

function getAllDiscounts(langId, selector, optionSelected, status) {
    $.get("/Admin/Products?handler=DiscountWithLanguage", { id: langId }, function (result) {

        var options = "<option value=''>تخفیف ...</option>";
        for (var i = 0; i < result.length; i++) {
            options += "<option value='" + result[i].discount_ID + "'>" + result[i].title + "</option>";
        }
        $(selector).html(options);
        if (status == "edit")
            $(selector).val(optionSelected);

    });
}

function createProductGroupFeature(GroupTranslationId) {
    $.get("/Admin/Products?handler=CreateFeaturesForProductGroup", { id: GroupTranslationId }, function (result) {
        if (result.status == "error") {
            if (isDevelopment) {
                alert(result.data);
            } else {
                ToastMessage("خطایی رخ داده است", "info", "خطا");
            }
        }
        $(".modal-dialog").css('max-width', '90%');
        $(".modal-header").css('background-color', '#008000b8');
        $(".headerModalColor").text("افزودن ویژگی ");
        $("#myModal").modal();
        $("#myModalBody").html(result);
    });
}

function getFeaturesByGroupId(featureGroupId) {
    $.get("/Admin/Products?handler=FeaturesByGroup", { featureGroupId: featureGroupId }, function (result) {
        $("#featureList").html(result);
    });
}

function insertProductGroupFeatures(ProductGroupId) {

    $(".btnAddFeature").children(".fa-spinner").removeClass('hide');
    $(".btnAddFeature").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');
   
    var checkboxValues = "";
    $('.forAdd:checked').each(function () { // گرفتن ولیو های چکس باکس هایی که تیک خورده باشند
        checkboxValues += $(this).val() + "&";
    });
    var features = [];
    features = checkboxValues.split("&");

    jQuery.ajaxSettings.traditional = true; // جهت ارسال آرایه بصورت گت این پراپرتی باید تنظیم شود

    $.ajax({
        method: 'GET',
        url: '/Admin/Products?handler=CreateProductGroupFeatures',
        data: {
            features: features,
            productGroupId:ProductGroupId,
            __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
        },
        success: function (result) {
            $(".btnAddFeature").children(".fa-spinner").addClass('hide');
            $(".btnAddFeature").children(".fa-spinner").siblings('span').text('افزودن');

            if (result == "ok") {
                $("#productGroupFeature_tb_wrapper").remove();
                getProductGroupFeatures(ProductGroupId);
                viewDetailsProductGroup(ProductGroupId);
                ToastMessage("ویژگی با موفقیت به لیست ویژگی های گروه اضافه گردید", "success", "کاربر گرامی");
            }
            else if (result == "exists") {
                ToastMessage("این ویژگی برای این گروه محصول ثبت شده است", "info", "کاربر گرامی");
            }
            else {
                if (isDevelopment) {
                    alert(result.data);
                } else {
                    ToastMessage("خطایی رخ داده است", "info", "خطا");
                }
            }
        },
        error: function (result) {
        }
    });
}

function getProductGroupFeatures(ProductGroupId) {
    $.get("/Admin/Products?handler=ProductGroupFeatures", { productGroupId: ProductGroupId }, function (result) {
        $("#productGroupFeatures").append(result);
    });
}

function deleteProductGroupFeatures(ProductGroupId,BtnDeleteText) {
    $.confirm({
        title: 'آیا برای حذف ویژگی گروه محصول مطمئن هستید؟', icon: 'fa fa-warning', theme: "material", animation: 'scale', content: '',
        buttons: {
            confirm: {
                text: 'تایید',
                btnClass: 'btn-blue',
                action: function () {
                    $(".btnDeleteFeature").children(".fa-spinner").removeClass('hide');
                    $(".btnDeleteFeature").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');

                    var checkboxValues = "";
                    $('.forRemove:checked').each(function () {  // گرفتن ولیو های چکس باکس هایی که تیک خورده باشند
                        checkboxValues += $(this).val() + "&";
                    });
                    var features = [];
                    features = checkboxValues.split("&");

                    jQuery.ajaxSettings.traditional = true; // جهت ارسال آرایه بصورت گت این پراپرتی باید تنظیم شود

                    $.ajax({
                        method: 'GET',
                        url: '/Admin/Products?handler=DeleteProductGroupFeatures',
                        data: {
                            features: features,
                            productGroupId: ProductGroupId,
                            __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
                        },
                        success: function (result) {
                            $(".btnDeleteFeature").children(".fa-spinner").addClass('hide');
                            $(".btnDeleteFeature").children(".fa-spinner").siblings('span').text(BtnDeleteText);

                            if (result == "ok") {
                                for (var i = 0; i < features.length - 1; i++) {
                                    $("#pgf_" + features[i]).hide("slow");
                                }
                                viewDetailsProductGroup(ProductGroupId);
                                ToastMessage("ویژگی با موفقیت از لیست ویژگی های گروه حذف گردید", "success", "کاربر گرامی");
                            }
                            else {
                                if (isDevelopment) {
                                    alert(result.data);
                                } else {
                                    ToastMessage("خطایی رخ داده است", "info", "خطا");
                                }
                            }
                        },
                        error: function (result) {
                        }
                    });
                }
            },
           
            cancel: {
                text: 'انصراف',

            }
        },
    });

   
}

function resetForm() {
    $("#language_id").val('0');
    $("#Title").val('');
    $("#ProductGroup_Id").val('');
    $("#Slug").val('');
    $("#Description").val('');
    $("#Discount").val('');

    $("#ImageWidth").val('0');
    $("#ImageHeight").val('0');
    $("#SmallImageWidth").val('0');
    $("#SmallImageHeight").val('0');

    $("#imgPreviewProductGroup").attr('src', '');
}

function resetForm2() {
    $("#Title").val('');
    $("#Slug").val('');
    $("#Description").val('');
    $("#Discount").val('');
}

function switchTab() {
    $("#tab_1").removeClass("active");
    $("#productGroup_trans").addClass("active");

    $("a[href='#tab_1']").removeClass('active');
    $("a[href='#productGroup_trans']").addClass('active');
}