﻿var isDevelopment = true;

function uploadTypeSwitch(uploadType) {
    if (uploadType == "File") {
        $(".fileUpload_div").removeClass('hide');
        $(".link_div").addClass('hide');
        $(".scirpt_div").addClass('hide');
        $("#Image").removeClass();
        $("#Image").addClass('fa fa-file');
    }
    else if (uploadType == "Link") {
        $(".fileUpload_div").addClass('hide');
        $(".link_div").removeClass('hide');
        $(".scirpt_div").addClass('hide');

        $("#Image").removeClass();
        $("#Image").addClass('fa fa-link');
    }
    else if (uploadType == "Script") {
        $(".fileUpload_div").addClass('hide');
        $(".link_div").addClass('hide');
        $(".scirpt_div").removeClass('hide');

        $("#Image").removeClass();
        $("#Image").addClass('fa fa-code');
    }
}

function createFileManage() {
    $.get("/Admin/FilesManage?handler=CreateFileManage", function (result) {
        if (result.status == "error") {
            if (isDevelopment) {
                alert(result.data);
            } else {
                ToastMessage("خطایی رخ داده است", "info", "خطا");
            }
        }
        $(".modal-dialog").css('max-width', '85%');
        $(".modal-header").css('background-color', '#008000b8');
        $(".headerModalColor").text("افزودن فایل");
        $("#myModal").modal();
        $("#myModalBody").html(result);
    });
}


function insertFileManage() {
    var bar = $('.bar');
    var percent = $('.percent');
    var status = $('#status');
    var frm = $("#fileManageFrm").valid();
    if (frm == true) {
        if ($("#Type").val() != "-1") {
            var fileData = "";
            if ($("#Type option:selected").text() == "Link")
                fileData = $("#link").val();
            else if ($("#Type option:selected").text() == "Script")
                fileData = $("#srcipt_txt").val();

            var fileManage = new FormData();
            var files = $("#fileUpload").get(0).files;

            if (fileData != "" || files.length > 0) {
                var isactive = $('#IsActive:checked').length;
                var status = "";
                if (isactive === 1) { status = "true"; }
                else { status = "false"; }

                fileManage.append("Type", $("#Type option:selected").text()),
                    fileManage.append("IsActive", status),
                    fileManage.append("FileData", fileData),
                    fileManage.append("uploadedFile", files[0])


                $("#btnInsertFileManage").children(".fa-spinner").removeClass('hide');
                $("#btnInsertFileManage").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');


                $.ajax({
                    method: 'POST',
                    url: '/Admin/FilesManage?handler=CreateFileManage',
                    contentType: false,
                    processData: false,


                    beforeSend: function (xhr) {
                        xhr.setRequestHeader("XSRF-TOKEN",
                            $('input:hidden[name="__RequestVerificationToken"]').val());

                    },
                    uploadProgress: function (event, position, total, percentComplete) {
                        var percentVal = percentComplete + '%';
                        bar.width(percentVal);
                        percent.html(percentVal);
                    },

                    data: fileManage,
                    success: function (result) {
                        $("#btnInsertFileManage").children(".fa-spinner").addClass('hide');
                        $("#btnInsertFileManage").children(".fa-spinner").siblings('span').text('ویرایش');

                        if (result.status == "ok") {

                            $(".insert-div").addClass('hide');
                            $(".update-div").removeClass('hide');

                            $("#FileManage_ID").val(result.fileManageId);

                            createFileManageTranslation(result.fileManageId);
                        }
                        else {
                            if (isDevelopment) {
                                alert(result.data);
                            } else {
                                ToastMessage("خطایی رخ داده است", "info", "خطا");
                            }
                        }
                    },
                    error: function (result) {
                    }
                });
            }
            else {
                ToastMessage(" فایل , اسکریپت , لینکی وارد نشده است", "warning", "کاربر گرامی");
            }
        }
        else {
            ToastMessage(" لطفا یک نوع آپلود انتخاب کنید", "info", "کاربر گرامی");
        }
    }
    else {
        return false;
    }
}

function createFileManageTranslation(fileManageId) {
    $.get("/Admin/FilesManage?handler=CreateFileManageTrans", { id: fileManageId }, function (result) {
        $("#fileManage_trans").html(result);
        $("#tab_1").removeClass("active");
        $("#fileManage_trans").addClass("active");

        $("a[href='#tab_1']").removeClass('active');
        $("a[href='#fileManage_trans']").addClass('active');
    });
}

function insertFileManageTranslation() {
    var frm = $("#fileManageTransFrm").valid();
    if (frm == true) {
        if ($("#language_id").val() != "0") {
            if (fileManageGroupTransId != 0 || fileManageGroupTransIdEdit != 0) {

                $("#btnInsertFileManageFinal").children(".fa-spinner").removeClass('hide');
                $("#btnInsertFileManageFinal").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');

                var groupTransId = 0;
                if (fileManageGroupTransId != 0)
                    groupTransId = fileManageGroupTransId;
                else
                    groupTransId = fileManageGroupTransIdEdit;

                var fileManageTranslation = {
                    FileManage_Id: $("#FileManage_Id").val(),
                    Title: $("#Title").val(),
                    Slug: $("#Slug").val(),
                    Description: CKEDITOR.instances['Description'].getData(),
                    MetaTitle: $("#MetaTitle").val(),
                    MetaKeword: $("#MetaKeword").val(),
                    MetaDescription: $("#MetaDescription").val(),
                    MetaOther: $("#MetaOther").val(),
                    TranslationGroup_Id: groupTransId,
                    Language_Id: $("#language_id").val(),
                }
                $.ajax({
                    method: 'POST',
                    url: '/Admin/FilesManage?handler=CreateFileManageTrans',
                    data: {
                        fileManageTranslation: fileManageTranslation,
                        __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
                    },
                    success: function (result) {
                        $("#btnInsertFileManageFinal").children(".fa-spinner").addClass('hide');
                        $("#btnInsertFileManageFinal").children(".fa-spinner").siblings('span').text('ثبت');

                        $(".insert-div").removeClass('hide');
                        $(".update-div").addClass('hide');

                        if (result.status == "ok") {

                            ToastMessage("فایل با موفقیت ثبت گردید", "success", "کاربر گرامی");

                            LoadDatatable("#fileManage_tb", false);
                            viewAllFileManages();
                            resetForm();
                        }
                        else if (result == "exsist") {
                            ToastMessage(" این فایل  برای این زبان قبلا ثبت شده است", "info", "کاربر گرامی");
                        }
                        else {
                            if (isDevelopment) {
                                alert(result.data);
                            } else {
                                ToastMessage("خطایی رخ داده است", "warning", "خطا");
                            }
                        }
                    },
                    error: function (result) {
                    }
                });
            }
            else {
                ToastMessage(" لطفا یک گروه را انتخاب کنید", "info", "کاربر گرامی");
            }
        }
        else {
            ToastMessage(" لطفا یک زبان را انتخاب کنید", "info", "کاربر گرامی");
        }
    }
    else {
        return false;
    }
}

$("#fileManage_tb").on("click", " .btnShowFile", function () { ///نمایش اسکریپت داخل مودال
    loadingPageShow();
    var script_file = $(this).attr('title');
    $(".modal-dialog").css('max-width', '65%');
    $(".modal-header").css('background-color', 'white');
    $(".headerModalColor").text("");
    $("#myModal").modal();
    $("#myModalBody").html(script_file);
    loadingPageHide();
});

function editFileManage(fileManageId, translationId) {
    $.get("/Admin/FilesManage?handler=EditFileManage", { id: fileManageId }, function (result) {
        if (result.status == "error") {
            if (isDevelopment) {
                alert(result.data);
            } else {
                ToastMessage("خطایی رخ داده است", "info", "خطا");
            }
        }
        else {
            $(".modal-dialog").css('max-width', '85%');
            $(".modal-header").css('background-color', '#008074b8');
            $(".headerModalColor").text("ویرایش فایل ");
            $("#myModal").modal();
            $("#myModalBody").html(result);
            editFileManageTranslation(translationId);
        }
    });
}

function editFileManageTranslation(translationId) {
    $.get("/Admin/FilesManage?handler=EditFileManageTrans", { id: translationId }, function (res) {

        $("#fileManage_trans").html(res);
        //getFileManageInfoWithLanguage('', translationId);
    });
}

function getFileManageInfoWithLanguage(langId, fileManageId, selectedGroupId) {

    $.get("/Admin/FilesManage?handler=FileManageInfo", { id: langId, fileManageId: fileManageId }, function (res) {

        if (res.status == "ok") {
            $(".update_div").removeClass('hide');
            $(".insert_div").addClass('hide');

            getAllFilesManageGroupForTreeView($("#language_id").val(), "dontShow");

            $("#Title").val(res.data.title);
            $("#Slug").val(res.data.slug);
            $("#Slug").attr('disabled', true);
            $("#isChange").prop('checked', false);
            $("#FileManageTranslation_ID").val(res.data.fileManageTranslation_ID);
            $("#TranslationGroup_Id").val(res.data.translationGroup_Id);
            CKEDITOR.instances['Description'].setData(res.data.description);
            $("#MetaTitle").val(res.data.metaTitle);
            $("#MetaKeword").val(res.data.metaKeword);
            $("#MetaDescription").val(res.data.metaDescription);
            $("#MetaOther").val(res.data.metaOther);
            $("#Title").attr('data-id', res.data.fileManageTranslation_ID);
        }
        else {
            resetForm2();
            $(".update_div").addClass('hide');
            $(".insert_div").removeClass('hide');

            getAllFilesManageGroupForTreeView($("#language_id").val());
        }
    });
}

function updateFileManage() {
    var frm = $("#fileManageFrm").valid();
    if (frm == true) {

        $("#btnUpdateFileManage").children(".fa-spinner").removeClass('hide');
        $("#btnUpdateFileManage").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');

        var fileData = "";
        if ($("#Type option:selected").text() == "Link")
            fileData = $("#link").val();
        else if ($("#Type option:selected").text() == "Script")
            fileData = $("#srcipt_txt").val();



        var fileManage = new FormData();
        var files = $("#fileUpload").get(0).files;

        var isactive = $('#IsActive:checked').length;
        var status = "";
        if (isactive === 1) { status = "true"; }
        else { status = "false"; }

        fileManage.append("Type", $("#Type option:selected").text()),
            fileManage.append("IsActive", status),
            fileManage.append("FileManage_ID", $("#FileManage_ID").val()),
            fileManage.append("CreateDate", $("#CreateDate").val()),
            fileManage.append("FileSize", $("#FileSize").val()),
            fileManage.append("OldFileData", $("#oldFileData").val()),
            fileManage.append("FileData", fileData),
            fileManage.append("uploadedFile", files[0])

        $.ajax({
            method: 'POST',
            url: '/Admin/FilesManage?handler=EditFileManage',
            contentType: false,
            processData: false,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            data: fileManage,
            success: function (result) {
                $("#btnUpdateFileManage").children(".fa-spinner").addClass('hide');
                $("#btnUpdateFileManage").children(".fa-spinner").siblings('span').text('ویرایش');

                if (result.status == "ok") {
                    LoadDatatable("#fileManage_tb", false);
                    ToastMessage("اطلاعات مشترک فایل  با موفقیت ویرایش گردید", "success", "کاربر گرامی");
                }
                else {
                    if (isDevelopment) {
                        alert(result.data);
                    } else {
                        ToastMessage("خطایی رخ داده است", "info", "خطا");
                    }
                }
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}

function updateFileManageTranslation() {
    var frm = $("#fileManageTransFrm").valid();
    if (frm == true) {
        if ($("#language_id").val() != "0") {
            $("#btnUpdateFileManageFinal").children(".fa-spinner").removeClass('hide');
            $("#btnUpdateFileManageFinal").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');
            var groupTransId = 0;

            if (fileManageGroupTransIdEdit != 0)
                groupTransId = fileManageGroupTransIdEdit;
            else
                groupTransId = $("#TranslationGroup_Id").val();

            var fileManageTranslation = {
                FileManageTranslation_ID: $("#FileManageTranslation_ID").val(),
                FileManage_Id: $("#FileManage_Id").val(),
                TranslationGroup_Id: groupTransId,
                Title: $("#Title").val(),
                Slug: $("#Slug").val(),
                Description: CKEDITOR.instances['Description'].getData(),
                MetaTitle: $("#MetaTitle").val(),
                MetaKeword: $("#MetaKeword").val(),
                MetaDescription: $("#MetaDescription").val(),
                MetaOther: $("#MetaOther").val(),
                SiteMap_Id: $("#SiteMap_Id").val(),
                Language_Id: $("#language_id").val(),
            }

            $.ajax({
                method: 'POST',
                url: '/Admin/FilesManage?handler=EditFileManageTrans',
                data: {
                    fileManageTranslation: fileManageTranslation,
                    selectedGroupId: selectedGroupIds,
                    __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
                },
                success: function (result) {
                    $("#btnUpdateFileManageFinal").children(".fa-spinner").addClass('hide');
                    $("#btnUpdateFileManageFinal").children(".fa-spinner").siblings('span').text('ویرایش');

                    if (result == "ok") {
                        resetForm();
                        ToastMessage(" فایل  با موفقیت ویرایش گردید", "success", "کاربر گرامی");
                        LoadDatatable("#fileManage_tb", false);
                        viewAllFileManages();
                    }
                    //else if (result == "exsist") {
                    //    ToastMessage(" این کد رنگ برای این زبان قبلا ثبت شده است", "info", "کاربر گرامی");
                    //}
                    else {
                        if (isDevelopment) {
                            alert(result.data);
                        } else {
                            ToastMessage("خطایی رخ داده است", "warning", "خطا");
                        }
                    }
                },
                error: function (result) {
                }
            });
        }
        else {
            ToastMessage(" لطفا یک زبان را انتخاب کنید", "info", "کاربر گرامی");
        }

    }
    else {
        return false;
    }
}

function getTypeId(typeText) {
    var typeId = "";
    if (typeText == "File")
        typeId = "0";
    else if (typeText == "Link")
        typeId = "1";
    else if (typeText == "Script")
        typeId = "2";

    return typeId;
}

function deleteFileManage(fileManageId, translationId) {
    $.confirm({
        title: 'آیا برای حذف فایل مطمئن هستید؟', icon: 'fa fa-warning', theme: "material", animation: 'scale', content: '',
        buttons: {
            confirm: {
                text: 'تایید',
                btnClass: 'btn-blue',
                action: function () {
                    $.get("/Admin/FilesManage?handler=DeleteFileManage", { id: translationId, fileManageId: fileManageId, status: "one" },
                        function (result) {
                            if (result == "ok") {
                                ToastMessage("فایل با موفقیت حذف گردید", "success", "کاربر گرامی");
                                LoadDatatable("#fileManage_tb", false);
                            }
                            else {
                                if (isDevelopment) {
                                    alert(result.data);
                                } else {
                                    ToastMessage("خطایی رخ داده است", "warning", "خطا");
                                }
                            }
                        });
                }
            },
            deleteall: {
                text: 'حذف در تمام زبان ها',
                btnClass: 'btn-red',
                action: function () {

                    $.get("/Admin/FilesManage?handler=DeleteFileManage", { id: translationId, fileManageId: fileManageId, status: "all" },
                        function (result) {
                            if (result == "ok") {
                                ToastMessage("فایل در همه زبان ها با موفقیت حذف گردید", "success", "کاربر گرامی");
                                LoadDatatable("#fileManage_tb", false);
                            }
                            else {
                                if (isDevelopment) {
                                    alert(result.data);
                                } else {
                                    ToastMessage("خطایی رخ داده است", "warning", "خطا");
                                }
                            }
                        });
                }
            },
            cancel: {
                text: 'انصراف',

            }
        },
    });
}

function resetForm() {
    $("#language_id").val('0');
    $("#Title").val('');
    $("#Slug").val('');
    $("#MetaTitle").val('');
    $("#MetaKeword").val('');
    $("#MetaDescription").val('');
    $("#MetaOther").val('');
    CKEDITOR.instances['Description'].setData('');
}

function resetForm2() {
    $("#Title").val('');
    $("#Slug").val('');
    $("#MetaTitle").val('');
    $("#MetaKeword").val('');
    $("#MetaDescription").val('');
    $("#MetaOther").val('');
    CKEDITOR.instances['Description'].setData('');
}