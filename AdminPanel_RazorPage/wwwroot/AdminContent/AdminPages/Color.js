﻿var isDevelopment = true;
///Toast Message
var ToastMessage = function (message, type, title) {
    var msgs = [message];
    var $toast = toastr[type](msgs, title);
    $toastlast = $toast;
    //return msgs;
};
toastr.options = {
    closeButton: 'true',
    progressBar: 'true',
    positionClass: 'toast-top-left',
};

$("#btnCreateColor").on('click', function () {
    $.get("/Admin/Colors?handler=Create", function (result) {
        $(".modal-dialog").css('max-width', '60%');
        $(".modal-header").css('background-color', '#008000b8');
        $(".headerModalColor").text("افزودن رنگ");
        $("#myModal").modal();
        $("#myModalBody").html(result);
    });
});

$("#btnInsertColor").on('click', function () {
    var frm = $("#colorFrm").valid();
    if (frm == true) {

        $("#btnInsertColor").children(".fa-spinner").removeClass('hide');
        $("#btnInsertColor").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');
        var color = {
            ColorCode: $("#ColorCode").val(),
        }
        $.ajax({
            method: 'POST',
            url: '/Admin/Colors?handler=Create',
            data: {
                color: color,
                __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
            },
            success: function (result) {
                $(".fa-spinner").addClass('hide');
                $(".fa-spinner").siblings('span').text('ویرایش');

                if (result.status == "ok") {

                    $(".insert-div").addClass('hide');
                    $(".update-div").removeClass('hide');

                    createColorTranslation(result.color_id);
                }
                else {
                    if (isDevelopment) {
                        alert(result.data);
                    } else {
                        ToastMessage("خطایی رخ داده است", "info", "خطا");
                    }
                }
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
});

function createColorTranslation(Id) {
    $.get("/Admin/Colors?handler=CreateColorTrans", { id: Id }, function (result) {
        $("#color_trans").html(result);
        $("#tab_1").removeClass("active");
        $("#color_trans").addClass("active");

        $("a[href='#tab_1']").removeClass('active');
        $("a[href='#color_trans']").addClass('active');
    });
}

$("#btnInsertColorFinal").on('click', function () {
    var frm = $("#colorTransFrm").valid();
    if (frm == true) {
        if ($("#language_id").val() != "0") {


            $(".fa-spinner").removeClass('hide');
            $(".fa-spinner").siblings('span').text('منتظر بمانید ...');
            var colorTranslation = {
                ColorId: $("#ColorId").val(),
                Title: $("#Title").val(),
                LanguageId: $("#language_id").val(),
            }
            $.ajax({
                method: 'POST',
                url: '/Admin/Colors?handler=CreateColorTrans',
                data: {
                    colorTranslation: colorTranslation,
                    __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
                },
                success: function (result) {
                    $(".fa-spinner").addClass('hide');
                    $(".fa-spinner").siblings('span').text('ثبت');
                    if (result == "ok") {
                        $("#language_id").val('0');
                        $("#Title").val('');
                        ToastMessage("رنگ با موفقیت ثبت گردید", "success", "کاربر گرامی");
                        LoadDatatable("#color_tb", true);
                    }
                    else if (result == "exsist") {
                        ToastMessage(" این کد رنگ برای این زبان قبلا ثبت شده است", "info", "کاربر گرامی");
                    }
                    else {
                        if (isDevelopment) {
                            alert(result.data);
                        } else {
                            ToastMessage("خطایی رخ داده است", "warning", "خطا");
                        }
                    }
                },
                error: function (result) {
                }
            });
        }
        else {
            ToastMessage(" لطفا یک زبان را انتخاب کنید", "info", "کاربر گرامی");
        }
    }
    else {
        return false;
    }
});

function viewAllColors() {
    $("#color_tb").DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.24/i18n/Persian.json"
        },
        "processing": true,
        "serverSide": true,
        "filter": true,
      
        "ajax": {
            "url": "/api/ColorsApi",
            "type": "POST",
            "datatype": "json"
        },
        "columns": [
            { "data": "title","name":"Title" }, //Param 2 ==> for orderby table
            {
                "data": "color.colorCode", "name":"color.ColorCode",
                mRender: function (data, type, row) {
                    return '<div style="color:' + row.color.colorCode + '" title="' + row.title + '">' +
                        row.color.colorCode + '</div>';
                }
            },
            {
                mRender: function (data, type, row) {
                    return '<div class="btn-group" role="group">' +
                        '<span class="btn btn-outline-info btnColorEdit" data-colorId="' + row.colorId + '" data-id="' + row.id + '"><i class="fa fa-edit fa-sm"></i></span>' +
                        '<span class="btn btn-outline-danger btnColorDelete" data-colorId="' + row.colorId + '" data-id="' + row.id + '"><i class="fa fa-trash fa-sm"></i></span></div>';
                }
            },
        ]

    });
    loadingPage();
}

$("#color_tb").on("click", " .btnColorEdit", function () {
    var colorId = $(this).attr('data-colorid');
    var Id = $(this).attr('data-id');
    $.get("/Admin/Colors?handler=Edit", { id: colorId }, function (result) {
        if (result.status == "error") {
            if (isDevelopment) {
                alert(result.data);
            } else {
                ToastMessage("خطایی رخ داده است", "info", "خطا");
            }
        }
        else {
            editColorTranslation(Id);
            $(".modal-dialog").css('max-width', '60%');
            $(".modal-header").css('background-color', '#008074b8');
            $(".headerModalColor").text("ویرایش رنگ");
            $("#myModal").modal();
            $("#myModalBody").html(result);
        }
    });
});

function editColorTranslation(Id) {
    $.get("/Admin/Colors?handler=EditColorTrans", { id: Id }, function (res) {
        $("#color_trans").html(res);
    });
}

function getColorTitleWithLanguage(langId, ColorId) {
    $.get("/Admin/Colors?handler=GetColorTitle", { id: langId, colorId: ColorId }, function (res) {
        if (res.status == "ok") {
            
            $("#Title").val(res.title);
            $("#Title").attr('data-id', res.id);
        }
        else {
            //ToastMessage(" عنوان رنگی یافت نشد!", "info", "کاربر گرامی");
        }
    });
}



$("#btnUpdateColor").on('click', function () {
    var frm = $("#colorFrm").valid();
    if (frm == true) {

        $("#btnUpdateColor").children(".fa-spinner").removeClass('hide');
        $("#btnUpdateColor").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');
        var color = {
            ColorId: $("#ColorId").val(),
            ColorCode: $("#ColorCode").val(),
        }
        $.ajax({
            method: 'POST',
            url: '/Admin/Colors?handler=Edit',
            data: {
                color: color,
                __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
            },
            success: function (result) {
                $("#btnUpdateColor").children(".fa-spinner").addClass('hide');
                $("#btnUpdateColor").children(".fa-spinner").siblings('span').text('ویرایش');

                if (result == "ok") {
                    LoadDatatable("#color_tb", false);
                    ToastMessage("اطلاعات عمومی رنگ با موفقیت ویرایش گردید", "success", "کاربر گرامی");
                }
                else {
                    if (isDevelopment) {
                        alert(result.data);
                    } else {
                        ToastMessage("خطایی رخ داده است", "info", "خطا");
                    }
                }
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
});

$("#btnUpdateColorTrans").on('click', function () {
    var frm = $("#colorTransFrm").valid();
    if (frm == true) {
        if ($("#language_id").val() != "0") {
            $("#btnUpdateColorTrans").children(".fa-spinner").removeClass('hide');
            $("#btnUpdateColorTrans").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');

            var colorTranslation = {
                Id: $("#Title").attr('data-id'),
                ColorId: $("#colorId_trans").val(),
                Title: $("#Title").val(),
                LanguageId: $("#language_id").val(),
            }
            $.ajax({
                method: 'POST',
                url: '/Admin/Colors?handler=EditColorTrans',
                data: {
                    colorTranslation: colorTranslation,
                    __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
                },
                success: function (result) {
                    $("#btnUpdateColorTrans").children(".fa-spinner").addClass('hide');
                    $("#btnUpdateColorTrans").children(".fa-spinner").siblings('span').text('ویرایش');

                    if (result == "ok") {
                        $("#language_id").val('0');
                        $("#Title").val('');
                        ToastMessage("رنگ با موفقیت ویرایش گردید", "success", "کاربر گرامی");
                        LoadDatatable("#color_tb", false);
                    }
                    else if (result == "exsist") {
                        ToastMessage(" این کد رنگ برای این زبان قبلا ثبت شده است", "info", "کاربر گرامی");
                    }
                    else {
                        if (isDevelopment) {
                            alert(result.data);
                        } else {
                            ToastMessage("خطایی رخ داده است", "warning", "خطا");
                        }
                    }
                },
                error: function (result) {
                }
            });
        }
        else {
            ToastMessage(" لطفا یک زبان را انتخاب کنید", "info", "کاربر گرامی");
        }

    }
    else {
        return false;
    }
});

$("#color_tb").on("click", " .btnColorDelete", function () {
    var colorId = $(this).attr('data-colorid');
    var Id = $(this).attr('data-id');
    $.confirm({
        title: 'آیا برای حذف مطمئن هستید؟', icon: 'fa fa-warning', animation: 'scale', content: '',
        buttons: {
            confirm: {
                text: 'تایید',
                btnClass: 'btn-blue',
                action: function () {
                    $.get("/Admin/Colors?handler=Delete", { id: Id, colorId: colorId, status: "one" },
                        function (result) {
                            if (result == "ok") {
                                ToastMessage("رنگ با موفقیت حذف گردید", "success", "کاربر گرامی");
                                LoadDatatable("#color_tb", false);
                            }
                            else {
                                if (isDevelopment) {
                                    alert(result.data);
                                } else {
                                    ToastMessage("خطایی رخ داده است", "warning", "خطا");
                                }
                            }
                        });
                }
            },
            deleteall: {
                text: 'حذف در تمام زبان ها',
                btnClass: 'btn-red',
                action: function () {
                   
                    $.get("/Admin/Colors?handler=Delete", { id: Id, colorId: colorId, status: "all" },
                        function (result) {
                            if (result == "ok") {
                                ToastMessage("رنگ در تمام زبان ها با موفقیت حذف گردید", "success", "کاربر گرامی");
                                LoadDatatable("#color_tb", false);
                            }
                            else {
                                if (isDevelopment) {
                                    alert(result.data);
                                } else {
                                    ToastMessage("خطایی رخ داده است", "warning", "خطا");
                                }
                            }
                        });
                }
            },
             cancel: {
                text: 'انصراف',

            }
        },
    });
});

$('#excel').on('click', function () {
    tableToExcel('color_tb', "test");
});

$("#print").on('click', function () {
    printHtml('color_tb');
});



