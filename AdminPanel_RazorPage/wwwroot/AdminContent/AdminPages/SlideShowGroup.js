﻿var isDevelopment = true;

function createSlideShowGroup() {
    $.get("/Admin/SlideShows?handler=CreateGroup", function (result) {
        $(".modal-dialog").css('max-width', '55%');
        $(".modal-header").css('background-color', '#008000b8');
        $(".headerModalColor").text("افزودن گروه");
        $("#myModal").modal();
        $("#myModalBody").html(result);
    });
}

function insertSlideShowGroup() {
    var frm = $("#slideShowGroupFrm").valid();
    if (frm == true) {

        $("#btnInsertSlideShowGroup").children(".fa-spinner").removeClass('hide');
        $("#btnInsertSlideShowGroup").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');

        var isactive = $('#IsActive:checked').length;
        var status = "";
        if (isactive === 1) { status = "true"; }
        else { status = "false"; }

        var slideShowGroup = {
            ImageWidth: $("#ImageWidth").val(),
            ImageHeight: $("#ImageHeight").val(),
            MediumPicWidth: $("#MediumPicWidth").val(),
            MediumPicHeight: $("#MediumPicHeight").val(),
            SmallPicWidth: $("#SmallPicWidth").val(),
            SmallPicHeight: $("#SmallPicHeight").val(),
            IsActive: status,
        }
        $.ajax({
            method: 'POST',
            url: '/Admin/SlideShows?handler=CreateGroup',
            data: {
                slideShowGroup: slideShowGroup,
                __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
            },
            success: function (result) {
                $(".fa-spinner").addClass('hide');
                $(".fa-spinner").siblings('span').text('ثبت');

                if (result.status == "ok") {

                    $(".insert-div").addClass('hide');
                    $(".update-div").removeClass('hide');

                    $("#SlideShowGroup_ID").val(result.groupId);
                    createSlideShowGroupTranslation(result.groupId);
                }
                else {
                    if (isDevelopment) {
                        alert(result.data);
                    } else {
                        ToastMessage("خطایی رخ داده است", "info", "خطا");
                    }
                }
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}

function createSlideShowGroupTranslation(groupId) {
    $.get("/Admin/SlideShows?handler=CreateGroupTrans", { id: groupId }, function (result) {
        $("#slideShowGroup_trans").html(result);
        $("#tab_1").removeClass("active");
        $("#slideShowGroup_trans").addClass("active");

        $("a[href='#tab_1']").removeClass('active');
        $("a[href='#slideShowGroup_trans']").addClass('active');
    });
}

function insertSlideShowGroupTranslation() {
    var frm = $("#slideShowGroupTransFrm").valid();
    if (frm == true) {
        if ($("#language_id").val() != "0") {

            $("#btnInsertSlideShowGroupTrans").children(".fa-spinner").removeClass('hide');
            $("#btnInsertSlideShowGroupTrans").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');

         
            var slideShowGroupTranslation = {
                SlideShowGroup_Id: $("#SlideShowGroup_Id").val(),
                Title: $("#Title").val(),
                Language_Id: $("#language_id").val()
            }
            $.ajax({
                method: 'POST',
                url: '/Admin/SlideShows?handler=CreateGroupTrans',
                data: {
                    slideShowGroupTranslation: slideShowGroupTranslation,
                    __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
                },
                success: function (result) {
                    $("#btnInsertSlideShowGroupTrans").children(".fa-spinner").addClass('hide');
                    $("#btnInsertSlideShowGroupTrans").children(".fa-spinner").siblings('span').text('ثبت');

                    $(".insert-div").removeClass('hide');
                    $(".update-div").addClass('hide');

                    if (result == "ok") {
                        resetForm();
                        ToastMessage("گروه اسلایدشو با موفقیت ثبت گردید", "success", "کاربر گرامی");
                        LoadDatatable("#slideshowGroup_tb", true);
                    }
                    else if (result == "exsist") {
                        ToastMessage("این گروه برای این زبان ثبت شده است", "info", "کاربر گرامی");
                    }
                    else {
                        if (isDevelopment) {
                            alert(result.data);
                        } else {
                            ToastMessage("خطایی رخ داده است", "warning", "خطا");
                        }
                    }
                },
                error: function (result) {
                }
            });
        }
        else {
            ToastMessage(" لطفا یک زبان را انتخاب کنید", "info", "کاربر گرامی");
        }
    }
    else {
        return false;
    }
}

function viewAllSlideShowGroups() {
    loadingPageShow();
    $("#slideshowGroup_tb").DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.24/i18n/Persian.json"
        },
        "processing": true,
        "serverSide": true,
        "filter": true,
        "ajax": {
            "url": "/Admin/SlideShows?handler=ViewAllGroups",
            "type": "POST",
            "datatype": "json",
            "data": { __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val() },
        },

        "columns": [
            { "data": "title", "name": "Title" },
            {
                data: "slideShowGroup.isActive",
                render: function (data) {
                    if (data) {
                        return '<span class="text-success">فعال</span >';
                    }
                    else {
                        return '<span class="text-danger">غیر فعال</span >';
                    }
                },
            },
            {
                mRender: function (data, type, row) {
                    return '<div class="btn-group" role="group">' +
                        '<span class="btn btn-outline-info" onclick="editSlideShowGroup(' + row.slideShowGroup_Id + ',' + row.slideShowGroupTranslation_ID + ')"><i class="fa fa-edit fa-sm"></i></span>' +
                        '<span class="btn btn-outline-danger" onclick="deleteSlideShowGroup(' + row.slideShowGroup_Id + ',' + row.slideShowGroupTranslation_ID + ')"><i class="fa fa-trash fa-sm"></i></span></div>';
                }
            },
        ]

    });
    loadingPageHide();
}

function editSlideShowGroup(slideShowGroupId,translationId) {
    $.get("/Admin/SlideShows?handler=EditGroup", { id: slideShowGroupId }, function (result) {
        if (result.status == "error") {
            if (isDevelopment) {
                alert(result.data);
            } else {
                ToastMessage("خطایی رخ داده است", "info", "خطا");
            }
        }
        $(".modal-dialog").css('max-width', '55%');
        $(".modal-header").css('background-color', '#008074b8');
        $(".headerModalColor").text("ویرایش گروه");
        $("#myModal").modal();
        $("#myModalBody").html(result);
        editSlideShowGroupTranslation(translationId);
    });
}


function editSlideShowGroupTranslation(translationId) {
    $.get("/Admin/SlideShows?handler=EditGroupTrans", { id: translationId }, function (res) {
        $("#slideShowGroup_trans").html(res);
    });
}

function getGroupTransInfoWithLangId(langId, groupId) {

    $.get("/Admin/SlideShows?handler=GroupTransInfo", { id: langId, slideShowGroupId: groupId }, function (res) {
        if (res.status == "ok") {
            $(".update_div").removeClass('hide');
            $(".insert_div").addClass('hide');

            $("#Title").val(res.title);
            $("#SlideShowGroupTranslation_ID").val(res.id);
        }
        else {
            $("#Title").val('');
            $(".update_div").addClass('hide');
            $(".insert_div").removeClass('hide');
        }
    });
}

function updateSlideShowGroup() {
    var frm = $("#slideShowGroupFrm").valid();
    if (frm == true) {

        $("#btnUpdateSlideShowGroup").children(".fa-spinner").removeClass('hide');
        $("#btnUpdateSlideShowGroup").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');

        var isactive = $('#IsActive:checked').length;
        var status = "";
        if (isactive === 1) { status = "true"; }
        else { status = "false"; }

        var slideShowGroup = {
            SlideShowGroup_ID: $("#SlideShowGroup_ID").val(),
            ImageWidth: $("#ImageWidth").val(),
            ImageHeight: $("#ImageHeight").val(),
            MediumPicWidth: $("#MediumPicWidth").val(),
            MediumPicHeight: $("#MediumPicHeight").val(),
            SmallPicWidth: $("#SmallPicWidth").val(),
            SmallPicHeight: $("#SmallPicHeight").val(),
            IsActive: status,
        }
        $.ajax({
            method: 'POST',
            url: '/Admin/SlideShows?handler=EditGroup',
            data: {
                slideShowGroup: slideShowGroup,
                __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
            },
            success: function (result) {
                $("#btnUpdateSlideShowGroup").children(".fa-spinner").addClass('hide');
                $("#btnUpdateSlideShowGroup").children(".fa-spinner").siblings('span').text('ویرایش');

                if (result == "ok") {
                    LoadDatatable("#slideshowGroup_tb", false);
                    ToastMessage("اطلاعات مشترک گروه اسلایدشو با موفقیت ویرایش گردید", "success", "کاربر گرامی");
                }
                else {
                    if (isDevelopment) {
                        alert(result.data);
                    } else {
                        ToastMessage("خطایی رخ داده است", "info", "خطا");
                    }
                }
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}

function updateSlideShowGroupTranslation() {
    var frm = $("#slideShowGroupTransFrm").valid();
    if (frm == true) {
        if ($("#language_id").val() != "0") {

            $("#btnUpdateSlideShowGroupTrans").children(".fa-spinner").removeClass('hide');
            $("#btnUpdateSlideShowGroupTrans").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');

           
            var slideShowGroupTranslation = {
                SlideShowGroupTranslation_ID: $("#SlideShowGroupTranslation_ID").val(),
                SlideShowGroup_Id: $("#SlideShowGroup_Id").val(),
                Title: $("#Title").val(),
                Language_Id: $("#language_id").val()
            }
            $.ajax({
                method: 'POST',
                url: '/Admin/SlideShows?handler=EditGroupTrans',
                data: {
                    slideShowGroupTranslation: slideShowGroupTranslation,
                    __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
                },
                success: function (result) {
                    $(".fa-spinner").addClass('hide');
                    $(".fa-spinner").siblings('span').text('ویرایش');
                  
                    if (result == "ok") {
                        resetForm();
                        ToastMessage("گروه اسلایدشو با موفقیت ویرایش گردید", "success", "کاربر گرامی");
                        LoadDatatable("#slideshowGroup_tb", true);
                    }

                    else {
                        if (isDevelopment) {
                            alert(result.data);
                        } else {
                            ToastMessage("خطایی رخ داده است", "warning", "خطا");
                        }
                    }
                },
                error: function (result) {
                }
            });
        }
        else {
            ToastMessage(" لطفا یک زبان را انتخاب کنید", "info", "کاربر گرامی");
        }
    }
    else {
        return false;
    }
}

function deleteSlideShowGroup(groupId, translationId) {
    $.confirm({
        title: 'آیا برای حذف گروه اسلایدشو مطمئن هستید؟', icon: 'fa fa-warning', theme: "material", animation: 'scale', content: '',
        buttons: {
            confirm: {
                text: 'تایید',
                btnClass: 'btn-blue',
                action: function () {
                    $.get("/Admin/SlideShows?handler=DeleteGroup", { id: translationId, groupId: groupId, status: "one" },
                        function (result) {
                            if (result == "ok") {
                                ToastMessage("گروه اسلایدشو با موفقیت حذف گردید", "success", "کاربر گرامی");
                                LoadDatatable("#slideshowGroup_tb", false);
                            }
                            else if (result == "exists") {
                                ToastMessage("برای این گروه اسلایدشو تعریف شده است", "warning", "خطا");
                            }
                            else {
                                if (isDevelopment) {
                                    alert(result.data);
                                } else {
                                    ToastMessage("خطایی رخ داده است", "warning", "خطا");
                                }
                            }
                        });
                }
            },
           
            cancel: {
                text: 'انصراف',

            }
        },
    });
}

function resetForm() {
    $("#Title").val('');
    $("#language_id").val('0');
}