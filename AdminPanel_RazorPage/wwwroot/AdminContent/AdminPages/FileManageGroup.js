﻿var isDevelopment = true;

var fileManageGroupIdForParentId = 0;
var fileManageGroupIdForParentIdEdit = 0;
var fileManageGroupTransId = 0;
var fileManageGroupTransIdEdit = 0;
///TreeView For FileManageGroup
function getAllFilesManageGroupForTreeView(lang, status) { //خواندن گروه ها برای تری ویو ها
    $.ajax({
        method: 'GET',
        url: '/Admin/FilesManage?handler=CreateTreeView',
        data: { lang: lang },
        success: function (response) {

            $('#fileManageGroup_tw').treeview({  //Create FileManageGroup

                data: response,
                expandIcon: 'fa fa-plus-square',
                collapseIcon: 'fa fa-minus-square',
                onNodeSelected: function (event, node) {
                    fileManageGroupIdForParentId = node.tags;
                }

            });
            if (status != "dontShow") {
                $('#fileManageGroup2_tw').treeview({   //index FileManage
                    data: response,
                    expandIcon: 'fa fa-plus-square',
                    collapseIcon: 'fa fa-minus-square',
                    onNodeSelected: function (event, node) {
                        viewDetailsFileManageGroup(node.tags);
                    },
                    //
                });
            }

            $('#fileManageGroup3_tw').treeview({   //Edit FileManageGroup
                data: response,
                expandIcon: 'fa fa-plus-square',
                collapseIcon: 'fa fa-minus-square',
                onNodeSelected: function (event, node) {
                    fileManageGroupIdForParentIdEdit = node.tags;
                },
            });

            $('#fileManageGroup4_tw').treeview({   //add file
                data: response,
                expandIcon: 'fa fa-plus-square',
                collapseIcon: 'fa fa-minus-square',

                onNodeSelected: function (event, node) {
                    var fgtId = node.href;
                    fgtId = fgtId.replace("#", "");
                    fileManageGroupTransId = parseInt(fgtId);
                },

            });

            if (status != "dontShow") {
                $('#fileManageGroup5_tw').treeview({   //view all files
                    data: response,
                    expandIcon: 'fa fa-plus-square',
                    collapseIcon: 'fa fa-minus-square',
                    onNodeSelected: function (event, node) {
                        var fgtId = node.href;
                        fgtId = fgtId.replace("#", "");
                        viewAllFileManages(parseInt(fgtId));
                    },
                });
            }

            $('#fileManageGroup6_tw').treeview({   //edit file
                data: response,
                expandIcon: 'fa fa-plus-square',
                collapseIcon: 'fa fa-minus-square',

                onNodeSelected: function (event, node) {
                    var fgtId = node.href;
                    fgtId = fgtId.replace("#", "");
                    fileManageGroupTransIdEdit = parseInt(fgtId);

                },

            });
        },
        error: function (response) {

        }
    });
}

function createFileManageGroup() {
    $.get("/Admin/FilesManage?handler=CreateGroup", function (result) {
        if (result.status == "error") {
            if (isDevelopment) {
                alert(result.data);
            } else {
                ToastMessage("خطایی رخ داده است", "info", "خطا");
            }
        }
        $(".modal-dialog").css('max-width', '75%');
        $(".modal-header").css('background-color', '#008000b8');
        $(".headerModalColor").text("افزودن گروه فایل ها");
        $("#myModal").modal();
        $("#myModalBody").html(result);
    });
}

function insertFileManageGroup() {
    var frm = $("#fileManageGroupFrm").valid();
    if (frm == true) {

        if (fileManageGroupIdForParentId != 0) {
            $("#btnInsertFileManageGroup").children(".fa-spinner").removeClass('hide');
            $("#btnInsertFileManageGroup").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');

            var isactive = $('#IsActive:checked').length;
            var status = "";
            if (isactive === 1) { status = "true"; }
            else { status = "false"; }

            var fileManageGroup = {
                Parent_Id: fileManageGroupIdForParentId,
                IsActive: status,

            }
            $.ajax({
                method: 'POST',
                url: '/Admin/FilesManage?handler=CreateGroup',
                data: {
                    fileManageGroup: fileManageGroup,
                    __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
                },
                success: function (result) {
                    $("#btnInsertFileManageGroup").children(".fa-spinner").addClass('hide');
                    $("#btnInsertFileManageGroup").children(".fa-spinner").siblings('span').text('ویرایش');

                    if (result.status == "ok") {

                        $(".insert-div").addClass('hide');
                        $(".update-div").removeClass('hide');

                        $("#FileManageGroup_ID").val(result.groupId);
                        createFileManageGroupTranslation(result.groupId);
                    }
                    else {
                        if (isDevelopment) {
                            alert(result.data);
                        } else {
                            ToastMessage("خطایی رخ داده است", "info", "خطا");
                        }
                    }
                },
                error: function (result) {
                }
            });
        }
        else {
            ToastMessage(" لطفا یک گروه را انتخاب کنید", "info", "کاربر گرامی");
        }
    }
    else {
        return false;
    }
}

function createFileManageGroupTranslation(Id) {
    $.get("/Admin/FilesManage?handler=CreateFileManageGroupTrans", { id: Id }, function (result) {
        $("#fileManageGroup_trans").html(result);

        $("#tab_1").removeClass("active");
        $("#fileManageGroup_trans").addClass("active");

        $("a[href='#tab_1']").removeClass('active');
        $("a[href='#fileManageGroup_trans']").addClass('active');
    });
}

function insertFileManageGroupTranslation() {
    var frm = $("#fileManageGroupTransFrm").valid();
    if (frm == true) {
        if ($("#language_id").val() != "0") {

            $("#btnInsertFileManageGroupFinal").children(".fa-spinner").removeClass('hide');
            $("#btnInsertFileManageGroupFinal").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');
            var fileManageGroupTranslation = {
                Title: $("#Title").val(),
                FileManageGroup_Id: $("#FileManageGroup_Id").val(),
                Slug: $("#Slug").val(),
                Language_Id: $("#language_id").val(),
                parentId: fileManageGroupIdForParentId,
            }
            $.ajax({
                method: 'POST',
                url: '/Admin/FilesManage?handler=CreateFileManageGroupTrans',
                data: {
                    fileManageGroupTranslation: fileManageGroupTranslation,
                    __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
                },
                success: function (result) {
                    $(".fa-spinner").addClass('hide');
                    $(".fa-spinner").siblings('span').text('ثبت');

                    $(".insert-div").removeClass('hide');
                    $(".update-div").addClass('hide');

                    if (result.status == "ok") {
                        resetForm();
                        ToastMessage("گروه فایل ها با موفقیت ثبت گردید", "success", "کاربر گرامی");

                        getAllFilesManageGroupForTreeView($("#global_language_id").val());
                    }
                    else if (result == "exsist") {
                        ToastMessage(" این گروه  برای این زبان قبلا ثبت شده است", "info", "کاربر گرامی");
                    }
                    else {
                        if (isDevelopment) {
                            alert(result.data);
                        } else {
                            ToastMessage("خطایی رخ داده است", "warning", "خطا");
                        }
                    }
                },
                error: function (result) {
                }
            });
        }
        else {
            ToastMessage(" لطفا یک زبان را انتخاب کنید", "info", "کاربر گرامی");
        }
    }
    else {
        return false;
    }
}

function viewDetailsFileManageGroup(group_id) {
    $.get("/Admin/FilesManage?handler=ViewDetailsFileManageGroup", { groupId: group_id }, function (response) {
        $("#fileManageGroupDetails").html(response);
    });
}

function editFileManageGroup(fileManageGroupId, Id) {
    $.get("/Admin/FilesManage?handler=EditFileManageGroup", { id: fileManageGroupId }, function (result) {
        if (result.status == "error") {
            if (isDevelopment) {
                alert(result.data);
            } else {
                ToastMessage("خطایی رخ داده است", "info", "خطا");
            }
        }
        else {

            $(".modal-dialog").css('max-width', '75%');
            $(".modal-header").css('background-color', '#008074b8');
            $(".headerModalColor").text("ویرایش گروه فایل ها");
            $("#myModal").modal();
            $("#myModalBody").html(result);
            editFileManageGroupTranslation(Id);

        }
    });
};

function editFileManageGroupTranslation(Id) {
    $.get("/Admin/FilesManage?handler=EditFileManageGroupTrans", { id: Id }, function (res) {
        if (result.status == "error") {
            if (isDevelopment) {
                alert(result.data);
            } else {
                ToastMessage("خطایی رخ داده است", "info", "خطا");
            }
        }
        else {
            $("#fileManageGroup_trans").html(res);
            //getFileManageGroupInfoWithLanguage('', Id);
        }
    });
}

function getFileManageGroupInfoWithLanguage(langId, fileManageGroupId) {

    $.get("/Admin/FilesManage?handler=FileManageGroupTransInfo", { id: langId, fileManageGroupId: fileManageGroupId }, function (res) {
        if (res.status == "ok") {
            $(".update_div").removeClass('hide');
            $(".insert_div").addClass('hide');

            $("#Title").val(res.title);
            $("#Slug").val(res.slug);
            $("#Title").attr('data-id', res.id);
        }
        else {
            resetForm2();
            $(".update_div").addClass('hide');
            $(".insert_div").removeClass('hide');
        }
    });
}

function updateFileManageGroup() {
    var frm = $("#fileManageGroupFrm").valid();
    if (frm == true) {

        $("#btnUpdateFileManageGroup").children(".fa-spinner").removeClass('hide');
        $("#btnUpdateFileManageGroup").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');

        var isactive = $('#IsActive:checked').length;
        var status = "";
        if (isactive === 1) { status = "true"; }
        else { status = "false"; }
        var parentId = 0;
        var file_group_id = $("#FileManageGroup_ID").val();
        if (fileManageGroupIdForParentIdEdit != 0) {
            parentId = fileManageGroupIdForParentIdEdit;
        }
        else {
            parentId = $("#Parent_Id").val();
        }
        var fileManageGroup = {
            FileManageGroup_ID: file_group_id,
            Parent_Id: parentId,
            IsActive: status,
        }


        $.ajax({
            method: 'POST',
            url: '/Admin/FilesManage?handler=EditFileManageGroup',
            data: {
                fileManageGroup: fileManageGroup,
                __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
            },
            success: function (result) {
                $("#btnUpdateFileManageGroup").children(".fa-spinner").addClass('hide');
                $("#btnUpdateFileManageGroup").children(".fa-spinner").siblings('span').text('ویرایش');

                if (result == "ok") {
                    viewDetailsFileManageGroup(file_group_id);
                    switchTab();
                }
                else {
                    if (isDevelopment) {
                        alert(result.data);
                    } else {
                        ToastMessage("خطایی رخ داده است", "info", "خطا");
                    }
                }
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}

function updateFileManageGroupTranslation() {
    var frm = $("#fileManageGroupTransFrm").valid();
    if (frm == true) {
        if ($("#language_id").val() != "0") {
            $("#btnUpdateFileManageGroupTrans").children(".fa-spinner").removeClass('hide');
            $("#btnUpdateFileManageGroupTrans").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');

            var file_group_id = $("#FileManageGroup_Id").val();

            if (fileManageGroupIdForParentIdEdit != 0) {
                parentId = fileManageGroupIdForParentIdEdit;
            }
            else {
                parentId = $("#Parent_Id").val();
            }

            var fileManageGroupTranslation = {
                FileManageGroupTranslation_ID: $("#Title").attr('data-id'),
                Title: $("#Title").val(),
                FileManageGroup_Id: file_group_id,
                Slug: $("#Slug").val(),
                Language_Id: $("#language_id").val(),
                SiteMap_Id: $("#SiteMap_Id").val(),
                ParentId: parentId,
            }

            $.ajax({
                method: 'POST',
                url: '/Admin/FilesManage?handler=EditFileManageGroupTrans',
                data: {
                    fileManageGroupTranslation: fileManageGroupTranslation,
                    __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
                },
                success: function (result) {
                    $("#btnUpdateFileManageGroupTrans").children(".fa-spinner").addClass('hide');
                    $("#btnUpdateFileManageGroupTrans").children(".fa-spinner").siblings('span').text('ویرایش');

                    if (result == "ok") {
                        resetForm();
                        ToastMessage("گروه فایل ها با موفقیت ویرایش گردید", "success", "کاربر گرامی");
                        viewDetailsFileManageGroup(file_group_id);
                        getAllFilesManageGroupForTreeView($("#language_id").val());
                    }
                    //else if (result == "exsist") {
                    //    ToastMessage(" این کد رنگ برای این زبان قبلا ثبت شده است", "info", "کاربر گرامی");
                    //}
                    else {
                        if (isDevelopment) {
                            alert(result.data);
                        } else {
                            ToastMessage("خطایی رخ داده است", "warning", "خطا");
                        }
                    }
                },
                error: function (result) {
                }
            });
        }
        else {
            ToastMessage(" لطفا یک زبان را انتخاب کنید", "info", "کاربر گرامی");
        }

    }
    else {
        return false;
    }
}

function deleteFileManageGroup(fileManageGroupId, Id) {
    $.confirm({
        title: 'آیا برای حذف گروه فایل ها مطمئن هستید؟', icon: 'fa fa-warning', theme: "material", animation: 'scale', content: '',
        buttons: {
            confirm: {
                text: 'تایید',
                btnClass: 'btn-blue',
                action: function () {
                    $.get("/Admin/FilesManage?handler=DeleteFileManageGroup", { id: Id, fileManageGroupId: fileManageGroupId, status: "one" },
                        function (result) {
                            if (result == "ok") {
                                ToastMessage("گروه فایل ها با موفقیت حذف گردید", "success", "کاربر گرامی");
                                //$("#fileManageGroup_tb").empty();
                                getAllFilesManageGroupForTreeView($("#global_language_id").val());
                            }
                            else if (result == "exists") {
                                ToastMessage("برای این گروه فایل  ثبت شده است", "warning", "کاربر گرامی");
                            }
                            else {
                                if (isDevelopment) {
                                    alert(result.data);
                                } else {
                                    ToastMessage("خطایی رخ داده است", "warning", "خطا");
                                }
                            }
                        });
                }
            },
            cancel: {
                text: 'انصراف',

            }
        },
    });
}

function switchTab() {
    $("#tab_1").removeClass("active");
    $("#fileManageGroup_trans").addClass("active");

    $("a[href='#tab_1']").removeClass('active');
    $("a[href='#fileManageGroup_trans']").addClass('active');
}

function resetForm() {
    $("#language_id").val('0');
    $("#Title").val('');
    $("#Slug").val('');
}

function resetForm2() {
    $("#Title").val('');
    $("#Slug").val('');
}

function viewAllFileManages(fileManageGroupTransId) {
    loadingPageShow();
    $("#fileManage_tb").DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.24/i18n/Persian.json"
        },
        "processing": true,
        "serverSide": true,
        "filter": true,
        "destroy": true,   /// allow reinitialize datattable
        "ajax": {
            "url": "/Admin/FilesManage?handler=ViewAllFilesManage",
            "type": "POST",
            "datatype": "json",
            "data":
            {
                fileManageGroupTranslationId: fileManageGroupTransId,
                __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
            },
        },

        "columns": [
            { "data": "title", "name": "title", "autoWidth": true },
            {
                data: "fileManage.isActive",
                render: function (data) {
                    if (data) {
                        return '<span class="text-success tooltips">فعال</span >';
                    }
                    else {
                        return '<span class="text-danger">غیر فعال</span >';
                    }
                },
            },
            {
                data: "fileManage.fileData",
                render: function (data) {
                    if (validURL(data))
                        return "<a href='" + data + "' target='_blank' class='tooltips'  title='" + data + "' data-toggle='tooltip' data-placement='top'>..." + data.substring(0, 20) + "</a>"
                    else if (data.includes("Files/FilesManage"))
                        return '---';
                    else {
                        return "<a class='btnShowFile' title='" + data + "'><span class='icon-eye'></span> مشاهده فایل</a>";
                    }
                },
            },
            {
                data: "fileManage.fileSize",
                render: function (data) {
                    if (data != "0") {
                        return data;
                    }
                    else {
                        return '---';
                    }
                },
            },
            {
                data: "fileManage.createDate",
                render: function (data) {
                    return new persianDate(new Date(data)).format("L");

                },
            },
            {
                mRender: function (data, type, row) {
                    return '<div class="btn-group" role="group">' +
                        '<span class="btn btn-outline-info" onclick="editFileManage(' + row.fileManage_Id + ',' + row.fileManageTranslation_ID + ')"><i class="fa fa-edit fa-sm"></i></span>' +
                        //'<span class="btn btn-outline-success" onclick="getFileManageStatistics(' + row.fileManageTranslation_Id + ')"><i class="fa fa-bar-chart fa-sm"></i></span>' +
                        '<span class="btn btn-outline-danger" onclick="deleteFileManage(' + row.fileManage_Id + ',' + row.fileManageTranslation_ID + ')"><i class="fa fa-trash fa-sm"></i></span></div>';
                }
            },
        ]

    });
    loadingPageHide();

}
function validURL(str) {
    var pattern = new RegExp('^(https?:\\/\\/)?' + // protocol
        '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
        '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
        '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
        '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
        '(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator
    return !!pattern.test(str);
}