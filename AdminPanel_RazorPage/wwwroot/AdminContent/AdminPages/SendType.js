﻿var isDevelopment = true;
function createSendType() {
    $.get("/Admin/SendTypes?handler=CreateSendType", function (result) {
        if (result.status == "error") {
            if (isDevelopment) {
                alert(result.data);
            } else {
                ToastMessage("خطایی رخ داده است", "info", "خطا");
            }
        }
        $(".modal-dialog").css('max-width', '65%');
        $(".modal-header").css('background-color', '#008000b8');
        $(".headerModalColor").text("افزودن روش ارسال");
        $("#myModal").modal();
        $("#myModalBody").html(result);
    });
}

function setSendTypeImage(typeId, imageSelector) {

    var labelClassName = "";
    switch (typeId) {

        case "1":
            labelClassName = "fa fa-truck";
            break;
        case "2":
            labelClassName = "fa fa-motorcycle";
            break;
        case "3":
            labelClassName = "fa fa-home";
            break;
        case "4":
            labelClassName = "fa fa-fighter-jet";
            break;
        default:
            labelClassName = "nothing";
    }
    if (labelClassName != "nothing") {
        $(imageSelector).attr('class', '');
        $(imageSelector).addClass(labelClassName);
    }
    else {
        $(imageSelector).attr('class', '');
        //$("#Image").text("آیتمی انتخاب نشده است");
    }

   
}

function getSendTypeTitle(typeTitle,status) {
    
    var typeId = "";
    var labelClassName = "";
    switch (typeTitle) {
        case "ماشینی":
            typeId = "1";
            labelClassName = "fa fa-truck";
            break;
        case "موتوری":
            typeId = "2";
            labelClassName = "fa fa-motorcycle";
            break;
        case "حضوری":
            typeId = "3";
            labelClassName = "fa fa-home";
            break;
        case "پیشتاز":
            typeId = "4";
            labelClassName = "fa fa-fighter-jet";
            break;
        default:
    }
    if (status != "getClass")
        return typeId;
    else
        return labelClassName;
}

function insertSendType() {
    var frm = $("#sendTypeFrm").valid();
    if (frm == true) {
        if ($("#Type").val() != "0") {
            $("#btnInsertSendType").children(".fa-spinner").removeClass('hide');
            $("#btnInsertSendType").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');
            var sendType = {
                Type: $("#Type option:selected").text(),
            }
            $.ajax({
                method: 'POST',
                url: '/Admin/SendTypes?handler=CreateSendType',
                data: {
                    sendType: sendType,
                    __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
                },
                success: function (result) {
                    $("#btnInsertSendType").children(".fa-spinner").addClass('hide');
                    $("#btnInsertSendType").children(".fa-spinner").siblings('span').text('ویرایش');

                    if (result.status == "ok") {

                        $(".insert-div").addClass('hide');
                        $(".update-div").removeClass('hide');
                        $("#SendType_ID").val(result.sendTypeId);
                        createSenTypeTranslation(result.sendTypeId);
                    }
                    else if (result == "exsist") {
                        ToastMessage(" این روش ارسال قبلا ثبت شده است", "info", "کاربر گرامی");
                    }
                    else {
                        if (isDevelopment) {
                            alert(result.data);
                        } else {
                            ToastMessage("خطایی رخ داده است", "info", "خطا");
                        }
                    }
                },
                error: function (result) {
                }
            });
        }
        else {
            ToastMessage(" لطفا روش ارسال انتخاب کنید", "info", "کاربر گرامی");
        }
    }
    else {
        return false;
    }
}

function createSenTypeTranslation(Id) {
    $.get("/Admin/SendTypes?handler=CreateSendTypeTrans", { id: Id }, function (result) {
        $("#sendType_trans").html(result);
        $("#tab_1").removeClass("active");
        $("#sendType_trans").addClass("active");

        $("a[href='#tab_1']").removeClass('active');
        $("a[href='#sendType_trans']").addClass('active');
    });
}

function insertSendTypeTranslation() {
    var frm = $("#sendTypeTransFrm").valid();
    if (frm == true) {
        if ($("#language_id").val() != "0") {

            $("#btnInsertSendTypeTrans").children(".fa-spinner").removeClass('hide');
            $("#btnInsertSendTypeTrans").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');

            var sendTypeTranslation = {
                SendType_Id: $("#SendType_Id").val(),
                Title: $("#Title").val(),
                Price: $("#Price").val(),
                Description: $("#Description").val(),
                Language_Id: $("#language_id").val(),
            }
            $.ajax({
                method: 'POST',
                url: '/Admin/SendTypes?handler=CreateSendTypeTrans',
                data: {
                    sendTypeTranslation: sendTypeTranslation,
                    __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
                },
                success: function (result) {
                    $(".fa-spinner").addClass('hide');
                    $(".fa-spinner").siblings('span').text('ثبت');

                    $(".insert-div").removeClass('hide');
                    $(".update-div").addClass('hide');

                    if (result == "ok") {
                        resetForm();
                        ToastMessage("روش ارسال با موفقیت ثبت گردید", "success", "کاربر گرامی");
                        LoadDatatable("#sendType_tb", true);
                    }
                    else if (result == "exsist") {
                        ToastMessage(" این روش ارسال  برای این زبان قبلا ثبت شده است", "info", "کاربر گرامی");
                    }
                    else {
                        if (isDevelopment) {
                            alert(result.data);
                        } else {
                            ToastMessage("خطایی رخ داده است", "warning", "خطا");
                        }
                    }
                },
                error: function (result) {
                }
            });
        }
        else {
            ToastMessage(" لطفا یک زبان را انتخاب کنید", "info", "کاربر گرامی");
        }
    }
    else {
        return false;
    }
}

function viewAllSendTypes() {
    loadingPageShow();
    $("#sendType_tb").DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.24/i18n/Persian.json"
        },
        "processing": true,
        "serverSide": true,
        "filter": true,
        "ajax": {
            "url": "/Admin/SendTypes?handler=ViewAllSendTypes",
            "type": "POST",
            "datatype": "json",
            "data": { __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val() },
        },
        
        "columns": [
            { "data": "sendType.type", "name": "sendType.Type", "autoWidth": true },
            
            {
                data: "title",
                render: function (data) {
                    var title = "";
                    if (data != undefined)
                        title = data
                    else
                        title = "---";

                    return title;
                },
            },
            {
                data: "sendType.type",
                mRender: function (data, type, row) {
                    return '<span class="' + getSendTypeTitle(row.sendType.type, "getClass") + '"></span >';
                }
            },
            {
                data: "price",
                render: function (data) {
                    var price = "";
                    if (data != undefined) {
                        price = data;
                        return price.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",") + " تومان ";
                    }
                    else
                        return "---";
                },
            },
            {
                mRender: function (data, type, row) {
                    return '<div class="btn-group" role="group">' +
                        '<span class="btn btn-outline-info" onclick="editSendType(' + row.sendType_Id + ',' + row.sendTypeTranslation_ID + ')"><i class="fa fa-edit fa-sm"></i></span>' +
                        '<span class="btn btn-outline-danger" onclick="deleteSendType(' + row.sendType_Id + ',' + row.sendTypeTranslation_ID + ')"><i class="fa fa-trash fa-sm"></i></span></div>';
                }
            },
        ]

    });
    loadingPageHide();
}

function editSendType(sendTypeId, translationId) {
    $.get("/Admin/SendTypes?handler=EditSendType", { id: sendTypeId }, function (result) {
        if (result.status == "error") {
            if (isDevelopment) {
                alert(result.data);
            } else {
                ToastMessage("خطایی رخ داده است", "info", "خطا");
            }
        }
        else {
            $(".modal-dialog").css('max-width', '65%');
            $(".modal-header").css('background-color', '#008074b8');
            $(".headerModalColor").text("ویرایش روش ارسال");
            $("#myModal").modal();
            $("#myModalBody").html(result);
            editSendTypeTranslation(translationId, sendTypeId);
        }
    });
}

function editSendTypeTranslation(Id, sendTypeId) {
    $.get("/Admin/SendTypes?handler=EditSendTypeTrans", { id: Id }, function (res) {
        $("#sendType_trans").html(res);
        //getSendTypeInfoWithLanguage('', sendTypeId);
    });
}

function getSendTypeInfoWithLanguage(langId, sendTypeId) {

    $.get("/Admin/SendTypes?handler=SendTypeInfo", { id: langId, sendTypeId: sendTypeId }, function (res) {
        if (res.status == "ok") {
            $(".update_div").removeClass('hide');
            $(".insert_div").addClass('hide');

            $("#Title").val(res.title);
            $("#Description").val(res.description);
            $("#Price").val(res.price);
            $("#Title").attr('data-id', res.id);
        }
        else {
            resetForm2();
            $(".update_div").addClass('hide');
            $(".insert_div").removeClass('hide');
        }
    });
}

function updateSendType() {
    var frm = $("#sendTypeFrm").valid();
    if (frm == true) {

        $("#btnUpdateSendType").children(".fa-spinner").removeClass('hide');
        $("#btnUpdateSendType").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');

        var sendType = {
            SendType_ID: $("#SendType_ID").val(),
            Type: $("#Type option:selected").text(),
        }

        $.ajax({
            method: 'POST',
            url: '/Admin/SendTypes?handler=EditSendType',
            data: {
                sendType: sendType,
                __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
            },
            success: function (result) {
                $("#btnUpdateSendType").children(".fa-spinner").addClass('hide');
                $("#btnUpdateSendType").children(".fa-spinner").siblings('span').text('ویرایش');

                if (result == "ok") {
                    LoadDatatable("#sendType_tb", false);
                    ToastMessage("اطلاعات مشترک روش ارسال با موفقیت ویرایش گردید", "success", "کاربر گرامی");
                }
                else {
                    if (isDevelopment) {
                        alert(result.data);
                    } else {
                        ToastMessage("خطایی رخ داده است", "info", "خطا");
                    }
                }
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}

function updateSendTypeTranslation() {
    var frm = $("#sendTypeTransFrm").valid();
    if (frm == true) {
        if ($("#language_id").val() != "0") {
            $("#btnUpdateSendTypeTrans").children(".fa-spinner").removeClass('hide');
            $("#btnInsertSendTypeTrans").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');

            var sendTypeTranslation = {
                SendTypeTranslation_ID: $("#Title").attr('data-id'),
                SendType_Id: $("#SendType_Id").val(),
                Title: $("#Title").val(),
                Price: $("#Price").val(),
                Description: $("#Description").val(),
                Language_Id: $("#language_id").val(),
            }

            $.ajax({
                method: 'POST',
                url: '/Admin/SendTypes?handler=EditSendTypeTrans',
                data: {
                    sendTypeTranslation: sendTypeTranslation,
                    __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
                },
                success: function (result) {
                    $("#btnUpdateSendTypeTrans").children(".fa-spinner").addClass('hide');
                    $("#btnUpdateSendTypeTrans").children(".fa-spinner").siblings('span').text('ویرایش');

                    if (result == "ok") {
                        resetForm();
                        ToastMessage("روش ارسال با موفقیت ویرایش گردید", "success", "کاربر گرامی");
                        LoadDatatable("#sendType_tb", false);
                    }
                    //else if (result == "exsist") {
                    //    ToastMessage(" این کد رنگ برای این زبان قبلا ثبت شده است", "info", "کاربر گرامی");
                    //}
                    else {
                        if (isDevelopment) {
                            alert(result.data);
                        } else {
                            ToastMessage("خطایی رخ داده است", "warning", "خطا");
                        }
                    }
                },
                error: function (result) {
                }
            });
        }
        else {
            ToastMessage(" لطفا یک زبان را انتخاب کنید", "info", "کاربر گرامی");
        }

    }
    else {
        return false;
    }
}

function deleteSendType(sendTypeId, translationId) {
    $.confirm({
        title: 'آیا برای حذف این روش ارسال مطمئن هستید؟', icon: 'fa fa-warning', theme: "material", animation: 'scale', content: '',
        buttons: {
            confirm: {
                text: 'تایید',
                btnClass: 'btn-blue',
                action: function () {
                    $.get("/Admin/SendTypes?handler=DeleteSendTypeTrans", { id: translationId, sendTypeId: sendTypeId, status: "one" },
                        function (result) {
                            if (result == "ok") {
                                ToastMessage("روش ارسال با موفقیت حذف گردید", "success", "کاربر گرامی");
                                LoadDatatable("#sendType_tb", false);
                            }
                            else {
                                if (isDevelopment) {
                                    alert(result.data);
                                } else {
                                    ToastMessage("خطایی رخ داده است", "warning", "خطا");
                                }
                            }
                        });
                }
            },
            deleteall: {
                text: 'حذف در تمام زبان ها',
                btnClass: 'btn-red',
                action: function () {

                    $.get("/Admin/SendTypes?handler=DeleteSendTypeTrans", { id: translationId, sendTypeId: sendTypeId, status: "all" },
                        function (result) {
                            if (result == "ok") {
                                ToastMessage("روش ارسال در تمام زبان ها با موفقیت حذف گردید", "success", "کاربر گرامی");
                                LoadDatatable("#sendType_tb", false);
                            }
                            else {
                                if (isDevelopment) {
                                    alert(result.data);
                                } else {
                                    ToastMessage("خطایی رخ داده است", "warning", "خطا");
                                }
                            }
                        });
                }
            },
            cancel: {
                text: 'انصراف',

            }
        },
    });
}


function resetForm() {
    $("#language_id").val('0');
    $("#Title").val('');
    $("#Price").val('');
    $("#Description").val('');
}
function resetForm2() {
    $("#Title").val('');
    $("#Price").val('');
    $("#Description").val('');
}