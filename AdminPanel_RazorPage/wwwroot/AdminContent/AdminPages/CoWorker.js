﻿var isDevelopment = true;

function createCoWorker() {
    $.get("/Admin/CoWorkers?handler=CreateCoWorker", function (result) {
        if (result.status == "error") {
            if (isDevelopment) {
                alert(result.data);
            } else {
                ToastMessage("خطایی رخ داده است", "info", "خطا");
            }
        }
        $(".modal-dialog").css('max-width', '65%');
        $(".modal-header").css('background-color', '#008000b8');
        $(".headerModalColor").text("افزودن همکاران");
        $("#myModal").modal();
        $("#myModalBody").html(result);
    });
}

function insertCoWorker() {
    var frm = $("#coWorkerFrm").valid();
    if (frm == true) {
        var coWorker = new FormData();
        var files = $("#Image").get(0).files;

        $("#btnInsertCoWorker").children(".fa-spinner").removeClass('hide');
        $("#btnInsertCoWorker").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');

            coWorker.append("imgCoWorker", files[0])

        $.ajax({
            method: 'POST',
            url: '/Admin/CoWorkers?handler=CreateCoWorker',
            contentType: false,
            processData: false,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            data: coWorker,

            success: function (result) {
                $("#btnInsertCoWorker").children(".fa-spinner").addClass('hide');
                $("#btnInsertCoWorker").children(".fa-spinner").siblings('span').text('ویرایش');

                if (result.status == "ok") {

                    $(".insert-div").addClass('hide');
                    $(".update-div").removeClass('hide');

                    $("#CoWorker_ID").val(result.coWorkerId);
                    createCoWorkerTranslation(result.coWorkerId);
                }
                else if (result == "noImage") {
                    ToastMessage("تصویری برای همکاران انتخاب نشده است", "warning", "هشدار");
                }
                else {
                    if (isDevelopment) {
                        alert(result.data);
                    } else {
                        ToastMessage("خطایی رخ داده است", "danger", "خطا");
                    }
                }
            },
            error: function (result) {
            }
        });
        //}
        //else {
        //    ToastMessage("لطفا یک تصویر انتخاب نمایید", "warning", "خطا");
        //}
    }
    else {
        return false;
    }
}

function createCoWorkerTranslation(coWorkerId) {
    $.get("/Admin/CoWorkers?handler=CreateCoWorkerTrans", { id: coWorkerId }, function (result) {
        $("#coWorker_trans").html(result);
        $("#tab_1").removeClass("active");
        $("#coWorker_trans").addClass("active");

        $("a[href='#tab_1']").removeClass('active');
        $("a[href='#coWorker_trans']").addClass('active');
    });
}

function insertCoWorkerTranslation() {
    var frm = $("#coWorkerTransFrm").valid();
    if (frm == true) {
        if ($("#language_id").val() != "0") {

            $("#btnInsertCoWorkerFinal").children(".fa-spinner").removeClass('hide');
            $("#btnInsertCoWorkerFinal").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');

            var isactive = $('#IsActive:checked').length;
            var status = "";
            if (isactive === 1) { status = "true"; }
            else { status = "false"; }

            var coWorkerTranslation = {
                CoWorker_Id: $("#CoWorker_Id").val(),
                Title: $("#Title").val(),
                Text: $("#Text").val(),
                Link: $("#Link").val(),
                TranslationGroup_Id: $("#TranslationGroup_Id").val(),
                Language_Id: $("#language_id").val(),
                IsActive: status
            }
            $.ajax({
                method: 'POST',
                url: '/Admin/CoWorkers?handler=CreateCoWorkerTrans',
                data: {
                    coWorkerTranslation: coWorkerTranslation,
                    __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
                },
                success: function (result) {
                    $(".fa-spinner").addClass('hide');
                    $(".fa-spinner").siblings('span').text('ثبت');

                    $(".insert-div").removeClass('hide');
                    $(".update-div").addClass('hide');

                    if (result == "ok") {
                        resetForm();
                        ToastMessage("همکاران با موفقیت ثبت گردید", "success", "کاربر گرامی");
                        LoadDatatable("#coWorker_tb", true);
                    }
                    else if (result == "exsist") {
                        ToastMessage(" این همکار  برای این زبان قبلا ثبت شده است", "info", "کاربر گرامی");
                    }
                    else {
                        if (isDevelopment) {
                            alert(result.data);
                        } else {
                            ToastMessage("خطایی رخ داده است", "warning", "خطا");
                        }
                    }
                },
                error: function (result) {
                }
            });
        }
        else {
            ToastMessage(" لطفا یک زبان را انتخاب کنید", "info", "کاربر گرامی");
        }
    }
    else {
        return false;
    }
}

function getCoWorkerGroupByLang(langId, status, selectedOption) {
    $.get("/Admin/CoWorkers?handler=CoWorkerGroupByLang", { id: langId }, function (result) {
        var options = "";
        if (status == "edit") {
            options = "<option value=''>انتخاب کنید ...</option>";
        }
        for (var i = 0; i < result.length; i++) {
            options += "<option value='" + result[i].coWorkerGroupTranslation_ID + "'>" + result[i].title + "</option>";
        }
        $("#TranslationGroup_Id").html(options);
        if (status == "edit") {
            $("#TranslationGroup_Id").val(selectedOption);
        }
    });
}

function getCoWorkerImageSize(groupId) {
    $.get("/Admin/CoWorkers?handler=CoWorkerImageSize", { groupId: groupId }, function (res) {
        $("#imgWidth").text(res.imageWidth + " * ");
        $("#imgHeight").text(res.imageHeight);
       
    });
}

function viewAllCoWorkers(GroupId) {
    loadingPageShow();
    $("#coWorker_tb").DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.24/i18n/Persian.json"
        },
        "processing": true,
        "serverSide": true,
        "filter": true,
        "destroy": true,   /// allow reinitialize datattable
        "ajax": {
            "url": "/Admin/CoWorkers?handler=ViewAllCoWorkers",
            "type": "POST",
            "datatype": "json",
            "data": {
                groupId: GroupId,
                __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
            },
        },

        "columns": [
            {
                data: "coWorker.image",
                mRender: function (data, type, row) {
                    return '<img width="60" height="60" title="' + row.title+'" src="/' + row.coWorker.image + '">';
                },
            },
            {
                data: "title",
                mRender: function (data, type, row) {
                    return '<a href="' + row.link + '" target="_blank">' + row.title+'</a>';
                },
                name:"title",
            },
            { "data": "coWorkerGroupTranslation.title", "name": "coWorkerGroupTranslation.title", "autoWidth": true },
            {
                data: "isActive",
                render: function (data) {
                    if (data) {
                        return '<span class="text-success">فعال</span >';
                    }
                    else {
                        return '<span class="text-danger">غیر فعال</span >';
                    }
                }, name: "isActive",
            },
            {
                mRender: function (data, type, row) {
                    return '<div class="btn-group" role="group">' +
                        '<span class="btn btn-outline-info" onclick="editCoWorker(' + row.coWorker_Id + ',' + row.coWorkerTranslation_ID + ')" ><i class="fa fa-edit fa-sm"></i></span>' +
                        '<span class="btn btn-outline-danger" onclick="deleteCoWorker(' + row.coWorker_Id + ',' + row.coWorkerTranslation_ID + ')"><i class="fa fa-trash fa-sm"></i></span></div>';
                }
            },
        ]

    });
    loadingPageHide();
}

function editCoWorker(coWorkerId, translationId) {
    $.get("/Admin/CoWorkers?handler=EditCoWorker", { id: coWorkerId }, function (result) {
        if (result.status == "error") {
            if (isDevelopment) {
                alert(result.data);
            } else {
                ToastMessage("خطایی رخ داده است", "info", "خطا");
            }
        }
        else {
            $(".modal-dialog").css('max-width', '65%');
            $(".modal-header").css('background-color', '#008074b8');
            $(".headerModalColor").text("ویرایش همکاران");
            $("#myModal").modal();
            $("#myModalBody").html(result);
            editCoWorkerTranslation(translationId);
        }
    });
}

function editCoWorkerTranslation(translationId) {
    $.get("/Admin/CoWorkers?handler=EditCoWorkerTrans", { id: translationId }, function (res) {
        $("#coWorker_trans").html(res);
    });
}

function getCoWorkerTransInfoWithLanguage(langId, coWorkerId,selectedGroupId) {

    $.get("/Admin/CoWorkers?handler=CoWorkerTransInfo", { id: langId, coWorkerId: coWorkerId }, function (res) {
        if (res.status == "ok") {
            $(".update_div").removeClass('hide');
            $(".insert_div").addClass('hide');

            var group_id = "";
            if (selectedGroupId != null)
                group_id = selectedGroupId;
            else
                group_id = res.groupId

            getCoWorkerGroupByLang($("#language_id").val(), "edit", group_id);

            $("#Title").val(res.title);
            $("#Link").val(res.link);
            $("#CoWorker_Id").val(res.coWorker_Id);
            $("#CoWorkerTranslation_ID").val(res.id);
            if (res.isActive)
                $(".icheckbox_square-blue").addClass('checked');
            else
                $(".icheckbox_square-blue").removeClass('checked');
        }
        else {
            resetForm2();
            $(".update_div").addClass('hide');
            $(".insert_div").removeClass('hide');

            getCoWorkerGroupByLang($("#language_id").val(), "", null);
        }
    });
}

function updateCoWorker() {
    var frm = $("#coWorkerFrm").valid();
    if (frm == true) {

        var coWorker = new FormData();
        var files = $("#Image").get(0).files;


        $("#btnUpdateCoWorker").children(".fa-spinner").removeClass('hide');
        $("#btnUpdateCoWorker").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');


            coWorker.append("Image", $("#oldImage").val()),
            coWorker.append("CoWorker_ID", $("#CoWorker_ID").val()),
            coWorker.append("GroupTranslationId", $("#coWorkerGroup").val()),
            coWorker.append("imgCoWorker", files[0])

        $.ajax({
            method: 'POST',
            url: '/Admin/CoWorkers?handler=EditCoWorker',
            contentType: false,
            processData: false,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            data: coWorker,
            success: function (result) {
                $("#btnUpdateCoWorker").children(".fa-spinner").addClass('hide');
                $("#btnUpdateCoWorker").children(".fa-spinner").siblings('span').text('ویرایش');

                if (result == "ok") {
                    LoadDatatable("#coWorker_tb", false);
                    ToastMessage("اطلاعات مشترک همکاران با موفقیت ویرایش گردید", "success", "کاربر گرامی");
                }
                else {
                    if (isDevelopment) {
                        alert(result.data);
                    } else {
                        ToastMessage("خطایی رخ داده است", "info", "خطا");
                    }
                }
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}

function updateCoWorkerTranslation() {
    var frm = $("#coWorkerTransFrm").valid();
    if (frm == true) {
        if ($("#language_id").val() != "0") {
            $("#btnUpdateCoWorkerFinal").children(".fa-spinner").removeClass('hide');
            $("#btnUpdateCoWorkerFinal").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');

            var isactive = $('#IsActive:checked').length;
            var status = "";
            if (isactive === 1) { status = "true"; }
            else { status = "false"; }

            var coWorkerTranslation = {
                CoWorkerTranslation_ID: $("#CoWorkerTranslation_ID").val(),
                CoWorker_Id: $("#CoWorker_Id").val(),
                Title: $("#Title").val(),
                Link: $("#Link").val(),
                TranslationGroup_Id: $("#TranslationGroup_Id").val(),
                Language_Id: $("#language_id").val(),
                IsActive: status
            }

            $.ajax({
                method: 'POST',
                url: '/Admin/CoWorkers?handler=EditCoWorkerTrans',
                data: {
                    coWorkerTranslation: coWorkerTranslation,
                    __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
                },
                success: function (result) {
                    $(".fa-spinner").addClass('hide');
                    $(".fa-spinner").siblings('span').text('ویرایش');

                    if (result == "ok") {
                        resetForm();
                        ToastMessage("همکاران با موفقیت ویرایش گردید", "success", "کاربر گرامی");
                        LoadDatatable("#coWorker_tb", false);
                    }
                    //else if (result == "exsist") {
                    //    ToastMessage(" این کد رنگ برای این زبان قبلا ثبت شده است", "info", "کاربر گرامی");
                    //}
                    else {
                        if (isDevelopment) {
                            alert(result.data);
                        } else {
                            ToastMessage("خطایی رخ داده است", "warning", "خطا");
                        }
                    }
                },
                error: function (result) {
                }
            });
        }
        else {
            ToastMessage(" لطفا یک زبان را انتخاب کنید", "info", "کاربر گرامی");
        }

    }
    else {
        return false;
    }
}

function deleteCoWorker(coWorkerId, translationId) {
    $.confirm({
        title: 'آیا برای حذف این همکار مطمئن هستید؟', icon: 'fa fa-warning', theme: "material", animation: 'scale', content: '',
        buttons: {
            confirm: {
                text: 'تایید',
                btnClass: 'btn-blue',
                action: function () {
                    $.get("/Admin/CoWorkers?handler=DeleteCoWorker", { id: translationId, coWorkerId: coWorkerId, status: "one" },
                        function (result) {
                            if (result == "ok") {
                                ToastMessage("همکار با موفقیت حذف گردید", "success", "کاربر گرامی");
                                LoadDatatable("#coWorker_tb", false);
                            }
                            else {
                                if (isDevelopment) {
                                    alert(result.data);
                                } else {
                                    ToastMessage("خطایی رخ داده است", "warning", "خطا");
                                }
                            }
                        });
                }
            },

            deleteall: {
                text: 'حذف در تمام زبان ها',
                btnClass: 'btn-red',
                action: function () {

                    $.get("/Admin/CoWorkers?handler=DeleteCoWorker", { id: translationId, coWorkerId: coWorkerId, status: "all" },
                        function (result) {
                            if (result == "ok") {
                                ToastMessage("همکار در تمام زبان ها با موفقیت حذف گردید", "success", "کاربر گرامی");
                                LoadDatatable("#coWorker_tb", false);
                            }
                            else {
                                if (isDevelopment) {
                                    alert(result.data);
                                } else {
                                    ToastMessage("خطایی رخ داده است", "warning", "خطا");
                                }
                            }
                        });
                }
            },
            cancel: {
                text: 'انصراف',

            }
        },
    });
}

function getCoWorkerGroupByLang_Global(langId, selector) {
    $.get("/Admin/CoWorkers?handler=CoWorkerGroupByLang", { id: langId }, function (result) {
        var options = "<option value=''>گروه همکاران ...</option>";
        for (var i = 0; i < result.length; i++) {
            options += "<option value='" + result[i].coWorkerGroup_Id + "'>" + result[i].title + "</option>";
        }
        $(selector).html(options);
    });
}

function resetForm() {
    $("#language_id").val('0');
    $("#Group_Id").val('0');
    $("#Title").val('');
    $("#Link").val('');
}
function resetForm2() {
    $("#Group_Id").val('0');
    $("#Title").val('');
    $("#Link").val('');
}