﻿var isDevelopment = true;

function createCoWorkerGroup() {
    $.get("/Admin/CoWorkers?handler=CreateGroup", function (result) {
        $(".modal-dialog").css('max-width', '60%');
        $(".modal-header").css('background-color', '#008000b8');
        $(".headerModalColor").text("افزودن گروه");
        $("#myModal").modal();
        $("#myModalBody").html(result);
    });
}

function insertCoWorkerGroup() {
    var frm = $("#coWorkerGroupFrm").valid();
    if (frm == true) {

        $("#btnInsertCoWorkerGroup").children(".fa-spinner").removeClass('hide');
        $("#btnInsertCoWorkerGroup").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');

        var isactive = $('#IsActive:checked').length;
        var status = "";
        if (isactive === 1) { status = "true"; }
        else { status = "false"; }

        var coWorkerGroup = {
            Title: $("#Title").val(),
            ImageWidth: $("#ImageWidth").val(),
            ImageHeight: $("#ImageHeight").val(),
            Language_Id: $("#global_language_id").val(),
            IsActive: status,
        }
        $.ajax({
            method: 'POST',
            url: '/Admin/CoWorkers?handler=CreateGroup',
            data: {
                coWorkerGroup: coWorkerGroup,
                __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
            },
            success: function (result) {
                $(".fa-spinner").addClass('hide');
                $(".fa-spinner").siblings('span').text('ثبت');

                if (result.status == "ok") {

                    $(".insert-div").addClass('hide');
                    $(".update-div").removeClass('hide');

                    $("#CoWorkerGroup_ID").val(result.groupId);
                    createCoWorkerGroupTranslation(result.groupId);
                }
                else {
                    if (isDevelopment) {
                        alert(result.data);
                    } else {
                        ToastMessage("خطایی رخ داده است", "info", "خطا");
                    }
                }
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}

function createCoWorkerGroupTranslation(groupId) {
    $.get("/Admin/CoWorkers?handler=CreateGroupTrans", { id: groupId }, function (result) {
        $("#coWorkerGroup_trans").html(result);
        $("#tab_1").removeClass("active");
        $("#coWorkerGroup_trans").addClass("active");

        $("a[href='#tab_1']").removeClass('active');
        $("a[href='#coWorkerGroup_trans']").addClass('active');
    });
}

function insertCoWorkerGroupTranslation() {
    var frm = $("#coWorkerGroupTransFrm").valid();
    if (frm == true) {
        if ($("#language_id").val() != "0") {

            $("#btnInsertCoWorkerGroupTrans").children(".fa-spinner").removeClass('hide');
            $("#btnInsertCoWorkerGroupTrans").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');


            var coWorkerGroupTranslation = {
                CoWorkerGroup_Id: $("#CoWorkerGroup_Id").val(),
                Title: $("#Title").val(),
                Language_Id: $("#language_id").val()
            }
            $.ajax({
                method: 'POST',
                url: '/Admin/CoWorkers?handler=CreateGroupTrans',
                data: {
                    coWorkerGroupTranslation: coWorkerGroupTranslation,
                    __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
                },
                success: function (result) {
                    $("#btnInsertCoWorkerGroupTrans").children(".fa-spinner").addClass('hide');
                    $("#btnInsertCoWorkerGroupTrans").children(".fa-spinner").siblings('span').text('ثبت');

                    $(".insert-div").removeClass('hide');
                    $(".update-div").addClass('hide');

                    if (result == "ok") {
                        resetForm();
                        ToastMessage("گروه همکاران با موفقیت ثبت گردید", "success", "کاربر گرامی");
                        LoadDatatable("#coWorkerGroup_tb", true);
                    }
                    else if (result == "exsist") {
                        ToastMessage(" این گروه همکار  برای این زبان قبلا ثبت شده است", "info", "کاربر گرامی");
                    }
                    else {
                        if (isDevelopment) {
                            alert(result.data);
                        } else {
                            ToastMessage("خطایی رخ داده است", "warning", "خطا");
                        }
                    }
                },
                error: function (result) {
                }
            });
        }
        else {
            ToastMessage(" لطفا یک زبان را انتخاب کنید", "info", "کاربر گرامی");
        }
    }
    else {
        return false;
    }
}

function viewAllCoWorkerGroups() {
    loadingPageShow();
    $("#coWorkerGroup_tb").DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.24/i18n/Persian.json"
        },
        "processing": true,
        "serverSide": true,
        "filter": true,
        "ajax": {
            "url": "/Admin/CoWorkers?handler=ViewAllGroups",
            "type": "POST",
            "datatype": "json",
            "data": { __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val() },
        },

        "columns": [
            { "data": "title", "name": "Title" },
            {
                data: "coWorkerGroup.isActive",
                render: function (data) {
                    if (data) {
                        return '<span class="text-success">فعال</span >';
                    }
                    else {
                        return '<span class="text-danger">غیر فعال</span >';
                    }
                },
            },
            {
                mRender: function (data, type, row) {
                    return '<div class="btn-group" role="group">' +
                        '<span class="btn btn-outline-info" onclick="editCoWorkerGroup(' + row.coWorkerGroup_Id + ',' + row.coWorkerGroupTranslation_ID + ')"><i class="fa fa-edit fa-sm"></i></span>' +
                        '<span class="btn btn-outline-danger" onclick="deleteCoWorkerGroup(' + row.coWorkerGroup_Id + ',' + row.coWorkerGroupTranslation_ID + ')"><i class="fa fa-trash fa-sm"></i></span></div>';
                }
            },
        ]

    });
    loadingPageHide();
}

function editCoWorkerGroup(coWorkerGroupId, translationId) {
    $.get("/Admin/CoWorkers?handler=EditGroup", { id: coWorkerGroupId }, function (result) {
        if (result.status == "error") {
            if (isDevelopment) {
                alert(result.data);
            } else {
                ToastMessage("خطایی رخ داده است", "info", "خطا");
            }
        }
        $(".modal-dialog").css('max-width', '60%');
        $(".modal-header").css('background-color', '#008074b8');
        $(".headerModalColor").text("ویرایش گروه");
        $("#myModal").modal();
        $("#myModalBody").html(result);
        editCoWorkerGroupTranslation(translationId);
    });
}


function editCoWorkerGroupTranslation(translationId) {
    $.get("/Admin/CoWorkers?handler=EditGroupTrans", { id: translationId }, function (res) {
        $("#coWorkerGroup_trans").html(res);
    });
}

function getGroupTransInfoWithLangId(langId, groupId) {

    $.get("/Admin/CoWorkers?handler=GroupTransInfo", { id: langId, coWorkerGroupId: groupId }, function (res) {
        if (res.status == "ok") {
            $(".update_div").removeClass('hide');
            $(".insert_div").addClass('hide');

            $("#Title").val(res.title);
            $("#CoWorkerGroupTranslation_ID").val(res.id);
            $("#CoWorkerGroup_Id").val(groupId);
        }
        else {
            $("#Title").val('');
            $(".update_div").addClass('hide');
            $(".insert_div").removeClass('hide');
        }
    });
}

function updateCoWorkerGroup() {
    var frm = $("#coWorkerGroupFrm").valid();
    if (frm == true) {

        $("#btnUpdateCoWorkerGroup").children(".fa-spinner").removeClass('hide');
        $("#btnUpdateCoWorkerGroup").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');

        var isactive = $('#IsActive:checked').length;
        var status = "";
        if (isactive === 1) { status = "true"; }
        else { status = "false"; }

        var coWorkerGroup = {
            CoWorkerGroup_ID: $("#CoWorkerGroup_ID").val(),
            ImageWidth: $("#ImageWidth").val(),
            ImageHeight: $("#ImageHeight").val(),
            Title: $("#Title").val(),
            Language_Id: $("#global_language_id").val(),
            IsActive: status,
        }
        $.ajax({
            method: 'POST',
            url: '/Admin/CoWorkers?handler=EditGroup',
            data: {
                coWorkerGroup: coWorkerGroup,
                __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
            },
            success: function (result) {
                $("#btnUpdateCoWorkerGroup").children(".fa-spinner").addClass('hide');
                $("#btnUpdateCoWorkerGroup").children(".fa-spinner").siblings('span').text('ویرایش');

                if (result == "ok") {
                    LoadDatatable("#coWorkerGroup_tb", false);
                    ToastMessage("اطلاعات مشترک گروه همکاران با موفقیت ویرایش گردید", "success", "کاربر گرامی");
                }
                else {
                    if (isDevelopment) {
                        alert(result.data);
                    } else {
                        ToastMessage("خطایی رخ داده است", "info", "خطا");
                    }
                }
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}

function updateCoWorkerGroupTranslation() {
    var frm = $("#coWorkerGroupTransFrm").valid();
    if (frm == true) {
        if ($("#language_id").val() != "0") {

            $("#btnUpdateCoWorkerTrans").children(".fa-spinner").removeClass('hide');
            $("#btnUpdateCoWorkerTrans").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');


            var coWorkerGroupTranslation = {
                CoWorkerGroupTranslation_ID: $("#CoWorkerGroupTranslation_ID").val(),
                CoWorkerGroup_Id: $("#CoWorkerGroup_Id").val(),
                Title: $("#Title").val(),
                Language_Id: $("#language_id").val()
            }
            $.ajax({
                method: 'POST',
                url: '/Admin/CoWorkers?handler=EditGroupTrans',
                data: {
                    coWorkerGroupTranslation: coWorkerGroupTranslation,
                    __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
                },
                success: function (result) {
                    $(".fa-spinner").addClass('hide');
                    $(".fa-spinner").siblings('span').text('ویرایش');

                    if (result == "ok") {
                        resetForm();
                        ToastMessage("گروه همکاران با موفقیت ویرایش گردید", "success", "کاربر گرامی");
                        LoadDatatable("#coWorkerGroup_tb", true);
                    }

                    else {
                        if (isDevelopment) {
                            alert(result.data);
                        } else {
                            ToastMessage("خطایی رخ داده است", "warning", "خطا");
                        }
                    }
                },
                error: function (result) {
                }
            });
        }
        else {
            ToastMessage(" لطفا یک زبان را انتخاب کنید", "info", "کاربر گرامی");
        }
    }
    else {
        return false;
    }
}

function deleteCoWorkerGroup(groupId, translationId) {
    $.confirm({
        title: 'آیا برای حذف گروه همکاران مطمئن هستید؟', icon: 'fa fa-warning', theme: "material", animation: 'scale', content: '',
        buttons: {
            confirm: {
                text: 'تایید',
                btnClass: 'btn-blue',
                action: function () {
                    $.get("/Admin/CoWorkers?handler=DeleteGroup", { id: translationId, groupId: groupId, status: "one" },
                        function (result) {
                            if (result == "ok") {
                                ToastMessage("گروه همکاران با موفقیت حذف گردید", "success", "کاربر گرامی");
                                LoadDatatable("#coWorkerGroup_tb", false);
                            }
                            else if (result == "exists") {
                                ToastMessage("برای این گروه همکار تعریف شده است", "warning", "خطا");
                            }
                            else {
                                if (isDevelopment) {
                                    alert(result.data);
                                } else {
                                    ToastMessage("خطایی رخ داده است", "warning", "خطا");
                                }
                            }
                        });
                }
            },

            cancel: {
                text: 'انصراف',

            }
        },
    });
}

function resetForm() {
    $("#Title").val('');
    $("#language_id").val('0');
}