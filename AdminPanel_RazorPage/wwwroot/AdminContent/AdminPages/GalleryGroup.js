﻿var isDevelopment = true;
function createGalleryGroup() {
    $.get("/Admin/Galleries?handler=CreateGalleryGroup", function (result) {
        $(".modal-dialog").css('max-width', '55%');
        $(".modal-header").css('background-color', '#008000b8');
        $(".headerModalColor").text("افزودن گروه گالری");
        $("#myModal").modal();
        $("#myModalBody").html(result);
    });
}

function insertGalleryGroup() {
    var frm = $("#galleryGroupFrm").valid();
    if (frm == true) {

        $("#btnInsertGalleryGroup").children(".fa-spinner").removeClass('hide');
        $("#btnInsertGalleryGroup").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');

        var galleryGroup = {
            LargPicWidth: $("#LargPicWidth").val(),
            LargPicHeight: $("#LargPicHeight").val(),
            MediumPicWidth: $("#MediumPicWidth").val(),
            MediumPicHeight: $("#MediumPicHeight").val(),
            SmallPicWidth: $("#SmallPicWidth").val(),
            SmallPicHeight: $("#SmallPicHeight").val(),
        }
        $.ajax({
            method: 'POST',
            url: '/Admin/Galleries?handler=CreateGalleryGroup',
            data: {
                galleryGroup: galleryGroup,
                __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
            },
            success: function (result) {
                $(".fa-spinner").addClass('hide');
                $(".fa-spinner").siblings('span').text('ثبت');

                if (result.status == "ok") {

                    $(".insert-div").addClass('hide');
                    $(".update-div").removeClass('hide');

                    $("#GalleryGroup_Id").val(result.groupId);
                    createGalleryGroupTranslation(result.groupId);
                }
                else {
                    if (isDevelopment) {
                        alert(result.data);
                    } else {
                        ToastMessage("خطایی رخ داده است", "info", "خطا");
                    }
                }
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}
function createGalleryGroupTranslation(groupId) {
    $.get("/Admin/Galleries?handler=CreateGroupTrans", { id: groupId }, function (result) {
        $("#galleryGroup_trans").html(result);
        $("#tab_1").removeClass("active");
        $("#galleryGroup_trans").addClass("active");

        $("a[href='#tab_1']").removeClass('active');
        $("a[href='#galleryGroup_trans']").addClass('active');
    });
}

function insertGalleryGroupTranslation() {
    var frm = $("#galleryGroupTransFrm").valid();
    if (frm == true) {
        if ($("#language_id").val() != "0") {

            $("#btnInsertGalleryGroupTrans").children(".fa-spinner").removeClass('hide');
            $("#btnInsertGalleryGroupTrans").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');


            var galleryGroupTranslation = {
                GalleryGroup_Id: $("#GalleryGroup_Id").val(),
                Title: $("#Title").val(),
                Language_Id: $("#language_id").val()
            }
            $.ajax({
                method: 'POST',
                url: '/Admin/Galleries?handler=CreateGroupTrans',
                data: {
                    galleryGroupTranslation: galleryGroupTranslation,
                    __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
                },
                success: function (result) {
                    $("#btnInsertGalleryGroupTrans").children(".fa-spinner").addClass('hide');
                    $("#btnInsertGalleryGroupTrans").children(".fa-spinner").siblings('span').text('ثبت');

                    $(".insert-div").removeClass('hide');
                    $(".update-div").addClass('hide');

                    if (result == "ok") {
                        resetForm();
                        ToastMessage("گروه گالری با موفقیت ثبت گردید", "success", "کاربر گرامی");
                        LoadDatatable("#galleryGroup_tb", true);
                        getGalleryGroupByLang_Global("", "#galleryGroup_ddl", "", "");
                    }
                    else if (result == "exsist") {
                        ToastMessage("این گروه برای این زبان ثبت شده است", "info", "کاربر گرامی");
                    }
                    else {
                        if (isDevelopment) {
                            alert(result.data);
                        } else {
                            ToastMessage("خطایی رخ داده است", "warning", "خطا");
                        }
                    }
                },
                error: function (result) {
                }
            });
        }
        else {
            ToastMessage(" لطفا یک زبان را انتخاب کنید", "info", "کاربر گرامی");
        }
    }
    else {
        return false;
    }
}
function viewAllGalleryGroups() {
    $("#galleryGroup_tb").DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.24/i18n/Persian.json"
        },
        "processing": true,
        "serverSide": true,
        "filter": true,
        "ajax": {
            "url": "/Admin/Galleries?handler=ViewAllGalleryGroups",
            "type": "POST",
            "datatype": "json",
            "data": { __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val() },
        },

        "columns": [
            { "data": "title", "name": "Title" },
            {
                mRender: function (data, type, row) {
                    return '<div class="btn-group" role="group">' +
                        '<span class="btn btn-outline-info" onclick="editGalleryGroup(' + row.galleryGroup_Id + ',' + row.galleryGroupTranslation_ID + ')"><i class="fa fa-edit fa-sm"></i></span>' +
                        '<span class="btn btn-outline-danger" onclick="deleteGalleryGroup(' + row.galleryGroup_Id + ',' + row.galleryGroupTranslation_ID + ')"><i class="fa fa-trash fa-sm"></i></span></div>';
                }
            },
        ]

    });
    loadingPage();
}

function editGalleryGroup(galleryGroupId,translationId) {
    $.get("/Admin/Galleries?handler=EditGalleryGroup", { id: galleryGroupId }, function (result) {
        if (result.status == "error") {
            if (isDevelopment) {
                alert(result.data);
            } else {
                ToastMessage("خطایی رخ داده است", "info", "خطا");
            }
        }
        $(".modal-dialog").css('max-width', '55%');
        $(".modal-header").css('background-color', '#008074b8');
        $(".headerModalColor").text("ویرایش گروه گالری");
        $("#myModal").modal();
        $("#myModalBody").html(result);

        editGalleryGroupTranslation(translationId);
    });
}

function editGalleryGroupTranslation(translationId) {
    $.get("/Admin/Galleries?handler=EditGroupTrans", { id: translationId }, function (res) {
        $("#galleryGroup_trans").html(res);
    });
}

function getGroupTransInfoWithLangId(langId, groupId) {

    $.get("/Admin/Galleries?handler=GroupTransInfo", { id: langId, galleryGroupId: groupId }, function (res) {
        if (res.status == "ok") {
            $(".update_div").removeClass('hide');
            $(".insert_div").addClass('hide');

            $("#Title").val(res.title);
            $("#GalleryGroupTranslation_ID").val(res.id);
        }
        else {
            $("#Title").val('');
            $(".update_div").addClass('hide');
            $(".insert_div").removeClass('hide');
        }
    });
}

function updateGalleryGroup() {
    var frm = $("#galleryGroupFrm").valid();
    if (frm == true) {

        $("#btnUpdateGalleryGroup").children(".fa-spinner").removeClass('hide');
        $("#btnUpdateGalleryGroup").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');

        var galleryGroup = {
            GalleryGroup_ID: $("#GalleryGroup_ID").val(),
            LargPicWidth: $("#LargPicWidth").val(),
            LargPicHeight: $("#LargPicHeight").val(),
            MediumPicWidth: $("#MediumPicWidth").val(),
            MediumPicHeight: $("#MediumPicHeight").val(),
            SmallPicWidth: $("#SmallPicWidth").val(),
            SmallPicHeight: $("#SmallPicHeight").val(),
        }
        $.ajax({
            method: 'POST',
            url: '/Admin/Galleries?handler=EditGalleryGroup',
            data: {
                galleryGroup: galleryGroup,
                __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
            },
            success: function (result) {
                $(".fa-spinner").addClass('hide');
                $(".fa-spinner").siblings('span').text('ویرایش');

                if (result == "ok") {
                    LoadDatatable("#galleryGroup_tb", false);
                    ToastMessage("اطلاعات مشترک گروه گالری با موفقیت ویرایش گردید", "success", "کاربر گرامی");
                }
                else {
                    if (isDevelopment) {
                        alert(result.data);
                    } else {
                        ToastMessage("خطایی رخ داده است", "info", "خطا");
                    }
                }
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}

function updateGalleryGroupTranslation() {
    var frm = $("#galleryGroupTransFrm").valid();
    if (frm == true) {
        if ($("#language_id").val() != "0") {

            $("#btnUpdateGalleryGroupTrans").children(".fa-spinner").removeClass('hide');
            $("#btnUpdateGalleryGroupTrans").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');


          
            var galleryGroupTranslation = {
                GalleryGroupTranslation_ID: $("#GalleryGroupTranslation_ID").val(),
                GalleryGroup_Id: $("#GalleryGroup_Id").val(),
                Title: $("#Title").val(),
                Language_Id: $("#language_id").val()
            }
            $.ajax({
                method: 'POST',
                url: '/Admin/Galleries?handler=EditGroupTrans',
                data: {
                    galleryGroupTranslation: galleryGroupTranslation,
                    __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
                },
                success: function (result) {
                    $(".fa-spinner").addClass('hide');
                    $(".fa-spinner").siblings('span').text('ویرایش');

                    if (result == "ok") {
                        resetForm();
                        ToastMessage("گروه گالری با موفقیت ویرایش گردید", "success", "کاربر گرامی");
                        LoadDatatable("#galleryGroup_tb", true);
                    }

                    else {
                        if (isDevelopment) {
                            alert(result.data);
                        } else {
                            ToastMessage("خطایی رخ داده است", "warning", "خطا");
                        }
                    }
                },
                error: function (result) {
                }
            });
        }
        else {
            ToastMessage(" لطفا یک زبان را انتخاب کنید", "info", "کاربر گرامی");
        }
    }
    else {
        return false;
    }
}

function deleteGalleryGroup(groupId, translationId) {
    $.confirm({
        title: 'آیا برای حذف گروه گالری مطمئن هستید؟', icon: 'fa fa-warning', theme: "material", animation: 'scale', content: '',
        buttons: {
            confirm: {
                text: 'تایید',
                btnClass: 'btn-blue',
                action: function () {
                    $.get("/Admin/Galleries?handler=DeleteGalleryGroup", { id: translationId, groupId: groupId, status: "one" },
                        function (result) {
                            if (result == "ok") {
                                ToastMessage("گروه گالری با موفقیت حذف گردید", "success", "کاربر گرامی");
                                LoadDatatable("#galleryGroup_tb", false);
                            }
                            else if (result == "exists") {
                                ToastMessage("برای این گروه گالری تعریف شده است", "warning", "خطا");
                            }
                            else {
                                if (isDevelopment) {
                                    alert(result.data);
                                } else {
                                    ToastMessage("خطایی رخ داده است", "warning", "خطا");
                                }
                            }
                        });
                }
            },

            cancel: {
                text: 'انصراف',

            }
        },
    });
}