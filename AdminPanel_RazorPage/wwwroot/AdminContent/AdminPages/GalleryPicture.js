﻿
//تابعی که با زدن ضرب در روی عکس فراخوانی میشود
function removePictureFile2(pictureFileName) {

    $.ajax({
        url: "/Admin/Galleries?handler=RemoveFileFromTemp",
        type: "GET",
        data: { filename: pictureFileName }
    }).done(function (result) {
        var responsePart = eval(result.d);

        removedPicName = pictureFileName;
        for (var i = 0; i < newImageArray.length; i++) {
            if (newImageArray[i] == pictureFileName) {
                newImageArray.splice(i, 1);
                break;
            }
        }
        var arr = '';
        for (var i = 0; i < newImageArray.length; i++) {
            arr += newImageArray[i] + ' * ';
        }
    });

}
///این تابع برای ارسال عکس در پوشه تمپ می باشد
function ImageUploader() {
    loadingPageShow();

    var formData = new FormData();
    var totalFiles = document.getElementById("ImageUpload").files.length;
    for (var i = 0; i < totalFiles; i++) {
        var file = document.getElementById("ImageUpload").files[i];
        formData.append("ImageUpload", file);
    }
    $.ajax({
        method: 'POST',
        url: '/Admin/Galleries?handler=Upload',
        data: formData,
        contentType: false,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("XSRF-TOKEN",
                $('input:hidden[name="__RequestVerificationToken"]').val());
        },
        processData: false,
        success: function (data) {
            var htmls = "";
            var fileNames = data.filePath;
            fileNames = fileNames.split("&");
            for (var i = 0; i < fileNames.length - 1; i++) {
                htmls += '<div class="myimage">' +
                    '<img  id="' + fileNames[i] + '" class="img-thumbnail imgPreviewsStyle" src="/' + fileNames[i] + '" width="100" height="100" />' +
                    '<span><i class="fa fa-fw fa-remove text-danger" title="حذف" ></i>' +
                    '</span>' +
                    '</div>';

                newImageArray.push(fileNames[i] + "&");
            }
            $("#imgPreviews").html(htmls);

            loadingPageHide();
            //newImageArray.push(responsePart[1]);
            $(".btn-warning").removeClass('hidden');
            //swal("آپلود تصاویر تکمیل شد!", "تصاویر با موفقیت آپلود شد!", "success");
        },
        error: function (result) {
            alert(result.error);
        }
    });
}


function Upload() { //ذخیره اصلی اطلاعات
    var imageArray = [];
    var frm = $("#galleryPicFrm").valid();
    if (frm === true) {
        $("#btnUploadGalleryPic").children(".fa-spinner").removeClass('hide');
        $("#btnUploadGalleryPic").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');

        var galleryId = $("#Gallery_Id").val();
        $("#imgPreviews").children('.myimage').children('img').each(function () {
            imageArray.push($(this).attr('id'));
        });


        $.ajax({
            data: {
                galleryId: galleryId,
                images: imageArray
            },
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            url: '/Admin/Galleries?handler=CreateGalleryPicture',
            success: function (response) {
                $(".fa-spinner").addClass('hide');
                $(".fa-spinner").siblings('span').text('ثبت');
                imageArray = [];

                if (response == "ok") {
                    ToastMessage("تصاویر گالری با موفقیت ثبت گردید", "success", "آپلود تصاویر");
                    $("#imgPreviews").html('');
                    ViewAllGalleryPic(galleryId);
                   
                }
                else if (response.status == "exist") {
                    ToastMessage("فایل انتخابی '" + response.fileName + "' برای این گالری ثبت شده است لطفا این عکس  را از لیست انتخابی حذف نمایید", "info", "آپلود تصاویر");
                }
            },

        });
    }
    else {
        return false;
    }
}
/////حذف تصاویر گالری
function checkboxSelectedGalleryPictures() {
    $.confirm({
        title: 'آیا برای حذف تصاویر مطمئن هستید؟', icon: 'fa fa-warning', theme: "material", animation: 'scale', content: '',
        buttons: {
            confirm: {
                text: 'تایید',
                btnClass: 'btn-blue',
                action: function () {

                    var checkboxValues = "";
                    $("div.checked").children('.forRemove').each(function () { // گرفتن ولیو های چکس باکس هایی که تیک خورده باشند
                        checkboxValues += $(this).val() + "&";
                    });
                    if (checkboxValues != "") {

                        var cv = [];
                        cv = checkboxValues.split("&");


                        $.ajax({
                            url: "/Admin/Galleries?handler=DeleteGalleryPic",
                            type: "Get",
                            data: { values: checkboxValues }
                        }).done(function (result) {

                            for (var i = 0; i < cv.length - 1; i++) {//substring(0, cv[i].indexOf(".jpg", 0))
                                $("#gp_" + cv[i]).hide("slow");
                            }
                            ToastMessage("عملیات حذف با موفقیت انجام گردید", "success", "کاربر گرامی");
                        });
                    }
                    else {
                        ToastMessage("تصویری برای حذف انتخاب نشده است", "warning", "کاربر گرامی");
                    }
                }
            },

            cancel: {
                text: 'انصراف',

            }
        },
    });
}