﻿var isDevelopment = true;

function createCity() {
    $.get("/Admin/Regions?handler=CreateCity", function (result) {
        if (result.status == "error") {
            if (isDevelopment) {
                alert(result.msg);
            } else {
                ToastMessage("خطایی رخ داده است", "info", "خطا");
            }
        }
        $(".modal-dialog").css('max-width', '55%');
        $(".modal-header").css('background-color', '#008000b8');
        $(".headerModalColor").text("افزودن شهر");
        $("#myModal").modal();
        $("#myModalBody").html(result);
    });
}

function insertCity() {
    var frm = $("#cityFrm").valid();
    if (frm == true) {
        $("#btnInsertCity").children(".fa-spinner").removeClass('hide');
        $("#btnInsertCity").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');
        var isactive = $('#IsActive:checked').length;
        var status = "";
        if (isactive === 1) { status = "true"; }
        else { status = "false"; }

        var city = {
            State_Id: $("#State_Id").val(),
            Name: $("#Name").val(),
            Code: $("#Code").val(),
            IsActive: status,
        }
        $.ajax({
            method: 'POST',
            url: '/Admin/Regions?handler=CreateCity',
            data: {
                city: city,
                __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
            },
            success: function (result) {
                $(".fa-spinner").addClass('hide');
                $(".fa-spinner").siblings('span').text('ثبت');

                if (result == "ok") {

                    LoadDatatable("#city_tb", false);
                    //$("#myModal").modal("hide");
                    ToastMessage("شهر با موفقیت ثبت گردید", "success", "کاربر گرامی");
                    clearForm();
                }
                else if (result == "exists") {
                    ToastMessage(" این شهر قبلا ثبت شده است", "info", "کاربر گرامی");
                }
                else {
                    if (isDevelopment) {
                        alert(result.data);
                    } else {
                        ToastMessage("خطایی رخ داده است", "info", "خطا");
                    }
                }
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}

function viewAllCities(stateId) {
    loadingPageShow();
    $("#city_tb").DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.24/i18n/Persian.json"
        },
        "processing": true,
        "serverSide": true,
        "filter": true,
        "destroy": true,
        "ajax": {
            "url": "/Admin/Regions?handler=ViewAllCities",
            "type": "POST",
            "datatype": "json",
            "data": {
                stateId: stateId,
                __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
            },
        },

        "columns": [
            { "data": "name", "name": "Name" },
            { "data": "code", "name": "Code" },
            {
                data: "isActive",
                render: function (data) {
                    if (data) {
                        return '<span class="text-success">فعال</span >';
                    }
                    else {
                        return '<span class="text-danger">غیر فعال</span >';
                    }
                },
            },
            {
                mRender: function (data, type, row) {
                    return '<div class="btn-group" role="group">' +
                        '<span class="btn btn-outline-info" onclick="editCity(' + row.city_ID + ')"><i class="fa fa-edit fa-sm"></i></span>' +
                        '<span class="btn btn-outline-danger btnCityDelete" onclick="deleteCity(' + row.city_ID + ')"><i class="fa fa-trash fa-sm"></i></span></div>';
                }
            },
        ]

    });
    loadingPageHide();
}

function editCity(Id) {
    $.get("/Admin/Regions?handler=EditCity", { id: Id }, function (result) {
        if (result.status == "error") {
            if (isDevelopment) {
                alert(result.msg);
            } else {
                ToastMessage("خطایی رخ داده است", "info", "خطا");
            }
        }
        $(".modal-dialog").css('max-width', '55%');
        $(".modal-header").css('background-color', '#008074b8');
        $(".headerModalColor").text("ویرایش شهر");
        $("#myModal").modal();
        $("#myModalBody").html(result);
    });
}

function updateCity() {
    var frm = $("#cityFrm").valid();
    if (frm == true) {
        $("#btnUpdateCity").children(".fa-spinner").removeClass('hide');
        $("#btnUpdateCity").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');
        var isactive = $('#IsActive:checked').length;
        var status = "";
        if (isactive === 1) { status = "true"; }
        else { status = "false"; }

        var city = {
            City_ID: $("#City_ID").val(),
            Name: $("#Name").val(),
            Code: $("#Code").val(),
            State_Id: $("#State_Id").val(),
            IsActive: status,
        }
        $.ajax({
            method: 'POST',
            url: '/Admin/Regions?handler=EditCity',
            data: {
                city: city,
                __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
            },
            success: function (result) {
                $(".fa-spinner").addClass('hide');
                $(".fa-spinner").siblings('span').text('ویرایش');

                if (result == "ok") {
                    LoadDatatable("#city_tb", false);
                    $("#myModal").modal("hide");
                    ToastMessage("شهر با موفقیت ویرایش گردید", "success", "کاربر گرامی");
                }
                else {
                    if (isDevelopment) {
                        alert(result.data);
                    } else {
                        ToastMessage("خطایی رخ داده است", "info", "خطا");
                    }
                }
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}

function deleteCity(cityId) {
    $.confirm({
        title: 'آیا برای حذف این شهر مطمئن هستید؟', icon: 'fa fa-warning', theme: 'material', animation: 'scale', content: '',
        buttons: {
            confirm: {
                text: 'تایید',
                btnClass: 'btn-blue',
                action: function () {

                    $.get("/Admin/Regions?handler=DeleteCity", { id: cityId }, function (result) {
                        if (result == "ok") {
                            LoadDatatable("#city_tb", false);
                            ToastMessage("شهر با موفقیت حذف گردید", "success", "کاربر گرامی");
                        }
                        else if (result.status == "error") {
                            if (isDevelopment) {
                                alert(result.data);
                            } else {
                                ToastMessage("خطایی رخ داده است", "info", "خطا");
                            }
                        }
                    });
                }
            },
            cancel: {
                text: 'انصراف',
            }
        },
    });

}

function clearForm() {
    $("#State_Id").val('');
    $("#State_CountryId").val('');
    $("#Name").val('');
    $("#Code").val('');
}
