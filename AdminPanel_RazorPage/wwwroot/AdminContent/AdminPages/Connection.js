﻿var isDevelopment = true;
///Toast Message
var ToastMessage = function (message, type, title) {
    var msgs = [message];
    var $toast = toastr[type](msgs, title);
    $toastlast = $toast;
    //return msgs;
};
toastr.options = {
    closeButton: 'true',
    progressBar: 'true',
    positionClass: 'toast-top-left',
};

function createConnection() {
    $.get("/Admin/Connections?handler=CreateConnection", function (result) {
        if (result.status == "error") {
            if (isDevelopment) {
                alert(result.data);
            } else {
                ToastMessage("خطایی رخ داده است", "info", "خطا");
            }
        }
        $(".modal-dialog").css('max-width', '65%');
        $(".modal-header").css('background-color', '#008000b8');
        $(".headerModalColor").text("افزودن راه های ارتباطی");
        $("#myModal").modal();
        $("#myModalBody").html(result);
    });
}

function getConnectionTypeTitle(typeTitle) {
    var result = "";
    switch (typeTitle) {

        case "Mobile":
            result = "0";
            break;
        case "Email":
            result = "1";
            break;
        case "Phone":
            result = "2";
            break;
        case "Map":
            result = "3";
            break;
        default:
            result = "nothing";
    }
    return result;
}

function setConnectionImage(typeId) {
    
    var labelClassName = "";
    switch (typeId) {

        case "0"://Mobile":
            labelClassName = "icon-phone-wire";
            break;
        case "1":
            labelClassName = "icon-envelope1";
            break;
        case "2":
            labelClassName = "icon-telephone-1";
            break;
        case "3":
            labelClassName = "icon-map-pin1";
            break;
        default:
            labelClassName = "nothing";
    }
    if (labelClassName != "nothing") {
        $("#Image").attr('class', '');
        $("#Image").addClass(labelClassName);
    }
    else {
        $("#Image").attr('class', '');
        //$("#Image").text("آیتمی انتخاب نشده است");
    }
}


function insertConnection() {
    var frm = $("#connectionFrm").valid();
    if (frm == true) {
        if ($("#Type").val() != "-1") {
            $("#btnInsertConnection").children(".fa-spinner").removeClass('hide');
            $("#btnInsertConnection").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');
            var connection = {
                GroupId: $("#GroupId").val(),
                Type: $("#Type").val(),
                Image: $("#Image").attr('class'),
            }
            $.ajax({
                method: 'POST',
                url: '/Admin/Connections?handler=CreateConnection',
                data: {
                    connection: connection,
                    __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
                },
                success: function (result) {
                    $(".fa-spinner").addClass('hide');
                    $(".fa-spinner").siblings('span').text('ویرایش');

                    if (result.status == "ok") {

                        $(".insert-div").addClass('hide');
                        $(".update-div").removeClass('hide');

                        $("#Id").val(result.connectionId);
                        createConnectionTranslation(result.connectionId);
                    }
                    else {
                        if (isDevelopment) {
                            alert(result.data);
                        } else {
                            ToastMessage("خطایی رخ داده است", "info", "خطا");
                        }
                    }
                },
                error: function (result) {
                }
            });
        }
        else {
            ToastMessage(" لطفا یک نوع راه ارتباطی انتخاب کنید", "info", "کاربر گرامی");
        }
    }
    else {
        return false;
    }
}

function createConnectionTranslation(Id) {
    $.get("/Admin/Connections?handler=CreateConnectionTrans", { id: Id }, function (result) {
        $("#connection_trans").html(result);
        $("#tab_1").removeClass("active");
        $("#connection_trans").addClass("active");

        $("a[href='#tab_1']").removeClass('active');
        $("a[href='#connection_trans']").addClass('active');
    });
}

function insertConnectionTranslation() {
    var frm = $("#connectionTransFrm").valid();
    if (frm == true) {
        if ($("#language_id").val() != "0") {

            $("#btnInsertConnectionFinal").children(".fa-spinner").removeClass('hide');
            $("#btnInsertConnectionFinal").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');
            var connectionTranslation = {
                ConnectionId: $("#ConnectionId").val(),
                Name: $("#Name").val(),
                Value: $("#Value").val(),
                LanguageId: $("#language_id").val(),
            }
            $.ajax({
                method: 'POST',
                url: '/Admin/Connections?handler=CreateConnectionTrans',
                data: {
                    connectionTranslation: connectionTranslation,
                    __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
                },
                success: function (result) {
                    $(".fa-spinner").addClass('hide');
                    $(".fa-spinner").siblings('span').text('ثبت');

                    $(".insert-div").removeClass('hide');
                    $(".update-div").addClass('hide');

                    if (result == "ok") {
                        resetForm();
                        ToastMessage("راه ارتباطی با موفقیت ثبت گردید", "success", "کاربر گرامی");
                        LoadDatatable("#connection_tb", true);
                    }
                    else if (result == "exsist") {
                        ToastMessage(" این  برای این زبان قبلا ثبت شده است", "info", "کاربر گرامی");
                    }
                    else {
                        if (isDevelopment) {
                            alert(result.data);
                        } else {
                            ToastMessage("خطایی رخ داده است", "warning", "خطا");
                        }
                    }
                },
                error: function (result) {
                }
            });
        }
        else {
            ToastMessage(" لطفا یک زبان را انتخاب کنید", "info", "کاربر گرامی");
        }
    }
    else {
        return false;
    }
}

function viewAllConnections() {
    $("#connection_tb").DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.24/i18n/Persian.json"
        },
        "processing": true,
        "serverSide": true,
        "filter": true,
        "ajax": {
            "url": "/Admin/Connections?handler=ViewAllConnections",
            "type": "POST",
            "datatype": "json",
            "data": { __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val() },
        },

        "columns": [
            { "data": "name", "name": "name", "autoWidth": true},
            { "data": "value", "name": "value", "autoWidth": true },
            { "data": "connection.connectionGroup.title", "name": "connection.connectionGroup.title", "autoWidth": true  },
            {
                data: "connection.image",
                render: function (data) {
                        return '<span class="'+data+'"></span >';
                },
            },
            {
                mRender: function (data, type, row) {
                    return '<div class="btn-group" role="group">' +
                        '<span class="btn btn-outline-info btnConnectionEdit" onclick="editConnection(' + row.connectionId + ',' + row.id +')" data-connectionid="' + row.connectionId + '" data-id="' + row.id + '"><i class="fa fa-edit fa-sm"></i></span>' +
                        '<span class="btn btn-outline-danger btnConnectionDelete" onclick="deleteConnection(' + row.connectionId + ',' + row.id +')" data-connectionid="' + row.connectionId + '" data-id="' + row.id + '"><i class="fa fa-trash fa-sm"></i></span></div>';
                }
            },
        ]

    });
    loadingPage();
}

//$("#connection_tb").on("click", " .btnConnectionEdit", function () {
function editConnection(connectionId, translationId) {
    var connectionId = connectionId;
    var Id = translationId; //$(this).attr('data-id');
    $.get("/Admin/Connections?handler=EditConnection", { id: connectionId }, function (result) {
        if (result.status == "error") {
            if (isDevelopment) {
                alert(result.data);
            } else {
                ToastMessage("خطایی رخ داده است", "info", "خطا");
            }
        }
        else {
            $(".modal-dialog").css('max-width', '65%');
            $(".modal-header").css('background-color', '#008074b8');
            $(".headerModalColor").text("ویرایش راه های ارتباطی");
            $("#myModal").modal();
            $("#myModalBody").html(result);
            editConnectionTranslation(Id);
        }
    });
}

function editConnectionTranslation(Id) {
    $.get("/Admin/Connections?handler=EditConnectionTrans", { id: Id }, function (res) {
        $("#connection_trans").html(res);
        getConnectionInfoWithLanguage('', Id);
    });
}

function getConnectionInfoWithLanguage(langId, connectionId) {
 
    $.get("/Admin/Connections?handler=ConnectionInfo", { id: langId, connectionId: connectionId }, function (res) {
        if (res.status == "ok") {
            $(".update_div").removeClass('hide');
            $(".insert_div").addClass('hide');


            $("#Name").val(res.name);
            $("#Value").val(res.value);
            $("#Name").attr('data-id', res.id);
        }
        else {
            resetForm2();
            $(".update_div").addClass('hide');
            $(".insert_div").removeClass('hide');
        }
    });
}

function updateConnection() {
    var frm = $("#connectionFrm").valid();
    if (frm == true) {

        $("#btnUpdateConnection").children(".fa-spinner").removeClass('hide');
        $("#btnUpdateConnection").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');
        var connection = {
            Id: $("#Id").val(),
            GroupId: $("#GroupId").val(),
            Type: $("#Type").val(),
            Image: $("#Image").attr('class'),
        }
        $.ajax({
            method: 'POST',
            url: '/Admin/Connections?handler=EditConnection',
            data: {
                connection: connection,
                __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
            },
            success: function (result) {
                $("#btnUpdateConnection").children(".fa-spinner").addClass('hide');
                $("#btnUpdateConnection").children(".fa-spinner").siblings('span').text('ویرایش');

                if (result == "ok") {
                    LoadDatatable("#connection_tb", false);
                    ToastMessage("اطلاعات مشترک راه های ارتباطی با موفقیت ویرایش گردید", "success", "کاربر گرامی");
                }
                else {
                    if (isDevelopment) {
                        alert(result.data);
                    } else {
                        ToastMessage("خطایی رخ داده است", "info", "خطا");
                    }
                }
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}

function updateConnectionTranslation() {
    var frm = $("#connectionTransFrm").valid();
    if (frm == true) {
        if ($("#language_id").val() != "0") {
            $("#btnUpdateConnectionTrans").children(".fa-spinner").removeClass('hide');
            $("#btnUpdateConnectionTrans").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');

            var connectionTranslation = {
                Id: $("#Name").attr('data-id'),
                ConnectionId: $("#ConnectionId").val(),
                Name: $("#Name").val(),
                Value: $("#Value").val(),
                LanguageId: $("#language_id").val(),
            }
            
            $.ajax({
                method: 'POST',
                url: '/Admin/Connections?handler=EditConnectionTrans',
                data: {
                    connectionTranslation: connectionTranslation,
                    __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
                },
                success: function (result) {
                    $("#btnUpdateConnectionTrans").children(".fa-spinner").addClass('hide');
                    $("#btnUpdateConnectionTrans").children(".fa-spinner").siblings('span').text('ویرایش');

                    if (result == "ok") {
                        resetForm();
                        ToastMessage("راه ارتباطی با موفقیت ویرایش گردید", "success", "کاربر گرامی");
                        LoadDatatable("#connection_tb", false);
                    }
                    //else if (result == "exsist") {
                    //    ToastMessage(" این کد رنگ برای این زبان قبلا ثبت شده است", "info", "کاربر گرامی");
                    //}
                    else {
                        if (isDevelopment) {
                            alert(result.data);
                        } else {
                            ToastMessage("خطایی رخ داده است", "warning", "خطا");
                        }
                    }
                },
                error: function (result) {
                }
            });
        }
        else {
            ToastMessage(" لطفا یک زبان را انتخاب کنید", "info", "کاربر گرامی");
        }

    }
    else {
        return false;
    }
}

function deleteConnection(connectionId,translationId) {
    var connectionId = connectionId;
    var Id = translationId;
    $.confirm({
        title: 'آیا برای حذف راه های ارتباطی مطمئن هستید؟', icon: 'fa fa-warning',theme:"material", animation: 'scale', content: '',
        buttons: {
            confirm: {
                text: 'تایید',
                btnClass: 'btn-blue',
                action: function () {
                    $.get("/Admin/Connections?handler=DeleteConnection", { id: Id, connectionId: connectionId, status: "one" },
                        function (result) {
                            if (result == "ok") {
                                ToastMessage("راه ارتباطی با موفقیت حذف گردید", "success", "کاربر گرامی");
                                LoadDatatable("#connection_tb", false);
                            }
                            else {
                                if (isDevelopment) {
                                    alert(result.data);
                                } else {
                                    ToastMessage("خطایی رخ داده است", "warning", "خطا");
                                }
                            }
                        });
                }
            },
            deleteall: {
                text: 'حذف در تمام زبان ها',
                btnClass: 'btn-red',
                action: function () {

                    $.get("/Admin/Connections?handler=DeleteConnection", { id: Id, connectionId: connectionId, status: "all" },
                        function (result) {
                            if (result == "ok") {
                                ToastMessage("راه ارتباطی در تمام زبان ها با موفقیت حذف گردید", "success", "کاربر گرامی");
                                LoadDatatable("#connection_tb", false);
                            }
                            else {
                                if (isDevelopment) {
                                    alert(result.data);
                                } else {
                                    ToastMessage("خطایی رخ داده است", "warning", "خطا");
                                }
                            }
                        });
                }
            },
            cancel: {
                text: 'انصراف',

            }
        },
    });
}

function resetForm() {
    $("#language_id").val('0');
    $("#Name").val('');
    $("#Value").val('');
}
function resetForm2() {
    $("#Name").val('');
    $("#Value").val('');
}
