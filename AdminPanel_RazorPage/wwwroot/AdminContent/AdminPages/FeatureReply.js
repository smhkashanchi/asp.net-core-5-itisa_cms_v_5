﻿var isDevelopment = true;
function checkIsColorType(typeId) {
    if (typeId == "1") {
        $(".colo_div").removeClass('hide');
    }
    else
        $(".colo_div").addClass('hide');
}

function createFeatureReply() {
    $.get("/Admin/Features?handler=CreateFeatureReply", function (result) {
        if (result.status == "error") {
            if (isDevelopment) {
                alert(result.data);
            } else {
                ToastMessage("خطایی رخ داده است", "info", "خطا");
            }
        }
        $(".modal-dialog").css('max-width', '65%');
        $(".modal-header").css('background-color', '#008000b8');
        $(".headerModalColor").text("افزودن جواب جواب ویژگی");
        $("#myModal").modal();
        $("#myModalBody").html(result);
    });
}

function insertFeatureReply() {
    var frm = $("#featureReplyFrm").valid();
    if (frm == true) {
        $("#btnInsertFeatureReply").children(".fa-spinner").removeClass('hide');
        $("#btnInsertFeatureReply").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');


        var isDefault = $('#IsDefault:checked').length;
        var status_isDefault = "";
        if (isDefault === 1) { status_isDefault = "true"; }
        else { status_isDefault = "false"; }

        var colorCode = $("#ColorCode").val();
        if ($("#Feature_Id").val() != "1")
            colorCode = "";

        var featureReply = {
            Feature_Id: $("#Feature_Id").val(),
            ColorCode: colorCode,
            IsDefault: status_isDefault
        }
        $.ajax({
            method: 'POST',
            url: '/Admin/Features?handler=CreateFeatureReply',
            data: {
                featureReply: featureReply,
                __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
            },
            success: function (result) {
                $("#btnInsertFeatureReply").children(".fa-spinner").addClass('hide');
                $("#btnInsertFeatureReply").children(".fa-spinner").siblings('span').text('ثبت');


                if (result.status == "ok") {

                    $(".insert-div").addClass('hide');
                    $(".update-div").removeClass('hide');

                    $("#FeatureReply_ID").val(result.featureReplyId);
                    createFeatureReplyTranslation(result.featureReplyId);
                }
                else {
                    if (isDevelopment) {
                        alert(result.data);
                    } else {
                        ToastMessage("خطایی رخ داده است", "warning", "خطا");
                    }
                }
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}

function createFeatureReplyTranslation(featureReplyId) {
    $.get("/Admin/Features?handler=CreateFeatureReplyTranslation", { id: featureReplyId }, function (result) {
        $("#featureReply_trans").html(result);
        $("#tab_1").removeClass("active");
        $("#featureReply_trans").addClass("active");

        $("a[href='#tab_1']").removeClass('active');
        $("a[href='#featureReply_trans']").addClass('active');
    });
}

function insertFeatureReplyTranslation() {
    var frm = $("#featureReplyTransFrm").valid();
    if (frm == true) {
        if ($("#language_id").val() != "0") {

            $("#btnInsertFeatureReplyFinal").children(".fa-spinner").removeClass('hide');
            $("#btnInsertFeatureReplyFinal").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');

            var featureReplyTranslation = {
                FeatureReply_Id: $("#FeatureReply_Id").val(),
                Title: $("#Title").val(),
                Language_Id: $("#language_id").val(),
            }
            $.ajax({
                method: 'POST',
                url: '/Admin/Features?handler=CreateFeatureReplyTranslation',
                data: {
                    featureReplyTranslation: featureReplyTranslation,
                    __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
                },
                success: function (result) {
                    $(".fa-spinner").addClass('hide');
                    $(".fa-spinner").siblings('span').text('ثبت');

                    $(".insert-div").removeClass('hide');
                    $(".update-div").addClass('hide');

                    if (result == "ok") {
                        resetForm();
                        ToastMessage("جواب جواب ویژگی با موفقیت ثبت گردید", "success", "کاربر گرامی");
                        LoadDatatable("#featureReply_tb", true);
                    }
                    else if (result == "exsist") {
                        ToastMessage(" این جواب جواب ویژگی  برای این زبان قبلا ثبت شده است", "info", "کاربر گرامی");
                    }
                    else {
                        if (isDevelopment) {
                            alert(result.data);
                        } else {
                            ToastMessage("خطایی رخ داده است", "warning", "خطا");
                        }
                    }
                },
                error: function (result) {
                }
            });
        }
        else {
            ToastMessage(" لطفا یک زبان را انتخاب کنید", "info", "کاربر گرامی");
        }
    }
    else {
        return false;
    }
}

function viewAllFeatureReplies(FeatureId) {
    loadingPageShow();
    $("#featureReply_tb").DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.24/i18n/Persian.json"
        },
        "processing": true,
        "serverSide": true,
        "filter": true,
        "destroy": true,   /// allow reinitialize datattable
        "ajax": {
            "url": "/Admin/Features?handler=ViewAllFeatureReplies",
            "type": "POST",
            "datatype": "json",
            "data": {
                featureId: FeatureId,
                __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
            },
        },

        "columns": [

            { "data": "title", "name": "title", "autoWidth": true },
            {
                data: "featureReply.colorCode",
                render: function (data) {
                    if (data != null) {
                        return '<span class="fill-td" style="background-color:' + data + '">' + data + '</span >';
                    }
                    else {
                        return '<span class="text-muted">---</span >';
                    }
                }
            },
            {
                data: "featureReply.isDefault",
                render: function (data) {
                    if (data) {
                        return '<span class="text-success">هست</span >';
                    }
                    else {
                        return '<span class="text-muted">نیست</span >';
                    }
                }
            },
            {
                mRender: function (data, type, row) {
                    return '<div class="btn-group" role="group">' +
                        '<span class="btn btn-outline-info" onclick="editFeatureReply(' + row.featureReply_Id + ',' + row.featureReplyTranslation_ID + ')" ><i class="fa fa-edit fa-sm"></i></span>' +
                        '<span class="btn btn-outline-danger" onclick="deleteFeatureReply(' + row.featureReply_Id + ',' + row.featureReplyTranslation_ID + ')"><i class="fa fa-trash fa-sm"></i></span></div>';
                }
            },
        ]

    });
    loadingPageHide();
}

function editFeatureReply(featureId, translationId) {
    $.get("/Admin/Features?handler=EditFeatureReply", { id: featureId }, function (result) {
        if (result.status == "error") {
            if (isDevelopment) {
                alert(result.data);
            } else {
                ToastMessage("خطایی رخ داده است", "info", "خطا");
            }
        }
        else {
            $(".modal-dialog").css('max-width', '65%');
            $(".modal-header").css('background-color', '#008074b8');
            $(".headerModalColor").text("ویرایش جواب ویژگی");
            $("#myModal").modal();
            $("#myModalBody").html(result);
            editFeatureReplyTranslation(translationId);
        }
    });
}

function editFeatureReplyTranslation(Id) {
    $.get("/Admin/Features?handler=EditFeatureReplyTranslation", { id: Id }, function (res) {
        $("#featureReply_trans").html(res);
    });
}

function getFeatureReplyInfoWithLanguage(langId, featureReplyId) {

    $.get("/Admin/Features?handler=FeatureReplyInfo", { id: langId, featureReplyId: featureReplyId }, function (res) {
        if (res.status == "ok") {
            $(".update_div").removeClass('hide');
            $(".insert_div").addClass('hide');

            $("#Title").val(res.title);

            $("#FeatureReplyTranslation_ID").val(res.id);
        }
        else {
            $("#Title").val('');
            $("#FeatureReplyTranslation_ID").val('');
            $(".update_div").addClass('hide');
            $(".insert_div").removeClass('hide');
        }
    });
}

function updateFeatureReply() {
    var frm = $("#featureReplyFrm").valid();
    if (frm == true) {

        $("#btnUpdateFeatureReply").children(".fa-spinner").removeClass('hide');
        $("#btnUpdateFeatureReply").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');

        var isDefault = $('#IsDefault:checked').length;
        var status_isDefault = "";
        if (isDefault === 1) { status_isDefault = "true"; }
        else { status_isDefault = "false"; }

        var colorCode = $("#ColorCode").val();
        if ($("#Feature_Id").val() != "1")
            colorCode = "";

        var featureReply = {
            FeatureReply_ID: $("#FeatureReply_ID").val(),
            Feature_Id: $("#Feature_Id").val(),
            ColorCode: colorCode,
            IsDefault: status_isDefault
        }

        $.ajax({
            method: 'POST',
            url: '/Admin/Features?handler=EditFeatureReply',
            data: {
                featureReply: featureReply,
                __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
            },
            success: function (result) {
                $("#btnUpdateFeatureReply").children(".fa-spinner").addClass('hide');
                $("#btnUpdateFeatureReply").children(".fa-spinner").siblings('span').text('ویرایش');

                if (result == "ok") {
                    LoadDatatable("#featureReply_tb", false);
                    ToastMessage("اطلاعات مشترک جواب ویژگی با موفقیت ویرایش گردید", "success", "کاربر گرامی");
                }
                else {
                    if (isDevelopment) {
                        alert(result.data);
                    } else {
                        ToastMessage("خطایی رخ داده است", "info", "خطا");
                    }
                }
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}

function updateFeatureReplyTranslation() {
    var frm = $("#featureReplyTransFrm").valid();
    if (frm == true) {
        if ($("#language_id").val() != "0") {
            $("#btnUpdateFeatureReplyFinal").children(".fa-spinner").removeClass('hide');
            $("#btnUpdateFeatureReplyFinal").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');

            var featureReplyTranslation = {
                FeatureReplyTranslation_ID: $("#FeatureReplyTranslation_ID").val(),
                FeatureReply_Id: $("#FeatureReply_Id").val(),
                Title: $("#Title").val(),
                Language_Id: $("#language_id").val(),
            }
            $.ajax({
                method: 'POST',
                url: '/Admin/Features?handler=EditFeatureReplyTranslation',
                data: {
                    featureReplyTranslation: featureReplyTranslation,
                    __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
                },
                success: function (result) {
                    $("#btnUpdateFeatureReplyFinal").children(".fa-spinner").addClass('hide');
                    $("#btnUpdateFeatureReplyFinal").children(".fa-spinner").siblings('span').text('ویرایش');

                    if (result == "ok") {
                        resetForm();
                        ToastMessage("جواب ویژگی با موفقیت ویرایش گردید", "success", "کاربر گرامی");
                        LoadDatatable("#featureReply_tb", false);
                    }

                    else {
                        if (isDevelopment) {
                            alert(result.data);
                        } else {
                            ToastMessage("خطایی رخ داده است", "warning", "خطا");
                        }
                    }
                },
                error: function (result) {
                }
            });
        }
        else {
            ToastMessage(" لطفا یک زبان را انتخاب کنید", "info", "کاربر گرامی");
        }

    }
    else {
        return false;
    }
}

function deleteFeatureReply(featureReplyId, translationId) {
    $.confirm({
        title: 'آیا برای حذف جواب ویژگی مطمئن هستید؟', icon: 'fa fa-warning', theme: "material", animation: 'scale', content: '',
        buttons: {
            confirm: {
                text: 'تایید',
                btnClass: 'btn-blue',
                action: function () {
                    $.get("/Admin/Features?handler=DeleteFeatureReply", { id: translationId, featureReplyId: featureReplyId, status: "one" },
                        function (result) {
                            if (result == "ok") {
                                ToastMessage("جواب ویژگی با موفقیت حذف گردید", "success", "کاربر گرامی");
                                LoadDatatable("#featureReply_tb", false);
                            }
                            //else if (result == "existInReply") {
                            //    ToastMessage("برای جواب ویژگی مقدار تعریف شده است", "info", "کاربر گرامی");
                            //}
                            else {
                                if (isDevelopment) {
                                    alert(result.data);
                                } else {
                                    ToastMessage("خطایی رخ داده است", "warning", "خطا");
                                }
                            }
                        });
                }
            },
            deleteall: {
                text: 'حذف در تمام زبان ها',
                btnClass: 'btn-red',
                action: function () {

                    $.get("/Admin/Features?handler=DeleteFeatureReply", { id: translationId, featureReplyId: featureReplyId, status: "all" },
                        function (result) {
                            if (result == "ok") {
                                ToastMessage("جواب ویژگی با موفقیت حذف گردید", "success", "کاربر گرامی");
                                LoadDatatable("#featureReply_tb", false);
                            }
                            //else if (result == "existInReply") {
                            //    ToastMessage("برای جواب ویژگی مقدار تعریف شده است", "info", "کاربر گرامی");
                            //}
                            else {
                                if (isDevelopment) {
                                    alert(result.data);
                                } else {
                                    ToastMessage("خطایی رخ داده است", "warning", "خطا");
                                }
                            }
                        });
                }
            },
            cancel: {
                text: 'انصراف',

            }
        },
    });
}

function resetForm() {
    $("#language_id").val('0');
    $("#Title").val('');
}
