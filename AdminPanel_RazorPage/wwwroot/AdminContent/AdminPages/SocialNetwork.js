﻿var isDevelopment = true;
function createSocialNetwork() {
    $.get("/Admin/SocialNetworks?handler=CreateSocialNetwork", function (result) {
        if (result.status == "error") {
            if (isDevelopment) {
                alert(result.data);
            } else {
                ToastMessage("خطایی رخ داده است", "info", "خطا");
            }
        }
        $(".modal-dialog").css('max-width', '65%');
        $(".modal-header").css('background-color', '#008000b8');
        $(".headerModalColor").text("افزودن شبکۀ اجتماعی");
        $("#myModal").modal();
        $("#myModalBody").html(result);
    });
}

function getSocialNetworkTypeTitle(typeTitle, status) {
    var typeId = "";
    var labelClassName = "";
    switch (typeTitle) {
        case "Telegram":
            labelClassName = "icon-telegram1";
            typeId = "0";
            break;
        case "Instagram":
            labelClassName = "icon-instagram1";
            typeId = "1";
            break;
        case "Twitter":
            labelClassName = "icon-twitter1";
            typeId = "2";
            break;
        case "Linekdin":
            labelClassName = "icon-linkedin1";
            typeId = "3";
            break;
        case "Facebook":
            labelClassName = "icon-facebook-o";
            typeId = "4";
            break;
        default:
            labelClassName = "nothing";
    }
    if (status == "getId")
        return typeId;
    else
        return labelClassName;
}

function setSocialNetworkImage(typeId, imageSelector) {

    var labelClassName = "";
    switch (typeId) {

        case "0"://Mobile":
            labelClassName = "icon-telegram1";
            break;
        case "1":
            labelClassName = "icon-instagram1";
            break;
        case "2":
            labelClassName = "icon-twitter1";
            break;
        case "3":
            labelClassName = "icon-linkedin1";
            break;
        case "4":
            labelClassName = "icon-facebook-o";
            break;
        default:
            labelClassName = "nothing";
    }
    if (labelClassName != "nothing") {
        $(imageSelector).attr('class', '');
        $(imageSelector).addClass(labelClassName);
    }
    else {
        $(imageSelector).attr('class', '');
        //$("#Image").text("آیتمی انتخاب نشده است");
    }
}

function insertSocialNetwork() {
    var frm = $("#socialNetworkFrm").valid();
    if (frm == true) {
        if ($("#Type").val() != "-1") {
            $("#btnInsertSocialNetwork").children(".fa-spinner").removeClass('hide');
            $("#btnInsertSocialNetwork").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');
            var socialNetwork = {
                Link: $("#Link").val(),
                Type: $("#Type").val(),
                //Image: $("#Image").attr('class'),
            }
            $.ajax({
                method: 'POST',
                url: '/Admin/SocialNetworks?handler=CreateSocialNetwork',
                data: {
                    socialNetwork: socialNetwork,
                    __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
                },
                success: function (result) {
                    $("#btnInsertSocialNetwork").children(".fa-spinner").addClass('hide');
                    $("#btnInsertSocialNetwork").children(".fa-spinner").siblings('span').text('ویرایش');

                    if (result.status == "ok") {

                        $(".insert-div").addClass('hide');
                        $(".update-div").removeClass('hide');
                        $("#SocialNetwork_ID").val(result.socialNetworkId);
                        createSocialNetworkTranslation(result.socialNetworkId);
                    }
                    else {
                        if (isDevelopment) {
                            alert(result.data);
                        } else {
                            ToastMessage("خطایی رخ داده است", "info", "خطا");
                        }
                    }
                },
                error: function (result) {
                }
            });
        }
        else {
            ToastMessage(" لطفا یک نوع راه ارتباطی انتخاب کنید", "info", "کاربر گرامی");
        }
    }
    else {
        return false;
    }
}

function createSocialNetworkTranslation(Id) {
    $.get("/Admin/SocialNetworks?handler=CreateSocialNetworkTrans", { id: Id }, function (result) {
        $("#socialNetwork_trans").html(result);
        $("#tab_1").removeClass("active");
        $("#socialNetwork_trans").addClass("active");

        $("a[href='#tab_1']").removeClass('active');
        $("a[href='#socialNetwork_trans']").addClass('active');
    });
}

function insertSocialNetworkTranslation() {
    var frm = $("#socialNetworkTransFrm").valid();
    if (frm == true) {
        if ($("#language_id").val() != "0") {

            $("#btnInsertSocialNetworkTrans").children(".fa-spinner").removeClass('hide');
            $("#btnInsertSocialNetworkTrans").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');

            var isactive = $('#IsActive:checked').length;
            var status = "";
            if (isactive === 1) { status = "true"; }
            else { status = "false"; }


            var socialNetworkTranslation = {
                SocialNetwork_Id: $("#SocialNetwork_Id").val(),
                Title: $("#Title").val(),
                IsActive: status,
                Language_Id: $("#language_id").val(),
            }
            $.ajax({
                method: 'POST',
                url: '/Admin/SocialNetworks?handler=CreateSocialNetworkTrans',
                data: {
                    socialNetworkTranslation: socialNetworkTranslation,
                    __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
                },
                success: function (result) {
                    $(".fa-spinner").addClass('hide');
                    $(".fa-spinner").siblings('span').text('ثبت');

                    $(".insert-div").removeClass('hide');
                    $(".update-div").addClass('hide');

                    if (result == "ok") {
                        resetForm();
                        ToastMessage("شبکه اجتماعی با موفقیت ثبت گردید", "success", "کاربر گرامی");
                        LoadDatatable("#socialNetwork_tb", true);
                    }
                    else if (result == "exsist") {
                        ToastMessage(" این شبکه اجتماعی  برای این زبان قبلا ثبت شده است", "info", "کاربر گرامی");
                    }
                    else {
                        if (isDevelopment) {
                            alert(result.data);
                        } else {
                            ToastMessage("خطایی رخ داده است", "warning", "خطا");
                        }
                    }
                },
                error: function (result) {
                }
            });
        }
        else {
            ToastMessage(" لطفا یک زبان را انتخاب کنید", "info", "کاربر گرامی");
        }
    }
    else {
        return false;
    }
}

function viewAllSocialNetworks() {
    loadingPageShow();
    $("#socialNetwork_tb").DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.24/i18n/Persian.json"
        },
        "processing": true,
        "serverSide": true,
        "filter": true,
        "ajax": {
            "url": "/Admin/SocialNetworks?handler=ViewAllSocialNetworks",
            "type": "POST",
            "datatype": "json",
            "data": { __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val() },
        },

        "columns": [
            { "data": "title", "name": "title", "autoWidth": true },
            {
                data: "socialNetwork.image",
                mRender: function (data, type, row) {
                    return '<span  data-toggle="tooltip" data-placement="top" title="' + row.title + '" class="tooltips ' + getSocialNetworkTypeTitle(row.socialNetwork.type, "getClass") + '"></span >';
                }
            },
            {
                data: "isActive",
                render: function (data) {
                    if (data) {
                        return '<span class="text-success">فعال</span >';
                    }
                    else {
                        return '<span class="text-danger">غیر فعال</span >';
                    }
                },
            },
            {
                mRender: function (data, type, row) {
                    return '<div class="btn-group" role="group">' +
                        '<span class="btn btn-outline-info" onclick="editSocialNetwork(' + row.socialNetwork_Id + ',' + row.socialNetworkTranslation_ID + ')"><i class="fa fa-edit fa-sm"></i></span>' +
                        '<span class="btn btn-outline-danger" onclick="deleteSocialNetwork(' + row.socialNetwork_Id + ',' + row.socialNetworkTranslation_ID+ ')"><i class="fa fa-trash fa-sm"></i></span></div>';
                }
            },
        ]

    });
    loadingPageHide();
}

function editSocialNetwork(socialNetworkId, translationId) {
    $.get("/Admin/SocialNetworks?handler=EditSocialNetwork", { id: socialNetworkId }, function (result) {
        if (result.status == "error") {
            if (isDevelopment) {
                alert(result.data);
            } else {
                ToastMessage("خطایی رخ داده است", "info", "خطا");
            }
        }
        else {
            $(".modal-dialog").css('max-width', '65%');
            $(".modal-header").css('background-color', '#008074b8');
            $(".headerModalColor").text("ویرایش شبکه های اجتماعی");
            $("#myModal").modal();
            $("#myModalBody").html(result);
            editSocialNetworkTranslation(translationId, socialNetworkId);
        }
    });
}

function editSocialNetworkTranslation(Id, socialNetwork_Id) {
    $.get("/Admin/SocialNetworks?handler=EditSocialNetworkTrans", { id: Id }, function (res) {
        $("#socialNetwork_trans").html(res);
        getSocialNetworkInfoWithLanguage('', socialNetwork_Id);
    });
}

function getSocialNetworkInfoWithLanguage(langId, socialNetworkId) {

    $.get("/Admin/SocialNetworks?handler=SocialNetworkInfo", { id: langId, socialNetworkId: socialNetworkId }, function (res) {
        if (res.status == "ok") {
            $(".update_div").removeClass('hide');
            $(".insert_div").addClass('hide');

            $("#Title").val(res.title);
            if (res.isActive)
                $(".icheckbox_square-blue").addClass('checked');
            else
                $(".icheckbox_square-blue").removeClass('checked');
            $("#Title").attr('data-id', res.id);
        }
        else {
            $("#Title").val('');
            $(".update_div").addClass('hide');
            $(".insert_div").removeClass('hide');
        }
    });
}

function updateSocialNetwork() {
    var frm = $("#socialNetworkFrm").valid();
    if (frm == true) {

        $("#btnUpdateSocialNetwork").children(".fa-spinner").removeClass('hide');
        $("#btnUpdateSocialNetwork").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');

        var socialNetwork = {
            SocialNetwork_ID: $("#SocialNetwork_ID").val(),
            Link: $("#Link").val(),
            Type: $("#Type").val(),
        }
        $.ajax({
            method: 'POST',
            url: '/Admin/SocialNetworks?handler=EditSocialNetwork',
            data: {
                socialNetwork: socialNetwork,
                __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
            },
            success: function (result) {
                $("#btnUpdateSocialNetwork").children(".fa-spinner").addClass('hide');
                $("#btnUpdateSocialNetwork").children(".fa-spinner").siblings('span').text('ویرایش');

                if (result == "ok") {
                    LoadDatatable("#socialNetwork_tb", false);
                    ToastMessage("اطلاعات مشترک شبکه های اجتماعی با موفقیت ویرایش گردید", "success", "کاربر گرامی");
                }
                else {
                    if (isDevelopment) {
                        alert(result.data);
                    } else {
                        ToastMessage("خطایی رخ داده است", "info", "خطا");
                    }
                }
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}

function updateSocialNetworkTranslation() {
    var frm = $("#socialNetworkTransFrm").valid();
    if (frm == true) {
        if ($("#language_id").val() != "0") {
            $("#btnUpdateSocialNetworkTrans").children(".fa-spinner").removeClass('hide');
            $("#btnUpdateSocialNetworkTrans").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');

            var isactive = $('#IsActive:checked').length;
            var status = "";
            if (isactive === 1) { status = "true"; }
            else { status = "false"; }


            var socialNetworkTranslation = {
                SocialNetworkTranslation_ID: $("#Title").attr('data-id'),
                SocialNetwork_Id: $("#SocialNetwork_Id").val(),
                Title: $("#Title").val(),
                IsActive: status,
                Language_Id: $("#language_id").val(),
            }

            $.ajax({
                method: 'POST',
                url: '/Admin/SocialNetworks?handler=EditSocialNetworkTrans',
                data: {
                    socialNetworkTranslation: socialNetworkTranslation,
                    __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
                },
                success: function (result) {
                    $("#btnInsertSocialNetworkTrans").children(".fa-spinner").addClass('hide');
                    $("#btnInsertSocialNetworkTrans").children(".fa-spinner").siblings('span').text('ویرایش');

                    if (result == "ok") {
                        resetForm();
                        ToastMessage("شبکه اجتماعی با موفقیت ویرایش گردید", "success", "کاربر گرامی");
                        LoadDatatable("#socialNetwork_tb", false);
                    }
                    //else if (result == "exsist") {
                    //    ToastMessage(" این کد رنگ برای این زبان قبلا ثبت شده است", "info", "کاربر گرامی");
                    //}
                    else {
                        if (isDevelopment) {
                            alert(result.data);
                        } else {
                            ToastMessage("خطایی رخ داده است", "warning", "خطا");
                        }
                    }
                },
                error: function (result) {
                }
            });
        }
        else {
            ToastMessage(" لطفا یک زبان را انتخاب کنید", "info", "کاربر گرامی");
        }

    }
    else {
        return false;
    }
}

function deleteSocialNetwork(socialNetworkId, translationId) {
    $.confirm({
        title: 'آیا برای حذف شبکه اجتماعی مطمئن هستید؟', icon: 'fa fa-warning', theme: "material", animation: 'scale', content: '',
        buttons: {
            confirm: {
                text: 'تایید',
                btnClass: 'btn-blue',
                action: function () {
                    $.get("/Admin/SocialNetworks?handler=DeleteSocialNetworkTrans", { id: translationId, socialNetworkId: socialNetworkId, status: "one" },
                        function (result) {
                            if (result == "ok") {
                                ToastMessage("شبکه اجتماعی با موفقیت حذف گردید", "success", "کاربر گرامی");
                                LoadDatatable("#socialNetwork_tb", false);
                            }
                            else {
                                if (isDevelopment) {
                                    alert(result.data);
                                } else {
                                    ToastMessage("خطایی رخ داده است", "warning", "خطا");
                                }
                            }
                        });
                }
            },
            deleteall: {
                text: 'حذف در تمام زبان ها',
                btnClass: 'btn-red',
                action: function () {

                    $.get("/Admin/SocialNetworks?handler=DeleteSocialNetworkTrans", { id: translationId, socialNetworkId: socialNetworkId, status: "all" },
                        function (result) {
                            if (result == "ok") {
                                ToastMessage("شبکه اجتماعی با موفقیت حذف گردید", "success", "کاربر گرامی");
                                LoadDatatable("#socialNetwork_tb", false);
                            }
                            else {
                                if (isDevelopment) {
                                    alert(result.data);
                                } else {
                                    ToastMessage("خطایی رخ داده است", "warning", "خطا");
                                }
                            }
                        });
                }
            },
            cancel: {
                text: 'انصراف',

            }
        },
    });
}

function resetForm() {
    $("#language_id").val('0');
    $("#Title").val('');
}