﻿var isDevelopment = true;
///Toast Message
var ToastMessage = function (message, type, title) {
    var msgs = [message];
    var $toast = toastr[type](msgs, title);
    $toastlast = $toast;
    //return msgs;
};
toastr.options = {
    closeButton: 'true',
    progressBar: 'true',
    positionClass: 'toast-top-left',
};
var contentGroupIdForParentId = 0;
var contentGroupIdForParentIdEdit = 0;
var pgID = 1;
///TreeView For ContentGroup
function getAllContentsGroupForTreeView(lang,status) { //خواندن گروه ها برای تری ویو ها
    $.ajax({
        method: 'GET',
        url: '/Admin/Contents?handler=CreateTreeView',
        data: { lang: lang },
        success: function (response) {

            $('#contentsGroup_tw').treeview({  //Create Content
               
                data: response,
                expandIcon: 'fa fa-plus-square',
                collapseIcon: 'fa fa-minus-square',
                onNodeSelected: function (event, node) {
                    contentGroupIdForParentId = node.tags;
                }

            });
            if (status != "dontShow") {
                $('#contentsGroup2_tw').treeview({   //index Content
                    data: response,
                    expandIcon: 'fa fa-plus-square',
                    collapseIcon: 'fa fa-minus-square',
                    onNodeSelected: function (event, node) {
                        viewDetailsContentGroup(node.tags);


                    },
                    //
                });
            }

            $('#contentsGroup3_tw').treeview({   //Edit ContentGroup
                data: response,
                expandIcon: 'fa fa-plus-square',
                collapseIcon: 'fa fa-minus-square',
                onNodeSelected: function (event, node) {
                    contentGroupIdForParentIdEdit = node.tags;
                },
            });

            $('#contentsGroup4_tw').treeview({   //add content
                data: response,
                expandIcon: 'fa fa-plus-square',
                collapseIcon: 'fa fa-minus-square',
                multiSelect: true,
                onNodeSelected: function (event, node) {
                    //var cgtId = node.href;
                    //cgtId = cgtId.replace("#", "");
                    selectedGroupIds.push(node.tags);
                },
                onNodeUnselected: function (event, node) {
                    //var cgtId = node.href;
                    //cgtId = cgtId.replace("#", "");
                    selectedGroupIds.splice(selectedGroupIds.indexOf(node.tags), 1);
                }
            });

            if (status != "dontShow") {
                $('#contentsGroup5_tw').treeview({   //view all content
                    data: response,
                    expandIcon: 'fa fa-plus-square',
                    collapseIcon: 'fa fa-minus-square',
                    onNodeSelected: function (event, node) {
                        var cgtId = node.href;
                        cgtId = cgtId.replace("#", "");
                        viewAllContents(parseInt(cgtId));
                    },
                });
            }

            $('#contentsGroup6_tw').treeview({   //edit content
                data: response,
                expandIcon: 'fa fa-plus-square',
                collapseIcon: 'fa fa-minus-square',
                multiSelect: true,
                onNodeSelected: function (event, node) {

                    selectedGroupIdsEdit.push(node.tags);
                },
                onNodeUnselected: function (event, node) {
                    var cgtId = node.href;
                    cgtId = cgtId.replace("#", "");
                    selectedGroupIdsEdit.splice(selectedGroupIdsEdit.indexOf(node.tags), 1);
                }
            });
        },
        error: function (response) {

        }
    });
}

function createContentGroup() {
    $.get("/Admin/Contents?handler=CreateGroup", function (result) {
        if (result.status == "error") {
            if (isDevelopment) {
                alert(result.data);
            } else {
                ToastMessage("خطایی رخ داده است", "info", "خطا");
            }
        }
        $(".modal-dialog").css('max-width', '75%');
        $(".modal-header").css('background-color', '#008000b8');
        $(".headerModalColor").text("افزودن گروه محتوا");
        $("#myModal").modal();
        $("#myModalBody").html(result);
    });
}

function insertContentGroup() {
    var frm = $("#contentGroupFrm").valid();
    if (frm == true) {

        /* if (contentGroupIdForParentId !=0) {*/
        $("#btnInsertContentGroup").children(".fa-spinner").removeClass('hide');
        $("#btnInsertContentGroup").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');

        var isactive = $('#IsActive:checked').length;
        var status = "";
        if (isactive === 1) { status = "true"; }
        else { status = "false"; }

        var contentGroup = {
            ParentId: contentGroupIdForParentId,
            IsActive: status,
            ContentImageWidth: $("#ContentImageWidth").val(),
            ContentImageHeight: $("#ContentImageHeight").val(),
        }
        $.ajax({
            method: 'POST',
            url: '/Admin/Contents?handler=CreateGroup',
            data: {
                contentGroup: contentGroup,
                __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
            },
            success: function (result) {
                $("#btnInsertContentGroup").children(".fa-spinner").addClass('hide');
                $("#btnInsertContentGroup").children(".fa-spinner").siblings('span').text('ویرایش');

                if (result.status == "ok") {

                    $(".insert-div").addClass('hide');
                    $(".update-div").removeClass('hide');

                    $("#ContentGroup_ID").val(result.groupId);
                    createContentGroupTranslation(result.groupId);
                }
                else {
                    if (isDevelopment) {
                        alert(result.data);
                    } else {
                        ToastMessage("خطایی رخ داده است", "info", "خطا");
                    }
                }
            },
            error: function (result) {
            }
        });
        //}
        //else {
        //    ToastMessage(" لطفا گروه را انتخاب انتخاب کنید", "info", "کاربر گرامی");
        //}
    }
    else {
        return false;
    }
}

function createContentGroupTranslation(Id) {
    $.get("/Admin/Contents?handler=CreateContentGroupTrans", { id: Id }, function (result) {
        $("#contentGroup_trans").html(result);

        switchTab();
    });
}

function insertContentGroupTranslation() {
    var frm = $("#contentGroupTransFrm").valid();
    if (frm == true) {
        if ($("#language_id").val() != "0") {
            
            $("#btnInsertContentGroupFinal").children(".fa-spinner").removeClass('hide');
            $("#btnInsertContentGroupFinal").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');
            var contentGroupTranslation = {
                Title: $("#Title").val(),
                Description: $("#Description").val(),
                ContentGroup_Id: $("#ContentGroup_Id").val(),
                Slug: $("#Slug").val(),
                Language_Id: $("#language_id").val(),
                parentId: contentGroupIdForParentId,
            }
            $.ajax({
                method: 'POST',
                url: '/Admin/Contents?handler=CreateContentGroupTrans',
                data: {
                    contentGroupTranslation: contentGroupTranslation,
                    __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
                },
                success: function (result) {
                    $(".fa-spinner").addClass('hide');
                    $(".fa-spinner").siblings('span').text('ثبت');

                    $(".insert-div").removeClass('hide');
                    $(".update-div").addClass('hide');

                    if (result.status == "ok") {
                        resetForm();
                        ToastMessage("گروه محتوا با موفقیت ثبت گردید", "success", "کاربر گرامی");
                        viewDetailsContentGroup(result.group_id);
                        getAllContentsGroupForTreeView($("#global_language_id").val());
                    }
                    else if (result == "exsist") {
                        ToastMessage(" این گروه  برای این زبان قبلا ثبت شده است", "info", "کاربر گرامی");
                    }
                    else {
                        if (isDevelopment) {
                            alert(result.data);
                        } else {
                            ToastMessage("خطایی رخ داده است", "warning", "خطا");
                        }
                    }
                },
                error: function (result) {
                }
            });
        }
        else {
            ToastMessage(" لطفا یک زبان را انتخاب کنید", "info", "کاربر گرامی");
        }
    }
    else {
        return false;
    }
}

function viewDetailsContentGroup(group_id) {
    $.get("/Admin/Contents?handler=ViewDetailsContentGroup", { groupId: group_id }, function (response) {
        $("#contentGroupDetails").html(response);
    });
}

//$("#contentGroup_tb").on("click", ".btnContentGroupEdit", function () {
function editContentGroup(contentGroupId, Id) {
    var contentGroupId = contentGroupId;//$(this).attr('data-contentGroupId');
    var Id = Id;// $(this).attr('data-id');
    $.get("/Admin/Contents?handler=EditContentGroup", { id: contentGroupId }, function (result) {
        if (result.status == "error") {
            if (isDevelopment) {
                alert(result.data);
            } else {
                ToastMessage("خطایی رخ داده است", "info", "خطا");
            }
        }
        else {

            $(".modal-dialog").css('max-width', '75%');
            $(".modal-header").css('background-color', '#008074b8');
            $(".headerModalColor").text("ویرایش گروه محتوا");
            $("#myModal").modal();
            $("#myModalBody").html(result);
            editContentGroupTranslation(Id);

        }
    });
};

function editContentGroupTranslation(Id) {
    
    $.get("/Admin/Contents?handler=EditContentGroupTrans", { id: Id }, function (res) {
        if (result.status == "error") {
            if (isDevelopment) {
                alert(result.data);
            } else {
                ToastMessage("خطایی رخ داده است", "info", "خطا");
            }
        }
        else {
            $("#contentGroup_trans").html(res);
            //getContentGroupInfoWithLanguage('', Id);
        }
    });
}

function getContentGroupInfoWithLanguage(langId, contentGroupId) {

    $.get("/Admin/Contents?handler=ContentGroupTransInfo", { id: langId, contentGroupId: contentGroupId }, function (res) {
        if (res.status == "ok") {
            $(".update_div").removeClass('hide');
            $(".insert_div").addClass('hide');

            $("#Title").val(res.title);
            $("#Slug").val(res.slug);
            $("#Description").val(res.description);
            $("#Title").attr('data-id', res.id);
        }
        else {
            resetForm2();
            $(".update_div").addClass('hide');
            $(".insert_div").removeClass('hide');
        }
    });
}

function updateContentGroup() {
    var frm = $("#contentGroupFrm").valid();
    if (frm == true) {

        $("#btnUpdateContentGroup").children(".fa-spinner").removeClass('hide');
        $("#btnUpdateContentGroup").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');

        var isactive = $('#IsActive:checked').length;
        var status = "";
        if (isactive === 1) { status = "true"; }
        else { status = "false"; }
        var parentId = 0;
        var content_group_id = $("#ContentGroup_ID").val();
        if (contentGroupIdForParentIdEdit != 0) {
            parentId = contentGroupIdForParentIdEdit;
        }
        else {
            parentId = $("#ParentId").val();
        }
        var contentGroup = {
            ContentImageWidth: $("#ContentImageWidth").val(),
            ContentImageHeight: $("#ContentImageHeight").val(),
            ContentGroup_ID: content_group_id,
            ParentId: parentId,
            IsActive: status,
        }


        $.ajax({
            method: 'POST',
            url: '/Admin/Contents?handler=EditContentGroup',
            data: {
                contentGroup: contentGroup,
                __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
            },
            success: function (result) {
                $("#btnUpdateContentGroup").children(".fa-spinner").addClass('hide');
                $("#btnUpdateContentGroup").children(".fa-spinner").siblings('span').text('ویرایش');

                if (result == "ok") {
                    viewDetailsContentGroup(content_group_id);
                    switchTab();
                }
                else {
                    if (isDevelopment) {
                        alert(result.data);
                    } else {
                        ToastMessage("خطایی رخ داده است", "info", "خطا");
                    }
                }
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}

function updateContentGroupTranslation() {
    var frm = $("#contentGroupTransFrm").valid();
    if (frm == true) {
        if ($("#language_id").val() != "0") {
            $("#btnUpdateContentGroupTrans").children(".fa-spinner").removeClass('hide');
            $("#btnUpdateContentGroupTrans").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');

            var content_group_id = $("#ContentGroup_Id").val();

            if (contentGroupIdForParentIdEdit != 0) {
                parentId = contentGroupIdForParentIdEdit;
            }
            else {
                parentId = $("#ParentId").val();
            }

            var contentGroupTranslation = {
                ContentGroupTranslation_ID: $("#Title").attr('data-id'),
                Title: $("#Title").val(),
                Description: $("#Description").val(),
                ContentGroup_Id: content_group_id,
                Slug: $("#Slug").val(),
                Language_Id: $("#language_id").val(),
                SiteMap_Id: $("#SiteMap_Id").val(),
                parentId: parentId,
            }

            $.ajax({
                method: 'POST',
                url: '/Admin/Contents?handler=EditContentGroupTrans',
                data: {
                    contentGroupTranslation: contentGroupTranslation,
                    __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
                },
                success: function (result) {
                    $("#btnUpdateContentGroupTrans").children(".fa-spinner").addClass('hide');
                    $("#btnUpdateContentGroupTrans").children(".fa-spinner").siblings('span').text('ویرایش');

                    if (result == "ok") {
                        resetForm();
                        ToastMessage("گروه محتوا با موفقیت ویرایش گردید", "success", "کاربر گرامی");
                        viewDetailsContentGroup(content_group_id);
                    }
                    //else if (result == "exsist") {
                    //    ToastMessage(" این کد رنگ برای این زبان قبلا ثبت شده است", "info", "کاربر گرامی");
                    //}
                    else {
                        if (isDevelopment) {
                            alert(result.data);
                        } else {
                            ToastMessage("خطایی رخ داده است", "warning", "خطا");
                        }
                    }
                },
                error: function (result) {
                }
            });
        }
        else {
            ToastMessage(" لطفا یک زبان را انتخاب کنید", "info", "کاربر گرامی");
        }

    }
    else {
        return false;
    }
}

function deleteContentGroup(contentGroupId, Id) {
    var contentGroupId = contentGroupId;
    var Id = Id;
    $.confirm({
        title: 'آیا برای حذف گروه محتوا مطمئن هستید؟', icon: 'fa fa-warning', theme: "material", animation: 'scale', content: '',
        buttons: {
            confirm: {
                text: 'تایید',
                btnClass: 'btn-blue',
                action: function () {
                    $.get("/Admin/Contents?handler=DeleteContentGroup", { id: Id, contentGroupId: contentGroupId, status: "one" },
                        function (result) {
                            if (result == "ok") {
                                ToastMessage("گروه محتوا با موفقیت حذف گردید", "success", "کاربر گرامی");
                                //$("#contentGroup_tb").empty();
                                getAllContentsGroupForTreeView($("#global_language_id").val());
                            }
                            else if (result == "exists") {
                                ToastMessage("برای این گروه محتوا ثبت شده است", "warning", "کاربر گرامی");
                            }
                            else {
                                if (isDevelopment) {
                                    alert(result.data);
                                } else {
                                    ToastMessage("خطایی رخ داده است", "warning", "خطا");
                                }
                            }
                        });
                }
            },
            cancel: {
                text: 'انصراف',

            }
        },
    });
}

function switchTab() {
    $("#tab_1").removeClass("active");
    $("#contentGroup_trans").addClass("active");

    $("a[href='#tab_1']").removeClass('active');
    $("a[href='#contentGroup_trans']").addClass('active');
}

function resetForm() {
    $("#language_id").val('0');
    $("#Title").val('');
    $("#Slug").val('');
    $("#Description").val('');
}

function resetForm2() {
    $("#Title").val('');
    $("#Slug").val('');
    $("#Description").val('');
}

/////
function viewAllContents(contentGroupTransId) {
    loadingPageShow();
    $("#content_tb").DataTable({
        "pageLength": 5,
        "lengthMenu": [[5, 10, 20, -1], [5, 10, 20, 'Todos']],
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.24/i18n/Persian.json"
        },
        "processing": true,
        "serverSide": true,
        "filter": true,
        "destroy": true,   /// allow reinitialize datattable
        "ajax": {
            "url": "/Admin/Contents?handler=ViewAllContents",
            "type": "POST",
            "datatype": "json",
            "data":
            {
                contentGroupTranslationId: contentGroupTransId,
                __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
            },
        },

        "columns": [
            {
                data: "contentTranslation.content.image",
                render: function (data) {
                    return '<img width="60" height="60" src="/' + data + '">';
                },
            },
            { "data": "contentTranslation.title", "name": "contentTranslation.title", "autoWidth": true },
            {
                data: "contentTranslation.content.isActive",
                render: function (data) {
                    if (data) {
                        return '<span class="text-success">فعال</span >';
                    }
                    else {
                        return '<span class="text-danger">غیر فعال</span >';
                    }
                },
            },

            {
                data: "contentTranslation.content.createDate",
                render: function (data) {
                    return new persianDate(new Date(data)).format("L");

                }, name: "contentTranslation.content.createDate",
            },
            {
                mRender: function (data, type, row) {
                    return '<div class="btn-group" role="group">' +
                        '<span class="btn btn-outline-info" onclick="editContent(' + row.contentTranslation.content_Id + ',' + row.contentTranslation_Id + ')"><i class="fa fa-edit fa-sm"></i></span>' +
                        '<span class="btn btn-outline-success" onclick="getContentStatistics(' + row.contentTranslation_Id + ')"><i class="fa fa-bar-chart fa-sm"></i></span>' +
                        '<span class="btn btn-outline-danger" onclick="deleteContent(' + row.contentTranslation.content_Id + ',' + row.contentTranslation_Id + ')"><i class="fa fa-trash fa-sm"></i></span></div>';
                }
            },
        ]

    });
    loadingPageHide();
}