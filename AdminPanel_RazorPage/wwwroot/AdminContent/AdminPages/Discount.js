﻿var isDevelopment = true;


function createDiscount() {
    $.get("/Admin/Discounts?handler=Create", function (result) {
        if (result.status == "error") {
            if (isDevelopment) {
                alert(result.msg);
            } else {
                ToastMessage("خطایی رخ داده است", "info", "خطا");
            }
        }
        $(".modal-dialog").css('max-width', '80%');
        $(".modal-header").css('background-color', '#008000b8');
        $(".headerModalColor").text("افزودن تخفیف");
        $("#myModal").modal();
        $("#myModalBody").html(result);
    });
}

function insertDiscount() {
    var frm = $("#discountFrm").valid();
    if (frm == true) {
        $("#btnInsertDiscount").children(".fa-spinner").removeClass('hide');
        $("#btnInsertDiscount").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');
        var isactive = $('#IsActive:checked').length;
        var status = "";
        if (isactive === 1) { status = "true"; }
        else { status = "false"; }

        var isRandom = $('#IsRandom:checked').length;
        var IsRandom_status = "";
        if (isRandom === 1) { IsRandom_status = "true"; }
        else { IsRandom_status = "false"; }

        var unLimitUse = $('#UnLimitUse:checked').length;
        var unLimitUse_status = "";
        if (unLimitUse === 1) { unLimitUse_status = "true"; }
        else { unLimitUse_status = "false"; }

        var unLimitTime = $('#UnLimitTime:checked').length;
        var unLimitTime_status = "";
        if (unLimitTime === 1) { unLimitTime_status = "true"; }
        else { unLimitTime_status = "false"; }


        var discount = {
            Title: $("#Title").val(),
            Percent: $("#Percent").val(),
            CountOff: $("#CountOff").val(),
            ExtraKeyCharCount: $("#ExtraChar").val(),
            RandomType: $("#RandomType").val(),
            Language_Id: $("#global_language_id").val(),
            UnLimitTime: unLimitTime_status,
            UnLimitUse: unLimitUse_status,
            IsRandom: IsRandom_status,
            IsActive: status,
        }
        $.ajax({
            method: 'POST',
            url: '/Admin/Discounts?handler=Create',
            data: {
                discount: discount,
                sdate: $("#tarikhAlt1").val(),
                eDate: $("#tarikhAlt2").val(),
                __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
            },
            success: function (result) {
                $(".fa-spinner").addClass('hide');
                $(".fa-spinner").siblings('span').text('ثبت');

                if (result == "ok") {

                    LoadDatatable("#discount_tb", false);
                    $("#myModal").modal("hide");
                    ToastMessage("تخفیف با موفقیت ثبت گردید", "success", "کاربر گرامی");
                    clearForm();
                }
                else if (result.res == "invalidDate") {
                    ToastMessage(" تاریخ پایان باید بعد از تاریخ شروع باشد", "info", "کاربر گرامی");
                }
                //else if (result == "exists") {
                //    ToastMessage(" این تخفیف قبلا ثبت شده است", "info", "کاربر گرامی");
                //}
                else {
                    if (isDevelopment) {
                        alert(result.data);
                    } else {
                        ToastMessage("خطایی رخ داده است", "info", "خطا");
                    }
                }
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}

function viewAllDiscounts() {
    loadingPageShow();
    $("#discount_tb").DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.24/i18n/Persian.json"
        },
        "processing": true,
        "serverSide": true,
        "filter": true,
        "ajax": {
            "url": "/Admin/Discounts?handler=ViewAllDiscounts",
            "type": "POST",
            "datatype": "json",
            "data": {
                __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
            },
        },

        "columns": [
            { "data": "title", "name": "Title" },
            { "data": "percent", "name": "Percent" },
            {
                data: "startDate",
                render: function (data) {
                    if (data != null) {
                        return new persianDate(new Date(data)).format("L");
                    }
                    else {
                        return '-';
                    }
                },
            },
            {
                data: "endDate",
                render: function (data) {
                    if (data != null) {
                        return new persianDate(new Date(data)).format("L");
                    }
                    else {
                        return '-';
                    }
                },
            },
            {
                data: "isActive",
                render: function (data) {
                    if (data) {
                        return '<span class="text-success">فعال</span >';
                    }
                    else {
                        return '<span class="text-danger">غیر فعال</span >';
                    }
                },
            },
            {
                data: "unLimitTime",
                render: function (data) {
                    if (data) {
                        return '<span class="text-primary">نامحدود</span >';
                    }
                    else {
                        return '<span class="text-warning">محدود</span >';
                    }
                },
            },
            {
                data: "unLimitUse",
                render: function (data) {
                    if (data) {
                        return '<span class="text-primary">نامحدود</span >';
                    }
                    else {
                        return '<span class="text-warning">محدود</span >';
                    }
                },
            },
            {
                mRender: function (data, type, row) {
                    if (row.isRandom) {
                        return ' <span class="badge badge-success">' + row.subDiscounts.length + ' تصادفی</span>';
                    }
                    else if (row.unLimitUse) {
                        return ' <span class="badge badge-dark"> نامحدود </span>';
                    }
                    else {
                        return ' <span class="badge badge-success">' + row.countOff + ' ثابت</span>';
                    }
                },
            },
            {
                mRender: function (data, type, row) {
                    var htmlButtons='<div class="btn-group" role="group">' +
                        '<span class="btn btn-outline-info" onclick="editDiscount(' + row.discount_ID + ')"><i class="fa fa-edit fa-sm"></i></span>';
                    if (row.isRandom) {
                        htmlButtons +='<span class="btn btn-outline-aqua tooltips" data-placement="top" title="لیست کدهای تخفیف" onclick="editSubDiscount(' + row.discount_ID + ')"><i class="fa fa-list fa-sm"></i></span>';
                    }
                    else {
                        htmlButtons += '<span class="btn btn-outline-aqua disabled not-allow-cursor tooltips" data-placement="top" title="کد تخفیف ندارد"><i class="fa fa-list fa-sm"></i></span>';
                    }
                    htmlButtons += '<span class="btn btn-outline-danger" onclick="deleteDiscount(' + row.discount_ID + ')"><i class="fa fa-trash fa-sm"></i></span></div>';
                    return htmlButtons;
                }
            },
        ]

    });
    loadingPageHide();
}

function editDiscount(Id) {
    $.get("/Admin/Discounts?handler=Edit", { id: Id }, function (result) {
        if (result.status == "error") {
            if (isDevelopment) {
                alert(result.msg);
            } else {
                ToastMessage("خطایی رخ داده است", "info", "خطا");
            }
        }
        $(".modal-dialog").css('max-width', '80%');
        $(".modal-header").css('background-color', '#008074b8');
        $(".headerModalColor").text("ویرایش تخفیف");
        $("#myModal").modal();
        $("#myModalBody").html(result);
    });
}

function updateDiscount() {
    var frm = $("#discountFrm").valid();
    if (frm == true) {
        $("#btnUpdateDiscount").children(".fa-spinner").removeClass('hide');
        $("#btnUpdateDiscount").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');
        var isactive = $('#IsActive:checked').length;
        var status = "";
        if (isactive === 1) { status = "true"; }
        else { status = "false"; }

        var isRandom = $('#IsRandom:checked').length;
        var IsRandom_status = "";
        if (isRandom === 1) { IsRandom_status = "true"; }
        else { IsRandom_status = "false"; }

        var unLimitUse = $('#UnLimitUse:checked').length;
        var unLimitUse_status = "";
        if (unLimitUse === 1) { unLimitUse_status = "true"; }
        else { unLimitUse_status = "false"; }

        var unLimitTime = $('#UnLimitTime:checked').length;
        var unLimitTime_status = "";
        if (unLimitTime === 1) { unLimitTime_status = "true"; }
        else { unLimitTime_status = "false"; }


        var discount = {
            Discount_ID: $("#Discount_ID").val(),
            Title: $("#Title").val(),
            Percent: $("#Percent").val(),
            CountOff: $("#CountOff").val(),
            CountUse: $("#CountUse").val(),
            ExtraKeyCharCount: $("#ExtraChar").val(),
            RandomType: $("#RandomType").val(),
            Language_Id: $("#global_language_id").val(),
            UnLimitTime: unLimitTime_status,
            UnLimitUse: unLimitUse_status,
            IsRandom: IsRandom_status,
            IsActive: status,
        }
        $.ajax({
            method: 'POST',
            url: '/Admin/Discounts?handler=Edit',
            data: {
                discount: discount,
                sdate: $("#tarikhAlt1").val(),
                eDate: $("#tarikhAlt2").val(),
                __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
            },
            success: function (result) {
                $(".fa-spinner").addClass('hide');
                $(".fa-spinner").siblings('span').text('ویرایش');

                if (result == "ok") {

                    LoadDatatable("#discount_tb", false);
                    $("#myModal").modal("hide");
                    ToastMessage("تخفیف با موفقیت ویرایش گردید", "success", "کاربر گرامی");
                    clearForm();
                }
                else if (result.res == "invalidDate") {
                    ToastMessage(" تاریخ پایان باید بعد از تاریخ شروع باشد", "info", "کاربر گرامی");
                }
                else if (result == "noAccess") {
                    ToastMessage(" تعداد تخفیف باید از تعداد تخفیف استفاده شده بیشتر باشد", "info", "کاربر گرامی");
                }
                //else if (result == "exists") {
                //    ToastMessage(" این تخفیف قبلا ثبت شده است", "info", "کاربر گرامی");
                //}
                else {
                    if (isDevelopment) {
                        alert(result.data);
                    } else {
                        ToastMessage("خطایی رخ داده است", "info", "خطا");
                    }
                }
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}

function editSubDiscount(discount_Id) {
    $.get("/Admin/Discounts?handler=SubDiscount", { discountId: discount_Id }, function (result) {
        if (result.status == "error") {
            if (isDevelopment) {
                alert(result.msg);
            } else {
                ToastMessage("خطایی رخ داده است", "info", "خطا");
            }
        }
        $(".modal-dialog").css('max-width', '60%');
        $(".modal-header").css('background-color', '#0cb18b');
        $(".headerModalColor").text("لیست تخفیف ها");
        $("#myModal").modal();
        $("#myModalBody").html(result);
    });
}

function deleteDiscount(discountId) {
    $.confirm({
        title: 'آیا برای حذف این تخفیف مطمئن هستید؟', icon: 'fa fa-warning', theme: 'material', animation: 'scale', content: '',
        buttons: {
            confirm: {
                text: 'تایید',
                btnClass: 'btn-blue',
                action: function () {

                    $.get("/Admin/Discounts?handler=Delete", { discountId: discountId }, function (result) {
                        if (result == "ok") {
                            LoadDatatable("#discount_tb", false);
                            ToastMessage("تخفیف با موفقیت حذف گردید", "success", "کاربر گرامی");
                        }
                        else if (result == "existsInSub") {
                            ToastMessage("این تخفیف استفاده شده است و قابل حذف نیست", "info", "کاربر گرامی");
                        }
                        else if (result.status == "error") {
                            if (isDevelopment) {
                                alert(result.data);
                            } else {
                                ToastMessage("خطایی رخ داده است", "info", "خطا");
                            }
                        }
                    });
                }
            },
            cancel: {
                text: 'انصراف',
            }
        },
    });

}


