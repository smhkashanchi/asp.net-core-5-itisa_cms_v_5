﻿var isDevelopment = true;
///Toast Message
var ToastMessage = function (message, type, title) {
    var msgs = [message];
    var $toast = toastr[type](msgs, title);
    $toastlast = $toast;
    //return msgs;
};
toastr.options = {
    closeButton: 'true',
    progressBar: 'true',
    positionClass: 'toast-top-left',
};

function createConnectionGroup() {
    $.get("/Admin/Connections?handler=CreateGroup", function (result) {
        $(".modal-dialog").css('max-width', '50%');
        $(".modal-header").css('background-color', '#008000b8');
        $(".headerModalColor").text("افزودن گروه");
        $("#myModal").modal();
        $("#myModalBody").html(result);
    });
}

function insertConnectionGroup() {
    var frm = $("#connectionGroupFrm").valid();
    if (frm == true) {

        $("#btnInsertConnectionGroup").children(".fa-spinner").removeClass('hide');
        $("#btnInsertConnectionGroup").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');

        var isactive = $('#IsActive:checked').length;
        var status = "";
        if (isactive === 1) { status = "true"; }
        else { status = "false"; }

        var connectionGroup = {
            Title: $("#Title").val(),
            Slug: $("#Slug").val(),
            IsActive: status,
        }
        $.ajax({
            method: 'POST',
            url: '/Admin/Connections?handler=CreateGroup',
            data: {
                connectionGroup: connectionGroup,
                __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
            },
            success: function (result) {
                $(".fa-spinner").addClass('hide');
                $(".fa-spinner").siblings('span').text('ثبت');

                if (result== "ok") {
                    LoadDatatable("#connectionGroup_tb", true);
                    $("#myModal").modal("hide");
                    ToastMessage("گروه راه های ارتباطی با موفقیت ثبت گردید", "success", "کاربر گرامی");
                }
                else {
                    if (isDevelopment) {
                        alert(result.data);
                    } else {
                        ToastMessage("خطایی رخ داده است", "info", "خطا");
                    }
                }
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}

function viewAllConnectionGroups() {
    $("#connectionGroup_tb").DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.24/i18n/Persian.json"
        },
        "processing": true,
        "serverSide": true,
        "filter": true,
        "ajax": {
            "url": "/Admin/Connections?handler=ViewAllGroups",
            "type": "POST",
            "datatype": "json",
            "data": {__RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()},
        },

        "columns": [
            { "data": "title", "name": "Title" },
            {
                data: "isActive",
                render: function (data) {
                    if (data) {
                        return '<span class="text-success">فعال</span >';
                    }
                    else {
                        return '<span class="text-danger">غیر فعال</span >';
                    }
                },
            },
            {
                mRender: function (data, type, row) {
                    return '<div class="btn-group" role="group">' +
                        '<span class="btn btn-outline-info btnConnectionGroupEdit" onclick="editConnectionGroup('+row.id+')" data-id="' + row.id + '"><i class="fa fa-edit fa-sm"></i></span>' +
                        '<span class="btn btn-outline-danger btnConnectionGroupDelete" onclick="deleteConnectionGroup(' + row.id +')" data-id="' + row.id + '"><i class="fa fa-trash fa-sm"></i></span></div>';
                }
            },
        ]

    });
    loadingPage();
}

//$("#connectionGroup_tb").on("click", " .btnConnectionGroupEdi3t", function () {
function editConnectionGroup(connectionGroupId) {
var Id = connectionGroupId;//$(this).attr('data-id');
    $.get("/Admin/Connections?handler=EditGroup", { id: Id }, function (result) {
        if (result.status == "error") {
            if (isDevelopment) {
                alert(result.data);
            } else {
                ToastMessage("خطایی رخ داده است", "info", "خطا");
            }
        }
        $(".modal-dialog").css('max-width', '50%');
        $(".modal-header").css('background-color', '#008074b8');
        $(".headerModalColor").text("ویرایش گروه");
        $("#myModal").modal();
        $("#myModalBody").html(result);
    });
}

function updateConnectionGroup() {
    var frm = $("#connectionGroupFrm").valid();
    if (frm == true) {

        $("#btnEditConnectionGroup").children(".fa-spinner").removeClass('hide');
        $("#btnEditConnectionGroup").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');

        var isactive = $('#IsActive:checked').length;
        var status = "";
        if (isactive === 1) { status = "true"; }
        else { status = "false"; }

        var connectionGroup = {
            Id: $("#Id").val(),
            Title: $("#Title").val(),
            Slug: $("#Slug").val(),
            IsActive: status,
        }
        $.ajax({
            method: 'POST',
            url: '/Admin/Connections?handler=EditGroup',
            data: {
                connectionGroup: connectionGroup,
                __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
            },
            success: function (result) {
                $(".fa-spinner").addClass('hide');
                $(".fa-spinner").siblings('span').text('ویرایش');

                if (result == "ok") {
                    LoadDatatable("#connectionGroup_tb", false);
                    $("#myModal").modal("hide");
                    ToastMessage("گروه راه های ارتباطی با موفقیت ویرایش گردید", "success", "کاربر گرامی");
                }
                else {
                    if (isDevelopment) {
                        alert(result.data);
                    } else {
                        ToastMessage("خطایی رخ داده است", "info", "خطا");
                    }
                }
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}

function deleteConnectionGroup(connctionGroupId) {
    var Id = connctionGroupId;// $(this).attr('data-id');
    $.confirm({
        title: 'آیا برای حذف گروه مطمئن هستید؟', icon: 'fa fa-warning',theme:'material', animation: 'scale', content: '',
        buttons: {
            confirm: {
                text: 'تایید',
                btnClass: 'btn-blue',
                action: function () {

                    $.get("/Admin/Connections?handler=DeleteGroup", { id: Id }, function (result) {
                        if (result == "ok") {
                            var table = $('#connectionGroup_tb').DataTable();
                            table.ajax.reload(null, false);
                            ToastMessage("گروه راه های ارتباطی با موفقیت حذف گردید", "success", "کاربر گرامی");
                        }
                        else if (result == "exists") {
                            ToastMessage("این گروه شامل راه ارتباطی می باشد", "warning", "کاربر گرامی");
                        }
                        else if (result.status =="error") {
                            if (isDevelopment) {
                                alert(result.data);
                            } else {
                                ToastMessage("خطایی رخ داده است", "info", "خطا");
                            }
                        }
                    });
                }
            },
            cancel: {
                text: 'انصراف',
            }
        },
    });

}
