﻿var isDevelopment = true;

function createGallery() {
    $.get("/Admin/Galleries?handler=CreateGallery", function (result) {
        if (result.status == "error") {
            if (isDevelopment) {
                alert(result.data);
            } else {
                ToastMessage("خطایی رخ داده است", "info", "خطا");
            }
        }
        $(".modal-dialog").css('max-width', '65%');
        $(".modal-header").css('background-color', '#008000b8');
        $(".headerModalColor").text("افزودن گالری");
        $("#myModal").modal();
        $("#myModalBody").html(result);
    });
}

function insertGallery() {
    var frm = $("#galleryFrm").valid();
    if (frm == true) {

        $("#btnInsertGallery").children(".fa-spinner").removeClass('hide');
        $("#btnInsertGallery").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');

        var gallery = new FormData();
        var files = $("#Image").get(0).files;

            gallery.append("imgGallery", files[0])

        $.ajax({
            method: 'POST',
            url: '/Admin/Galleries?handler=CreateGallery',
            contentType: false,
            processData: false,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            data: gallery,

            success: function (result) {
                $("#btnInsertGallery").children(".fa-spinner").addClass('hide');
                $("#btnInsertGallery").children(".fa-spinner").siblings('span').text('ویرایش');

                if (result.status == "ok") {

                    $(".insert-div").addClass('hide');
                    $(".update-div").removeClass('hide');

                    $("#Gallery_ID").val(result.galleryId);
                    createGalleryTranslation(result.galleryId);
                }
                else {
                    if (isDevelopment) {
                        alert(result.data);
                    } else {
                        ToastMessage("خطایی رخ داده است", "info", "خطا");
                    }
                }
            },
            error: function (result) {
            }
        });

    }
    else {
        return false;
    }
}

function createGalleryTranslation(galleryId) {
    $.get("/Admin/Galleries?handler=CreateGalleryTrans", { id: galleryId }, function (result) {
        
        $("#gallery_trans").html(result);
        $("#tab_1").removeClass("active");
        $("#gallery_trans").addClass("active");

        $("a[href='#tab_1']").removeClass('active');
        $("a[href='#gallery_trans']").addClass('active');
    });
}

function insertGalleryTranslation() {
    var frm = $("#galleryTransFrm").valid();
    if (frm == true) {
        if ($("#language_id").val() != "0") {

            $("#btnInsertGalleryFinal").children(".fa-spinner").removeClass('hide');
            $("#btnInsertGalleryFinal").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');

            var isactive = $('#IsActive:checked').length;
            var status = "";
            if (isactive === 1) { status = "true"; }
            else { status = "false"; }
            var galleryTranslation = {
                Gallery_Id: $("#Gallery_Id").val(),
                GroupTranslation_Id: $("#GroupTranslation_Id option:selected").attr('data-id'),
                Title: $("#Title").val(),
                Slug: $("#Slug").val(),
                Language_Id: $("#language_id").val(),
                IsActive: status
            }
            $.ajax({
                method: 'POST',
                url: '/Admin/Galleries?handler=CreateGalleryTrans',
                data: {
                    galleryTranslation: galleryTranslation,
                    __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
                },
                success: function (result) {
                    $(".fa-spinner").addClass('hide');
                    $(".fa-spinner").siblings('span').text('ثبت');

                    $(".insert-div").removeClass('hide');
                    $(".update-div").addClass('hide');

                    if (result == "ok") {
                        resetForm();
                        ToastMessage("گالری با موفقیت ثبت گردید", "success", "کاربر گرامی");
                        LoadDatatable("#gallery_tb", true);
                    }
                    else if (result == "exsist") {
                        ToastMessage(" این گالری  برای این زبان قبلا ثبت شده است", "info", "کاربر گرامی");
                    }
                    else {
                        if (isDevelopment) {
                            alert(result.data);
                        } else {
                            ToastMessage("خطایی رخ داده است", "warning", "خطا");
                        }
                    }
                },
                error: function (result) {
                }
            });
        }
        else {
            ToastMessage(" لطفا یک زبان را انتخاب کنید", "info", "کاربر گرامی");
        }
    }
    else {
        return false;
    }
}

function viewAllGalleries(GroupId) {
    $("#gallery_tb").DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.24/i18n/Persian.json"
        },
        "processing": true,
        "serverSide": true,
        "filter": true,
        "destroy": true,   /// allow reinitialize datattable
        "ajax": {
            "url": "/Admin/Galleries?handler=ViewAllGalleries",
            "type": "POST",
            "datatype": "json",
            "data": {
                groupId: GroupId,
                __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
            },
        },

        "columns": [
            {
                data: "gallery.image",
                render: function (data) {
                    var imgSrc = "";
                    if (data != undefined)
                        imgSrc = "/" + data+"";
                    else
                        imgSrc = "/Files/Images/no-image.png";

                    return '<img width="60" height="60" src="' + imgSrc + '">';
                },
            },
            { "data": "title", "name": "title", "autoWidth": true },
            { "data": "galleryGroupTranslation.title", "name": "galleryGroupTranslation.title", "autoWidth": true },
            {
                data: "isActive",
                render: function (data) {
                    if (data) {
                        return '<span class="text-success">فعال</span >';
                    }
                    else {
                        return '<span class="text-danger">غیر فعال</span >';
                    }
                },
            },
            {
                mRender: function (data, type, row) {
                    return '<div class="btn-group" role="group">' +
                        '<span class="btn btn-outline-info" onclick="editGallery(' + row.gallery_Id + ',' + row.galleryTranslation_ID + ')" ><i class="fa fa-edit fa-sm"></i></span>' +
                        '<span class="btn btn-outline-secondary" onclick="CreateGalleryPic(' + row.gallery_Id + ')"><i class="fa fa-picture-o fa-sm"></i></span>'+
                        '<span class="btn btn-outline-danger" onclick="deleteGallery(' + row.gallery_Id + ',' + row.galleryTranslation_ID + ')"><i class="fa fa-trash fa-sm"></i></span></div>';
                }
            },
        ]

    });
    loadingPage();
}

function editGallery(galleryId, translationId) {
    $.get("/Admin/Galleries?handler=EditGallery", { id: galleryId }, function (result) {
        if (result.status == "error") {
            if (isDevelopment) {
                alert(result.data);
            } else {
                ToastMessage("خطایی رخ داده است", "info", "خطا");
            }
        }
        else {
            $(".modal-dialog").css('max-width', '65%');
            $(".modal-header").css('background-color', '#008074b8');
            $(".headerModalColor").text("ویرایش گالری");
            $("#myModal").modal();
            $("#myModalBody").html(result);
            editGalleryTranslation(translationId);
        }
    });
}

function editGalleryTranslation(translationId) {
    $.get("/Admin/Galleries?handler=EditGalleryTrans", { id: translationId }, function (res) {
        $("#gallery_trans").html(res);
        //getGalleryTransInfoWithLanguage('', translationId);
    });
}

function getGalleryTransInfoWithLanguage(langId, galleryId) {
    $.get("/Admin/Galleries?handler=GalleryTransInfo", { id: langId, galleryId: galleryId }, function (res) {
        if (res.status == "ok") {

            $(".update_div").removeClass('hide');
            $(".insert_div").addClass('hide');

            getGalleryGroupByLang_Global2($("#language_id").val(), "#GroupTranslation_Id", res.groupId, "");

            $("#Title").val(res.title);
            $("#Slug").val(res.slug);
            $("#GalleryTranslation_ID").val(res.id);
            if (res.isActive)
                $(".icheckbox_square-blue").addClass('checked');
            else
                $(".icheckbox_square-blue").removeClass('checked');
        }
        else {
            resetForm2();
            $(".update_div").addClass('hide');
            $(".insert_div").removeClass('hide');
            getGalleryGroupByLang_Global2($("#language_id").val(), "#GroupTranslation_Id", "", "");
        }
    });
}

function updateGallery() {
    var frm = $("#galleryFrm").valid();
    if (frm == true) {

        $("#btnUpdateGallery").children(".fa-spinner").removeClass('hide');
        $("#btnUpdateGallery").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');

        var gallery = new FormData();
        var files = $("#Image").get(0).files;


        gallery.append("Gallery_ID", $("#Gallery_ID").val()),
            gallery.append("Image", $("#oldImage").val()),
        gallery.append("imgGallery", files[0])

        $.ajax({
            method: 'POST',
            url: '/Admin/Galleries?handler=EditGallery',
            contentType: false,
            processData: false,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            data: gallery,
            success: function (result) {
                $("#btnUpdateGallery").children(".fa-spinner").addClass('hide');
                $("#btnUpdateGallery").children(".fa-spinner").siblings('span').text('ویرایش');

                if (result == "ok") {
                    LoadDatatable("#gallery_tb", false);
                    ToastMessage("اطلاعات مشترک گالری با موفقیت ویرایش گردید", "success", "کاربر گرامی");
                    //$("#Image").val(null);
                }
                else {
                    if (isDevelopment) {
                        alert(result.data);
                    } else {
                        ToastMessage("خطایی رخ داده است", "info", "خطا");
                    }
                }
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}

function updateGalleryTranslation() {
    var frm = $("#galleryTransFrm").valid();
    if (frm == true) {
        if ($("#language_id").val() != "0") {
            $("#btnUpdateGalleryTrans").children(".fa-spinner").removeClass('hide');
            $("#btnUpdateGalleryTrans").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');

            var isactive = $('#IsActive:checked').length;
            var status = "";
            if (isactive === 1) { status = "true"; }
            else { status = "false"; }

            var galleryTranslation = {
                IsActive: status,
                Gallery_Id: $("#Gallery_Id").val(),
                GalleryTranslation_ID: $("#GalleryTranslation_ID").val(),
                GroupTranslation_Id: $("#GroupTranslation_Id option:selected").attr('data-id'),
                Slug: $("#Slug").val(),
                Title: $("#Title").val(),
                Language_Id: $("#language_id").val(),
            }

            $.ajax({
                method: 'POST',
                url: '/Admin/Galleries?handler=EditGalleryTrans',
                data: {
                    galleryTranslation: galleryTranslation,
                    __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
                },
                success: function (result) {
                   $(".fa-spinner").addClass('hide');
                   $(".fa-spinner").siblings('span').text('ویرایش');

                    if (result == "ok") {
                        resetForm();
                        ToastMessage("گالری با موفقیت ویرایش گردید", "success", "کاربر گرامی");
                        LoadDatatable("#gallery_tb", false);
                    }
                    //else if (result == "exsist") {
                    //    ToastMessage(" این کد رنگ برای این زبان قبلا ثبت شده است", "info", "کاربر گرامی");
                    //}
                    else {
                        if (isDevelopment) {
                            alert(result.data);
                        } else {
                            ToastMessage("خطایی رخ داده است", "warning", "خطا");
                        }
                    }
                },
                error: function (result) {
                }
            });
        }
        else {
            ToastMessage(" لطفا یک زبان را انتخاب کنید", "info", "کاربر گرامی");
        }

    }
    else {
        return false;
    }
}

function deleteGallery(galleryId, translationId) {
    $.confirm({
        title: 'آیا برای حذف گالری مطمئن هستید؟', icon: 'fa fa-warning', theme: "material", animation: 'scale', content: '',
        buttons: {
            confirm: {
                text: 'تایید',
                btnClass: 'btn-blue',
                action: function () {
                    $.get("/Admin/Galleries?handler=DeleteGallery", { id: translationId, galleryId: galleryId, status: "one" },
                        function (result) {
                            if (result == "ok") {
                                ToastMessage("گالری با موفقیت حذف گردید", "success", "کاربر گرامی");
                                LoadDatatable("#gallery_tb", false);
                            }
                            else {
                                if (isDevelopment) {
                                    alert(result.data);
                                } else {
                                    ToastMessage("خطایی رخ داده است", "warning", "خطا");
                                }
                            }
                        });
                }
            },
            deleteall: {
                text: 'حذف در تمام زبان ها',
                btnClass: 'btn-red',
                action: function () {

                    $.get("/Admin/Galleries?handler=DeleteGallery", { id: translationId, galleryId: galleryId, status: "all" },
                        function (result) {
                            if (result == "ok") {
                                ToastMessage("گالری در تمام زبان ها با موفقیت حذف گردید", "success", "کاربر گرامی");
                                LoadDatatable("#gallery_tb", false);
                            }
                            else {
                                if (isDevelopment) {
                                    alert(result.data);
                                } else {
                                    ToastMessage("خطایی رخ داده است", "warning", "خطا");
                                }
                            }
                        });
                }
            },
            cancel: {
                text: 'انصراف',

            }
        },
    });
}

function getGalleryGroupByLang_Global(langId, selector,optionSelected,status) {
    $.get("/Admin/Galleries?handler=GalleryGroupByLang", { id: langId }, function (result) {
        var options = "<option value=''>گروه گالری ...</option>";
        for (var i = 0; i < result.length; i++) {
            options += "<option value='" + result[i].galleryGroup_Id + "' data-id='" + result[i].galleryGroupTranslation_ID+"'>" + result[i].title + "</option>";
        }
        $(selector).html(options);
       
    });
}
function getGalleryGroupByLang_Global2(langId, selector, optionSelected, status) {
    $.get("/Admin/Galleries?handler=GalleryGroupByLang", { id: langId }, function (result) {
        
        var options = "<option value=''>گروه گالری ...</option>";
        for (var i = 0; i < result.length; i++) {
            options += "<option value='" + result[i].galleryGroupTranslation_ID + "' data-id='" + result[i].galleryGroupTranslation_ID + "'>" + result[i].title + "</option>";
        }
        $(selector).html(options);
        $(selector).val(optionSelected);

    });
}

function resetForm() {
    $("#language_id").val('0');
    $("#Title").val('');
    $("#Slug").val('');
}

function resetForm2() {
    $("#Title").val('');
    $("#Slug").val('');
}

///////
//// نمایش تصاویر گالری
function ViewAllGalleryPic(id) {
    $.get("/Admin/Galleries?handler=ViewAllGalleryPicture", { galleryId: id }, function (res) {
        $("#ViewAllGalleryPic").html(res);
    });
}
//// افزودن تصاویر گالری
function CreateGalleryPic(id) {
    $.get("/Admin/Galleries?handler=CreateGalleryPicture", { Id: id }, function (res) {
        ViewAllGalleryPic(id);
        $(".modal-dialog").css('max-width', '90%');
        $(".modal-header").css('background-color', 'rgb(76 35 35 / 59%)');
        $(".headerModalColor").text("تصاویر گالری");
        $("#myModal").modal();
        $("#myModalBody").html(res);
    });
}