﻿var isDevelopment = true;

function createFAQ() {
    $.get("/Admin/FAQs?handler=CreateFAQ", function (result) {
        if (result.status == "error") {
            if (isDevelopment) {
                alert(result.msg);
            } else {
                ToastMessage("خطایی رخ داده است", "info", "خطا");
            }
        }
        $(".modal-dialog").css('max-width', '75%');
        $(".modal-header").css('background-color', '#008000b8');
        $(".headerModalColor").text("افزودن سوال متداول");
        $("#myModal").modal();
        $("#myModalBody").html(result);
    });
}

function insertFAQ() {
    var frm = $("#fAQFrm").valid();
    if (frm == true) {
        $("#btnInsertFAQ").children(".fa-spinner").removeClass('hide');
        $("#btnInsertFAQ").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');
        var isactive = $('#IsActive:checked').length;
        var status = "";
        if (isactive === 1) { status = "true"; }
        else { status = "false"; }

        var fAQ = {
            Question: $("#Question").val(),
            Answer: $("#Answer").val(),
            Language_Id: $("#language_id").val(),
            IsActive: status,
        }
        $.ajax({
            method: 'POST',
            url: '/Admin/FAQs?handler=CreateFAQ',
            data: {
                fAQ: fAQ,
                __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
            },
            success: function (result) {
                $(".fa-spinner").addClass('hide');
                $(".fa-spinner").siblings('span').text('ثبت');

                if (result == "ok") {

                    LoadDatatable("#fAQ_tb", false);
                    $("#myModal").modal("hide");
                    ToastMessage("سوال متداول با موفقیت ثبت گردید", "success", "کاربر گرامی");
                }
               
                else {
                    if (isDevelopment) {
                        alert(result.data);
                    } else {
                        ToastMessage("خطایی رخ داده است", "info", "خطا");
                    }
                }
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}

function viewAllFAQs(stateId) {
    loadingPageShow();
    $("#fAQ_tb").DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.24/i18n/Persian.json"
        },
        "processing": true,
        "serverSide": true,
        "filter": true,
        "destroy": true,
        "ajax": {
            "url": "/Admin/FAQs?handler=ViewAllFAQs",
            "type": "POST",
            "datatype": "json",
            "data": {
                stateId: stateId,
                __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
            },
        },
        "columns": [
            {
                data: "question",
                render: function (data) {
                    if (data.length > 30) {
                        return '<span title="' + data + '">' + data.substring(0, 29) +'...</span >';
                    }
                    else {
                        return '<span>'+data+ '</span >';
                    }
                },
                name: "question",
            },
            {
                data: "isActive",
                render: function (data) {
                    if (data) {
                        return '<span class="text-success">فعال</span >';
                    }
                    else {
                        return '<span class="text-danger">غیر فعال</span >';
                    }
                },
            },
            {
                mRender: function (data, type, row) {
                    return '<div class="btn-group" role="group">' +
                        '<span class="btn btn-outline-info" onclick="editFAQ(' + row.faQ_ID + ')"><i class="fa fa-edit fa-sm"></i></span>' +
                        '<span class="btn btn-outline-danger" onclick="deleteFAQ(' + row.faQ_ID + ')"><i class="fa fa-trash fa-sm"></i></span></div>';
                }
            },
        ]

    });
    loadingPageHide();
}

function editFAQ(Id) {
    $.get("/Admin/FAQs?handler=EditFAQ", { id: Id }, function (result) {
        if (result.status == "error") {
            if (isDevelopment) {
                alert(result.msg);
            } else {
                ToastMessage("خطایی رخ داده است", "info", "خطا");
            }
        }
        $(".modal-dialog").css('max-width', '75%');
        $(".modal-header").css('background-color', '#008074b8');
        $(".headerModalColor").text("ویرایش سوال متداول");
        $("#myModal").modal();
        $("#myModalBody").html(result);
    });
}

function updateFAQ() {
    var frm = $("#fAQFrm").valid();
    if (frm == true) {
        $("#btnUpdateFAQ").children(".fa-spinner").removeClass('hide');
        $("#btnUpdateFAQ").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');
        var isactive = $('#IsActive:checked').length;
        var status = "";
        if (isactive === 1) { status = "true"; }
        else { status = "false"; }

        var fAQ = {
            FAQ_ID: $("#FAQ_ID").val(),
            Question: $("#Question").val(),
            Answer: $("#Answer").val(),
            Language_Id: $("#language_id").val(),
            IsActive: status,
        }
        $.ajax({
            method: 'POST',
            url: '/Admin/FAQs?handler=EditFAQ',
            data: {
                fAQ: fAQ,
                __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
            },
            success: function (result) {
                $(".fa-spinner").addClass('hide');
                $(".fa-spinner").siblings('span').text('ویرایش');

                if (result == "ok") {
                    LoadDatatable("#fAQ_tb", false);
                    $("#myModal").modal("hide");
                    ToastMessage("سوال متداول با موفقیت ویرایش گردید", "success", "کاربر گرامی");
                }
                else {
                    if (isDevelopment) {
                        alert(result.data);
                    } else {
                        ToastMessage("خطایی رخ داده است", "info", "خطا");
                    }
                }
            },
            error: function (result) {
            }
        });
    }
    else {
        return false;
    }
}

function deleteFAQ(fAQId) {
    $.confirm({
        title: 'آیا برای حذف این سوال مطمئن هستید؟', icon: 'fa fa-warning', theme: 'material', animation: 'scale', content: '',
        buttons: {
            confirm: {
                text: 'تایید',
                btnClass: 'btn-blue',
                action: function () {

                    $.get("/Admin/FAQs?handler=DeleteFAQ", { id: fAQId }, function (result) {
                        if (result == "ok") {
                            LoadDatatable("#fAQ_tb", false);
                            ToastMessage("سوال با موفقیت حذف گردید", "success", "کاربر گرامی");
                        }
                        else if (result.status == "error") {
                            if (isDevelopment) {
                                alert(result.data);
                            } else {
                                ToastMessage("خطایی رخ داده است", "info", "خطا");
                            }
                        }
                    });
                }
            },
            cancel: {
                text: 'انصراف',
            }
        },
    });

}

function setDirectionWithLanguage(language) {
    if (language == "fa-IR") {
        $("#Question").attr('dir', 'rtl');
        $("#Answer").attr('dir', 'rtl');
    }
    else if (language == "en-US") {
        $("#Question").attr('dir', 'ltr');
        $("#Answer").attr('dir', 'ltr');
    }
}
