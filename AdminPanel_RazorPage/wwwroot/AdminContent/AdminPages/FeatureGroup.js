﻿var isDevelopment = true;
function createFeatureGroup() {
    $.get("/Admin/Features?handler=CreateFeatureGroup", function (result) {
        if (result.status == "error") {
            if (isDevelopment) {
                alert(result.data);
            } else {
                ToastMessage("خطایی رخ داده است", "info", "خطا");
            }
        }
        $(".modal-dialog").css('max-width', '65%');
        $(".modal-header").css('background-color', '#008000b8');
        $(".headerModalColor").text("افزودن گروه ویژگی");
        $("#myModal").modal();
        $("#myModalBody").html(result);
    });
}

function insertFeatureGroup() {
    var frm = $("#featureGroupFrm").valid();
    if (frm == true) {
        if ($("#language_id").val() != "0") {

            $("#btnInsertFeatureGroup").children(".fa-spinner").removeClass('hide');
            $("#btnInsertFeatureGroup").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');

            var featureGroupTranslation = {
                FeatureGroup_Id: $("#FeatureGroup_Id").val(),
                Title: $("#Title").val(),
                Language_Id: $("#language_id").val(),
            }
            $.ajax({
                method: 'POST',
                url: '/Admin/Features?handler=CreateFeatureGroup',
                data: {
                    featureGroupTranslation: featureGroupTranslation,
                    __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
                },
                success: function (result) {
                    $(".fa-spinner").addClass('hide');
                    $(".fa-spinner").siblings('span').text('ثبت');

                    $(".insert-div").removeClass('hide');
                    $(".update-div").addClass('hide');

                    if (result.status == "ok") {
                        $("#Title").val('');
                        $("#FeatureGroup_Id").val(result.groupId);
                        ToastMessage("گروه ویژگی با موفقیت ثبت گردید", "success", "کاربر گرامی");
                        LoadDatatable("#featureGroup_tb", true);
                        getFeatureGroupByLang_Global("#featureGroup_ddl");
                    }
                    else if (result == "exsist") {
                        ToastMessage(" این گروه ویژگی  برای این زبان قبلا ثبت شده است", "info", "کاربر گرامی");
                    }
                    else {
                        if (isDevelopment) {
                            alert(result.data);
                        } else {
                            ToastMessage("خطایی رخ داده است", "warning", "خطا");
                        }
                    }
                },
                error: function (result) {
                }
            });
        }
        else {
            ToastMessage(" لطفا یک زبان را انتخاب کنید", "info", "کاربر گرامی");
        }
    }
    else {
        return false;
    }
}

function viewAllFeatureGroups() {
    loadingPageShow();
    $("#featureGroup_tb").DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.24/i18n/Persian.json"
        },
        "processing": true,
        "serverSide": true,
        "filter": true,
        "ajax": {
            "url": "/Admin/Features?handler=ViewAllFeatureGroups",
            "type": "POST",
            "datatype": "json",
            "data": { __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val() },
        },

        "columns": [
            { "data": "title", "name": "title", "autoWidth": true },
            {
                mRender: function (data, type, row) {
                    return '<div class="btn-group" role="group">' +
                        '<span class="btn btn-outline-info" onclick="editFeatureGroup(' + row.featureGroupTranslation_ID + ')"><i class="fa fa-edit fa-sm"></i></span>' +
                        '<span class="btn btn-outline-danger" onclick="deleteFeatureGroup(' + row.featureGroup_Id + ',' + row.featureGroupTranslation_ID + ')"><i class="fa fa-trash fa-sm"></i></span></div>';
                }
            },
        ]

    });
    loadingPageHide();
}

function editFeatureGroup(TranslationId) {
    $.get("/Admin/Features?handler=EditFeatureGroup", { id: TranslationId}, function (result) {
        if (result.status == "error") {
            if (isDevelopment) {
                alert(result.data);
            } else {
                ToastMessage("خطایی رخ داده است", "info", "خطا");
            }
        }
        $(".modal-dialog").css('max-width', '65%');
        $(".modal-header").css('background-color', '#008074b8');
        $(".headerModalColor").text("ویرایش گروه ویژگی");
        $("#myModal").modal();
        $("#myModalBody").html(result);
    });
}

function getFeatureGroupInfoWithLanguage(langId, featureGroupId) {

    $.get("/Admin/Features?handler=FeatureGroupInfo", { id: langId, featureGroupId: featureGroupId }, function (res) {
        if (res.status == "ok") {
            $(".update_div").removeClass('hide');
            $(".insert_div").addClass('hide');

            $("#Title").val(res.title);
            $("#Title").attr('data-id', res.id);
        }
        else {
            $("#Title").val('');
            $("#Title").removeAttr('data-id');
            $(".update_div").addClass('hide');
            $(".insert_div").removeClass('hide');
        }
    });
}

function updateFeatureGroup() {
    var frm = $("#featureGroupFrm").valid();
    if (frm == true) {
        if ($("#language_id").val() != "0") {

            $("#btnUpdateFeatureGroup").children(".fa-spinner").removeClass('hide');
            $("#btnUpdateFeatureGroup").children(".fa-spinner").siblings('span').text('منتظر بمانید ...');

            var featureGroupTranslation = {
                FeatureGroupTranslation_ID: $("#Title").attr('data-id'),
                FeatureGroup_Id: $("#FeatureGroup_Id").val(),
                Title: $("#Title").val(),
                Language_Id: $("#language_id").val(),
            }
            $.ajax({
                method: 'POST',
                url: '/Admin/Features?handler=EditFeatureGroup',
                data: {
                    featureGroupTranslation: featureGroupTranslation,
                    __RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val()
                },
                success: function (result) {
                    $(".fa-spinner").addClass('hide');
                    $(".fa-spinner").siblings('span').text('ویرایش');

                    $(".insert-div").removeClass('hide');
                    $(".update-div").addClass('hide');

                    if (result == "ok") {
                        $("#Title").val('');
                        $("#Title").removeAttr('data-id');
                        ToastMessage("گروه ویژگی با موفقیت ویرایش گردید", "success", "کاربر گرامی");
                        LoadDatatable("#featureGroup_tb", true);
                        getFeatureGroupByLang_Global("#featureGroup_ddl");
                    }
                    else {
                        if (isDevelopment) {
                            alert(result.data);
                        } else {
                            ToastMessage("خطایی رخ داده است", "warning", "خطا");
                        }
                    }
                },
                error: function (result) {
                }
            });
        }
        else {
            ToastMessage(" لطفا یک زبان را انتخاب کنید", "info", "کاربر گرامی");
        }
    }
    else {
        return false;
    }
}

function deleteFeatureGroup(featureGroupId, translationId) {
    $.confirm({
        title: 'آیا برای حذف ویژگی مطمئن هستید؟', icon: 'fa fa-warning', theme: "material", animation: 'scale', content: '',
        buttons: {
            confirm: {
                text: 'تایید',
                btnClass: 'btn-blue',
                action: function () {
                    $.get("/Admin/Features?handler=DeleteFeatureGroup", { id: translationId, featureGroupId: featureGroupId, status: "one" },
                        function (result) {
                            if (result == "ok") {
                                ToastMessage("ویژگی با موفقیت حذف گردید", "success", "کاربر گرامی");
                                LoadDatatable("#featureGroup_tb", false);
                            }
                            else if (result == "existInFeature") {
                                ToastMessage("برای گروه ویژگی , ویژگی تعریف شده است", "info", "کاربر گرامی");
                            }
                            else {
                                if (isDevelopment) {
                                    alert(result.data);
                                } else {
                                    ToastMessage("خطایی رخ داده است", "warning", "خطا");
                                }
                            }
                        });
                }
            },
            deleteall: {
                text: 'حذف در تمام زبان ها',
                btnClass: 'btn-red',
                action: function () {

                    $.get("/Admin/Features?handler=DeleteFeatureGroup", { id: translationId, featureGroupId: featureGroupId, status: "all" },
                        function (result) {
                            if (result == "ok") {
                                ToastMessage("ویژگی با موفقیت حذف گردید", "success", "کاربر گرامی");
                                LoadDatatable("#featureGroup_tb", false);
                            }
                            else if (result == "existInFeature") {
                                ToastMessage("برای گروه ویژگی , ویژگی تعریف شده است", "info", "کاربر گرامی");
                            }
                            else {
                                if (isDevelopment) {
                                    alert(result.data);
                                } else {
                                    ToastMessage("خطایی رخ داده است", "warning", "خطا");
                                }
                            }
                        });
                }
            },
            cancel: {
                text: 'انصراف',

            }
        },
    });
}

function getFeatureGroupByLang_Global(selector,selectedItem,status) {
    $.get("/Admin/Features?handler=FeatureGroup_Global", function (result) {
        var options ="<option value=''>گروه ویژگی ...</option>";
        for (var i = 0; i < result.length; i++) {
            options += "<option value='" + result[i].featureGroup_Id + "'>" + result[i].title + "</option>";
        }
        $(selector).html(options);
        debugger;
        if (status == "edit")
            $(selector).val(selectedItem);
    });
}

