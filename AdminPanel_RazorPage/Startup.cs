using Application.ViewModels;

using Data.Context;

using FluentValidation.AspNetCore;

using Ioc;
using System.Text.Json;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.AspNetCore.Rewrite;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http.Features;
using GleamTech.AspNet.Core;

namespace ItisaCMS_RazorPage
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //services.Configure<FormOptions>(x =>
            //{
            //    x.MultipartBodyLengthLimit = 209715200;
            //});

            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(
                    Configuration.GetConnectionString("AdminPanelRazor_Connection")));
            services.AddDatabaseDeveloperPageExceptionFilter();

            services.AddIdentity<IdentityUser, IdentityRole>(options =>
            {
                options.Password.RequiredLength = 6;
                options.Password.RequireLowercase = false;
                options.Password.RequireUppercase = false;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireDigit = false;
                options.SignIn.RequireConfirmedEmail = true;
            })
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            services.AddSession(option =>
           option.IdleTimeout = TimeSpan.FromMinutes(20));

            services.AddMvc()
                .AddNewtonsoftJson(// for use include object in json
                  options =>
                  {
                      options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                  })

                .AddFluentValidation();

            services.AddAuthentication(options =>
            {
                options.DefaultScheme = CookieAuthenticationDefaults.AuthenticationScheme;
            })
                .AddCookie(options =>
                {
                    options.ExpireTimeSpan = TimeSpan.FromMinutes(60);
                    options.Cookie.Name = "Cookie";

                });

            services.AddAntiforgery(o => o.HeaderName = "XSRF-TOKEN");
            //Required Https
            services.Configure<MvcOptions>(option =>
            {
                option.Filters.Add(new RequireHttpsAttribute());
            });

            services.AddHttpContextAccessor();
            services.AddAutoMapper(typeof(Startup));
            services.AddControllersWithViews();

            services.AddLocalization(options => options.ResourcesPath = "Resources");


            services.AddRazorPages()

            .AddRazorPagesOptions(options =>
            {
                options.Conventions.AddAreaPageRoute("Admin", "/Languages/Languages", "admin/languages");
                options.Conventions.AddAreaPageRoute("Admin", "/Colors/Colors", "admin/colors");
                options.Conventions.AddAreaPageRoute("Admin", "/Connections/Connections", "admin/connections");
                options.Conventions.AddAreaPageRoute("Admin", "/Contents/Contents", "admin/contents");
                options.Conventions.AddAreaPageRoute("Admin", "/Galleries/Galleries", "admin/galleries");
                options.Conventions.AddAreaPageRoute("Admin", "/SocialNetworks/SocialNetworks", "admin/socialNetworks");
                options.Conventions.AddAreaPageRoute("Admin", "/SlideShows/SlideShows", "admin/slideShows");
                options.Conventions.AddAreaPageRoute("Admin", "/SeoPages/SeoPages", "admin/seoPages");
                options.Conventions.AddAreaPageRoute("Admin", "/SendTypes/SendTypes", "admin/sendTypes");
                options.Conventions.AddAreaPageRoute("Admin", "/CoWorkers/CoWorkers", "admin/coWorkers");
                options.Conventions.AddAreaPageRoute("Admin", "/FilesManage/FilesManage", "admin/filesManage");
                options.Conventions.AddAreaPageRoute("Admin", "/Regions/Regions", "admin/regions");
                options.Conventions.AddAreaPageRoute("Admin", "/Discounts/Discounts", "admin/discounts");
                options.Conventions.AddAreaPageRoute("Admin", "/FAQs/FAQs", "admin/fAQs");
                options.Conventions.AddAreaPageRoute("Admin", "/Products/Products", "admin/products");
                options.Conventions.AddAreaPageRoute("Admin", "/Features/Features", "admin/features");
            })
            .AddViewLocalization(LanguageViewLocationExpanderFormat.Suffix,
                  options => { options.ResourcesPath = "Resources"; })
              .AddDataAnnotationsLocalization(options =>
              {
                  options.DataAnnotationLocalizerProvider = (type, factory) =>
                      factory.Create(typeof(ShareResource));
              });
            services.AddGleamTech();

            RegisterServices(services);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseMigrationsEndPoint();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            var rewriteOption = new RewriteOptions().AddRedirectToHttps();
            app.UseRewriter(rewriteOption);

            app.UseHttpsRedirection();

            app.UseGleamTech();

            app.UseStaticFiles();

            app.UseCookiePolicy();

            var supportedCultures = new List<CultureInfo>()
            {
                new CultureInfo("fa-IR"),
                new CultureInfo("en-US")
            };
            var options = new RequestLocalizationOptions()
            {
                DefaultRequestCulture = new RequestCulture("fa-IR"),
                SupportedCultures = supportedCultures,
                SupportedUICultures = supportedCultures,
                RequestCultureProviders = new List<IRequestCultureProvider>()
                {
                    new QueryStringRequestCultureProvider(),
                    new CookieRequestCultureProvider()
                }
            };
            app.UseRequestLocalization(options);


            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseSession();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapRazorPages();
                app.UseEndpoints(endpoints =>
                {
                    endpoints.MapRazorPages();
                    endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
                });
            });
        }

        public static void RegisterServices(IServiceCollection services)
        {
            DependencyContainer.RegisterServices(services);
        }

    }
}
