﻿using Application.Interfaces;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;

namespace ItisaCMS_RazorPage.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LanguageApi : ControllerBase
    {
        private ILanguageServices _languageServices;
        public LanguageApi(ILanguageServices languageServices)
        {
            _languageServices = languageServices;
        }
        [HttpPost]
        public async Task<IActionResult> GetLanguages()
        {
            try
            {
                var draw = Request.Form["draw"].FirstOrDefault();
                var start = Request.Form["start"].FirstOrDefault();
                var length = Request.Form["length"].FirstOrDefault();
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
                var searchValue = Request.Form["search[value]"].FirstOrDefault();
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                var languageData = await _languageServices.GetAll();
                if (!(string.IsNullOrEmpty(sortColumn)))
                {
                    languageData = languageData.AsQueryable().OrderBy(sortColumn + " " + sortColumnDirection);
                }
                if (!string.IsNullOrEmpty(searchValue))
                {
                    languageData = languageData.Where(m => m.Id.Contains(searchValue)
                                                || m.Name.Contains(searchValue));
                }
                recordsTotal = languageData.Count();
                var data = languageData.Skip(skip).Take(pageSize).ToList();
                var jsonData = new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data };
                return Ok(jsonData);
            }
            catch (Exception)
            {
                throw;
            }
        } 
    }
}
