﻿using Application.ViewModels;

using Domain.DomainClasses.Region;

using GleamTech.AspNet.UI;
using GleamTech.FileUltimate.AspNet.UI;

using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ItisaCMS_RazorPage.Controllers
{
    public class Utilities : Controller
    {
        public IWebHostEnvironment _hostingEnvironment;

        public Utilities(IWebHostEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }

        [HttpPost]
        [Route("file_uploads")]
        [Route("{culture}/file_uploads")]
        public IActionResult UploadImage(IFormFile upload, string CKEditorFuncNum, string CKEditor, string langCode)
        {
            if (upload.Length <= 0)
                return null;

            if (!Directory.Exists(Path.Combine(_hostingEnvironment.WebRootPath, "CkImages")))
            {
                Directory.CreateDirectory(Path.Combine(_hostingEnvironment.WebRootPath, "CkImages"));
            }
            var fileName = upload.FileName.Substring(0, upload.FileName.IndexOf(".")) + DateTime.Now.ToString("yyyyMMddHHmmss") + Path.GetExtension(upload.FileName).ToLower();

            var path = Path.Combine(
                _hostingEnvironment.WebRootPath, "CkImages",
                fileName);
            using (var stream = new FileStream(path, FileMode.Create))
            {
                upload.CopyTo(stream);
            }
            var url = $"{"/CkImages/"}{fileName}";

            return Json(new { uploaded = true, url });
        }

        //[Route("/admin/get-captcha-image")]
        //public IActionResult GetCaptchaImage()
        //{
        //    int width = 94;
        //    int height = 32;
        //    var captchaCode = CaptchaNum.GenerateCaptchaCode();
        //    var result = CaptchaNum.GenerateCaptchaImage(width, height, captchaCode);
        //    HttpContext.Session.SetString("CaptchaCode", result.CaptchaCode);
        //    Stream s = new MemoryStream(result.CaptchaByteData);
        //    var mysession = HttpContext.Session.GetString("CaptchaCode");

        //    return new FileStreamResult(s, "image/png");
        //}


        public IActionResult UsingPartial()
        {
            var fileManager = GetFileManagerModel();

            return View(fileManager);
        }

        public IActionResult FileManagerPartialView()
        {
            var fileManager = GetFileManagerModel();

            return PartialView(fileManager);
        }

        private FileManager GetFileManagerModel()
        {
            
            var fileManager1 = new FileManager
            {
                Id = "fileManager1",
                Width = 800,
                Height = 400,
                Resizable = true,
                DisplayLanguage = "en",
                Hidden = true,
                CollapseRibbon = true,
                DisplayMode = DisplayMode.Window,
                WindowOptions =
                {
                    Title = "Choose a file",
                    Modal = true
                },
                ClientEvents = new FileManagerClientEvents
                {
                    Chosen = "fileManagerChosen"
                },
                Chooser = true,
                ChooserMultipleSelection = true
            };
            fileManager1.RootFolders.Add(new FileManagerRootFolder
            {
                Name = "Files",
                Location = "~/Files"
            });
            fileManager1.RootFolders[0].AccessControls.Add(new FileManagerAccessControl
            {
                Path = @"\",
                AllowedPermissions = FileManagerPermissions.Full
            });

            return fileManager1;

        }
    }
}
