﻿using Application.Interfaces;

using Domain.DomainClasses.Color;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;

namespace ItisaCMS_RazorPage.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ColorsApi : ControllerBase
    {
        private IColorTranslationServices _colorTranslationServices;
        public ColorsApi(IColorTranslationServices colorTranslationServices)
        {
            _colorTranslationServices = colorTranslationServices;
        }
        [HttpPost]
        public async Task<IActionResult> GetColors()
        {
            try
            {
                var draw = Request.Form["draw"].FirstOrDefault();
                var start = Request.Form["start"].FirstOrDefault();
                var length = Request.Form["length"].FirstOrDefault();
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
                var searchValue = Request.Form["search[value]"].FirstOrDefault();
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                var colors = await _colorTranslationServices.GetWithIncludeAsync<ColorTranslation>
                (
                selector: x => x,
                include: s => s.Include(x => x.Color),
                where: x => x.LanguageId == CultureInfo.CurrentCulture.Name
                );
                if (!(string.IsNullOrEmpty(sortColumn)))
                {
                    colors = colors.AsQueryable().OrderBy(sortColumn + " " + sortColumnDirection);
                }
                if (!string.IsNullOrEmpty(searchValue))
                {
                    colors = colors.Where(m => m.Title.Contains(searchValue));
                                                
                }
                recordsTotal = colors.Count();
                var data = colors.Skip(skip).Take(pageSize).ToList();
                var jsonData = new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data };
                return Ok(jsonData);
            }
            catch (Exception err)
            {
                throw;
            }
        }
    }
}
