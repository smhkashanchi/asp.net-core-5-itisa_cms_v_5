﻿using Application.ViewModels;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Razor.Runtime.TagHelpers;
using Microsoft.AspNetCore.Razor.TagHelpers;



using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ItisaCMS_RazorPage.TagHelpers
{
    // You may need to install the Microsoft.AspNetCore.Razor.Runtime package into your project
    [HtmlTargetElement("ul",Attributes ="page-model")]
    public class PagerTagHelper : TagHelper
    {
        [ViewContext]
        [HtmlAttributeNotBound]
        public ViewContext viewContext { get; set; }
        public PagingInfo pageModel { get; set; }
        public string PageClass { get; set; }
        public string PageClassSelected { get; set; }
        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            TagBuilder result = new TagBuilder("ul");
            for (int i = 1; i <= pageModel.TotalPage; i++)
            {

                TagBuilder aTag = new TagBuilder("a");
                string url = pageModel.UrlParam.Replace(":", i.ToString());
                aTag.Attributes["href"] = url;
                aTag.AddCssClass(PageClass);
                aTag.InnerHtml.AppendHtml(i.ToString());

                TagBuilder liTag = new TagBuilder("li");
                liTag.AddCssClass("page-item");
                liTag.AddCssClass(i == pageModel.CurrentPage ? "active" : "");

                liTag.InnerHtml.AppendHtml(aTag);
                result.InnerHtml.AppendHtml(liTag);
            }
            output.Content.AppendHtml(result.InnerHtml);
        }
    }
}
