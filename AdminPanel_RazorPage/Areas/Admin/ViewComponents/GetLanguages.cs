﻿using Application.Interfaces;

using Domain.Interfaces;

using Microsoft.AspNetCore.Mvc;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ItisaCMS_RazorPage.Areas.Admin.ViewComponents
{
    public class GetLanguages : ViewComponent
    {
        public ILanguageServices _langRepository;
        public GetLanguages(ILanguageServices langRepository)
        {
            _langRepository = langRepository;
        }
        public async Task<IViewComponentResult> InvokeAsync()
        {
            return await Task.FromResult((IViewComponentResult)View("GetLanguages", await _langRepository.GetAll(x=>x.IsActive)));
        }
    }
}
