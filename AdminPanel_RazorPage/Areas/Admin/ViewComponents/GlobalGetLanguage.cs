﻿using Domain.Interfaces;

using Microsoft.AspNetCore.Mvc;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ItisaCMS_RazorPage.Areas.Admin.ViewComponents
{
    public class GlobalGetLanguage:ViewComponent
    {
        public ILanguageRepository _langRepository;
        public GlobalGetLanguage(ILanguageRepository langRepository)
        {
            _langRepository = langRepository;
        }
        public async Task<IViewComponentResult> InvokeAsync()
        {
            return await Task.FromResult((IViewComponentResult)View("GlobalGetLanguage", await _langRepository.GetAll(x => x.IsActive)));
        }
    }
}
