using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;
using Application.Interfaces;

using Domain.DomainClasses.Feature;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace ItisaCMS_RazorPage.Areas.Admin.Pages.Features
{
    public class FeaturesModel : PageModel
    {
        private IFeatureGroupServices _featureGroupServices;
        private IFeatureGroupTranslationServices _featureGroupTranslationServices;
        private IFeatureServices _featureServices;
        private IFeatureTranslationServices _featureTranslationServices;
        private IFeatureReplyServices _featureReplyServices;
        private IFeatureReplyTranslationServices _featureReplyTranslationServices;
        public FeaturesModel(IFeatureGroupServices featureGroupServices, IFeatureGroupTranslationServices featureGroupTranslationServices
            , IFeatureServices featureServices, IFeatureTranslationServices featureTranslationServices,
            IFeatureReplyServices featureReplyServices, IFeatureReplyTranslationServices featureReplyTranslationServices)
        {
            _featureGroupServices = featureGroupServices;
            _featureGroupTranslationServices = featureGroupTranslationServices;
            _featureServices = featureServices;
            _featureTranslationServices = featureTranslationServices;
            _featureReplyServices = featureReplyServices;
            _featureReplyTranslationServices = featureReplyTranslationServices;
        }

        #region FeatureGroup
        public IActionResult OnGetCreateFeatureGroup()
        {
            try
            {
                return Partial("_CreateFeatureGroup");
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
        }
        public async Task<IActionResult> OnPostCreateFeatureGroup(FeatureGroupTranslation featureGroupTranslation)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    FeatureGroup featureGroup = new FeatureGroup();
                    if (featureGroupTranslation.FeatureGroup_Id == 0)
                    {

                        await _featureGroupServices.Insert(featureGroup);
                        await _featureGroupServices.Save();
                    }
                    if (!await _featureGroupTranslationServices
                        .ExistsAsync(x => x.Language_Id == featureGroupTranslation.Language_Id && x.FeatureGroup_Id == featureGroupTranslation.FeatureGroup_Id))
                    {
                        if (featureGroupTranslation.FeatureGroup_Id == 0)
                            featureGroupTranslation.FeatureGroup_Id = featureGroup.FeatureGroup_ID;

                        await _featureGroupTranslationServices.Insert(featureGroupTranslation);
                        await _featureGroupTranslationServices.Save();
                        return new JsonResult(new { status = "ok", groupId = featureGroupTranslation.FeatureGroup_Id });
                    }
                    return new JsonResult("exsist");

                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
        }

        #region ViewAllFeatureGroups
        public async Task<IActionResult> OnPostViewAllFeatureGroups()
        {
            try
            {
                var draw = Request.Form["draw"].FirstOrDefault();
                var start = Request.Form["start"].FirstOrDefault();
                var length = Request.Form["length"].FirstOrDefault();
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
                var searchValue = Request.Form["search[value]"].FirstOrDefault();
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                var featureGroups = await _featureGroupTranslationServices.GetAllAsyn(
                where: x => x.Language_Id == CultureInfo.CurrentCulture.Name
                );
                if (!(string.IsNullOrEmpty(sortColumn)))
                {
                    featureGroups = featureGroups.AsQueryable().OrderBy(sortColumn + " " + sortColumnDirection);
                }
                if (!string.IsNullOrEmpty(searchValue))
                {
                    featureGroups = featureGroups.Where(m => m.Title.Contains(searchValue));

                }
                recordsTotal = featureGroups.Count();
                var data = featureGroups.Skip(skip).Take(pageSize).ToList();
                var jsonData = new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data };
                return new JsonResult(jsonData);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        public async Task<IActionResult> OnGetEditFeatureGroup(int id)
        {
            try
            {
                var featureGroupTrans = await _featureGroupTranslationServices.GetWithIncludeAsync
                    (
                    selector: x => x,
                    where: x => x.FeatureGroupTranslation_ID == id,
                    include: s => s.Include(x => x.FeatureGroup));
                return Partial("_EditFeatureGroup", featureGroupTrans.FirstOrDefault());
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
        }
        public async Task<IActionResult> OnGetFeatureGroupInfo(string id, int featureGroupId)
        {

            string languageId = string.IsNullOrEmpty(id) ? CultureInfo.CurrentCulture.Name : id.Trim();
            var featureGroupIdTrans = await _featureGroupTranslationServices
                .GetAsync(x => x.Language_Id == languageId && x.FeatureGroup_Id == featureGroupId);
            if (featureGroupIdTrans != null)
            {
                return new JsonResult(new
                {
                    status = "ok",
                    title = featureGroupIdTrans.Title,
                    id = featureGroupIdTrans.FeatureGroupTranslation_ID
                });
            }

            return new JsonResult("nok");
        }
        public async Task<IActionResult> OnPostEditFeatureGroup(FeatureGroupTranslation featureGroupTranslation)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (!await _featureGroupTranslationServices
                        .ExistsAsync(x => x.Language_Id == featureGroupTranslation.Language_Id && x.FeatureGroup_Id == featureGroupTranslation.FeatureGroup_Id))
                    {
                        await _featureGroupTranslationServices.Insert(featureGroupTranslation);
                        await _featureGroupTranslationServices.Save();
                    }
                    else
                    {
                        _featureGroupTranslationServices.Update(featureGroupTranslation);
                        await _featureGroupTranslationServices.Save();
                    }
                    return new JsonResult("ok");

                }
                foreach (var modelState in ViewData.ModelState.Values)
                {
                    foreach (var error in modelState.Errors)
                    {

                    }
                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
        }

        public async Task<IActionResult> OnGetDeleteFeatureGroup(int id, int featureGroupId, string status)
        {
            try
            {
                if (!await _featureServices.ExistsAsync(x => x.FeatureGroup_Id == featureGroupId))
                {
                    var featureGroup = await _featureGroupServices.GetAsync(x => x.FeatureGroup_ID == featureGroupId);
                    var featureGroupTransList = await _featureGroupTranslationServices.GetAllAsyn(x => x.FeatureGroup_Id == featureGroupId);
                    var featureGroupTrans = await _featureGroupTranslationServices.GetAsync(x => x.FeatureGroupTranslation_ID == id);
                    if (status == "one")
                    {
                        if (featureGroupTransList.Count() > 1)
                        {
                            _featureGroupTranslationServices.Delete(featureGroupTrans);
                            await _featureGroupTranslationServices.Save();
                            return new JsonResult("ok");
                        }
                        else
                        {
                            _featureGroupTranslationServices.Delete(featureGroupTrans);
                            await _featureGroupTranslationServices.Save();
                            _featureGroupServices.Delete(featureGroup);
                            await _featureGroupServices.Save();
                            return new JsonResult("ok");
                        }
                    }
                    else if (status == "all")
                    {
                        foreach (var featureGroupTranslation in featureGroupTransList)
                        {
                            _featureGroupTranslationServices.Delete(featureGroupTranslation);
                            await _featureGroupTranslationServices.Save();
                        }

                        _featureGroupServices.Delete(featureGroup);
                        await _featureGroupServices.Save();
                        return new JsonResult("ok");
                    }
                }
                return new JsonResult("existInFeature");
            }
            catch (Exception err)
            {
                return new JsonResult(new
                {
                    status = "error",
                    data = err.Message + " *** " + err.InnerException
                });
            }

        }

        public async Task<IActionResult> OnGetFeatureGroup_Global()
        {
            var featureGroups = await _featureGroupTranslationServices.GetAllAsyn
                (x => x.Language_Id == CultureInfo.CurrentCulture.Name);
            return new JsonResult(featureGroups);
        }
        #endregion

        #region Feature
        public IActionResult OnGetCreateFeature()
        {
            return Partial("_CreateFeature");
        }

        public async Task<IActionResult> OnPostCreateFeature(Feature feature)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    await _featureServices.Insert(feature);
                    await _featureServices.Save();
                    return new JsonResult(new { status = "ok", featureId = feature.Feature_ID });
                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
        }

        public IActionResult OnGetCreateFeatureTranslation(int id)
        {
            TempData["FeatureId"] = id;
            return Partial("_CreateFeatureTranslation");
        }

        public async Task<IActionResult> OnPostCreateFeatureTranslation(FeatureTranslation featureTranslation)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (!await _featureTranslationServices
                     .ExistsAsync(x => x.Language_Id == featureTranslation.Language_Id && x.Feature_Id == featureTranslation.Feature_Id))
                    {
                        await _featureTranslationServices.Insert(featureTranslation);
                        await _featureTranslationServices.Save();
                        return new JsonResult("ok");
                    }
                    return new JsonResult("exsist");
                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
        }

        #region ViewAllFeatures
        public async Task<IActionResult> OnPostViewAllFeatures(int groupId)
        {
            try
            {
               
                var draw = Request.Form["draw"].FirstOrDefault();
                var start = Request.Form["start"].FirstOrDefault();
                var length = Request.Form["length"].FirstOrDefault();
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
                var searchValue = Request.Form["search[value]"].FirstOrDefault();
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                IEnumerable<FeatureTranslation> features = null;
                if (groupId != 0)
                {
                    features = await _featureTranslationServices.GetWithIncludeAsync<FeatureTranslation>
                    (
                    selector: x => x,
                    include: s => s.Include(x => x.Feature),
                    where: x => x.Language_Id == CultureInfo.CurrentCulture.Name && x.Feature.FeatureGroup_Id == groupId
                    );
                }
                else
                {
                    features = await _featureTranslationServices.GetWithIncludeAsync<FeatureTranslation>
                    (
                    selector: x => x,
                    include: s => s.Include(x => x.Feature),
                    where: x => x.Language_Id == CultureInfo.CurrentCulture.Name
                    );
                }
                if (!(string.IsNullOrEmpty(sortColumn)))
                {
                    features = features.AsQueryable().OrderBy(sortColumn + " " + sortColumnDirection);
                }
                if (!string.IsNullOrEmpty(searchValue))
                {
                    features = features.Where(m => m.Title.Contains(searchValue));

                }
                recordsTotal = features.Count();
                var data = features.Skip(skip).Take(pageSize).ToList();
                var jsonData = new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data };
                return new JsonResult(jsonData);
            }
            catch (Exception err)
            {
                throw;
            }
        }
        #endregion

        public async Task<IActionResult> OnGetEditFeature(int id)
        {
            try
            {
                var feature = await _featureServices.GetAsync(x => x.Feature_ID == (Int16)id);

                return Partial("_EditFeature", feature);
            }
            catch (Exception err)
            {
                return new JsonResult(new
                {
                    status = "error",
                    data = err.Message + " *** " + err.InnerException
                });
            }
        }
        public async Task<IActionResult> OnGetEditFeatureTranslation(int id)
        {
            var featureTrans = await _featureTranslationServices.GetAsync(x => x.FeatureTranslation_ID == id);

            return Partial("_EditFeatureTranslation", featureTrans);
        }
        public async Task<IActionResult> OnGetFeatureInfo(string id, int featureId)
        {

            string languageId = string.IsNullOrEmpty(id) ? CultureInfo.CurrentCulture.Name : id.Trim();
            var featureIdTrans = await _featureTranslationServices
                .GetAsync(x => x.Language_Id == languageId && x.Feature_Id == featureId);
            if (featureIdTrans != null)
            {
                return new JsonResult(new
                {
                    status = "ok",
                    title = featureIdTrans.Title,
                    id = featureIdTrans.FeatureTranslation_ID
                });
            }

            return new JsonResult("nok");
        }
        public async Task<IActionResult> OnPostEditFeature(Feature feature)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _featureServices.Update(feature);
                    await _featureServices.Save();
                    return new JsonResult("ok");
                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new
                {
                    status = "error",
                    data = err.Message + " *** " + err.InnerException
                });
            }
        }
        public async Task<IActionResult> OnPostEditFeatureTranslation(FeatureTranslation featureTranslation)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _featureTranslationServices.Update(featureTranslation);
                    await _featureTranslationServices.Save();
                    return new JsonResult("ok");
                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new
                {
                    status = "error",
                    data = err.Message + " *** " + err.InnerException
                });
            }
        }

        public async Task<IActionResult> OnGetDeleteFeatureTranslation(int id, int featureId, string status)
        {
            try
            {
                var featureTransList = await _featureTranslationServices.GetWithIncludeAsync
                        (selector: x => x,
                        where: x => x.Feature_Id == featureId);

                var featureTrans = await _featureTranslationServices.GetAsync(x => x.FeatureTranslation_ID == id);
                if (status == "one")
                {
                    if (!await _featureReplyServices.ExistsAsync(x => x.Feature_Id == featureId))
                    {
                        if (featureTransList.Count() > 1)
                        {

                            _featureTranslationServices.Delete(featureTrans);
                            await _featureTranslationServices.Save();
                        }
                        else
                        {
                            _featureTranslationServices.Delete(featureTrans);
                            await _featureTranslationServices.Save();

                            var feature = await _featureServices.GetAsync(x => x.Feature_ID == featureId);
                            _featureServices.Delete(feature);
                            await _featureServices.Save();
                        }
                        return new JsonResult("ok");
                    }
                    return new JsonResult("existInReply");
                }
                else if (status == "all")
                {
                    if (!await _featureReplyServices.ExistsAsync(x => x.Feature_Id == featureId))
                    {
                        _featureTranslationServices.DeleteAll(featureTransList);
                        await _featureTranslationServices.Save();

                        var feature = await _featureServices.GetAsync(x => x.Feature_ID == featureId);
                        _featureServices.Delete(feature);
                        await _featureServices.Save();
                        return new JsonResult("ok");
                    }
                    return new JsonResult("existInReply");
                }
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
            return new JsonResult("nok");
        }

        public async Task<IActionResult> OnGetFeatures_Global(int featureGroupId)
        {
            IEnumerable<FeatureTranslation> features = null;
            if(featureGroupId!=0)
            features = await _featureTranslationServices.GetWithIncludeAsync
                (
                selector:x=>x,
               where: x => x.Language_Id == CultureInfo.CurrentCulture.Name && x.Feature.FeatureGroup_Id==featureGroupId,
               include:s=>s.Include(x=>x.Feature));
            else
                features = await _featureTranslationServices.GetAllAsyn
               (x => x.Language_Id == CultureInfo.CurrentCulture.Name);

            return new JsonResult(features);
        }
        #endregion

        #region FeatureReply
        public IActionResult OnGetCreateFeatureReply()
        {
            try
            {
                return Partial("_CreateFeatureReply");
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
        }
        public async Task<IActionResult> OnPostCreateFeatureReply(FeatureReply featureReply)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    await _featureReplyServices.Insert(featureReply);
                    await _featureReplyServices.Save();
                    return new JsonResult(new { status = "ok", featureReplyId = featureReply.FeatureReply_ID });
                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
        }
        public IActionResult OnGetCreateFeatureReplyTranslation(int id)
        {
            TempData["FeatureReplyId"] = id;
            return Partial("_CreateFeatureReplyTrans");
        }
        public async Task<IActionResult> OnPostCreateFeatureReplyTranslation(FeatureReplyTranslation featureReplyTranslation)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (!await _featureReplyTranslationServices
                        .ExistsAsync(x => x.Language_Id == featureReplyTranslation.Language_Id && x.FeatureReply_Id == featureReplyTranslation.FeatureReply_Id))
                    {
                        await _featureReplyTranslationServices.Insert(featureReplyTranslation);
                        await _featureReplyTranslationServices.Save();
                        return new JsonResult("ok");
                    }
                    return new JsonResult("exsist");

                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
        }

        #region ViewAllFeatureReplies
        public async Task<IActionResult> OnPostViewAllFeatureReplies(int featureId)
        {
            try
            {
                var draw = Request.Form["draw"].FirstOrDefault();
                var start = Request.Form["start"].FirstOrDefault();
                var length = Request.Form["length"].FirstOrDefault();
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
                var searchValue = Request.Form["search[value]"].FirstOrDefault();
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                IEnumerable<FeatureReplyTranslation> featureReplies = null;
                if (featureId != 0)
                {
                    featureReplies = await _featureReplyTranslationServices.GetWithIncludeAsync<FeatureReplyTranslation>
                    (
                    selector: x => x,
                    include: s => s.Include(x => x.FeatureReply),
                    where: x => x.Language_Id == CultureInfo.CurrentCulture.Name && x.FeatureReply.Feature_Id == featureId
                    );
                }
                else
                    featureReplies = await _featureReplyTranslationServices.GetWithIncludeAsync<FeatureReplyTranslation>
                       (
                       selector: x => x,
                       include: s => s.Include(x => x.FeatureReply),
                       where: x => x.Language_Id == CultureInfo.CurrentCulture.Name
                       );

                if (!(string.IsNullOrEmpty(sortColumn)))
                {
                    featureReplies = featureReplies.AsQueryable().OrderBy(sortColumn + " " + sortColumnDirection);
                }
                if (!string.IsNullOrEmpty(searchValue))
                {
                    featureReplies = featureReplies.Where(m => m.Title.Contains(searchValue));

                }
                recordsTotal = featureReplies.Count();
                var data = featureReplies.Skip(skip).Take(pageSize).ToList();
                var jsonData = new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data };
                return new JsonResult(jsonData);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        public async Task<IActionResult> OnGetEditFeatureReply(int id)
        {
            try
            {
                var featureReply = await _featureReplyServices.GetAsync(x => x.FeatureReply_ID == (Int16)id);

                return Partial("_EditFeatureReply", featureReply);
            }
            catch (Exception err)
            {
                return new JsonResult(new
                {
                    status = "error",
                    data = err.Message + " *** " + err.InnerException
                });
            }
        }
        public async Task<IActionResult> OnGetEditFeatureReplyTranslation(int id)
        {
            var featureReplyTrans = await _featureReplyTranslationServices.GetAsync(x => x.FeatureReplyTranslation_ID == id);

            return Partial("_EditFeatureReplyTrans", featureReplyTrans);
        }
        public async Task<IActionResult> OnGetFeatureReplyInfo(string id, int featureReplyId)
        {

            string languageId = string.IsNullOrEmpty(id) ? CultureInfo.CurrentCulture.Name : id.Trim();
            var featureReplyIdTrans = await _featureReplyTranslationServices
                .GetAsync(x => x.Language_Id == languageId && x.FeatureReply_Id == featureReplyId);
            if (featureReplyIdTrans != null)
            {
                return new JsonResult(new
                {
                    status = "ok",
                    title = featureReplyIdTrans.Title,
                    id = featureReplyIdTrans.FeatureReplyTranslation_ID
                });
            }

            return new JsonResult("nok");
        }
        public async Task<IActionResult> OnPostEditFeatureReply(FeatureReply featureReply)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _featureReplyServices.Update(featureReply);
                    await _featureReplyServices.Save();
                    return new JsonResult("ok");
                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new
                {
                    status = "error",
                    data = err.Message + " *** " + err.InnerException
                });
            }
        }
        public async Task<IActionResult> OnPostEditFeatureReplyTranslation(FeatureReplyTranslation featureReplyTranslation)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _featureReplyTranslationServices.Update(featureReplyTranslation);
                    await _featureReplyTranslationServices.Save();
                    return new JsonResult("ok");
                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new
                {
                    status = "error",
                    data = err.Message + " *** " + err.InnerException
                });
            }
        }

        public async Task<IActionResult> OnGetDeleteFeatureReply(int id, int featureReplyId, string status)
        {
            try
            {
                var featureReplyIdTransList = await _featureReplyTranslationServices.GetWithIncludeAsync
                        (selector: x => x,
                        where: x => x.FeatureReply_Id == featureReplyId);

                var featureReplyTrans = await _featureReplyTranslationServices.GetAsync(x => x.FeatureReplyTranslation_ID == id);

                if (status == "one")
                {

                    if (featureReplyIdTransList.Count() > 1)
                    {
                        _featureReplyTranslationServices.Delete(featureReplyTrans);
                        await _featureReplyTranslationServices.Save();
                    }
                    else
                    {
                        _featureReplyTranslationServices.Delete(featureReplyTrans);
                        await _featureReplyTranslationServices.Save();

                        var featureReply = await _featureReplyServices.GetAsync(x => x.FeatureReply_ID == featureReplyId);
                        _featureReplyServices.Delete(featureReply);
                        await _featureReplyServices.Save();
                    }
                    return new JsonResult("ok");
                }
                else if (status == "all")
                {
                    _featureReplyTranslationServices.DeleteAll(featureReplyIdTransList);
                    await _featureReplyTranslationServices.Save();

                    var featureReply = await _featureReplyServices.GetAsync(x => x.FeatureReply_ID == featureReplyId);
                    _featureReplyServices.Delete(featureReply);
                    await _featureReplyServices.Save();
                    return new JsonResult("ok");
                }
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
            return new JsonResult("nok");
        } 
        #endregion
    }
}
