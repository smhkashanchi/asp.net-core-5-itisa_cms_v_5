using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Domain.DomainClasses;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Application.Interfaces;
using Application.ViewModels;
using System.Globalization;
using Application.Utilities;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Http;

namespace ItisaCMS_RazorPage.Areas.Admin.Pages.Languages
{
    public class LanguagesModel : PageModel
    {
        public static string LanguageId = "";
        private ILanguageServices _languageServices;
        public LanguagesModel(ILanguageServices languageServices)
        {
            _languageServices = languageServices;
        }
        [BindProperty]
        public Language Language { get; set; }
        public IActionResult OnGetCreate()
        {
            return Partial("_Create");
        }
        public async Task<IActionResult> OnPostCreate()
        {
            try
            {

                if (ModelState.IsValid)
                {
                    if (!await _languageServices.IsExist(Language.Id.Trim()))
                    {
                        await _languageServices.Insert(Language);
                        await _languageServices.Save();
                        return new JsonResult(new { status = "ok", data = Language });
                    }
                    return new JsonResult(new { status = "isExsist", data = Language });
                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
        }
        public async Task<IActionResult> OnGetViewAll()
        {
            var langs = await _languageServices.GetAll(x=>x.IsActive);
            return new JsonResult(langs);
        }
        public async Task<IActionResult> OnGetEdit(string id)
        {
            try
            {
                Language = await _languageServices.GetOne(id);
                if (Language != null)
                {
                    LanguageId = Language.Id;
                    return Partial("_Edit", Language);
                }
                return NotFound();
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
        }
        public async Task<IActionResult> OnPostEdit(Language language)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (language.Id == LanguageId)
                    {
                        _languageServices.Update(language);
                        await _languageServices.Save();
                        return new JsonResult("ok");
                    }
                    if (!await _languageServices.IsExist(language.Id.Trim()))
                    {
                        _languageServices.Update(language);
                        await _languageServices.Save();
                        return new JsonResult("ok");
                    }
                    return new JsonResult("isExsist");

                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
        }
        public async Task<IActionResult> OnGetDelete(string id)
        {

            try
            {
                var language = await _languageServices.GetOne(id);
                if (language != null)
                {
                    _languageServices.Delete(language);
                    await _languageServices.Save();
                    return new JsonResult("ok");
                }
                return new JsonResult("nok");
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
        }
        public IActionResult OnGetChangeLanguage(string culture)
        {
            Response.Cookies.Append(CookieRequestCultureProvider.DefaultCookieName,
                CookieRequestCultureProvider.MakeCookieValue(new RequestCulture(culture)),
                new CookieOptions() { Expires = DateTimeOffset.UtcNow.AddDays(5) });

            //return Redirect(Request.Headers["Referer"].ToString());
            return new JsonResult(CultureInfo.CurrentCulture.Name);
        }
        //public IActionResult OnPostConvertToPdf(string HtmlText)
        //{
        //    if (!string.IsNullOrEmpty(HtmlText))
        //    {
        //        PdfGenerator pdfGenerator = new PdfGenerator(_converter);
        //        pdfGenerator.CreatePDF(HtmlText);
        //    }
        //    return new JsonResult("nok");
        //}

    }
}
