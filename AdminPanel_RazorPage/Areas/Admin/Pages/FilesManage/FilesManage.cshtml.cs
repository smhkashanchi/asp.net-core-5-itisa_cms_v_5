﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

using Application.Interfaces;
using Application.Services;
using Application.Utilities;
using System.Linq.Dynamic.Core;
using Domain.DomainClasses.FilesManage;

using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

using Newtonsoft.Json;
using Utilities;

namespace ItisaCMS_RazorPage.Areas.Admin.Pages.FilesManage
{
    public class FilesManageModel : PageModel
    {
        public static string FilePath = "Files/FilesManage";
        public static Int16 ParentId = 0;
        public static Int16 ParentIdEdit = 0;
        private SiteMap sm;
        private IWebHostEnvironment _hostEnvironment;
        public ISiteMapService _siteMapSerivce;
        private IFileManageGroupServices _fileManageGroupServices;
        private IFileManageGroupTranslationServices _fileManageGroupTranslationSrevices;
        private IFileManageServices _fileManageServices;
        private IFileManageTranslationServices _fileManageTranslationServices;
        public FilesManageModel(ISiteMapService siteMapService, IFileManageGroupServices fileManageGroupServices
            , IFileManageGroupTranslationServices fileManageGroupTranslationSrevices, IFileManageServices fileManageServices
            , IFileManageTranslationServices fileManageTranslationServices, IGalleryTranslationServices galleryTranslationServices
            , IWebHostEnvironment environment)
        {
            _siteMapSerivce = siteMapService;
            sm = new SiteMap();
            _hostEnvironment = environment;
            _fileManageGroupServices = fileManageGroupServices;
            _fileManageGroupTranslationSrevices = fileManageGroupTranslationSrevices;
            _fileManageServices = fileManageServices;
            _fileManageTranslationServices = fileManageTranslationServices;
        }

        #region Treeview
        public async Task<IActionResult> OnGetCreateTreeView(int id, string lang)
        {
            id = 1;
            return Content(JsonConvert.SerializeObject(await ViewInTreeView(id, lang)));
        }

        public async Task<List<Node>> ViewInTreeView(int CGId, string lang)//گرفتن گروه مطالب بصورت جیسون و فرستادن به تابع ساخت تری ویو
        {
            List<Node> TNodes = new List<Node>();
            List<Node> TNodes2 = new List<Node>();

            string LangId = "";
            if (lang != null)
                LangId = lang;
            else
                LangId = CultureInfo.CurrentCulture.Name;

            var fileManageGroupData = await _fileManageGroupTranslationSrevices.GetWithIncludeAsync
                <FileManageGroupTranslation>(
                selector: x => x,
                where: x => x.FileManageGroup.FileManageGroup_ID == CGId && x.Language_Id == LangId,
                include: x => x.Include(s => s.FileManageGroup)
                );

            var groups = await _fileManageGroupTranslationSrevices.GetWithIncludeAsync
                (
                selector: x => x,
                where: x => x.FileManageGroup.Parent_Id == CGId && x.Language_Id == LangId,
                include: x => x.Include(s => s.FileManageGroup)
                );
            foreach (var item in groups)
            {
                var node = await ViewInTreeView(item.FileManageGroup_Id, item.Language_Id);
                TNodes2.Add(node.FirstOrDefault());
            }


            TNodes.Add(new Node()
            {
                text = " " + fileManageGroupData.FirstOrDefault().Title,
                href = "#" + fileManageGroupData.FirstOrDefault().FileManageGroupTranslation_ID,
                tags = CGId.ToString(),
                nodes = TNodes2
            });

            return TNodes;
        }
        #endregion

        #region FileManageGroup
        public IActionResult OnGetCreateGroup(FileManageGroup fileManageGroup)
        {
            return Partial("_CreateGroup", fileManageGroup);
        }
        public async Task<IActionResult> OnPostCreateGroup(FileManageGroup fileManageGroup)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ParentId = fileManageGroup.Parent_Id.Value;
                    await _fileManageGroupServices.Insert(fileManageGroup);
                    await _fileManageGroupServices.Save();
                    return new JsonResult(new { status = "ok", groupId = fileManageGroup.FileManageGroup_ID });
                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
        }
        public IActionResult OnGetCreateFileManageGroupTrans(int id)
        {
            TempData["FileManageGroupId"] = id;
            return Partial("_CreateFileManageGroupTrans");
        }
        public async Task<IActionResult> OnPostCreateFileManageGroupTrans(FileManageGroupTranslation fileManageGroupTranslation)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (!await _fileManageGroupTranslationSrevices
                        .ExistsAsync(x => x.Language_Id == fileManageGroupTranslation.Language_Id
                        && x.FileManageGroup_Id == fileManageGroupTranslation.FileManageGroup_Id))
                    {
                        //sitemap
                        fileManageGroupTranslation.ParentId = ParentId;
                        fileManageGroupTranslation.SiteMap_Id = await _siteMapSerivce.AddFileManageGroup(fileManageGroupTranslation);

                        await _fileManageGroupTranslationSrevices.Insert(fileManageGroupTranslation);
                        await _fileManageGroupTranslationSrevices.Save();
                        return new JsonResult(new { status = "ok", group_id = fileManageGroupTranslation.FileManageGroup_Id });
                    }
                    return new JsonResult("exsist");

                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }

        }

        #region OnGetViewDetailsFileManageGroup
        public async Task<IActionResult> OnGetViewDetailsFileManageGroup(int groupId)
        {
            try
            {
                var fileManageGroups = await _fileManageGroupTranslationSrevices.GetWithIncludeAsync<FileManageGroupTranslation>
                (
                selector: x => x,
                include: s => s.Include(x => x.FileManageGroup),
                where: x => x.FileManageGroup.FileManageGroup_ID == groupId && x.Language_Id == CultureInfo.CurrentCulture.Name
                );

                return Partial("_ViewDetailsFileManageGroup", fileManageGroups.FirstOrDefault());
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        public async Task<IActionResult> OnGetEditFileManageGroup(int id)
        {
            try
            {
                var fileManageGroup = await _fileManageGroupServices.GetAsync(x => x.FileManageGroup_ID == (Int16)id);
                return Partial("_EditFileManageGroup", fileManageGroup);
            }
            catch (Exception err)
            {
                return new JsonResult(new
                {
                    status = "error",
                    data = err.Message + " *** " + err.InnerException
                });
            }
        }
        public async Task<IActionResult> OnGetEditFileManageGroupTrans(int id)
        {
            try
            {
                var fileManageGroupTranslation = await _fileManageGroupTranslationSrevices.GetAsync(x => x.FileManageGroupTranslation_ID == id);

                return Partial("_EditFileManageGroupTrans", fileManageGroupTranslation);
            }
            catch (Exception err)
            {
                return new JsonResult(new
                {
                    status = "error",
                    data = err.Message + " *** " + err.InnerException
                });
            }
        }
        public async Task<IActionResult> OnGetFileManageGroupTransInfo(string id, int fileManageGroupId)
        {

            string languageId = string.IsNullOrEmpty(id) ? CultureInfo.CurrentCulture.Name : id.Trim();
            var fileManageGroupTrans = await _fileManageGroupTranslationSrevices
                .GetAsync(x => x.Language_Id == languageId && x.FileManageGroup_Id == fileManageGroupId);
            if (fileManageGroupTrans != null)
            {
                return new JsonResult(new
                {
                    status = "ok",
                    title = fileManageGroupTrans.Title,
                    slug = fileManageGroupTrans.Slug,
                    id = fileManageGroupTrans.FileManageGroupTranslation_ID
                });
            }

            return new JsonResult("nok");
        }

        public async Task<IActionResult> OnPostEditFileManageGroup(FileManageGroup fileManageGroup)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ParentIdEdit = fileManageGroup.Parent_Id.Value; //NotMap
                    _fileManageGroupServices.Update(fileManageGroup);
                    await _fileManageGroupServices.Save();
                    return new JsonResult("ok");
                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new
                {
                    status = "error",
                    data = err.Message + " *** " + err.InnerException
                });
            }
        }
        public async Task<IActionResult> OnPostEditFileManageGroupTrans(FileManageGroupTranslation fileManageGroupTranslation)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    //sitemap
                    await _siteMapSerivce.EditFileManageGroup(fileManageGroupTranslation);

                    _fileManageGroupTranslationSrevices.Update(fileManageGroupTranslation);
                    await _fileManageGroupTranslationSrevices.Save();
                    return new JsonResult("ok");
                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new
                {
                    status = "error",
                    data = err.Message + " *** " + err.InnerException
                });
            }
        }

        public async Task<IActionResult> OnGetDeleteFileManageGroup(int id, int fileManageGroupId, string status)
        {
            try
            {
                if (status == "one")
                {
                    ///TODO : IsExsits In FileManage
                    ///
                    if (!await _fileManageTranslationServices
                        .ExistsAsync(x => x.TranslationGroup_Id == id))
                    {
                        var fileManageGroupTrans = await _fileManageGroupTranslationSrevices.GetAsync(x => x.FileManageGroupTranslation_ID == id);

                        var fileManageGroupTransList = await _fileManageGroupTranslationSrevices.GetAllAsyn(x => x.FileManageGroup_Id == fileManageGroupId);

                        if (fileManageGroupTransList.Count() > 1)
                        {
                            //sitemap and fileManageGroup
                            await _siteMapSerivce.DeleteFileManageGroup(fileManageGroupTrans);

                            _fileManageGroupTranslationSrevices.Delete(fileManageGroupTrans);
                            await _fileManageGroupTranslationSrevices.Save();
                            return new JsonResult("ok");
                        }
                        else
                        {
                            //sitemap and fileManageGroup
                            await _siteMapSerivce.DeleteFileManageGroup(fileManageGroupTrans);

                            _fileManageGroupTranslationSrevices.Delete(fileManageGroupTrans);
                            await _fileManageGroupTranslationSrevices.Save();

                            //Delete FileManageGroup
                            var fileManageGroup = await _fileManageGroupServices.GetAsync(x => x.FileManageGroup_ID == fileManageGroupId);
                            _fileManageGroupServices.Delete(fileManageGroup);
                            await _fileManageGroupServices.Save();
                            return new JsonResult("ok");
                        }
                    }
                    return new JsonResult("exists");

                }
                else if (status == "all")
                {

                }
            }
            catch (Exception err)
            {
                return new JsonResult(new
                {
                    status = "error",
                    data = err.Message + " *** " + err.InnerException
                });
            }
            return new JsonResult("nok");
        }
        #endregion

        #region FileManage
        public IActionResult OnGetCreateFileManage()
        {
            return Partial("_CreateFileManage");
        }
        public async Task<IActionResult> OnPostCreateFileManage(FileManage fileManage, IFormFile uploadedFile)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (uploadedFile != null && fileManage.Type == UploadType.File.ToString())
                    {
                        fileManage.FileData = await CreateFile(uploadedFile);
                        fileManage.FileSize = MyExtensions.ToMB(uploadedFile.Length);
                    }

                    fileManage.CreateDate = DateTime.Now;

                    await _fileManageServices.Insert(fileManage);
                    await _fileManageServices.Save();
                    return new JsonResult(new { status = "ok", fileManageId = fileManage.FileManage_ID });
                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
        }
        public IActionResult OnGetCreateFileManageTrans(int id)
        {
            TempData["FileManageId"] = id;
            return Partial("_CreateFileManageTrans");
        }
        public async Task<IActionResult> OnPostCreateFileManageTrans(FileManageTranslation fileManageTranslation)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (!await _fileManageTranslationServices
                       .ExistsAsync(x => x.Language_Id == fileManageTranslation.Language_Id && x.FileManage_Id == fileManageTranslation.FileManage_Id))
                    {
                        ////sitemap
                        fileManageTranslation.SiteMap_Id = await _siteMapSerivce.AddFileManage(fileManageTranslation);

                        ///
                        await _fileManageTranslationServices.Insert(fileManageTranslation);
                        await _fileManageTranslationServices.Save();

                        return new JsonResult(new { status = "ok", fileManageTrans_id = fileManageTranslation.FileManageTranslation_ID, fileManage_id = fileManageTranslation.FileManage_Id });
                    }
                    return new JsonResult("exsist");
                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }

        }

        #region ViewAllFiles
        public async Task<IActionResult> OnPostViewAllFilesManage(int fileManageGroupTranslationId)
        {
            try
            {
                var draw = Request.Form["draw"].FirstOrDefault();
                var start = Request.Form["start"].FirstOrDefault();
                var length = Request.Form["length"].FirstOrDefault();
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
                var searchValue = Request.Form["search[value]"].FirstOrDefault();
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                IEnumerable<FileManageTranslation> fileManages = null;
                if (fileManageGroupTranslationId != 0)
                {
                    fileManages = await _fileManageTranslationServices.GetWithIncludeAsync<FileManageTranslation>
                      (
                      selector: x => x,
                      include: s => s.Include(x => x.FileManage),
                      where: x => x.TranslationGroup_Id == fileManageGroupTranslationId && x.Language_Id == CultureInfo.CurrentCulture.Name
                      );
                }
                else
                    fileManages = await _fileManageTranslationServices.GetWithIncludeAsync<FileManageTranslation>
                  (
                  selector: x => x,
                  include: s => s.Include(x => x.FileManage),
                  where: x => x.Language_Id == CultureInfo.CurrentCulture.Name
                  );

                if (!(string.IsNullOrEmpty(sortColumn)))
                {
                    fileManages = fileManages.AsQueryable().OrderBy(sortColumn + " " + sortColumnDirection);
                }
                if (!string.IsNullOrEmpty(searchValue))
                {
                    fileManages = fileManages.Where(m => m.Title.Contains(searchValue) || m.FileManage.CreateDate.ToShamsi().ToString().Contains(searchValue)
                    || m.FileManage.FileData.Contains(searchValue) || m.FileManage.FileSize.Contains(searchValue));

                }
                recordsTotal = fileManages.Count();
                var data = fileManages.Skip(skip).Take(pageSize).ToList();
                var jsonData = new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data };
                return new JsonResult(jsonData);
            }
            catch (Exception err)
            {
                throw;
            }
        }
        #endregion

        public async Task<IActionResult> OnGetEditFileManage(int id)
        {
            try
            {
                var fileManage = await _fileManageServices.GetAsync(x => x.FileManage_ID == id);

                return Partial("_EditFileManage", fileManage);
            }
            catch (Exception err)
            {
                return new JsonResult(new
                {
                    status = "error",
                    data = err.Message + " *** " + err.InnerException
                });
            }
        }
        public async Task<IActionResult> OnGetEditFileManageTrans(int id)
        {
            var fileManageTrans = await _fileManageTranslationServices.GetAsync(x => x.FileManageTranslation_ID == id);

            return Partial("_EditFileManageTrans", fileManageTrans);
        }
        public async Task<IActionResult> OnGetFileManageInfo(string id, int fileManageId)
        {

            string languageId = string.IsNullOrEmpty(id) ? CultureInfo.CurrentCulture.Name : id.Trim();
            var fileManageTrans = await _fileManageTranslationServices
                .GetAsync(x => x.Language_Id == languageId && x.FileManage_Id == fileManageId);
            if (fileManageTrans != null)
            {
                return new JsonResult(new
                {
                    status = "ok",
                    data = fileManageTrans
                });
            }

            return new JsonResult("nok");
        }

        public async Task<IActionResult> OnPostEditFileManage(FileManage fileManage, IFormFile uploadedFile)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (fileManage.FileData != fileManage.OldFileData)
                    {
                        if (fileManage.OldFileData.Contains("Files/FilesManage"))
                            FileManipulate.DeleteFile(_hostEnvironment.WebRootPath, "", fileManage.OldFileData);
                    }
                    if (uploadedFile != null)
                    {
                        string targetPath = Path.Combine(_hostEnvironment.WebRootPath, FilePath);
                        FileManipulate.CreateDirectory(targetPath);

                        fileManage.FileData = await CreateFile(uploadedFile);
                        fileManage.FileSize = MyExtensions.ToMB(uploadedFile.Length);
                    }
                    if (fileManage.Type != UploadType.File.ToString())
                        fileManage.FileSize = "0";

                    _fileManageServices.Update(fileManage);
                    await _fileManageServices.Save();
                    return new JsonResult(new { status = "ok", fileManageId = fileManage.FileManage_ID });
                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
        }
        public async Task<IActionResult> OnPostEditFileManageTrans(FileManageTranslation fileManageTranslation)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ////sitemap
                    fileManageTranslation.SiteMap_Id = await _siteMapSerivce.EditFileManage(fileManageTranslation);

                    ///
                    _fileManageTranslationServices.Update(fileManageTranslation);
                    await _fileManageTranslationServices.Save();
                    return new JsonResult("ok");
                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
        }

        public async Task<IActionResult> OnGetDeleteFileManage(int id, int FileManageId, string status)
        {
            try
            {
                var FileManageTransList = await _fileManageTranslationServices.GetWithIncludeAsync
                       (selector: x => x,
                       where: x => x.FileManage_Id == FileManageId,
                        include: s => s.Include(x => x.FileManageGroupTranslation));

                var FileManageTrans = await _fileManageTranslationServices.GetWithIncludeAsync
                    (selector: x => x,
                    where: x => x.FileManageTranslation_ID == id,
                    include: s => s.Include(x => x.FileManageGroupTranslation));

                if (status == "one")
                {
                    ////sitemap
                    await _siteMapSerivce.DeleteFileManage(FileManageTrans.FirstOrDefault());

                    if (FileManageTransList.Count() > 1)
                    {
                        _fileManageTranslationServices.Delete(FileManageTrans.FirstOrDefault());
                        await _fileManageTranslationServices.Save();
                    }
                    else
                    {
                        //var FileManageTrans = await _fileManageTranslationServices.GetAsync(x => x.FileManageTranslation_ID == id);
                        _fileManageTranslationServices.Delete(FileManageTrans.FirstOrDefault());
                        await _fileManageTranslationServices.Save();

                        var fileManage = await _fileManageServices.GetAsync(x => x.FileManage_ID == FileManageId);

                        if (fileManage.FileData.Contains("Files/FilesManage"))
                        {
                            FileManipulate.DeleteFile(_hostEnvironment.WebRootPath, "", fileManage.FileData);
                        }
                        _fileManageServices.Delete(fileManage);
                        await _fileManageGroupServices.Save();
                    }
                    return new JsonResult("ok");
                }
                else if (status == "all")
                {
                    ////sitemap
                    await _siteMapSerivce.DeleteAllFileManage(FileManageTransList);

                    _fileManageTranslationServices.DeleteAll(FileManageTransList);
                    await _fileManageTranslationServices.Save();

                    var fileManage = await _fileManageServices.GetAsync(x => x.FileManage_ID == FileManageId);
                    if (fileManage.FileData.Contains("Files/FilesManage"))
                    {
                        FileManipulate.DeleteFile(_hostEnvironment.WebRootPath, "", fileManage.FileData);
                    }

                    _fileManageServices.Delete(fileManage);
                    await _fileManageServices.Save();
                    return new JsonResult("ok");
                }

            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
            return new JsonResult("nok");
        }
        public async Task<string> CreateFile(IFormFile uploadedFile)
        {
            string targetPath = Path.Combine(_hostEnvironment.WebRootPath, FilePath);

            FileManipulate.CreateDirectory(targetPath);
            Tuple<string, string> FileUploadedPath = await FileManipulate.CreateFile(_hostEnvironment.WebRootPath, uploadedFile, FilePath);
            return Path.Combine(FilePath, FileUploadedPath.Item2);
        }
        #endregion

    }
}
