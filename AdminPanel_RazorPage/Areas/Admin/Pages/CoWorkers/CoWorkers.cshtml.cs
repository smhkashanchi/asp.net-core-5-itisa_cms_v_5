using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Application.Interfaces;

using Domain.DomainClasses.CoWorker;
using System.Linq.Dynamic.Core;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using System.Globalization;
using Microsoft.AspNetCore.Http;
using System.IO;
using Application.Utilities;

namespace ItisaCMS_RazorPage.Areas.Admin.Pages.CoWorkers
{
    public class CoWorkersModel : PageModel
    {
        public static string CoWorkerPicFilePath = "Files/CoWorker";
        private IWebHostEnvironment _hostEnvironment;
        private ICoWorkerGroupServices _coWorkerGroupServices;
        private ICoWorkerGroupTranslationServices _coWorkerGroupTranslationServices;
        private ICoWorkerServices _coWorkerServices;
        private ICoWorkerTranslationServices _coWorkerTranslationServices;
        public CoWorkersModel(ICoWorkerGroupServices coWorkerGroupServices, ICoWorkerGroupTranslationServices coWorkerGroupTranslationServices
            , ICoWorkerServices coWorkerServices, ICoWorkerTranslationServices coWorkerTranslationServices
            , IWebHostEnvironment hostEnvironment)
        {
            _hostEnvironment = hostEnvironment;
            _coWorkerGroupServices = coWorkerGroupServices;
            _coWorkerGroupTranslationServices = coWorkerGroupTranslationServices;
            _coWorkerServices = coWorkerServices;
            _coWorkerTranslationServices = coWorkerTranslationServices;
        }

        #region CoWorkerGroup
        public IActionResult OnGetCreateGroup()
        {
            CoWorkerGroup coWorkerGroup = new();
            TempData["PicWidth"] = coWorkerGroup.ImageWidth;
            TempData["PicHeight"] = coWorkerGroup.ImageHeight;
            return Partial("_CreateGroup");
        }
        public async Task<IActionResult> OnPostCreateGroup(CoWorkerGroup coWorkerGroup)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    await _coWorkerGroupServices.Insert(coWorkerGroup);
                    await _coWorkerGroupServices.Save();
                    return new JsonResult(new { status = "ok", groupId = coWorkerGroup.CoWorkerGroup_ID });
                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
        }
        public IActionResult OnGetCreateGroupTrans(int id)
        {
            TempData["GroupId"] = id;
            return Partial("_CreateGroupTrans");
        }
        public async Task<IActionResult> OnPostCreateGroupTrans(CoWorkerGroupTranslation coWorkerGroupTranslation)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (!await _coWorkerGroupTranslationServices
                        .ExistsAsync(x => x.Language_Id == coWorkerGroupTranslation.Language_Id && x.CoWorkerGroup_Id == coWorkerGroupTranslation.CoWorkerGroup_Id))
                    {
                        await _coWorkerGroupTranslationServices.Insert(coWorkerGroupTranslation);
                        await _coWorkerGroupTranslationServices.Save();
                        return new JsonResult("ok");
                    }
                    return new JsonResult("exsist");
                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
        }

        #region ViewAllGroups
        public async Task<IActionResult> OnPostViewAllGroups()
        {
            try
            {
                var draw = Request.Form["draw"].FirstOrDefault();
                var start = Request.Form["start"].FirstOrDefault();
                var length = Request.Form["length"].FirstOrDefault();
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
                var searchValue = Request.Form["search[value]"].FirstOrDefault();
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                var coWorkerGroups = await _coWorkerGroupTranslationServices.GetWithIncludeAsync<CoWorkerGroupTranslation>
                (
                selector: x => x,
                include: s => s.Include(x => x.CoWorkerGroup),
                where: x => x.Language_Id == CultureInfo.CurrentCulture.Name
                );
                if (!(string.IsNullOrEmpty(sortColumn)))
                {
                    coWorkerGroups = coWorkerGroups.AsQueryable().OrderBy(sortColumn + " " + sortColumnDirection);
                }
                if (!string.IsNullOrEmpty(searchValue))
                {
                    coWorkerGroups = coWorkerGroups.Where(m => m.Title.Contains(searchValue));

                }
                recordsTotal = coWorkerGroups.Count();
                var data = coWorkerGroups.Skip(skip).Take(pageSize).ToList();
                var jsonData = new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data };
                return new JsonResult(jsonData);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        public async Task<IActionResult> OnGetEditGroup(int id)
        {
            try
            {
                var coWorkerGroup = await _coWorkerGroupServices.GetAsync(x => x.CoWorkerGroup_ID == (byte)id);
                return Partial("_EditGroup", coWorkerGroup);
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
        }
        public async Task<IActionResult> OnGetEditGroupTrans(int id)
        {
            var coWorkerGroupTrans = await _coWorkerGroupTranslationServices.GetAsync(x => x.CoWorkerGroupTranslation_ID == id);

            return Partial("_EditGroupTrans", coWorkerGroupTrans);
        }
        public async Task<IActionResult> OnGetGroupTransInfo(string id, int coWorkerGroupId)
        {

            string languageId = string.IsNullOrEmpty(id) ? CultureInfo.CurrentCulture.Name : id.Trim();
            var coWorkerGroupTrans = await _coWorkerGroupTranslationServices
                .GetAsync(x => x.Language_Id == id && x.CoWorkerGroup_Id == coWorkerGroupId);
            if (coWorkerGroupTrans != null)
            {
                return new JsonResult(new
                {
                    status = "ok",
                    title = coWorkerGroupTrans.Title,
                    id = coWorkerGroupTrans.CoWorkerGroupTranslation_ID
                });
            }
            return new JsonResult("nok");
        }
        public async Task<IActionResult> OnPostEditGroup(CoWorkerGroup coWorkerGroup)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    await _coWorkerGroupServices.Update(coWorkerGroup);
                    await _coWorkerGroupServices.Save();
                    return new JsonResult("ok");
                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
        }
        public async Task<IActionResult> OnPostEditGroupTrans(CoWorkerGroupTranslation coWorkerGroupTranslation)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    await _coWorkerGroupTranslationServices.Update(coWorkerGroupTranslation);
                    await _coWorkerGroupTranslationServices.Save();
                    return new JsonResult("ok");
                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
        }
        public async Task<IActionResult> OnGetDeleteGroup(int id, int groupId, string status)
        {
            try
            {
                if (status == "one")
                {
                    if (!await _coWorkerTranslationServices.ExistsAsync(x => x.TranslationGroup_Id == id))
                    {
                        var groupTransList = await _coWorkerGroupTranslationServices.GetWithIncludeAsync
                            (selector: x => x,
                            where: x => x.CoWorkerGroup_Id == groupId);
                        if (groupTransList.Count() > 1)
                        {
                            var groupTrans = await _coWorkerGroupTranslationServices.GetAsync(x => x.CoWorkerGroupTranslation_ID == id);
                            _coWorkerGroupTranslationServices.DeleteAsync(groupTrans);
                            await _coWorkerGroupTranslationServices.Save();
                        }
                        else
                        {
                            var groupTrans = await _coWorkerGroupTranslationServices.GetAsync(x => x.CoWorkerGroupTranslation_ID == id);
                            _coWorkerGroupTranslationServices.DeleteAsync(groupTrans);
                            await _coWorkerGroupTranslationServices.Save();

                            var group = await _coWorkerGroupServices.GetAsync(x => x.CoWorkerGroup_ID == groupId);
                            _coWorkerGroupServices.DeleteAsync(group);
                            await _coWorkerGroupServices.Save();
                        }
                        return new JsonResult("ok");
                    }
                    return new JsonResult("exists");
                }

            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
            return new JsonResult("nok");
        }
        #endregion

        #region CoWorker
        public async Task<IActionResult> OnGetCreateCoWorker()
        {
            try
            {
                var coWorkerGroups = await _coWorkerGroupTranslationServices.GetWithIncludeAsync<CoWorkerGroupTranslation>
                     (
                     selector: x => x,
                     include: s => s.Include(x => x.CoWorkerGroup),
                     where: x => x.Language_Id == "fa-IR" && x.CoWorkerGroup.IsActive
                     );
                TempData["CoWorkerGroups"] = coWorkerGroups;

                return Partial("_CreateCoWorker");
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
        }
        public async Task<IActionResult> OnPostCreateCoWorker(CoWorker coWorker, IFormFile imgCoWorker)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (imgCoWorker != null)
                    {
                        coWorker.Image = await CreateCoWorkerImagesWithoutResize(imgCoWorker);
                    }
                    else
                        return new JsonResult("noImage");

                    await _coWorkerServices.Insert(coWorker);
                    await _coWorkerServices.Save();
                    return new JsonResult(new { status = "ok", coWorkerId = coWorker.CoWorker_ID });
                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
        }
        public IActionResult OnGetCreateCoWorkerTrans(int id)
        {
            TempData["CoWorkerId"] = id;
            return Partial("_CreateCoWorkerTrans");
        }
        public async Task<IActionResult> OnPostCreateCoWorkerTrans(CoWorkerTranslation coWorkerTranslation)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (!await _coWorkerTranslationServices
                        .ExistsAsync(x => x.Language_Id == coWorkerTranslation.Language_Id && x.CoWorker_Id == coWorkerTranslation.CoWorker_Id))
                    {
                        await _coWorkerTranslationServices.Insert(coWorkerTranslation);
                        await _coWorkerTranslationServices.Save();
                        return new JsonResult("ok");
                    }
                    return new JsonResult("exsist");

                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
        }

        public async Task<IActionResult> OnGetCoWorkerImageSize(int groupId)
        {
            var coWorkerGroup = await _coWorkerGroupServices.GetAsync(x => x.CoWorkerGroup_ID == groupId);
            return new JsonResult(coWorkerGroup);
        }
        public async Task<IActionResult> OnGetCoWorkerGroupByLang(string id)
        {
            string lang_Id = "";
            if (string.IsNullOrEmpty(id))
                lang_Id = CultureInfo.CurrentCulture.Name;
            else
                lang_Id = id;

            var coWorkerGroups = await _coWorkerGroupTranslationServices.GetWithIncludeAsync<CoWorkerGroupTranslation>
                     (
                     selector: x => x,
                     include: s => s.Include(x => x.CoWorkerGroup),
                     where: x => x.Language_Id == lang_Id && x.CoWorkerGroup.IsActive
                     );

            return new JsonResult(coWorkerGroups);
        }

        #region ViewAllCoWorkers
        public async Task<IActionResult> OnPostViewAllCoWorkers(int groupId)
        {
            try
            {
                var draw = Request.Form["draw"].FirstOrDefault();
                var start = Request.Form["start"].FirstOrDefault();
                var length = Request.Form["length"].FirstOrDefault();
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
                var searchValue = Request.Form["search[value]"].FirstOrDefault();
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                IEnumerable<CoWorkerTranslation> coWorkers = null;
                if (groupId != 0)
                    coWorkers = await _coWorkerTranslationServices.GetWithIncludeAsync<CoWorkerTranslation>
                    (
                    selector: x => x,
                    include: s => s.Include(x => x.CoWorker).Include(x => x.CoWorkerGroupTranslation),
                    where: x => x.Language_Id == CultureInfo.CurrentCulture.Name && x.CoWorkerGroupTranslation.CoWorkerGroup_Id== groupId
                    );
                else
                    coWorkers = await _coWorkerTranslationServices.GetWithIncludeAsync<CoWorkerTranslation>
                       (
                       selector: x => x,
                       include: s => s.Include(x => x.CoWorker).Include(x => x.CoWorkerGroupTranslation),
                       where: x => x.Language_Id == CultureInfo.CurrentCulture.Name
                       );

                if (!(string.IsNullOrEmpty(sortColumn)))
                {
                    coWorkers = coWorkers.AsQueryable().OrderBy(sortColumn + " " + sortColumnDirection);
                }
                if (!string.IsNullOrEmpty(searchValue))
                {
                    coWorkers = coWorkers.Where(m => m.Title.Contains(searchValue)
                        ||m.Link.Contains(searchValue)
                        || m.CoWorkerGroupTranslation.Title.Contains(searchValue));

                }
                recordsTotal = coWorkers.Count();
                var data = coWorkers.Skip(skip).Take(pageSize).ToList();
                var jsonData = new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data };
                return new JsonResult(jsonData);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        public async Task<IActionResult> OnGetEditCoWorker(int id)
        {
            try
            {
                var coWorkerGroups = await _coWorkerGroupTranslationServices.GetWithIncludeAsync<CoWorkerGroupTranslation>
                    (
                    selector: x => x,
                    include: s => s.Include(x => x.CoWorkerGroup),
                    where: x => x.Language_Id == "fa-IR" && x.CoWorkerGroup.IsActive
                    );
                TempData["CoWorkerGroups"] = coWorkerGroups;


                var coWorker = await _coWorkerServices.GetAsync(x => x.CoWorker_ID == (Int16)id);
                return Partial("_EditCoWorker", coWorker);
            }
            catch (Exception err)
            {
                return new JsonResult(new
                {
                    status = "error",
                    data = err.Message + " *** " + err.InnerException
                });
            }
        }
        public async Task<IActionResult> OnGetEditCoWorkerTrans(int id)
        {
            var coWorkerTrans = await _coWorkerTranslationServices.GetAsync(x => x.CoWorkerTranslation_ID == id);

            return Partial("_EditCoWorkerTrans", coWorkerTrans);
        }
        public async Task<IActionResult> OnGetCoWorkerTransInfo(string id, int coWorkerId)
        {

            string languageId = string.IsNullOrEmpty(id) ? CultureInfo.CurrentCulture.Name : id.Trim();
            var coWorkerTrans = await _coWorkerTranslationServices
                .GetAsync(x => x.Language_Id == languageId && x.CoWorker_Id == coWorkerId);
            if (coWorkerTrans != null)
            {
                return new JsonResult(new
                {
                    status = "ok",
                    title = coWorkerTrans.Title,
                    isActive = coWorkerTrans.IsActive,
                    link = coWorkerTrans.Link,
                    coWorker_Id = coWorkerTrans.CoWorker_Id,
                    groupId = coWorkerTrans.TranslationGroup_Id,
                    id = coWorkerTrans.CoWorkerTranslation_ID
                });
            }

            return new JsonResult("nok");
        }

        public async Task<IActionResult> OnPostEditCoWorker(CoWorker coWorker, IFormFile imgCoWorker, IFormFile imgOrginal, IFormFile imgThumb, IFormFile imgMedium)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (imgCoWorker != null)
                    {
                        if (coWorker.Image != null)
                        {
                            FileManipulate.DeleteFile(_hostEnvironment.WebRootPath, CoWorkerPicFilePath, coWorker.Image);
                        }
                        coWorker.Image = await CreateCoWorkerImagesWithoutResize(imgCoWorker);
                    }
                    await _coWorkerServices.Update(coWorker);
                    await _coWorkerServices.Save();
                    return new JsonResult("ok");
                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new
                {
                    status = "error",
                    data = err.Message + " *** " + err.InnerException
                });
            }
        }
        public async Task<IActionResult> OnPostEditCoWorkerTrans(CoWorkerTranslation coWorkerTranslation)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    await _coWorkerTranslationServices.Update(coWorkerTranslation);
                    await _coWorkerTranslationServices.Save();
                    return new JsonResult("ok");
                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new
                {
                    status = "error",
                    data = err.Message + " *** " + err.InnerException
                });
            }
        }

        public async Task<IActionResult> OnGetDeleteCoWorker(int id, int coWorkerId, string status)
        {
            try
            {
                var coWorkerTransList = await _coWorkerTranslationServices.GetWithIncludeAsync
                        (selector: x => x,
                        where: x => x.CoWorker_Id == (Int16)coWorkerId);
                var coWorkerTrans = await _coWorkerTranslationServices.GetAsync(x => x.CoWorkerTranslation_ID == id);
                if (status == "one")
                {
                    
                    if (coWorkerTransList.Count() > 1)
                    {
                        
                        _coWorkerTranslationServices.Delete(coWorkerTrans);
                        await _coWorkerTranslationServices.Save();
                    }
                    else
                    {
                        //var coWorkerTrans = await _coWorkerTranslationServices.GetAsync(x => x.CoWorkerTranslation_ID == id);
                        _coWorkerTranslationServices.Delete(coWorkerTrans);
                        await _coWorkerTranslationServices.Save();

                        var coWorker = await _coWorkerServices.GetAsync(x => x.CoWorker_ID == coWorkerId);

                        if (coWorker.Image != null)
                        {
                            FileManipulate.DeleteFile(_hostEnvironment.WebRootPath, CoWorkerPicFilePath, coWorker.Image);
                        }
                        _coWorkerServices.DeleteAsync(coWorker);
                        await _coWorkerServices.Save();
                    }
                    return new JsonResult("ok");
                }
                else if (status == "all")
                {

                    _coWorkerTranslationServices.DeleteAll(coWorkerTransList);
                    await _coWorkerTranslationServices.Save();

                    var coWorker = await _coWorkerServices.GetAsync(x => x.CoWorker_ID == coWorkerId);
                    if (coWorker.Image != null)
                        FileManipulate.DeleteFile(_hostEnvironment.WebRootPath, CoWorkerPicFilePath, coWorker.Image);

                    _coWorkerServices.DeleteAsync(coWorker);
                    await _coWorkerServices.Save();

                    return new JsonResult("ok");
                }

            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
            return new JsonResult("nok");
        }

        #region ImageCreator
        public async Task<string> CreateCoWorkerImagesWithoutResize(IFormFile uploadedImage)
        {
            string FullImagePath = "";
            string targetPath = Path.Combine(_hostEnvironment.WebRootPath, CoWorkerPicFilePath);

            FileManipulate.CreateDirectory(targetPath);

            ///For Combine :ImageName and ImageFullPath
            Tuple<string, string> ImagePath = await FileManipulate.CreateFile(_hostEnvironment.WebRootPath, uploadedImage, CoWorkerPicFilePath);
            FullImagePath = Path.Combine(CoWorkerPicFilePath, ImagePath.Item2);

            return FullImagePath;
        }
        #endregion

        #endregion
    }
}
