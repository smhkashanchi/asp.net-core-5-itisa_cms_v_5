﻿using Application.Interfaces;
using Application.Utilities;

using Domain.DomainClasses.Gallery;

using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

using Utilities;

namespace ItisaCMS_RazorPage.Areas.Admin.Pages.Galleries
{
    public class GalleriesModel : PageModel
    {
        public static string GalleryFilePath = "Files/Galleries";
        public static string galleryPicTempFilePath = "Files/Galleries/GalleryPic/Temp";
        public static string galleryPicFilePath = "Files/Galleries/GalleryPic";
        public static string galleryThumbPicFilePath = "Files/Galleries/GalleryPic/Thumb";
        public static string galleryMediumPicFilePath = "Files/Galleries/GalleryPic/Medium";
        private IWebHostEnvironment _hostEnvironment;
        private IGalleryGroupServices _galleryGroupServices;
        private IGalleryServices _galleryServices;
        private IGalleryPictureServices _galleryPictureServices;
        private IGalleryTranslationServices _galleryTranslationServices;
        private IGalleryGroupTranslationServices _galleryGroupTranslationServices;
        private ILanguageServices _languageServices;
        public GalleriesModel(IGalleryGroupServices galleryGroupServices, IGalleryServices galleryServices
            , IGalleryTranslationServices galleryTranslationServices, IGalleryPictureServices galleryPictureServices
            , IWebHostEnvironment environment, IGalleryGroupTranslationServices galleryGroupTranslationServices, ILanguageServices languageServices)
        {
            _hostEnvironment = environment;
            _galleryGroupServices = galleryGroupServices;
            _galleryServices = galleryServices;
            _galleryTranslationServices = galleryTranslationServices;
            _galleryPictureServices = galleryPictureServices;
            _galleryGroupTranslationServices = galleryGroupTranslationServices;
            _languageServices = languageServices;
        }

        #region GalleryGroup
        public IActionResult OnGetCreateGalleryGroup()
        {
            return Partial("_CreateGalleryGroup");
        }
        public async Task<IActionResult> OnPostCreateGalleryGroup(GalleryGroup galleryGroup)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    await _galleryGroupServices.Insert(galleryGroup);
                    await _galleryGroupServices.Save();
                    return new JsonResult(new { status = "ok", groupId = galleryGroup.GalleryGroup_ID });
                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
        }
        public IActionResult OnGetCreateGroupTrans(int id)
        {
            TempData["GroupId"] = id;
            return Partial("_CreateGroupTrans");
        }
        public async Task<IActionResult> OnPostCreateGroupTrans(GalleryGroupTranslation galleryGroupTranslation)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (!await _galleryGroupTranslationServices
                     .ExistsAsync(x => x.Language_Id == galleryGroupTranslation.Language_Id && x.GalleryGroup_Id == galleryGroupTranslation.GalleryGroup_Id))
                    {
                        await _galleryGroupTranslationServices.Insert(galleryGroupTranslation);
                        await _galleryGroupTranslationServices.Save();
                        return new JsonResult("ok");
                    }
                    return new JsonResult("exsist");
                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
        }
        #region ViewAllGroups
        public async Task<IActionResult> OnPostViewAllGalleryGroups()
        {
            try
            {
                var draw = Request.Form["draw"].FirstOrDefault();
                var start = Request.Form["start"].FirstOrDefault();
                var length = Request.Form["length"].FirstOrDefault();
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
                var searchValue = Request.Form["search[value]"].FirstOrDefault();
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                var galleryGroups = await _galleryGroupTranslationServices.GetWithIncludeAsync<GalleryGroupTranslation>
                (
                selector: x => x,
                include: s => s.Include(x => x.GalleryGroup),
                where: x => x.Language_Id == CultureInfo.CurrentCulture.Name
                );
                if (!(string.IsNullOrEmpty(sortColumn)))
                {
                    galleryGroups = galleryGroups.AsQueryable().OrderBy(sortColumn + " " + sortColumnDirection);
                }
                if (!string.IsNullOrEmpty(searchValue))
                {
                    galleryGroups = galleryGroups.Where(m => m.Title.Contains(searchValue));

                }
                recordsTotal = galleryGroups.Count();
                var data = galleryGroups.Skip(skip).Take(pageSize).ToList();
                var jsonData = new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data };
                return new JsonResult(jsonData);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        public async Task<IActionResult> OnGetEditGalleryGroup(int id)
        {
            try
            {
                var galleryGroup = await _galleryGroupServices
                    .GetAsync(x => x.GalleryGroup_ID == (byte)id);
                return Partial("_EditGroup", galleryGroup);
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
        }
        public async Task<IActionResult> OnGetEditGroupTrans(int id)
        {
            var galleryGroupTrans = await _galleryGroupTranslationServices.GetAsync(x => x.GalleryGroupTranslation_ID == id);

            return Partial("_EditGroupTrans", galleryGroupTrans);
        }
        public async Task<IActionResult> OnGetGroupTransInfo(string id, int galleryGroupId)
        {

            string languageId = string.IsNullOrEmpty(id) ? CultureInfo.CurrentCulture.Name : id.Trim();
            var galleryGroupTrans = await _galleryGroupTranslationServices
                .GetAsync(x => x.Language_Id == id && x.GalleryGroup_Id == galleryGroupId);
            if (galleryGroupTrans != null)
            {
                return new JsonResult(new
                {
                    status = "ok",
                    title = galleryGroupTrans.Title,
                    id = galleryGroupTrans.GalleryGroupTranslation_ID
                });
            }
            return new JsonResult("nok");
        }
        public async Task<IActionResult> OnPostEditGalleryGroup(GalleryGroup galleryGroup)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    await _galleryGroupServices.Update(galleryGroup);
                    await _galleryGroupServices.Save();
                    return new JsonResult("ok");
                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
        }
        public async Task<IActionResult> OnPostEditGroupTrans(GalleryGroupTranslation galleryGroupTranslation)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    await _galleryGroupTranslationServices.Update(galleryGroupTranslation);
                    await _galleryGroupTranslationServices.Save();
                    return new JsonResult("ok");
                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
        }
        public async Task<IActionResult> OnGetDeleteGalleryGroup(int id, int groupId, string status)
        {
            try
            {
                if (status == "one")
                {
                    if (!await _galleryTranslationServices.ExistsAsync(x => x.GroupTranslation_Id == id))
                    {
                        var groupTransList = await _galleryGroupTranslationServices.GetWithIncludeAsync
                            (selector: x => x,
                            where: x => x.GalleryGroup_Id == groupId);
                        if (groupTransList.Count() > 1)
                        {
                            var groupTrans = await _galleryGroupTranslationServices.GetAsync(x => x.GalleryGroupTranslation_ID == id);
                            _galleryGroupTranslationServices.Delete(groupTrans);
                            await _galleryGroupTranslationServices.Save();
                        }
                        else
                        {
                            var groupTrans = await _galleryGroupTranslationServices.GetAsync(x => x.GalleryGroupTranslation_ID == id);
                            _galleryGroupTranslationServices.Delete(groupTrans);
                            await _galleryGroupTranslationServices.Save();

                            var group = await _galleryGroupServices.GetAsync(x => x.GalleryGroup_ID == groupId);
                            _galleryGroupServices.DeleteAsync(group);
                            await _galleryGroupServices.Save();
                        }
                        return new JsonResult("ok");
                    }
                    return new JsonResult("exists");
                }
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
            return new JsonResult("nok");
        }

        #endregion

        #region Gallery
        public IActionResult OnGetCreateGallery()
        {
            try
            {

                return Partial("_CreateGallery");
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
        }
        public async Task<IActionResult> OnPostCreateGallery(Gallery gallery, IFormFile imgGallery)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (imgGallery != null)
                    {
                        FileManipulate.CreateDirectory(GalleryFilePath, _hostEnvironment.WebRootPath);
                        Tuple<string, string> ImagePath = await FileManipulate.CreateFile(_hostEnvironment.WebRootPath, imgGallery, GalleryFilePath);
                        gallery.Image = Path.Combine(GalleryFilePath, ImagePath.Item2); /// دخیره مسیر عکس همراه با نام عکس
                    }
                    await _galleryServices.InsertGallery(gallery);
                    await _galleryServices.Save();
                    return new JsonResult(new { status = "ok", galleryId = gallery.Gallery_ID });
                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
        }
        public IActionResult OnGetCreateGalleryTrans(int id)
        {
            TempData["GalleryId"] = id;
            return Partial("_CreateGalleryTrans");
        }
        public async Task<IActionResult> OnGetGalleryGroupByLang(string id)
        {
            string lang = "";
            if (string.IsNullOrEmpty(id))
                lang = CultureInfo.CurrentCulture.Name;
            else
                lang = id;

            var galleryGroups = await _galleryGroupTranslationServices.GetWithIncludeAsync<GalleryGroupTranslation>
                (
                selector: x => x,
                where: x => x.Language_Id == lang,
                include: s => s.Include(x => x.GalleryGroup)
                );
            return new JsonResult(galleryGroups);
        }
        public async Task<IActionResult> OnPostCreateGalleryTrans(GalleryTranslation galleryTranslation)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (!await _galleryTranslationServices
                        .ExistsAsync(x => x.Language_Id == galleryTranslation.Language_Id && x.Gallery_Id == galleryTranslation.Gallery_Id))
                    {
                        if (string.IsNullOrEmpty(galleryTranslation.Slug))
                            galleryTranslation.Slug = galleryTranslation.Title.ToSlug();


                        await _galleryTranslationServices.Insert(galleryTranslation);
                        await _galleryTranslationServices.Save();
                        return new JsonResult("ok");
                    }
                    return new JsonResult("exsist");

                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
        }
        #region ViewAllGalleries
        public async Task<IActionResult> OnPostViewAllGalleries(int groupId)
        {
            try
            {
                var draw = Request.Form["draw"].FirstOrDefault();
                var start = Request.Form["start"].FirstOrDefault();
                var length = Request.Form["length"].FirstOrDefault();
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
                var searchValue = Request.Form["search[value]"].FirstOrDefault();
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                IEnumerable<GalleryTranslation> galleries = null;
                if (groupId != 0)
                    galleries = await _galleryTranslationServices.GetWithIncludeAsync<GalleryTranslation>
                    (
                    selector: x => x,
                    include: s => s.Include(x => x.Gallery).Include(x => x.GalleryGroupTranslation),
                    where: x => x.Language_Id == CultureInfo.CurrentCulture.Name && x.GalleryGroupTranslation.GalleryGroup_Id == groupId
                    );
                else
                    galleries = await _galleryTranslationServices.GetWithIncludeAsync<GalleryTranslation>
                   (
                   selector: x => x,
                   include: s => s.Include(x => x.Gallery).Include(x => x.GalleryGroupTranslation),
                   where: x => x.Language_Id == CultureInfo.CurrentCulture.Name);

                if (!(string.IsNullOrEmpty(sortColumn)))
                {
                    galleries = galleries.AsQueryable().OrderBy(sortColumn + " " + sortColumnDirection);
                }
                if (!string.IsNullOrEmpty(searchValue))
                {
                    galleries = galleries.Where(m => m.Title.Contains(searchValue)
                        || m.GalleryGroupTranslation.Title.Contains(searchValue));

                }
                recordsTotal = galleries.Count();
                var data = galleries.Skip(skip).Take(pageSize).ToList();
                var jsonData = new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data };
                return new JsonResult(jsonData);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        public async Task<IActionResult> OnGetEditGallery(int id)
        {
            try
            {
                var gallery = await _galleryServices.GetAsync(x => x.Gallery_ID == (Int16)id);

                return Partial("_EditGallery", gallery);
            }
            catch (Exception err)
            {
                return new JsonResult(new
                {
                    status = "error",
                    data = err.Message + " *** " + err.InnerException
                });
            }
        }
        public async Task<IActionResult> OnGetEditGalleryTrans(int id)
        {
            var galleryTrans = await _galleryTranslationServices.GetAsync(x => x.GalleryTranslation_ID == id);

            return Partial("_EditGalleryTrans", galleryTrans);
        }
        public async Task<IActionResult> OnGetGalleryTransInfo(string id, int galleryId)
        {

            string languageId = string.IsNullOrEmpty(id) ? CultureInfo.CurrentCulture.Name : id.Trim();
            var galleryTrans = await _galleryTranslationServices
                .GetAsync(x => x.Language_Id == languageId && x.Gallery_Id == galleryId);
            if (galleryTrans != null)
            {
                return new JsonResult(new
                {
                    status = "ok",
                    title = galleryTrans.Title,
                    isActive = galleryTrans.IsActive,
                    slug = galleryTrans.Slug,
                    groupId = galleryTrans.GroupTranslation_Id,
                    id = galleryTrans.GalleryTranslation_ID
                });
            }

            return new JsonResult("nok");
        }

        public async Task<IActionResult> OnPostEditGallery(Gallery gallery, IFormFile imgGallery)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (imgGallery != null)
                    {
                        if (gallery.Image != null && !gallery.Image.Contains("Files/Images"))
                            FileManipulate.DeleteFile(_hostEnvironment.WebRootPath, "", gallery.Image);

                        FileManipulate.CreateDirectory(GalleryFilePath);
                        Tuple<string, string> ImagePath = await FileManipulate.CreateFile(_hostEnvironment.WebRootPath, imgGallery, GalleryFilePath);
                        gallery.Image = Path.Combine(ImagePath.Item1, ImagePath.Item2);
                    }
                    await _galleryServices.UpdateGallery(gallery);
                    await _galleryServices.Save();
                    return new JsonResult("ok");
                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new
                {
                    status = "error",
                    data = err.Message + " *** " + err.InnerException
                });
            }
        }
        public async Task<IActionResult> OnPostEditGalleryTrans(GalleryTranslation galleryTranslation)
        {
            try
            {
                if (ModelState.IsValid)
                {


                    _galleryTranslationServices.Update(galleryTranslation);
                    await _galleryTranslationServices.Save();
                    return new JsonResult("ok");
                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new
                {
                    status = "error",
                    data = err.Message + " *** " + err.InnerException
                });
            }
        }
        public async Task<IActionResult> OnGetDeleteGallery(int id, int galleryId, string status)
        {
            try
            {
                var galleryTransList = await _galleryTranslationServices.GetWithIncludeAsync
                       (selector: x => x,
                       where: x => x.Gallery_Id == galleryId);

                var galleryTrans = await _galleryTranslationServices.GetAsync(x => x.GalleryTranslation_ID == id);

                if (status == "one")
                {
                    if (galleryTransList.Count() > 1)
                    {
                        _galleryTranslationServices.Delete(galleryTrans);
                        await _galleryTranslationServices.Save();
                    }
                    else
                    {
                        _galleryTranslationServices.Delete(galleryTrans);
                        await _galleryTranslationServices.Save();

                        var gallery = await _galleryServices.GetAsync(x => x.Gallery_ID == galleryId);

                        if (gallery.Image != null && !gallery.Image.Contains("Files/Images"))
                            FileManipulate.DeleteFile(_hostEnvironment.WebRootPath, "", gallery.Image);

                        _galleryServices.Delete(gallery);
                        await _galleryServices.Save();
                    }
                    return new JsonResult("ok");
                }
                else if (status == "all")
                {

                    _galleryTranslationServices.DeleteAll(galleryTransList);
                    await _galleryTranslationServices.Save();

                    var gallery = await _galleryServices.GetAsync(x => x.Gallery_ID == galleryId);

                    if (gallery.Image != null && !gallery.Image.Contains("Files/Images"))
                        FileManipulate.DeleteFile(_hostEnvironment.WebRootPath, "", gallery.Image);

                    _galleryServices.Delete(gallery);
                    await _galleryServices.Save();

                    return new JsonResult("ok");
                }

            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
            return new JsonResult("nok");
        }
        #endregion

        #region GalleryPicture

        public async Task<IActionResult> OnGetViewAllGalleryPicture(int galleryId)
        {
            var gallery = await _galleryTranslationServices.GetWithIncludeAsync(
                  selector: x => x,
                  where: x => x.Gallery_Id == galleryId,
                  include: s => s.Include(x => x.GalleryGroupTranslation).ThenInclude(x => x.GalleryGroup)
                  );
            TempData["GalleryGroup"] = gallery.FirstOrDefault().GalleryGroupTranslation.GalleryGroup;

            var galleryPictures = await _galleryPictureServices.GetAllAsyn(x => x.Gallery_Id == galleryId);
            return Partial("_ViewAllGalleryPicture", galleryPictures);
        }
        public IActionResult OnGetCreateGalleryPicture(int Id)
        {
            TempData["GalleryId"] = Id;
            return Partial("_CreateGalleryPicture");
        }
        public async Task<IActionResult> OnPostUpload(List<IFormFile> ImageUpload)
        {//این تابع جهت ذخیره عکس ها در فولدر تمپ
            FileManipulate.CreateDirectory(galleryPicTempFilePath, _hostEnvironment.WebRootPath);
            RemoveTempFiles();
            Tuple<string, long> fileNames = await FileManipulate.CreateMultiFile(ImageUpload, _hostEnvironment.WebRootPath, galleryPicTempFilePath);

            return new JsonResult(new { count = ImageUpload.Count, fileNames.Item2, filePath = fileNames.Item1 });
        }
        public async Task<IActionResult> OnPostCreateGalleryPicture(Int16 galleryId, List<string> images)
        {
            try
            {
                string directory = Path.GetDirectoryName(images[0]);
                string thumbDirectory = Path.Combine(directory, "Thumb");
                string mediumDirectory = Path.Combine(directory, "Medium");

                FileManipulate.CreateDirectory("", _hostEnvironment.WebRootPath, thumbDirectory,mediumDirectory);

                var gallery = await _galleryTranslationServices.GetWithIncludeAsync(
                    selector: x => x,
                    where: x => x.Gallery_Id == galleryId,
                    include: s => s.Include(x => x.GalleryGroupTranslation).ThenInclude(x => x.GalleryGroup)
                    );

                GalleryPicture galleryPicture = new GalleryPicture();
                for (int i = 0; i < images.Count; i++)
                {
                    int smallPicWidth = int.Parse(gallery.FirstOrDefault().GalleryGroupTranslation.GalleryGroup.SmallPicWidth);
                    int smallPicHeight = int.Parse(gallery.FirstOrDefault().GalleryGroupTranslation.GalleryGroup.SmallPicHeight);
                    int mediumPicWidth = int.Parse(gallery.FirstOrDefault().GalleryGroupTranslation.GalleryGroup.MediumPicWidth);
                    int mediumPicHeight = int.Parse(gallery.FirstOrDefault().GalleryGroupTranslation.GalleryGroup.MediumPicHeight);

                    /// Item1 :Orginal Item2:Medium Item3:Thumb
                    Tuple<string, string, string> fileOfFilePaths = FileManipulate.CreateNewFileNameWithSizeInfo(images[i], smallPicWidth, smallPicHeight);


                    string orginalFullFilePath = Path.Combine(_hostEnvironment.WebRootPath, fileOfFilePaths.Item1);
                    string mediumFullFilePath = Path.Combine(_hostEnvironment.WebRootPath, fileOfFilePaths.Item2);
                    string thumbFullFilePath = Path.Combine(_hostEnvironment.WebRootPath, fileOfFilePaths.Item3);

                    string fileName = Path.GetFileName(images[i]);

                    if (!System.IO.File.Exists(thumbFullFilePath) && !System.IO.File.Exists(mediumFullFilePath))
                    {
                        galleryPicture.GalleryPic_ID = Guid.NewGuid().ToString();
                        galleryPicture.Gallery_Id = galleryId;
                        galleryPicture.Image = images[i].Trim().ToString();
                        await _galleryPictureServices.Insert(galleryPicture);
                        await _galleryPictureServices.Save();
                    }

                    if (!System.IO.File.Exists(thumbFullFilePath))
                        FileManipulate.resizeImage2(orginalFullFilePath, thumbFullFilePath, smallPicWidth, smallPicHeight);

                    if (!System.IO.File.Exists(mediumFullFilePath))
                        FileManipulate.resizeImage2(orginalFullFilePath, mediumFullFilePath, mediumPicWidth, mediumPicHeight);

                }

                return new JsonResult("ok");

            }
            catch (Exception err)
            {

                throw;
            }
        }
        public void OnGetRemoveFileFromTemp(string filename)
        {
            FileManipulate.DeleteFile(_hostEnvironment.WebRootPath, galleryPicTempFilePath, filename);
        }
        public JsonResult RemoveTempFiles()
        {
            string sourcePath = Path.Combine(_hostEnvironment.WebRootPath, galleryPicTempFilePath);
            string[] files = System.IO.Directory.GetFiles(sourcePath);
            foreach (string file in files)
            {
                if (System.IO.File.Exists(System.IO.Path.Combine(sourcePath, file)))
                {
                    try
                    {
                        System.IO.File.Delete(file);
                    }
                    catch (System.IO.IOException)
                    {
                        return new JsonResult("NOK");
                    }
                }
            }
            return new JsonResult("ok");
        }
        public async Task<JsonResult> OnGetDeleteGalleryPic(string values)
        {
            if (values != "")
            {
                string[] splitedValues = values.Split('&');
                for (int i = 0; i < splitedValues.Length - 1; i++)
                {
                    var galleryPic = await _galleryPictureServices.GetAsync(x => x.GalleryPic_ID == splitedValues[i]);
                    var gallery = await _galleryTranslationServices.GetWithIncludeAsync(
                      selector: x => x,
                      where: x => x.Gallery_Id == galleryPic.Gallery_Id,
                      include: s => s.Include(x => x.GalleryGroupTranslation).ThenInclude(x => x.GalleryGroup)
                      );

                    if (galleryPic != null)
                    {
                        int smallPicWidth = int.Parse(gallery.FirstOrDefault().GalleryGroupTranslation.GalleryGroup.SmallPicWidth);
                        int smallPicHeight = int.Parse(gallery.FirstOrDefault().GalleryGroupTranslation.GalleryGroup.SmallPicHeight);
                        int mediumPicWidth = int.Parse(gallery.FirstOrDefault().GalleryGroupTranslation.GalleryGroup.MediumPicWidth);
                        int mediumPicHeight = int.Parse(gallery.FirstOrDefault().GalleryGroupTranslation.GalleryGroup.MediumPicHeight);

                        /// Item1 :Orginal Item2:Medium Item3:Thumb
                        Tuple<string, string, string> fileOfFilePaths = FileManipulate.CreateNewFileNameWithSizeInfo(galleryPic.Image, smallPicWidth, smallPicHeight);
                        string thumbFullFilePath = Path.Combine(_hostEnvironment.WebRootPath, fileOfFilePaths.Item3);
                        string mediumFullFilePath = Path.Combine(_hostEnvironment.WebRootPath, fileOfFilePaths.Item2);

                        RemoveFile(thumbFullFilePath, mediumFullFilePath);
                        _galleryPictureServices.DeleteAsync(galleryPic);
                        await _galleryPictureServices.Save();
                    }
                }
            }
            return new JsonResult("ok");
        }
        public void RemoveFile(string thumbFilePath,string mediumFilePath)
        {
            FileManipulate.DeleteFile("", thumbFilePath,"");
            FileManipulate.DeleteFile("", mediumFilePath,"");
        }


        #endregion
    }
}
