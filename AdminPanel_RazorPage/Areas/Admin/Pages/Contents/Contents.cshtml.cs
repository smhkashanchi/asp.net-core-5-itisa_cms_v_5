﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

using Application.Interfaces;
using Application.Services;
using Application.Utilities;

using Domain.DomainClasses.Content;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;

using Newtonsoft.Json;

using Utilities;
using Microsoft.AspNetCore.Hosting;
using Application.ViewModels;
using AutoMapper;

namespace ItisaCMS_RazorPage.Areas.Admin.Pages.Contents
{
    public class ContentsModel : PageModel
    {
        public static string temp = "";
        List<short> Ids = new List<short>();
        List<short> ContentGroupTranslationIdSelected = new List<short>();
        public static string ContentPicFilePath = "Files/Content";
        public static string ContentThumbPicFilePath = "Files/Content/Thumb";
        public static byte ParentId = 0;
        public static byte ParentIdEdit = 0;
        public static List<short> SelectedGroupIds = new List<short>();
        private SiteMap sm;
        private IMapper _mapper;
        private IWebHostEnvironment _hostEnvironment;
        public ISiteMapService _siteMapSerivce;
        private IContentGroupServices _contentGroupServices;
        private IContentGroupTranslationSrevices _contentGroupTranslationSrevices;
        private IContentServices _contentServices;
        private IGalleryTranslationServices _galleryTranslationServices;
        private IContentTranslationServices _contentTranslationServices;
        private IMiddleContentGroupServices _middleContentGroupServices;
        public ContentsModel(ISiteMapService siteMapService, IContentGroupServices contentGroupServices
            , IContentGroupTranslationSrevices contentGroupTranslationSrevices, IContentServices contentServices
            , IContentTranslationServices contentTranslationServices, IGalleryTranslationServices galleryTranslationServices
            , IMiddleContentGroupServices middleContentGroupServices, IWebHostEnvironment environment, IMapper mapper)
        {
            _siteMapSerivce = siteMapService;
            sm = new SiteMap();
            _mapper = mapper;
            _hostEnvironment = environment;
            _contentGroupServices = contentGroupServices;
            _contentGroupTranslationSrevices = contentGroupTranslationSrevices;
            _contentServices = contentServices;
            _contentTranslationServices = contentTranslationServices;
            _galleryTranslationServices = galleryTranslationServices;
            _middleContentGroupServices = middleContentGroupServices;
        }
        #region Treeview
        public async Task<IActionResult> OnGetCreateTreeView(int id, string lang)
        {
            id = 1;
            return Content(JsonConvert.SerializeObject(await ViewInTreeView(id, lang)));
        }

        public async Task<List<Node>> ViewInTreeView(int CGId, string lang)//گرفتن گروه مطالب بصورت جیسون و فرستادن به تابع ساخت تری ویو
        {
            List<Node> TNodes = new List<Node>();
            List<Node> TNodes2 = new List<Node>();

            string LangId = "";
            if (lang != null)
            {
                LangId = lang;
            }
            else
            {
                LangId = CultureInfo.CurrentCulture.Name;
            }//(string.IsNullOrEmpty(lang.Trim()) ? CultureInfo.CurrentCulture.Name : lang.Trim());
            var contentGroupData = await _contentGroupTranslationSrevices.GetWithIncludeAsync
                (
                selector: x => x,
                where: x => x.ContentGroup.ContentGroup_ID == CGId && x.Language_Id == LangId,
                include: x => x.Include(s => s.ContentGroup)
                );

            var groups = await _contentGroupTranslationSrevices.GetWithIncludeAsync
                (
                selector: x => x,
                where: x => x.ContentGroup.ParentId == CGId && x.Language_Id == LangId,
                include: x => x.Include(s => s.ContentGroup)
                );
            foreach (var item in groups)
            {
                var node = await ViewInTreeView(item.ContentGroup_Id, item.Language_Id);
                TNodes2.Add(node.FirstOrDefault());
            }


            TNodes.Add(new Node()
            {
                text = " " + contentGroupData.FirstOrDefault().Title,
                href = "#" + contentGroupData.FirstOrDefault().ContentGroupTranslation_ID,
                tags = CGId.ToString(),
                nodes = TNodes2
            });

            return TNodes;
        }
        #endregion

        #region ContentGroup
        public IActionResult OnGetCreateGroup(ContentGroup contentGroup)
        {
            return Partial("_CreateGroup", contentGroup);
        }
        public async Task<IActionResult> OnPostCreateGroup(ContentGroup contentGroup)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ParentId = contentGroup.ParentId.Value;
                    await _contentGroupServices.Insert(contentGroup);
                    await _contentGroupServices.Save();
                    return new JsonResult(new { status = "ok", groupId = contentGroup.ContentGroup_ID });
                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
        }
        public IActionResult OnGetCreateContentGroupTrans(int id)
        {
            TempData["ContentGroupId"] = id;
            return Partial("_CreateContentGroupTrans");
        }
        public async Task<IActionResult> OnPostCreateContentGroupTrans(ContentGroupTranslation contentGroupTranslation)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (!await _contentGroupTranslationSrevices
                        .ExistsAsync(x => x.Language_Id == contentGroupTranslation.Language_Id
                        && x.ContentGroup_Id == contentGroupTranslation.ContentGroup_Id))
                    {
                        //sitemap
                        contentGroupTranslation.ParentId = ParentId;
                        contentGroupTranslation.SiteMap_Id = await _siteMapSerivce.AddContentGroup(contentGroupTranslation);

                        await _contentGroupTranslationSrevices.Insert(contentGroupTranslation);
                        await _contentGroupTranslationSrevices.Save();
                        return new JsonResult(new { status = "ok", group_id = contentGroupTranslation.ContentGroup_Id });
                    }
                    return new JsonResult("exsist");

                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }

        }

        #region OnGetViewDetailsContentGroup
        public async Task<IActionResult> OnGetViewDetailsContentGroup(int groupId)
        {
            try
            {
                var contentGroups = await _contentGroupTranslationSrevices.GetWithIncludeAsync<ContentGroupTranslation>
                (
                selector: x => x,
                include: s => s.Include(x => x.ContentGroup),
                where: x => x.ContentGroup.ContentGroup_ID == groupId && x.Language_Id == CultureInfo.CurrentCulture.Name
                );

                return Partial("_ViewDetailsContentGroup", contentGroups.FirstOrDefault());
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        public async Task<IActionResult> OnGetEditContentGroup(int id)
        {
            try
            {
                var contentGroup = await _contentGroupServices.GetAsync(x => x.ContentGroup_ID == (byte)id);
                return Partial("_EditContentGroup", contentGroup);
            }
            catch (Exception err)
            {
                return new JsonResult(new
                {
                    status = "error",
                    data = err.Message + " *** " + err.InnerException
                });
            }
        }
        public async Task<IActionResult> OnGetEditContentGroupTrans(int id)
        {
            try
            {
                var contentGroupTranslation = await _contentGroupTranslationSrevices.GetAsync(x => x.ContentGroupTranslation_ID == id);

                return Partial("_EditContentGroupTrans", contentGroupTranslation);
            }
            catch (Exception err)
            {
                return new JsonResult(new
                {
                    status = "error",
                    data = err.Message + " *** " + err.InnerException
                });
            }
        }
        public async Task<IActionResult> OnGetContentGroupTransInfo(string id, int contentGroupId)
        {

            string languageId = string.IsNullOrEmpty(id) ? CultureInfo.CurrentCulture.Name : id.Trim();
            var contentGroupTrans = await _contentGroupTranslationSrevices
                .GetAsync(x => x.Language_Id == languageId && x.ContentGroup_Id == contentGroupId);
            if (contentGroupTrans != null)
            {
                return new JsonResult(new
                {
                    status = "ok",
                    title = contentGroupTrans.Title,
                    description = contentGroupTrans.Description,
                    slug = contentGroupTrans.Slug,
                    id = contentGroupTrans.ContentGroupTranslation_ID
                });
            }

            return new JsonResult("nok");
        }

        public async Task<IActionResult> OnPostEditContentGroup(ContentGroup contentGroup)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ParentIdEdit = contentGroup.ParentId.Value;
                    await _contentGroupServices.Update(contentGroup);
                    await _contentGroupServices.Save();
                    return new JsonResult("ok");
                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new
                {
                    status = "error",
                    data = err.Message + " *** " + err.InnerException
                });
            }
        }
        public async Task<IActionResult> OnPostEditContentGroupTrans(ContentGroupTranslation contentGroupTranslation)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    //sitemap
                    await _siteMapSerivce.EditContentGroup(contentGroupTranslation);

                    await _contentGroupTranslationSrevices.Update(contentGroupTranslation);
                    await _contentGroupTranslationSrevices.Save();
                    return new JsonResult("ok");
                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new
                {
                    status = "error",
                    data = err.Message + " *** " + err.InnerException
                });
            }
        }

        public async Task<IActionResult> OnGetDeleteContentGroup(int id, int contentGroupId, string status)
        {
            try
            {
                if (status == "one")
                {
                    ///TODO : IsExsits In Content
                    ///
                    if (!await _middleContentGroupServices
                        .ExistsAsync(x => x.ContentGroupTranslation_Id == id))
                    {
                        var contentGroupTrans = await _contentGroupTranslationSrevices.GetAsync(x => x.ContentGroupTranslation_ID == id);

                        var contentGroupTransList = await _contentGroupTranslationSrevices.GetAllAsyn(x => x.ContentGroup_Id == contentGroupId);

                        if (contentGroupTransList.Count() > 1)
                        {
                            //sitemap and contentGroup
                            await _siteMapSerivce.DeleteContentGroup(contentGroupTrans);
                            return new JsonResult("ok");
                        }
                        else
                        {
                            //sitemap and contentGroup
                            await _siteMapSerivce.DeleteContentGroup(contentGroupTrans);

                            //Delete ContentGroup
                            var contentGroup = await _contentGroupServices.GetAsync(x => x.ContentGroup_ID == contentGroupId);
                            _contentGroupServices.DeleteAsync(contentGroup);
                            await _contentGroupServices.Save();
                            return new JsonResult("ok");
                        }
                    }
                    return new JsonResult("exists");

                }
                else if (status == "all")
                {

                }
            }
            catch (Exception err)
            {
                return new JsonResult(new
                {
                    status = "error",
                    data = err.Message + " *** " + err.InnerException
                });
            }
            return new JsonResult("nok");
        }
        #endregion

        #region Content
        public async Task<IActionResult> OnGetCreateContent()
        {
            var galleries = await _galleryTranslationServices.GetAll(x => x.Language_Id == CultureInfo.CurrentCulture.Name);
            TempData["Galleries"] = galleries.ToList();
            return Partial("_CreateContent");
        }
        public async Task<IActionResult> OnPostCreateContent(Content content, IFormFile imgContent)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (imgContent != null)
                    {
                        string targetPath = Path.Combine(_hostEnvironment.WebRootPath, ContentPicFilePath);
                        string pathThumb = Path.Combine(_hostEnvironment.WebRootPath, ContentThumbPicFilePath);

                        FileManipulate.CreateDirectory(targetPath, _hostEnvironment.WebRootPath, pathThumb);
                        Tuple<string, string> ImagePath = await FileManipulate.CreateFile(_hostEnvironment.WebRootPath, imgContent, ContentPicFilePath);
                        ///For Combine :ImageName and ImageFullPath
                        Tuple<string, string, string> tuple = FileManipulate.Combine(_hostEnvironment.WebRootPath, ImagePath.Item2, targetPath, pathThumb);
                        FileManipulate.resizeImage2(tuple.Item1, tuple.Item2);
                        content.Image = Path.Combine(ContentPicFilePath, ImagePath.Item2);
                        content.SmallImage = Path.Combine(ContentThumbPicFilePath, ImagePath.Item2);
                    }
                    //ParentId = contentGroup.ParentId.Value;
                    content.CreateDate = DateTime.Now;
                    content.ReleaseDate = DateTime.Now;
                    await _contentServices.Insert(content);
                    await _contentServices.Save();
                    return new JsonResult(new { status = "ok", contentId = content.Content_ID });
                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
        }
        public IActionResult OnGetCreateContentTrans(int id)
        {
            TempData["ContentId"] = id;
            return Partial("_CreateContentTrans");
        }
        public async Task<IActionResult> OnPostCreateContentTrans(ContentTranslation contentTranslation, List<string> selectedGroupId)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    contentTranslation.selectedGroupIds = selectedGroupId.Select(short.Parse).ToList();
                    for (int i = 0; i < contentTranslation.selectedGroupIds.Count; i++)
                    {
                        ContentGroupTranslationIdSelected = await
                            GetAllParents(contentTranslation.selectedGroupIds[i], contentTranslation.Language_Id);
                    }
                    contentTranslation.selectedGroupIds = ContentGroupTranslationIdSelected.Distinct().ToList();
                    ////sitemap
                    ///
                    if (!await _contentTranslationServices
                       .ExistsAsync(x => x.Language_Id == contentTranslation.Language_Id
                       && x.Content_Id == contentTranslation.Content_Id))
                    {
                        await _contentTranslationServices.Insert(contentTranslation);
                        await _contentTranslationServices.Save();
                    }
                    else
                        return new JsonResult("exists");

                    List<int> siteMapIds = await _siteMapSerivce.AddContent(contentTranslation);
                    for (int i = 0; i < siteMapIds.Count; i++)
                    {
                        ContentMiddleGroup middleContentGroupServices = new()
                        {
                            ID = Guid.NewGuid().ToString(),
                            ContentGroupTranslation_Id = (byte)contentTranslation.selectedGroupIds[i],
                            ContentTranslation_Id = contentTranslation.ContentTranslation_ID,
                            SiteMap_Id = siteMapIds[i]
                        };
                        await _middleContentGroupServices.Insert(middleContentGroupServices);
                        await _middleContentGroupServices.Save();
                    }
                    return new JsonResult(new { status = "ok", contentTrans_id = contentTranslation.ContentTranslation_ID, content_id = contentTranslation.Content_Id });
                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }

        }
        public async Task<ContentGroup> GetCurrentContentGroup(Int16 groupId)
        {
            var contentGroup = await _contentGroupServices.GetAsync(x => x.ContentGroup_ID == groupId);
            return contentGroup;
        }

        #region ViewAllContents
        public async Task<IActionResult> OnPostViewAllContents(int contentGroupTranslationId)
        {
            try
            {
                var draw = Request.Form["draw"].FirstOrDefault();
                var start = Request.Form["start"].FirstOrDefault();
                var length = Request.Form["length"].FirstOrDefault();
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
                var searchValue = Request.Form["search[value]"].FirstOrDefault();
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                IEnumerable<ContentMiddleGroup> contents = null;
                if (contentGroupTranslationId != 0)
                    contents = await _middleContentGroupServices.GetWithIncludeAsync<ContentMiddleGroup>
                       (
                       selector: x => x,
                       include: s => s.Include(x => x.ContentTranslation).ThenInclude(x => x.Content),
                       where: x => x.ContentGroupTranslation_Id == contentGroupTranslationId && x.ContentTranslation.Language_Id == CultureInfo.CurrentCulture.Name
                       );
                else
                    contents = await _middleContentGroupServices.GetWithIncludeAsync<ContentMiddleGroup>
                           (
                           selector: x => x,
                           include: s => s.Include(x => x.ContentTranslation).ThenInclude(x => x.Content),
                           where: x => x.ContentTranslation.Language_Id == CultureInfo.CurrentCulture.Name
                           );

                contents = contents.OrderByDescending(x => x.ContentTranslation.ContentTranslation_ID);
                if (contentGroupTranslationId == 0)
                    ///برای حذف آیتم های تکراری == Distinct
                    contents = contents.GroupBy(x => x.ContentTranslation_Id).Select(x => x.First());

                if (!(string.IsNullOrEmpty(sortColumn)))
                {
                    contents = contents.AsQueryable().OrderBy(sortColumn + " " + sortColumnDirection);
                }
                if (!string.IsNullOrEmpty(searchValue))
                {
                    contents = contents.Where(m => m.ContentTranslation.Title.Contains(searchValue)
                    || m.ContentTranslation.Content.CreateDate.ToString().Contains(searchValue)
                    );
                        
                }
                recordsTotal = contents.Count();
                var data = contents.Skip(skip).Take(pageSize).ToList();
                var jsonData = new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data };
                return new JsonResult(jsonData);
            }
            catch (Exception err)
            {
                throw;
            }
        }
        #endregion

        public async Task<IActionResult> OnGetEditContent(int id)
        {
            try
            {
                var content = await _contentServices.GetAsync(x => x.Content_ID == id);

                var galleries = await _galleryTranslationServices.GetAll(x => x.Language_Id == CultureInfo.CurrentCulture.Name);
                TempData["Galleries"] = galleries.ToList();


                return Partial("_EditContent", content);
            }
            catch (Exception err)
            {
                return new JsonResult(new
                {
                    status = "error",
                    data = err.Message + " *** " + err.InnerException
                });
            }
        }
        public async Task<IActionResult> OnGetEditContentTrans(int id)
        {
            var contentTrans = await _contentTranslationServices.GetAsync(x => x.ContentTranslation_ID == id);
            var middleContentGroups = await _middleContentGroupServices.GetWithIncludeAsync
                (
                selector: x => x,
                where: x => x.ContentTranslation_Id == id,
                include: s => s.Include(x => x.ContentGroupTranslation));
            TempData["ContentGroups"] = middleContentGroups;
            foreach (var middleGroup in middleContentGroups)
            {
                SelectedGroupIds.Add(middleGroup.ContentGroupTranslation_Id);
            }
            return Partial("_EditContentTrans", contentTrans);
        }
        public async Task<IActionResult> OnGetContentInfo(string id, int contentId)
        {

            string languageId = string.IsNullOrEmpty(id) ? CultureInfo.CurrentCulture.Name : id.Trim();
            var contentTrans = await _contentTranslationServices
                .GetWithIncludeAsync(
                selector:x=>x,
                where:x => x.Language_Id == languageId && x.Content_Id == contentId,
                include:s=>s.Include(x=>x.ContentMiddleGroups).ThenInclude(x=>x.ContentGroupTranslation)
                );
            if (contentTrans != null)
            {
                return new JsonResult(new
                {
                    status = "ok",
                    //middleGroup=contentTrans.FirstOrDefault().ContentMiddleGroups,
                    data = contentTrans.FirstOrDefault()
                });
            }

            return new JsonResult("nok");
        }

        public async Task<IActionResult> OnPostEditContent(Content content, IFormFile imgContent)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (imgContent != null)
                    {
                        string targetPath = Path.Combine(_hostEnvironment.WebRootPath, ContentPicFilePath);
                        string pathThumb = Path.Combine(_hostEnvironment.WebRootPath, ContentThumbPicFilePath);

                        FileManipulate.CreateDirectory(targetPath, pathThumb);
                        FileManipulate.DeleteFile(_hostEnvironment.WebRootPath, "", content.SmallImage);
                        FileManipulate.DeleteFile(_hostEnvironment.WebRootPath, "", content.Image);


                        Tuple<string, string> ImagePath = await FileManipulate.CreateFile(_hostEnvironment.WebRootPath, imgContent, ContentPicFilePath);
                        ///For Combine :ImageName and ImageFullPath
                        Tuple<string, string, string> tuple = FileManipulate.Combine(_hostEnvironment.WebRootPath, ImagePath.Item2, targetPath, pathThumb);
                        FileManipulate.resizeImage2(tuple.Item1, tuple.Item2);
                        content.Image = Path.Combine(ContentPicFilePath, ImagePath.Item2);
                        content.SmallImage = Path.Combine(ContentThumbPicFilePath, ImagePath.Item2);
                    }
                    content.ReleaseDate = DateTime.Now;
                    await _contentServices.Update(content);
                    await _contentServices.Save();
                    return new JsonResult(new { status = "ok", contentId = content.Content_ID });
                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
        }
        public async Task<IActionResult> OnPostEditContentTrans(ContentTranslation contentTranslation, List<string> selectedGroupId)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (selectedGroupId.Count > 0)
                    {
                        contentTranslation.selectedGroupIds = selectedGroupId.Select(short.Parse).ToList();
                        for (int i = 0; i < contentTranslation.selectedGroupIds.Count; i++)
                        {
                            ContentGroupTranslationIdSelected = await
                                GetAllParents(contentTranslation.selectedGroupIds[i], contentTranslation.Language_Id);
                        }
                        contentTranslation.selectedGroupIds = ContentGroupTranslationIdSelected.Distinct().ToList();
                    }
                    else
                    {
                        contentTranslation.selectedGroupIds = SelectedGroupIds;
                    }
                    var middleContentGroups = await _middleContentGroupServices.GetAllAsyn(x => x.ContentTranslation_Id == contentTranslation.ContentTranslation_ID);


                    await _siteMapSerivce.DeleteContent(contentTranslation);

                    _middleContentGroupServices.DeleteAll(middleContentGroups);
                    await _middleContentGroupServices.Save();
                    ////sitemap
                    ///

                    List<int> siteMapIds = await _siteMapSerivce.AddContent(contentTranslation);
                    for (int i = 0; i < siteMapIds.Count; i++)
                    {
                        if (!await _middleContentGroupServices.ExistsAsync
                            (x => x.ContentTranslation_Id == contentTranslation.ContentTranslation_ID && x.SiteMap_Id == siteMapIds[i]))
                        {
                            ContentMiddleGroup middleContentGroupServices = new()
                            {
                                ID = Guid.NewGuid().ToString(),
                                ContentGroupTranslation_Id = (byte)contentTranslation.selectedGroupIds[i],
                                ContentTranslation_Id = contentTranslation.ContentTranslation_ID,
                                SiteMap_Id = siteMapIds[i]
                            };
                            await _middleContentGroupServices.Insert(middleContentGroupServices);
                            await _middleContentGroupServices.Save();
                        }
                    }
                    await _contentTranslationServices.Update(contentTranslation);
                    await _contentTranslationServices.Save();

                    return new JsonResult(new { status = "ok", contentTrans_id = contentTranslation.ContentTranslation_ID, content_id = contentTranslation.Content_Id });
                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
        }

        public async Task<IActionResult> OnGetDeleteContent(int id, int contentId, string status)
        {
            try
            {
                if (status == "one")
                {
                    var middleContentGroup = await _middleContentGroupServices
                        .GetWithIncludeAsync<ContentMiddleGroup>
                        (
                        selector: x => x,
                        where: x => x.ContentTranslation_Id == id,
                        include: s => s.Include(x => x.ContentGroupTranslation).Include(x => x.ContentTranslation)
                        );

                    var contentTransList = await _contentTranslationServices.GetWithIncludeAsync
                        (selector: x => x,
                        where: x => x.Content_Id == contentId);
                    if (contentTransList.Count() > 1)
                    {
                        //var contentTrans = await _contentTranslationServices.GetAsync(x => x.ContentTranslation_ID == id);

                        await _siteMapSerivce.DeleteContent(middleContentGroup.FirstOrDefault().ContentTranslation);

                        _contentTranslationServices.DeleteAsync(middleContentGroup.FirstOrDefault().ContentTranslation);
                        await _contentTranslationServices.Save();
                    }
                    else
                    {
                        var contentTrans = await _contentTranslationServices.GetAsync(x => x.ContentTranslation_ID == id);

                        await _siteMapSerivce.DeleteContent(contentTrans);

                        _contentTranslationServices.DeleteAsync(contentTrans);
                        await _contentTranslationServices.Save();

                        var content = await _contentServices.GetAsync(x => x.Content_ID == contentId);

                        FileManipulate.DeleteFile(_hostEnvironment.WebRootPath, "", content.Image);
                        FileManipulate.DeleteFile(_hostEnvironment.WebRootPath, "", content.SmallImage);
                        _contentServices.DeleteAsync(content);
                        await _contentServices.Save();
                    }
                    return new JsonResult("ok");
                }
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
            return new JsonResult("nok");
        }

        public async Task<IActionResult> OnGetContentStatistics(int id)
        {
            var content = await _contentTranslationServices.GetAsync(x => x.ContentTranslation_ID == id);
            if (content != null)
            {

                ContentStatisticsVM contentStatisticsVM = _mapper.Map<ContentStatisticsVM>(content);
                return Partial("_ContentStatistics", contentStatisticsVM);
                //return new JsonResult(new
                //{
                //    visitCount = content.VisitCount,
                //    likeCount = content.LikeCount,
                //    votes = content.Votes,
                //    sumVotes = content.SumVotes,
                //    avgVotes = content.AvgVotes
                //});
            }
            return new JsonResult("nok");
        }
        #endregion


        public async Task<List<short>> GetAllParents(short CGId, string lang)
        {
            string LangId = "";
            if (lang != null)
                LangId = lang;
            else
                LangId = CultureInfo.CurrentCulture.Name;

            var groups = await _contentGroupTranslationSrevices.GetWithIncludeAsync
                  (
                  selector: x => x,
                  where: x => x.ContentGroup_Id == CGId && x.Language_Id == LangId,
                  include: x => x.Include(s => s.ContentGroup)
                  );
            foreach (var item in groups)
            {
                if (item.ContentGroup.ParentId != null)
                {
                    //if (Ids.IndexOf(item.ContentGroupTranslation_ID) == -1) { }
                    Ids.Add(item.ContentGroupTranslation_ID);
                    await GetAllParents(item.ContentGroup.ParentId.Value, item.Language_Id);
                }
                else
                {
                }
            }
            return Ids;
        }

    }
}
