﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

using Application.Interfaces;

using Domain.DomainClasses.SendType;
using System.Linq.Dynamic.Core;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc.Rendering;
using Application.Utilities;

namespace ItisaCMS_RazorPage.Areas.Admin.Pages.SendTypes
{
    public class SendTypesModel : PageModel
    {
        public static Dictionary<int, string> SendTypeDic = new Dictionary<int, string>();

        private ISendTypeServices _sendTypeServices;
        private ISendTypeTranslationServices _sendTypeTranslationServices;
        public SendTypesModel(ISendTypeServices sendTypeServices, ISendTypeTranslationServices sendTypeTranslationServices)
        {

            _sendTypeServices = sendTypeServices;
            _sendTypeTranslationServices = sendTypeTranslationServices;
        }

        #region SendType
        public IActionResult OnGetCreateSendType()
        {
            try
            {
                var dic = Dictionaries.GetSendTypeDic();
                TempData["SendType_Types"] = dic;

                return Partial("_CreateSendType");
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
        }
        public async Task<IActionResult> OnPostCreateSendType(SendType sendType)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (!await _sendTypeServices.ExistsAsync(x => x.Type == sendType.Type))
                    {
                        await _sendTypeServices.Insert(sendType);
                        await _sendTypeServices.Save();
                        return new JsonResult(new { status = "ok", sendTypeId = sendType.SendType_ID });
                    }
                    return new JsonResult("exists");
                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
        }
        public IActionResult OnGetCreateSendTypeTrans(int id)
        {
            TempData["SendTypeId"] = id;
            return Partial("_CreateSendTypeTrans");
        }
        public async Task<IActionResult> OnPostCreateSendTypeTrans(SendTypeTranslation sendTypeTranslation)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (!await _sendTypeTranslationServices
                        .ExistsAsync(x => x.Language_Id == sendTypeTranslation.Language_Id && x.SendType_Id == sendTypeTranslation.SendType_Id))
                    {
                        await _sendTypeTranslationServices.Insert(sendTypeTranslation);
                        await _sendTypeTranslationServices.Save();
                        return new JsonResult("ok");
                    }
                    return new JsonResult("exsist");
                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
        }

        #region ViewAllSendTypes
        public async Task<IActionResult> OnPostViewAllSendTypes()
        {
            try
            {
                var draw = Request.Form["draw"].FirstOrDefault();
                var start = Request.Form["start"].FirstOrDefault();
                var length = Request.Form["length"].FirstOrDefault();
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
                var searchValue = Request.Form["search[value]"].FirstOrDefault();
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                var sendTypes = await _sendTypeTranslationServices.GetWithIncludeAsync<SendTypeTranslation>
                (
                selector: x => x,
                include: s => s.Include(x => x.SendType),
                where: x => x.Language_Id == CultureInfo.CurrentCulture.Name
                );
                if (!(string.IsNullOrEmpty(sortColumn)))
                {
                    sendTypes = sendTypes.AsQueryable().OrderBy(sortColumn + " " + sortColumnDirection);
                }
                if (!string.IsNullOrEmpty(searchValue))
                {
                    sendTypes = sendTypes.Where(m => m.Title.Contains(searchValue) || m.Price.ToString().Contains(searchValue) ||
                    m.SendType.Type.Contains(searchValue));

                }
                recordsTotal = sendTypes.Count();
                var data = sendTypes.Skip(skip).Take(pageSize).ToList();
                var jsonData = new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data };
                return new JsonResult(jsonData);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        public async Task<IActionResult> OnGetEditSendType(int id)
        {
            try
            {
                var dic = Dictionaries.GetSendTypeDic();
                TempData["SendType_Types"] = dic;

                var sendType = await _sendTypeServices.GetAsync(x => x.SendType_ID == (byte)id);

                return Partial("_EditSendType", sendType);
            }
            catch (Exception err)
            {
                return new JsonResult(new
                {
                    status = "error",
                    data = err.Message + " *** " + err.InnerException
                });
            }
        }
        public async Task<IActionResult> OnGetEditSendTypeTrans(int id)
        {
            var sendTypeTrans = await _sendTypeTranslationServices.GetAsync(x => x.SendTypeTranslation_ID == id);

            return Partial("_EditSendTypeTrans", sendTypeTrans);
        }
        public async Task<IActionResult> OnGetSendTypeInfo(string id, int sendTypeId)
        {

            string languageId = string.IsNullOrEmpty(id) ? CultureInfo.CurrentCulture.Name : id.Trim();
            var sendTypeTrans = await _sendTypeTranslationServices
                .GetAsync(x => x.Language_Id == languageId && x.SendType_Id == sendTypeId);
            if (sendTypeTrans != null)
            {
                return new JsonResult(new
                {
                    status = "ok",
                    title = sendTypeTrans.Title,
                    description = sendTypeTrans.Description,
                    price = sendTypeTrans.Price,
                    id = sendTypeTrans.SendTypeTranslation_ID
                });
            }

            return new JsonResult("nok");
        }
        public async Task<IActionResult> OnPostEditSendType(SendType sendType)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    await _sendTypeServices.Update(sendType);
                    await _sendTypeServices.Save();
                    return new JsonResult("ok");
                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new
                {
                    status = "error",
                    data = err.Message + " *** " + err.InnerException
                });
            }
        }
        public async Task<IActionResult> OnPostEditSendTypeTrans(SendTypeTranslation sendTypeTranslation)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    await _sendTypeTranslationServices.Update(sendTypeTranslation);
                    await _sendTypeTranslationServices.Save();
                    return new JsonResult("ok");
                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new
                {
                    status = "error",
                    data = err.Message + " *** " + err.InnerException
                });
            }
        }

        public async Task<IActionResult> OnGetDeleteSendTypeTrans(int id, int sendTypeId, string status)
        {
            try
            {
                var sendTypeTransList = await _sendTypeTranslationServices.GetWithIncludeAsync
                        (selector: x => x,
                        where: x => x.SendType_Id == sendTypeId);
                var sendTypeTrans = await _sendTypeTranslationServices.GetAsync(x => x.SendTypeTranslation_ID == id);

                if (status == "one")
                {

                    if (sendTypeTransList.Count() > 1)
                    {

                        _sendTypeTranslationServices.Delete(sendTypeTrans);
                        await _sendTypeTranslationServices.Save();
                    }
                    else
                    {
                        _sendTypeTranslationServices.Delete(sendTypeTrans);
                        await _sendTypeTranslationServices.Save();

                        var sendType = await _sendTypeServices.GetAsync(x => x.SendType_ID == sendTypeId);
                        _sendTypeServices.DeleteAsync(sendType);
                        await _sendTypeServices.Save();
                    }
                    return new JsonResult("ok");
                }
                else if (status == "all")
                {

                    _sendTypeTranslationServices.DeleteAll(sendTypeTransList);
                    await _sendTypeTranslationServices.Save();

                    var sendType = await _sendTypeServices.GetAsync(x => x.SendType_ID == sendTypeId);
                    _sendTypeServices.DeleteAsync(sendType);
                    await _sendTypeServices.Save();

                    return new JsonResult("ok");
                }
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
            return new JsonResult("nok");
        }
        #endregion
    }
}
