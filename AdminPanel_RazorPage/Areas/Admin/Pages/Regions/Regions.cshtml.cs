using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

using Application.Interfaces;
using System.Linq.Dynamic.Core;
using Domain.DomainClasses.Region;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Application.ViewModels;

namespace ItisaCMS_RazorPage.Areas.Admin.Pages.Regions
{
    public class RegionsModel : PageModel
    {
        private ICountryServices _countryServices;
        private IStateServices _stateServices;
        private ICityServices _cityServices;

        public RegionsModel(ICountryServices countryServices, ICityServices cityServices, IStateServices stateServices)
        {
            _countryServices = countryServices;
            _stateServices = stateServices;
            _cityServices = cityServices;
        }
        #region Country
        public IActionResult OnGetIndex()
        {
            return Partial("Index");
        }
        public IActionResult OnGetCreateCountry()
        {
            return Partial("_CreateCountry");
        }
       
        public IActionResult OnPostPostTest([FromBody] List<TestCountry> detalle)
        {
            if (detalle.Any(x => TryValidateModel(detalle)))
            {
                return new JsonResult("ok");
            }
            return new JsonResult("ok");
        }
        public async Task<IActionResult> OnPostCreateCountry(Country country)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    await _countryServices.Insert(country);
                    await _countryServices.Save();
                    return new JsonResult("ok");
                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", msg = err.Message });
            }
        }

        #region ViewAllCountries
        public async Task<IActionResult> OnPostViewAllCountries()
        {
            try
            {
                var draw = Request.Form["draw"].FirstOrDefault();
                var start = Request.Form["start"].FirstOrDefault();
                var length = Request.Form["length"].FirstOrDefault();
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
                var searchValue = Request.Form["search[value]"].FirstOrDefault();
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                var countries = await _countryServices.GetAllAsyn();
                if (!(string.IsNullOrEmpty(sortColumn)))
                {
                    countries = countries.AsQueryable().OrderBy(sortColumn + " " + sortColumnDirection);
                }
                if (!string.IsNullOrEmpty(searchValue))
                {
                    countries = countries.Where(m => m.Name.Contains(searchValue) || m.Code.Contains(searchValue));

                }
                recordsTotal = countries.Count();
                var data = countries.Skip(skip).Take(pageSize).ToList();
                var jsonData = new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data };
                return new JsonResult(jsonData);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        public async Task<IActionResult> OnGetEditCountry(int id)
        {
            var country = await _countryServices.GetAsync(x => x.Country_ID == id);
            return Partial("_EditCountry", country);
        }
        public async Task<IActionResult> OnPostEditCountry(Country country)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    await _countryServices.Update(country);
                    await _countryServices.Save();
                    return new JsonResult("ok");
                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", msg = err.Message });
            }
        }
        public async Task<IActionResult> OnGetDeleteCountry(int id)
        {
            try
            {
                var country = await _countryServices.GetAsync(x => x.Country_ID == id);
                if (country != null)
                {
                    if (!await _stateServices.ExistsAsync(x => x.CountryId == id))
                    {
                        _countryServices.Delete(country);
                        await _countryServices.Save();
                        return new JsonResult("ok");
                    }
                    return new JsonResult("exists");
                }
                return new JsonResult("nok");
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
        }

        public async Task<IActionResult> OnGetCountiesByLang()
        {
            var countries = await _countryServices.GetAllAsyn();
            return new JsonResult(countries);
        }

        public async Task<IActionResult> OnGetCountiesByLangForCity()
        {
            var countries = await _countryServices.GetAllAsyn(x=>x.IsActive);
            return new JsonResult(countries);
        }
        #endregion

        #region State
        public async Task<IActionResult> OnGetCreateState()
        {
            var countries = await _countryServices.GetAllAsyn(x => x.IsActive);
            TempData["Countries"] = countries;
            return Partial("_CreateState");
        }
        public async Task<IActionResult> OnPostCreateState(State state)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (!await _stateServices.ExistsAsync(x => x.Name == state.Name))
                    {
                        await _stateServices.Insert(state);
                        await _stateServices.Save();
                        return new JsonResult("ok");
                    }
                    return new JsonResult("exist");
                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", msg = err.Message });
            }
        }
        #region ViewAllStates
        public async Task<IActionResult> OnPostViewAllStates(int countryId)
        {
            try
            {
                var draw = Request.Form["draw"].FirstOrDefault();
                var start = Request.Form["start"].FirstOrDefault();
                var length = Request.Form["length"].FirstOrDefault();
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
                var searchValue = Request.Form["search[value]"].FirstOrDefault();
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                IEnumerable<State> states = null;
                if (countryId != 0)
                    states = await _stateServices.GetAllAsyn(x => x.CountryId == countryId);
                else
                    states = await _stateServices.GetAllAsyn();

                if (!(string.IsNullOrEmpty(sortColumn)))
                {
                    states = states.AsQueryable().OrderBy(sortColumn + " " + sortColumnDirection);
                }
                if (!string.IsNullOrEmpty(searchValue))
                {
                    states = states.Where(m => m.Name.Contains(searchValue) || m.Code.Contains(searchValue));

                }
                recordsTotal = states.Count();
                var data = states.Skip(skip).Take(pageSize).ToList();
                var jsonData = new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data };
                return new JsonResult(jsonData);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        public async Task<IActionResult> OnGetEditState(int id)
        {
            var countries = await _countryServices.GetAllAsyn(x => x.IsActive);
            TempData["Countries"] = countries;

            var state = await _stateServices.GetAsync(x => x.State_ID == id);
            return Partial("_EditState", state);
        }
        public async Task<IActionResult> OnPostEditState(State state)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    await _stateServices.Update(state);
                    await _stateServices.Save();
                    return new JsonResult("ok");
                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", msg = err.Message });
            }
        }
        public async Task<IActionResult> OnGetDeleteState(int id)
        {
            try
            {
                var state = await _stateServices.GetAsync(x => x.State_ID == id);
                if (state != null)
                {
                    if (!await _cityServices.ExistsAsync(x => x.State_Id == id))
                    {
                        _stateServices.Delete(state);
                        await _stateServices.Save();
                        return new JsonResult("ok");
                    }
                    return new JsonResult("exists");
                }
                return new JsonResult("nok");
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
        }

        public async Task<IActionResult> OnGetStatesByCountryId(int countryId)
        {
            IEnumerable<State> states = null;
            if (countryId != 0) 
              states = await _stateServices.GetAllAsyn(x=>x.CountryId==countryId);
            else
                states= await _stateServices.GetAllAsyn();

            return new JsonResult(states);
        }
        #endregion

        #region City
        public IActionResult OnGetCreateCity()
        {

            return Partial("_CreateCity");
        }
        public async Task<IActionResult> OnPostCreateCity(City city)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (!await _cityServices.ExistsAsync(x => x.Name == city.Name))
                    {
                        await _cityServices.Insert(city);
                        await _cityServices.Save();
                        return new JsonResult("ok");
                    }
                    return new JsonResult("exist");
                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", msg = err.Message });
            }
        }

        #region ViewAllCities
        public async Task<IActionResult> OnPostViewAllCities(int stateId)
        {
            try
            {
                var draw = Request.Form["draw"].FirstOrDefault();
                var start = Request.Form["start"].FirstOrDefault();
                var length = Request.Form["length"].FirstOrDefault();
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
                var searchValue = Request.Form["search[value]"].FirstOrDefault();
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                IEnumerable<City> cities = null;
                if (stateId != 0)
                    cities = await _cityServices.GetAllAsyn(x => x.State_Id == stateId);
                else
                    cities = await _cityServices.GetAllAsyn();

                if (!(string.IsNullOrEmpty(sortColumn)))
                {
                    cities = cities.AsQueryable().OrderBy(sortColumn + " " + sortColumnDirection);
                }
                if (!string.IsNullOrEmpty(searchValue))
                {
                    cities = cities.Where(m => m.Name.Contains(searchValue) || m.Code.Contains(searchValue));

                }
                recordsTotal = cities.Count();
                var data = cities.Skip(skip).Take(pageSize).ToList();
                var jsonData = new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data };
                return new JsonResult(jsonData);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        public async Task<IActionResult> OnGetEditCity(int id)
        {
            var city = await _cityServices.GetWithIncludeAsync(

                selector: x => x,
                where: x => x.City_ID == id,
                include: s => s.Include(x => x.State));
            return Partial("_EditCity", city.FirstOrDefault());
        }

        public async Task<IActionResult> OnPostEditCity(City city)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    await _cityServices.Update(city);
                    await _cityServices.Save();
                    return new JsonResult("ok");
                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", msg = err.Message });
            }
        }

        public async Task<IActionResult> OnGetDeleteCity(int id)
        {
            try
            {
                var city = await _cityServices.GetAsync(x => x.City_ID == id);
                if (city != null)
                {
                    _cityServices.Delete(city);
                    await _cityServices.Save();
                    return new JsonResult("ok");
                }
                return new JsonResult("nok");
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
        } 
        #endregion

    }
}
