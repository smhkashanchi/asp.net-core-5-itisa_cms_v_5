using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

using Application.Interfaces;

using Domain.DomainClasses.Connection;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;

namespace ItisaCMS_RazorPage.Areas.Admin.Pages.Connections
{
    public class ConnectionsModel : PageModel
    {
        private IConnectionGroupServices _connectionGroupServices;
        private IConnectionServices _connectionServices;
        private IConnectionTranslationServices _connectionTranslationServices;
        public ConnectionsModel(IConnectionGroupServices connectionGroupServices, IConnectionServices connectionServices
            , IConnectionTranslationServices connectionTranslationServices)
        {
            _connectionGroupServices = connectionGroupServices;
            _connectionServices = connectionServices;
            _connectionTranslationServices = connectionTranslationServices;
        }
        #region ConnectionGroup

        public IActionResult OnGetCreateGroup()
        {
            return Partial("_CreateGroup");
        }
        public async Task<IActionResult> OnPostCreateGroup(ConnectionGroup connectionGroup)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    await _connectionGroupServices.Insert(connectionGroup);
                    await _connectionGroupServices.Save();
                    return new JsonResult("ok");
                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
        }

        #region ViewAllGroups
        public async Task<IActionResult> OnPostViewAllGroups()
        {
            try
            {
                var draw = Request.Form["draw"].FirstOrDefault();
                var start = Request.Form["start"].FirstOrDefault();
                var length = Request.Form["length"].FirstOrDefault();
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
                var searchValue = Request.Form["search[value]"].FirstOrDefault();
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                var connectionGroups = await _connectionGroupServices.GetAllAsyn();
                if (!(string.IsNullOrEmpty(sortColumn)))
                {
                    connectionGroups = connectionGroups.AsQueryable().OrderBy(sortColumn + " " + sortColumnDirection);
                }
                if (!string.IsNullOrEmpty(searchValue))
                {
                    connectionGroups = connectionGroups.Where(m => m.Title.Contains(searchValue));

                }
                recordsTotal = connectionGroups.Count();
                var data = connectionGroups.Skip(skip).Take(pageSize).ToList();
                var jsonData = new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data };
                return new JsonResult(jsonData);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        public async Task<IActionResult> OnGetEditGroup(int id)
        {
            try
            {
                var connectionGroup = await _connectionGroupServices.GetAsync(x => x.Id == (byte)id);
                return Partial("_EditGroup", connectionGroup);
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
        }
        public async Task<IActionResult> OnPostEditGroup(ConnectionGroup connectionGroup)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _connectionGroupServices.Update(connectionGroup);
                    await _connectionGroupServices.Save();
                    return new JsonResult("ok");
                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
        }
        public async Task<IActionResult> OnGetDeleteGroup(int id)
        {
            try
            {
                var connectionGroup = await _connectionGroupServices.GetAsync(x => x.Id == (byte)id);
                if (connectionGroup != null)
                {
                    if (!await _connectionServices.ExistsAsync(x => x.GroupId == id))
                    {
                        _connectionGroupServices.DeleteAsync(connectionGroup);
                        await _connectionGroupServices.Save();
                        return new JsonResult("ok");
                    }
                    return new JsonResult("exists");
                }
                return new JsonResult("nok");
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
        }
        #endregion

        #region Connection
        public async Task<IActionResult> OnGetCreateConnection()
        {
            try
            {
                var connectionGroups = await _connectionGroupServices.GetAllAsyn(x => x.IsActive);
                TempData["ConnectionGroups"] = connectionGroups;
                var connectionTypes = EnumConnectionType.GetValues<ConnectionType>();
                TempData["ConnectionTypes"] = connectionTypes;
                return Partial("_CreateConnection");
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
        }
        public async Task<IActionResult> OnPostCreateConnection(Connection connection)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string connectionType = GetEnumValue(connection.Type);
                    connection.Type = connectionType;
                    await _connectionServices.InsertConnection(connection);
                    await _connectionServices.Save();
                    return new JsonResult(new { status = "ok", connectionId = connection.Id });
                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
        }
        public IActionResult OnGetCreateConnectionTrans(int id)
        {
            TempData["ConnectionId"] = id;
            return Partial("_CreateConnectionTrans");
        }
        public async Task<IActionResult> OnPostCreateConnectionTrans(ConnectionTranslation connectionTranslation)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (!await _connectionTranslationServices
                        .ExistsAsync(x => x.LanguageId == connectionTranslation.LanguageId && x.ConnectionId == connectionTranslation.ConnectionId))
                    {
                        await _connectionTranslationServices.Insert(connectionTranslation);
                        await _connectionTranslationServices.Save();
                        return new JsonResult("ok");
                    }
                    return new JsonResult("exsist");

                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
        }
        #region ViewAllConnections
        public async Task<IActionResult> OnPostViewAllConnections()
        {
            try
            {
                var draw = Request.Form["draw"].FirstOrDefault();
                var start = Request.Form["start"].FirstOrDefault();
                var length = Request.Form["length"].FirstOrDefault();
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
                var searchValue = Request.Form["search[value]"].FirstOrDefault();
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                var connections = await _connectionTranslationServices.GetWithIncludeAsync<ConnectionTranslation>
                (
                selector: x => x,
                include: s => s.Include(x => x.Connection).ThenInclude(x => x.ConnectionGroup),
                where: x => x.LanguageId == CultureInfo.CurrentCulture.Name
                );
                if (!(string.IsNullOrEmpty(sortColumn)))
                {
                    connections = connections.AsQueryable().OrderBy(sortColumn + " " + sortColumnDirection);
                }
                if (!string.IsNullOrEmpty(searchValue))
                {
                    connections = connections.Where(m => m.Name.Contains(searchValue) || m.Value.Contains(searchValue)
                        || m.Connection.Image.Contains(searchValue));

                }
                recordsTotal = connections.Count();
                var data = connections.Skip(skip).Take(pageSize).ToList();
                var jsonData = new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data };
                return new JsonResult(jsonData);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        public async Task<IActionResult> OnGetEditConnection(int id)
        {
            try
            {
                var connection = await _connectionServices.GetAsync(x => x.Id == (byte)id);

                var connectionGroups = await _connectionGroupServices.GetAllAsyn(x => x.IsActive);
                TempData["ConnectionGroups"] = connectionGroups;
                var connectionTypes = EnumConnectionType.GetValues<ConnectionType>();
                TempData["ConnectionTypes"] = connectionTypes;

                return Partial("_EditConnection", connection);
            }
            catch (Exception err)
            {
                return new JsonResult(new
                {
                    status = "error",
                    data = err.Message + " *** " + err.InnerException
                });
            }
        }
        public async Task<IActionResult> OnGetEditConnectionTrans(int id)
        {
            var connectionTrans = await _connectionTranslationServices.GetAsync(x => x.Id == id);

            return Partial("_EditConnectionTrans", connectionTrans);
        }
        public async Task<IActionResult> OnGetConnectionInfo(string id, int connectionId)
        {

            string languageId = string.IsNullOrEmpty(id) ? CultureInfo.CurrentCulture.Name : id.Trim();
            var connectionTrans = await _connectionTranslationServices
                .GetAsync(x => x.LanguageId == languageId && x.ConnectionId == connectionId);
            if (connectionTrans != null)
            {
                return new JsonResult(new
                {
                    status = "ok",
                    name = connectionTrans.Name,
                    value = connectionTrans.Value,
                    id = connectionTrans.Id
                });
            }

            return new JsonResult("nok");
        }

        public async Task<IActionResult> OnPostEditConnection(Connection connection)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string connectionType = GetEnumValue(connection.Type);
                    connection.Type = connectionType;
                    _connectionServices.UpdateConnection(connection);
                    await _connectionServices.Save();
                    return new JsonResult("ok");
                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new
                {
                    status = "error",
                    data = err.Message + " *** " + err.InnerException
                });
            }
        }
        public async Task<IActionResult> OnPostEditConnectionTrans(ConnectionTranslation connectionTranslation)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _connectionTranslationServices.Update(connectionTranslation);
                    await _connectionTranslationServices.Save();
                    return new JsonResult("ok");
                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new
                {
                    status = "error",
                    data = err.Message + " *** " + err.InnerException
                });
            }
        }

        public async Task<IActionResult> OnGetDeleteConnection(int id, int connectionId, string status)
        {
            try
            {
                if (status == "one")
                {
                    var connectionTransList = await _connectionTranslationServices.GetWithIncludeAsync
                        (selector: x => x,
                        where: x => x.ConnectionId == connectionId);
                    if (connectionTransList.Count() > 1)
                    {
                        var connectionTrans = await _connectionTranslationServices.GetAsync(x => x.Id == id);
                        _connectionTranslationServices.Delete(connectionTrans);
                        await _connectionTranslationServices.Save();
                    }
                    else
                    {
                        var connectionTrans = await _connectionTranslationServices.GetAsync(x => x.Id == id);
                        _connectionTranslationServices.Delete(connectionTrans);
                        await _connectionTranslationServices.Save();

                        var connection = await _connectionServices.GetAsync(x => x.Id == connectionId);
                        _connectionServices.Delete(connection);
                        await _connectionServices.Save();
                    }
                    return new JsonResult("ok");
                }
                else if (status == "all")
                {
                    var connectionTrans = await _connectionTranslationServices.GetAsync(x => x.Id == id);
                    var connectionTransList = await _connectionTranslationServices
                        .GetWithIncludeAsync(selector: x => x, where: x => x.ConnectionId == connectionTrans.ConnectionId);

                    _connectionTranslationServices.DeleteAll(connectionTransList);
                    await _connectionTranslationServices.Save();

                    var connection = await _connectionServices.GetAsync(x => x.Id == connectionId);
                    _connectionServices.Delete(connection);
                    await _connectionServices.Save();
                    return new JsonResult("ok");
                }
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
            return new JsonResult("nok");
        }
        #endregion

        #region Enum
        public string GetEnumValue(string connectionTypes)
        {
            string connectionType = "";
            switch (connectionTypes)
            {
                case "0":
                    connectionType = ConnectionType.Mobile.ToString();
                    break;
                case "1":
                    connectionType = ConnectionType.Email.ToString();
                    break;
                case "2":
                    connectionType = ConnectionType.Phone.ToString();
                    break;
                case "3":
                    connectionType = ConnectionType.Map.ToString();
                    break;
            }
            return connectionType;
        }
        #endregion
    }
}
