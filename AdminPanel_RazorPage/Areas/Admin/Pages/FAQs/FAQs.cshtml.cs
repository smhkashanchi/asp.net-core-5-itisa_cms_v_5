using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;
using Application.Interfaces;

using Domain.DomainClasses.FAQ;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace ItisaCMS_RazorPage.Areas.Admin.Pages.FAQs
{
    public class FAQModel : PageModel
    {
        private IFAQServices _fAQServices;
        public FAQModel(IFAQServices fAQServices)
        {
            _fAQServices = fAQServices;
        }

        #region FAQ
        public IActionResult OnGetCreateFAQ()
        {
            return Partial("_CreateFAQ");
        }
        public async Task<IActionResult> OnPostCreateFAQ(FAQ fAQ)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    await _fAQServices.Insert(fAQ);
                    await _fAQServices.Save();
                    return new JsonResult("ok");
                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", msg = err.Message });
            }
        }
        #region ViewAllFAQs
        public async Task<IActionResult> OnPostViewAllFAQs(int countryId)
        {
            try
            {
                var draw = Request.Form["draw"].FirstOrDefault();
                var start = Request.Form["start"].FirstOrDefault();
                var length = Request.Form["length"].FirstOrDefault();
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
                var searchValue = Request.Form["search[value]"].FirstOrDefault();
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;

                var fAQs = await _fAQServices.GetAllAsyn(x => x.Language_Id == CultureInfo.CurrentCulture.Name);

                if (!(string.IsNullOrEmpty(sortColumn)))
                {
                    fAQs = fAQs.AsQueryable().OrderBy(sortColumn + " " + sortColumnDirection);
                }
                if (!string.IsNullOrEmpty(searchValue))
                {
                    fAQs = fAQs.Where(m => m.Question.Contains(searchValue) || m.Answer.Contains(searchValue));

                }
                recordsTotal = fAQs.Count();
                var data = fAQs.Skip(skip).Take(pageSize).ToList();
                var jsonData = new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data };
                return new JsonResult(jsonData);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        public async Task<IActionResult> OnGetEditFAQ(int id)
        {
            var fAQ = await _fAQServices.GetAsync(x => x.FAQ_ID == id);
            return Partial("_EditFAQ", fAQ);
        }
        public async Task<IActionResult> OnPostEditFAQ(FAQ fAQ)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    await _fAQServices.Update(fAQ);
                    await _fAQServices.Save();
                    return new JsonResult("ok");
                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", msg = err.Message });
            }
        }
        public async Task<IActionResult> OnGetDeleteFAQ(int id)
        {
            try
            {
                var fAQ = await _fAQServices.GetAsync(x => x.FAQ_ID == id);
                if (fAQ != null)
                {
                    _fAQServices.Delete(fAQ);
                    await _fAQServices.Save();
                    return new JsonResult("ok");
                }
                return new JsonResult("nok");
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
        }
      
        #endregion
    }
}
