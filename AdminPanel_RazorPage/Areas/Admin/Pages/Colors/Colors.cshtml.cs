using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

using Application.Interfaces;

using Domain.DomainClasses;
using Domain.DomainClasses.Color;
using Domain.Interfaces;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace ItisaCMS_RazorPage.Areas.Admin.Pages.Colors
{
    public class ColorsModel : PageModel
    {
        public static string LanguageId = "";
        public static Dictionary<int, string> DicLanguageColors = new();
        private IColorServices _colorServices;
        private IColorTranslationServices _colorTranslationServices;
        private ILanguageServices _languageServices;
        public ColorsModel(IColorTranslationServices colorTranslationServices, IColorServices colorServices
            , ILanguageServices languageServices)
        {
            _colorServices = colorServices;
            _colorTranslationServices = colorTranslationServices;
            _languageServices = languageServices;
        }
        public void OnGet()
        {
        }

        public IActionResult OnGetCreate()
        {
            return Partial("_Create");
        }
        public async Task<IActionResult> OnPostCreate(Color color)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    await _colorServices.InsertColor(color);
                    await _colorServices.Save();
                    return new JsonResult(new { status = "ok", color_id = color.ColorId });
                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
        }
        public IActionResult OnGetCreateColorTrans(int id)
        {
            TempData["ColorId"] = id;
            return Partial("_CreateColorTrans");
        }
        public async Task<IActionResult> OnPostCreateColorTrans(ColorTranslation colorTranslation)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (!await _colorTranslationServices.ExistsAsync(x => x.ColorId == colorTranslation.ColorId
                     && x.LanguageId == colorTranslation.LanguageId))
                    {
                        await _colorTranslationServices.Insert(colorTranslation);
                        await _colorTranslationServices.Save();
                        return new JsonResult("ok");
                    }
                    return new JsonResult("exsist");
                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
        }

        public async Task<IActionResult> OnGetEdit(int id)
        {
            try
            {
                var color = await _colorServices.GetAsync(x => x.ColorId == (Int16)id);

                return Partial("_Edit", color);
            }
            catch (Exception err)
            {
                return new JsonResult(new
                {
                    status = "error",
                    data = err.Message + " *** " + err.InnerException
                });
            }
        }
        public async Task<IActionResult> OnGetEditColorTrans(int id)
        {
            var colorTrans = await _colorTranslationServices.GetAsync(x => x.Id == id);

            var languageColors = await _colorTranslationServices.GetWithIncludeAsync<ColorTranslation>(
                   selector: x => x,
                   include: s => s.Include(x => x.Color),
                   where: x => x.ColorId == colorTrans.ColorId
                   );
           
            return Partial("_EditColorTrans", languageColors);
        }
        public async Task<IActionResult> OnGetGetColorTitle(string id, int colorId)
        {
            if (!string.IsNullOrEmpty(id))
            {
                var color = await _colorTranslationServices
                    .GetAsync(x => x.LanguageId == id.Trim() && x.ColorId == colorId);
                if (color != null)
                {
                    return new JsonResult(new { status = "ok", title = color.Title, id = color.Id });
                }
                return new JsonResult("nok");
            }
            var defualtColor = await _colorTranslationServices.GetAsync(x => x.LanguageId == CultureInfo.CurrentCulture.Name && x.ColorId == colorId);
            return new JsonResult(new { status = "ok", title = defualtColor.Title, id = defualtColor.Id });
        }

        public async Task<IActionResult> OnPostEdit(Color color)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _colorServices.Update(color);
                    await _colorServices.Save();
                    return new JsonResult("ok");
                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new
                {
                    status = "error",
                    data = err.Message + " *** " + err.InnerException
                });
            }
        }
        public async Task<IActionResult> OnPostEditColorTrans(ColorTranslation colorTranslation)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _colorTranslationServices.Update(colorTranslation);
                    await _colorTranslationServices.Save();
                    return new JsonResult("ok");
                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new
                {
                    status = "error",
                    data = err.Message + " *** " + err.InnerException
                });
            }
        }
        public async Task<IActionResult> OnGetDelete(int id, int colorId, string status)
        {
            try
            {
                if (status == "one")
                {
                    var colorTrans = await _colorTranslationServices.GetAsync(x => x.Id == id);
                    _colorTranslationServices.DeleteAsync(colorTrans);
                    await _colorTranslationServices.Save();
                    return new JsonResult("ok");
                }
                else if (status == "all")
                {
                    var colorTrans = await _colorTranslationServices.GetAsync(x => x.Id == id);
                    var colorsTrans = await _colorTranslationServices.GetAllAsyn(x => x.ColorId == colorTrans.ColorId);
                    _colorTranslationServices.DeleteAll(colorsTrans);
                    await _colorTranslationServices.Save();

                    var color = await _colorServices.GetAsync(x => x.ColorId == colorId);
                    _colorServices.Delete(color);
                    await _colorServices.Save();
                    return new JsonResult("ok");
                }
            }
            catch (Exception err)
            {
                return new JsonResult(new
                {
                    status = "error",
                    data = err.Message + " *** " + err.InnerException
                });
            }
            return new JsonResult("nok");
        }

    }
}
