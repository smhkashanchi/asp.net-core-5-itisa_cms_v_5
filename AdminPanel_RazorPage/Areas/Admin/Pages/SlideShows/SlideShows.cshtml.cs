﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Application.Interfaces;
using System.Linq.Dynamic.Core;
using Domain.DomainClasses.SlideShow;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Globalization;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Application.Utilities;
using Utilities;
using Microsoft.EntityFrameworkCore;

namespace ItisaCMS_RazorPage.Areas.Admin.Pages.SlideShows
{
    public class SlideShowsModel : PageModel
    {
        public static string SlideShowPicFilePath = "Files/SlideShow";
        public static string SlideShowThumbPicFilePath = "Files/SlideShow/Thumb";
        public static string SlideShowMediumPicFilePath = "Files/SlideShow/Medium";
        private IWebHostEnvironment _hostEnvironment;
        private ISlideShowGroupServices _slideShowGroupServices;
        private ISlideShowServices _slideShowServices;
        private ISlideShowTranslationServices _slideShowTranslationServices;
        private ISlideShowGroupTranslationServices _slideShowGroupTranslationServices;
        public SlideShowsModel(ISlideShowGroupServices slideShowGroupServices, ISlideShowServices slideShowServices
            , ISlideShowTranslationServices slideShowTranslationServices, IWebHostEnvironment hostEnvironment
            , ISlideShowGroupTranslationServices slideShowGroupTranslationServices)
        {
            _hostEnvironment = hostEnvironment;
            _slideShowGroupServices = slideShowGroupServices;
            _slideShowServices = slideShowServices;
            _slideShowTranslationServices = slideShowTranslationServices;
            _slideShowGroupTranslationServices = slideShowGroupTranslationServices;
        }

        #region SlideShowGroup
        public IActionResult OnGetCreateGroup()
        {
            return Partial("_CreateGroup");
        }
        public async Task<IActionResult> OnPostCreateGroup(SlideShowGroup slideShowGroup)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    await _slideShowGroupServices.Insert(slideShowGroup);
                    await _slideShowGroupServices.Save();
                    return new JsonResult(new { status = "ok", groupId = slideShowGroup.SlideShowGroup_ID });
                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
        }
        public IActionResult OnGetCreateGroupTrans(int id)
        {
            TempData["GroupId"] = id;
            return Partial("_CreateGroupTrans");
        }
        public async Task<IActionResult> OnPostCreateGroupTrans(SlideShowGroupTranslation slideShowGroupTranslation)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (!await _slideShowGroupTranslationServices
                       .ExistsAsync(x => x.Language_Id == slideShowGroupTranslation.Language_Id && x.SlideShowGroup_Id == slideShowGroupTranslation.SlideShowGroup_Id))
                    {
                        await _slideShowGroupTranslationServices.Insert(slideShowGroupTranslation);
                        await _slideShowGroupTranslationServices.Save();
                        return new JsonResult("ok");
                    }
                    return new JsonResult("exsist");
                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
        }

        #region ViewAllGroups
        public async Task<IActionResult> OnPostViewAllGroups()
        {
            try
            {
                var draw = Request.Form["draw"].FirstOrDefault();
                var start = Request.Form["start"].FirstOrDefault();
                var length = Request.Form["length"].FirstOrDefault();
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
                var searchValue = Request.Form["search[value]"].FirstOrDefault();
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                var slideShowGroups = await _slideShowGroupTranslationServices.GetWithIncludeAsync<SlideShowGroupTranslation>
                (
                selector: x => x,
                include: s => s.Include(x => x.SlideShowGroup),
                where: x => x.Language_Id == CultureInfo.CurrentCulture.Name
                );
                if (!(string.IsNullOrEmpty(sortColumn)))
                {
                    slideShowGroups = slideShowGroups.AsQueryable().OrderBy(sortColumn + " " + sortColumnDirection);
                }
                if (!string.IsNullOrEmpty(searchValue))
                {
                    slideShowGroups = slideShowGroups.Where(m => m.Title.Contains(searchValue));

                }
                recordsTotal = slideShowGroups.Count();
                var data = slideShowGroups.Skip(skip).Take(pageSize).ToList();
                var jsonData = new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data };
                return new JsonResult(jsonData);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        public async Task<IActionResult> OnGetEditGroup(int id)
        {
            try
            {
                var slideshowGroup = await _slideShowGroupServices.GetAsync(x => x.SlideShowGroup_ID == (byte)id);
                return Partial("_EditGroup", slideshowGroup);
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
        }
        public async Task<IActionResult> OnGetEditGroupTrans(int id)
        {
            var slideShowGroupTrans = await _slideShowGroupTranslationServices.GetAsync(x => x.SlideShowGroupTranslation_ID == id);

            return Partial("_EditGroupTrans", slideShowGroupTrans);
        }
        public async Task<IActionResult> OnGetGroupTransInfo(string id, int slideShowGroupId)
        {

            string languageId = string.IsNullOrEmpty(id) ? CultureInfo.CurrentCulture.Name : id.Trim();
            var slideShowGroupTrans = await _slideShowGroupTranslationServices
                .GetAsync(x => x.Language_Id == id && x.SlideShowGroup_Id == slideShowGroupId);
            if (slideShowGroupTrans != null)
            {
                return new JsonResult(new
                {
                    status = "ok",
                    title = slideShowGroupTrans.Title,
                    id = slideShowGroupTrans.SlideShowGroupTranslation_ID
                });
            }
            return new JsonResult("nok");
        }
        public async Task<IActionResult> OnPostEditGroup(SlideShowGroup slideShowGroup)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    await _slideShowGroupServices.Update(slideShowGroup);
                    await _slideShowGroupServices.Save();
                    return new JsonResult("ok");
                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
        }
        public async Task<IActionResult> OnPostEditGroupTrans(SlideShowGroupTranslation slideShowGroupTranslation)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    await _slideShowGroupTranslationServices.Update(slideShowGroupTranslation);
                    await _slideShowGroupTranslationServices.Save();
                    return new JsonResult("ok");
                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
        }
        public async Task<IActionResult> OnGetDeleteGroup(int id, int groupId, string status)
        {
            try
            {
                if (status == "one")
                {
                    if (!await _slideShowTranslationServices.ExistsAsync(x => x.TranslationGroup_Id == id))
                    {
                        var groupTransList = await _slideShowGroupTranslationServices.GetWithIncludeAsync
                            (selector: x => x,
                            where: x => x.SlideShowGroup_Id == groupId);
                        if (groupTransList.Count() > 1)
                        {
                            var groupTrans = await _slideShowGroupTranslationServices.GetAsync(x => x.SlideShowGroupTranslation_ID == id);
                            _slideShowGroupTranslationServices.DeleteAsync(groupTrans);
                            await _slideShowGroupTranslationServices.Save();
                        }
                        else
                        {
                            var groupTrans = await _slideShowGroupTranslationServices.GetAsync(x => x.SlideShowGroupTranslation_ID == id);
                            _slideShowGroupTranslationServices.DeleteAsync(groupTrans);
                            await _slideShowGroupTranslationServices.Save();

                            var group = await _slideShowGroupServices.GetAsync(x => x.SlideShowGroup_ID == groupId);
                            _slideShowGroupServices.DeleteAsync(group);
                            await _slideShowGroupServices.Save();
                        }
                        return new JsonResult("ok");
                    }
                    return new JsonResult("exists");
                }

            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
            return new JsonResult("nok");
        }
        #endregion

        #region SlideShow
        public async Task<IActionResult> OnGetCreateSlideShow()
        {
            try
            {
                var slideShowGroups = await _slideShowGroupTranslationServices.GetWithIncludeAsync<SlideShowGroupTranslation>
                     (
                     selector: x => x,
                     include: s => s.Include(x => x.SlideShowGroup),
                     where: x => x.Language_Id == "fa-IR" && x.SlideShowGroup.IsActive
                     );
                TempData["SlideShowGroups"] = slideShowGroups;

                return Partial("_CreateSlideShow");
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
        }
        public async Task<IActionResult> OnPostCreateSlideShow(SlideShow slideShow, IFormFile imgSlideShow, IFormFile imgOrginal, IFormFile imgThumb, IFormFile imgMedium)
        {
            try
            {
                //if (ModelState.IsValid)
                //{
                if (imgSlideShow != null)
                {
                    var slideShowGroup = await _slideShowGroupServices.GetAsync(x => x.SlideShowGroup_ID == slideShow.GroupTranslationId);
                    Tuple<string, string, string> slideShowImages = await
                        CreateSlideShowImages(imgSlideShow, slideShowGroup.SmallPicWidth, slideShowGroup.SmallPicHeight
                        , slideShowGroup.MediumPicWidth, slideShowGroup.MediumPicHeight);
                    slideShow.Image = slideShowImages.Item1;
                    slideShow.SmallImage = slideShowImages.Item2;
                    slideShow.MediumImage = slideShowImages.Item3;
                    slideShow.SimpleUpload = true;
                }
                else if (imgOrginal != null && imgMedium != null && imgThumb != null)
                {
                    slideShow.Image = await CreateSlideShowImagesWithoutResize(imgOrginal, "Orginal");
                    slideShow.MediumImage = await CreateSlideShowImagesWithoutResize(imgMedium, "Medium");
                    slideShow.SmallImage = await CreateSlideShowImagesWithoutResize(imgThumb, "Thumb");
                    slideShow.SimpleUpload = false;
                }
                else
                    return new JsonResult("noImage");

                await _slideShowServices.Insert(slideShow);
                await _slideShowServices.Save();
                return new JsonResult(new { status = "ok", slideShowId = slideShow.SlideShow_ID });
                //}
                //return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
        }
        public IActionResult OnGetCreateSlideShowTrans(int id)
        {
            TempData["SlideShowId"] = id;
            return Partial("_CreateSlideShowTrans");
        }
        public async Task<IActionResult> OnPostCreateSlideShowTrans(SlideShowTranslation slideShowTranslation)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (!await _slideShowTranslationServices
                        .ExistsAsync(x => x.Language_Id == slideShowTranslation.Language_Id && x.SlideShow_Id == slideShowTranslation.SlideShow_Id))
                    {
                        await _slideShowTranslationServices.Insert(slideShowTranslation);
                        await _slideShowTranslationServices.Save();
                        return new JsonResult("ok");
                    }
                    return new JsonResult("exsist");

                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
        }

        #region ViewAllSlideShows
        public async Task<IActionResult> OnPostViewAllSlideShows(int groupId)
        {
            try
            {
                var draw = Request.Form["draw"].FirstOrDefault();
                var start = Request.Form["start"].FirstOrDefault();
                var length = Request.Form["length"].FirstOrDefault();
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
                var searchValue = Request.Form["search[value]"].FirstOrDefault();
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                IEnumerable<SlideShowTranslation> slideShows = null;
                if (groupId != 0)
                {
                    slideShows = await _slideShowTranslationServices.GetWithIncludeAsync<SlideShowTranslation>
                       (
                       selector: x => x,
                       include: s => s.Include(x => x.SlideShow).Include(x => x.SlideShowGroupTranslation),
                       where: x => x.Language_Id == CultureInfo.CurrentCulture.Name && x.SlideShowGroupTranslation.SlideShowGroup_Id== groupId
                       );
                }
                else
                {
                    slideShows = await _slideShowTranslationServices.GetWithIncludeAsync<SlideShowTranslation>
               (
               selector: x => x,
               include: s => s.Include(x => x.SlideShow).Include(x => x.SlideShowGroupTranslation),
               where: x => x.Language_Id == CultureInfo.CurrentCulture.Name
               );
                }
                if (!(string.IsNullOrEmpty(sortColumn)))
                {
                    slideShows = slideShows.AsQueryable().OrderBy(sortColumn + " " + sortColumnDirection);
                }
                if (!string.IsNullOrEmpty(searchValue))
                {
                    slideShows = slideShows.Where(m => m.Title.Contains(searchValue)
                        || m.SlideShowGroupTranslation.Title.Contains(searchValue));

                }
                recordsTotal = slideShows.Count();
                var data = slideShows.Skip(skip).Take(pageSize).ToList();
                var jsonData = new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data };
                return new JsonResult(jsonData);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        public async Task<IActionResult> OnGetEditSlideShow(int id)
        {
            try
            {
                var slideShowGroups = await _slideShowGroupTranslationServices.GetWithIncludeAsync<SlideShowGroupTranslation>
                    (
                    selector: x => x,
                    include: s => s.Include(x => x.SlideShowGroup),
                    where: x => x.Language_Id == "fa-IR" && x.SlideShowGroup.IsActive
                    );
                TempData["SlideShowGroups"] = slideShowGroups;


                var slideShow = await _slideShowServices.GetAsync(x => x.SlideShow_ID == (Int16)id);
                return Partial("_EditSlideShow", slideShow);
            }
            catch (Exception err)
            {
                return new JsonResult(new
                {
                    status = "error",
                    data = err.Message + " *** " + err.InnerException
                });
            }
        }
        public async Task<IActionResult> OnGetEditSlideShowTrans(int id)
        {
            var slideShowTrans = await _slideShowTranslationServices.GetAsync(x => x.SlideShowTranslation_ID == id);

            return Partial("_EditSlideShowTrans", slideShowTrans);
        }
        public async Task<IActionResult> OnGetSlideShowTransInfo(string id, int slideShowId)
        {

            string languageId = string.IsNullOrEmpty(id) ? CultureInfo.CurrentCulture.Name : id.Trim();
            var slideShowTrans = await _slideShowTranslationServices
                .GetAsync(x => x.Language_Id == languageId && x.SlideShow_Id == slideShowId);
            if (slideShowTrans != null)
            {
                return new JsonResult(new
                {
                    status = "ok",
                    title = slideShowTrans.Title,
                    isActive = slideShowTrans.IsActive,
                    text = slideShowTrans.Text,
                    link = slideShowTrans.Link,
                    slideShow_Id = slideShowTrans.SlideShow_Id,
                    groupId = slideShowTrans.TranslationGroup_Id,
                    id = slideShowTrans.SlideShowTranslation_ID
                });
            }

            return new JsonResult("nok");
        }

        public async Task<IActionResult> OnPostEditSlideShow(SlideShow slideShow, IFormFile imgSlideShow, IFormFile imgOrginal, IFormFile imgThumb, IFormFile imgMedium)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (imgSlideShow != null)
                    {
                        if (slideShow.Image != null)
                        {
                            FileManipulate.DeleteFile(_hostEnvironment.WebRootPath, SlideShowPicFilePath, slideShow.Image);
                            FileManipulate.DeleteFile(_hostEnvironment.WebRootPath, SlideShowThumbPicFilePath, slideShow.SmallImage);
                            FileManipulate.DeleteFile(_hostEnvironment.WebRootPath, SlideShowMediumPicFilePath, slideShow.MediumImage);
                        }
                        var slideShowGroup = await _slideShowGroupServices.GetAsync(x => x.SlideShowGroup_ID == slideShow.GroupTranslationId);
                        Tuple<string, string, string> slideShowImages = await
                            CreateSlideShowImages(imgSlideShow, slideShowGroup.SmallPicWidth, slideShowGroup.SmallPicHeight
                            , slideShowGroup.MediumPicWidth, slideShowGroup.MediumPicHeight);

                        slideShow.Image = slideShowImages.Item1;
                        slideShow.SmallImage = slideShowImages.Item2;
                        slideShow.MediumImage = slideShowImages.Item3;
                        slideShow.SimpleUpload = true;
                    }
                    else if (imgOrginal != null && imgMedium != null && imgThumb != null)
                    {
                        if (slideShow.Image != null)
                        {
                            FileManipulate.DeleteFile(_hostEnvironment.WebRootPath, SlideShowPicFilePath, slideShow.Image);
                            FileManipulate.DeleteFile(_hostEnvironment.WebRootPath, SlideShowThumbPicFilePath, slideShow.SmallImage);
                            FileManipulate.DeleteFile(_hostEnvironment.WebRootPath, SlideShowMediumPicFilePath, slideShow.MediumImage);
                        }

                        slideShow.Image = await CreateSlideShowImagesWithoutResize(imgOrginal, "Orginal");
                        slideShow.MediumImage = await CreateSlideShowImagesWithoutResize(imgMedium, "Medium");
                        slideShow.SmallImage = await CreateSlideShowImagesWithoutResize(imgThumb, "Thumb");
                        slideShow.SimpleUpload = false;
                    }

                    await _slideShowServices.Update(slideShow);
                    await _slideShowServices.Save();
                    return new JsonResult("ok");
                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new
                {
                    status = "error",
                    data = err.Message + " *** " + err.InnerException
                });
            }
        }
        public async Task<IActionResult> OnPostEditSlideShowTrans(SlideShowTranslation slideShowTranslation)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    await _slideShowTranslationServices.Update(slideShowTranslation);
                    await _slideShowTranslationServices.Save();
                    return new JsonResult("ok");
                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new
                {
                    status = "error",
                    data = err.Message + " *** " + err.InnerException
                });
            }
        }

        public async Task<IActionResult> OnGetDeleteSlideShow(int id, int slideShowId, string status)
        {
            try
            {
                var slideShowTransList = await _slideShowTranslationServices.GetWithIncludeAsync
                       (selector: x => x,
                       where: x => x.SlideShow_Id == (Int16)slideShowId);

                var slideShowTrans = await _slideShowTranslationServices.GetAsync(x => x.SlideShowTranslation_ID == id);

                if (status == "one")
                {
                   
                    if (slideShowTransList.Count() > 1)
                    {
                       
                        _slideShowTranslationServices.Delete(slideShowTrans);
                        await _slideShowTranslationServices.Save();
                    }
                    else
                    {
                        _slideShowTranslationServices.Delete(slideShowTrans);
                        await _slideShowTranslationServices.Save();

                        var slideShow = await _slideShowServices.GetAsync(x => x.SlideShow_ID == slideShowId);

                        if (slideShow.Image != null)
                        {
                            FileManipulate.DeleteFile(_hostEnvironment.WebRootPath, SlideShowPicFilePath, slideShow.Image);
                            FileManipulate.DeleteFile(_hostEnvironment.WebRootPath, SlideShowMediumPicFilePath, slideShow.MediumImage);
                            FileManipulate.DeleteFile(_hostEnvironment.WebRootPath, SlideShowThumbPicFilePath, slideShow.SmallImage);
                        }
                        _slideShowServices.DeleteAsync(slideShow);
                        await _slideShowServices.Save();
                    }
                    return new JsonResult("ok");
                }
                else if (status == "all")
                {

                    _slideShowTranslationServices.DeleteAll(slideShowTransList);
                    await _slideShowTranslationServices.Save();

                    var slideShow = await _slideShowServices.GetAsync(x => x.SlideShow_ID == slideShowId);

                    if (slideShow.Image != null)
                    {
                        FileManipulate.DeleteFile(_hostEnvironment.WebRootPath, SlideShowPicFilePath, slideShow.Image);
                        FileManipulate.DeleteFile(_hostEnvironment.WebRootPath, SlideShowMediumPicFilePath, slideShow.MediumImage);
                        FileManipulate.DeleteFile(_hostEnvironment.WebRootPath, SlideShowThumbPicFilePath, slideShow.SmallImage);
                    }
                    _slideShowServices.DeleteAsync(slideShow);
                    await _slideShowServices.Save();

                    return new JsonResult("ok");
                }
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
            return new JsonResult("nok");
        }

        //// تابع آن چینچ زبان در گرفتن گروه اسلایدشو
        public async Task<IActionResult> OnGetSlideShowGroupByLang(string id)
        {
            string lang_Id = "";
            if (string.IsNullOrEmpty(id))
                lang_Id = CultureInfo.CurrentCulture.Name;
            else
                lang_Id = id;

            var slideShowGroups = await _slideShowGroupTranslationServices.GetWithIncludeAsync<SlideShowGroupTranslation>
                     (
                     selector: x => x,
                     include: s => s.Include(x => x.SlideShowGroup),
                     where: x => x.Language_Id == lang_Id && x.SlideShowGroup.IsActive
                     );

            return new JsonResult(slideShowGroups);
        }
        ////گرفتن اندازه های تصاویر آپلودی
        public async Task<IActionResult> OnGetSlideShowImageSize(int groupId)
        {
            var slideshowGroup = await _slideShowGroupServices.GetAsync(x => x.SlideShowGroup_ID == groupId);
            return new JsonResult(slideshowGroup);
        }
        //// ساخت عکس 
        public async Task<Tuple<string, string, string>> CreateSlideShowImages(IFormFile imgContent, string thumbWidth, string thumbHeight, string mediumWidth, string mediumHeight)
        {
            string targetPath = Path.Combine(_hostEnvironment.WebRootPath, SlideShowPicFilePath);
            string pathMedium = Path.Combine(_hostEnvironment.WebRootPath, SlideShowMediumPicFilePath);
            string pathThumb = Path.Combine(_hostEnvironment.WebRootPath, SlideShowThumbPicFilePath);

            FileManipulate.CreateDirectory(targetPath, pathThumb, pathMedium);
            Tuple<string, string> ImagePath = await FileManipulate.CreateFile(_hostEnvironment.WebRootPath, imgContent, SlideShowPicFilePath);
            ///For Combine :ImageName and ImageFullPath
            Tuple<string, string, string> tuple = FileManipulate.Combine(_hostEnvironment.WebRootPath, ImagePath.Item2, targetPath, pathThumb, pathMedium);
            FileManipulate.resizeImage2(tuple.Item1, tuple.Item2, Convert.ToInt32(thumbWidth), Convert.ToInt32(thumbHeight));
            FileManipulate.resizeImage2(tuple.Item1, tuple.Item3, Convert.ToInt32(mediumWidth), Convert.ToInt32(mediumHeight));
            string orginalImagePath = Path.Combine(SlideShowPicFilePath, ImagePath.Item2);
            string thumbImagePath = Path.Combine(SlideShowThumbPicFilePath, ImagePath.Item2);
            string mediumImagePath = Path.Combine(SlideShowMediumPicFilePath, ImagePath.Item2);
            Tuple<string, string, string> result = Tuple.Create(orginalImagePath, thumbImagePath, mediumImagePath);
            return result;
        }

        public async Task<string> CreateSlideShowImagesWithoutResize(IFormFile uploadedImage, string which)
        {
            string FullImagePath = "";
            string targetPath = Path.Combine(_hostEnvironment.WebRootPath, SlideShowPicFilePath);
            string pathMedium = Path.Combine(_hostEnvironment.WebRootPath, SlideShowMediumPicFilePath);
            string pathThumb = Path.Combine(_hostEnvironment.WebRootPath, SlideShowThumbPicFilePath);

            FileManipulate.CreateDirectory(targetPath, pathThumb, pathMedium);

            ///For Combine :ImageName and ImageFullPath
            if (which == "Orginal")
            {
                Tuple<string, string> ImagePath = await FileManipulate.CreateFile(_hostEnvironment.WebRootPath, uploadedImage, SlideShowPicFilePath);
                FullImagePath = Path.Combine(SlideShowPicFilePath, ImagePath.Item2);
            }
            else if (which == "Medium")
            {
                Tuple<string, string> ImagePath = await FileManipulate.CreateFile(_hostEnvironment.WebRootPath, uploadedImage, SlideShowMediumPicFilePath);
                FullImagePath = Path.Combine(SlideShowMediumPicFilePath, ImagePath.Item2);
            }
            else if (which == "Thumb")
            {
                Tuple<string, string> ImagePath = await FileManipulate.CreateFile(_hostEnvironment.WebRootPath, uploadedImage, SlideShowThumbPicFilePath);

                FullImagePath = Path.Combine(SlideShowThumbPicFilePath, ImagePath.Item2);
            }

            return FullImagePath;
        }
        #endregion
    }
}
