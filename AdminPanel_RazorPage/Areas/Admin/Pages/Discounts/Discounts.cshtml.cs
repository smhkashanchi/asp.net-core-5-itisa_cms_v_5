﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

using Application.Interfaces;
using System.Linq.Dynamic.Core;
using Domain.DomainClasses.Product;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

using Utilities;
using Microsoft.EntityFrameworkCore;

namespace ItisaCMS_RazorPage.Areas.Admin.Pages.Discounts
{
    public class DiscountsModel : PageModel
    {
        public static bool IS_RANDOM = false;
        public static int COUNT_DISCOUNT_OLD = 0;
        private IDiscountServices _discountServices;
        private ISubDiscountServices _subDiscountServices;
        public DiscountsModel(IDiscountServices discountServices, ISubDiscountServices subDiscountServices)
        {
            _discountServices = discountServices;
            _subDiscountServices = subDiscountServices;
        }
        public IActionResult OnGetCreate()
        {
            return Partial("_Create");
        }
        public async Task<IActionResult> OnPostCreate(Discount discount, string sdate, string eDate)
        {
            if (ModelState.IsValid)
            {
                List<string> discount_cods = new List<string>();
                if (!discount.UnLimitTime)
                {
                    Tuple<DateTime, DateTime> MiladiDate = await PersianDateToMiladi(sdate, eDate);
                    discount.StartDate = MiladiDate.Item1.ToString("MM/dd/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture); //تاریخ شروع
                    discount.EndDate = MiladiDate.Item2.ToString("MM/dd/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
                    if (MiladiDate.Item1 >= MiladiDate.Item2)
                        return new JsonResult(new { res = "invalidDate" });
                }
                if (discount.IsRandom)
                {
                    for (int i = 0; i < discount.CountOff; i++)
                    {
                        string discount_code = MyExtensions.GenerateDiscountCode(discount.Title, discount.RandomType, discount.ExtraKeyCharCount.Value);
                        discount_cods.Add(discount_code);
                    }
                    discount.CountOff =(short)discount_cods.Count;
                    await _discountServices.Insert(discount);
                    await _discountServices.Save();

                    for (int i = 0; i < discount_cods.Count; i++)
                    {
                        SubDiscount subDiscount = new()
                        {
                            Discount_Id = discount.Discount_ID,
                            Title = discount_cods[i],
                        };
                        await _subDiscountServices.Insert(subDiscount);
                        await _subDiscountServices.Save();
                    }
                    return new JsonResult("ok");
                }
                await _discountServices.Insert(discount);
                await _discountServices.Save();
                return new JsonResult("ok");
            }
            foreach (var modelState in ViewData.ModelState.Values)
            {
                foreach (var error in modelState.Errors)
                {
                    // DoSomethingWith(error);
                }
            }
            return Page();
        }

        #region ViewAllDiscounts
        public async Task<IActionResult> OnPostViewAllDiscounts()
        {
            try
            {
                var draw = Request.Form["draw"].FirstOrDefault();
                var start = Request.Form["start"].FirstOrDefault();
                var length = Request.Form["length"].FirstOrDefault();
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
                var searchValue = Request.Form["search[value]"].FirstOrDefault();
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                var discounts = await _discountServices.GetWithIncludeAsync
                    (
                    selector: x => x,
                    where: x => x.Language_Id == CultureInfo.CurrentCulture.Name,
                    include:s=>s.Include(x=>x.SubDiscounts)
                    );

                if (!(string.IsNullOrEmpty(sortColumn)))
                {
                    discounts = discounts.AsQueryable().OrderBy(sortColumn + " " + sortColumnDirection);
                }
                if (!string.IsNullOrEmpty(searchValue))
                {
                    discounts = discounts.Where(m => m.Title.Contains(searchValue) || m.StartDate.ToString().Contains(searchValue) ||
                    m.EndDate.ToString().Contains(searchValue) ||
                     m.Percent.ToString().Contains(searchValue)
                    );

                }
                recordsTotal = discounts.Count();
                var data = discounts.Skip(skip).Take(pageSize).OrderByDescending(x => x.Discount_ID).ToList();
                var jsonData = new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data };
                return new JsonResult(jsonData);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        public async Task<IActionResult> OnGetEdit(int id)
        {
            var discont = await _discountServices.GetAsync(x => x.Discount_ID == id);
            TempData["UseDiscountCount"] = discont.CountUse;
            TempData["DiscountCount"] = discont.CountOff;
            IS_RANDOM = discont.IsRandom;
            if (discont.IsRandom)
            {
                var sub_discount = await _subDiscountServices.GetAllAsyn(x => x.Discount_Id == id);
                TempData["DiscountCount"] = sub_discount.Count();
                var sub_discount_not_use = await _subDiscountServices.GetAllAsyn(x => x.Discount_Id == id && !x.IsUse);
                TempData["UseDiscountCount"] = sub_discount.Count()- sub_discount_not_use.Count();
                int length = await GetLastKeyExtra(sub_discount);
                TempData["KeyExtra"] = length;
            }
            return Partial("_Edit", discont);
        }
        public async Task<IActionResult> OnPostEdit(Discount discount, string sdate, string eDate)
        {
            if (ModelState.IsValid)
            {
                List<string> discount_cods = new List<string>();
                if (discount.UnLimitUse)
                    discount.CountOff = 0;

                if (!discount.UnLimitTime)
                {
                    Tuple<DateTime, DateTime> MiladiDate = await PersianDateToMiladi(sdate, eDate);
                    discount.StartDate = MiladiDate.Item1.ToString("MM/dd/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture); //تاریخ شروع
                    discount.EndDate = MiladiDate.Item2.ToString("MM/dd/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
                    if (MiladiDate.Item1 >= MiladiDate.Item2)
                        return new JsonResult(new { res = "invalidDate" });
                }
                if (discount.IsRandom)
                {
                    var sub_discount = await _subDiscountServices.GetAllAsyn(x => x.Discount_Id == discount.Discount_ID);
                    var sub_discount_use = await _subDiscountServices.GetAllAsyn(x => x.Discount_Id == discount.Discount_ID && x.IsUse);
                    var sub_discount_not_use = await _subDiscountServices.GetAllAsyn(x => x.Discount_Id == discount.Discount_ID && !x.IsUse);

                    //// دو حالت وجود دارد
                    ///حالت اول
                    if (discount.CountOff > sub_discount.Count())
                    {
                        int countOffLenght = discount.CountOff.Value - sub_discount.Count();
                        for (int i = 0; i < countOffLenght; i++)
                        {
                            string discount_code = MyExtensions.GenerateDiscountCode(discount.Title, discount.RandomType, discount.ExtraKeyCharCount.Value);
                            discount_cods.Add(discount_code);
                        }
                    }
                    /// حالت دوم
                    else if (discount.CountOff < sub_discount.Count())
                    {
                        if (sub_discount_use.Count() < discount.CountOff)
                        {
                            int countOffLenght = sub_discount.Count() - discount.CountOff.Value;
                            for (int i = 0; i < countOffLenght; i++)
                            {
                                _subDiscountServices.Delete(sub_discount_not_use.ElementAtOrDefault(i));
                                await _subDiscountServices.Save();
                            }
                        }
                        else
                            return new JsonResult("noAccess");
                    }
                    await _discountServices.Update(discount);
                    await _discountServices.Save();

                    for (int i = 0; i < discount_cods.Count; i++)
                    {
                        SubDiscount subDiscount = new()
                        {
                            Discount_Id = discount.Discount_ID,
                            Title = discount_cods[i],
                        };
                        await _subDiscountServices.Insert(subDiscount);
                        await _subDiscountServices.Save();
                    }
                    return new JsonResult("ok");

                }
                if (IS_RANDOM)
                {
                    var sub_discount_not_use = await _subDiscountServices.GetAllAsyn(x => x.Discount_Id == discount.Discount_ID && !x.IsUse);
                    _subDiscountServices.DeleteAll(sub_discount_not_use);
                    await _subDiscountServices.Save();
                }
                await _discountServices.Update(discount);
                await _discountServices.Save();
                return new JsonResult("ok");
            }
            foreach (var modelState in ViewData.ModelState.Values)
            {
                foreach (var error in modelState.Errors)
                {
                    // DoSomethingWith(error);
                }
            }
            return Page();
        }
        public async Task<IActionResult> OnGetSubDiscount(int discountId)
        {
            var subDiscount = await _subDiscountServices.GetAllAsyn(x => x.Discount_Id == discountId);
            var subDiscount_Used = await _subDiscountServices.GetAllAsyn(x => x.Discount_Id == discountId && x.IsUse);

            return Partial("_SubDiscount", subDiscount);
        }
        public async Task<IActionResult> OnGetDelete(int discountId)
        {
            try
            {
                var discount = await _discountServices.GetAsync(x => x.Discount_ID == discountId);
                if (!await _subDiscountServices.ExistsAsync(x => x.Discount_Id == discount.Discount_ID && x.IsUse))
                {
                    var subDiscounts = await _subDiscountServices.GetAllAsyn(x => x.Discount_Id == discount.Discount_ID);
                    if (subDiscounts.Count() > 0)
                    {
                        _subDiscountServices.DeleteAll(subDiscounts);
                        await _subDiscountServices.Save();
                    }
                    _discountServices.Delete(discount);
                    await _discountServices.Save();
                    return new JsonResult("ok");
                }
                return new JsonResult("existsInSub");
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message });
            }
        }
        public async Task<Tuple<DateTime, DateTime>> PersianDateToMiladi(string sdate, string eDate)
        {
            DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            DateTime dtStart = dtDateTime.AddSeconds(double.Parse(sdate)).ToLocalTime();
            DateTime dtEnd = dtDateTime.AddSeconds(double.Parse(eDate)).ToLocalTime();
            return Tuple.Create(dtStart, dtEnd);
        }
        public async Task<int> GetLastKeyExtra(IEnumerable<SubDiscount> subDiscount)
        {
            string title = subDiscount.OrderByDescending(x => x.SubDiscount_ID).FirstOrDefault().Title;
            int startIndex = title.IndexOf("_");
            int length = title.Length - startIndex;
            title = title.Substring(startIndex + 1, length - 1);
            return title.Length;
        }

    }
}
