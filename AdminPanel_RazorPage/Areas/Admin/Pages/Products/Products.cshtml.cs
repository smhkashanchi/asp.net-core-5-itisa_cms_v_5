﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

using Application.Interfaces;
using Application.Services;
using Application.Utilities;
using System.Linq.Dynamic.Core;
using AutoMapper;

using Domain.DomainClasses.Product;

using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

using Newtonsoft.Json;
using Domain.DomainClasses.Feature;
using Application.ViewModels;
using GleamTech.FileUltimate.AspNet.UI;
using GleamTech.AspNet.UI;
using System.IO;
using Domain.DomainClasses.Gallery;
using Utilities;
using Microsoft.AspNetCore.Authorization;

namespace ItisaCMS_RazorPage.Areas.Admin.Pages.Products
{
    public class ProductsModel : PageModel
    {
        public static string temp = "";
        List<int> Ids = new List<int>();
        List<int> ProductGroupTranslationIdSelected = new List<int>();
        public static string ProductPicFilePath = "Files/Product";
        public static string ProductGroupPicFilePath = "Files/Product/Group";
        public static string ProductThumbPicFilePath = "Files/Product/Thumb";
        public static short ParentId = 0;
        public static short ParentIdEdit = 0;
        public static List<short> SelectedGroupIds = new List<short>();
        private SiteMap sm;
        private IMapper _mapper;
        private IWebHostEnvironment _hostEnvironment;
        public ISiteMapService _siteMapSerivce;
        private IProductGroupServices _productGroupServices;
        private IProductGroupTranslationServices _productGroupTranslationSrevices;
        private IProductServices _productServices;
        private IGalleryTranslationServices _galleryTranslationServices;
        private IGalleryServices _galleryServices;
        private IGalleryPictureServices _galleryPictureServices;
        private IProductTranslationServices _productTranslationServices;
        private IProductMiddleGroupServices _middleProductGroupServices;
        private IDiscountServices _discountServices;
        private ILanguageServices _languageServices;
        private IFeatureGroupTranslationServices _featureGroupTranslationServices;
        private IFeatureTranslationServices _featureTranslationServices;
        private IProductGroupFeatureServices _productGroupFeatureServices;
        private IProductFeatureServices _productFeatureServices;
        private IFeatureReplyTranslationServices _featureReplyTranslationServices;
        private ISimilarProductServices _similarProductServices;
        private ISuggestProductServices _suggestProductServices;
        public ProductsModel(ISiteMapService siteMapService, IProductGroupServices productGroupServices
            , IProductGroupTranslationServices productGroupTranslationSrevices, IProductServices productServices
            , IProductTranslationServices productTranslationServices, IGalleryTranslationServices galleryTranslationServices
            , IProductMiddleGroupServices middleProductGroupServices, IWebHostEnvironment environment, IDiscountServices discountServices, IMapper mapper
            , ILanguageServices languageServices, IFeatureGroupTranslationServices featureGroupTranslationServices
            , IFeatureTranslationServices featureTranslationServices, IProductGroupFeatureServices productGroupFeatureServices
            , IFeatureReplyTranslationServices featureReplyTranslationServices, IProductFeatureServices productFeatureServices, IGalleryPictureServices galleryPictureServices
            , IGalleryServices galleryServices, ISimilarProductServices similarProductServices, ISuggestProductServices suggestProductServices)
        {
            _siteMapSerivce = siteMapService;
            sm = new SiteMap();
            _mapper = mapper;
            _hostEnvironment = environment;
            _productGroupServices = productGroupServices;
            _productGroupTranslationSrevices = productGroupTranslationSrevices;
            _productServices = productServices;
            _productTranslationServices = productTranslationServices;
            _galleryTranslationServices = galleryTranslationServices;
            _middleProductGroupServices = middleProductGroupServices;
            _discountServices = discountServices;
            _languageServices = languageServices;
            _featureGroupTranslationServices = featureGroupTranslationServices;
            _featureTranslationServices = featureTranslationServices;
            _productGroupFeatureServices = productGroupFeatureServices;
            _featureReplyTranslationServices = featureReplyTranslationServices;
            _productFeatureServices = productFeatureServices;
            _galleryPictureServices = galleryPictureServices;
            _galleryServices = galleryServices;
            _similarProductServices = similarProductServices;
            _suggestProductServices = suggestProductServices;
        }

        #region Treeview
        public async Task<IActionResult> OnGetCreateTreeView(int id, string lang)
        {
            id = 1;
            return Content(JsonConvert.SerializeObject(await ViewInTreeView(id, lang)));
        }

        public async Task<List<Node>> ViewInTreeView(int PGId, string lang)//گرفتن گروه مطالب بصورت جیسون و فرستادن به تابع ساخت تری ویو
        {
            List<Node> TNodes = new List<Node>();
            List<Node> TNodes2 = new List<Node>();

            string LangId = "";
            if (lang != null)
                LangId = lang;
            else
                LangId = CultureInfo.CurrentCulture.Name;

            var productGroupData = await _productGroupTranslationSrevices.GetWithIncludeAsync
                (
                selector: x => x,
                where: x => x.ProductGroup.ProductGroup_ID == PGId && x.Language_Id == LangId,
                include: x => x.Include(s => s.ProductGroup)
                );

            var groups = await _productGroupTranslationSrevices.GetWithIncludeAsync
                (
                selector: x => x,
                where: x => x.ProductGroup.ParentId == PGId && x.Language_Id == LangId,
                include: x => x.Include(s => s.ProductGroup)
                );
            foreach (var item in groups)
            {
                var node = await ViewInTreeView(item.ProductGroup_Id, item.Language_Id);
                TNodes2.Add(node.FirstOrDefault());
            }


            TNodes.Add(new Node()
            {
                text = " " + productGroupData.FirstOrDefault().Title,
                href = "#" + productGroupData.FirstOrDefault().ProductGroupTranslation_ID,
                tags = PGId.ToString(),
                nodes = TNodes2
            });

            return TNodes;
        }

        #endregion

        #region ProductGroup
        public async Task<IActionResult> OnGetCreateProductGroup(int id)
        {
            var thisProductGroup = await _productGroupTranslationSrevices.GetAsync(x => x.ProductGroup_Id == id && x.Language_Id == CultureInfo.CurrentCulture.Name);
            TempData["GroupTitle"] = thisProductGroup.Title;
            TempData["GroupId"] = id; //ParentId
            return Partial("_CreateProductGroup");
        }
        public async Task<IActionResult> OnPostCreateProductGroup(ProductGroup productGroup, IFormFile imgProduct)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (imgProduct != null)
                    {
                        productGroup.Image = await FileManipulate.CreateImagesWithoutResize
                            (_hostEnvironment.WebRootPath, ProductGroupPicFilePath, imgProduct);
                    }


                    ParentId = productGroup.ParentId.Value;
                    await _productGroupServices.Insert(productGroup);
                    await _productGroupServices.Save();
                    return new JsonResult(new
                    { status = "ok", groupId = productGroup.ProductGroup_ID, image = productGroup.Image, parentId = productGroup.ParentId });
                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
        }
        public async Task<IActionResult> OnGetCreateProductGroupTrans(int id)
        {
            TempData["ProductGroupId"] = id;
            var language = await _languageServices.GetOne(CultureInfo.CurrentCulture.Name);
            TempData["Language1"] = language;
            return Partial("_CreateProductGroupTrans");
        }
        public async Task<IActionResult> OnGetCreateAnotherProductGroupTrans(string id)
        {
            var language = await _languageServices.GetOne(id);
            TempData["Language"] = language;
            return Partial("_CreateAnotherProductGroupTrans");
        }
        public async Task<IActionResult> OnPostCreateProductGroupTrans([FromBody] IEnumerable<ProductGroupTranslation> productGroupTranslations)
        {
            try
            {
                if (productGroupTranslations.Any(x => TryValidateModel(productGroupTranslations)))
                {
                    //sitemap
                    var productGroup = await _productGroupServices.GetAsync(x => x.ProductGroup_ID == productGroupTranslations.FirstOrDefault().ProductGroup_Id);
                    productGroup.SiteMap_Id = await _siteMapSerivce.AddProductGroup(productGroupTranslations);
                    _productGroupServices.Update(productGroup);
                    await _productGroupServices.Save();

                    await _productGroupTranslationSrevices.InsertRange(productGroupTranslations);
                    await _productGroupTranslationSrevices.Save();
                    return new JsonResult(new { status = "ok", group_id = productGroupTranslations.FirstOrDefault().ProductGroup_Id });
                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }

        }

        #region OnGetViewDetailsProductGroup
        public async Task<IActionResult> OnGetViewDetailsProductGroup(int groupId)
        {
            try
            {
                var productGroups = await _productGroupServices.GetAllAsyn();
                TempData["ProductGroups"] = productGroups;
                var productGroupTrans = await _productGroupTranslationSrevices.GetAsync(
                     where: x => x.ProductGroup.ProductGroup_ID == groupId && x.Language_Id == CultureInfo.CurrentCulture.Name,
                     include: s => s.Include(x => x.ProductGroup).ThenInclude(x => x.ProductGroupFeatures)
                    );

                return Partial("_ViewDetailsProductGroup", productGroupTrans);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        public async Task<IActionResult> OnGetEditProductGroup(int id)
        {
            try
            {
                var productGroup = await _productGroupServices.GetWithIncludeAsync
                    (
                    selector: x => x,
                    where: x => x.ProductGroup_ID == (byte)id,
                    include: s => s.Include(x => x.ProductGroupTranslations));

                TempData["GroupTitle"] = productGroup.FirstOrDefault().ProductGroupTranslations.FirstOrDefault().Title;

                return Partial("_EditProductGroup", productGroup.FirstOrDefault());
            }
            catch (Exception err)
            {
                return new JsonResult(new
                {
                    status = "error",
                    data = err.Message + " *** " + err.InnerException
                });
            }
        }
        public async Task<IActionResult> OnGetEditProductGroupTrans(int id)
        {
            try
            {
                var language = await _languageServices.GetOne(CultureInfo.CurrentCulture.Name);
                TempData["Language1"] = language;

                var productGroupTranslation = await _productGroupTranslationSrevices.GetAsync(x => x.ProductGroupTranslation_ID == id);

                return Partial("_EditProductGroupTrans", productGroupTranslation);
            }
            catch (Exception err)
            {
                return new JsonResult(new
                {
                    status = "error",
                    data = err.Message + " *** " + err.InnerException
                });
            }
        }

        public async Task<IActionResult> OnGetEditAnotherProductGroupTrans(string id, int productGroupId)
        {
            var language = await _languageServices.GetOne(id);
            TempData["Language"] = language;
            string languageId = string.IsNullOrEmpty(id) ? CultureInfo.CurrentCulture.Name : id.Trim();

            var productGroupTrans = await _productGroupTranslationSrevices
                .GetAsync(x => x.Language_Id == languageId && x.ProductGroup_Id == productGroupId);
            return Partial("_EditAnotherProductGroupTrans", productGroupTrans);
        }

        public async Task<IActionResult> OnPostEditProductGroup(ProductGroup productGroup, IFormFile imgProduct)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (imgProduct != null)
                    {
                        if (productGroup.Image != null && !productGroup.Image.Contains("Files/Images"))
                            FileManipulate.DeleteFile(_hostEnvironment.WebRootPath, "", productGroup.Image);

                        productGroup.Image = await FileManipulate.CreateImagesWithoutResize
                           (_hostEnvironment.WebRootPath, ProductGroupPicFilePath, imgProduct);
                    }

                    ParentIdEdit = productGroup.ParentId.Value;
                    _productGroupServices.Update(productGroup);
                    await _productGroupServices.Save();
                    return new JsonResult("ok");
                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new
                {
                    status = "error",
                    data = err.Message + " *** " + err.InnerException
                });
            }
        }
        public async Task<IActionResult> OnPostEditProductGroupTrans([FromBody] IEnumerable<ProductGroupTranslation> productGroupTranslations)
        {
            try
            {
                if (productGroupTranslations.Any(x => TryValidateModel(productGroupTranslations)))
                {

                    foreach (var productGroupTranslation in productGroupTranslations)
                    {
                        if (await _productGroupTranslationSrevices.ExistsAsync(x => x.Language_Id == productGroupTranslation.Language_Id && x.ProductGroup_Id == productGroupTranslation.ProductGroup_Id))
                        {
                            //sitemap
                            await _siteMapSerivce.EditProductGroup(productGroupTranslation);

                            _productGroupTranslationSrevices.Update(productGroupTranslation);
                            await _productGroupTranslationSrevices.Save();
                        }
                        else
                        {
                            //sitemap
                            var thisProductGroupTrans = await _productGroupTranslationSrevices.GetWithIncludeAsync
                                (
                                selector: x => x,
                                where: x => x.ProductGroup_Id == productGroupTranslation.ProductGroup_Id,
                                include: s => s.Include(x => x.ProductGroup)
                                );
                            if (thisProductGroupTrans.FirstOrDefault().ProductGroup.SiteMap_Id != 0)
                                await _siteMapSerivce.AddProductGroup(productGroupTranslation, true);
                            else
                                await _siteMapSerivce.AddProductGroup(productGroupTranslation, false);

                            await _productGroupTranslationSrevices.Insert(productGroupTranslation);
                            await _productGroupTranslationSrevices.Save();
                        }
                    }




                    return new JsonResult(new { status = "ok", group_id = productGroupTranslations.FirstOrDefault().ProductGroup_Id });
                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }

        }

        public async Task<IActionResult> OnGetDeleteProductGroup(int id, int productGroupId, string status)
        {
            try
            {
                var productGroup = await _productGroupServices.GetAsync(x => x.ProductGroup_ID == productGroupId);
                var productGroupTransList = await _productGroupTranslationSrevices.GetWithIncludeAsync
                    (
                    selector: x => x,
                    where: x => x.ProductGroup_Id == productGroupId,
                    include: s => s.Include(x => x.ProductGroup));

                var productGroupTrans = await _productGroupTranslationSrevices.GetWithIncludeAsync
                    (
                    selector: x => x,
                    where: x => x.ProductGroupTranslation_ID == id,
                    include: s => s.Include(x => x.ProductGroup)
                    );



                if (status == "one")
                {
                    if (!await _middleProductGroupServices
                        .ExistsAsync(x => x.ProductGroup_Id == productGroupTrans.FirstOrDefault().ProductGroup_Id))
                    {


                        if (!await _productGroupServices.ExistsAsync(x => x.ParentId == productGroupTrans.FirstOrDefault().ProductGroup_Id))
                        {
                            if (productGroupTransList.Count() > 1)
                            {
                                //sitemap 
                                await _siteMapSerivce.DeleteProductGroup(productGroupTrans.FirstOrDefault(), productGroupTransList.Count());

                                _productGroupTranslationSrevices.Delete(productGroupTrans.FirstOrDefault());
                                await _productGroupTranslationSrevices.Save();
                                return new JsonResult("ok");
                            }
                            else
                            {
                                //sitemap 
                                await _siteMapSerivce.DeleteProductGroup(productGroupTrans.FirstOrDefault(), productGroupTransList.Count());

                                _productGroupTranslationSrevices.Delete(productGroupTrans.FirstOrDefault());
                                await _productGroupTranslationSrevices.Save();

                                //Delete ProductGroup
                                if (productGroup.Image != null)
                                    FileManipulate.DeleteFile(_hostEnvironment.WebRootPath, "", productGroup.Image);



                                _productGroupServices.Delete(productGroup);
                                await _productGroupServices.Save();
                                return new JsonResult("ok");
                            }
                        }
                        return new JsonResult("existParent");
                    }
                    return new JsonResult("existProduct");

                }
                else if (status == "all")
                {
                    //sitemap 
                    bool flag = false;


                    foreach (var productGroupTranslation in productGroupTransList)
                    {
                        if (!await _middleProductGroupServices
                       .ExistsAsync(x => x.ProductGroup_Id == productGroupTranslation.ProductGroupTranslation_ID))
                        {
                            if (!await _productGroupServices.ExistsAsync(x => x.ParentId == productGroupTranslation.ProductGroup_Id))
                            {
                                if (flag == false)
                                {
                                    await _siteMapSerivce.DeleteProductGroup(productGroupTransList);
                                    flag = true;
                                }
                                _productGroupTranslationSrevices.Delete(productGroupTranslation);
                                await _productGroupTranslationSrevices.Save();
                            }
                            else
                                return new JsonResult(new { status = "existParent", title = productGroupTranslation.Title });
                        }
                        else
                            return new JsonResult("existProduct");
                    }



                    //_productGroupTranslationSrevices.DeleteAll(productGroupTransList);
                    //await _productGroupTranslationSrevices.Save();
                    //Delete ProductGroup

                    if (productGroup.Image != null)
                        FileManipulate.DeleteFile(_hostEnvironment.WebRootPath, "", productGroup.Image);

                    _productGroupServices.Delete(productGroup);
                    await _productGroupServices.Save();
                    return new JsonResult("ok");
                }
            }
            catch (Exception err)
            {
                return new JsonResult(new
                {
                    status = "error",
                    data = err.Message + " *** " + err.InnerException
                });
            }
            return new JsonResult("nok");
        }

        public async Task<IActionResult> OnGetDiscountWithLanguage(string id)
        {
            string lang = "";
            if (string.IsNullOrEmpty(id))
                lang = CultureInfo.CurrentCulture.Name;
            else
                lang = id;

            var discounts = await _discountServices.GetAllAsyn(x => x.IsActive && x.Language_Id == lang);
            return new JsonResult(discounts);
        }

        public async Task<IActionResult> OnGetCreateFeaturesForProductGroup(int id)
        {
            var thisProductGroup = await _productGroupTranslationSrevices.GetAsync(x => x.ProductGroupTranslation_ID == id);
            var featureGroups = await _featureGroupTranslationServices.GetAllAsyn(x => x.Language_Id == CultureInfo.CurrentCulture.Name);
            TempData["FeatureGroups"] = featureGroups;
            return Partial("_CreateFeaturesForProductGroup", thisProductGroup);
        }
        public async Task<IActionResult> OnGetFeaturesByGroup(int featureGroupId)
        {
            var features = await _featureTranslationServices.GetWithIncludeAsync
                (
                selector: x => x,
                where: x => x.Feature.FeatureGroup_Id == featureGroupId && x.Language_Id == CultureInfo.CurrentCulture.Name,
                include: s => s.Include(x => x.Feature)
                );
            return Partial("_FeaturesByGroup", features);
        }
        public async Task<IActionResult> OnGetCreateProductGroupFeatures(List<string> features, int productGroupId)
        {
            try
            {
                List<ProductGroupFeature> productGroupFeatures = new List<ProductGroupFeature>();
                for (int i = 0; i < features.Count - 1; i++)
                {
                    if (!await _productGroupFeatureServices.ExistsAsync(x => x.Feature_Id == Convert.ToInt32(features[i]) && x.ProductGroup_Id == productGroupId))
                    {
                        productGroupFeatures.Add(new ProductGroupFeature()
                        {
                            ProductGroupFeature_ID = Guid.NewGuid().ToString(),
                            Feature_Id = Convert.ToInt32(features[i]),
                            ProductGroup_Id = Convert.ToInt16(productGroupId),
                        });
                    }
                    else
                        return new JsonResult("exists");
                }
                await _productGroupFeatureServices.InsertRange(productGroupFeatures);
                await _productGroupFeatureServices.Save();
                return new JsonResult("ok");
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
        }

        public async Task<IActionResult> OnGetProductGroupFeatures(int productGroupId)
        {
            var productGroupFeatures = await _productGroupFeatureServices.GetWithIncludeAsync
                (
                selector: x => x,
                where: x => x.ProductGroup_Id == productGroupId,
                include: s => s.Include(x => x.Feature).ThenInclude(x => x.FeatureTranslations).Include(x => x.ProductGroup)
                );

            return Partial("_ProductGroupFeatures", productGroupFeatures);
        }

        public async Task<IActionResult> OnGetDeleteProductGroupFeatures(List<string> features, int productGroupId)
        {
            try
            {
                List<ProductGroupFeature> productGroupFeatures = new List<ProductGroupFeature>();
                for (int i = 0; i < features.Count - 1; i++)
                {
                    var productGroupFeature = await _productGroupFeatureServices
                         .GetAsync(x => x.ProductGroup_Id == productGroupId && x.Feature_Id == Convert.ToInt32(features[i]));
                    productGroupFeatures.Add(productGroupFeature);
                }
                _productGroupFeatureServices.DeleteRange(productGroupFeatures);
                await _productGroupFeatureServices.Save();
                return new JsonResult("ok");
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
        }
        #endregion

        public async Task<IActionResult> OnGetCreateProduct()
        {
            var language = await _languageServices.GetOne(CultureInfo.CurrentCulture.Name);
            TempData["Language"] = language;
            return Partial("_CreateProduct");
        }
        public async Task<IActionResult> OnGetCreateAnotherProductTrans(string id)
        {
            var language = await _languageServices.GetOne(id);
            TempData["Language1"] = language;
            return Partial("_CreateAnotherProductTrans");
        }
        public async Task<IActionResult> OnGetEditProduct(int id)
        {
            var language = await _languageServices.GetOne(CultureInfo.CurrentCulture.Name);
            var thisProduct = await _productTranslationServices.GetWithIncludeAsync
                (
                selector: x => x,
                where: x => x.Product_Id == id && x.Language_Id == language.Id,
                include: s => s.Include(x => x.Product));
            TempData["Language"] = language;
            return Partial("_EditProduct", thisProduct.FirstOrDefault());
        }
        public async Task<IActionResult> OnGetEditAnotherProductTrans(string lang, int productId)
        {
            var language = await _languageServices.GetOne(lang);
            var thisProduct = await _productTranslationServices.GetAsync(x => x.Product_Id == productId && x.Language_Id == language.Id);
            TempData["Language1"] = language;
            return Partial("_EditAnotherProductTrans", thisProduct);
        }

        //// ثبت اطلاعات مشترک محصول
        public IActionResult OnGetCreateProductSetting()
        {
            return Partial("_CreateProductSetting");
        }
        public async Task<IActionResult> OnPostUpdateProductSetting(Product product)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (product.Gallery_Id == 0)
                        product.Gallery_Id = null;

                    _productServices.Update(product);
                    await _productServices.Save();
                    return new JsonResult("ok");
                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
        }
        public IActionResult OnGetEditProductSetting()
        {
            return Partial("_EditProductSetting");
        }

        public async Task<IActionResult> OnPostCreateProductTrans([FromBody] IEnumerable<ProductTranslation> productTranslations)
        {
            try
            {
                if (productTranslations.Any(x => TryValidateModel(productTranslations)))
                {
                    List<ProductTranslation> productTranslationList = new List<ProductTranslation>();
                    Product product = new()
                    {
                        CreateDate = DateTime.Now
                    };
                    if (productTranslations.Any(x => x.Product_Id == 0))
                    {
                        await _productServices.Insert(product);
                        await _productServices.Save();
                    }
                    foreach (var productTranslation in productTranslations)
                    {
                        if (!await _productTranslationServices
                       .ExistsAsync(x => x.Language_Id == productTranslation.Language_Id && x.Product_Id == productTranslation.Product_Id))
                        {
                            if (productTranslation.Product_Id == 0)
                                productTranslation.Product_Id = product.Product_ID;

                            productTranslationList.Add(productTranslation);
                        }
                        else
                        {
                            var thisProductTranslation = await _productTranslationServices.GetAsync(x => x.Language_Id == productTranslation.Language_Id && x.Product_Id == productTranslation.Product_Id);
                            _productTranslationServices.Update(thisProductTranslation);
                            await _productTranslationServices.Save();
                            return new JsonResult(new { status = "ok", productId = product.Product_ID });
                        }
                    }
                    await _productTranslationServices.InsertRange(productTranslationList);
                    await _productTranslationServices.Save();
                    List<int> ids = productTranslationList.Select(x => x.ProductTranslation_ID).ToList();
                    return new JsonResult(new { status = "ok", productId = product.Product_ID, productTranId = ids, createDate = product.CreateDate });
                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
        }
        public async Task<IActionResult> OnPostUpdateProductTrans([FromBody] IEnumerable<ProductTranslation> productTranslations)
        {
            try
            {
                if (productTranslations.Any(x => TryValidateModel(productTranslations)))
                {
                    List<ProductTranslation> productTranslationList = new List<ProductTranslation>();

                    foreach (var productTranslation in productTranslations)
                    {
                        if (!await _productTranslationServices
                       .ExistsAsync(x => x.Language_Id == productTranslation.Language_Id && x.Product_Id == productTranslation.Product_Id))
                        {

                            await _productTranslationServices.Insert(productTranslation);
                            await _productTranslationServices.Save();
                        }
                        else
                        {
                            _productTranslationServices.Update(productTranslation);
                            await _productTranslationServices.Save();
                        }
                    }

                    //List<int> ids = productTranslationList.Select(x => x.ProductTranslation_ID).ToList();
                    return new JsonResult(new { status = "ok", productId = productTranslations.FirstOrDefault().Product_Id });
                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
        }
        public async Task<IActionResult> OnGetDeleteProduct(int id, int productId, string status)
        {
            try
            {
                var product = await _productServices.GetAsync(x => x.Product_ID == productId,
                orderBy: x => x.OrderBy(s => s.Product_ID),
                include: x => x.Include(s => s.Gallery).Include(s=>s.SimilarProduct)
                .Include(s => s.SimilarProduct2).Include(s => s.SuggestProduct).Include(s => s.SuggestProducts));

                var productTransList = await _productTranslationServices.GetWithIncludeAsync
                    (
                    selector: x => x,
                    where: x => x.Product_Id == productId,
                    include: s => s.Include(x => x.Product));

                var productTrans = await _productTranslationServices.GetWithIncludeAsync
                    (
                    selector: x => x,
                    where: x => x.ProductTranslation_ID == id,
                    include: s => s.Include(x => x.Product)
                    );



                if (status == "one")
                {
                    if (productTransList.Count() > 1)
                    {
                        //sitemap 
                        await _siteMapSerivce.DeleteProduct(productTrans.FirstOrDefault(), productTransList.Count(),"one");

                        _productTranslationServices.Delete(productTrans.FirstOrDefault());
                        await _productTranslationServices.Save();
                        return new JsonResult("ok");
                    }
                    else
                    {
                        //sitemap 
                        await _siteMapSerivce.DeleteProduct(productTrans.FirstOrDefault(), productTransList.Count(),"one");

                        _productTranslationServices.Delete(productTrans.FirstOrDefault());
                        await _productTranslationServices.Save();

                        Int16 productGalleryId = 0;
                        //Delete Gallery
                        if (product.Gallery_Id != null)
                            productGalleryId = product.Gallery_Id.Value;

                        if (product.SuggestProduct.Count() > 0)
                        {
                            _suggestProductServices.DeleteAll(product.SuggestProduct);
                            await _suggestProductServices.Save();
                        }
                        if (product.SuggestProducts.Count() > 0)
                        {
                            _suggestProductServices.DeleteAll(product.SuggestProducts);
                            await _suggestProductServices.Save();
                        }
                        if (product.SimilarProduct.Count() > 0)
                        {
                            _similarProductServices.DeleteAll(product.SimilarProduct);
                            await _similarProductServices.Save();
                        }
                        if (product.SimilarProduct2.Count() > 0)
                        {
                            _similarProductServices.DeleteAll(product.SimilarProduct2);
                            await _similarProductServices.Save();
                        }

                        await _productServices.Delete(product);
                        await _productServices.Save();
                        return new JsonResult("ok");
                    }


                }
                else if (status == "all")
                {
                    //sitemap 
                    bool flag = false;


                    foreach (var productTranslation in productTransList)
                    {
                        if (flag == false)
                        {
                            await _siteMapSerivce.DeleteProduct(productTranslation, productTransList.Count(),"all");
                            flag = true;
                        }
                        _productTranslationServices.Delete(productTranslation);
                        await _productTranslationServices.Save();

                    }



                    //_productGroupTranslationSrevices.DeleteAll(productGroupTransList);
                    //await _productGroupTranslationSrevices.Save();
                    //Delete ProductGroup
                    Int16 productGalleryId = 0;
                    //Delete Gallery
                    if (product.Gallery_Id != null)
                        productGalleryId = product.Gallery_Id.Value;

                    if (product.SuggestProduct.Count() > 0)
                    {
                        _suggestProductServices.DeleteAll(product.SuggestProduct);
                        await _suggestProductServices.Save();
                    }
                    if (product.SuggestProducts.Count() > 0)
                    {
                        _suggestProductServices.DeleteAll(product.SuggestProducts);
                        await _suggestProductServices.Save();
                    }
                    if (product.SimilarProduct.Count() > 0)
                    {
                        _similarProductServices.DeleteAll(product.SimilarProduct);
                        await _similarProductServices.Save();
                    }
                    if (product.SimilarProduct2.Count() > 0)
                    {
                        _similarProductServices.DeleteAll(product.SimilarProduct2);
                        await _similarProductServices.Save();
                    }

                    await _productServices.Delete(product);
                    await _productServices.Save();
                    if (product.Gallery_Id != null)
                    {
                        _galleryServices.Delete(product.Gallery);
                        await _galleryServices.Save();
                    }
                    return new JsonResult("ok");
                }
            }
            catch (Exception err)
            {
                return new JsonResult(new
                {
                    status = "error",
                    data = err.Message + " *** " + err.InnerException
                });
            }
            return new JsonResult("nok");
        }
        //// ثبت گروه های محصول
        public async Task<IActionResult> OnPostCreateGroupOfProduct(List<string> SelectedProductGroupIds, int productId)
        {
            //List<ProductMiddleGroup> productMiddleGroups = new List<ProductMiddleGroup>();
            ProductMiddleGroup productMiddleGroup = new ProductMiddleGroup();
            List<int> siteMapIds = new List<int>();
            var productTranslations = await _productTranslationServices.GetAllAsyn(x => x.Product_Id == productId);

            List<int> ProductGroupIds = new List<int>();
            if (SelectedProductGroupIds.Contains(null))
            {
                SelectedProductGroupIds.RemoveAt(SelectedProductGroupIds.IndexOf(null));
                ProductGroupIds = SelectedProductGroupIds.Select(int.Parse).ToList();
            }
            else
                ProductGroupIds = SelectedProductGroupIds.Select(int.Parse).ToList();

            List<int> ProductGroupIds2 = SelectedProductGroupIds.Select(int.Parse).ToList();
            for (int i = 0; i < ProductGroupIds.Count; i++) ///گرفتن پدرهای آیدی های انتخاب شده
            {
                ProductGroupTranslationIdSelected = await
                    GetAllParents(ProductGroupIds[i]);
            }
            ProductGroupIds = ProductGroupTranslationIdSelected.Distinct().ToList();

          

            //Delete Old In Middle
            foreach (var productTranslation in productTranslations)
            {
                await _siteMapSerivce.DeleteProduct(productTranslation, productTranslations.Count(), "all");
            }

            var thisProductMiddleId = await _middleProductGroupServices.GetAllAsyn(x => x.Product_Id == productId);
            _middleProductGroupServices.DeleteAll(thisProductMiddleId);
            await _middleProductGroupServices.Save();


            //SiteMap
            siteMapIds = await _siteMapSerivce.AddProduct(productTranslations, ProductGroupIds);


            for (int i = 0; i < siteMapIds.Count; i++)
            {
                if (!await _middleProductGroupServices.ExistsAsync(x => x.Product_Id == productId && x.ProductGroup_Id == Convert.ToInt16(ProductGroupIds[i])))
                {
                    productMiddleGroup.ID = Guid.NewGuid().ToString();
                    productMiddleGroup.ProductGroup_Id = Convert.ToInt16(ProductGroupIds[i]);
                    productMiddleGroup.Product_Id = productId;
                    productMiddleGroup.SiteMap_Id = siteMapIds[i];
                    await _middleProductGroupServices.Insert(productMiddleGroup);
                    await _middleProductGroupServices.Save();
                }
            }
            return new JsonResult("ok");
        }
        public async Task<IActionResult> OnGetViewAllGroupsOfProduct(int id)
        {

            var productMiddleGroups = await _middleProductGroupServices.GetWithIncludeAsync
                (
                selector: x => x,
                where: x => x.Product_Id == id&& x.ProductGroup.ProductGroupTranslations.Any(x=>x.Language_Id==CultureInfo.CurrentCulture.Name) && x.ProductGroup.ProductGroupTranslations.Select(s=>s.ProductGroup_Id).Contains(x.ProductGroup_Id),
                include: s => s.Include(x => x.ProductGroup).ThenInclude(x => x.ProductGroupTranslations));

            return Partial("_ViewAllGroupsOfProduct", productMiddleGroups);
        }
        #region ViewAllProducts
        public async Task<IActionResult> OnPostViewAllProducts(int productGroupId)
        {
            try
            {
                var draw = Request.Form["draw"].FirstOrDefault();
                var start = Request.Form["start"].FirstOrDefault();
                var length = Request.Form["length"].FirstOrDefault();
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
                var searchValue = Request.Form["search[value]"].FirstOrDefault();
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                IEnumerable<ProductTranslation> products = null;
                if (productGroupId != 0 && productGroupId != 1)
                    products = await _productTranslationServices.GetWithIncludeAsync
                        (
                        selector: x => x,
                        where: x => x.Language_Id == CultureInfo.CurrentCulture.Name && x.Product.ProductMiddleGroups.Any(s => s.ProductGroup_Id == productGroupId),
                        include: s => s.Include(x => x.Product).ThenInclude(x => x.ProductMiddleGroups)
                        );
                else
                    products = await _productTranslationServices.GetWithIncludeAsync
                        (
                        selector: x => x,
                        // شرط پایین بررسی میکند درصورتی که این محصول داخل جدول میدل باشد رو گت کند
                        where: x => //x.Product.ProductMiddleGroups.Select(s => s.Product_Id).Contains(x.Product_Id) &&
                            x.Language_Id == CultureInfo.CurrentCulture.Name,
                        include: s => s.Include(x => x.Product).ThenInclude(x => x.ProductMiddleGroups)
                        );


                products = products.OrderByDescending(x => x.Product.ProductTranslations.FirstOrDefault().ProductTranslation_ID);
                if (productGroupId == 0)
                    ///برای حذف آیتم های تکراری == Distinct
                    products = products.GroupBy(x => x.Product_Id).Select(x => x.First());

                if (!(string.IsNullOrEmpty(sortColumn)))
                {
                    products = products.AsQueryable().OrderBy(sortColumn + " " + sortColumnDirection);
                }
                if (!string.IsNullOrEmpty(searchValue))
                {
                    products = products.Where(m => m.Title.Contains(searchValue)
                    || m.Product.CreateDate.ToString().Contains(searchValue)
                    );

                }
                recordsTotal = products.Count();
                var data = products.Skip(skip).Take(pageSize).ToList();
                var jsonData = new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data };
                return new JsonResult(jsonData);
            }
            catch (Exception err)
            {
                throw;
            }
        }
        #endregion

        //// ویژگی های محصول
        public async Task<IActionResult> OnGetProductGroupFeaturesForProduct(List<string> SelectedProductGroupIds)
        {
            List<int> ProductGroupIds = SelectedProductGroupIds.Select(int.Parse).ToList();
            var features = await _featureTranslationServices.GetWithIncludeAsync(
                selector: x => x,
                where: x => x.Language_Id == CultureInfo.CurrentCulture.Name && x.Feature.ProductGroupFeatures.Any(s => ProductGroupIds.Contains(s.ProductGroup_Id)),
                include: s => s.Include(x => x.Feature)
                );

            //features = features.Where(x => ProductGroupIds.Contains(x.ProductGroup_Id)).ToList();

            return new JsonResult(features);
        }
        public async Task<IActionResult> OnGetFeatureReplyOfFeature(int featureId)
        {
            var feature = await _featureTranslationServices.GetAsync(x => x.Feature_Id == featureId && x.Language_Id == CultureInfo.CurrentCulture.Name);
            TempData["FeatureTitle"] = feature.Title;
            var featureReplies = await _featureReplyTranslationServices.GetWithIncludeAsync
                (
                selector: x => x,
                where: x => x.FeatureReply.Feature_Id == featureId && x.Language_Id == CultureInfo.CurrentCulture.Name,
                include: s => s.Include(x => x.FeatureReply)
                );

            return Partial("_FeatureReplyOfFeature", featureReplies);
        }
        public async Task<IActionResult> OnPostCreateProductFeatures(ProductFeatureVM productFeatureVM)
        {
            List<int> FeatureReplyIds = productFeatureVM.FeatureReplyIds.Select(int.Parse).ToList();
            ProductFeature productFeature = new ProductFeature();
            for (int i = 0; i < FeatureReplyIds.Count; i++)
            {
                if (!await _productFeatureServices.ExistsAsync(x => x.Product_Id == productFeatureVM.ProductId && x.FeatureReply_Id == FeatureReplyIds[i]))
                {
                    productFeature.ProductFeature_ID = Guid.NewGuid().ToString();
                    productFeature.FeatureReply_Id = FeatureReplyIds[i];
                    productFeature.Feature_Id = productFeatureVM.FeatureId;
                    productFeature.Product_Id = productFeatureVM.ProductId;
                    productFeature.IsVariable = productFeatureVM.IsVariable;
                    await _productFeatureServices.Insert(productFeature);
                    await _productFeatureServices.Save();
                }
                else
                    return new JsonResult("exists");

            }
            return new JsonResult("ok");
        }
        public async Task<IActionResult> OnGetViewAllProductFeatures(int productId)
        {
            TempData["ProductId"] = productId;
            var productFeatures = await _featureReplyTranslationServices.GetWithIncludeAsync
                (
                selector: x => x,
                where: x => x.FeatureReply.ProductFeatures.Select(s => s.FeatureReply_Id).Contains(x.FeatureReply_Id)
                  && x.FeatureReply.ProductFeatures.Any(x => x.Product_Id == productId) && x.Language_Id == CultureInfo.CurrentCulture.Name,
                include: s => s.Include(x => x.FeatureReply)
                .ThenInclude(x => x.Feature)
                .ThenInclude(x => x.FeatureTranslations)
                .Include(x => x.FeatureReply)
                .ThenInclude(x => x.ProductFeatures));

            return Partial("_ViewAllProductFeatures", productFeatures);
        }

        public async Task<IActionResult> OnGetDeleteProductFeatures(List<string> featureReplies, int productId)
        {
            try
            {
                List<ProductFeature> productFeatures = new List<ProductFeature>();
                for (int i = 0; i < featureReplies.Count - 1; i++)
                {
                    var productFeature = await _productFeatureServices
                         .GetAsync(x => x.Product_Id == productId && x.FeatureReply_Id == Convert.ToInt32(featureReplies[i]));
                    productFeatures.Add(productFeature);
                }
                _productFeatureServices.DeleteRange(productFeatures);
                await _productFeatureServices.Save();
                return new JsonResult("ok");
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
        }
        ////تصاویر محصول
        public IActionResult OnGetFileManagerPartialView()
        {
            var fileManager = FileManagerSetting.GetFileManagerModel();

            return Partial("_FileManagerPartialView", fileManager);
        }
        public async Task<IActionResult> OnPostCreateProductGallery(int productId, Int16 galleryId, List<string> images)
        {
            try
            {
                var thisProduct = await _productTranslationServices.GetWithIncludeAsync
                       (
                       selector: x => x,
                       where: x => x.Product_Id == productId && x.Language_Id == CultureInfo.CurrentCulture.Name,
                       include: s => s.Include(x => x.Product)
                       );
                if (galleryId == 0)
                {
                    Gallery productGallery = new Gallery();
                    await _galleryServices.InsertGallery(productGallery);
                    await _galleryServices.Save();
                    await _galleryTranslationServices.Insert(new GalleryTranslation()
                    {
                        Gallery_Id = productGallery.Gallery_ID,
                        GroupTranslation_Id = 1,
                        Title = thisProduct.FirstOrDefault().Title,
                        Slug = thisProduct.FirstOrDefault().Title.ToSlug(),
                        IsActive = true,
                        Language_Id = CultureInfo.CurrentCulture.Name
                    });
                    await _galleryTranslationServices.Save();
                    thisProduct.FirstOrDefault().Product.Gallery_Id = productGallery.Gallery_ID;
                    await CreateGalleryPicture(productGallery.Gallery_ID, images);
                }
                else
                {
                    await DeleteGalleryPicture(galleryId);
                    await CreateGalleryPicture(galleryId, images);
                    thisProduct.FirstOrDefault().Product.Gallery_Id = galleryId;
                }
                _productServices.Update(thisProduct.FirstOrDefault().Product);
                await _productServices.Save();
                return new JsonResult(new { status = "ok", galleryId = thisProduct.FirstOrDefault().Product.Gallery_Id });
            }
            catch (Exception err)
            {

                throw;
            }
        }
        public async Task<IActionResult> OnGetViewAllProductPictures(int productId)
        {
            var thisProduct = await _productServices.GetAsync(x => x.Product_ID == productId);
            var gallery = await _galleryServices.GetAsync(x => x.Gallery_ID == thisProduct.Gallery_Id);
            if (gallery != null)
            {
                var galleryPictures = await _galleryPictureServices.GetAllAsyn(x => x.Gallery_Id == gallery.Gallery_ID);
                return Partial("_ViewAllProductPictures", galleryPictures);
            }
            return new JsonResult("invalid");
        }
        ////محصولات مشابه

        public IActionResult OnGetCreateSimilarProduct()
        {
            return Partial("_CreateSimilarProduct");
        }
        public async Task<IActionResult> OnPostViewAllProductsForSimilarProduct(int productGroupId, int productId)
        {
            try
            {
                var draw = Request.Form["draw"].FirstOrDefault();
                var start = Request.Form["start"].FirstOrDefault();
                var length = Request.Form["length"].FirstOrDefault();
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
                var searchValue = Request.Form["search[value]"].FirstOrDefault();
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                IEnumerable<ProductTranslation> products = null;
                if (productGroupId == 1 || productGroupId == 0)
                    products = await _productTranslationServices.GetWithIncludeAsync
                         (
                         selector: x => x,
                         // شرط پایین بررسی میکند درصورتی که این محصول داخل جدول میدل باشد رو گت کند
                         where: x => x.Product.ProductMiddleGroups.Select(s => s.Product_Id).Contains(x.Product_Id) &&
                             x.Language_Id == CultureInfo.CurrentCulture.Name && x.Product_Id != productId,
                         include: s => s.Include(x => x.Product).ThenInclude(x => x.ProductMiddleGroups)
                         );
                else
                    products = await _productTranslationServices.GetWithIncludeAsync
                  (
                  selector: x => x,
                  where: x => x.Language_Id == CultureInfo.CurrentCulture.Name && x.Product_Id != productId && x.Product.ProductMiddleGroups.Any(s => s.ProductGroup_Id == productGroupId),
                  include: s => s.Include(x => x.Product).ThenInclude(x => x.ProductMiddleGroups)
                  );



                products = products.OrderByDescending(x => x.Product.ProductTranslations.FirstOrDefault().ProductTranslation_ID);
                if (productGroupId == 0)
                    ///برای حذف آیتم های تکراری == Distinct
                    products = products.GroupBy(x => x.Product_Id).Select(x => x.First());

                if (!(string.IsNullOrEmpty(sortColumn)))
                {
                    products = products.AsQueryable().OrderBy(sortColumn + " " + sortColumnDirection);
                }
                if (!string.IsNullOrEmpty(searchValue))
                {
                    products = products.Where(m => m.Title.Contains(searchValue)
                    || m.Product.CreateDate.ToString().Contains(searchValue)
                    );

                }
                recordsTotal = products.Count();
                var data = products.Skip(skip).Take(pageSize).ToList();
                var jsonData = new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data };
                return new JsonResult(jsonData);
            }
            catch (Exception err)
            {
                throw;
            }
        }
        public async Task<IActionResult> OnPostCreateSimilarProduct(int productId, int similarProductId)
        {
            try
            {
                if (productId != 0 & similarProductId != 0)
                {
                    if (!await _similarProductServices.ExistsAsync(x => x.Product_Id == productId && x.ProductSimilar_Id == similarProductId))
                    {
                        await _similarProductServices.Insert(new SimilarProduct()
                        {
                            ID = Guid.NewGuid().ToString(),
                            Product_Id = productId,
                            ProductSimilar_Id = similarProductId
                        });
                        await _similarProductServices.Save();
                        return new JsonResult("ok");
                    }
                    return new JsonResult("exists");
                }
                return new JsonResult("invalid");
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
        }
        public async Task<IActionResult> OnGetViewAllSimilarProducts(int productId)
        {
            var similarProducts = await _similarProductServices.GetWithIncludeAsync(
                selector: x => x,
                where: x => x.Product_Id == productId,
                include: s => s.Include(x => x.Similar_Product).ThenInclude(x => x.ProductTranslations));

            return Partial("_ViewAllSimilarProducts", similarProducts);
        }
        public async Task<IActionResult> OnGetDeleteSimilarProducts(List<string> similarProducts, int productId)
        {
            try
            {
                List<SimilarProduct> similarProductList = new List<SimilarProduct>();
                for (int i = 0; i < similarProducts.Count - 1; i++)
                {
                    var similarProduct = await _similarProductServices
                         .GetAsync(x => x.Product_Id == productId && x.ProductSimilar_Id == Convert.ToInt32(similarProducts[i]));
                    similarProductList.Add(similarProduct);
                }
                _similarProductServices.DeleteAll(similarProductList);
                await _similarProductServices.Save();
                return new JsonResult("ok");
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
        }

        ////محصولات پیشنهادی
        public async Task<IActionResult> OnPostCreateSuggestProduct(int productId, int suggestProductId)
        {
            try
            {
                if (productId != 0 & suggestProductId != 0)
                {
                    if (!await _suggestProductServices.ExistsAsync(x => x.Product_Id == productId && x.ProductSuggest_Id == suggestProductId))
                    {
                        await _suggestProductServices.Insert(new SuggestProduct()
                        {
                            ID = Guid.NewGuid().ToString(),
                            Product_Id = productId,
                            ProductSuggest_Id = suggestProductId
                        });
                        await _suggestProductServices.Save();
                        return new JsonResult("ok");
                    }
                    return new JsonResult("exists");
                }
                return new JsonResult("invalid");
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
        }
        public async Task<IActionResult> OnGetViewAllSuggestProducts(int productId)
        {
            var suggestProducts = await _suggestProductServices.GetWithIncludeAsync(
                selector: x => x,
                where: x => x.Product_Id == productId,
                include: s => s.Include(x => x.Suggest_Product).ThenInclude(x => x.ProductTranslations));

            return Partial("_ViewAllSuggestProducts", suggestProducts);
        }
        public async Task<IActionResult> OnGetDeleteSuggestProducts(List<string> suggestProducts, int productId)
        {
            try
            {
                List<SuggestProduct> suggestProductList = new List<SuggestProduct>();
                for (int i = 0; i < suggestProducts.Count - 1; i++)
                {
                    var suggestProduct = await _suggestProductServices
                         .GetAsync(x => x.Product_Id == productId && x.ProductSuggest_Id == Convert.ToInt32(suggestProducts[i]));
                    suggestProductList.Add(suggestProduct);
                }
                _suggestProductServices.DeleteAll(suggestProductList);
                await _suggestProductServices.Save();
                return new JsonResult("ok");
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
        }

        //// متد های داخلی
        private async Task CreateGalleryPicture(Int16 galleryId, List<string> images)
        {
            string directory = Path.GetDirectoryName(images[0]);
            string thumbDirectory = Path.Combine(directory, "Thumb");
            string mediumDirectory = Path.Combine(directory, "Medium");

            FileManipulate.CreateDirectory("", _hostEnvironment.WebRootPath, thumbDirectory, mediumDirectory);

            var gallery = await _galleryTranslationServices.GetWithIncludeAsync(
                selector: x => x,
                where: x => x.Gallery_Id == galleryId,
                include: s => s.Include(x => x.GalleryGroupTranslation).ThenInclude(x => x.GalleryGroup)
                );

            int smallPicWidth = int.Parse(gallery.FirstOrDefault().GalleryGroupTranslation.GalleryGroup.SmallPicWidth);
            int smallPicHeight = int.Parse(gallery.FirstOrDefault().GalleryGroupTranslation.GalleryGroup.SmallPicHeight);
            int mediumPicWidth = int.Parse(gallery.FirstOrDefault().GalleryGroupTranslation.GalleryGroup.MediumPicWidth);
            int mediumPicHeight = int.Parse(gallery.FirstOrDefault().GalleryGroupTranslation.GalleryGroup.MediumPicHeight);

            GalleryPicture galleryPicture = new GalleryPicture();
            for (int i = 0; i < images.Count; i++)
            {


                /// Item1 :Orginal Item2:Medium Item3:Thumb
                Tuple<string, string, string> fileOfFilePaths = FileManipulate.CreateNewFileNameWithSizeInfo(images[i], smallPicWidth, smallPicHeight);


                string orginalFullFilePath = Path.Combine(_hostEnvironment.WebRootPath, fileOfFilePaths.Item1);
                string mediumFullFilePath = Path.Combine(_hostEnvironment.WebRootPath, fileOfFilePaths.Item2);
                string thumbFullFilePath = Path.Combine(_hostEnvironment.WebRootPath, fileOfFilePaths.Item3);

                string fileName = Path.GetFileName(images[i]);
                if (!await _galleryPictureServices.ExistsAsync(x => x.Image == images[i].Trim() && x.Gallery_Id == galleryId))
                {
                    galleryPicture.GalleryPic_ID = Guid.NewGuid().ToString();
                    galleryPicture.Gallery_Id = galleryId;
                    galleryPicture.Image = images[i].Trim().ToString();
                    await _galleryPictureServices.Insert(galleryPicture);
                    await _galleryPictureServices.Save();
                }

                if (!System.IO.File.Exists(thumbFullFilePath))
                    FileManipulate.resizeImage2(orginalFullFilePath, thumbFullFilePath, smallPicWidth, smallPicHeight);

                if (!System.IO.File.Exists(mediumFullFilePath))
                    FileManipulate.resizeImage2(orginalFullFilePath, mediumFullFilePath, mediumPicWidth, mediumPicHeight);

            }
            await Task.CompletedTask;
        }
        private async Task DeleteGalleryPicture(Int16 galleryId)
        {
            var galleryPics = await _galleryPictureServices.GetAllAsyn(x => x.Gallery_Id == galleryId);
            var gallery = await _galleryTranslationServices.GetWithIncludeAsync(
                 selector: x => x,
                 where: x => x.Gallery_Id == galleryId,
                 include: s => s.Include(x => x.GalleryGroupTranslation).ThenInclude(x => x.GalleryGroup)
                 );

            int smallPicWidth = int.Parse(gallery.FirstOrDefault().GalleryGroupTranslation.GalleryGroup.SmallPicWidth);
            int smallPicHeight = int.Parse(gallery.FirstOrDefault().GalleryGroupTranslation.GalleryGroup.SmallPicHeight);
            int mediumPicWidth = int.Parse(gallery.FirstOrDefault().GalleryGroupTranslation.GalleryGroup.MediumPicWidth);
            int mediumPicHeight = int.Parse(gallery.FirstOrDefault().GalleryGroupTranslation.GalleryGroup.MediumPicHeight);

            foreach (var galleryPicture in galleryPics)
            {
                //var galleryPic = await _galleryPictureServices.GetAsync(x => x.GalleryPic_ID == splitedValues[i]);

                if (galleryPicture != null)
                {
                    /// Item1 :Orginal Item2:Medium Item3:Thumb
                    Tuple<string, string, string> fileOfFilePaths = FileManipulate.CreateNewFileNameWithSizeInfo(galleryPicture.Image, smallPicWidth, smallPicHeight);
                    string thumbFullFilePath = Path.Combine(_hostEnvironment.WebRootPath, fileOfFilePaths.Item3);
                    string mediumFullFilePath = Path.Combine(_hostEnvironment.WebRootPath, fileOfFilePaths.Item2);

                    RemoveFile(thumbFullFilePath, mediumFullFilePath);
                    _galleryPictureServices.DeleteAsync(galleryPicture);
                    await _galleryPictureServices.Save();
                }
            }
            await Task.CompletedTask;
        }
        private void RemoveFile(string thumbFilePath, string mediumFilePath)
        {
            FileManipulate.DeleteFile("", thumbFilePath, "");
            FileManipulate.DeleteFile("", mediumFilePath, "");
        }
        public async Task<List<int>> GetAllParents(int CGId)
        {


            var groups = await _productGroupServices.GetAllAsyn(x => x.ProductGroup_ID == CGId);
            //await _productGroupTranslationSrevices.GetWithIncludeAsync
            //  (
            //  selector: x => x,
            //  where: x => x.ProductGroup_Id == CGId && x.Language_Id == LangId,
            //  include: x => x.Include(s => s.ProductGroup)
            //  );
            foreach (var item in groups)
            {
                if (item.ParentId != null)
                {
                    //if (Ids.IndexOf(item.ContentGroupTranslation_ID) == -1) { }
                    Ids.Add(item.ProductGroup_ID);
                    await GetAllParents(item.ParentId.Value);
                }
                else
                {
                }
            }
            return Ids;
        }


    }
}
