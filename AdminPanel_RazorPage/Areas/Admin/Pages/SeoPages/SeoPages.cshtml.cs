using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Application.Interfaces;

using Domain.DomainClasses.Seo;
using System.Linq.Dynamic.Core;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace ItisaCMS_RazorPage.Areas.Admin.Pages.SeoPages
{
    public class SeoPagesModel : PageModel
    {
        private ISeoPageServices _seoPageServices;
        public SeoPagesModel(ISeoPageServices seoPageServices)
        {
            _seoPageServices = seoPageServices;
        }

        #region SeoPage
        public IActionResult OnGetCreateSeoPage()
        {
            return Partial("_CreateSeoPage");
        }
        public async Task<IActionResult> OnPostCreateSeoPage(SeoPage seoPage)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    await _seoPageServices.Insert(seoPage);
                    await _seoPageServices.Save();
                    return new JsonResult("ok");
                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
        }

        #region ViewAllSeoPages
        public async Task<IActionResult> OnPostViewAllSeoPages()
        {
            try
            {
                var draw = Request.Form["draw"].FirstOrDefault();
                var start = Request.Form["start"].FirstOrDefault();
                var length = Request.Form["length"].FirstOrDefault();
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
                var searchValue = Request.Form["search[value]"].FirstOrDefault();
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                var seoPages = await _seoPageServices.GetAllAsyn();
                if (!(string.IsNullOrEmpty(sortColumn)))
                {
                    seoPages = seoPages.AsQueryable().OrderBy(sortColumn + " " + sortColumnDirection);
                }
                if (!string.IsNullOrEmpty(searchValue))
                {
                    seoPages = seoPages.Where(m => m.PageUrl.Contains(searchValue));

                }
                recordsTotal = seoPages.Count();
                var data = seoPages.Skip(skip).Take(pageSize).ToList();
                var jsonData = new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data };
                return new JsonResult(jsonData);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        public async Task<IActionResult> OnGetEditSeoPage(int id)
        {
            try
            {
                var seoPage = await _seoPageServices
                    .GetAsync(x => x.SeoPage_ID == (Int16)id);
                return Partial("_EditSeoPage", seoPage);
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
        }
        public async Task<IActionResult> OnPostEditSeoPage(SeoPage seoPage)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    await _seoPageServices.Update(seoPage);
                    await _seoPageServices.Save();
                    return new JsonResult("ok");
                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
        }

        public async Task<IActionResult> OnGetDeleteSeoPage(int id)
        {
            try
            {
                var seoPage = await _seoPageServices
                    .GetAsync(x => x.SeoPage_ID == (Int16)id);
                if (seoPage != null)
                {
                    //TODO : exists in gallery
                    _seoPageServices.DeleteAsync(seoPage);
                    await _seoPageServices.Save();
                    return new JsonResult("ok");

                    // return new JsonResult("exists");
                }
                return new JsonResult("nok");
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
        }

        #endregion
    }
}
