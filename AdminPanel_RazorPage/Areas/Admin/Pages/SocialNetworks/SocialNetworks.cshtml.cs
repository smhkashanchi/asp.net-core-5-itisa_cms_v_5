using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

using Application.Interfaces;

using Domain.DomainClasses.SocialNetwork;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;

namespace ItisaCMS_RazorPage.Areas.Admin.Pages.SocialNetworks
{
    public class SocialNetworksModel : PageModel
    {
        private ISocialNetworkServices _socialNetworkServices;
        private ISocialNetworkTranslationServices _socialNetworkTranslationServices;
        public SocialNetworksModel(ISocialNetworkServices socialNetworkServices,ISocialNetworkTranslationServices socialNetworkTranslationServices)
        {
            _socialNetworkServices = socialNetworkServices;
            _socialNetworkTranslationServices = socialNetworkTranslationServices;
        }
        #region SocialNetwork
        public IActionResult OnGetCreateSocialNetwork()
        {
            try
            {
                //var socialNetworkTypes = EnumSocialNetworkType.GetValues<SocialNetworkType>();
                //TempData["SocialNetworkTypes"] = socialNetworkTypes;
                return Partial("_CreateSocialNetwork");
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
        }
        public async Task<IActionResult> OnPostCreateSocialNetwork(SocialNetwork socialNetwork)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string socialNetworkType = GetEnumValue(socialNetwork.Type);
                    socialNetwork.Type = socialNetworkType;
                    await _socialNetworkServices.Insert(socialNetwork);
                    await _socialNetworkServices.Save();
                    return new JsonResult(new { status = "ok", socialNetworkId = socialNetwork.SocialNetwork_ID });
                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
        }
        public IActionResult OnGetCreateSocialNetworkTrans(int id)
        {
            TempData["SocialNetworkId"] = id;
            return Partial("_CreateSocialNetworkTrans");
        }
        public async Task<IActionResult> OnPostCreateSocialNetworkTrans(SocialNetworkTranslation socialNetworkTranslation)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (!await _socialNetworkTranslationServices
                        .ExistsAsync(x => x.Language_Id == socialNetworkTranslation.Language_Id && x.SocialNetwork_Id == socialNetworkTranslation.SocialNetwork_Id))
                    {
                        await _socialNetworkTranslationServices.Insert(socialNetworkTranslation);
                        await _socialNetworkTranslationServices.Save();
                        return new JsonResult("ok");
                    }
                    return new JsonResult("exsist");

                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
        }

        #region ViewAllSocialNetworks
        public async Task<IActionResult> OnPostViewAllSocialNetworks()
        {
            try
            {
                var draw = Request.Form["draw"].FirstOrDefault();
                var start = Request.Form["start"].FirstOrDefault();
                var length = Request.Form["length"].FirstOrDefault();
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
                var searchValue = Request.Form["search[value]"].FirstOrDefault();
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                var socialNetworks = await _socialNetworkTranslationServices.GetWithIncludeAsync<SocialNetworkTranslation>
                (
                selector: x => x,
                include: s => s.Include(x => x.SocialNetwork),
                where: x => x.Language_Id == CultureInfo.CurrentCulture.Name
                );
                if (!(string.IsNullOrEmpty(sortColumn)))
                {
                    socialNetworks = socialNetworks.AsQueryable().OrderBy(sortColumn + " " + sortColumnDirection);
                }
                if (!string.IsNullOrEmpty(searchValue))
                {
                    socialNetworks = socialNetworks.Where(m => m.Title.Contains(searchValue) ||
                    m.SocialNetwork.Link.Contains(searchValue));

                }
                recordsTotal = socialNetworks.Count();
                var data = socialNetworks.Skip(skip).Take(pageSize).ToList();
                var jsonData = new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data };
                return new JsonResult(jsonData);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        public async Task<IActionResult> OnGetEditSocialNetwork(int id)
        {
            try
            {
                var socialNetwork = await _socialNetworkServices.GetAsync(x => x.SocialNetwork_ID == (Int16)id);

                return Partial("_EditSocialNetwork", socialNetwork);
            }
            catch (Exception err)
            {
                return new JsonResult(new
                {
                    status = "error",
                    data = err.Message + " *** " + err.InnerException
                });
            }
        }
        public async Task<IActionResult> OnGetEditSocialNetworkTrans(int id)
        {
            var socialNetworkTrans = await _socialNetworkTranslationServices.GetAsync(x => x.SocialNetworkTranslation_ID == id);

            return Partial("_EditSocialNetworkTrans", socialNetworkTrans);
        }
        public async Task<IActionResult> OnGetSocialNetworkInfo(string id, int socialNetworkId)
        {

            string languageId = string.IsNullOrEmpty(id) ? CultureInfo.CurrentCulture.Name : id.Trim();
            var socialNetworkIdTrans = await _socialNetworkTranslationServices
                .GetAsync(x => x.Language_Id == languageId && x.SocialNetwork_Id == socialNetworkId);
            if (socialNetworkIdTrans != null)
            {
                return new JsonResult(new
                {
                    status = "ok",
                    title = socialNetworkIdTrans.Title,
                    isActive = socialNetworkIdTrans.IsActive,
                    id = socialNetworkIdTrans.SocialNetworkTranslation_ID
                });
            }

            return new JsonResult("nok");
        }
        public async Task<IActionResult> OnPostEditSocialNetwork(SocialNetwork socialNetwork)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string socialNetworkType = GetEnumValue(socialNetwork.Type);
                    socialNetwork.Type = socialNetworkType;
                    await _socialNetworkServices.Update(socialNetwork);
                    await _socialNetworkServices.Save();
                    return new JsonResult("ok");
                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new
                {
                    status = "error",
                    data = err.Message + " *** " + err.InnerException
                });
            }
        }
        public async Task<IActionResult> OnPostEditSocialNetworkTrans(SocialNetworkTranslation socialNetworkTranslation)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    await _socialNetworkTranslationServices.Update(socialNetworkTranslation);
                    await _socialNetworkTranslationServices.Save();
                    return new JsonResult("ok");
                }
                return Page();
            }
            catch (Exception err)
            {
                return new JsonResult(new
                {
                    status = "error",
                    data = err.Message + " *** " + err.InnerException
                });
            }
        }

        public async Task<IActionResult> OnGetDeleteSocialNetworkTrans(int id, int socialNetworkId, string status)
        {
            try
            {
                var socialNetworkIdTransList = await _socialNetworkTranslationServices.GetWithIncludeAsync
                        (selector: x => x,
                        where: x => x.SocialNetwork_Id == socialNetworkId);
                if (status == "one")
                {
                    
                    if (socialNetworkIdTransList.Count() > 1)
                    {
                        var socialNetworkTrans = await _socialNetworkTranslationServices.GetAsync(x => x.SocialNetworkTranslation_ID == id);
                        _socialNetworkTranslationServices.DeleteAsync(socialNetworkTrans);
                        await _socialNetworkTranslationServices.Save();
                    }
                    else
                    {
                        var socialNetworkTrans = await _socialNetworkTranslationServices.GetAsync(x => x.SocialNetworkTranslation_ID == id);
                        _socialNetworkTranslationServices.DeleteAsync(socialNetworkTrans);
                        await _socialNetworkTranslationServices.Save();

                        var socialNetwork = await _socialNetworkServices.GetAsync(x => x.SocialNetwork_ID == socialNetworkId);
                        _socialNetworkServices.DeleteAsync(socialNetwork);
                        await _socialNetworkServices.Save();
                    }
                    return new JsonResult("ok");
                }
                else if (status == "all")
                {
                    _socialNetworkTranslationServices.DeleteAllAsync(socialNetworkIdTransList);
                    await _socialNetworkTranslationServices.Save();

                    var socialNetwork = await _socialNetworkServices.GetAsync(x => x.SocialNetwork_ID == socialNetworkId);
                    _socialNetworkServices.DeleteAsync(socialNetwork);
                    await _socialNetworkServices.Save();
                    return new JsonResult("ok");
                }
            }
            catch (Exception err)
            {
                return new JsonResult(new { status = "error", data = err.Message + " *** " + err.InnerException });
            }
            return new JsonResult("nok");
        }


        #endregion

        #region Enum
        public string GetEnumValue(string connectionTypes)
        {
            string connectionType = "";
            switch (connectionTypes)
            {
                case "0":
                    connectionType = SocialNetworkType.Telegram.ToString();
                    break;
                case "1":
                    connectionType = SocialNetworkType.Instagram.ToString();
                    break;
                case "2":
                    connectionType = SocialNetworkType.Twitter.ToString();
                    break;
                case "3":
                    connectionType = SocialNetworkType.Linekdin.ToString();
                    break;
                case "4":
                    connectionType = SocialNetworkType.Facebook.ToString();
                    break;
            }
            return connectionType;
        }
        #endregion
    }
}
