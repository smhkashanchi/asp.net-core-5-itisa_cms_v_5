﻿using Application.ViewModels;

using AutoMapper;

using Domain.DomainClasses.Content;

using System;
using System.Collections.Generic;
using System.Text;

namespace ItisaCMS_RazorPage
{
    public class MappingProfile:Profile
    {

        public MappingProfile()
        {
            CreateMap<ContentTranslation, ContentStatisticsVM>();
        }
    }
}
