#pragma checksum "D:\DotNetCore\AdminPanel_991220\AdminPanel_RazorPage\AdminPanel_RazorPage\Areas\Admin\Pages\Products\_EditProductSetting.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "c9c51b1ad1835940b3419ead27ceae39385efe24"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(ItisaCMS_RazorPage.Pages.Products.Areas_Admin_Pages_Products__EditProductSetting), @"mvc.1.0.view", @"/Areas/Admin/Pages/Products/_EditProductSetting.cshtml")]
namespace ItisaCMS_RazorPage.Pages.Products
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "D:\DotNetCore\AdminPanel_991220\AdminPanel_RazorPage\AdminPanel_RazorPage\Areas\Admin\Pages\_ViewImports.cshtml"
using Microsoft.AspNetCore.Identity;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "D:\DotNetCore\AdminPanel_991220\AdminPanel_RazorPage\AdminPanel_RazorPage\Areas\Admin\Pages\_ViewImports.cshtml"
using ItisaCMS_RazorPage;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "D:\DotNetCore\AdminPanel_991220\AdminPanel_RazorPage\AdminPanel_RazorPage\Areas\Admin\Pages\_ViewImports.cshtml"
using Data;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "D:\DotNetCore\AdminPanel_991220\AdminPanel_RazorPage\AdminPanel_RazorPage\Areas\Admin\Pages\_ViewImports.cshtml"
using Domain.DomainClasses;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "D:\DotNetCore\AdminPanel_991220\AdminPanel_RazorPage\AdminPanel_RazorPage\Areas\Admin\Pages\_ViewImports.cshtml"
using Domain.DomainClasses.Color;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "D:\DotNetCore\AdminPanel_991220\AdminPanel_RazorPage\AdminPanel_RazorPage\Areas\Admin\Pages\_ViewImports.cshtml"
using Domain.DomainClasses.Content;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "D:\DotNetCore\AdminPanel_991220\AdminPanel_RazorPage\AdminPanel_RazorPage\Areas\Admin\Pages\_ViewImports.cshtml"
using Domain.DomainClasses.Connection;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "D:\DotNetCore\AdminPanel_991220\AdminPanel_RazorPage\AdminPanel_RazorPage\Areas\Admin\Pages\_ViewImports.cshtml"
using Domain.DomainClasses.Gallery;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "D:\DotNetCore\AdminPanel_991220\AdminPanel_RazorPage\AdminPanel_RazorPage\Areas\Admin\Pages\_ViewImports.cshtml"
using Domain.DomainClasses.SocialNetwork;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "D:\DotNetCore\AdminPanel_991220\AdminPanel_RazorPage\AdminPanel_RazorPage\Areas\Admin\Pages\_ViewImports.cshtml"
using Domain.DomainClasses.SlideShow;

#line default
#line hidden
#nullable disable
#nullable restore
#line 11 "D:\DotNetCore\AdminPanel_991220\AdminPanel_RazorPage\AdminPanel_RazorPage\Areas\Admin\Pages\_ViewImports.cshtml"
using Domain.DomainClasses.Seo;

#line default
#line hidden
#nullable disable
#nullable restore
#line 12 "D:\DotNetCore\AdminPanel_991220\AdminPanel_RazorPage\AdminPanel_RazorPage\Areas\Admin\Pages\_ViewImports.cshtml"
using Domain.DomainClasses.SendType;

#line default
#line hidden
#nullable disable
#nullable restore
#line 13 "D:\DotNetCore\AdminPanel_991220\AdminPanel_RazorPage\AdminPanel_RazorPage\Areas\Admin\Pages\_ViewImports.cshtml"
using Domain.DomainClasses.CoWorker;

#line default
#line hidden
#nullable disable
#nullable restore
#line 14 "D:\DotNetCore\AdminPanel_991220\AdminPanel_RazorPage\AdminPanel_RazorPage\Areas\Admin\Pages\_ViewImports.cshtml"
using Domain.DomainClasses.FilesManage;

#line default
#line hidden
#nullable disable
#nullable restore
#line 15 "D:\DotNetCore\AdminPanel_991220\AdminPanel_RazorPage\AdminPanel_RazorPage\Areas\Admin\Pages\_ViewImports.cshtml"
using Domain.DomainClasses.Region;

#line default
#line hidden
#nullable disable
#nullable restore
#line 16 "D:\DotNetCore\AdminPanel_991220\AdminPanel_RazorPage\AdminPanel_RazorPage\Areas\Admin\Pages\_ViewImports.cshtml"
using Domain.DomainClasses.Product;

#line default
#line hidden
#nullable disable
#nullable restore
#line 17 "D:\DotNetCore\AdminPanel_991220\AdminPanel_RazorPage\AdminPanel_RazorPage\Areas\Admin\Pages\_ViewImports.cshtml"
using Domain.DomainClasses.FAQ;

#line default
#line hidden
#nullable disable
#nullable restore
#line 18 "D:\DotNetCore\AdminPanel_991220\AdminPanel_RazorPage\AdminPanel_RazorPage\Areas\Admin\Pages\_ViewImports.cshtml"
using Domain.DomainClasses.Feature;

#line default
#line hidden
#nullable disable
#nullable restore
#line 19 "D:\DotNetCore\AdminPanel_991220\AdminPanel_RazorPage\AdminPanel_RazorPage\Areas\Admin\Pages\_ViewImports.cshtml"
using ItisaCMS_RazorPage.Areas.Admin.Pages;

#line default
#line hidden
#nullable disable
#nullable restore
#line 20 "D:\DotNetCore\AdminPanel_991220\AdminPanel_RazorPage\AdminPanel_RazorPage\Areas\Admin\Pages\_ViewImports.cshtml"
using Application.Utilities;

#line default
#line hidden
#nullable disable
#nullable restore
#line 21 "D:\DotNetCore\AdminPanel_991220\AdminPanel_RazorPage\AdminPanel_RazorPage\Areas\Admin\Pages\_ViewImports.cshtml"
using Application.ViewModels;

#line default
#line hidden
#nullable disable
#nullable restore
#line 22 "D:\DotNetCore\AdminPanel_991220\AdminPanel_RazorPage\AdminPanel_RazorPage\Areas\Admin\Pages\_ViewImports.cshtml"
using System.Globalization;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"c9c51b1ad1835940b3419ead27ceae39385efe24", @"/Areas/Admin/Pages/Products/_EditProductSetting.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"807a1580c4a9630e6d5dba11f3bc30cb01462509", @"/Areas/Admin/Pages/_ViewImports.cshtml")]
    public class Areas_Admin_Pages_Products__EditProductSetting : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<Product>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("control-label"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("type", "text", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("form-control"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("text-danger"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_4 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("type", "checkbox", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_5 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("iCheck"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.LabelTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_LabelTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.InputTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.ValidationMessageTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_ValidationMessageTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n<div class=\"card-body\">\r\n    <div class=\"row\">\r\n        <div class=\"col-md-3\">\r\n            ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("label", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "c9c51b1ad1835940b3419ead27ceae39385efe2410305", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_LabelTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.LabelTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_LabelTagHelper);
#nullable restore
#line 6 "D:\DotNetCore\AdminPanel_991220\AdminPanel_RazorPage\AdminPanel_RazorPage\Areas\Admin\Pages\Products\_EditProductSetting.cshtml"
__Microsoft_AspNetCore_Mvc_TagHelpers_LabelTagHelper.For = ModelExpressionProvider.CreateModelExpression(ViewData, __model => __model.Code);

#line default
#line hidden
#nullable disable
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-for", __Microsoft_AspNetCore_Mvc_TagHelpers_LabelTagHelper.For, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n            ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("input", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagOnly, "c9c51b1ad1835940b3419ead27ceae39385efe2411881", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.InputTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.InputTypeName = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
#nullable restore
#line 7 "D:\DotNetCore\AdminPanel_991220\AdminPanel_RazorPage\AdminPanel_RazorPage\Areas\Admin\Pages\Products\_EditProductSetting.cshtml"
__Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.For = ModelExpressionProvider.CreateModelExpression(ViewData, __model => __model.Code);

#line default
#line hidden
#nullable disable
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-for", __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.For, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n            ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("span", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "c9c51b1ad1835940b3419ead27ceae39385efe2413660", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_ValidationMessageTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.ValidationMessageTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_ValidationMessageTagHelper);
#nullable restore
#line 8 "D:\DotNetCore\AdminPanel_991220\AdminPanel_RazorPage\AdminPanel_RazorPage\Areas\Admin\Pages\Products\_EditProductSetting.cshtml"
__Microsoft_AspNetCore_Mvc_TagHelpers_ValidationMessageTagHelper.For = ModelExpressionProvider.CreateModelExpression(ViewData, __model => __model.Code);

#line default
#line hidden
#nullable disable
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-validation-for", __Microsoft_AspNetCore_Mvc_TagHelpers_ValidationMessageTagHelper.For, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_3);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n\r\n        </div>\r\n        <div class=\"col-md-3 pt-4-2\">\r\n            <label>\r\n                ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("input", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "c9c51b1ad1835940b3419ead27ceae39385efe2415398", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.InputTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper);
#nullable restore
#line 13 "D:\DotNetCore\AdminPanel_991220\AdminPanel_RazorPage\AdminPanel_RazorPage\Areas\Admin\Pages\Products\_EditProductSetting.cshtml"
__Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.For = ModelExpressionProvider.CreateModelExpression(ViewData, __model => __model.IsExist);

#line default
#line hidden
#nullable disable
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-for", __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.For, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.InputTypeName = (string)__tagHelperAttribute_4.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_4);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_5);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral(" ");
#nullable restore
#line 13 "D:\DotNetCore\AdminPanel_991220\AdminPanel_RazorPage\AdminPanel_RazorPage\Areas\Admin\Pages\Products\_EditProductSetting.cshtml"
                                                                     Write(Html.DisplayNameFor(model => model.IsExist));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n            </label>\r\n        </div>\r\n        <div class=\"col-md-3 pt-4-2\">\r\n            <label>\r\n                ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("input", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "c9c51b1ad1835940b3419ead27ceae39385efe2417655", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.InputTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper);
#nullable restore
#line 18 "D:\DotNetCore\AdminPanel_991220\AdminPanel_RazorPage\AdminPanel_RazorPage\Areas\Admin\Pages\Products\_EditProductSetting.cshtml"
__Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.For = ModelExpressionProvider.CreateModelExpression(ViewData, __model => __model.IsPreview);

#line default
#line hidden
#nullable disable
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-for", __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.For, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.InputTypeName = (string)__tagHelperAttribute_4.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_4);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_5);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral(" ");
#nullable restore
#line 18 "D:\DotNetCore\AdminPanel_991220\AdminPanel_RazorPage\AdminPanel_RazorPage\Areas\Admin\Pages\Products\_EditProductSetting.cshtml"
                                                                        Write(Html.DisplayNameFor(model => model.IsPreview));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n            </label>\r\n        </div>\r\n        <div class=\"col-md-3 pt-4-2\">\r\n            <label>\r\n                ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("input", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "c9c51b1ad1835940b3419ead27ceae39385efe2419919", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.InputTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper);
#nullable restore
#line 23 "D:\DotNetCore\AdminPanel_991220\AdminPanel_RazorPage\AdminPanel_RazorPage\Areas\Admin\Pages\Products\_EditProductSetting.cshtml"
__Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.For = ModelExpressionProvider.CreateModelExpression(ViewData, __model => __model.BestSelling);

#line default
#line hidden
#nullable disable
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-for", __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.For, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.InputTypeName = (string)__tagHelperAttribute_4.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_4);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_5);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral(" ");
#nullable restore
#line 23 "D:\DotNetCore\AdminPanel_991220\AdminPanel_RazorPage\AdminPanel_RazorPage\Areas\Admin\Pages\Products\_EditProductSetting.cshtml"
                                                                          Write(Html.DisplayNameFor(model => model.BestSelling));

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
            </label>
        </div>
    </div>
</div>

<script>
    $(function () {
        $('.iCheck').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-red',
            increaseArea: '20%' // optional
        });
    });
</script>
");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<Product> Html { get; private set; }
    }
}
#pragma warning restore 1591
