#pragma checksum "D:\DotNetCore\AdminPanel_991220\AdminPanel_RazorPage\AdminPanel_RazorPage\Areas\Admin\Pages\Products\_CreateFeaturesForProductGroup.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "c076269d1cb5a9cf1552174c8f86f4f8e7605eb4"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(ItisaCMS_RazorPage.Pages.Products.Areas_Admin_Pages_Products__CreateFeaturesForProductGroup), @"mvc.1.0.view", @"/Areas/Admin/Pages/Products/_CreateFeaturesForProductGroup.cshtml")]
namespace ItisaCMS_RazorPage.Pages.Products
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "D:\DotNetCore\AdminPanel_991220\AdminPanel_RazorPage\AdminPanel_RazorPage\Areas\Admin\Pages\_ViewImports.cshtml"
using Microsoft.AspNetCore.Identity;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "D:\DotNetCore\AdminPanel_991220\AdminPanel_RazorPage\AdminPanel_RazorPage\Areas\Admin\Pages\_ViewImports.cshtml"
using ItisaCMS_RazorPage;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "D:\DotNetCore\AdminPanel_991220\AdminPanel_RazorPage\AdminPanel_RazorPage\Areas\Admin\Pages\_ViewImports.cshtml"
using Data;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "D:\DotNetCore\AdminPanel_991220\AdminPanel_RazorPage\AdminPanel_RazorPage\Areas\Admin\Pages\_ViewImports.cshtml"
using Domain.DomainClasses;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "D:\DotNetCore\AdminPanel_991220\AdminPanel_RazorPage\AdminPanel_RazorPage\Areas\Admin\Pages\_ViewImports.cshtml"
using Domain.DomainClasses.Color;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "D:\DotNetCore\AdminPanel_991220\AdminPanel_RazorPage\AdminPanel_RazorPage\Areas\Admin\Pages\_ViewImports.cshtml"
using Domain.DomainClasses.Content;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "D:\DotNetCore\AdminPanel_991220\AdminPanel_RazorPage\AdminPanel_RazorPage\Areas\Admin\Pages\_ViewImports.cshtml"
using Domain.DomainClasses.Connection;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "D:\DotNetCore\AdminPanel_991220\AdminPanel_RazorPage\AdminPanel_RazorPage\Areas\Admin\Pages\_ViewImports.cshtml"
using Domain.DomainClasses.Gallery;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "D:\DotNetCore\AdminPanel_991220\AdminPanel_RazorPage\AdminPanel_RazorPage\Areas\Admin\Pages\_ViewImports.cshtml"
using Domain.DomainClasses.SocialNetwork;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "D:\DotNetCore\AdminPanel_991220\AdminPanel_RazorPage\AdminPanel_RazorPage\Areas\Admin\Pages\_ViewImports.cshtml"
using Domain.DomainClasses.SlideShow;

#line default
#line hidden
#nullable disable
#nullable restore
#line 11 "D:\DotNetCore\AdminPanel_991220\AdminPanel_RazorPage\AdminPanel_RazorPage\Areas\Admin\Pages\_ViewImports.cshtml"
using Domain.DomainClasses.Seo;

#line default
#line hidden
#nullable disable
#nullable restore
#line 12 "D:\DotNetCore\AdminPanel_991220\AdminPanel_RazorPage\AdminPanel_RazorPage\Areas\Admin\Pages\_ViewImports.cshtml"
using Domain.DomainClasses.SendType;

#line default
#line hidden
#nullable disable
#nullable restore
#line 13 "D:\DotNetCore\AdminPanel_991220\AdminPanel_RazorPage\AdminPanel_RazorPage\Areas\Admin\Pages\_ViewImports.cshtml"
using Domain.DomainClasses.CoWorker;

#line default
#line hidden
#nullable disable
#nullable restore
#line 14 "D:\DotNetCore\AdminPanel_991220\AdminPanel_RazorPage\AdminPanel_RazorPage\Areas\Admin\Pages\_ViewImports.cshtml"
using Domain.DomainClasses.FilesManage;

#line default
#line hidden
#nullable disable
#nullable restore
#line 15 "D:\DotNetCore\AdminPanel_991220\AdminPanel_RazorPage\AdminPanel_RazorPage\Areas\Admin\Pages\_ViewImports.cshtml"
using Domain.DomainClasses.Region;

#line default
#line hidden
#nullable disable
#nullable restore
#line 16 "D:\DotNetCore\AdminPanel_991220\AdminPanel_RazorPage\AdminPanel_RazorPage\Areas\Admin\Pages\_ViewImports.cshtml"
using Domain.DomainClasses.Product;

#line default
#line hidden
#nullable disable
#nullable restore
#line 17 "D:\DotNetCore\AdminPanel_991220\AdminPanel_RazorPage\AdminPanel_RazorPage\Areas\Admin\Pages\_ViewImports.cshtml"
using Domain.DomainClasses.FAQ;

#line default
#line hidden
#nullable disable
#nullable restore
#line 18 "D:\DotNetCore\AdminPanel_991220\AdminPanel_RazorPage\AdminPanel_RazorPage\Areas\Admin\Pages\_ViewImports.cshtml"
using Domain.DomainClasses.Feature;

#line default
#line hidden
#nullable disable
#nullable restore
#line 19 "D:\DotNetCore\AdminPanel_991220\AdminPanel_RazorPage\AdminPanel_RazorPage\Areas\Admin\Pages\_ViewImports.cshtml"
using ItisaCMS_RazorPage.Areas.Admin.Pages;

#line default
#line hidden
#nullable disable
#nullable restore
#line 20 "D:\DotNetCore\AdminPanel_991220\AdminPanel_RazorPage\AdminPanel_RazorPage\Areas\Admin\Pages\_ViewImports.cshtml"
using Application.Utilities;

#line default
#line hidden
#nullable disable
#nullable restore
#line 21 "D:\DotNetCore\AdminPanel_991220\AdminPanel_RazorPage\AdminPanel_RazorPage\Areas\Admin\Pages\_ViewImports.cshtml"
using Application.ViewModels;

#line default
#line hidden
#nullable disable
#nullable restore
#line 22 "D:\DotNetCore\AdminPanel_991220\AdminPanel_RazorPage\AdminPanel_RazorPage\Areas\Admin\Pages\_ViewImports.cshtml"
using System.Globalization;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"c076269d1cb5a9cf1552174c8f86f4f8e7605eb4", @"/Areas/Admin/Pages/Products/_CreateFeaturesForProductGroup.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"807a1580c4a9630e6d5dba11f3bc30cb01462509", @"/Areas/Admin/Pages/_ViewImports.cshtml")]
    public class Areas_Admin_Pages_Products__CreateFeaturesForProductGroup : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<ProductGroupTranslation>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("value", "", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("id", new global::Microsoft.AspNetCore.Html.HtmlString("FeatureGroup_ddl"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("form-control"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("name", "_ValidationScriptsPartial", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_4 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/AdminContent/AdminPages/ProductGroup.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.SelectTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_SelectTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.PartialTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_PartialTagHelper;
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n<div class=\"cart-body\">\r\n    <span class=\"text-primary\">افزودن ویژگی به <span class=\"text-primary text-bold\">");
#nullable restore
#line 4 "D:\DotNetCore\AdminPanel_991220\AdminPanel_RazorPage\AdminPanel_RazorPage\Areas\Admin\Pages\Products\_CreateFeaturesForProductGroup.cshtml"
                                                                               Write(Model.Title);

#line default
#line hidden
#nullable disable
            WriteLiteral("</span></span>\r\n    <hr />\r\n    <div class=\"row\">\r\n        <div class=\"col-md-6\">\r\n            <div class=\"row\">\r\n                <div class=\"col-md-6\">\r\n                    ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("select", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "c076269d1cb5a9cf1552174c8f86f4f8e7605eb410771", async() => {
                WriteLiteral("\r\n                        ");
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "c076269d1cb5a9cf1552174c8f86f4f8e7605eb411056", async() => {
                    WriteLiteral("گروه ویژگی ...");
                }
                );
                __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
                __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper.Value = (string)__tagHelperAttribute_0.Value;
                __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                WriteLiteral("\r\n                    ");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_SelectTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.SelectTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_SelectTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
#nullable restore
#line 10 "D:\DotNetCore\AdminPanel_991220\AdminPanel_RazorPage\AdminPanel_RazorPage\Areas\Admin\Pages\Products\_CreateFeaturesForProductGroup.cshtml"
__Microsoft_AspNetCore_Mvc_TagHelpers_SelectTagHelper.Items = (new SelectList((IEnumerable<FeatureGroupTranslation>)TempData["FeatureGroups"],"FeatureGroup_Id","Title"));

#line default
#line hidden
#nullable disable
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-items", __Microsoft_AspNetCore_Mvc_TagHelpers_SelectTagHelper.Items, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n\r\n                </div>\r\n                <div class=\"col-md-6\">\r\n                    <button class=\"btn btn-outline-success btnAddFeature disabled\"");
            BeginWriteAttribute("onclick", " onclick=\"", 748, "\"", 810, 3);
            WriteAttributeValue("", 758, "insertProductGroupFeatures(\'", 758, 28, true);
#nullable restore
#line 16 "D:\DotNetCore\AdminPanel_991220\AdminPanel_RazorPage\AdminPanel_RazorPage\Areas\Admin\Pages\Products\_CreateFeaturesForProductGroup.cshtml"
WriteAttributeValue("", 786, Model.ProductGroup_Id, 786, 22, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 808, "\')", 808, 2, true);
            EndWriteAttribute();
            WriteLiteral("><i class=\"fa fa-plus\"></i>  <i class=\"fa fa-spinner fa-spin hide\"></i><span");
            BeginWriteAttribute("class", " class=\"", 887, "\"", 895, 0);
            EndWriteAttribute();
            WriteLiteral(@">افزودن</span> </button>
                </div>
            </div>
            <div id=""featureList"">

            </div>
        </div>

        <div class=""col-md-6"" id=""productGroupFeatures"">
            <div class=""col-md-6"">
                <button class=""btn btn-outline-danger btnDeleteFeature disabled""");
            BeginWriteAttribute("onclick", " onclick=\"", 1216, "\"", 1306, 7);
            WriteAttributeValue("", 1226, "deleteProductGroupFeatures(\'", 1226, 28, true);
#nullable restore
#line 26 "D:\DotNetCore\AdminPanel_991220\AdminPanel_RazorPage\AdminPanel_RazorPage\Areas\Admin\Pages\Products\_CreateFeaturesForProductGroup.cshtml"
WriteAttributeValue("", 1254, Model.ProductGroup_Id, 1254, 22, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 1276, "\',\'حذف", 1276, 6, true);
            WriteAttributeValue(" ", 1282, "ویژگی", 1283, 6, true);
            WriteAttributeValue(" ", 1288, "از", 1289, 3, true);
#nullable restore
#line 26 "D:\DotNetCore\AdminPanel_991220\AdminPanel_RazorPage\AdminPanel_RazorPage\Areas\Admin\Pages\Products\_CreateFeaturesForProductGroup.cshtml"
WriteAttributeValue(" ", 1291, Model.Title, 1292, 12, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 1304, "\')", 1304, 2, true);
            EndWriteAttribute();
            WriteLiteral("><i class=\"fa fa-trash\"></i>  <i class=\"fa fa-spinner fa-spin hide\"></i><span");
            BeginWriteAttribute("class", " class=\"", 1384, "\"", 1392, 0);
            EndWriteAttribute();
            WriteLiteral(">حذف ویژگی از ");
#nullable restore
#line 26 "D:\DotNetCore\AdminPanel_991220\AdminPanel_RazorPage\AdminPanel_RazorPage\Areas\Admin\Pages\Products\_CreateFeaturesForProductGroup.cshtml"
                                                                                                                                                                                                                                                                          Write(Model.Title);

#line default
#line hidden
#nullable disable
            WriteLiteral(" </span> </button>\r\n            </div>\r\n        </div>\r\n\r\n    </div>\r\n\r\n\r\n\r\n</div>\r\n\r\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("partial", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "c076269d1cb5a9cf1552174c8f86f4f8e7605eb417049", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_PartialTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.PartialTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_PartialTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_PartialTagHelper.Name = (string)__tagHelperAttribute_3.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_3);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "c076269d1cb5a9cf1552174c8f86f4f8e7605eb418166", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_4);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n<script>\r\n    $(function () {\r\n        getProductGroupFeatures(\'");
#nullable restore
#line 40 "D:\DotNetCore\AdminPanel_991220\AdminPanel_RazorPage\AdminPanel_RazorPage\Areas\Admin\Pages\Products\_CreateFeaturesForProductGroup.cshtml"
                            Write(Model.ProductGroup_Id);

#line default
#line hidden
#nullable disable
            WriteLiteral("\');\r\n    });\r\n    $(\"#FeatureGroup_ddl\").change(function () {\r\n        getFeaturesByGroupId($(this).val());\r\n    });\r\n</script>\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<ProductGroupTranslation> Html { get; private set; }
    }
}
#pragma warning restore 1591
