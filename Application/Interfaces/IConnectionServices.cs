﻿using Domain.DomainClasses.Connection;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface IConnectionServices
    {
        Task<Connection> GetAsync(Expression<Func<Connection, bool>> where = null, Func<IQueryable<Connection>
         , IOrderedQueryable<Connection>> orderBy = null, Func<IQueryable<Connection>, IIncludableQueryable<Connection, object>> include = null, bool tracking = false);
        Task<bool> ExistsAsync(Expression<Func<Connection, bool>> where = null, Func<IQueryable<Connection>,
          IIncludableQueryable<Connection, object>> include = null);
        Task InsertConnection(Connection connection);
        void UpdateConnection(Connection connection);
        void Delete(Connection connection);
        Task Save();
    }
}
