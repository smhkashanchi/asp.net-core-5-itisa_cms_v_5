﻿using Domain.DomainClasses.Product;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface IProductGroupTranslationServices
    {
        Task<IEnumerable<ProductGroupTranslation>> GetAllAsyn(Expression<Func<ProductGroupTranslation, bool>> where = null, Func<IQueryable<ProductGroupTranslation>, IOrderedQueryable<ProductGroupTranslation>> orderBy = null
         , Func<IQueryable<ProductGroupTranslation>, IIncludableQueryable<ProductGroupTranslation, object>> include = null, bool tracking = false);

        Task<ProductGroupTranslation> GetAsync(Expression<Func<ProductGroupTranslation, bool>> where = null, Func<IQueryable<ProductGroupTranslation>
            , IOrderedQueryable<ProductGroupTranslation>> orderBy = null, Func<IQueryable<ProductGroupTranslation>, IIncludableQueryable<ProductGroupTranslation, object>> include = null, bool tracking = false);

        Task InsertRange(IEnumerable<ProductGroupTranslation> productGroupTranslations);
        Task Insert(ProductGroupTranslation productGroupTranslation);
        Task<bool> ExistsAsync(Expression<Func<ProductGroupTranslation, bool>> where = null, Func<IQueryable<ProductGroupTranslation>,
            IIncludableQueryable<ProductGroupTranslation, object>> include = null);

        Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<ProductGroupTranslation, TResult>> selector = null,
                                          Expression<Func<ProductGroupTranslation, bool>> where = null,
                                          Func<IQueryable<ProductGroupTranslation>, IOrderedQueryable<ProductGroupTranslation>> orderBy = null,
                                          Func<IQueryable<ProductGroupTranslation>, IIncludableQueryable<ProductGroupTranslation, object>> include = null,
                                          bool disableTracking = true);
        void Update(ProductGroupTranslation productGroupTranslation);

        void Delete(ProductGroupTranslation productGroupTranslation);
        void DeleteAll(IEnumerable<ProductGroupTranslation> productGroupTranslations);
        Task Save();
    }
}
