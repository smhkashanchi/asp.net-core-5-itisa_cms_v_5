﻿using Domain.DomainClasses.Region;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface ICountryServices
    {
        Task<IEnumerable<Country>> GetAllAsyn(Expression<Func<Country, bool>> where = null, Func<IQueryable<Country>, IOrderedQueryable<Country>> orderBy = null
           , Func<IQueryable<Country>, IIncludableQueryable<Country, object>> include = null, bool tracking = false);

        Task<Country> GetAsync(Expression<Func<Country, bool>> where = null, Func<IQueryable<Country>
            , IOrderedQueryable<Country>> orderBy = null, Func<IQueryable<Country>, IIncludableQueryable<Country, object>> include = null, bool tracking = false);

        Task Insert(Country country);
        Task<bool> ExistsAsync(Expression<Func<Country, bool>> where = null, Func<IQueryable<Country>,
            IIncludableQueryable<Country, object>> include = null);

        Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<Country, TResult>> selector = null,
                                          Expression<Func<Country, bool>> where = null,
                                          Func<IQueryable<Country>, IOrderedQueryable<Country>> orderBy = null,
                                          Func<IQueryable<Country>, IIncludableQueryable<Country, object>> include = null,
                                          bool disableTracking = true);
        Task Update(Country country);

        void Delete(Country country);
        void DeleteAll(IEnumerable<Country> countrys);
        Task Save();
    }
}
