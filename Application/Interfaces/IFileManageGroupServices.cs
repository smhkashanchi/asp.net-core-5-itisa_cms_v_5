﻿using Domain.DomainClasses.FilesManage;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface IFileManageGroupServices
    {
        Task<IEnumerable<FileManageGroup>> GetAllAsyn(Expression<Func<FileManageGroup, bool>> where = null, Func<IQueryable<FileManageGroup>, IOrderedQueryable<FileManageGroup>> orderBy = null
           , Func<IQueryable<FileManageGroup>, IIncludableQueryable<FileManageGroup, object>> include = null, bool tracking = false);

        Task<FileManageGroup> GetAsync(Expression<Func<FileManageGroup, bool>> where = null, Func<IQueryable<FileManageGroup>
            , IOrderedQueryable<FileManageGroup>> orderBy = null, Func<IQueryable<FileManageGroup>, IIncludableQueryable<FileManageGroup, object>> include = null, bool tracking = false);

        Task Insert(FileManageGroup fileManageGroup);
        Task<bool> ExistsAsync(Expression<Func<FileManageGroup, bool>> where = null, Func<IQueryable<FileManageGroup>,
            IIncludableQueryable<FileManageGroup, object>> include = null);

        Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<FileManageGroup, TResult>> selector = null,
                                          Expression<Func<FileManageGroup, bool>> where = null,
                                          Func<IQueryable<FileManageGroup>, IOrderedQueryable<FileManageGroup>> orderBy = null,
                                          Func<IQueryable<FileManageGroup>, IIncludableQueryable<FileManageGroup, object>> include = null,
                                          bool disableTracking = true);
        void Update(FileManageGroup fileManageGroup);

        void Delete(FileManageGroup fileManageGroup);
        Task Save();
    }
}
