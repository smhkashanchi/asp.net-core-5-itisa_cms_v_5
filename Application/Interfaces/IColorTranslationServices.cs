﻿using Domain.DomainClasses.Color;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface IColorTranslationServices
    {
        Task<IEnumerable<ColorTranslation>> GetAllAsyn(Expression<Func<ColorTranslation, bool>> where = null, Func<IQueryable<ColorTranslation>, IOrderedQueryable<ColorTranslation>> orderBy = null
            , Func<IQueryable<ColorTranslation>, IIncludableQueryable<ColorTranslation, object>> include = null, bool tracking = false);
        Task<ColorTranslation> GetAsync(Expression<Func<ColorTranslation, bool>> where = null, Func<IQueryable<ColorTranslation>
           , IOrderedQueryable<ColorTranslation>> orderBy = null, Func<IQueryable<ColorTranslation>, IIncludableQueryable<ColorTranslation, object>> include = null, bool tracking = false);

        Task Insert(ColorTranslation colorTranslation);
        Task<bool> ExistsAsync(Expression<Func<ColorTranslation, bool>> where = null,Func<IQueryable<ColorTranslation>, IIncludableQueryable<ColorTranslation
            ,object>> include = null);

        Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<ColorTranslation, TResult>> selector = null,
                                          Expression<Func<ColorTranslation, bool>> where = null,
                                          Func<IQueryable<ColorTranslation>, IOrderedQueryable<ColorTranslation>> orderBy = null,
                                          Func<IQueryable<ColorTranslation>, IIncludableQueryable<ColorTranslation, object>> include = null,
                                          bool disableTracking = true);
        void Update(ColorTranslation colorTranslation);
        void DeleteAsync(ColorTranslation colorTranslation);
        void DeleteAll(IEnumerable<ColorTranslation> colorsTranslation);
        Task Save();
    }
}
