﻿using Domain.DomainClasses.SendType;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface ISendTypeServices
    {
        Task<IEnumerable<SendType>> GetAllAsyn(Expression<Func<SendType, bool>> where = null, Func<IQueryable<SendType>, IOrderedQueryable<SendType>> orderBy = null
         , Func<IQueryable<SendType>, IIncludableQueryable<SendType, object>> include = null, bool tracking = false);

        Task<SendType> GetAsync(Expression<Func<SendType, bool>> where = null, Func<IQueryable<SendType>
            , IOrderedQueryable<SendType>> orderBy = null, Func<IQueryable<SendType>, IIncludableQueryable<SendType, object>> include = null, bool tracking = false);

        Task Insert(SendType sendType);
        Task<bool> ExistsAsync(Expression<Func<SendType, bool>> where = null, Func<IQueryable<SendType>,
            IIncludableQueryable<SendType, object>> include = null);

        Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<SendType, TResult>> selector = null,
                                          Expression<Func<SendType, bool>> where = null,
                                          Func<IQueryable<SendType>, IOrderedQueryable<SendType>> orderBy = null,
                                          Func<IQueryable<SendType>, IIncludableQueryable<SendType, object>> include = null,
                                          bool disableTracking = true);
        Task Update(SendType sendType);

        void DeleteAsync(SendType sendType);
        Task Save();
    }
}
