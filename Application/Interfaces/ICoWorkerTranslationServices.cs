﻿using Domain.DomainClasses.CoWorker;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface ICoWorkerTranslationServices
    {
        Task<IEnumerable<CoWorkerTranslation>> GetAllAsyn(Expression<Func<CoWorkerTranslation, bool>> where = null, Func<IQueryable<CoWorkerTranslation>, IOrderedQueryable<CoWorkerTranslation>> orderBy = null
        , Func<IQueryable<CoWorkerTranslation>, IIncludableQueryable<CoWorkerTranslation, object>> include = null, bool tracking = false);

        Task<CoWorkerTranslation> GetAsync(Expression<Func<CoWorkerTranslation, bool>> where = null, Func<IQueryable<CoWorkerTranslation>
            , IOrderedQueryable<CoWorkerTranslation>> orderBy = null, Func<IQueryable<CoWorkerTranslation>, IIncludableQueryable<CoWorkerTranslation, object>> include = null, bool tracking = false);

        Task<bool> ExistsAsync(Expression<Func<CoWorkerTranslation, bool>> where = null, Func<IQueryable<CoWorkerTranslation>,
            IIncludableQueryable<CoWorkerTranslation, object>> include = null);

        Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<CoWorkerTranslation, TResult>> selector = null,
                                          Expression<Func<CoWorkerTranslation, bool>> where = null,
                                          Func<IQueryable<CoWorkerTranslation>, IOrderedQueryable<CoWorkerTranslation>> orderBy = null,
                                          Func<IQueryable<CoWorkerTranslation>, IIncludableQueryable<CoWorkerTranslation, object>> include = null,
                                          bool disableTracking = true);

        Task Insert(CoWorkerTranslation coWorkerTranslation);
        Task Update(CoWorkerTranslation coWorkerTranslation);

        void Delete(CoWorkerTranslation coWorkerTranslation);
        void DeleteAll(IEnumerable<CoWorkerTranslation> coWorkerTranslations);
        Task Save();
    }
}
