﻿using Domain.DomainClasses.Product;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface IProductTranslationServices
    {
        Task<IEnumerable<ProductTranslation>> GetAllAsyn(Expression<Func<ProductTranslation, bool>> where = null, Func<IQueryable<ProductTranslation>, IOrderedQueryable<ProductTranslation>> orderBy = null
         , Func<IQueryable<ProductTranslation>, IIncludableQueryable<ProductTranslation, object>> include = null, bool tracking = false);

        Task<ProductTranslation> GetAsync(Expression<Func<ProductTranslation, bool>> where = null, Func<IQueryable<ProductTranslation>
            , IOrderedQueryable<ProductTranslation>> orderBy = null, Func<IQueryable<ProductTranslation>, IIncludableQueryable<ProductTranslation, object>> include = null, bool tracking = false);

        Task Insert(ProductTranslation productTranslation);
        Task InsertRange(IEnumerable<ProductTranslation> productTranslations);
        Task<bool> ExistsAsync(Expression<Func<ProductTranslation, bool>> where = null, Func<IQueryable<ProductTranslation>,
            IIncludableQueryable<ProductTranslation, object>> include = null);

        Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<ProductTranslation, TResult>> selector = null,
                                          Expression<Func<ProductTranslation, bool>> where = null,
                                          Func<IQueryable<ProductTranslation>, IOrderedQueryable<ProductTranslation>> orderBy = null,
                                          Func<IQueryable<ProductTranslation>, IIncludableQueryable<ProductTranslation, object>> include = null,
                                          bool disableTracking = true);
        void Update(ProductTranslation productTranslation);

        void Delete(ProductTranslation productTranslation);
        void DeleteAll(IEnumerable<ProductTranslation> productTranslations);
        Task Save();
    }
}
