﻿using Domain.DomainClasses.Content;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface IContentServices
    {
        Task<IEnumerable<Content>> GetAllAsyn(Expression<Func<Content, bool>> where = null, Func<IQueryable<Content>, IOrderedQueryable<Content>> orderBy = null
         , Func<IQueryable<Content>, IIncludableQueryable<Content, object>> include = null, bool tracking = false);

        Task<Content> GetAsync(Expression<Func<Content, bool>> where = null, Func<IQueryable<Content>
            , IOrderedQueryable<Content>> orderBy = null, Func<IQueryable<Content>, IIncludableQueryable<Content, object>> include = null, bool tracking = false);

        Task Insert(Content Content);
        Task<bool> ExistsAsync(Expression<Func<Content, bool>> where = null, Func<IQueryable<Content>,
            IIncludableQueryable<Content, object>> include = null);

        Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<Content, TResult>> selector = null,
                                          Expression<Func<Content, bool>> where = null,
                                          Func<IQueryable<Content>, IOrderedQueryable<Content>> orderBy = null,
                                          Func<IQueryable<Content>, IIncludableQueryable<Content, object>> include = null,
                                          bool disableTracking = true);
        Task Update(Content content);

        void DeleteAsync(Content content);
        Task Save();
    }
}
