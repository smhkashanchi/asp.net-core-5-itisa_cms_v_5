﻿using Domain.DomainClasses.Content;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface IContentGroupTranslationSrevices
    {
        Task<IEnumerable<ContentGroupTranslation>> GetAllAsyn(Expression<Func<ContentGroupTranslation, bool>> where = null, Func<IQueryable<ContentGroupTranslation>, IOrderedQueryable<ContentGroupTranslation>> orderBy = null
         , Func<IQueryable<ContentGroupTranslation>, IIncludableQueryable<ContentGroupTranslation, object>> include = null, bool tracking = false);

        Task<ContentGroupTranslation> GetAsync(Expression<Func<ContentGroupTranslation, bool>> where = null, Func<IQueryable<ContentGroupTranslation>
            , IOrderedQueryable<ContentGroupTranslation>> orderBy = null, Func<IQueryable<ContentGroupTranslation>, IIncludableQueryable<ContentGroupTranslation, object>> include = null, bool tracking = false);

        Task Insert(ContentGroupTranslation ContentGroupTranslation);
        Task<bool> ExistsAsync(Expression<Func<ContentGroupTranslation, bool>> where = null, Func<IQueryable<ContentGroupTranslation>,
            IIncludableQueryable<ContentGroupTranslation, object>> include = null);

        Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<ContentGroupTranslation, TResult>> selector = null,
                                          Expression<Func<ContentGroupTranslation, bool>> where = null,
                                          Func<IQueryable<ContentGroupTranslation>, IOrderedQueryable<ContentGroupTranslation>> orderBy = null,
                                          Func<IQueryable<ContentGroupTranslation>, IIncludableQueryable<ContentGroupTranslation, object>> include = null,
                                          bool disableTracking = true);
        Task Update(ContentGroupTranslation ContentGroupTranslation);

        void DeleteAsync(ContentGroupTranslation ContentGroupTranslation);
        void DeleteAllAsync(IEnumerable<ContentGroupTranslation> ContentGroupTranslations);
        Task Save();
    }
}
