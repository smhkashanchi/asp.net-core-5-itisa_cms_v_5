﻿using Domain.DomainClasses.SlideShow;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface ISlideShowTranslationServices
    {
        Task<SlideShowTranslation> GetAsync(Expression<Func<SlideShowTranslation, bool>> where = null, Func<IQueryable<SlideShowTranslation>
        , IOrderedQueryable<SlideShowTranslation>> orderBy = null, Func<IQueryable<SlideShowTranslation>, IIncludableQueryable<SlideShowTranslation, object>> include = null, bool tracking = false);

        Task<Dictionary<string, int>> GetAllToDictionary(int Id);
        Task<bool> ExistsAsync(Expression<Func<SlideShowTranslation, bool>> where = null, Func<IQueryable<SlideShowTranslation>,
         IIncludableQueryable<SlideShowTranslation, object>> include = null);
        Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<SlideShowTranslation, TResult>> selector = null,
                                          Expression<Func<SlideShowTranslation, bool>> where = null,
                                          Func<IQueryable<SlideShowTranslation>, IOrderedQueryable<SlideShowTranslation>> orderBy = null,
                                          Func<IQueryable<SlideShowTranslation>, IIncludableQueryable<SlideShowTranslation, object>> include = null,
                                          bool disableTracking = true);

        Task Insert(SlideShowTranslation slideShowTranslation);
        Task Update(SlideShowTranslation slideShowTranslation);
        void Delete(SlideShowTranslation slideShowTranslation);
        void DeleteAll(IEnumerable<SlideShowTranslation> slideShowTranslation);
        Task Save();
    }
}
