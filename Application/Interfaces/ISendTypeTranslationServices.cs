﻿using Domain.DomainClasses.SendType;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface ISendTypeTranslationServices
    {
        Task<IEnumerable<SendTypeTranslation>> GetAllAsyn(Expression<Func<SendTypeTranslation, bool>> where = null, Func<IQueryable<SendTypeTranslation>, IOrderedQueryable<SendTypeTranslation>> orderBy = null
          , Func<IQueryable<SendTypeTranslation>, IIncludableQueryable<SendTypeTranslation, object>> include = null, bool tracking = false);

        Task<SendTypeTranslation> GetAsync(Expression<Func<SendTypeTranslation, bool>> where = null, Func<IQueryable<SendTypeTranslation>
            , IOrderedQueryable<SendTypeTranslation>> orderBy = null, Func<IQueryable<SendTypeTranslation>, IIncludableQueryable<SendTypeTranslation, object>> include = null, bool tracking = false);

        Task Insert(SendTypeTranslation sendTypeTranslation);
        Task<bool> ExistsAsync(Expression<Func<SendTypeTranslation, bool>> where = null, Func<IQueryable<SendTypeTranslation>,
            IIncludableQueryable<SendTypeTranslation, object>> include = null);

        Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<SendTypeTranslation, TResult>> selector = null,
                                          Expression<Func<SendTypeTranslation, bool>> where = null,
                                          Func<IQueryable<SendTypeTranslation>, IOrderedQueryable<SendTypeTranslation>> orderBy = null,
                                          Func<IQueryable<SendTypeTranslation>, IIncludableQueryable<SendTypeTranslation, object>> include = null,
                                          bool disableTracking = true);
        Task Update(SendTypeTranslation sendTypeTranslation);

        void Delete(SendTypeTranslation sendTypeTranslation);
        void DeleteAll(IEnumerable<SendTypeTranslation> sendTypeTranslations);
        Task Save();
    }
}
