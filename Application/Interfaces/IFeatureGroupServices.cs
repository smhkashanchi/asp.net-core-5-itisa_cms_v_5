﻿using Domain.DomainClasses.Feature;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface IFeatureGroupServices
    {
        Task<IEnumerable<FeatureGroup>> GetAllAsyn(Expression<Func<FeatureGroup, bool>> where = null, Func<IQueryable<FeatureGroup>, IOrderedQueryable<FeatureGroup>> orderBy = null
         , Func<IQueryable<FeatureGroup>, IIncludableQueryable<FeatureGroup, object>> include = null, bool tracking = false);

        Task<FeatureGroup> GetAsync(Expression<Func<FeatureGroup, bool>> where = null, Func<IQueryable<FeatureGroup>
            , IOrderedQueryable<FeatureGroup>> orderBy = null, Func<IQueryable<FeatureGroup>, IIncludableQueryable<FeatureGroup, object>> include = null, bool tracking = false);

        Task Insert(FeatureGroup featureGroup);
        Task<bool> ExistsAsync(Expression<Func<FeatureGroup, bool>> where = null, Func<IQueryable<FeatureGroup>,
            IIncludableQueryable<FeatureGroup, object>> include = null);

        Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<FeatureGroup, TResult>> selector = null,
                                          Expression<Func<FeatureGroup, bool>> where = null,
                                          Func<IQueryable<FeatureGroup>, IOrderedQueryable<FeatureGroup>> orderBy = null,
                                          Func<IQueryable<FeatureGroup>, IIncludableQueryable<FeatureGroup, object>> include = null,
                                          bool disableTracking = true);
        void Update(FeatureGroup featureGroup);

        void Delete(FeatureGroup featureGroup);
        Task Save();
    }
}
