﻿using Domain.DomainClasses.Feature;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface IFeatureReplyServices
    {
        Task<IEnumerable<FeatureReply>> GetAllAsyn(Expression<Func<FeatureReply, bool>> where = null, Func<IQueryable<FeatureReply>, IOrderedQueryable<FeatureReply>> orderBy = null
          , Func<IQueryable<FeatureReply>, IIncludableQueryable<FeatureReply, object>> include = null, bool tracking = false);

        Task<FeatureReply> GetAsync(Expression<Func<FeatureReply, bool>> where = null, Func<IQueryable<FeatureReply>
            , IOrderedQueryable<FeatureReply>> orderBy = null, Func<IQueryable<FeatureReply>, IIncludableQueryable<FeatureReply, object>> include = null, bool tracking = false);

        Task Insert(FeatureReply featureReply);
        Task<bool> ExistsAsync(Expression<Func<FeatureReply, bool>> where = null, Func<IQueryable<FeatureReply>,
            IIncludableQueryable<FeatureReply, object>> include = null);

        Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<FeatureReply, TResult>> selector = null,
                                          Expression<Func<FeatureReply, bool>> where = null,
                                          Func<IQueryable<FeatureReply>, IOrderedQueryable<FeatureReply>> orderBy = null,
                                          Func<IQueryable<FeatureReply>, IIncludableQueryable<FeatureReply, object>> include = null,
                                          bool disableTracking = true);
        void Update(FeatureReply featureReply);

        void Delete(FeatureReply featureReply);
        Task Save();
    }
}
