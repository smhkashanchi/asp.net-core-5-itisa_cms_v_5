﻿using Domain.DomainClasses.SlideShow;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface ISlideShowGroupTranslationServices
    {
        Task<IEnumerable<SlideShowGroupTranslation>> GetAllAsyn(Expression<Func<SlideShowGroupTranslation, bool>> where = null, Func<IQueryable<SlideShowGroupTranslation>, IOrderedQueryable<SlideShowGroupTranslation>> orderBy = null
          , Func<IQueryable<SlideShowGroupTranslation>, IIncludableQueryable<SlideShowGroupTranslation, object>> include = null, bool tracking = false);

        Task<SlideShowGroupTranslation> GetAsync(Expression<Func<SlideShowGroupTranslation, bool>> where = null, Func<IQueryable<SlideShowGroupTranslation>
            , IOrderedQueryable<SlideShowGroupTranslation>> orderBy = null, Func<IQueryable<SlideShowGroupTranslation>, IIncludableQueryable<SlideShowGroupTranslation, object>> include = null, bool tracking = false);

        Task<bool> ExistsAsync(Expression<Func<SlideShowGroupTranslation, bool>> where = null, Func<IQueryable<SlideShowGroupTranslation>,
            IIncludableQueryable<SlideShowGroupTranslation, object>> include = null);

        Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<SlideShowGroupTranslation, TResult>> selector = null,
                                          Expression<Func<SlideShowGroupTranslation, bool>> where = null,
                                          Func<IQueryable<SlideShowGroupTranslation>, IOrderedQueryable<SlideShowGroupTranslation>> orderBy = null,
                                          Func<IQueryable<SlideShowGroupTranslation>, IIncludableQueryable<SlideShowGroupTranslation, object>> include = null,
                                          bool disableTracking = true);

        Task Insert(SlideShowGroupTranslation slideShowGroupTranslation);
        Task Update(SlideShowGroupTranslation slideShowGroupTranslation);

        void DeleteAsync(SlideShowGroupTranslation slideShowGroupTranslation);
        Task Save();
    }
}
