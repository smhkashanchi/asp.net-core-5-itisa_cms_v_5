﻿using Domain.DomainClasses.Feature;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface IFeatureGroupTranslationServices
    {
        Task<IEnumerable<FeatureGroupTranslation>> GetAllAsyn(Expression<Func<FeatureGroupTranslation, bool>> where = null, Func<IQueryable<FeatureGroupTranslation>, IOrderedQueryable<FeatureGroupTranslation>> orderBy = null
        , Func<IQueryable<FeatureGroupTranslation>, IIncludableQueryable<FeatureGroupTranslation, object>> include = null, bool tracking = false);

        Task<FeatureGroupTranslation> GetAsync(Expression<Func<FeatureGroupTranslation, bool>> where = null, Func<IQueryable<FeatureGroupTranslation>
            , IOrderedQueryable<FeatureGroupTranslation>> orderBy = null, Func<IQueryable<FeatureGroupTranslation>, IIncludableQueryable<FeatureGroupTranslation, object>> include = null, bool tracking = false);

        Task Insert(FeatureGroupTranslation featureGroupTranslation);
        Task<bool> ExistsAsync(Expression<Func<FeatureGroupTranslation, bool>> where = null, Func<IQueryable<FeatureGroupTranslation>,
            IIncludableQueryable<FeatureGroupTranslation, object>> include = null);

        Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<FeatureGroupTranslation, TResult>> selector = null,
                                          Expression<Func<FeatureGroupTranslation, bool>> where = null,
                                          Func<IQueryable<FeatureGroupTranslation>, IOrderedQueryable<FeatureGroupTranslation>> orderBy = null,
                                          Func<IQueryable<FeatureGroupTranslation>, IIncludableQueryable<FeatureGroupTranslation, object>> include = null,
                                          bool disableTracking = true);
        void Update(FeatureGroupTranslation featureGroupTranslation);

        void Delete(FeatureGroupTranslation featureGroupTranslation);
        Task Save();
    }
}
