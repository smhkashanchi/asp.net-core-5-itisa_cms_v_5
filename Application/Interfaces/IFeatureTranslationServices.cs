﻿using Domain.DomainClasses.Feature;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface IFeatureTranslationServices
    {
        Task<IEnumerable<FeatureTranslation>> GetAllAsyn(Expression<Func<FeatureTranslation, bool>> where = null, Func<IQueryable<FeatureTranslation>, IOrderedQueryable<FeatureTranslation>> orderBy = null
          , Func<IQueryable<FeatureTranslation>, IIncludableQueryable<FeatureTranslation, object>> include = null, bool tracking = false);

        Task<FeatureTranslation> GetAsync(Expression<Func<FeatureTranslation, bool>> where = null, Func<IQueryable<FeatureTranslation>
            , IOrderedQueryable<FeatureTranslation>> orderBy = null, Func<IQueryable<FeatureTranslation>, IIncludableQueryable<FeatureTranslation, object>> include = null, bool tracking = false);

        Task Insert(FeatureTranslation featureTranslation);
        Task<bool> ExistsAsync(Expression<Func<FeatureTranslation, bool>> where = null, Func<IQueryable<FeatureTranslation>,
            IIncludableQueryable<FeatureTranslation, object>> include = null);

        Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<FeatureTranslation, TResult>> selector = null,
                                          Expression<Func<FeatureTranslation, bool>> where = null,
                                          Func<IQueryable<FeatureTranslation>, IOrderedQueryable<FeatureTranslation>> orderBy = null,
                                          Func<IQueryable<FeatureTranslation>, IIncludableQueryable<FeatureTranslation, object>> include = null,
                                          bool disableTracking = true);
        void Update(FeatureTranslation featureTranslation);

        void Delete(FeatureTranslation featureTranslation);
        void DeleteAll(IEnumerable<FeatureTranslation> featureTranslations);
        Task Save();
    }
}
