﻿using Domain.DomainClasses.Product;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface ISubDiscountServices
    {
        Task<IEnumerable<SubDiscount>> GetAllAsyn(Expression<Func<SubDiscount, bool>> where = null, Func<IQueryable<SubDiscount>, IOrderedQueryable<SubDiscount>> orderBy = null
              , Func<IQueryable<SubDiscount>, IIncludableQueryable<SubDiscount, object>> include = null, bool tracking = false);

        Task<SubDiscount> GetAsync(Expression<Func<SubDiscount, bool>> where = null, Func<IQueryable<SubDiscount>
            , IOrderedQueryable<SubDiscount>> orderBy = null, Func<IQueryable<SubDiscount>, IIncludableQueryable<SubDiscount, object>> include = null, bool tracking = false);

        Task Insert(SubDiscount subDiscount);
        Task<bool> ExistsAsync(Expression<Func<SubDiscount, bool>> where = null, Func<IQueryable<SubDiscount>,
            IIncludableQueryable<SubDiscount, object>> include = null);

        Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<SubDiscount, TResult>> selector = null,
                                          Expression<Func<SubDiscount, bool>> where = null,
                                          Func<IQueryable<SubDiscount>, IOrderedQueryable<SubDiscount>> orderBy = null,
                                          Func<IQueryable<SubDiscount>, IIncludableQueryable<SubDiscount, object>> include = null,
                                          bool disableTracking = true);
        Task Update(SubDiscount subDiscount);

        void Delete(SubDiscount subDiscount);
        void DeleteAll(IEnumerable<SubDiscount> subDiscounts);
        Task Save();
    }
}
