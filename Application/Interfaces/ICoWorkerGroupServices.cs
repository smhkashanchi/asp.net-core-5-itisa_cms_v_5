﻿using Domain.DomainClasses.CoWorker;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface ICoWorkerGroupServices
    {
        Task<IEnumerable<CoWorkerGroup>> GetAllAsyn(Expression<Func<CoWorkerGroup, bool>> where = null, Func<IQueryable<CoWorkerGroup>, IOrderedQueryable<CoWorkerGroup>> orderBy = null
         , Func<IQueryable<CoWorkerGroup>, IIncludableQueryable<CoWorkerGroup, object>> include = null, bool tracking = false);

        Task<CoWorkerGroup> GetAsync(Expression<Func<CoWorkerGroup, bool>> where = null, Func<IQueryable<CoWorkerGroup>
            , IOrderedQueryable<CoWorkerGroup>> orderBy = null, Func<IQueryable<CoWorkerGroup>, IIncludableQueryable<CoWorkerGroup, object>> include = null, bool tracking = false);

        Task<bool> ExistsAsync(Expression<Func<CoWorkerGroup, bool>> where = null, Func<IQueryable<CoWorkerGroup>,
            IIncludableQueryable<CoWorkerGroup, object>> include = null);

        Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<CoWorkerGroup, TResult>> selector = null,
                                          Expression<Func<CoWorkerGroup, bool>> where = null,
                                          Func<IQueryable<CoWorkerGroup>, IOrderedQueryable<CoWorkerGroup>> orderBy = null,
                                          Func<IQueryable<CoWorkerGroup>, IIncludableQueryable<CoWorkerGroup, object>> include = null,
                                          bool disableTracking = true);

        Task Insert(CoWorkerGroup coWorkerGroup);
        Task Update(CoWorkerGroup coWorkerGroup);

        void DeleteAsync(CoWorkerGroup coWorkerGroup);
        Task Save();
    }
}
