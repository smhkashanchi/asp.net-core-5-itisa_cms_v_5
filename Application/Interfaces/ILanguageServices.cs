﻿using Domain.DomainClasses;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface ILanguageServices
    {
        Task<IEnumerable<Language>> GetAll(Expression<Func<Language, bool>> where = null, Func<IQueryable<Language>, IOrderedQueryable<Language>> orderBy = null, Func<IQueryable<Language>, IIncludableQueryable<Language, object>> include = null, bool tracking = false);
        Task<IEnumerable<Language>> GetAllIsActive();
        Task<Language> GetOne(string Id);
        Task Insert(Language language);
        void Update(Language language);
        void Delete(Language language);
        Task<bool> IsExist(string Id);
        Task Save();
    }
}
