﻿using Domain.DomainClasses.Gallery;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface IGalleryTranslationServices
    {
        Task<GalleryTranslation> GetAsync(Expression<Func<GalleryTranslation, bool>> where = null, Func<IQueryable<GalleryTranslation>
       , IOrderedQueryable<GalleryTranslation>> orderBy = null, Func<IQueryable<GalleryTranslation>, IIncludableQueryable<GalleryTranslation, object>> include = null, bool tracking = false);

        Task<bool> ExistsAsync(Expression<Func<GalleryTranslation, bool>> where = null, Func<IQueryable<GalleryTranslation>,
         IIncludableQueryable<GalleryTranslation, object>> include = null);
        Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<GalleryTranslation, TResult>> selector = null,
                                          Expression<Func<GalleryTranslation, bool>> where = null,
                                          Func<IQueryable<GalleryTranslation>, IOrderedQueryable<GalleryTranslation>> orderBy = null,
                                          Func<IQueryable<GalleryTranslation>, IIncludableQueryable<GalleryTranslation, object>> include = null,
                                          bool disableTracking = true);

        Task<IEnumerable<GalleryTranslation>> GetAll(Expression<Func<GalleryTranslation, bool>> where = null, Func<IQueryable<GalleryTranslation>, IOrderedQueryable<GalleryTranslation>> orderBy = null, Func<IQueryable<GalleryTranslation>, IIncludableQueryable<GalleryTranslation, object>> include = null, bool tracking = false);

        Task Insert(GalleryTranslation galleryTranslation);
        void Update(GalleryTranslation galleryTranslation);
        void Delete(GalleryTranslation galleryTranslation);
        void DeleteAll(IEnumerable<GalleryTranslation> galleryTranslation);
        Task Save();
    }
}
