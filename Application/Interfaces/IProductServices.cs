﻿using Domain.DomainClasses.Product;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface IProductServices
    {
        Task<IEnumerable<Product>> GetAllAsyn(Expression<Func<Product, bool>> where = null, Func<IQueryable<Product>, IOrderedQueryable<Product>> orderBy = null
         , Func<IQueryable<Product>, IIncludableQueryable<Product, object>> include = null, bool tracking = false);

        Task<Product> GetAsync(Expression<Func<Product, bool>> where = null, Func<IQueryable<Product>
            , IOrderedQueryable<Product>> orderBy = null, Func<IQueryable<Product>, IIncludableQueryable<Product, object>> include = null, bool tracking = false);

        Task Insert(Product product);
        Task<bool> ExistsAsync(Expression<Func<Product, bool>> where = null, Func<IQueryable<Product>,
            IIncludableQueryable<Product, object>> include = null);

        Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<Product, TResult>> selector = null,
                                          Expression<Func<Product, bool>> where = null,
                                          Func<IQueryable<Product>, IOrderedQueryable<Product>> orderBy = null,
                                          Func<IQueryable<Product>, IIncludableQueryable<Product, object>> include = null,
                                          bool disableTracking = true);
        void Update(Product product);

        Task Delete(Product product);
        Task Save();
    }
}
