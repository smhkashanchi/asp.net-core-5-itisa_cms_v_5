﻿using Domain.DomainClasses.Product;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface IProductGroupServices
    {
        Task<IEnumerable<ProductGroup>> GetAllAsyn(Expression<Func<ProductGroup, bool>> where = null, Func<IQueryable<ProductGroup>, IOrderedQueryable<ProductGroup>> orderBy = null
          , Func<IQueryable<ProductGroup>, IIncludableQueryable<ProductGroup, object>> include = null, bool tracking = false);

        Task<ProductGroup> GetAsync(Expression<Func<ProductGroup, bool>> where = null, Func<IQueryable<ProductGroup>
            , IOrderedQueryable<ProductGroup>> orderBy = null, Func<IQueryable<ProductGroup>, IIncludableQueryable<ProductGroup, object>> include = null, bool tracking = false);

        Task Insert(ProductGroup productGroup);
        Task<bool> ExistsAsync(Expression<Func<ProductGroup, bool>> where = null, Func<IQueryable<ProductGroup>,
            IIncludableQueryable<ProductGroup, object>> include = null);

        Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<ProductGroup, TResult>> selector = null,
                                          Expression<Func<ProductGroup, bool>> where = null,
                                          Func<IQueryable<ProductGroup>, IOrderedQueryable<ProductGroup>> orderBy = null,
                                          Func<IQueryable<ProductGroup>, IIncludableQueryable<ProductGroup, object>> include = null,
                                          bool disableTracking = true);
        void Update(ProductGroup productGroup);

        void Delete(ProductGroup productGroup);
        Task Save();
    }
}
