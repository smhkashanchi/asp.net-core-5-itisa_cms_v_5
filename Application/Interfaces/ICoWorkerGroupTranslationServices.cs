﻿using Domain.DomainClasses.CoWorker;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface ICoWorkerGroupTranslationServices
    {
        Task<IEnumerable<CoWorkerGroupTranslation>> GetAllAsyn(Expression<Func<CoWorkerGroupTranslation, bool>> where = null, Func<IQueryable<CoWorkerGroupTranslation>, IOrderedQueryable<CoWorkerGroupTranslation>> orderBy = null
         , Func<IQueryable<CoWorkerGroupTranslation>, IIncludableQueryable<CoWorkerGroupTranslation, object>> include = null, bool tracking = false);

        Task<CoWorkerGroupTranslation> GetAsync(Expression<Func<CoWorkerGroupTranslation, bool>> where = null, Func<IQueryable<CoWorkerGroupTranslation>
            , IOrderedQueryable<CoWorkerGroupTranslation>> orderBy = null, Func<IQueryable<CoWorkerGroupTranslation>, IIncludableQueryable<CoWorkerGroupTranslation, object>> include = null, bool tracking = false);

        Task<bool> ExistsAsync(Expression<Func<CoWorkerGroupTranslation, bool>> where = null, Func<IQueryable<CoWorkerGroupTranslation>,
            IIncludableQueryable<CoWorkerGroupTranslation, object>> include = null);

        Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<CoWorkerGroupTranslation, TResult>> selector = null,
                                          Expression<Func<CoWorkerGroupTranslation, bool>> where = null,
                                          Func<IQueryable<CoWorkerGroupTranslation>, IOrderedQueryable<CoWorkerGroupTranslation>> orderBy = null,
                                          Func<IQueryable<CoWorkerGroupTranslation>, IIncludableQueryable<CoWorkerGroupTranslation, object>> include = null,
                                          bool disableTracking = true);

        Task Insert(CoWorkerGroupTranslation coWorkerGroupTranslation);
        Task Update(CoWorkerGroupTranslation coWorkerGroupTranslation);

        void DeleteAsync(CoWorkerGroupTranslation coWorkerGroupTranslation);
        Task Save();
    }
}
