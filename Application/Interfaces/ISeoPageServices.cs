﻿using Domain.DomainClasses.Seo;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface ISeoPageServices
    {
        Task<IEnumerable<SeoPage>> GetAllAsyn(Expression<Func<SeoPage, bool>> where = null, Func<IQueryable<SeoPage>, IOrderedQueryable<SeoPage>> orderBy = null
         , Func<IQueryable<SeoPage>, IIncludableQueryable<SeoPage, object>> include = null, bool tracking = false);

        Task<SeoPage> GetAsync(Expression<Func<SeoPage, bool>> where = null, Func<IQueryable<SeoPage>
            , IOrderedQueryable<SeoPage>> orderBy = null, Func<IQueryable<SeoPage>, IIncludableQueryable<SeoPage, object>> include = null, bool tracking = false);

        Task<bool> ExistsAsync(Expression<Func<SeoPage, bool>> where = null, Func<IQueryable<SeoPage>,
            IIncludableQueryable<SeoPage, object>> include = null);

        Task Insert(SeoPage SeoPage);
        Task Update(SeoPage SeoPage);

        void DeleteAsync(SeoPage SeoPage);
        Task Save();
    }
}
