﻿using Domain.DomainClasses.Region;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface ICityServices
    {
        Task<IEnumerable<City>> GetAllAsyn(Expression<Func<City, bool>> where = null, Func<IQueryable<City>, IOrderedQueryable<City>> orderBy = null
               , Func<IQueryable<City>, IIncludableQueryable<City, object>> include = null, bool tracking = false);

        Task<City> GetAsync(Expression<Func<City, bool>> where = null, Func<IQueryable<City>
            , IOrderedQueryable<City>> orderBy = null, Func<IQueryable<City>, IIncludableQueryable<City, object>> include = null, bool tracking = false);

        Task Insert(City city);
        Task<bool> ExistsAsync(Expression<Func<City, bool>> where = null, Func<IQueryable<City>,
            IIncludableQueryable<City, object>> include = null);

        Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<City, TResult>> selector = null,
                                          Expression<Func<City, bool>> where = null,
                                          Func<IQueryable<City>, IOrderedQueryable<City>> orderBy = null,
                                          Func<IQueryable<City>, IIncludableQueryable<City, object>> include = null,
                                          bool disableTracking = true);
        Task Update(City city);

        void Delete(City city);
        void DeleteAll(IEnumerable<City> citys);
        Task Save();
    }
}
