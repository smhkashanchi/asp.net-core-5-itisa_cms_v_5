﻿using Domain.DomainClasses.FilesManage;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface IFileManageTranslationServices
    {
        Task<IEnumerable<FileManageTranslation>> GetAllAsyn(Expression<Func<FileManageTranslation, bool>> where = null, Func<IQueryable<FileManageTranslation>, IOrderedQueryable<FileManageTranslation>> orderBy = null
             , Func<IQueryable<FileManageTranslation>, IIncludableQueryable<FileManageTranslation, object>> include = null, bool tracking = false);

        Task<FileManageTranslation> GetAsync(Expression<Func<FileManageTranslation, bool>> where = null, Func<IQueryable<FileManageTranslation>
            , IOrderedQueryable<FileManageTranslation>> orderBy = null, Func<IQueryable<FileManageTranslation>, IIncludableQueryable<FileManageTranslation, object>> include = null, bool tracking = false);

        Task Insert(FileManageTranslation fileManageTranslation);
        Task<bool> ExistsAsync(Expression<Func<FileManageTranslation, bool>> where = null, Func<IQueryable<FileManageTranslation>,
            IIncludableQueryable<FileManageTranslation, object>> include = null);

        Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<FileManageTranslation, TResult>> selector = null,
                                          Expression<Func<FileManageTranslation, bool>> where = null,
                                          Func<IQueryable<FileManageTranslation>, IOrderedQueryable<FileManageTranslation>> orderBy = null,
                                          Func<IQueryable<FileManageTranslation>, IIncludableQueryable<FileManageTranslation, object>> include = null,
                                          bool disableTracking = true);
        void Update(FileManageTranslation fileManageTranslation);

        void Delete(FileManageTranslation fileManageTranslation);
        void DeleteAll(IEnumerable<FileManageTranslation> fileManageTranslations);
        Task Save();
    }
}
