﻿using Domain.DomainClasses.Product;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface IProductFeatureServices
    {
        Task<IEnumerable<ProductFeature>> GetAllAsyn(Expression<Func<ProductFeature, bool>> where = null, Func<IQueryable<ProductFeature>, IOrderedQueryable<ProductFeature>> orderBy = null
        , Func<IQueryable<ProductFeature>, IIncludableQueryable<ProductFeature, object>> include = null, bool tracking = false);
        Task InsertRange(List<ProductFeature> productGroupFeatures);
        Task Insert(ProductFeature productGroupFeature);
        Task<bool> ExistsAsync(Expression<Func<ProductFeature, bool>> where = null, Func<IQueryable<ProductFeature>,
            IIncludableQueryable<ProductFeature, object>> include = null);

        Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<ProductFeature, TResult>> selector = null,
                                          Expression<Func<ProductFeature, bool>> where = null,
                                          Func<IQueryable<ProductFeature>, IOrderedQueryable<ProductFeature>> orderBy = null,
                                          Func<IQueryable<ProductFeature>, IIncludableQueryable<ProductFeature, object>> include = null,
                                          bool disableTracking = true);

        Task<ProductFeature> GetAsync(Expression<Func<ProductFeature, bool>> where = null, Func<IQueryable<ProductFeature>
           , IOrderedQueryable<ProductFeature>> orderBy = null, Func<IQueryable<ProductFeature>, IIncludableQueryable<ProductFeature, object>> include = null, bool tracking = false);

        void DeleteRange(List<ProductFeature> productGroupFeatures);
        Task Save();
    }
}
