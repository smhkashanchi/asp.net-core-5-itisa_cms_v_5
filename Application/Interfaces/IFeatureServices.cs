﻿using Domain.DomainClasses.Feature;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface IFeatureServices
    {
        Task<IEnumerable<Feature>> GetAllAsyn(Expression<Func<Feature, bool>> where = null, Func<IQueryable<Feature>, IOrderedQueryable<Feature>> orderBy = null
          , Func<IQueryable<Feature>, IIncludableQueryable<Feature, object>> include = null, bool tracking = false);

        Task<Feature> GetAsync(Expression<Func<Feature, bool>> where = null, Func<IQueryable<Feature>
            , IOrderedQueryable<Feature>> orderBy = null, Func<IQueryable<Feature>, IIncludableQueryable<Feature, object>> include = null, bool tracking = false);

        Task Insert(Feature feature);
        Task<bool> ExistsAsync(Expression<Func<Feature, bool>> where = null, Func<IQueryable<Feature>,
            IIncludableQueryable<Feature, object>> include = null);

        Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<Feature, TResult>> selector = null,
                                          Expression<Func<Feature, bool>> where = null,
                                          Func<IQueryable<Feature>, IOrderedQueryable<Feature>> orderBy = null,
                                          Func<IQueryable<Feature>, IIncludableQueryable<Feature, object>> include = null,
                                          bool disableTracking = true);
        void Update(Feature feature);

        void Delete(Feature feature);
        Task Save();
    }
}
