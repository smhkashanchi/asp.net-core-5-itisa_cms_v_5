﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels
{
    public class TestCountry
    {
        public int ProductoId { get; set; }
        [Required]
        public string Value { get; set; }
    }
}
