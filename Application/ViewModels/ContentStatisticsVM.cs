﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels
{
    public class ContentStatisticsVM
    {
        public Int16 VisitCount { get; set; }
        public Int16 LikeCount { get; set; }
        public Int16 Votes { get; set; }
        public Int16 SumVotes { get; set; }
        public Int16 AvgVotes { get; set; }
    }
}
