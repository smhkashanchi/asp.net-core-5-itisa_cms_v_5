﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels
{
    public class ProductFeatureVM
    {
        public int ProductId { get; set; }
        public int FeatureId { get; set; }
        public List<string> FeatureReplyIds { get; set; }
        public bool IsVariable { get; set; }
    }
}
