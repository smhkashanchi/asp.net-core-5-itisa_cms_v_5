﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ViewModels
{
    public class PagingInfo
    {
        public int TotalItem { get; set; }
        public int ItemPerPage { get; set; }
        public int CurrentPage { get; set; }
        public int TotalPage => (int)Math.Ceiling((decimal)TotalItem / ItemPerPage);
        public string UrlParam { get; set; }
    }
}
