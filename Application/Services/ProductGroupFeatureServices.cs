﻿using Application.Interfaces;

using Domain.DomainClasses.Product;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class ProductGroupFeatureServices : IProductGroupFeatureServices
    {
        private IProductGroupFeatureRepository _productGroupFeatureRepository;
        public ProductGroupFeatureServices(IProductGroupFeatureRepository productGroupFeatureRepository)
        {
            _productGroupFeatureRepository = productGroupFeatureRepository;
        }
        public void DeleteRange(List<ProductGroupFeature> productGroupFeatures)
        {
            _productGroupFeatureRepository.DeleteRange(productGroupFeatures);
        }

        public async Task<bool> ExistsAsync(Expression<Func<ProductGroupFeature, bool>> where = null, Func<IQueryable<ProductGroupFeature>, IIncludableQueryable<ProductGroupFeature, object>> include = null)
        {
            return await _productGroupFeatureRepository.ExistsAsync(where, include);
        }

        public async Task<IEnumerable<ProductGroupFeature>> GetAllAsyn(Expression<Func<ProductGroupFeature, bool>> where = null, Func<IQueryable<ProductGroupFeature>, IOrderedQueryable<ProductGroupFeature>> orderBy = null, Func<IQueryable<ProductGroupFeature>, IIncludableQueryable<ProductGroupFeature, object>> include = null, bool tracking = false)
        {
            return await _productGroupFeatureRepository.GetAllAsyn(where,orderBy, include);
        }

        public async Task<ProductGroupFeature> GetAsync(Expression<Func<ProductGroupFeature, bool>> where = null, Func<IQueryable<ProductGroupFeature>, IOrderedQueryable<ProductGroupFeature>> orderBy = null, Func<IQueryable<ProductGroupFeature>, IIncludableQueryable<ProductGroupFeature, object>> include = null, bool tracking = false)
        {
            return await _productGroupFeatureRepository.GetAsync(where,orderBy,include);
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<ProductGroupFeature, TResult>> selector = null, Expression<Func<ProductGroupFeature, bool>> where = null, Func<IQueryable<ProductGroupFeature>, IOrderedQueryable<ProductGroupFeature>> orderBy = null, Func<IQueryable<ProductGroupFeature>, IIncludableQueryable<ProductGroupFeature, object>> include = null, bool disableTracking = true)
        {
            return await _productGroupFeatureRepository.GetWithIncludeAsync(selector, where, orderBy, include);
        }

        public async Task InsertRange(List<ProductGroupFeature> productGroupFeatures)
        {
            await _productGroupFeatureRepository.InsertRange(productGroupFeatures);
        }

        public async Task Save()
        {
            await _productGroupFeatureRepository.Save();
        }
    }
}
