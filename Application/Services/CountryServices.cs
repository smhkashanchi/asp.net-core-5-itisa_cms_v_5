﻿using Application.Interfaces;

using Domain.DomainClasses.Region;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class CountryServices : ICountryServices
    {
        private ICountryRepository _countryRepository;
        public CountryServices(ICountryRepository countryRepository)
        {
            _countryRepository = countryRepository;
        }
        public void Delete(Country country)
        {
            _countryRepository.Delete(country);
        }

        public void DeleteAll(IEnumerable<Country> countries)
        {
            _countryRepository.DeleteAll(countries);
        }

        public async Task<bool> ExistsAsync(Expression<Func<Country, bool>> where = null, Func<IQueryable<Country>, IIncludableQueryable<Country, object>> include = null)
        {
            return await _countryRepository.ExistsAsync(where, include);
        }

        public async Task<IEnumerable<Country>> GetAllAsyn(Expression<Func<Country, bool>> where = null, Func<IQueryable<Country>, IOrderedQueryable<Country>> orderBy = null, Func<IQueryable<Country>, IIncludableQueryable<Country, object>> include = null, bool tracking = false)
        {
            return await _countryRepository.GetAllAsyn(where, orderBy, include);
        }

        public async Task<Country> GetAsync(Expression<Func<Country, bool>> where = null, Func<IQueryable<Country>, IOrderedQueryable<Country>> orderBy = null, Func<IQueryable<Country>, IIncludableQueryable<Country, object>> include = null, bool tracking = false)
        {
            return await _countryRepository.GetAsync(where, orderBy, include);
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<Country, TResult>> selector = null, Expression<Func<Country, bool>> where = null, Func<IQueryable<Country>, IOrderedQueryable<Country>> orderBy = null, Func<IQueryable<Country>, IIncludableQueryable<Country, object>> include = null, bool disableTracking = true)
        {
            return await _countryRepository.GetWithIncludeAsync(selector, where, orderBy, include);
        }

        public async Task Insert(Country country)
        {
            await _countryRepository.Insert(country);
        }

        public async Task Save()
        {
            await _countryRepository.Save();
        }

        public async Task Update(Country country)
        {
            await _countryRepository.Update(country);
        }
    }
}
