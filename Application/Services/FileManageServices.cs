﻿using Application.Interfaces;

using Domain.DomainClasses.FilesManage;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class FileManageServices : IFileManageServices
    {
        private IFileManageRepository _fileManageRepository;
        public FileManageServices(IFileManageRepository fileManageRepository)
        {
            _fileManageRepository = fileManageRepository;
        }
        public void Delete(FileManage fileManage)
        {
            _fileManageRepository.Delete(fileManage);
        }

        public async Task<bool> ExistsAsync(Expression<Func<FileManage, bool>> where = null, Func<IQueryable<FileManage>, IIncludableQueryable<FileManage, object>> include = null)
        {
            return await _fileManageRepository.ExistsAsync(where, include);
        }

        public async Task<IEnumerable<FileManage>> GetAllAsyn(Expression<Func<FileManage, bool>> where = null, Func<IQueryable<FileManage>, IOrderedQueryable<FileManage>> orderBy = null, Func<IQueryable<FileManage>, IIncludableQueryable<FileManage, object>> include = null, bool tracking = false)
        {
            return await _fileManageRepository.GetAllAsyn(where, orderBy, include);
        }

        public async Task<FileManage> GetAsync(Expression<Func<FileManage, bool>> where = null, Func<IQueryable<FileManage>, IOrderedQueryable<FileManage>> orderBy = null, Func<IQueryable<FileManage>, IIncludableQueryable<FileManage, object>> include = null, bool tracking = false)
        {
            return await _fileManageRepository.GetAsync(where, orderBy, include);
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<FileManage, TResult>> selector = null, Expression<Func<FileManage, bool>> where = null, Func<IQueryable<FileManage>, IOrderedQueryable<FileManage>> orderBy = null, Func<IQueryable<FileManage>, IIncludableQueryable<FileManage, object>> include = null, bool disableTracking = true)
        {
            return await _fileManageRepository.GetWithIncludeAsync(selector, where, orderBy, include);
        }

        public async Task Insert(FileManage fileManage)
        {
            await _fileManageRepository.Insert(fileManage);
        }

        public async Task Save()
        {
            await _fileManageRepository.Save();
        }

        public void Update(FileManage fileManage)
        {
            _fileManageRepository.Update(fileManage);
        }
    }
}
