﻿using Application.Interfaces;

using Domain.DomainClasses;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class LanguageServices : ILanguageServices
    {
        private ILanguageRepository _languageRepository;
        public LanguageServices(ILanguageRepository languageRepository)
        {
            _languageRepository = languageRepository;
        }
        public void Delete(Language language)
        {
            _languageRepository.Delete(language);
        }

        public async Task<IEnumerable<Language>> GetAll(Expression<Func<Language, bool>> where = null, Func<IQueryable<Language>,
            IOrderedQueryable<Language>> orderBy = null, Func<IQueryable<Language>, IIncludableQueryable<Language, object>> include = null, bool tracking = false)
        {
            return await _languageRepository.GetAll(where,orderBy,include);
        }

        public async Task<IEnumerable<Language>> GetAllIsActive()
        {
            return await _languageRepository.GetAllIsActive();
        }

        public async Task<Language> GetOne(string Id)
        {
            return await _languageRepository.GetOne(Id);
        }

        public Task Insert(Language language)
        {
            _languageRepository.Insert(language);
            return Task.CompletedTask;
        }

        public async Task<bool> IsExist(string Id)
        {
            return await _languageRepository.IsExist(Id);
        }

        public async Task Save()
        {
            await _languageRepository.Save();
        }

        public void Update(Language language)
        {
            _languageRepository.Update(language);
        }
    }
}
