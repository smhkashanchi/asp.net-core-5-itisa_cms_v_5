﻿using Application.Interfaces;

using Domain.DomainClasses.Product;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repository
{
    public class ProductFeatureServices : IProductFeatureServices
    {
        private IProductFeatureRepository _productFeatureRepository;
        public ProductFeatureServices(IProductFeatureRepository productFeatureRepository)
        {
            _productFeatureRepository = productFeatureRepository;
        }
        public void DeleteRange(List<ProductFeature> productGroupFeatures)
        {
            _productFeatureRepository.DeleteRange(productGroupFeatures);
        }

        public async Task<bool> ExistsAsync(Expression<Func<ProductFeature, bool>> where = null, Func<IQueryable<ProductFeature>, IIncludableQueryable<ProductFeature, object>> include = null)
        {
            return await _productFeatureRepository.ExistsAsync(where, include);
        }

        public async Task<IEnumerable<ProductFeature>> GetAllAsyn(Expression<Func<ProductFeature, bool>> where = null, Func<IQueryable<ProductFeature>, IOrderedQueryable<ProductFeature>> orderBy = null, Func<IQueryable<ProductFeature>, IIncludableQueryable<ProductFeature, object>> include = null, bool tracking = false)
        {
            return await _productFeatureRepository.GetAllAsyn(where, orderBy, include);
        }

        public async Task<ProductFeature> GetAsync(Expression<Func<ProductFeature, bool>> where = null, Func<IQueryable<ProductFeature>, IOrderedQueryable<ProductFeature>> orderBy = null, Func<IQueryable<ProductFeature>, IIncludableQueryable<ProductFeature, object>> include = null, bool tracking = false)
        {
            return await _productFeatureRepository.GetAsync(where, orderBy, include);
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<ProductFeature, TResult>> selector = null, Expression<Func<ProductFeature, bool>> where = null, Func<IQueryable<ProductFeature>, IOrderedQueryable<ProductFeature>> orderBy = null, Func<IQueryable<ProductFeature>, IIncludableQueryable<ProductFeature, object>> include = null, bool disableTracking = true)
        {
           return await _productFeatureRepository.GetWithIncludeAsync(selector,where, orderBy, include);
        }

        public async Task InsertRange(List<ProductFeature> productGroupFeatures)
        {
            await _productFeatureRepository.InsertRange(productGroupFeatures);
        }

        public async Task Insert(ProductFeature productGroupFeature)
        {
            await _productFeatureRepository.Insert(productGroupFeature);
        }

        public async Task Save()
        {
            await _productFeatureRepository.Save();
        }
    }
}
