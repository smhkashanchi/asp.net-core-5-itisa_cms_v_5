﻿using Application.Interfaces;

using Domain.DomainClasses.CoWorker;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class CoWorkerTranslationServices : ICoWorkerTranslationServices
    {
        private ICoWorkerTranslationRepository _coWorkerTranslationRepository;
        public CoWorkerTranslationServices(ICoWorkerTranslationRepository coWorkerTranslationRepository)
        {
            _coWorkerTranslationRepository = coWorkerTranslationRepository;
        }
        public void Delete(CoWorkerTranslation coWorkerTranslation)
        {
            _coWorkerTranslationRepository.Delete(coWorkerTranslation);
        }

        public void DeleteAll(IEnumerable<CoWorkerTranslation> coWorkerTranslations)
        {
            _coWorkerTranslationRepository.DeleteAll(coWorkerTranslations);
        }

        public async Task<bool> ExistsAsync(Expression<Func<CoWorkerTranslation, bool>> where = null, Func<IQueryable<CoWorkerTranslation>, IIncludableQueryable<CoWorkerTranslation, object>> include = null)
        {
            return await _coWorkerTranslationRepository.ExistsAsync(where, include);
        }

        public async Task<IEnumerable<CoWorkerTranslation>> GetAllAsyn(Expression<Func<CoWorkerTranslation, bool>> where = null, Func<IQueryable<CoWorkerTranslation>, IOrderedQueryable<CoWorkerTranslation>> orderBy = null, Func<IQueryable<CoWorkerTranslation>, IIncludableQueryable<CoWorkerTranslation, object>> include = null, bool tracking = false)
        {
            return await _coWorkerTranslationRepository.GetAllAsyn(where, orderBy, include);
        }

        public async Task<CoWorkerTranslation> GetAsync(Expression<Func<CoWorkerTranslation, bool>> where = null, Func<IQueryable<CoWorkerTranslation>, IOrderedQueryable<CoWorkerTranslation>> orderBy = null, Func<IQueryable<CoWorkerTranslation>, IIncludableQueryable<CoWorkerTranslation, object>> include = null, bool tracking = false)
        {
            return await _coWorkerTranslationRepository.GetAsync(where, orderBy, include);
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<CoWorkerTranslation, TResult>> selector = null, Expression<Func<CoWorkerTranslation, bool>> where = null, Func<IQueryable<CoWorkerTranslation>, IOrderedQueryable<CoWorkerTranslation>> orderBy = null, Func<IQueryable<CoWorkerTranslation>, IIncludableQueryable<CoWorkerTranslation, object>> include = null, bool disableTracking = true)
        {
            return await _coWorkerTranslationRepository.GetWithIncludeAsync(selector, where, orderBy, include);
        }

        public async Task Insert(CoWorkerTranslation coWorkerTranslation)
        {
            await _coWorkerTranslationRepository.Insert(coWorkerTranslation);
        }

        public async Task Save()
        {
            await _coWorkerTranslationRepository.Save();
        }

        public async Task Update(CoWorkerTranslation coWorkerTranslation)
        {
            await _coWorkerTranslationRepository.Update(coWorkerTranslation);
        }
    }
}
