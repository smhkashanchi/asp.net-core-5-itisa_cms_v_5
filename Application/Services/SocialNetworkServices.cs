﻿using Application.Interfaces;

using Domain.DomainClasses.SocialNetwork;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class SocialNetworkServices : ISocialNetworkServices
    {
        private ISocialNetworkRepository _socialNetworkRepository;
        public SocialNetworkServices(ISocialNetworkRepository socialNetworkRepository)
        {
            _socialNetworkRepository = socialNetworkRepository;
        }
        public void DeleteAsync(SocialNetwork socialNetwork)
        {
            _socialNetworkRepository.DeleteAsync(socialNetwork);
        }

        public async Task<bool> ExistsAsync(Expression<Func<SocialNetwork, bool>> where = null, Func<IQueryable<SocialNetwork>, IIncludableQueryable<SocialNetwork, object>> include = null)
        {
            return await _socialNetworkRepository.ExistsAsync(where, include);
        }

        public async Task<IEnumerable<SocialNetwork>> GetAllAsyn(Expression<Func<SocialNetwork, bool>> where = null, Func<IQueryable<SocialNetwork>, IOrderedQueryable<SocialNetwork>> orderBy = null, Func<IQueryable<SocialNetwork>, IIncludableQueryable<SocialNetwork, object>> include = null, bool tracking = false)
        {
            return await _socialNetworkRepository.GetAllAsyn(where, orderBy, include);
        }

        public async Task<SocialNetwork> GetAsync(Expression<Func<SocialNetwork, bool>> where = null, Func<IQueryable<SocialNetwork>, IOrderedQueryable<SocialNetwork>> orderBy = null, Func<IQueryable<SocialNetwork>, IIncludableQueryable<SocialNetwork, object>> include = null, bool tracking = false)
        {
            return await _socialNetworkRepository.GetAsync(where, orderBy, include);
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<SocialNetwork, TResult>> selector = null, Expression<Func<SocialNetwork, bool>> where = null, Func<IQueryable<SocialNetwork>, IOrderedQueryable<SocialNetwork>> orderBy = null, Func<IQueryable<SocialNetwork>, IIncludableQueryable<SocialNetwork, object>> include = null, bool disableTracking = true)
        {
            return await _socialNetworkRepository.GetWithIncludeAsync(selector, where, orderBy, include);
        }

        public async Task Insert(SocialNetwork socialNetwork)
        {
            await _socialNetworkRepository.Insert(socialNetwork);
        }

        public async Task Save()
        {
            await _socialNetworkRepository.Save();
        }

        public async Task Update(SocialNetwork socialNetwork)
        {
           await _socialNetworkRepository.Update(socialNetwork);
        }
    }
}
