﻿using Application.Interfaces;

using Domain.DomainClasses.Connection;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class ConnectionServices : IConnectionServices
    {
        private IConnectionRepository _connectionRepository;
        public ConnectionServices(IConnectionRepository connectionRepository)
        {
            _connectionRepository = connectionRepository;
        }
        public void Delete(Connection connection)
        {
            _connectionRepository.Delete(connection);
        }

        public async Task<bool> ExistsAsync(Expression<Func<Connection, bool>> where = null, Func<IQueryable<Connection>, IIncludableQueryable<Connection, object>> include = null)
        {
            return await _connectionRepository.ExistsAsync(where, include);
        }

        public async Task<Connection> GetAsync(Expression<Func<Connection, bool>> where = null, Func<IQueryable<Connection>, IOrderedQueryable<Connection>> orderBy = null, Func<IQueryable<Connection>, IIncludableQueryable<Connection, object>> include = null, bool tracking = false)
        {
            return await _connectionRepository.GetAsync(where, orderBy, include);
        }

        public async Task InsertConnection(Connection connection)
        {
            await _connectionRepository.InsertConnection(connection);
        }

        public async Task Save()
        {
            await _connectionRepository.Save();
        }

        public void UpdateConnection(Connection connection)
        {
            _connectionRepository.UpdateConnection(connection);
        }
    }
}
