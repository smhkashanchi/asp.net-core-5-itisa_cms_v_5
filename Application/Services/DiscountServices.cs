﻿using Application.Interfaces;

using Domain.DomainClasses.Product;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class DiscountServices : IDiscountServices
    {
        private IDiscountRepository _discountRepository;
        public DiscountServices(IDiscountRepository discountRepository)
        {
            _discountRepository = discountRepository;
        }
        public void Delete(Discount discount)
        {
            _discountRepository.Delete(discount);
        }

        public void DeleteAll(IEnumerable<Discount> discounts)
        {
            _discountRepository.DeleteAll(discounts);
        }

        public async Task<bool> ExistsAsync(Expression<Func<Discount, bool>> where = null, Func<IQueryable<Discount>, IIncludableQueryable<Discount, object>> include = null)
        {
            return await _discountRepository.ExistsAsync(where, include);
        }

        public async Task<IEnumerable<Discount>> GetAllAsyn(Expression<Func<Discount, bool>> where = null, Func<IQueryable<Discount>, IOrderedQueryable<Discount>> orderBy = null, Func<IQueryable<Discount>, IIncludableQueryable<Discount, object>> include = null, bool tracking = false)
        {
            return await _discountRepository.GetAllAsyn(where, orderBy, include);
        }

        public async Task<Discount> GetAsync(Expression<Func<Discount, bool>> where = null, Func<IQueryable<Discount>, IOrderedQueryable<Discount>> orderBy = null, Func<IQueryable<Discount>, IIncludableQueryable<Discount, object>> include = null, bool tracking = false)
        {
            return await _discountRepository.GetAsync(where, orderBy, include);
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<Discount, TResult>> selector = null, Expression<Func<Discount, bool>> where = null, Func<IQueryable<Discount>, IOrderedQueryable<Discount>> orderBy = null, Func<IQueryable<Discount>, IIncludableQueryable<Discount, object>> include = null, bool disableTracking = true)
        {
            return await _discountRepository.GetWithIncludeAsync(selector, where, orderBy, include);
        }

        public async Task Insert(Discount discount)
        {
            await _discountRepository.Insert(discount);
        }

        public async Task Save()
        {
            await _discountRepository.Save();
        }

        public async Task Update(Discount discount)
        {
            await _discountRepository.Update(discount);
        }
    }
}
