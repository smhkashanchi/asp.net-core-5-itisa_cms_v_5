﻿using Application.Interfaces;

using Domain.DomainClasses.SlideShow;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class SlideShowGroupServices : ISlideShowGroupServices
    {
        private ISlideShowGroupRepository _slideShowGroupRepository;
        public SlideShowGroupServices(ISlideShowGroupRepository slideShowGroupRepository)
        {
            _slideShowGroupRepository = slideShowGroupRepository;
        }
        public void DeleteAsync(SlideShowGroup slideShowGroup)
        {
            _slideShowGroupRepository.DeleteAsync(slideShowGroup);
        }

        public async Task<bool> ExistsAsync(Expression<Func<SlideShowGroup, bool>> where = null, Func<IQueryable<SlideShowGroup>, IIncludableQueryable<SlideShowGroup, object>> include = null)
        {
            return await _slideShowGroupRepository.ExistsAsync(where, include);
        }

        public async Task<IEnumerable<SlideShowGroup>> GetAllAsyn(Expression<Func<SlideShowGroup, bool>> where = null, Func<IQueryable<SlideShowGroup>, IOrderedQueryable<SlideShowGroup>> orderBy = null, Func<IQueryable<SlideShowGroup>, IIncludableQueryable<SlideShowGroup, object>> include = null, bool tracking = false)
        {
            return await _slideShowGroupRepository.GetAllAsyn(where, orderBy, include);
        }

        public async Task<SlideShowGroup> GetAsync(Expression<Func<SlideShowGroup, bool>> where = null, Func<IQueryable<SlideShowGroup>, IOrderedQueryable<SlideShowGroup>> orderBy = null, Func<IQueryable<SlideShowGroup>, IIncludableQueryable<SlideShowGroup, object>> include = null, bool tracking = false)
        {
            return await _slideShowGroupRepository.GetAsync(where, orderBy, include);
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<SlideShowGroup, TResult>> selector = null, Expression<Func<SlideShowGroup, bool>> where = null, Func<IQueryable<SlideShowGroup>, IOrderedQueryable<SlideShowGroup>> orderBy = null, Func<IQueryable<SlideShowGroup>, IIncludableQueryable<SlideShowGroup, object>> include = null, bool disableTracking = true)
        {
            return await _slideShowGroupRepository.GetWithIncludeAsync(selector, where, orderBy, include);
        }

        public async Task Insert(SlideShowGroup slideShowGroup)
        {
            await _slideShowGroupRepository.Insert(slideShowGroup);
        }

        public async Task Save()
        {
            await _slideShowGroupRepository.Save();
        }

        public async Task Update(SlideShowGroup slideShowGroup)
        {
            await _slideShowGroupRepository.Update(slideShowGroup);
        }
    }
}
