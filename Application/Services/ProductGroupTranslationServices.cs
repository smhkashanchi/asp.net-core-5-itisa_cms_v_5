﻿using Application.Interfaces;

using Domain.DomainClasses.Product;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class ProductGroupTranslationServices : IProductGroupTranslationServices
    {
        private IProductGroupTranslaionRepository _productGroupTranslaionRepository;
        public ProductGroupTranslationServices(IProductGroupTranslaionRepository productGroupTranslaionRepository)
        {
            _productGroupTranslaionRepository = productGroupTranslaionRepository;
        }
        public void Delete(ProductGroupTranslation productGroupTranslation)
        {
            _productGroupTranslaionRepository.Delete(productGroupTranslation);
        }

        public void DeleteAll(IEnumerable<ProductGroupTranslation> productGroupTranslations)
        {
            _productGroupTranslaionRepository.DeleteAll(productGroupTranslations);
        }

        public async Task<bool> ExistsAsync(Expression<Func<ProductGroupTranslation, bool>> where = null, Func<IQueryable<ProductGroupTranslation>, IIncludableQueryable<ProductGroupTranslation, object>> include = null)
        {
            return await _productGroupTranslaionRepository.ExistsAsync(where, include);
        }

        public async Task<IEnumerable<ProductGroupTranslation>> GetAllAsyn(Expression<Func<ProductGroupTranslation, bool>> where = null, Func<IQueryable<ProductGroupTranslation>, IOrderedQueryable<ProductGroupTranslation>> orderBy = null, Func<IQueryable<ProductGroupTranslation>, IIncludableQueryable<ProductGroupTranslation, object>> include = null, bool tracking = false)
        {
            return await _productGroupTranslaionRepository.GetAllAsyn(where, orderBy, include);
        }

        public async Task<ProductGroupTranslation> GetAsync(Expression<Func<ProductGroupTranslation, bool>> where = null, Func<IQueryable<ProductGroupTranslation>, IOrderedQueryable<ProductGroupTranslation>> orderBy = null, Func<IQueryable<ProductGroupTranslation>, IIncludableQueryable<ProductGroupTranslation, object>> include = null, bool tracking = false)
        {
            return await _productGroupTranslaionRepository.GetAsync(where, orderBy, include);
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<ProductGroupTranslation, TResult>> selector = null, Expression<Func<ProductGroupTranslation, bool>> where = null, Func<IQueryable<ProductGroupTranslation>, IOrderedQueryable<ProductGroupTranslation>> orderBy = null, Func<IQueryable<ProductGroupTranslation>, IIncludableQueryable<ProductGroupTranslation, object>> include = null, bool disableTracking = true)
        {
            return await _productGroupTranslaionRepository.GetWithIncludeAsync(selector, where, orderBy, include);
        }

        public async Task Insert(ProductGroupTranslation productGroupTranslation)
        {
            await _productGroupTranslaionRepository.Insert(productGroupTranslation);
        }

        public async Task InsertRange(IEnumerable<ProductGroupTranslation> productGroupTranslations)
        {
            await _productGroupTranslaionRepository.InsertRange(productGroupTranslations);
        }

        public async Task Save()
        {
            await _productGroupTranslaionRepository.Save();
        }

        public void Update(ProductGroupTranslation productGroupTranslation)
        {
            _productGroupTranslaionRepository.Update(productGroupTranslation);
        }
    }
}
