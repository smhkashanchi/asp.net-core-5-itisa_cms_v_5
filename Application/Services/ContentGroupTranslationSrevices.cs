﻿using Application.Interfaces;

using Domain.DomainClasses.Content;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class ContentGroupTranslationSrevices : IContentGroupTranslationSrevices
    {
        private IContentGroupTranslationRepository _contentGroupTranslationRepository;
        public ContentGroupTranslationSrevices(IContentGroupTranslationRepository contentGroupTranslationRepository)
        {
            _contentGroupTranslationRepository = contentGroupTranslationRepository;
        }

        public void DeleteAllAsync(IEnumerable<ContentGroupTranslation> contentGroupTranslations)
        {
            _contentGroupTranslationRepository.DeleteAllAsync(contentGroupTranslations);
        }

        public void DeleteAsync(ContentGroupTranslation contentGroupTranslation)
        {
            _contentGroupTranslationRepository.DeleteAsync(contentGroupTranslation);
        }

        public async Task<bool> ExistsAsync(Expression<Func<ContentGroupTranslation, bool>> where = null, Func<IQueryable<ContentGroupTranslation>, IIncludableQueryable<ContentGroupTranslation, object>> include = null)
        {
            return await _contentGroupTranslationRepository.ExistsAsync(where, include);
        }

        public async Task<IEnumerable<ContentGroupTranslation>> GetAllAsyn(Expression<Func<ContentGroupTranslation, bool>> where = null, Func<IQueryable<ContentGroupTranslation>, IOrderedQueryable<ContentGroupTranslation>> orderBy = null, Func<IQueryable<ContentGroupTranslation>, IIncludableQueryable<ContentGroupTranslation, object>> include = null, bool tracking = false)
        {
            return await _contentGroupTranslationRepository.GetAllAsyn(where,orderBy,include);
        }

        public async Task<ContentGroupTranslation> GetAsync(Expression<Func<ContentGroupTranslation, bool>> where = null, Func<IQueryable<ContentGroupTranslation>, IOrderedQueryable<ContentGroupTranslation>> orderBy = null, Func<IQueryable<ContentGroupTranslation>, IIncludableQueryable<ContentGroupTranslation, object>> include = null, bool tracking = false)
        {
            return await _contentGroupTranslationRepository.GetAsync(where, orderBy, include);
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<ContentGroupTranslation, TResult>> selector = null, Expression<Func<ContentGroupTranslation, bool>> where = null, Func<IQueryable<ContentGroupTranslation>, IOrderedQueryable<ContentGroupTranslation>> orderBy = null, Func<IQueryable<ContentGroupTranslation>, IIncludableQueryable<ContentGroupTranslation, object>> include = null, bool disableTracking = true)
        {
            return await _contentGroupTranslationRepository.GetWithIncludeAsync(selector,where, orderBy, include);
        }

        public async Task Insert(ContentGroupTranslation contentGroupTranslation)
        {
            await _contentGroupTranslationRepository.Insert(contentGroupTranslation);
        }

        public async Task Save()
        {
            await _contentGroupTranslationRepository.Save();
        }

        public async Task Update(ContentGroupTranslation contentGroupTranslation)
        {
           await _contentGroupTranslationRepository.Update(contentGroupTranslation);
        }
    }
}
