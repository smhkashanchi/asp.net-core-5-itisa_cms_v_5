﻿using Application.Interfaces;

using Domain.DomainClasses.Color;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class ColorServices : IColorServices
    {
        private IColorRepository _colorRepository;
        public ColorServices(IColorRepository colorRepository)
        {
            _colorRepository = colorRepository;
        }

        public void Delete(Color color)
        {
            _colorRepository.Delete(color);
        }

        public async Task<Color> GetAsync(Expression<Func<Color, bool>> where = null, Func<IQueryable<Color>, IOrderedQueryable<Color>> orderBy = null, Func<IQueryable<Color>, IIncludableQueryable<Color, object>> include = null, bool tracking = false)
        {
            return await _colorRepository.GetAsync(where, orderBy, include);
        }

        public async Task InsertColor(Color color)
        {
           await _colorRepository.InsertColor(color);
        }

        public async Task Save()
        {
           await _colorRepository.Save();
        }

        public void Update(Color color)
        {
            _colorRepository.UpdateColor(color);
        }
    }
}
