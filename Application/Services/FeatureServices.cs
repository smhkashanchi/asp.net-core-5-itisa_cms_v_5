﻿using Application.Interfaces;

using Domain.DomainClasses.Feature;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class FeatureServices : IFeatureServices
    {
        private IFeatureRepository _featureRepository;
        public FeatureServices(IFeatureRepository featureRepository)
        {
            _featureRepository = featureRepository;
        }
        public void Delete(Feature feature)
        {
            _featureRepository.Delete(feature);
        }

        public async Task<bool> ExistsAsync(Expression<Func<Feature, bool>> where = null, Func<IQueryable<Feature>, IIncludableQueryable<Feature, object>> include = null)
        {
            return await _featureRepository.ExistsAsync(where, include);
        }

        public async Task<IEnumerable<Feature>> GetAllAsyn(Expression<Func<Feature, bool>> where = null, Func<IQueryable<Feature>, IOrderedQueryable<Feature>> orderBy = null, Func<IQueryable<Feature>, IIncludableQueryable<Feature, object>> include = null, bool tracking = false)
        {
            return await _featureRepository.GetAllAsyn(where, orderBy, include);
        }

        public async Task<Feature> GetAsync(Expression<Func<Feature, bool>> where = null, Func<IQueryable<Feature>, IOrderedQueryable<Feature>> orderBy = null, Func<IQueryable<Feature>, IIncludableQueryable<Feature, object>> include = null, bool tracking = false)
        {
            return await _featureRepository.GetAsync(where, orderBy, include);
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<Feature, TResult>> selector = null, Expression<Func<Feature, bool>> where = null, Func<IQueryable<Feature>, IOrderedQueryable<Feature>> orderBy = null, Func<IQueryable<Feature>, IIncludableQueryable<Feature, object>> include = null, bool disableTracking = true)
        {
            return await _featureRepository.GetWithIncludeAsync(selector, where, orderBy, include);
        }

        public async Task Insert(Feature feature)
        {
            await _featureRepository.Insert(feature);
        }

        public async Task Save()
        {
            await _featureRepository.Save();
        }

        public void Update(Feature feature)
        {
            _featureRepository.Update(feature);
        }
    }
}
