﻿using Application.Interfaces;

using Domain.DomainClasses.Connection;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class ConnectionTranslationServices : IConnectionTranslationServices
    {
        private IConnectionTranslationRepository _connectionTranslationRepository;
        public ConnectionTranslationServices(IConnectionTranslationRepository connectionTranslationRepository)
        {
            _connectionTranslationRepository = connectionTranslationRepository;
        }
        public void Delete(ConnectionTranslation connectionTranslation)
        {
            _connectionTranslationRepository.Delete(connectionTranslation);
        }

        public void DeleteAll(IEnumerable<ConnectionTranslation> connectionTranslation)
        {
            _connectionTranslationRepository.DeleteAll(connectionTranslation);
        }

        public async Task<bool> ExistsAsync(Expression<Func<ConnectionTranslation, bool>> where = null, Func<IQueryable<ConnectionTranslation>, IIncludableQueryable<ConnectionTranslation, object>> include = null)
        {
            return await _connectionTranslationRepository.ExistsAsync(where, include);
        }

        public async Task<Dictionary<string, int>> GetAllToDictionary(int Id)
        {
            return await _connectionTranslationRepository.GetAllToDictionary(Id);
        }

        public async Task<ConnectionTranslation> GetAsync(Expression<Func<ConnectionTranslation, bool>> where = null, Func<IQueryable<ConnectionTranslation>, IOrderedQueryable<ConnectionTranslation>> orderBy = null, Func<IQueryable<ConnectionTranslation>, IIncludableQueryable<ConnectionTranslation, object>> include = null, bool tracking = false)
        {
            return await _connectionTranslationRepository.GetAsync(where, orderBy, include); 
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<ConnectionTranslation, TResult>> selector = null, Expression<Func<ConnectionTranslation, bool>> where = null, Func<IQueryable<ConnectionTranslation>, IOrderedQueryable<ConnectionTranslation>> orderBy = null, Func<IQueryable<ConnectionTranslation>, IIncludableQueryable<ConnectionTranslation, object>> include = null, bool disableTracking = true)
        {
            return await _connectionTranslationRepository.GetWithIncludeAsync(selector, where, orderBy, include);
        }

        public async Task Insert(ConnectionTranslation connectionTranslation)
        {
            await _connectionTranslationRepository.Insert(connectionTranslation);
        }

        public async Task Save()
        {
            await _connectionTranslationRepository.Save();
        }

        public void Update(ConnectionTranslation connectionTranslation)
        {
            _connectionTranslationRepository.Update(connectionTranslation);
        }
    }
}
