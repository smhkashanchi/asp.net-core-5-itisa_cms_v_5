﻿using Application.Interfaces;

using Domain.DomainClasses.SlideShow;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class SlideShowServices : ISlideShowServices
    {
        private ISlideShowRepository _slideShowRepository;
        public SlideShowServices(ISlideShowRepository slideShowRepository)
        {
            _slideShowRepository = slideShowRepository;
        }
        public void DeleteAsync(SlideShow slideShow)
        {
            _slideShowRepository.DeleteAsync(slideShow);
        }

        public async Task<bool> ExistsAsync(Expression<Func<SlideShow, bool>> where = null, Func<IQueryable<SlideShow>, IIncludableQueryable<SlideShow, object>> include = null)
        {
            return await _slideShowRepository.ExistsAsync(where, include);
        }

        public async Task<IEnumerable<SlideShow>> GetAllAsyn(Expression<Func<SlideShow, bool>> where = null, Func<IQueryable<SlideShow>, IOrderedQueryable<SlideShow>> orderBy = null, Func<IQueryable<SlideShow>, IIncludableQueryable<SlideShow, object>> include = null, bool tracking = false)
        {
            return await _slideShowRepository.GetAllAsyn(where, orderBy, include);
        }

        public async Task<SlideShow> GetAsync(Expression<Func<SlideShow, bool>> where = null, Func<IQueryable<SlideShow>, IOrderedQueryable<SlideShow>> orderBy = null, Func<IQueryable<SlideShow>, IIncludableQueryable<SlideShow, object>> include = null, bool tracking = false)
        {
            return await _slideShowRepository.GetAsync(where, orderBy, include);
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<SlideShow, TResult>> selector = null, Expression<Func<SlideShow, bool>> where = null, Func<IQueryable<SlideShow>, IOrderedQueryable<SlideShow>> orderBy = null, Func<IQueryable<SlideShow>, IIncludableQueryable<SlideShow, object>> include = null, bool disableTracking = true)
        {
            return await _slideShowRepository.GetWithIncludeAsync(selector, where, orderBy, include);
        }

        public async Task Insert(SlideShow slideShow)
        {
            await _slideShowRepository.Insert(slideShow);
        }

        public async Task Save()
        {
            await _slideShowRepository.Save();
        }

        public async Task Update(SlideShow slideShow)
        {
           await _slideShowRepository.Update(slideShow);
        }
    }
}
