﻿using Application.Interfaces;

using Domain.DomainClasses.Gallery;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class GalleryTranslationServices : IGalleryTranslationServices
    {
        private IGalleryTranslationRepository _galleryTranslationRepository;
        public GalleryTranslationServices(IGalleryTranslationRepository galleryTranslationRepository)
        {
            _galleryTranslationRepository = galleryTranslationRepository;
        }
        public void Delete(GalleryTranslation galleryTranslation)
        {
            _galleryTranslationRepository.Delete(galleryTranslation);
        }

        public void DeleteAll(IEnumerable<GalleryTranslation> galleryTranslations)
        {
            _galleryTranslationRepository.DeleteAll(galleryTranslations);
        }

        public async Task<bool> ExistsAsync(Expression<Func<GalleryTranslation, bool>> where = null, Func<IQueryable<GalleryTranslation>, IIncludableQueryable<GalleryTranslation, object>> include = null)
        {
            return await _galleryTranslationRepository.ExistsAsync(where, include);
        }

        public async Task<IEnumerable<GalleryTranslation>> GetAll(Expression<Func<GalleryTranslation, bool>> where = null, Func<IQueryable<GalleryTranslation>, IOrderedQueryable<GalleryTranslation>> orderBy = null, Func<IQueryable<GalleryTranslation>, IIncludableQueryable<GalleryTranslation, object>> include = null, bool tracking = false)
        {
            return await _galleryTranslationRepository.GetAll(where, orderBy, include);
        }

        public async Task<GalleryTranslation> GetAsync(Expression<Func<GalleryTranslation, bool>> where = null, Func<IQueryable<GalleryTranslation>, IOrderedQueryable<GalleryTranslation>> orderBy = null, Func<IQueryable<GalleryTranslation>, IIncludableQueryable<GalleryTranslation, object>> include = null, bool tracking = false)
        {
            return await _galleryTranslationRepository.GetAsync(where, orderBy, include);
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<GalleryTranslation, TResult>> selector = null, Expression<Func<GalleryTranslation, bool>> where = null, Func<IQueryable<GalleryTranslation>, IOrderedQueryable<GalleryTranslation>> orderBy = null, Func<IQueryable<GalleryTranslation>, IIncludableQueryable<GalleryTranslation, object>> include = null, bool disableTracking = true)
        {
            return await _galleryTranslationRepository.GetWithIncludeAsync(selector, where, orderBy, include);
        }

        public async Task Insert(GalleryTranslation galleryTranslation)
        {
            await _galleryTranslationRepository.Insert(galleryTranslation);
        }

        public async Task Save()
        {
            await _galleryTranslationRepository.Save();
        }

        public void Update(GalleryTranslation galleryTranslation)
        {
            _galleryTranslationRepository.Update(galleryTranslation);
        }
    }
}
