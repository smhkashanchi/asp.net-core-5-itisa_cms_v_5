﻿using Application.Interfaces;

using Domain.DomainClasses.Gallery;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class GalleryGroupTranslationServices : IGalleryGroupTranslationServices
    {
        private IGalleryGroupTranslationRepository _galleryGroupTranslationRepository;
        public GalleryGroupTranslationServices(IGalleryGroupTranslationRepository galleryGroupTranslationRepository)
        {
            _galleryGroupTranslationRepository = galleryGroupTranslationRepository;
        }
        public void Delete(GalleryGroupTranslation galleryGroupTranslation)
        {
            _galleryGroupTranslationRepository.Delete(galleryGroupTranslation);
        }

        public async Task<bool> ExistsAsync(Expression<Func<GalleryGroupTranslation, bool>> where = null, Func<IQueryable<GalleryGroupTranslation>, IIncludableQueryable<GalleryGroupTranslation, object>> include = null)
        {
            return await _galleryGroupTranslationRepository.ExistsAsync(where, include);
        }

        public async Task<IEnumerable<GalleryGroupTranslation>> GetAllAsyn(Expression<Func<GalleryGroupTranslation, bool>> where = null, Func<IQueryable<GalleryGroupTranslation>, IOrderedQueryable<GalleryGroupTranslation>> orderBy = null, Func<IQueryable<GalleryGroupTranslation>, IIncludableQueryable<GalleryGroupTranslation, object>> include = null, bool tracking = false)
        {
            return await _galleryGroupTranslationRepository.GetAllAsyn(where, orderBy, include);
        }

        public async Task<GalleryGroupTranslation> GetAsync(Expression<Func<GalleryGroupTranslation, bool>> where = null, Func<IQueryable<GalleryGroupTranslation>, IOrderedQueryable<GalleryGroupTranslation>> orderBy = null, Func<IQueryable<GalleryGroupTranslation>, IIncludableQueryable<GalleryGroupTranslation, object>> include = null, bool tracking = false)
        {
            return await _galleryGroupTranslationRepository.GetAsync(where, orderBy, include);
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<GalleryGroupTranslation, TResult>> selector = null, Expression<Func<GalleryGroupTranslation, bool>> where = null, Func<IQueryable<GalleryGroupTranslation>, IOrderedQueryable<GalleryGroupTranslation>> orderBy = null, Func<IQueryable<GalleryGroupTranslation>, IIncludableQueryable<GalleryGroupTranslation, object>> include = null, bool disableTracking = true)
        {
            return await _galleryGroupTranslationRepository.GetWithIncludeAsync(selector, where, orderBy, include);
        }

        public async Task Insert(GalleryGroupTranslation galleryGroupTranslation)
        {
            await _galleryGroupTranslationRepository.Insert(galleryGroupTranslation);
        }

        public async Task Save()
        {
            await _galleryGroupTranslationRepository.Save();
        }

        public async Task Update(GalleryGroupTranslation galleryGroupTranslation)
        {
            await _galleryGroupTranslationRepository.Update(galleryGroupTranslation);
        }
    }
}
