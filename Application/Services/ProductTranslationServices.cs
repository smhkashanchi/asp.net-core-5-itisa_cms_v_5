﻿using Application.Interfaces;

using Domain.DomainClasses.Product;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class ProductTranslationServices : IProductTranslationServices
    {
        private IProductTranslationRepository _productTranslationRepository;
        public ProductTranslationServices(IProductTranslationRepository productTranslationRepository)
        {
            _productTranslationRepository = productTranslationRepository;
        }
        public void Delete(ProductTranslation productTranslation)
        {
            _productTranslationRepository.Delete(productTranslation);
        }

        public void DeleteAll(IEnumerable<ProductTranslation> productTranslations)
        {
            _productTranslationRepository.DeleteAll(productTranslations);
        }

        public async Task<bool> ExistsAsync(Expression<Func<ProductTranslation, bool>> where = null, Func<IQueryable<ProductTranslation>, IIncludableQueryable<ProductTranslation, object>> include = null)
        {
            return await _productTranslationRepository.ExistsAsync(where, include);
        }

        public async Task<IEnumerable<ProductTranslation>> GetAllAsyn(Expression<Func<ProductTranslation, bool>> where = null, Func<IQueryable<ProductTranslation>, IOrderedQueryable<ProductTranslation>> orderBy = null, Func<IQueryable<ProductTranslation>, IIncludableQueryable<ProductTranslation, object>> include = null, bool tracking = false)
        {
            return await _productTranslationRepository.GetAllAsyn(where, orderBy, include);
        }

        public async Task<ProductTranslation> GetAsync(Expression<Func<ProductTranslation, bool>> where = null, Func<IQueryable<ProductTranslation>, IOrderedQueryable<ProductTranslation>> orderBy = null, Func<IQueryable<ProductTranslation>, IIncludableQueryable<ProductTranslation, object>> include = null, bool tracking = false)
        {
            return await _productTranslationRepository.GetAsync(where, orderBy, include);
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<ProductTranslation, TResult>> selector = null, Expression<Func<ProductTranslation, bool>> where = null, Func<IQueryable<ProductTranslation>, IOrderedQueryable<ProductTranslation>> orderBy = null, Func<IQueryable<ProductTranslation>, IIncludableQueryable<ProductTranslation, object>> include = null, bool disableTracking = true)
        {
            return await _productTranslationRepository.GetWithIncludeAsync(selector, where, orderBy, include);
        }

        public async Task Insert(ProductTranslation productTranslation)
        {
            await _productTranslationRepository.Insert(productTranslation);
        }

        public async Task InsertRange(IEnumerable<ProductTranslation> productTranslations)
        {
            await _productTranslationRepository.InsertRange(productTranslations);
        }

        public async Task Save()
        {
            await _productTranslationRepository.Save();
        }

        public void Update(ProductTranslation productTranslation)
        {
            _productTranslationRepository.Update(productTranslation);
        }
    }
}
