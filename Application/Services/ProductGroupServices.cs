﻿using Application.Interfaces;

using Domain.DomainClasses.Product;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class ProductGroupServices : IProductGroupServices
    {
        private IProductGroupRepository _productGroupRepository;
        public ProductGroupServices(IProductGroupRepository productGroupRepository)
        {
            _productGroupRepository = productGroupRepository;
        }
        public void Delete(ProductGroup productGroup)
        {
            _productGroupRepository.Delete(productGroup);
        }

        public async Task<bool> ExistsAsync(Expression<Func<ProductGroup, bool>> where = null, Func<IQueryable<ProductGroup>, IIncludableQueryable<ProductGroup, object>> include = null)
        {
            return await _productGroupRepository.ExistsAsync(where, include);
        }

        public async Task<IEnumerable<ProductGroup>> GetAllAsyn(Expression<Func<ProductGroup, bool>> where = null, Func<IQueryable<ProductGroup>, IOrderedQueryable<ProductGroup>> orderBy = null, Func<IQueryable<ProductGroup>, IIncludableQueryable<ProductGroup, object>> include = null, bool tracking = false)
        {
            return await _productGroupRepository.GetAllAsyn(where, orderBy, include);
        }

        public async Task<ProductGroup> GetAsync(Expression<Func<ProductGroup, bool>> where = null, Func<IQueryable<ProductGroup>, IOrderedQueryable<ProductGroup>> orderBy = null, Func<IQueryable<ProductGroup>, IIncludableQueryable<ProductGroup, object>> include = null, bool tracking = false)
        {
            return await _productGroupRepository.GetAsync(where, orderBy, include);
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<ProductGroup, TResult>> selector = null, Expression<Func<ProductGroup, bool>> where = null, Func<IQueryable<ProductGroup>, IOrderedQueryable<ProductGroup>> orderBy = null, Func<IQueryable<ProductGroup>, IIncludableQueryable<ProductGroup, object>> include = null, bool disableTracking = true)
        {
            return await _productGroupRepository.GetWithIncludeAsync(selector, where, orderBy, include);
        }

        public async Task Insert(ProductGroup productGroup)
        {
            await _productGroupRepository.Insert(productGroup);
        }

        public async Task Save()
        {
            await _productGroupRepository.Save();
        }

        public void Update(ProductGroup productGroup)
        {
            _productGroupRepository.Update(productGroup);
        }
    }
}
