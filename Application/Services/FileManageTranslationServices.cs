﻿using Application.Interfaces;

using Domain.DomainClasses.FilesManage;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class FileManageTranslationServices : IFileManageTranslationServices
    {
        private IFileManageTranslationRepository _fileManageTranslationRepository;
        public FileManageTranslationServices(IFileManageTranslationRepository fileManageTranslationRepository)
        {
            _fileManageTranslationRepository = fileManageTranslationRepository;
        }
        public void Delete(FileManageTranslation fileManageTranslation)
        {
            _fileManageTranslationRepository.Delete(fileManageTranslation);
        }

        public void DeleteAll(IEnumerable<FileManageTranslation> fileManageTranslations)
        {
            _fileManageTranslationRepository.DeleteAll(fileManageTranslations);
        }

        public async Task<bool> ExistsAsync(Expression<Func<FileManageTranslation, bool>> where = null, Func<IQueryable<FileManageTranslation>, IIncludableQueryable<FileManageTranslation, object>> include = null)
        {
            return await _fileManageTranslationRepository.ExistsAsync(where, include);
        }

        public async Task<IEnumerable<FileManageTranslation>> GetAllAsyn(Expression<Func<FileManageTranslation, bool>> where = null, Func<IQueryable<FileManageTranslation>, IOrderedQueryable<FileManageTranslation>> orderBy = null, Func<IQueryable<FileManageTranslation>, IIncludableQueryable<FileManageTranslation, object>> include = null, bool tracking = false)
        {
            return await _fileManageTranslationRepository.GetAllAsyn(where, orderBy, include);
        }

        public async Task<FileManageTranslation> GetAsync(Expression<Func<FileManageTranslation, bool>> where = null, Func<IQueryable<FileManageTranslation>, IOrderedQueryable<FileManageTranslation>> orderBy = null, Func<IQueryable<FileManageTranslation>, IIncludableQueryable<FileManageTranslation, object>> include = null, bool tracking = false)
        {
            return await _fileManageTranslationRepository.GetAsync(where, orderBy, include);
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<FileManageTranslation, TResult>> selector = null, Expression<Func<FileManageTranslation, bool>> where = null, Func<IQueryable<FileManageTranslation>, IOrderedQueryable<FileManageTranslation>> orderBy = null, Func<IQueryable<FileManageTranslation>, IIncludableQueryable<FileManageTranslation, object>> include = null, bool disableTracking = true)
        {
            return await _fileManageTranslationRepository.GetWithIncludeAsync(selector, where, orderBy, include);
        }

        public async Task Insert(FileManageTranslation fileManageTranslation)
        {
            await _fileManageTranslationRepository.Insert(fileManageTranslation);
        }

        public async Task Save()
        {
            await _fileManageTranslationRepository.Save();
        }

        public void Update(FileManageTranslation fileManageTranslation)
        {
            _fileManageTranslationRepository.Update(fileManageTranslation);
        }
    }
}
