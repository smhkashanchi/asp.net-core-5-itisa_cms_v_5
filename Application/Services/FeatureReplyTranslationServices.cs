﻿using Application.Interfaces;

using Domain.DomainClasses.Feature;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class FeatureReplyTranslationServices : IFeatureReplyTranslationServices
    {
        private IFeatureReplyTranslationRepository _featureReplyTranslationRepository;
        public FeatureReplyTranslationServices(IFeatureReplyTranslationRepository featureReplyTranslationRepository)
        {
            _featureReplyTranslationRepository = featureReplyTranslationRepository;
        }
        public void Delete(FeatureReplyTranslation featureReplyTranslation)
        {
            _featureReplyTranslationRepository.Delete(featureReplyTranslation);
        }

        public void DeleteAll(IEnumerable<FeatureReplyTranslation> featureReplyTranslations)
        {
            _featureReplyTranslationRepository.DeleteAll(featureReplyTranslations);
        }

        public async Task<bool> ExistsAsync(Expression<Func<FeatureReplyTranslation, bool>> where = null, Func<IQueryable<FeatureReplyTranslation>, IIncludableQueryable<FeatureReplyTranslation, object>> include = null)
        {
            return await _featureReplyTranslationRepository.ExistsAsync(where, include);
        }

        public async Task<IEnumerable<FeatureReplyTranslation>> GetAllAsyn(Expression<Func<FeatureReplyTranslation, bool>> where = null, Func<IQueryable<FeatureReplyTranslation>, IOrderedQueryable<FeatureReplyTranslation>> orderBy = null, Func<IQueryable<FeatureReplyTranslation>, IIncludableQueryable<FeatureReplyTranslation, object>> include = null, bool tracking = false)
        {
            return await _featureReplyTranslationRepository.GetAllAsyn(where, orderBy, include);
        }

        public async Task<FeatureReplyTranslation> GetAsync(Expression<Func<FeatureReplyTranslation, bool>> where = null, Func<IQueryable<FeatureReplyTranslation>, IOrderedQueryable<FeatureReplyTranslation>> orderBy = null, Func<IQueryable<FeatureReplyTranslation>, IIncludableQueryable<FeatureReplyTranslation, object>> include = null, bool tracking = false)
        {
            return await _featureReplyTranslationRepository.GetAsync(where, orderBy, include);
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<FeatureReplyTranslation, TResult>> selector = null, Expression<Func<FeatureReplyTranslation, bool>> where = null, Func<IQueryable<FeatureReplyTranslation>, IOrderedQueryable<FeatureReplyTranslation>> orderBy = null, Func<IQueryable<FeatureReplyTranslation>, IIncludableQueryable<FeatureReplyTranslation, object>> include = null, bool disableTracking = true)
        {
            return await _featureReplyTranslationRepository.GetWithIncludeAsync(selector, where, orderBy, include);
        }

        public async Task Insert(FeatureReplyTranslation featureReplyTranslation)
        {
            await _featureReplyTranslationRepository.Insert(featureReplyTranslation);
        }

        public async Task Save()
        {
            await _featureReplyTranslationRepository.Save();
        }

        public void Update(FeatureReplyTranslation featureReplyTranslation)
        {
            _featureReplyTranslationRepository.Update(featureReplyTranslation);
        }
    }
}
