﻿using Application.Interfaces;

using Domain.DomainClasses.Product;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class ProductServices : IProductServices
    {
        private IProductRepository _productRepository;
        public ProductServices(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }
        public async Task Delete(Product product)
        {
           await _productRepository.Delete(product);
        }

        public async Task<bool> ExistsAsync(Expression<Func<Product, bool>> where = null, Func<IQueryable<Product>, IIncludableQueryable<Product, object>> include = null)
        {
            return await _productRepository.ExistsAsync(where, include);
        }

        public async Task<IEnumerable<Product>> GetAllAsyn(Expression<Func<Product, bool>> where = null, Func<IQueryable<Product>, IOrderedQueryable<Product>> orderBy = null, Func<IQueryable<Product>, IIncludableQueryable<Product, object>> include = null, bool tracking = false)
        {
            return await _productRepository.GetAllAsyn(where, orderBy, include);
        }

        public async Task<Product> GetAsync(Expression<Func<Product, bool>> where = null, Func<IQueryable<Product>, IOrderedQueryable<Product>> orderBy = null, Func<IQueryable<Product>, IIncludableQueryable<Product, object>> include = null, bool tracking = false)
        {
            return await _productRepository.GetAsync(where, orderBy, include);
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<Product, TResult>> selector = null, Expression<Func<Product, bool>> where = null, Func<IQueryable<Product>, IOrderedQueryable<Product>> orderBy = null, Func<IQueryable<Product>, IIncludableQueryable<Product, object>> include = null, bool disableTracking = true)
        {
            return await _productRepository.GetWithIncludeAsync(selector, where, orderBy, include);
        }

        public async Task Insert(Product product)
        {
            await _productRepository.Insert(product);
        }

        public async Task Save()
        {
            await _productRepository.Save();
        }

        public void Update(Product product)
        {
            _productRepository.Update(product);
        }
    }
}
