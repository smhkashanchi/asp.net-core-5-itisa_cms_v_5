﻿using Application.Interfaces;

using Domain.DomainClasses.Color;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class ColorTranslationServices : IColorTranslationServices
    {
        private IColorTranslationRepository _colorTranslationRepository;
        public ColorTranslationServices(IColorTranslationRepository colorTranslationRepository)
        {
            _colorTranslationRepository = colorTranslationRepository;
        }

        public void DeleteAll(IEnumerable<ColorTranslation> colorsTranslation)
        {
            _colorTranslationRepository.DeleteAll(colorsTranslation);
        }

        public void DeleteAsync(ColorTranslation colorTranslation)
        {
            _colorTranslationRepository.DeleteAsync(colorTranslation);
        }

        public async Task<bool> ExistsAsync(Expression<Func<ColorTranslation, bool>> where = null, Func<IQueryable<ColorTranslation>
            , IIncludableQueryable<ColorTranslation, object>> include = null)
        {
            return await _colorTranslationRepository.ExistsAsync(where,include);
        }

        public async Task<IEnumerable<ColorTranslation>> GetAllAsyn(Expression<Func<ColorTranslation, bool>> where = null, Func<IQueryable<ColorTranslation>, IOrderedQueryable<ColorTranslation>> orderBy = null, Func<IQueryable<ColorTranslation>, IIncludableQueryable<ColorTranslation, object>> include = null, bool tracking = false)
        {
            return await _colorTranslationRepository.GetAllAsyn(where, orderBy, include);
        }

        public async Task<ColorTranslation> GetAsync(Expression<Func<ColorTranslation, bool>> where = null, Func<IQueryable<ColorTranslation>, IOrderedQueryable<ColorTranslation>> orderBy = null, Func<IQueryable<ColorTranslation>, IIncludableQueryable<ColorTranslation, object>> include = null, bool tracking = false)
        {
            return await _colorTranslationRepository.GetAsync(where, orderBy, include);
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<ColorTranslation, TResult>> selector = null, Expression<Func<ColorTranslation, bool>> where = null, Func<IQueryable<ColorTranslation>, IOrderedQueryable<ColorTranslation>> orderBy = null, Func<IQueryable<ColorTranslation>, IIncludableQueryable<ColorTranslation, object>> include = null, bool disableTracking = true)
        {
            return await _colorTranslationRepository.GetWithIncludeAsync(selector, where, orderBy, include);
        }

        public async Task Insert(ColorTranslation colorTranslation)
        {
           await _colorTranslationRepository.Insert(colorTranslation);
        }

        public async Task Save()
        {
           await _colorTranslationRepository.Save();
        }

        public void Update(ColorTranslation colorTranslation)
        {
            _colorTranslationRepository.Update(colorTranslation);
        }
    }
}
