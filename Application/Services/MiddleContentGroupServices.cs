﻿using Application.Interfaces;

using Domain.DomainClasses.Content;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class MiddleContentGroupServices : IMiddleContentGroupServices
    {
        private IMiddleContentGroupRepository _middleContentGroupRepository;
        public MiddleContentGroupServices(IMiddleContentGroupRepository middleContentGroupRepository)
        {
            _middleContentGroupRepository = middleContentGroupRepository;
        }
        public void Delete(ContentMiddleGroup contentMiddleGroup)
        {
            _middleContentGroupRepository.Delete(contentMiddleGroup);
        }

        public void DeleteAll(IEnumerable<ContentMiddleGroup> contentMiddleGroups)
        {
            _middleContentGroupRepository.DeleteAll(contentMiddleGroups);
        }

        public async Task<bool> ExistsAsync(Expression<Func<ContentMiddleGroup, bool>> where = null, Func<IQueryable<ContentMiddleGroup>, IIncludableQueryable<ContentMiddleGroup, object>> include = null)
        {
            return await _middleContentGroupRepository.ExistsAsync(where, include);
        }

        public async Task<IEnumerable<ContentMiddleGroup>> GetAllAsyn(Expression<Func<ContentMiddleGroup, bool>> where = null, Func<IQueryable<ContentMiddleGroup>, IOrderedQueryable<ContentMiddleGroup>> orderBy = null, Func<IQueryable<ContentMiddleGroup>, IIncludableQueryable<ContentMiddleGroup, object>> include = null, bool tracking = false)
        {
            return await _middleContentGroupRepository.GetAllAsyn(where, orderBy, include);
        }

        public async Task<ContentMiddleGroup> GetAsync(Expression<Func<ContentMiddleGroup, bool>> where = null, Func<IQueryable<ContentMiddleGroup>, IOrderedQueryable<ContentMiddleGroup>> orderBy = null, Func<IQueryable<ContentMiddleGroup>, IIncludableQueryable<ContentMiddleGroup, object>> include = null, bool tracking = false)
        {
            return await _middleContentGroupRepository.GetAsync(where, orderBy, include);
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<ContentMiddleGroup, TResult>> selector = null, Expression<Func<ContentMiddleGroup, bool>> where = null, Func<IQueryable<ContentMiddleGroup>, IOrderedQueryable<ContentMiddleGroup>> orderBy = null, Func<IQueryable<ContentMiddleGroup>, IIncludableQueryable<ContentMiddleGroup, object>> include = null, bool disableTracking = true)
        {
            return await _middleContentGroupRepository.GetWithIncludeAsync(selector, where, orderBy, include);
        }

        public async Task Insert(ContentMiddleGroup contentMiddleGroup)
        {
            await _middleContentGroupRepository.Insert(contentMiddleGroup);
        }

        public async Task Save()
        {
            await _middleContentGroupRepository.Save();
        }

        public async Task Update(ContentMiddleGroup contentMiddleGroup)
        {
            await _middleContentGroupRepository.Update(contentMiddleGroup);
        }
    }
}
