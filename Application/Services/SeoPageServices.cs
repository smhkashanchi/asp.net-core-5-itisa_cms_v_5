﻿using Application.Interfaces;

using Domain.DomainClasses.Seo;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class SeoPageServices : ISeoPageServices
    {
        private ISeoPageRepository _seoPageRepository;
        public SeoPageServices(ISeoPageRepository seoPageRepository)
        {
            _seoPageRepository = seoPageRepository;
        }
        public void DeleteAsync(SeoPage SeoPage)
        {
            _seoPageRepository.DeleteAsync(SeoPage);
        }

        public async Task<bool> ExistsAsync(Expression<Func<SeoPage, bool>> where = null, Func<IQueryable<SeoPage>, IIncludableQueryable<SeoPage, object>> include = null)
        {
            return await _seoPageRepository.ExistsAsync(where, include);
        }

        public async Task<IEnumerable<SeoPage>> GetAllAsyn(Expression<Func<SeoPage, bool>> where = null, Func<IQueryable<SeoPage>, IOrderedQueryable<SeoPage>> orderBy = null, Func<IQueryable<SeoPage>, IIncludableQueryable<SeoPage, object>> include = null, bool tracking = false)
        {
            return await _seoPageRepository.GetAllAsyn(where, orderBy, include);
        }

        public async Task<SeoPage> GetAsync(Expression<Func<SeoPage, bool>> where = null, Func<IQueryable<SeoPage>, IOrderedQueryable<SeoPage>> orderBy = null, Func<IQueryable<SeoPage>, IIncludableQueryable<SeoPage, object>> include = null, bool tracking = false)
        {
            return await _seoPageRepository.GetAsync(where, orderBy, include);
        }

        public async Task Insert(SeoPage SeoPage)
        {
            await _seoPageRepository.Insert(SeoPage);
        }

        public async Task Save()
        {
            await _seoPageRepository.Save();
        }

        public async Task Update(SeoPage SeoPage)
        {
            await _seoPageRepository.Update(SeoPage);
        }
    }
}
