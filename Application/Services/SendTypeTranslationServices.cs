﻿using Application.Interfaces;

using Domain.DomainClasses.SendType;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class SendTypeTranslationServices : ISendTypeTranslationServices
    {
        private ISendTypeTranslationRepository _sendTypeTranslationRepository;
        public SendTypeTranslationServices(ISendTypeTranslationRepository sendTypeTranslationRepository)
        {
            _sendTypeTranslationRepository = sendTypeTranslationRepository;
        }
        public void Delete(SendTypeTranslation sendTypeTranslation)
        {
            _sendTypeTranslationRepository.Delete(sendTypeTranslation);
        }

        public void DeleteAll(IEnumerable<SendTypeTranslation> sendTypeTranslations)
        {
            _sendTypeTranslationRepository.DeleteAll(sendTypeTranslations);
        }

        public async Task<bool> ExistsAsync(Expression<Func<SendTypeTranslation, bool>> where = null, Func<IQueryable<SendTypeTranslation>, IIncludableQueryable<SendTypeTranslation, object>> include = null)
        {
           return await _sendTypeTranslationRepository.ExistsAsync(where, include);
        }

        public async Task<IEnumerable<SendTypeTranslation>> GetAllAsyn(Expression<Func<SendTypeTranslation, bool>> where = null, Func<IQueryable<SendTypeTranslation>, IOrderedQueryable<SendTypeTranslation>> orderBy = null, Func<IQueryable<SendTypeTranslation>, IIncludableQueryable<SendTypeTranslation, object>> include = null, bool tracking = false)
        {
            return await _sendTypeTranslationRepository.GetAllAsyn(where, orderBy, include);
        }

        public async Task<SendTypeTranslation> GetAsync(Expression<Func<SendTypeTranslation, bool>> where = null, Func<IQueryable<SendTypeTranslation>, IOrderedQueryable<SendTypeTranslation>> orderBy = null, Func<IQueryable<SendTypeTranslation>, IIncludableQueryable<SendTypeTranslation, object>> include = null, bool tracking = false)
        {
            return await _sendTypeTranslationRepository.GetAsync(where, orderBy, include);
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<SendTypeTranslation, TResult>> selector = null, Expression<Func<SendTypeTranslation, bool>> where = null, Func<IQueryable<SendTypeTranslation>, IOrderedQueryable<SendTypeTranslation>> orderBy = null, Func<IQueryable<SendTypeTranslation>, IIncludableQueryable<SendTypeTranslation, object>> include = null, bool disableTracking = true)
        {
            return await _sendTypeTranslationRepository.GetWithIncludeAsync(selector, where, orderBy, include);
        }

        public async Task Insert(SendTypeTranslation sendTypeTranslation)
        {
            await _sendTypeTranslationRepository.Insert(sendTypeTranslation);
        }

        public async Task Save()
        {
            await _sendTypeTranslationRepository.Save();
        }

        public async Task Update(SendTypeTranslation sendTypeTranslation)
        {
            await _sendTypeTranslationRepository.Update(sendTypeTranslation);
        }
    }
}
