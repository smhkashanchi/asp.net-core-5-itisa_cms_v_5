﻿using Application.Interfaces;

using Domain.DomainClasses.Content;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class ContentServices : IContentServices
    {
        private IContentRepository _contentRepository;
        public ContentServices(IContentRepository contentRepository)
        {
            _contentRepository = contentRepository;
        }
        public void DeleteAsync(Content content)
        {
            _contentRepository.DeleteAsync(content);
        }

        public async Task<bool> ExistsAsync(Expression<Func<Content, bool>> where = null, Func<IQueryable<Content>, IIncludableQueryable<Content, object>> include = null)
        {
            return await _contentRepository.ExistsAsync(where, include);
        }

        public async Task<IEnumerable<Content>> GetAllAsyn(Expression<Func<Content, bool>> where = null, Func<IQueryable<Content>, IOrderedQueryable<Content>> orderBy = null, Func<IQueryable<Content>, IIncludableQueryable<Content, object>> include = null, bool tracking = false)
        {
            return await _contentRepository.GetAllAsyn(where,orderBy, include);
        }

        public async Task<Content> GetAsync(Expression<Func<Content, bool>> where = null, Func<IQueryable<Content>, IOrderedQueryable<Content>> orderBy = null, Func<IQueryable<Content>, IIncludableQueryable<Content, object>> include = null, bool tracking = false)
        {
            return await _contentRepository.GetAsync(where, orderBy, include);
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<Content, TResult>> selector = null, Expression<Func<Content, bool>> where = null, Func<IQueryable<Content>, IOrderedQueryable<Content>> orderBy = null, Func<IQueryable<Content>, IIncludableQueryable<Content, object>> include = null, bool disableTracking = true)
        {
            return await _contentRepository.GetWithIncludeAsync(selector,where, orderBy, include);
        }

        public async Task Insert(Content content)
        {
            await _contentRepository.Insert(content);
        }

        public async Task Save()
        {
            await _contentRepository.Save();
        }

        public async Task Update(Content content)
        {
            await _contentRepository.Update(content);
        }
    }
}
