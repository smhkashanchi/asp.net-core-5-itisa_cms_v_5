﻿using Application.Interfaces;

using Domain.DomainClasses.CoWorker;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class CoWorkerGroupServices : ICoWorkerGroupServices
    {
        private ICoWorkerGroupRepository _coWorkerGroupRepository;
        public CoWorkerGroupServices(ICoWorkerGroupRepository coWorkerGroupRepository)
        {
            _coWorkerGroupRepository = coWorkerGroupRepository;
        }
        public void DeleteAsync(CoWorkerGroup coWorkerGroup)
        {
            _coWorkerGroupRepository.DeleteAsync(coWorkerGroup);
        }

        public async Task<bool> ExistsAsync(Expression<Func<CoWorkerGroup, bool>> where = null, Func<IQueryable<CoWorkerGroup>, IIncludableQueryable<CoWorkerGroup, object>> include = null)
        {
            return await _coWorkerGroupRepository.ExistsAsync(where, include);
        }

        public async Task<IEnumerable<CoWorkerGroup>> GetAllAsyn(Expression<Func<CoWorkerGroup, bool>> where = null, Func<IQueryable<CoWorkerGroup>, IOrderedQueryable<CoWorkerGroup>> orderBy = null, Func<IQueryable<CoWorkerGroup>, IIncludableQueryable<CoWorkerGroup, object>> include = null, bool tracking = false)
        {
            return await _coWorkerGroupRepository.GetAllAsyn(where, orderBy, include);
        }

        public async Task<CoWorkerGroup> GetAsync(Expression<Func<CoWorkerGroup, bool>> where = null, Func<IQueryable<CoWorkerGroup>, IOrderedQueryable<CoWorkerGroup>> orderBy = null, Func<IQueryable<CoWorkerGroup>, IIncludableQueryable<CoWorkerGroup, object>> include = null, bool tracking = false)
        {
            return await _coWorkerGroupRepository.GetAsync(where, orderBy, include);
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<CoWorkerGroup, TResult>> selector = null, Expression<Func<CoWorkerGroup, bool>> where = null, Func<IQueryable<CoWorkerGroup>, IOrderedQueryable<CoWorkerGroup>> orderBy = null, Func<IQueryable<CoWorkerGroup>, IIncludableQueryable<CoWorkerGroup, object>> include = null, bool disableTracking = true)
        {
            return await _coWorkerGroupRepository.GetWithIncludeAsync(selector,where, orderBy, include);
        }

        public async Task Insert(CoWorkerGroup coWorkerGroup)
        {
            await _coWorkerGroupRepository.Insert(coWorkerGroup);
        }

        public async Task Save()
        {
            await _coWorkerGroupRepository.Save();
        }

        public async Task Update(CoWorkerGroup coWorkerGroup)
        {
            await _coWorkerGroupRepository.Update(coWorkerGroup);
        }
    }
}
