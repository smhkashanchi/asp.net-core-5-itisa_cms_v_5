﻿using Application.Interfaces;

using Domain.DomainClasses.Connection;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class ConnectionGroupServices : IConnectionGroupServices
    {
        private IConnectionGroupRepository _connectionGroupRepository;
        public ConnectionGroupServices(IConnectionGroupRepository connectionGroupRepository)
        {
            _connectionGroupRepository = connectionGroupRepository;
        }
        public void DeleteAsync(ConnectionGroup ConnectionGroup)
        {
            _connectionGroupRepository.DeleteAsync(ConnectionGroup);
        }

        public async Task<bool> ExistsAsync(Expression<Func<ConnectionGroup, bool>> where = null, Func<IQueryable<ConnectionGroup>, IIncludableQueryable<ConnectionGroup, object>> include = null)
        {
            return await _connectionGroupRepository.ExistsAsync(where, include);
        }

        public async Task<IEnumerable<ConnectionGroup>> GetAllAsyn(Expression<Func<ConnectionGroup, bool>> where = null, Func<IQueryable<ConnectionGroup>, IOrderedQueryable<ConnectionGroup>> orderBy = null, Func<IQueryable<ConnectionGroup>, IIncludableQueryable<ConnectionGroup, object>> include = null, bool tracking = false)
        {
            return await _connectionGroupRepository.GetAllAsyn(where, orderBy, include);
        }

        public async Task<ConnectionGroup> GetAsync(Expression<Func<ConnectionGroup, bool>> where = null, Func<IQueryable<ConnectionGroup>, IOrderedQueryable<ConnectionGroup>> orderBy = null, Func<IQueryable<ConnectionGroup>, IIncludableQueryable<ConnectionGroup, object>> include = null, bool tracking = false)
        {
            return await _connectionGroupRepository.GetAsync(where, orderBy, include);
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<ConnectionGroup, TResult>> selector = null, Expression<Func<ConnectionGroup, bool>> where = null, Func<IQueryable<ConnectionGroup>, IOrderedQueryable<ConnectionGroup>> orderBy = null, Func<IQueryable<ConnectionGroup>, IIncludableQueryable<ConnectionGroup, object>> include = null, bool disableTracking = true)
        {
            return await _connectionGroupRepository.GetWithIncludeAsync(selector,where, orderBy, include);
        }

        public async Task Insert(ConnectionGroup ConnectionGroup)
        {
            await _connectionGroupRepository.Insert(ConnectionGroup);
        }

        public async Task Save()
        {
            await _connectionGroupRepository.Save();
        }

        public void Update(ConnectionGroup ConnectionGroup)
        {
            _connectionGroupRepository.Update(ConnectionGroup);
        }
    }
}
