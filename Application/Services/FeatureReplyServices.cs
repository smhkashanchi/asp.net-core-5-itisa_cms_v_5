﻿using Application.Interfaces;

using Domain.DomainClasses.Feature;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class FeatureReplyServices : IFeatureReplyServices
    {
        private IFeatureReplyRepository _featureReplyRepository;
        public FeatureReplyServices(IFeatureReplyRepository featureReplyRepository)
        {
            _featureReplyRepository = featureReplyRepository;
        }
        public void Delete(FeatureReply featureReply)
        {
            _featureReplyRepository.Delete(featureReply);
        }

        public async Task<bool> ExistsAsync(Expression<Func<FeatureReply, bool>> where = null, Func<IQueryable<FeatureReply>, IIncludableQueryable<FeatureReply, object>> include = null)
        {
            return await _featureReplyRepository.ExistsAsync(where, include);
        }

        public async Task<IEnumerable<FeatureReply>> GetAllAsyn(Expression<Func<FeatureReply, bool>> where = null, Func<IQueryable<FeatureReply>, IOrderedQueryable<FeatureReply>> orderBy = null, Func<IQueryable<FeatureReply>, IIncludableQueryable<FeatureReply, object>> include = null, bool tracking = false)
        {
            return await _featureReplyRepository.GetAllAsyn(where, orderBy, include);
        }

        public async Task<FeatureReply> GetAsync(Expression<Func<FeatureReply, bool>> where = null, Func<IQueryable<FeatureReply>, IOrderedQueryable<FeatureReply>> orderBy = null, Func<IQueryable<FeatureReply>, IIncludableQueryable<FeatureReply, object>> include = null, bool tracking = false)
        {
            return await _featureReplyRepository.GetAsync(where, orderBy, include);
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<FeatureReply, TResult>> selector = null, Expression<Func<FeatureReply, bool>> where = null, Func<IQueryable<FeatureReply>, IOrderedQueryable<FeatureReply>> orderBy = null, Func<IQueryable<FeatureReply>, IIncludableQueryable<FeatureReply, object>> include = null, bool disableTracking = true)
        {
            return await _featureReplyRepository.GetWithIncludeAsync(selector, where, orderBy, include);
        }

        public async Task Insert(FeatureReply featureReply)
        {
            await _featureReplyRepository.Insert(featureReply);
        }

        public async Task Save()
        {
            await _featureReplyRepository.Save();
        }

        public void Update(FeatureReply featureReply)
        {
            _featureReplyRepository.Update(featureReply);
        }
    }
}
