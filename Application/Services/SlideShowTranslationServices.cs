﻿using Application.Interfaces;

using Domain.DomainClasses.SlideShow;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class SlideShowTranslationServices : ISlideShowTranslationServices
    {
        private ISlideShowTranslationRepository _slideShowTranslationRepository;
        public SlideShowTranslationServices(ISlideShowTranslationRepository slideShowTranslationRepository)
        {
            _slideShowTranslationRepository = slideShowTranslationRepository;
        }
        public void Delete(SlideShowTranslation slideShowTranslation)
        {
            _slideShowTranslationRepository.Delete(slideShowTranslation);
        }

        public void DeleteAll(IEnumerable<SlideShowTranslation> slideShowTranslation)
        {
            _slideShowTranslationRepository.DeleteAll(slideShowTranslation);
        }

        public async Task<bool> ExistsAsync(Expression<Func<SlideShowTranslation, bool>> where = null, Func<IQueryable<SlideShowTranslation>, IIncludableQueryable<SlideShowTranslation, object>> include = null)
        {
            return await _slideShowTranslationRepository.ExistsAsync(where, include);
        }

        public Task<Dictionary<string, int>> GetAllToDictionary(int Id)
        {
            throw new NotImplementedException();
        }

        public async Task<SlideShowTranslation> GetAsync(Expression<Func<SlideShowTranslation, bool>> where = null, Func<IQueryable<SlideShowTranslation>, IOrderedQueryable<SlideShowTranslation>> orderBy = null, Func<IQueryable<SlideShowTranslation>, IIncludableQueryable<SlideShowTranslation, object>> include = null, bool tracking = false)
        {
            return await _slideShowTranslationRepository.GetAsync(where, orderBy, include);
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<SlideShowTranslation, TResult>> selector = null, Expression<Func<SlideShowTranslation, bool>> where = null, Func<IQueryable<SlideShowTranslation>, IOrderedQueryable<SlideShowTranslation>> orderBy = null, Func<IQueryable<SlideShowTranslation>, IIncludableQueryable<SlideShowTranslation, object>> include = null, bool disableTracking = true)
        {
            return await _slideShowTranslationRepository.GetWithIncludeAsync(selector, where, orderBy, include);
        }

        public async Task Insert(SlideShowTranslation slideShowTranslation)
        {
            await _slideShowTranslationRepository.Insert(slideShowTranslation);
        }

        public async Task Save()
        {
            await _slideShowTranslationRepository.Save();
        }

        public async Task Update(SlideShowTranslation slideShowTranslation)
        {
            await _slideShowTranslationRepository.Update(slideShowTranslation);
        }
    }
}
