﻿using Application.Interfaces;

using Domain.DomainClasses.Gallery;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class GalleryGroupServices:IGalleryGroupServices
    {
        private IGalleryGroupRepository _galleryGroupRepository;
        public GalleryGroupServices(IGalleryGroupRepository galleryGroupRepository)
        {
            _galleryGroupRepository = galleryGroupRepository;
        }

        public void DeleteAsync(GalleryGroup galleryGroup)
        {
            _galleryGroupRepository.DeleteAsync(galleryGroup);
        }

        public async Task<bool> ExistsAsync(Expression<Func<GalleryGroup, bool>> where = null, Func<IQueryable<GalleryGroup>, IIncludableQueryable<GalleryGroup, object>> include = null)
        {
            return await _galleryGroupRepository.ExistsAsync(where, include);
        }

        public async Task<IEnumerable<GalleryGroup>> GetAllAsyn(Expression<Func<GalleryGroup, bool>> where = null, Func<IQueryable<GalleryGroup>, IOrderedQueryable<GalleryGroup>> orderBy = null, Func<IQueryable<GalleryGroup>, IIncludableQueryable<GalleryGroup, object>> include = null, bool tracking = false)
        {
            return await _galleryGroupRepository.GetAllAsyn(where, orderBy, include);
        }

        public async Task<GalleryGroup> GetAsync(Expression<Func<GalleryGroup, bool>> where = null, Func<IQueryable<GalleryGroup>, IOrderedQueryable<GalleryGroup>> orderBy = null, Func<IQueryable<GalleryGroup>, IIncludableQueryable<GalleryGroup, object>> include = null, bool tracking = false)
        {
            return await _galleryGroupRepository.GetAsync(where, orderBy, include);
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<GalleryGroup, TResult>> selector = null, Expression<Func<GalleryGroup, bool>> where = null, Func<IQueryable<GalleryGroup>, IOrderedQueryable<GalleryGroup>> orderBy = null, Func<IQueryable<GalleryGroup>, IIncludableQueryable<GalleryGroup, object>> include = null, bool disableTracking = true)
        {
            return await _galleryGroupRepository.GetWithIncludeAsync(selector, where, orderBy, include);
        }

        public async Task Insert(GalleryGroup galleryGroup)
        {
            await _galleryGroupRepository.Insert(galleryGroup);
        }

        public async Task Save()
        {
            await _galleryGroupRepository.Save();
        }

        public async Task Update(GalleryGroup galleryGroup)
        {
            _galleryGroupRepository.Update(galleryGroup);
        }
    }
}
