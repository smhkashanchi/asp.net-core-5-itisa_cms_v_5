﻿using Application.Interfaces;

using Domain.DomainClasses.Gallery;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class GalleryPictureServices : IGalleryPictureServices
    {
        private IGalleryPictureRepository _galleryPictureRepository;
        public GalleryPictureServices(IGalleryPictureRepository galleryPictureRepository)
        {
            _galleryPictureRepository = galleryPictureRepository;
        }
        public void DeleteAsync(GalleryPicture galleryPicture)
        {
            _galleryPictureRepository.DeleteAsync(galleryPicture);
        }

        public async Task<bool> ExistsAsync(Expression<Func<GalleryPicture, bool>> where = null, Func<IQueryable<GalleryPicture>, IIncludableQueryable<GalleryPicture, object>> include = null)
        {
            return await _galleryPictureRepository.ExistsAsync(where, include);
        }

        public async Task<IEnumerable<GalleryPicture>> GetAllAsyn(Expression<Func<GalleryPicture, bool>> where = null, Func<IQueryable<GalleryPicture>, IOrderedQueryable<GalleryPicture>> orderBy = null, Func<IQueryable<GalleryPicture>, IIncludableQueryable<GalleryPicture, object>> include = null, bool tracking = false)
        {
            return await _galleryPictureRepository.GetAllAsyn(where, orderBy, include);
        }

        public async Task<GalleryPicture> GetAsync(Expression<Func<GalleryPicture, bool>> where = null, Func<IQueryable<GalleryPicture>, IOrderedQueryable<GalleryPicture>> orderBy = null, Func<IQueryable<GalleryPicture>, IIncludableQueryable<GalleryPicture, object>> include = null, bool tracking = false)
        {
            return await _galleryPictureRepository.GetAsync(where, orderBy, include);
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<GalleryPicture, TResult>> selector = null, Expression<Func<GalleryPicture, bool>> where = null, Func<IQueryable<GalleryPicture>, IOrderedQueryable<GalleryPicture>> orderBy = null, Func<IQueryable<GalleryPicture>, IIncludableQueryable<GalleryPicture, object>> include = null, bool disableTracking = true)
        {
            return await _galleryPictureRepository.GetWithIncludeAsync(selector, where, orderBy, include);
        }

        public async Task Insert(GalleryPicture galleryPicture)
        {
            await _galleryPictureRepository.Insert(galleryPicture);
        }

        public async Task Save()
        {
            await _galleryPictureRepository.Save();
        }

        public void Update(GalleryPicture galleryPicture)
        {
            _galleryPictureRepository.Update(galleryPicture);
        }
    }
}
