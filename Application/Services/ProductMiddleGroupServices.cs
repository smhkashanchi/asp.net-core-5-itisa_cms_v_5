﻿using Application.Interfaces;

using Domain.DomainClasses.Product;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class ProductMiddleGroupServices : IProductMiddleGroupServices
    {
        private IProductMiddleGroupRepository _productMiddleGroupRepository;
        public ProductMiddleGroupServices(IProductMiddleGroupRepository productMiddleGroupRepository)
        {
            _productMiddleGroupRepository = productMiddleGroupRepository;
        }
        public void Delete(ProductMiddleGroup productMiddleGroup)
        {
          _productMiddleGroupRepository.Delete(productMiddleGroup);
        }

        public void DeleteAll(IEnumerable<ProductMiddleGroup> productMiddleGroups)
        {
            _productMiddleGroupRepository.DeleteAll(productMiddleGroups);
        }

        public async Task<bool> ExistsAsync(Expression<Func<ProductMiddleGroup, bool>> where = null, Func<IQueryable<ProductMiddleGroup>, IIncludableQueryable<ProductMiddleGroup, object>> include = null)
        {
            return await _productMiddleGroupRepository.ExistsAsync(where, include);
        }

        public async Task<IEnumerable<ProductMiddleGroup>> GetAllAsyn(Expression<Func<ProductMiddleGroup, bool>> where = null, Func<IQueryable<ProductMiddleGroup>, IOrderedQueryable<ProductMiddleGroup>> orderBy = null, Func<IQueryable<ProductMiddleGroup>, IIncludableQueryable<ProductMiddleGroup, object>> include = null, bool tracking = false)
        {
            return await _productMiddleGroupRepository.GetAllAsyn(where, orderBy, include);
        }

        public async Task<ProductMiddleGroup> GetAsync(Expression<Func<ProductMiddleGroup, bool>> where = null, Func<IQueryable<ProductMiddleGroup>, IOrderedQueryable<ProductMiddleGroup>> orderBy = null, Func<IQueryable<ProductMiddleGroup>, IIncludableQueryable<ProductMiddleGroup, object>> include = null, bool tracking = false)
        {
            return await _productMiddleGroupRepository.GetAsync(where, orderBy, include);
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<ProductMiddleGroup, TResult>> selector = null, Expression<Func<ProductMiddleGroup, bool>> where = null, Func<IQueryable<ProductMiddleGroup>, IOrderedQueryable<ProductMiddleGroup>> orderBy = null, Func<IQueryable<ProductMiddleGroup>, IIncludableQueryable<ProductMiddleGroup, object>> include = null, bool disableTracking = true)
        {
            return await _productMiddleGroupRepository.GetWithIncludeAsync(selector, where, orderBy, include);
        }

        public async Task Insert(ProductMiddleGroup productMiddleGroup)
        {
            await _productMiddleGroupRepository.Insert(productMiddleGroup);
        }

        public async Task InsertRange(IEnumerable<ProductMiddleGroup> productMiddleGroups)
        {
            await _productMiddleGroupRepository.InsertRange(productMiddleGroups);
        }

        public async Task Save()
        {
            await _productMiddleGroupRepository.Save();
        }

        public void Update(ProductMiddleGroup productMiddleGroup)
        {
            _productMiddleGroupRepository.Update(productMiddleGroup);
        }
    }
}
