﻿using Application.Interfaces;

using Domain.DomainClasses.SlideShow;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class SlideShowGroupTranslationServices : ISlideShowGroupTranslationServices
    {
        private ISlideShowGroupTranslationRepository _slideShowGroupTranslationRepository;
        public SlideShowGroupTranslationServices(ISlideShowGroupTranslationRepository slideShowGroupTranslationRepository)
        {
            _slideShowGroupTranslationRepository = slideShowGroupTranslationRepository;
        }
        public void DeleteAsync(SlideShowGroupTranslation slideShowGroupTranslation)
        {
            _slideShowGroupTranslationRepository.DeleteAsync(slideShowGroupTranslation);
        }

        public async Task<bool> ExistsAsync(Expression<Func<SlideShowGroupTranslation, bool>> where = null, Func<IQueryable<SlideShowGroupTranslation>, IIncludableQueryable<SlideShowGroupTranslation, object>> include = null)
        {
            return await _slideShowGroupTranslationRepository.ExistsAsync(where, include);
        }

        public async Task<IEnumerable<SlideShowGroupTranslation>> GetAllAsyn(Expression<Func<SlideShowGroupTranslation, bool>> where = null, Func<IQueryable<SlideShowGroupTranslation>, IOrderedQueryable<SlideShowGroupTranslation>> orderBy = null, Func<IQueryable<SlideShowGroupTranslation>, IIncludableQueryable<SlideShowGroupTranslation, object>> include = null, bool tracking = false)
        {
            return await _slideShowGroupTranslationRepository.GetAllAsyn(where, orderBy, include);
        }

        public async Task<SlideShowGroupTranslation> GetAsync(Expression<Func<SlideShowGroupTranslation, bool>> where = null, Func<IQueryable<SlideShowGroupTranslation>, IOrderedQueryable<SlideShowGroupTranslation>> orderBy = null, Func<IQueryable<SlideShowGroupTranslation>, IIncludableQueryable<SlideShowGroupTranslation, object>> include = null, bool tracking = false)
        {
            return await _slideShowGroupTranslationRepository.GetAsync(where, orderBy, include);
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<SlideShowGroupTranslation, TResult>> selector = null, Expression<Func<SlideShowGroupTranslation, bool>> where = null, Func<IQueryable<SlideShowGroupTranslation>, IOrderedQueryable<SlideShowGroupTranslation>> orderBy = null, Func<IQueryable<SlideShowGroupTranslation>, IIncludableQueryable<SlideShowGroupTranslation, object>> include = null, bool disableTracking = true)
        {
            return await _slideShowGroupTranslationRepository.GetWithIncludeAsync(selector, where, orderBy, include);
        }

        public async Task Insert(SlideShowGroupTranslation slideShowGroupTranslation)
        {
            await _slideShowGroupTranslationRepository.Insert(slideShowGroupTranslation);
        }

        public async Task Save()
        {
            await _slideShowGroupTranslationRepository.Save();
        }

        public async Task Update(SlideShowGroupTranslation slideShowGroupTranslation)
        {
            await _slideShowGroupTranslationRepository.Update(slideShowGroupTranslation);
        }
    }
}
