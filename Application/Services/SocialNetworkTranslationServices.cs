﻿using Application.Interfaces;

using Domain.DomainClasses.SocialNetwork;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class SocialNetworkTranslationServices : ISocialNetworkTranslationServices
    {
        private ISocialNetworkTranslationRepository _socialNetworkTranslationRepository;
        public SocialNetworkTranslationServices(ISocialNetworkTranslationRepository socialNetworkTranslationRepository)
        {
            _socialNetworkTranslationRepository = socialNetworkTranslationRepository;
        }
        public void DeleteAllAsync(IEnumerable<SocialNetworkTranslation> SocialNetworkTranslations)
        {
            _socialNetworkTranslationRepository.DeleteAllAsync(SocialNetworkTranslations);
        }

        public void DeleteAsync(SocialNetworkTranslation SocialNetworkTranslation)
        {
            _socialNetworkTranslationRepository.DeleteAsync(SocialNetworkTranslation);
        }

        public async Task<bool> ExistsAsync(Expression<Func<SocialNetworkTranslation, bool>> where = null, Func<IQueryable<SocialNetworkTranslation>, IIncludableQueryable<SocialNetworkTranslation, object>> include = null)
        {
            return await _socialNetworkTranslationRepository.ExistsAsync(where, include);
        }

        public async Task<IEnumerable<SocialNetworkTranslation>> GetAllAsyn(Expression<Func<SocialNetworkTranslation, bool>> where = null, Func<IQueryable<SocialNetworkTranslation>, IOrderedQueryable<SocialNetworkTranslation>> orderBy = null, Func<IQueryable<SocialNetworkTranslation>, IIncludableQueryable<SocialNetworkTranslation, object>> include = null, bool tracking = false)
        {
            return await _socialNetworkTranslationRepository.GetAllAsyn(where, orderBy, include);
        }

        public async Task<SocialNetworkTranslation> GetAsync(Expression<Func<SocialNetworkTranslation, bool>> where = null, Func<IQueryable<SocialNetworkTranslation>, IOrderedQueryable<SocialNetworkTranslation>> orderBy = null, Func<IQueryable<SocialNetworkTranslation>, IIncludableQueryable<SocialNetworkTranslation, object>> include = null, bool tracking = false)
        {
            return await _socialNetworkTranslationRepository.GetAsync(where, orderBy, include);
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<SocialNetworkTranslation, TResult>> selector = null, Expression<Func<SocialNetworkTranslation, bool>> where = null, Func<IQueryable<SocialNetworkTranslation>, IOrderedQueryable<SocialNetworkTranslation>> orderBy = null, Func<IQueryable<SocialNetworkTranslation>, IIncludableQueryable<SocialNetworkTranslation, object>> include = null, bool disableTracking = true)
        {
            return await _socialNetworkTranslationRepository.GetWithIncludeAsync(selector, where, orderBy, include);
        }

        public async Task Insert(SocialNetworkTranslation SocialNetworkTranslation)
        {
            await _socialNetworkTranslationRepository.Insert(SocialNetworkTranslation);
        }

        public async Task Save()
        {
            await _socialNetworkTranslationRepository.Save();
        }

        public async Task Update(SocialNetworkTranslation SocialNetworkTranslation)
        {
            await _socialNetworkTranslationRepository.Update(SocialNetworkTranslation);
        }
    }
}
