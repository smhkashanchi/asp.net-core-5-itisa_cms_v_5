﻿using Application.Interfaces;

using Domain.DomainClasses.Feature;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class FeatureGroupServices : IFeatureGroupServices
    {
        private IFeatureGroupRepository _featureGroupRepository;
        public FeatureGroupServices(IFeatureGroupRepository featureGroupRepository)
        {
            _featureGroupRepository = featureGroupRepository;
        }
        public void Delete(FeatureGroup featureGroup)
        {
            _featureGroupRepository.Delete(featureGroup);
        }

        public async Task<bool> ExistsAsync(Expression<Func<FeatureGroup, bool>> where = null, Func<IQueryable<FeatureGroup>, IIncludableQueryable<FeatureGroup, object>> include = null)
        {
            return await _featureGroupRepository.ExistsAsync(where, include);
        }

        public async Task<IEnumerable<FeatureGroup>> GetAllAsyn(Expression<Func<FeatureGroup, bool>> where = null, Func<IQueryable<FeatureGroup>, IOrderedQueryable<FeatureGroup>> orderBy = null, Func<IQueryable<FeatureGroup>, IIncludableQueryable<FeatureGroup, object>> include = null, bool tracking = false)
        {
            return await _featureGroupRepository.GetAllAsyn(where, orderBy, include);
        }

        public async Task<FeatureGroup> GetAsync(Expression<Func<FeatureGroup, bool>> where = null, Func<IQueryable<FeatureGroup>, IOrderedQueryable<FeatureGroup>> orderBy = null, Func<IQueryable<FeatureGroup>, IIncludableQueryable<FeatureGroup, object>> include = null, bool tracking = false)
        {
            return await _featureGroupRepository.GetAsync(where, orderBy, include);
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<FeatureGroup, TResult>> selector = null, Expression<Func<FeatureGroup, bool>> where = null, Func<IQueryable<FeatureGroup>, IOrderedQueryable<FeatureGroup>> orderBy = null, Func<IQueryable<FeatureGroup>, IIncludableQueryable<FeatureGroup, object>> include = null, bool disableTracking = true)
        {
            return await _featureGroupRepository.GetWithIncludeAsync(selector, where, orderBy, include);
        }

        public async Task Insert(FeatureGroup featureGroup)
        {
            await _featureGroupRepository.Insert(featureGroup);
        }

        public async Task Save()
        {
            await _featureGroupRepository.Save();
        }

        public void Update(FeatureGroup featureGroup)
        {
            _featureGroupRepository.Update(featureGroup);
        }
    }
}
