﻿using Application.Interfaces;

using Domain.DomainClasses.CoWorker;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class CoWorkerGroupTranslationServices : ICoWorkerGroupTranslationServices
    {
        private ICoWorkerGroupTranslationRepository _coWorkerGroupTranslationRepository;
        public CoWorkerGroupTranslationServices(ICoWorkerGroupTranslationRepository coWorkerGroupTranslationRepository)
        {
            _coWorkerGroupTranslationRepository = coWorkerGroupTranslationRepository;
        }
        public void DeleteAsync(CoWorkerGroupTranslation coWorkerGroupTranslation)
        {
            _coWorkerGroupTranslationRepository.DeleteAsync(coWorkerGroupTranslation);
        }

        public async Task<bool> ExistsAsync(Expression<Func<CoWorkerGroupTranslation, bool>> where = null, Func<IQueryable<CoWorkerGroupTranslation>, IIncludableQueryable<CoWorkerGroupTranslation, object>> include = null)
        {
            return await _coWorkerGroupTranslationRepository.ExistsAsync(where, include);
        }

        public async Task<IEnumerable<CoWorkerGroupTranslation>> GetAllAsyn(Expression<Func<CoWorkerGroupTranslation, bool>> where = null, Func<IQueryable<CoWorkerGroupTranslation>, IOrderedQueryable<CoWorkerGroupTranslation>> orderBy = null, Func<IQueryable<CoWorkerGroupTranslation>, IIncludableQueryable<CoWorkerGroupTranslation, object>> include = null, bool tracking = false)
        {
            return await _coWorkerGroupTranslationRepository.GetAllAsyn(where, orderBy, include);
        }

        public async Task<CoWorkerGroupTranslation> GetAsync(Expression<Func<CoWorkerGroupTranslation, bool>> where = null, Func<IQueryable<CoWorkerGroupTranslation>, IOrderedQueryable<CoWorkerGroupTranslation>> orderBy = null, Func<IQueryable<CoWorkerGroupTranslation>, IIncludableQueryable<CoWorkerGroupTranslation, object>> include = null, bool tracking = false)
        {
            return await _coWorkerGroupTranslationRepository.GetAsync(where, orderBy, include);
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<CoWorkerGroupTranslation, TResult>> selector = null, Expression<Func<CoWorkerGroupTranslation, bool>> where = null, Func<IQueryable<CoWorkerGroupTranslation>, IOrderedQueryable<CoWorkerGroupTranslation>> orderBy = null, Func<IQueryable<CoWorkerGroupTranslation>, IIncludableQueryable<CoWorkerGroupTranslation, object>> include = null, bool disableTracking = true)
        {
            return await _coWorkerGroupTranslationRepository.GetWithIncludeAsync(selector, where, orderBy, include);
        }

        public async Task Insert(CoWorkerGroupTranslation coWorkerGroupTranslation)
        {
            await _coWorkerGroupTranslationRepository.Insert(coWorkerGroupTranslation);
        }

        public async Task Save()
        {
            await _coWorkerGroupTranslationRepository.Save();
        }

        public async Task Update(CoWorkerGroupTranslation coWorkerGroupTranslation)
        {
            await _coWorkerGroupTranslationRepository.Update(coWorkerGroupTranslation);
        }
    }
}
