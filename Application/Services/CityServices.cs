﻿using Application.Interfaces;

using Domain.DomainClasses.Region;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class CityServices : ICityServices
    {
        private ICityRepository _cityRepository;
        public CityServices(ICityRepository cityRepository)
        {
            _cityRepository = cityRepository;
        }
        public void Delete(City city)
        {
            _cityRepository.Delete(city);
        }

        public void DeleteAll(IEnumerable<City> cities)
        {
            _cityRepository.DeleteAll(cities);
        }

        public async Task<bool> ExistsAsync(Expression<Func<City, bool>> where = null, Func<IQueryable<City>, IIncludableQueryable<City, object>> include = null)
        {
            return await _cityRepository.ExistsAsync(where, include);
        }

        public async Task<IEnumerable<City>> GetAllAsyn(Expression<Func<City, bool>> where = null, Func<IQueryable<City>, IOrderedQueryable<City>> orderBy = null, Func<IQueryable<City>, IIncludableQueryable<City, object>> include = null, bool tracking = false)
        {
            return await _cityRepository.GetAllAsyn(where, orderBy, include);
        }

        public async Task<City> GetAsync(Expression<Func<City, bool>> where = null, Func<IQueryable<City>, IOrderedQueryable<City>> orderBy = null, Func<IQueryable<City>, IIncludableQueryable<City, object>> include = null, bool tracking = false)
        {
             return await _cityRepository.GetAsync(where, orderBy, include);
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<City, TResult>> selector = null, Expression<Func<City, bool>> where = null, Func<IQueryable<City>, IOrderedQueryable<City>> orderBy = null, Func<IQueryable<City>, IIncludableQueryable<City, object>> include = null, bool disableTracking = true)
        {
            return await _cityRepository.GetWithIncludeAsync(selector, where, orderBy, include);
        }

        public async Task Insert(City city)
        {
            await _cityRepository.Insert(city);
        }

        public async Task Save()
        {
            await _cityRepository.Save();
        }

        public async Task Update(City city)
        {
            await _cityRepository.Update(city);
        }
    }
}
