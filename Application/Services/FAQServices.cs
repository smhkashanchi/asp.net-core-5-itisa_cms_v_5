﻿using Application.Interfaces;

using Domain.DomainClasses.FAQ;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class FAQServices : IFAQServices
    {
        private IFAQRepository _fAQRepository;
        public FAQServices(IFAQRepository fAQRepository)
        {
            _fAQRepository = fAQRepository;
        }
        public void Delete(FAQ fAQ)
        {
            _fAQRepository.Delete(fAQ);
        }

        public void DeleteAll(IEnumerable<FAQ> fAQs)
        {
            _fAQRepository.DeleteAll(fAQs);
        }

        public async Task<bool> ExistsAsync(Expression<Func<FAQ, bool>> where = null, Func<IQueryable<FAQ>, IIncludableQueryable<FAQ, object>> include = null)
        {
            return await _fAQRepository.ExistsAsync(where, include);
        }

        public async Task<IEnumerable<FAQ>> GetAllAsyn(Expression<Func<FAQ, bool>> where = null, Func<IQueryable<FAQ>, IOrderedQueryable<FAQ>> orderBy = null, Func<IQueryable<FAQ>, IIncludableQueryable<FAQ, object>> include = null, bool tracking = false)
        {
            return await _fAQRepository.GetAllAsyn(where, orderBy, include);
        }

        public async Task<FAQ> GetAsync(Expression<Func<FAQ, bool>> where = null, Func<IQueryable<FAQ>, IOrderedQueryable<FAQ>> orderBy = null, Func<IQueryable<FAQ>, IIncludableQueryable<FAQ, object>> include = null, bool tracking = false)
        {
            return await _fAQRepository.GetAsync(where, orderBy, include);
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<FAQ, TResult>> selector = null, Expression<Func<FAQ, bool>> where = null, Func<IQueryable<FAQ>, IOrderedQueryable<FAQ>> orderBy = null, Func<IQueryable<FAQ>, IIncludableQueryable<FAQ, object>> include = null, bool disableTracking = true)
        {
            return await _fAQRepository.GetWithIncludeAsync(selector, where, orderBy, include);
        }

        public async Task Insert(FAQ fAQ)
        {
            await _fAQRepository.Insert(fAQ);
        }

        public async Task Save()
        {
            await _fAQRepository.Save();
        }

        public async Task Update(FAQ fAQ)
        {
            await _fAQRepository.Update(fAQ);
        }
    }
}
