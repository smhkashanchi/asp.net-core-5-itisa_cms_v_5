﻿using Application.Interfaces;

using Domain.DomainClasses.Content;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class ContentTranslationServices : IContentTranslationServices
    {
        private IContentTranslationRepository _contentTranslationRepository;
        public ContentTranslationServices(IContentTranslationRepository contentTranslationRepository)
        {
            _contentTranslationRepository = contentTranslationRepository;
        }
        public void DeleteAllAsync(IEnumerable<ContentTranslation> contentTranslations)
        {
            _contentTranslationRepository.DeleteAllAsync(contentTranslations);
        }

        public void DeleteAsync(ContentTranslation contentTranslation)
        {
            _contentTranslationRepository.DeleteAsync(contentTranslation);
        }

        public Task Detach(ContentTranslation contentTranslation)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> ExistsAsync(Expression<Func<ContentTranslation, bool>> where = null, Func<IQueryable<ContentTranslation>, IIncludableQueryable<ContentTranslation, object>> include = null)
        {
            return await _contentTranslationRepository.ExistsAsync(where, include);
        }

        public async Task<IEnumerable<ContentTranslation>> GetAllAsyn(Expression<Func<ContentTranslation, bool>> where = null, Func<IQueryable<ContentTranslation>, IOrderedQueryable<ContentTranslation>> orderBy = null, Func<IQueryable<ContentTranslation>, IIncludableQueryable<ContentTranslation, object>> include = null, bool tracking = false)
        {
            return await _contentTranslationRepository.GetAllAsyn(where, orderBy, include);
        }

        public async Task<ContentTranslation> GetAsync(Expression<Func<ContentTranslation, bool>> where = null, Func<IQueryable<ContentTranslation>, IOrderedQueryable<ContentTranslation>> orderBy = null, Func<IQueryable<ContentTranslation>, IIncludableQueryable<ContentTranslation, object>> include = null, bool tracking = false)
        {
            return await _contentTranslationRepository.GetAsync(where, orderBy, include);
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<ContentTranslation, TResult>> selector = null, Expression<Func<ContentTranslation, bool>> where = null, Func<IQueryable<ContentTranslation>, IOrderedQueryable<ContentTranslation>> orderBy = null, Func<IQueryable<ContentTranslation>, IIncludableQueryable<ContentTranslation, object>> include = null, bool disableTracking = true)
        {
            return await _contentTranslationRepository.GetWithIncludeAsync(selector, where, orderBy, include);
        }

        public async Task Insert(ContentTranslation contentTranslation)
        {
            await _contentTranslationRepository.Insert(contentTranslation);
        }

        public async Task Save()
        {
            await _contentTranslationRepository.Save();
        }

        public async Task Update(ContentTranslation contentTranslation)
        {
            await _contentTranslationRepository.Update(contentTranslation);
        }
    }
}
