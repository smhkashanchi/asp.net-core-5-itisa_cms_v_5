﻿using Application.Interfaces;

using Domain.DomainClasses.Seo;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class MainSiteMapServices : IMainSiteMapServices
    {
        private IMainSiteMapRepository _mainSiteMapRepository;
        public MainSiteMapServices(IMainSiteMapRepository mainSiteMapRepository)
        {
            _mainSiteMapRepository = mainSiteMapRepository;
        }
        public void DeleteAsync(MainSiteMap mainSiteMap)
        {
            _mainSiteMapRepository.DeleteAsync(mainSiteMap);
        }

        public async Task<bool> ExistsAsync(Expression<Func<MainSiteMap, bool>> where = null, Func<IQueryable<MainSiteMap>, IIncludableQueryable<MainSiteMap, object>> include = null)
        {
            return await _mainSiteMapRepository.ExistsAsync(where, include);
        }

        public async Task<IEnumerable<MainSiteMap>> GetAllAsyn(Expression<Func<MainSiteMap, bool>> where = null, Func<IQueryable<MainSiteMap>, IOrderedQueryable<MainSiteMap>> orderBy = null, Func<IQueryable<MainSiteMap>, IIncludableQueryable<MainSiteMap, object>> include = null, bool tracking = false)
        {
            return await _mainSiteMapRepository.GetAllAsyn(where,orderBy, include);
        }

        public async Task<MainSiteMap> GetAsync(Expression<Func<MainSiteMap, bool>> where = null, Func<IQueryable<MainSiteMap>, IOrderedQueryable<MainSiteMap>> orderBy = null, Func<IQueryable<MainSiteMap>, IIncludableQueryable<MainSiteMap, object>> include = null, bool tracking = false)
        {
            return await _mainSiteMapRepository.GetAsync(where, orderBy, include);
        }

        public Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<MainSiteMap, TResult>> selector = null, Expression<Func<MainSiteMap, bool>> where = null, Func<IQueryable<MainSiteMap>, IOrderedQueryable<MainSiteMap>> orderBy = null, Func<IQueryable<MainSiteMap>, IIncludableQueryable<MainSiteMap, object>> include = null, bool disableTracking = true)
        {
            throw new NotImplementedException();
        }

        public async Task Insert(MainSiteMap mainSiteMap)
        {
            await _mainSiteMapRepository.Insert(mainSiteMap);
        }

        public async Task Save()
        {
            await _mainSiteMapRepository.Save();
        }

        public void Update(MainSiteMap mainSiteMap)
        {
            _mainSiteMapRepository.Update(mainSiteMap);
        }
    }
}
