﻿using Application.Interfaces;

using Domain.DomainClasses.Gallery;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class GalleryServices : IGalleryServices
    {
        private IGalleryRepository _galleryRepository;
        public GalleryServices(IGalleryRepository galleryRepository)
        {
            _galleryRepository = galleryRepository;
        }
        public void Delete(Gallery gallery)
        {
            _galleryRepository.Delete(gallery);
        }

        public async Task<bool> ExistsAsync(Expression<Func<Gallery, bool>> where = null, Func<IQueryable<Gallery>, IIncludableQueryable<Gallery, object>> include = null)
        {
            return await _galleryRepository.ExistsAsync(where, include);
        }

        public async Task<Gallery> GetAsync(Expression<Func<Gallery, bool>> where = null, Func<IQueryable<Gallery>, IOrderedQueryable<Gallery>> orderBy = null, Func<IQueryable<Gallery>, IIncludableQueryable<Gallery, object>> include = null, bool tracking = false)
        {
            return await _galleryRepository.GetAsync(where, orderBy, include);
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<Gallery, TResult>> selector = null, Expression<Func<Gallery, bool>> where = null, Func<IQueryable<Gallery>, IOrderedQueryable<Gallery>> orderBy = null, Func<IQueryable<Gallery>, IIncludableQueryable<Gallery, object>> include = null, bool disableTracking = true)
        {
            return await _galleryRepository.GetWithIncludeAsync(selector, where, orderBy, include);
        }

        public async Task InsertGallery(Gallery gallery)
        {
            await _galleryRepository.InsertGallery(gallery);
        }

        public async Task Save()
        {
            await _galleryRepository.Save();
        }

        public async Task UpdateGallery(Gallery gallery)
        {
            await _galleryRepository.UpdateGallery(gallery);
        }
    }
}
