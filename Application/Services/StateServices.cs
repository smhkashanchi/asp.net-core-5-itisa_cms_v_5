﻿using Application.Interfaces;

using Domain.DomainClasses.Region;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class StateServices : IStateServices
    {
        private IStateRepository _stateRepository;
        public StateServices(IStateRepository stateRepository)
        {
            _stateRepository = stateRepository;
        }
        public void Delete(State state)
        {
            _stateRepository.Delete(state);
        }

        public void DeleteAll(IEnumerable<State> states)
        {
            _stateRepository.DeleteAll(states);
        }

        public async Task<bool> ExistsAsync(Expression<Func<State, bool>> where = null, Func<IQueryable<State>, IIncludableQueryable<State, object>> include = null)
        {
            return await _stateRepository.ExistsAsync(where, include);
        }

        public async Task<IEnumerable<State>> GetAllAsyn(Expression<Func<State, bool>> where = null, Func<IQueryable<State>, IOrderedQueryable<State>> orderBy = null, Func<IQueryable<State>, IIncludableQueryable<State, object>> include = null, bool tracking = false)
        {
            return await _stateRepository.GetAllAsyn(where, orderBy, include);
        }

        public async Task<State> GetAsync(Expression<Func<State, bool>> where = null, Func<IQueryable<State>, IOrderedQueryable<State>> orderBy = null, Func<IQueryable<State>, IIncludableQueryable<State, object>> include = null, bool tracking = false)
        {
            return await _stateRepository.GetAsync(where, orderBy, include);
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<State, TResult>> selector = null, Expression<Func<State, bool>> where = null, Func<IQueryable<State>, IOrderedQueryable<State>> orderBy = null, Func<IQueryable<State>, IIncludableQueryable<State, object>> include = null, bool disableTracking = true)
        {
            return await _stateRepository.GetWithIncludeAsync(selector, where, orderBy, include);
        }

        public async Task Insert(State state)
        {
            await _stateRepository.Insert(state);
        }

        public async Task Save()
        {
            await _stateRepository.Save();
        }

        public async Task Update(State state)
        {
            await _stateRepository.Update(state);
        }
    }
}
