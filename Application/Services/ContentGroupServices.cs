﻿using Application.Interfaces;

using Domain.DomainClasses.Content;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class ContentGroupServices : IContentGroupServices
    {
        private IContentGroupRepository _contentGroupRepository;
        public ContentGroupServices(IContentGroupRepository contentGroupRepository)
        {
            _contentGroupRepository = contentGroupRepository;
        }
        public void DeleteAsync(ContentGroup contentGroup)
        {
            _contentGroupRepository.DeleteAsync(contentGroup);
        }

        public async Task<bool> ExistsAsync(Expression<Func<ContentGroup, bool>> where = null, Func<IQueryable<ContentGroup>, IIncludableQueryable<ContentGroup, object>> include = null)
        {
            return await _contentGroupRepository.ExistsAsync(where, include);
        }

        public async Task<IEnumerable<ContentGroup>> GetAllAsyn(Expression<Func<ContentGroup, bool>> where = null, Func<IQueryable<ContentGroup>, IOrderedQueryable<ContentGroup>> orderBy = null, Func<IQueryable<ContentGroup>, IIncludableQueryable<ContentGroup, object>> include = null, bool tracking = false)
        {
            return await _contentGroupRepository.GetAllAsyn(where, orderBy, include);
        }

        public async Task<ContentGroup> GetAsync(Expression<Func<ContentGroup, bool>> where = null, Func<IQueryable<ContentGroup>, IOrderedQueryable<ContentGroup>> orderBy = null, Func<IQueryable<ContentGroup>, IIncludableQueryable<ContentGroup, object>> include = null, bool tracking = false)
        {
            return await _contentGroupRepository.GetAsync(where, orderBy, include);
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<ContentGroup, TResult>> selector = null, Expression<Func<ContentGroup, bool>> where = null, Func<IQueryable<ContentGroup>, IOrderedQueryable<ContentGroup>> orderBy = null, Func<IQueryable<ContentGroup>, IIncludableQueryable<ContentGroup, object>> include = null, bool disableTracking = true)
        {
            return await _contentGroupRepository.GetWithIncludeAsync(selector,where, orderBy,include);
        }

        public async Task Insert(ContentGroup contentGroup)
        {
            await _contentGroupRepository.Insert(contentGroup);
        }

        public async Task Save()
        {
            await _contentGroupRepository.Save();
        }

        public async Task Update(ContentGroup contentGroup)
        {
            _contentGroupRepository.Update(contentGroup);
        }
    }
}
