﻿using Application.Interfaces;

using Domain.DomainClasses.Content;
using Domain.DomainClasses.FilesManage;
using Domain.DomainClasses.Product;
using Domain.DomainClasses.Seo;

using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

using Utilities;

namespace Application.Services
{

    public interface ISiteMapService
    {
        public Task<int> AddContentGroup(ContentGroupTranslation contentGroupTranslation);
        public Task EditContentGroup(ContentGroupTranslation contentGroupTranslation);
        public Task DeleteContentGroup(ContentGroupTranslation contentGroupTranslation);
        public Task<List<int>> AddContent(ContentTranslation contentTranslation);
        public Task<List<int>> EditContent(ContentTranslation contentTranslation);
        public Task DeleteContent(ContentTranslation contentTranslation);

        public Task<int> AddFileManageGroup(FileManageGroupTranslation fileManageGroupTranslation);
        public Task EditFileManageGroup(FileManageGroupTranslation fileManageGroupTranslation);
        public Task DeleteFileManageGroup(FileManageGroupTranslation fileManageGroupTranslation);

        public Task<int> AddFileManage(FileManageTranslation fileManageTranslation);
        public Task<int> EditFileManage(FileManageTranslation fileManageTranslation);
        public Task DeleteFileManage(FileManageTranslation fileManageTranslation);
        public Task DeleteAllFileManage(IEnumerable<FileManageTranslation> fileManageTranslations);


        public Task<int> AddProductGroup(IEnumerable<ProductGroupTranslation> productGroupTranslations);
        public Task<int> AddProductGroup(ProductGroupTranslation productGroupTranslation, bool isExistSiteMap);
        public Task EditProductGroup(ProductGroupTranslation productGroupTranslations);
        public Task DeleteProductGroup(IEnumerable<ProductGroupTranslation> productGroupTranslations);
        public Task DeleteProductGroup(ProductGroupTranslation productGroupTranslation, int productGroupTransCount);
        public Task<List<int>> AddProduct(IEnumerable<ProductTranslation> productTranslations, List<int> productGroupIds);
        public Task DeleteProduct(ProductTranslation productTranslation, int productTransCount, string status);
        //public Task<int> EditProduct(ProductInfo product);
        //public Task DeleteProduct(ProductInfo product);
        //public Task InitialProductGroup();


        Task CheckSiteMapFile();
        public string Default_Index_Xml { get; set; }
        public string Default_Xml { get; set; }
        public string Path_Index { get; set; }
        public string Schema { get; set; }
        public string Host { get; set; }

    }
    [Obsolete]

    public class SiteMapService : ISiteMapService
    {

        private string _typeContent = "";
        private string _currentLang = "";

        private readonly SiteMap sm = new SiteMap();

        private IContentGroupTranslationSrevices _contentGroupTranslationSrevices;
        private IContentTranslationServices _contentTranslationServices;
        public IMainSiteMapServices _mainSiteMapServices;
        public IMiddleContentGroupServices _middleContentGroupServices;

        public IFileManageGroupTranslationServices _fileManageGroupTranslationServices;
        public IFileManageTranslationServices _fileManageTranslationServices;

        public IProductGroupTranslationServices _productGroupTranslationServices;
        public IProductGroupServices _productGroupServices;
        public IProductMiddleGroupServices _productMiddleGroupServices;
        public IProductTranslationServices _productTranslationServices;
        private string _default_index_xml;
        public string Default_Index_Xml
        {
            get => _default_index_xml;

            set => _default_index_xml = value;
        }

        private string _default_xml;
        public string Default_Xml
        {
            get => _default_xml;
            set => _default_xml = value;
        }

        private string _path_index;
        public string Path_Index { get => _path_index; set => _path_index = value; }

        private string _schema;
        public string Schema { get => _schema; set => _schema = value; }

        private string _host;
        public string Host { get => _host; set => _host = value; }



        public SiteMapService(IContentGroupTranslationSrevices contentGroupTranslationSrevices
            , IMainSiteMapServices mainSiteMapServices, IContentTranslationServices contentTranslationServices
            , IMiddleContentGroupServices middleContentGroupServices, IFileManageGroupTranslationServices fileManageGroupTranslationServices,
             IFileManageTranslationServices fileManageTranslationServices, IProductGroupTranslationServices productGroupTranslationServices
            , IProductTranslationServices productTranslationServices, IProductMiddleGroupServices productMiddleGroupServices
            , IProductGroupServices productGroupServices)
        {
            _contentGroupTranslationSrevices = contentGroupTranslationSrevices;
            _mainSiteMapServices = mainSiteMapServices;
            _contentTranslationServices = contentTranslationServices;
            _middleContentGroupServices = middleContentGroupServices;
            _fileManageGroupTranslationServices = fileManageGroupTranslationServices;
            _fileManageTranslationServices = fileManageTranslationServices;
            _productGroupTranslationServices = productGroupTranslationServices;
            _productTranslationServices = productTranslationServices;
            _productMiddleGroupServices = productMiddleGroupServices;
            _productGroupServices = productGroupServices;

            ChangeCulture();
            _default_index_xml = "<?xml version='1.0' encoding='UTF-8'?>\n<sitemapindex xmlns='http://www.sitemaps.org/schemas/sitemap/0.9'>\n" +
                "<sitemap>\n" +
                $"<loc>https://topmat.ir/sitemap/main-pages.xml</loc>\n" +
                $"<lastmod>{DateTimeEN()}</lastmod>\n" +
                "</sitemap>\n" +
                "<sitemap>\n" +
                "<loc>https://topmat.ir/sitemap/categories.xml</loc>\n" +
                $"<lastmod>{DateTimeEN()}</lastmod>\n" +
                "</sitemap>\n" +
                "</sitemapindex>";
            ReverseCulture();

            _default_xml = "<?xml version='1.0' encoding='UTF-8'?>\n<urlset xmlns='http://www.sitemaps.org/schemas/sitemap/0.9'></urlset>";

            _path_index = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/sitemap.xml");

            _schema = "https";

            _host = "test.ir";


        }


        #region Content
        public async Task<int> AddContentGroup(ContentGroupTranslation contentGroupTranslation)
        {
            //check is sitemap.xml exists
            await CheckSiteMapFile();

            //check type of content | news or content
            await GetTypeContents(contentGroupTranslation, "contentGroup");

            //add this group to categories.xml
            await AddCategory(null, contentGroupTranslation);

            var mainSiteMap = new MainSiteMap
            {
                Title = contentGroupTranslation.Title,
                Location = $"sitemap-{(_typeContent == "content" ? "contents" : "news")}-{MyExtensions.GenerateRandomSlug()}",
                Type = 1
            };

            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/SiteMap/" + mainSiteMap.Location + ".xml");
            string loc_url = Path.Combine($"{_schema}://{_host}/", "sitemap/" + mainSiteMap.Location + ".xml");

            //create new & empty .xml file
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(_default_xml);
            doc.Save(path);

            ChangeCulture();

            mainSiteMap.CreateDate = DateTime.Now;

            //add this group in sitemap.xml
            SiteMap sm = new SiteMap();
            sm.AddSiteMap(loc_url, DateTimeEN(), _path_index);
            sm.ReadTheFile(_path_index);


            await _mainSiteMapServices.Insert(mainSiteMap);
            await _mainSiteMapServices.Save();

            ReverseCulture();

            return mainSiteMap.SiteMap_ID;

        }
        public async Task EditContentGroup(ContentGroupTranslation contentGroupTranslation)
        {
            await CheckSiteMapFile();

            await GetTypeContents(contentGroupTranslation, "contentGroup");

            await EditCategory(null, contentGroupTranslation);

        }

        public async Task DeleteContentGroup(ContentGroupTranslation contentGroupTranslation)
        {
            var mainSiteMap = await _mainSiteMapServices.GetAsync
                (x => x.SiteMap_ID == contentGroupTranslation.SiteMap_Id);

            await CheckSiteMapFile();

            await DeleteCategory(null, contentGroupTranslation);

            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/SiteMap/" + mainSiteMap.Location + ".xml"); ;
            string loc_url = Path.Combine($"{_schema}://{_host}/", "sitemap/" + mainSiteMap.Location + ".xml");

            System.IO.File.Delete(path);

            SiteMap sm = new SiteMap();
            sm.DeleteSiteMap(loc_url, _path_index);

            _contentGroupTranslationSrevices.DeleteAsync(contentGroupTranslation);
            await _contentGroupTranslationSrevices.Save();

            _mainSiteMapServices.DeleteAsync(mainSiteMap);
            await _mainSiteMapServices.Save();

        }

        public async Task<List<int>> AddContent(ContentTranslation contentTranslation)
        {
            List<int> siteMapIds = new List<int>();
            for (int i = 0; i < contentTranslation.selectedGroupIds.Count; i++)
            {

                var contentGroup = await _contentGroupTranslationSrevices
                    .GetWithIncludeAsync(
                    selector: x => x,
                    where: x => x.ContentGroupTranslation_ID == (Int16)contentTranslation.selectedGroupIds[i],
                    include: s => s.Include(x => x.ContentGroup));
                await GetTypeContents(contentGroup.FirstOrDefault(), "content");

                var sitemap = await _mainSiteMapServices.GetAsync(x => x.SiteMap_ID == contentGroup.FirstOrDefault().SiteMap_Id);

                var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/SiteMap/" + sitemap.Location + ".xml");

                ChangeCulture();

                string date = DateTimeEN();

                if (sm.CheckDate(path))
                {
                    contentGroup.FirstOrDefault().SiteMap_Id = await AddSiteMap(sitemap, path, date, contentTranslation.Slug, _path_index, true, null, contentGroup.FirstOrDefault().Language_Id, _typeContent);

                    await _contentGroupTranslationSrevices.Update(contentGroup.FirstOrDefault());
                    await _contentGroupTranslationSrevices.Save();
                }
                else
                {
                    sm.AddUrl($"{_schema}://{_host}/{contentGroup.FirstOrDefault().Language_Id}/{_typeContent}-details/" + contentTranslation.Slug, date, "0.7", path, "weekly");
                    sm.ReadTheFile(path);
                }

                ReverseCulture();

                siteMapIds.Add(contentGroup.FirstOrDefault().SiteMap_Id.Value);
            }
            return siteMapIds;
        }

        public async Task<List<int>> EditContent(ContentTranslation contentTranslation)
        {
            List<int> siteMapIds = new List<int>();
            var contentOld = await _contentTranslationServices.GetAsync(x => x.ContentTranslation_ID == contentTranslation.ContentTranslation_ID, tracking: true);

            for (int i = 0; i < contentTranslation.selectedGroupIds.Count; i++)
            {

                var contentGroup = await _contentGroupTranslationSrevices
                    .GetWithIncludeAsync(
                    selector: x => x,
                    where: x => x.ContentGroupTranslation_ID == (Int16)contentTranslation.selectedGroupIds[i],
                    include: s => s.Include(x => x.ContentGroup));


                await GetTypeContents(contentGroup.FirstOrDefault(), "content");

                ChangeCulture();

                // if content slug modified
                if (contentTranslation.Slug != contentOld.Slug)
                {
                    var contentMiddleGroup = await _middleContentGroupServices.GetAsync(x => x.ContentGroupTranslation_Id == (Int16)contentTranslation.selectedGroupIds[i]
                        && x.ContentTranslation_Id == contentTranslation.ContentTranslation_ID);
                    //TODO : if !=null
                    var sitemap = await _mainSiteMapServices.GetAsync(x => x.SiteMap_ID == contentMiddleGroup.SiteMap_Id);
                    var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/SiteMap/" + sitemap.Location + ".xml");

                    string date = DateTimeEN();

                    //checking sitemap file date
                    if (sm.CheckDate(path))
                    {
                        // if the last content date in file was more 3 month
                        if (contentGroup.FirstOrDefault().SiteMap_Id != null)
                        {
                            if (contentMiddleGroup.SiteMap_Id == contentGroup.FirstOrDefault().SiteMap_Id)
                            {
                                // if content sitemap & content group sitemap is same
                                contentGroup.FirstOrDefault().SiteMap_Id =
                                    await AddSiteMap(sitemap, path, date, contentTranslation.Slug, _path_index, false, contentOld.Slug, contentGroup.FirstOrDefault().Language_Id, _typeContent);

                                contentMiddleGroup.SiteMap_Id = contentGroup.FirstOrDefault().SiteMap_Id.Value;
                                await _contentGroupTranslationSrevices.Update(contentGroup.FirstOrDefault());
                                await _contentGroupTranslationSrevices.Save();
                            }
                            else
                            {
                                var sitemapContentGroup = await _mainSiteMapServices.GetAsync(x => x.SiteMap_ID == contentGroup.FirstOrDefault().SiteMap_Id);
                                string PathContentGroup = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/SiteMap/" + sitemapContentGroup.Location + ".xml");
                                //درصورت یکی نبودن فایل های مطلب و گروه  وارد این بلاک میشود
                                //در خط بعدی چک میشود که 
                                //تاریخ آخرین آدرس در فایل گروه قبل تر از مدت زمان مد نظر هست یا خیر
                                //در این صورت آدرس قبلی حذف و آدرس ویرایش شده در فایل جدید اضافه خواهد شد
                                if (sm.CheckDate(PathContentGroup))
                                {
                                    contentGroup.FirstOrDefault().SiteMap_Id = await
                                        AddSiteMap(sitemapContentGroup, path, date, contentTranslation.Slug, _path_index, false, contentOld.Slug, contentGroup.FirstOrDefault().Language_Id, _typeContent);
                                    contentMiddleGroup.SiteMap_Id = contentGroup.FirstOrDefault().SiteMap_Id.Value;
                                    await _contentGroupTranslationSrevices.Update(contentGroup.FirstOrDefault());
                                    await _contentGroupTranslationSrevices.Save();
                                }
                                else
                                {
                                    // در صورتی که اخرین رکورد در فایل گروه کمتر
                                    //از مدت زمان مدنظر باشد در فایل گروه ، ادرس ویرایش شده درج و از محل قبلی حذف خواهد شد
                                    string[] s = sm.DeleteUrl($"{_schema}://{_host}/{contentGroup.FirstOrDefault().Language_Id}/{_typeContent}-details/" + contentOld.Slug, path);
                                    sm.AddUrl($"{_schema}://{_host}/{contentGroup.FirstOrDefault().Language_Id}/{_typeContent}-details/" + contentTranslation.Slug, date, "0.7", PathContentGroup, "weekly");
                                    sm.ReadTheFile(PathContentGroup);
                                    contentMiddleGroup.SiteMap_Id = contentGroup.FirstOrDefault().SiteMap_Id.Value;
                                }
                            }
                        }
                    }
                    else
                    {
                        //در صورتی که تاریخ آخرین ادرس ثبت شده در فایل مطلب کمتر از مدت زمان مدنظر باشد ادرس ویرایش شده در فایل مطلب حذف و آدرس جدید جایگزین خواهد شد.
                        string[] s = sm.DeleteUrl($"{_schema}://{_host}/{contentGroup.FirstOrDefault().Language_Id}/{_typeContent}-details/" + contentOld.Slug, path);
                        sm.AddUrl($"{_schema}://{_host}/{contentGroup.FirstOrDefault().Language_Id}/{_typeContent}-details/" + contentTranslation.Slug, date, "0.5", path, "weekly");
                        sm.ReadTheFile(path);
                    }

                    siteMapIds.Add(contentMiddleGroup.SiteMap_Id);
                }
                ReverseCulture();
            }
            return siteMapIds;
        }

        public async Task DeleteContent(ContentTranslation contentTranslation)
        {
            var middleContentGroups = await _middleContentGroupServices.GetWithIncludeAsync<ContentMiddleGroup>
                (
                selector: x => x,
                where: x => x.ContentTranslation_Id == contentTranslation.ContentTranslation_ID,
                include: s => s.Include(x => x.ContentGroupTranslation).ThenInclude(x => x.ContentGroup).Include(x => x.ContentTranslation)
                );

            foreach (var middlecontentGroup in middleContentGroups)
            {
                await GetTypeContents(middlecontentGroup.ContentGroupTranslation, "content");

                var sitemap = await _mainSiteMapServices.GetAsync(x => x.SiteMap_ID == middlecontentGroup.SiteMap_Id);
                var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/SiteMap/" + sitemap.Location + ".xml");
                sm.DeleteUrl($"{_schema}://{_host}/{middlecontentGroup.ContentGroupTranslation.Language_Id}/{_typeContent}-details/" + middlecontentGroup.ContentTranslation.Slug, path);
            }
        }

        public async Task GetTypeContents(ContentGroupTranslation contentGroupTranslation, string typeObj)
        {
            IEnumerable<ContentGroupTranslation> group = null;
            if (typeObj == "content")
            {
                group = await _contentGroupTranslationSrevices.GetWithIncludeAsync
                   (
                   selector: x => x,
                   where: x => x.ContentGroup.ContentGroup_ID == contentGroupTranslation.ContentGroup.ParentId,
                   include: x => x.Include(s => s.ContentGroup));
            }
            else
            {
                byte parentId = 0;
                if (contentGroupTranslation.ParentId == 0)
                    parentId = contentGroupTranslation.ContentGroup.ParentId.Value;
                else
                    parentId = contentGroupTranslation.ParentId;

                group = await _contentGroupTranslationSrevices.GetWithIncludeAsync
                   (
                   selector: x => x,
                   where: x => x.ContentGroup.ContentGroup_ID == parentId,
                   include: x => x.Include(s => s.ContentGroup));
            }

            if (contentGroupTranslation.ParentId == 2 || group.FirstOrDefault().ParentId == 2) _typeContent = "content";
            else if (contentGroupTranslation.ParentId == 3 || group.FirstOrDefault().ParentId == 3) _typeContent = "news";
        }
        #endregion

        #region FileManage
        public async Task<int> AddFileManageGroup(FileManageGroupTranslation fileManageGroupTranslation)
        {
            //check is sitemap.xml exists
            await CheckSiteMapFile();


            //add this group to categories.xml
            await AddCategory(fileManageGroupTranslation, null);

            var mainSiteMap = new MainSiteMap
            {
                Title = fileManageGroupTranslation.Title,
                Location = $"sitemap-files-{MyExtensions.GenerateRandomSlug()}",
                Type = 1
            };

            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/SiteMap/" + mainSiteMap.Location + ".xml");
            string loc_url = Path.Combine($"{_schema}://{_host}/", "sitemap/" + mainSiteMap.Location + ".xml");

            //create new & empty .xml file
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(_default_xml);
            doc.Save(path);

            ChangeCulture();

            mainSiteMap.CreateDate = DateTime.Now;

            //add this group in sitemap.xml
            SiteMap sm = new SiteMap();
            sm.AddSiteMap(loc_url, DateTimeEN(), _path_index);
            sm.ReadTheFile(_path_index);


            await _mainSiteMapServices.Insert(mainSiteMap);
            await _mainSiteMapServices.Save();

            ReverseCulture();

            return mainSiteMap.SiteMap_ID;
        }

        public async Task EditFileManageGroup(FileManageGroupTranslation fileManageGroupTranslation)
        {
            await CheckSiteMapFile();

            await EditCategory(fileManageGroupTranslation, null);
        }

        public async Task DeleteFileManageGroup(FileManageGroupTranslation fileManageGroupTranslation)
        {
            var mainSiteMap = await _mainSiteMapServices.GetAsync
                 (x => x.SiteMap_ID == fileManageGroupTranslation.SiteMap_Id);

            await CheckSiteMapFile();

            await DeleteCategory(fileManageGroupTranslation, null);

            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/SiteMap/" + mainSiteMap.Location + ".xml"); ;
            string loc_url = Path.Combine($"{_schema}://{_host}/", "sitemap/" + mainSiteMap.Location + ".xml");

            System.IO.File.Delete(path);

            SiteMap sm = new SiteMap();
            sm.DeleteSiteMap(loc_url, _path_index);

            //_contentGroupTranslationSrevices.DeleteAsync(contentGroupTranslation);
            //await _contentGroupTranslationSrevices.Save();

            _mainSiteMapServices.DeleteAsync(mainSiteMap);
            await _mainSiteMapServices.Save();
        }

        public async Task<int> AddFileManage(FileManageTranslation fileManageTranslation)
        {
            ChangeCulture();

            var fileGroup = await _fileManageGroupTranslationServices.
                GetAsync(x => x.FileManageGroupTranslation_ID == fileManageTranslation.TranslationGroup_Id);

            var sitemap = await _mainSiteMapServices.GetAsync(x => x.SiteMap_ID == fileGroup.SiteMap_Id);

            var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/SiteMap/" + sitemap.Location + ".xml");

            string date = DateTimeEN();

            if (sm.CheckDate(path))
            {
                fileGroup.SiteMap_Id = await AddSiteMap(sitemap, path, date, fileManageTranslation.Slug, _path_index, true, null, fileGroup.Language_Id, "videos");

                _fileManageGroupTranslationServices.Update(fileGroup);
                await _fileManageGroupTranslationServices.Save();
            }
            else
            {
                sm.AddUrl($"{_schema}://{_host}/{fileGroup.Language_Id}/video-details/" + fileManageTranslation.Slug, date, "0.7", path, "weekly");
                sm.ReadTheFile(path);
            }

            ReverseCulture();
            return fileGroup.SiteMap_Id.Value;
        }

        public async Task<int> EditFileManage(FileManageTranslation fileManageTranslation)
        {
            ChangeCulture();

            var fileGroup = await _fileManageGroupTranslationServices
                .GetAsync(x => x.FileManageGroupTranslation_ID == fileManageTranslation.TranslationGroup_Id);
            var oldFile = await _fileManageTranslationServices
                .GetAsync(x => x.FileManageTranslation_ID == fileManageTranslation.FileManageTranslation_ID);

            // if content slug modified
            if (fileManageTranslation.Slug != oldFile.Slug)
            {
                var sitemap = await _mainSiteMapServices.GetAsync(x => x.SiteMap_ID == fileManageTranslation.SiteMap_Id);
                var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/SiteMap/" + sitemap.Location + ".xml");
                string date = DateTimeEN();

                //checking sitemap file date
                if (sm.CheckDate(path))
                {
                    // if the last content date in file was more 3 month
                    if (fileGroup.SiteMap_Id != null)
                    {
                        if (fileManageTranslation.SiteMap_Id == fileGroup.SiteMap_Id)
                        {
                            // if content sitemap & content group sitemap is same
                            fileGroup.SiteMap_Id = await AddSiteMap(sitemap, path, date, fileManageTranslation.Slug, _path_index, false, oldFile.Slug, fileGroup.Language_Id, "videos");
                            fileManageTranslation.SiteMap_Id = fileGroup.SiteMap_Id;
                            _fileManageGroupTranslationServices.Update(fileGroup);
                            await _fileManageGroupTranslationServices.Save();
                        }
                        else
                        {
                            var sitemapContentGroup = await _mainSiteMapServices.GetAsync(x => x.SiteMap_ID == fileGroup.SiteMap_Id);
                            string PathContentGroup = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/SiteMap/" + sitemapContentGroup.Location + ".xml");
                            //درصورت یکی نبودن فایل های مطلب و گروه  وارد این بلاک میشود
                            //در خط بعدی چک میشود که 
                            //تاریخ آخرین آدرس در فایل گروه قبل تر از مدت زمان مد نظر هست یا خیر
                            //در این صورت آدرس قبلی حذف و آدرس ویرایش شده در فایل جدید اضافه خواهد شد
                            if (sm.CheckDate(PathContentGroup))
                            {
                                fileGroup.SiteMap_Id = await AddSiteMap(sitemapContentGroup, path, date, fileManageTranslation.Slug, _path_index, false, oldFile.Slug, fileGroup.Language_Id, "videos");
                                fileManageTranslation.SiteMap_Id = fileGroup.SiteMap_Id;
                                _fileManageGroupTranslationServices.Update(fileGroup);
                                await _fileManageGroupTranslationServices.Save();
                            }
                            else
                            {
                                // در صورتی که اخرین رکورد در فایل گروه کمتر
                                //از مدت زمان مدنظر باشد در فایل گروه ، ادرس ویرایش شده درج و از محل قبلی حذف خواهد شد
                                string[] s = sm.DeleteUrl($"{_schema}://{_host}/{fileGroup.Language_Id}/video-details/" + oldFile.Slug, path);
                                sm.AddUrl($"{_schema}://{_host}/{fileGroup.Language_Id}/video-details/" + fileManageTranslation.Slug, date, "0.7", PathContentGroup, "weekly");
                                sm.ReadTheFile(PathContentGroup);
                                fileManageTranslation.SiteMap_Id = fileGroup.SiteMap_Id;
                            }
                        }
                    }
                }
                else
                {
                    //در صورتی که تاریخ آخرین ادرس ثبت شده در فایل مطلب کمتر از مدت زمان مدنظر باشد ادرس ویرایش شده در فایل مطلب حذف و آدرس جدید جایگزین خواهد شد.
                    string[] s = sm.DeleteUrl($"{_schema}://{_host}/{fileGroup.Language_Id}/video-details/" + oldFile.Slug, path);
                    sm.AddUrl($"{_schema}://{_host}/{fileGroup.Language_Id}/video-details/" + fileManageTranslation.Slug, date, "0.7", path, "weekly");
                    sm.ReadTheFile(path);
                }
            }

            ReverseCulture();

            return fileManageTranslation.SiteMap_Id.Value;
        }

        public async Task DeleteFileManage(FileManageTranslation fileManageTranslation)
        {
            var sitemap = await _mainSiteMapServices.GetAsync(x => x.SiteMap_ID == fileManageTranslation.SiteMap_Id);
            var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/SiteMap/" + sitemap.Location + ".xml");
            sm.DeleteUrl($"{_schema}://{_host}/{fileManageTranslation.FileManageGroupTranslation.Language_Id}/video-details/" + fileManageTranslation.Slug, path);
        }

        public async Task DeleteAllFileManage(IEnumerable<FileManageTranslation> fileManageTranslations)
        {
            foreach (var fileManageTranslation in fileManageTranslations)
            {
                var sitemap = await _mainSiteMapServices.GetAsync(x => x.SiteMap_ID == fileManageTranslation.SiteMap_Id);
                var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/SiteMap/" + sitemap.Location + ".xml");
                sm.DeleteUrl($"{_schema}://{_host}/{fileManageTranslation.FileManageGroupTranslation.Language_Id}/video-details/" + fileManageTranslation.Slug, path);
            }
        }
        #endregion



        //#region Product
        //public async Task<int> AddProductGroup(ProductsGroup productsGroup)
        //{
        //    ChangeCulture();
        //    await CheckSiteMapFile();
        //    await AddCategory(productsGroup);

        //    var mainSiteMap = new MainSiteMap
        //    {
        //        Title = productsGroup.ProductGroupTitle,
        //        Location = $"sitemap-products-{Extensions.GenerateRandomSlug()}",
        //        Type = 2
        //    };


        //    string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/SiteMap/" + mainSiteMap.Location + ".xml");
        //    string loc_url = Path.Combine($"{_schema}://{_host}/", "sitemap/" + mainSiteMap.Location + ".xml");

        //    XmlDocument doc = new XmlDocument();
        //    doc.LoadXml(_default_xml);
        //    doc.Save(path);

        //    mainSiteMap.CreateDate = DateTime.Now;

        //    SiteMap sm = new SiteMap();
        //    sm.AddSiteMap(loc_url, DateTimeEN(), _path_index);
        //    sm.ReadTheFile(_path_index);

        //    await _mainSiteMapRepository.AddAsync(mainSiteMap);
        //    await _mainSiteMapRepository.SaveChangesAsync();

        //    ReverseCulture();
        //    return mainSiteMap.SiteMap_ID;

        //}
        //public async Task EditProductGroup(ProductsGroup productGroup)
        //{
        //    await CheckSiteMapFile();

        //    await EditCategory(productGroup, null);
        //}

        //public async Task DeleteProductGroup(ProductsGroup productsGroup)
        //{
        //    var mainSiteMap = await _mainSiteMapRepository.GetByIdAsync(productsGroup.SiteMap_ID);

        //    await CheckSiteMapFile();

        //    await DeleteCategory(productsGroup, null);

        //    string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/SiteMap/" + mainSiteMap.Location + ".xml"); ;
        //    string loc_url = Path.Combine($"{_schema}://{_host}/", "sitemap/" + mainSiteMap.Location + ".xml");

        //    System.IO.File.Delete(path);

        //    SiteMap sm = new SiteMap();
        //    sm.DeleteSiteMap(loc_url, _path_index);

        //    await _mainSiteMapRepository.DeleteAsync(mainSiteMap);
        //    await _mainSiteMapRepository.SaveChangesAsync();

        //}

        //public async Task<int> AddProduct(ProductInfo product)
        //{
        //    ChangeCulture();

        //    var productGroup = await _productGroupRepository.GetByIdAsync(product.ProductGroupId);

        //    var sitemap = await _mainSiteMapRepository.GetByIdAsync(productGroup.SiteMap_ID);

        //    var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/SiteMap/" + sitemap.Location + ".xml");

        //    string date = DateTimeEN();

        //    //check time of last record in xml file 
        //    if (sm.CheckDate(path))
        //    {
        //        //add new file
        //        productGroup.SiteMap_ID = await AddSiteMap(sitemap, path, date, product.Slug, _path_index, true, null, productGroup.Lang, "products");

        //        await _productGroupRepository.UpdateAsync(productGroup);
        //        await _productGroupRepository.SaveChangesAsync();
        //    }
        //    else
        //    {
        //        //add url to file 

        //        sm.AddUrl($"{_schema}://{_host}/{productGroup.Lang}/product-details/" + product.Slug, date, "0.7", path, "weekly");
        //        sm.ReadTheFile(path);
        //    }

        //    ReverseCulture();
        //    return productGroup.SiteMap_ID.Value;

        //}

        //public async Task<int> EditProduct(ProductInfo product)
        //{
        //    ChangeCulture();

        //    var productGroup = await _productGroupRepository.GetByIdAsync(product.ProductGroupId);
        //    var productOld = await _productInfoRepository.GetByIdAsync(product.ProductInfo_ID, tracking: true);

        //    // if content slug modified
        //    if (product.Slug != productOld.Slug)
        //    {
        //        var sitemap = await _mainSiteMapRepository.GetByIdAsync(product.SiteMap_ID);
        //        var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/SiteMap/" + sitemap.Location + ".xml");
        //        string date = DateTimeEN();

        //        //checking sitemap file date
        //        if (sm.CheckDate(path))
        //        {
        //            // if the last content date in file was more 3 month
        //            if (productGroup.SiteMap_ID != null)
        //            {
        //                if (product.SiteMap_ID == productGroup.SiteMap_ID)
        //                {
        //                    // if content sitemap & content group sitemap is same
        //                    productGroup.SiteMap_ID = await AddSiteMap(sitemap, path, date, product.Slug, _path_index, false, productOld.Slug, productGroup.Lang, "products");
        //                    product.SiteMap_ID = productGroup.SiteMap_ID;
        //                    await _productGroupRepository.UpdateAsync(productGroup);
        //                    await _productGroupRepository.SaveChangesAsync();
        //                }
        //                else
        //                {
        //                    var sitemapProductGroup = await _mainSiteMapRepository.GetByIdAsync(productGroup.SiteMap_ID);
        //                    string PathProductGroup = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/SiteMap/" + sitemapProductGroup.Location + ".xml");
        //                    //درصورت یکی نبودن فایل های مطلب و گروه  وارد این بلاک میشود
        //                    //در خط بعدی چک میشود که 
        //                    //تاریخ آخرین آدرس در فایل گروه قبل تر از مدت زمان مد نظر هست یا خیر
        //                    //در این صورت آدرس قبلی حذف و آدرس ویرایش شده در فایل جدید اضافه خواهد شد
        //                    if (sm.CheckDate(PathProductGroup))
        //                    {
        //                        productGroup.SiteMap_ID = await AddSiteMap(sitemapProductGroup, path, date, product.Slug, _path_index, false, productOld.Slug, productGroup.Lang, "products");
        //                        product.SiteMap_ID = productGroup.SiteMap_ID;
        //                        await _productGroupRepository.UpdateAsync(productGroup);
        //                        await _productGroupRepository.SaveChangesAsync();
        //                    }
        //                    else
        //                    {
        //                        // در صورتی که اخرین رکورد در فایل گروه کمتر
        //                        //از مدت زمان مدنظر باشد در فایل گروه ، ادرس ویرایش شده درج و از محل قبلی حذف خواهد شد
        //                        string[] s = sm.DeleteUrl($"{_schema}://{_host}/{productGroup.Lang}/product-details/" + productOld.Slug, path);
        //                        sm.AddUrl($"{_schema}://{_host}/{productGroup.Lang}/product-details/" + product.Slug, date, "0.7", PathProductGroup, "weekly");
        //                        sm.ReadTheFile(PathProductGroup);
        //                        product.SiteMap_ID = productGroup.SiteMap_ID;
        //                    }
        //                }
        //            }
        //        }
        //        else
        //        {
        //            //در صورتی که تاریخ آخرین ادرس ثبت شده در فایل مطلب کمتر از مدت زمان مدنظر باشد ادرس ویرایش شده در فایل مطلب حذف و آدرس جدید جایگزین خواهد شد.
        //            sm.DeleteUrl($"{_schema}://{_host}/{productGroup.Lang}/product-details/" + productOld.Slug, path);
        //            sm.AddUrl($"{_schema}://{_host}/{productGroup.Lang}/product-details/" + product.Slug, date, "0.7", path, "weekly");
        //            sm.ReadTheFile(path);
        //        }
        //    }
        //    ReverseCulture();

        //    return product.SiteMap_ID.Value;
        //}

        //public async Task DeleteProduct(ProductInfo product)
        //{
        //    var productGroup = await _productGroupRepository.GetByIdAsync(product.ProductGroupId);

        //    var sitemap = await _mainSiteMapRepository.GetByIdAsync(product.SiteMap_ID);
        //    var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/SiteMap/" + sitemap.Location + ".xml");
        //    sm.DeleteUrl($"{_schema}://{_host}/{product.ProductsGroup.Lang}/product-details/" + product.Slug, path);
        //}


        //#endregion

        #region Common Functions

        [Obsolete]
        public async Task<int> AddSiteMap(MainSiteMap sitemap, string path, string date, string slug,
                   string pathIndex, bool IsAdd, string OldSlug = null, string lang = null, string model = null)
        {

            var url = "";
            if (model == "news") url = "news-details";
            else if (model == "content") url = "content-details";
            else if (model == "products") url = "product-details";
            else if (model == "videos") url = "video-details";

            ChangeCulture();

            //change text content
            model = model == "content" ? "contents" : model;

            var NewSiteMap = new MainSiteMap
            {
                Location = $"sitemap-{model}-{ MyExtensions.GenerateRandomSlug() }",
                CreateDate = DateTime.Now,
                Type = sitemap.Type
            };

            NewSiteMap.Title = NewSiteMap.Location;

            //create new sitemap and initial values
            string NewPath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/SiteMap/" + NewSiteMap.Location + ".xml");
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(_default_xml);
            doc.Save(NewPath);

            await _mainSiteMapServices.Insert(NewSiteMap);
            await _mainSiteMapServices.Save();

            if (!IsAdd)
            {
                string[] s = sm.DeleteUrl($"{_schema}://{_host}/{lang}/{url}/" + OldSlug, path);
            }

            //add new url to sitemap file

            sm.AddUrl($"{_schema}://{_host}/{lang}/{url}/{ slug}", date, "0.7", NewPath, "weekly");
            sm.ReadTheFile(NewPath);
            string loc_url = Path.Combine($"{_schema}://{_host}/", "sitemap/" + NewSiteMap.Location + ".xml");

            //add new sitemap to sitemap.xml
            sm.AddSiteMap(loc_url, date, pathIndex);
            sm.ReadTheFile(pathIndex);
            ReverseCulture();

            return NewSiteMap.SiteMap_ID;

        }
        public async Task CheckSiteMapFile()
        {
            if (!Directory.Exists("wwwroot/Files/SiteMap"))
            {
                Directory.CreateDirectory("wwwroot/Files/SiteMap");
            }

            if (!System.IO.File.Exists("wwwroot/sitemap.xml"))
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(_default_index_xml);
                doc.Save(_path_index);

            }

            await Task.CompletedTask;
        }
        public async Task AddCategory(FileManageGroupTranslation fileManageGroupTranslation = null, ContentGroupTranslation contentGroupTranslation = null, ProductGroupTranslation productGroupTranslation = null)
        {
            ChangeCulture();

            await CheckSiteMapFile();

            var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/SiteMap/categories.xml");
            if (!System.IO.File.Exists("wwwroot/Files/SiteMap/categories.xml"))
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(_default_xml);
                doc.Save(path);

            }

            if (productGroupTranslation != null)
            {
                sm.AddUrl($"{_schema}://{_host}/{productGroupTranslation.Language_Id}/products-archive/" + productGroupTranslation.Slug, DateTimeEN(), "0.8", path, "daily");
                sm.ReadTheFile(path);
            }
            else if (fileManageGroupTranslation != null)
            {
                sm.AddUrl($"{_schema}://{_host}/{fileManageGroupTranslation.Language_Id}/files-archive/" + fileManageGroupTranslation.Slug, DateTimeEN(), "0.7", path, "daily");
                sm.ReadTheFile(path);

            }
            else if (contentGroupTranslation != null)
            {
                sm.AddUrl($"{_schema}://{_host}/{contentGroupTranslation.Language_Id}/{(_typeContent == "content" ? "contents" : "news")}/" + contentGroupTranslation.Slug, DateTimeEN(), "0.8", path, "daily");
                sm.ReadTheFile(path);
            }

            ReverseCulture();
        }
        public async Task EditCategory(FileManageGroupTranslation fileManageGroupTranslation = null, ContentGroupTranslation contentGroupTranslation = null, ProductGroupTranslation productGroupTranslation = null)
        {
            ChangeCulture();

            await CheckSiteMapFile();

            var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/SiteMap/categories.xml");
            if (!System.IO.File.Exists("wwwroot/Files/SiteMap/categories.xml"))
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(_default_xml);
                doc.Save(path);

            }
            if (productGroupTranslation != null)
            {

                var oldProductGroup = await _productGroupTranslationServices.GetAsync(x => x.ProductGroup_Id == productGroupTranslation.ProductGroup_Id && x.Language_Id == productGroupTranslation.Language_Id, tracking: true);
                if (oldProductGroup != null)
                {
                    sm.DeleteUrl($"{_schema}://{_host}/{productGroupTranslation.Language_Id}/products-archive/" + oldProductGroup.Slug, path);
                    sm.AddUrl($"{_schema}://{_host}/{productGroupTranslation.Language_Id}/products-archive/" + productGroupTranslation.Slug, DateTimeEN(), "0.8", path, "daily");

                    sm.ReadTheFile(path);
                }
            }
            else if (fileManageGroupTranslation != null)
            {
                var oldFileManageGroup = await _fileManageGroupTranslationServices
                    .GetAsync(x => x.FileManageGroup_Id == fileManageGroupTranslation.FileManageGroup_Id && x.Language_Id == fileManageGroupTranslation.Language_Id, tracking: true);

                sm.DeleteUrl($"{_schema}://{_host}/{fileManageGroupTranslation.Language_Id}/files-archive/" + oldFileManageGroup.Slug, path);
                sm.AddUrl($"{_schema}://{_host}/{fileManageGroupTranslation.Language_Id}/files-archive/" + fileManageGroupTranslation.Slug, DateTimeEN(), "0.8", path, "daily");

                sm.ReadTheFile(path);
            }
            else if (contentGroupTranslation != null)
            {
                var oldContentGroup = await _contentGroupTranslationSrevices
                    .GetAsync(x => x.ContentGroup_Id == contentGroupTranslation.ContentGroup_Id && x.Language_Id == contentGroupTranslation.Language_Id, tracking: true);

                sm.DeleteUrl($"{_schema}://{_host}/{contentGroupTranslation.Language_Id}/{(_typeContent == "content" ? "contents" : "news")}/" + oldContentGroup.Language_Id, path);
                sm.AddUrl($"{_schema}://{_host}/{contentGroupTranslation.Language_Id}/{(_typeContent == "content" ? "contents" : "news")}/" + oldContentGroup.Language_Id, DateTimeEN(), "0.8", path, "daily");

                sm.ReadTheFile(path);
            }
            ReverseCulture();
        }
        public async Task DeleteCategory(FileManageGroupTranslation fileManageGroupTranslation = null, ContentGroupTranslation contentGroupTranslation = null, ProductGroupTranslation productGroupTranslation = null)
        {
            await CheckSiteMapFile();


            var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/SiteMap/categories.xml");
            if (!System.IO.File.Exists("wwwroot/Files/SiteMap/categories.xml"))
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(_default_xml);
                doc.Save(path);

            }
            if (productGroupTranslation != null)
            {
                sm.DeleteUrl($"{_schema}://{_host}/{productGroupTranslation.Language_Id}/products-archive/" + productGroupTranslation.Slug, path);
            }
            else if (fileManageGroupTranslation != null)
            {
                sm.DeleteUrl($"{_schema}://{_host}/{fileManageGroupTranslation.Language_Id}/files-archive/" + fileManageGroupTranslation.Slug, path);
            }
            else if (contentGroupTranslation != null)
            {
                sm.DeleteUrl($"{_schema}://{_host}/{contentGroupTranslation.Language_Id}/{(_typeContent == "content" ? "contents" : "news")}/" + contentGroupTranslation.Language_Id, path);
            }
        }
        public void ChangeCulture()
        {
            _currentLang = CultureInfo.CurrentCulture.Name;
            CultureInfo.CurrentCulture = new CultureInfo("en");
        }
        public void ReverseCulture() => CultureInfo.CurrentCulture = new CultureInfo(_currentLang);

        public string DateTimeEN()
        {
            return DateTime.Now.ToString("yyyy-MM-dd");
        }

        #region Product

        public async Task<int> AddProductGroup(IEnumerable<ProductGroupTranslation> productGroupTranslations)
        {
            //check is sitemap.xml exists
            await CheckSiteMapFile();

            //add this group to categories.xml
            foreach (var productGroupTranslation in productGroupTranslations)
            {
                await AddCategory(null, null, productGroupTranslation);
            }

            var mainSiteMap = new MainSiteMap
            {
                Title = productGroupTranslations.FirstOrDefault().Title,
                Location = $"sitemap-products-{MyExtensions.GenerateRandomSlug()}",
                Type = 1
            };

            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/SiteMap/" + mainSiteMap.Location + ".xml");
            string loc_url = Path.Combine($"{_schema}://{_host}/", "sitemap/" + mainSiteMap.Location + ".xml");

            //create new & empty .xml file
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(_default_xml);
            doc.Save(path);

            ChangeCulture();

            mainSiteMap.CreateDate = DateTime.Now;

            //add this group in sitemap.xml
            SiteMap sm = new SiteMap();
            sm.AddSiteMap(loc_url, DateTimeEN(), _path_index);
            sm.ReadTheFile(_path_index);


            await _mainSiteMapServices.Insert(mainSiteMap);
            await _mainSiteMapServices.Save();

            ReverseCulture();

            return mainSiteMap.SiteMap_ID;
        }

        public async Task EditProductGroup(ProductGroupTranslation productGroupTranslations)
        {
            await CheckSiteMapFile();
            await EditCategory(null, null, productGroupTranslations);

        }

        public async Task DeleteProductGroup(IEnumerable<ProductGroupTranslation> productGroupTranslations)
        {
            var mainSiteMap = await _mainSiteMapServices.GetAsync
                  (x => x.SiteMap_ID == productGroupTranslations.FirstOrDefault().ProductGroup.SiteMap_Id);

            await CheckSiteMapFile();

            foreach (var productGroupTranslation in productGroupTranslations)
            {
                await DeleteCategory(null, null, productGroupTranslation);
            }

            if (mainSiteMap != null)
            {
                string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/SiteMap/" + mainSiteMap.Location + ".xml"); ;
                string loc_url = Path.Combine($"{_schema}://{_host}/", "sitemap/" + mainSiteMap.Location + ".xml");
                if (File.Exists(path))
                    System.IO.File.Delete(path);

                SiteMap sm = new SiteMap();
                sm.DeleteSiteMap(loc_url, _path_index);


                _mainSiteMapServices.DeleteAsync(mainSiteMap);
                await _mainSiteMapServices.Save();
            }
        }

        public async Task DeleteProductGroup(ProductGroupTranslation productGroupTranslation, int productGroupTransCount)
        {
            var mainSiteMap = await _mainSiteMapServices.GetAsync
                (x => x.SiteMap_ID == productGroupTranslation.ProductGroup.SiteMap_Id);

            await CheckSiteMapFile();

            await DeleteCategory(null, null, productGroupTranslation);

            if (productGroupTransCount == 1 && mainSiteMap != null)
            {
                string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/SiteMap/" + mainSiteMap.Location + ".xml"); ;
                string loc_url = Path.Combine($"{_schema}://{_host}/", "sitemap/" + mainSiteMap.Location + ".xml");
                if (File.Exists(path))
                    System.IO.File.Delete(path);

                SiteMap sm = new SiteMap();
                sm.DeleteSiteMap(loc_url, _path_index);


                if (mainSiteMap != null)
                {
                    _mainSiteMapServices.DeleteAsync(mainSiteMap);
                    await _mainSiteMapServices.Save();
                }
            }
        }

        public async Task<int> AddProductGroup(ProductGroupTranslation productGroupTranslation, bool isExistSiteMap)
        {
            //check is sitemap.xml exists
            await CheckSiteMapFile();

            //add this group to categories.xml
            await AddCategory(null, null, productGroupTranslation);

            if (!isExistSiteMap)
            {
                var mainSiteMap = new MainSiteMap
                {
                    Title = productGroupTranslation.Title,
                    Location = $"sitemap-products-{MyExtensions.GenerateRandomSlug()}",
                    Type = 1
                };

                string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/SiteMap/" + mainSiteMap.Location + ".xml");
                string loc_url = Path.Combine($"{_schema}://{_host}/", "sitemap/" + mainSiteMap.Location + ".xml");

                //create new & empty .xml file
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(_default_xml);
                doc.Save(path);

                ChangeCulture();

                mainSiteMap.CreateDate = DateTime.Now;

                //add this group in sitemap.xml
                SiteMap sm = new SiteMap();
                sm.AddSiteMap(loc_url, DateTimeEN(), _path_index);
                sm.ReadTheFile(_path_index);


                await _mainSiteMapServices.Insert(mainSiteMap);
                await _mainSiteMapServices.Save();

                ReverseCulture();

                return mainSiteMap.SiteMap_ID;
            }
            return 0;
        }

        public async Task<List<int>> AddProduct(IEnumerable<ProductTranslation> productTranslations, List<int> productGroupIds)
        {
            List<int> siteMapIds = new List<int>();
            ChangeCulture();
            for (int i = 0; i < productGroupIds.Count; i++)
            {
                var productGroup = await _productGroupServices.GetWithIncludeAsync
                    (
                    selector: x => x,
                    where: x => x.ProductGroup_ID == productGroupIds[i],
                    include: s => s.Include(x => x.ProductGroupTranslations)
                    );
                var siteMap = await _mainSiteMapServices.GetAsync(x => x.SiteMap_ID == productGroup.FirstOrDefault().SiteMap_Id);
                var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/SiteMap/" + siteMap.Location + ".xml");

                string date = DateTimeEN();

                //check time of last record in xml file 
                //if (sm.CheckDate(path))
                //{
                //    foreach (var productTranslation in productTranslations)
                //    {
                //        //add new file
                //        productGroup.FirstOrDefault().SiteMap_Id = await AddSiteMap(siteMap, path, date, productTranslation.Slug, _path_index, true, null, productGroup.FirstOrDefault().ProductGroupTranslations.FirstOrDefault().Language_Id, "products");
                //        _productGroupServices.Update(productGroup.FirstOrDefault());
                //        await _productGroupServices.Save();
                //    }
                //}
                //else
                //{
                foreach (var productTranslation in productTranslations)
                {
                    //add url to file 
                    sm.AddUrl($"{_schema}://{_host}/{productTranslation.Language_Id}/product-details/" + productTranslation.Slug, date, "0.7", path, "weekly");
                    sm.ReadTheFile(path);
                }
                //}
                ReverseCulture();
                siteMapIds.Add(productGroup.FirstOrDefault().SiteMap_Id.Value);
            }
            return siteMapIds;
        }

        public async Task DeleteProduct(ProductTranslation productTranslation, int productTransCount, string status)
        {
            if (productTransCount == 1)
            {
                var productMiddleGroups = await _productMiddleGroupServices.GetWithIncludeAsync
                   (
                   selector: x => x,
                   where: x => x.Product_Id == productTranslation.Product_Id,
                   include: s => s.Include(x => x.Product).ThenInclude(x => x.ProductTranslations).Include(x => x.ProductGroup)
                   );

                if (productMiddleGroups.Count() > 0)
                {
                    var products = productMiddleGroups.FirstOrDefault().Product.ProductTranslations;

                    foreach (var item in productMiddleGroups)
                    {

                        var sitemap = await _mainSiteMapServices.GetAsync(x => x.SiteMap_ID == item.ProductGroup.SiteMap_Id);
                        var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/SiteMap/" + sitemap.Location + ".xml");
                        sm.DeleteUrl($"{_schema}://{_host}/{products.FirstOrDefault().Language_Id}/product-details/" + products.FirstOrDefault().Slug, path);
                    }
                }
            }
            else
            {
                var productMiddleGroups = await _productMiddleGroupServices.GetWithIncludeAsync
                 (
                 selector: x => x,
                 where: x => x.Product_Id == productTranslation.Product_Id,
                 include: s => s.Include(x => x.Product).ThenInclude(x => x.ProductTranslations).Include(x => x.ProductGroup)
                 );
                if (productMiddleGroups.Count() > 0)
                {
                    foreach (var product in productMiddleGroups)
                    {
                        var sitemap = await _mainSiteMapServices.GetAsync(x => x.SiteMap_ID == product.ProductGroup.SiteMap_Id);
                        var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files/SiteMap/" + sitemap.Location + ".xml");
                        if (status == "one")
                        {
                            var thisProduct = product.Product.ProductTranslations.FirstOrDefault(x => x.Language_Id == CultureInfo.CurrentCulture.Name);
                            sm.DeleteUrl($"{_schema}://{_host}/{thisProduct.Language_Id}/product-details/" + thisProduct.Slug, path);
                        }
                        else if (status == "all")
                        {
                            var productTranslations = await _productTranslationServices.GetAllAsyn(x => x.Product_Id == productTranslation.Product_Id);
                            foreach (var productTrans in productTranslations)
                            {
                                sm.DeleteUrl($"{_schema}://{_host}/{productTrans.Language_Id}/product-details/" + productTrans.Slug, path);
                            }
                        }
                    }
                }
            }

        }

        #endregion



        //public async Task InitialProductGroup()
        //{
        //    var menu = await _productGroupRepository.GetAllAsync(x => x.ParentId <= 2);
        //    var vm = new List<ProductGroupMenuViewModel>();
        //    foreach (var item in menu)
        //    {
        //        var model = new ProductGroupMenuViewModel();
        //        var group = await _productGroupRepository.GetAllAsync(x => x.ParentId == item.ProductGroup_ID);
        //        var isparent = group.Any() ? true : false;
        //        if (group.Any())
        //        {
        //            isparent = true;
        //            model.Childrens = group;
        //        }
        //        else
        //        {
        //            isparent = false;

        //        }
        //        model.ProductsGroup = item;
        //        vm.Add(model);
        //    }



        //    foreach (var item in vm)
        //    {
        //        await AddProductGroup(item.ProductsGroup);

        //        foreach (var child in item.Childrens)
        //        {
        //            await AddProductGroup(child);
        //        }
        //    }

        //}


        #endregion

    }

    public class SiteMap
    {
        public SiteMap()
        {
        }

        [Obsolete]


        public void AddSiteMap(string Url, string CreateDate, string path)
        {
            XmlDataDocument doc = new XmlDataDocument();
            doc.Load(path);

            XmlElement RootElement = (XmlElement)doc.GetElementsByTagName("sitemapindex").Item(0);
            XmlElement urlElement = doc.CreateElement("sitemap");
            XmlElement locElement = doc.CreateElement("loc");
            locElement.InnerText = Url;
            urlElement.AppendChild(locElement);
            XmlElement dateElement = doc.CreateElement("lastmod");
            dateElement.InnerText = CreateDate;
            urlElement.AppendChild(dateElement);
            RootElement.AppendChild(urlElement);

            doc.Save(path);

        }

        [Obsolete]
        public void DeleteSiteMap(string Url, string path)
        {
            Url = Url.ToLower();
            XmlDataDocument doc = new XmlDataDocument();
            doc.Load(path);
            XmlElement RootElement = (XmlElement)doc.GetElementsByTagName("sitemapindex").Item(0);

            foreach (XmlElement element in RootElement.ChildNodes)
            {
                string tempUrl = element.ChildNodes[0].InnerText.ToLower();
                if (tempUrl == Url)
                {
                    XmlNode node = element;
                    node.ParentNode.RemoveChild(node);
                }
            }
            doc.Save(path);
        }

        [Obsolete]
        public bool CheckDate(string path)
        {
            XmlDataDocument doc = new XmlDataDocument();
            doc.Load(path);
            var lastUrl = doc.GetElementsByTagName("urlset").Item(0).LastChild;
            if (lastUrl != null)
            {
                var LastDate = lastUrl["lastmod"].InnerText.Split('-');
                var date = new DateTime(int.Parse(LastDate[0]), int.Parse(LastDate[1]), int.Parse(LastDate[2]));
                var date3MonthAgo = DateTime.Now.AddMonths(-3).Date;
                if (date < date3MonthAgo)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            return false;

        }

        [Obsolete]
        public void AddUrl(string Url, string CreateDate, string Priority, string path, string ChangeFreq)
        {
            string[] msg = new string[2];
            //msg = CheckUrl(Url, CreateDate, Priority);
            //if (msg[0] == "valid")
            //{
            XmlDataDocument doc = new XmlDataDocument();
            doc.Load(path);
            XmlElement RootElement = (XmlElement)doc.GetElementsByTagName("urlset").Item(0);
            XmlElement urlElement = doc.CreateElement("url");
            XmlElement locElement = doc.CreateElement("loc");
            locElement.InnerText = Url;
            urlElement.AppendChild(locElement);

            XmlElement dateElement = doc.CreateElement("lastmod");
            dateElement.InnerText = CreateDate;
            urlElement.AppendChild(dateElement);

            XmlElement changefreq = doc.CreateElement("changefreq");
            changefreq.InnerText = ChangeFreq;
            urlElement.AppendChild(changefreq);

            XmlElement priorityElement = doc.CreateElement("priority");
            priorityElement.InnerText = Priority;
            urlElement.AppendChild(priorityElement);

            RootElement.AppendChild(urlElement);


            doc.Save(path);

        }

        [Obsolete]
        public string[] DeleteUrl(string Url, string path)
        {
            Url = Url.ToLower();
            string[] msg = new string[1];
            XmlDataDocument doc = new XmlDataDocument();
            doc.Load(path);
            XmlElement RootElement = (XmlElement)doc.GetElementsByTagName("urlset").Item(0);

            foreach (XmlElement element in RootElement.ChildNodes)
            {
                string tempUrl = element.ChildNodes[0].InnerText.ToLower();
                if (tempUrl == Url)
                {
                    XmlNode node = element;
                    node.ParentNode.RemoveChild(node);
                    msg[0] = "ok";
                }
                else
                {
                    msg[0] = "notFound";
                }
            }
            doc.Save(path);
            return msg;
        }


        public void ReadTheFile(string path)
        {
            string line = "";
            using (StreamReader sr = new StreamReader(path))
            {
                line = sr.ReadToEnd();
                line = line.Replace(" xmlns=\"\"", "");

            }
            using (StreamWriter sw = new StreamWriter(path))
            {
                sw.Write(line);
            }
        }
    }

}

