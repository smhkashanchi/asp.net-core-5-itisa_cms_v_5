﻿using Application.Interfaces;

using Domain.DomainClasses.Feature;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class FeatureTranslationServices : IFeatureTranslationServices
    {
        private IFeatureTranslationRepository _featureTranslationRepository;
        public FeatureTranslationServices(IFeatureTranslationRepository featureTranslationRepository)
        {
            _featureTranslationRepository = featureTranslationRepository;
        }
        public void Delete(FeatureTranslation featureTranslation)
        {
            _featureTranslationRepository.Delete(featureTranslation);
        }

        public void DeleteAll(IEnumerable<FeatureTranslation> featureTranslations)
        {
            _featureTranslationRepository.DeleteAll(featureTranslations);
        }

        public async Task<bool> ExistsAsync(Expression<Func<FeatureTranslation, bool>> where = null, Func<IQueryable<FeatureTranslation>, IIncludableQueryable<FeatureTranslation, object>> include = null)
        {
            return await _featureTranslationRepository.ExistsAsync(where, include);
        }

        public async Task<IEnumerable<FeatureTranslation>> GetAllAsyn(Expression<Func<FeatureTranslation, bool>> where = null, Func<IQueryable<FeatureTranslation>, IOrderedQueryable<FeatureTranslation>> orderBy = null, Func<IQueryable<FeatureTranslation>, IIncludableQueryable<FeatureTranslation, object>> include = null, bool tracking = false)
        {
            return await _featureTranslationRepository.GetAllAsyn(where, orderBy, include);
        }

        public async Task<FeatureTranslation> GetAsync(Expression<Func<FeatureTranslation, bool>> where = null, Func<IQueryable<FeatureTranslation>, IOrderedQueryable<FeatureTranslation>> orderBy = null, Func<IQueryable<FeatureTranslation>, IIncludableQueryable<FeatureTranslation, object>> include = null, bool tracking = false)
        {
            return await _featureTranslationRepository.GetAsync(where, orderBy, include);
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<FeatureTranslation, TResult>> selector = null, Expression<Func<FeatureTranslation, bool>> where = null, Func<IQueryable<FeatureTranslation>, IOrderedQueryable<FeatureTranslation>> orderBy = null, Func<IQueryable<FeatureTranslation>, IIncludableQueryable<FeatureTranslation, object>> include = null, bool disableTracking = true)
        {
            return await _featureTranslationRepository.GetWithIncludeAsync(selector, where, orderBy, include);
        }

        public async Task Insert(FeatureTranslation featureTranslation)
        {
            await _featureTranslationRepository.Insert(featureTranslation);
        }

        public async Task Save()
        {
            await _featureTranslationRepository.Save();
        }

        public void Update(FeatureTranslation featureTranslation)
        {
            _featureTranslationRepository.Update(featureTranslation);
        }
    }
}
