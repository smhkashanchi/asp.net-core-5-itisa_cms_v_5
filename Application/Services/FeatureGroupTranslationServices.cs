﻿using Application.Interfaces;

using Domain.DomainClasses.Feature;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class FeatureGroupTranslationServices : IFeatureGroupTranslationServices
    {
        private IFeatureGroupTranslationRepository _featureGroupTranslationRepository;
        public FeatureGroupTranslationServices(IFeatureGroupTranslationRepository featureGroupTranslationRepository)
        {
            _featureGroupTranslationRepository = featureGroupTranslationRepository;
        }
        public void Delete(FeatureGroupTranslation featureGroupTranslation)
        {
            _featureGroupTranslationRepository.Delete(featureGroupTranslation);
        }

        public async Task<bool> ExistsAsync(Expression<Func<FeatureGroupTranslation, bool>> where = null, Func<IQueryable<FeatureGroupTranslation>, IIncludableQueryable<FeatureGroupTranslation, object>> include = null)
        {
            return await _featureGroupTranslationRepository.ExistsAsync(where, include);
        }

        public async Task<IEnumerable<FeatureGroupTranslation>> GetAllAsyn(Expression<Func<FeatureGroupTranslation, bool>> where = null, Func<IQueryable<FeatureGroupTranslation>, IOrderedQueryable<FeatureGroupTranslation>> orderBy = null, Func<IQueryable<FeatureGroupTranslation>, IIncludableQueryable<FeatureGroupTranslation, object>> include = null, bool tracking = false)
        {
            return await _featureGroupTranslationRepository.GetAllAsyn(where, orderBy, include);
        }

        public async Task<FeatureGroupTranslation> GetAsync(Expression<Func<FeatureGroupTranslation, bool>> where = null, Func<IQueryable<FeatureGroupTranslation>, IOrderedQueryable<FeatureGroupTranslation>> orderBy = null, Func<IQueryable<FeatureGroupTranslation>, IIncludableQueryable<FeatureGroupTranslation, object>> include = null, bool tracking = false)
        {
            return await _featureGroupTranslationRepository.GetAsync(where, orderBy, include);
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<FeatureGroupTranslation, TResult>> selector = null, Expression<Func<FeatureGroupTranslation, bool>> where = null, Func<IQueryable<FeatureGroupTranslation>, IOrderedQueryable<FeatureGroupTranslation>> orderBy = null, Func<IQueryable<FeatureGroupTranslation>, IIncludableQueryable<FeatureGroupTranslation, object>> include = null, bool disableTracking = true)
        {
            return await _featureGroupTranslationRepository.GetWithIncludeAsync(selector, where, orderBy, include);
        }

        public async Task Insert(FeatureGroupTranslation featureGroupTranslation)
        {
            await _featureGroupTranslationRepository.Insert(featureGroupTranslation);
        }

        public async Task Save()
        {
            await _featureGroupTranslationRepository.Save();
        }

        public void Update(FeatureGroupTranslation featureGroupTranslation)
        {
            _featureGroupTranslationRepository.Update(featureGroupTranslation);
        }
    }
}
