﻿using Application.Interfaces;

using Domain.DomainClasses.SendType;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class SendTypeServices : ISendTypeServices
    {
        private ISendTypeRepository _sendTypeRepository;
        public SendTypeServices(ISendTypeRepository sendTypeRepository)
        {
            _sendTypeRepository = sendTypeRepository;
        }
        public void DeleteAsync(SendType sendType)
        {
            _sendTypeRepository.DeleteAsync(sendType);
        }

        public async Task<bool> ExistsAsync(Expression<Func<SendType, bool>> where = null, Func<IQueryable<SendType>, IIncludableQueryable<SendType, object>> include = null)
        {
            return await _sendTypeRepository.ExistsAsync(where, include);
        }

        public async Task<IEnumerable<SendType>> GetAllAsyn(Expression<Func<SendType, bool>> where = null, Func<IQueryable<SendType>, IOrderedQueryable<SendType>> orderBy = null, Func<IQueryable<SendType>, IIncludableQueryable<SendType, object>> include = null, bool tracking = false)
        {
            return await _sendTypeRepository.GetAllAsyn(where, orderBy, include);
        }

        public async Task<SendType> GetAsync(Expression<Func<SendType, bool>> where = null, Func<IQueryable<SendType>, IOrderedQueryable<SendType>> orderBy = null, Func<IQueryable<SendType>, IIncludableQueryable<SendType, object>> include = null, bool tracking = false)
        {
            return await _sendTypeRepository.GetAsync(where, orderBy, include);
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<SendType, TResult>> selector = null, Expression<Func<SendType, bool>> where = null, Func<IQueryable<SendType>, IOrderedQueryable<SendType>> orderBy = null, Func<IQueryable<SendType>, IIncludableQueryable<SendType, object>> include = null, bool disableTracking = true)
        {
            return await _sendTypeRepository.GetWithIncludeAsync(selector, where, orderBy, include);
        }

        public async Task Insert(SendType sendType)
        {
            await _sendTypeRepository.Insert(sendType);
        }

        public async Task Save()
        {
            await _sendTypeRepository.Save();
        }

        public async Task Update(SendType sendType)
        {
            await _sendTypeRepository.Update(sendType);
        }
    }
}
