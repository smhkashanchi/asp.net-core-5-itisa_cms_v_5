﻿using Application.Interfaces;

using Domain.DomainClasses.FilesManage;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class FileManageGroupTranslationServices : IFileManageGroupTranslationServices
    {
        private IFileManageGroupTranslationRepository _fileManageGroupTranslationRepository;
        public FileManageGroupTranslationServices(IFileManageGroupTranslationRepository fileManageGroupTranslationRepository)
        {
            _fileManageGroupTranslationRepository = fileManageGroupTranslationRepository;
        }
        public void Delete(FileManageGroupTranslation fileManageGroupTranslation)
        {
            _fileManageGroupTranslationRepository.Delete(fileManageGroupTranslation);
        }

        public async Task<bool> ExistsAsync(Expression<Func<FileManageGroupTranslation, bool>> where = null, Func<IQueryable<FileManageGroupTranslation>, IIncludableQueryable<FileManageGroupTranslation, object>> include = null)
        {
            return await _fileManageGroupTranslationRepository.ExistsAsync(where, include);
        }

        public async Task<IEnumerable<FileManageGroupTranslation>> GetAllAsyn(Expression<Func<FileManageGroupTranslation, bool>> where = null, Func<IQueryable<FileManageGroupTranslation>, IOrderedQueryable<FileManageGroupTranslation>> orderBy = null, Func<IQueryable<FileManageGroupTranslation>, IIncludableQueryable<FileManageGroupTranslation, object>> include = null, bool tracking = false)
        {
            return await _fileManageGroupTranslationRepository.GetAllAsyn(where, orderBy, include);
        }

        public async Task<FileManageGroupTranslation> GetAsync(Expression<Func<FileManageGroupTranslation, bool>> where = null, Func<IQueryable<FileManageGroupTranslation>, IOrderedQueryable<FileManageGroupTranslation>> orderBy = null, Func<IQueryable<FileManageGroupTranslation>, IIncludableQueryable<FileManageGroupTranslation, object>> include = null, bool tracking = false)
        {
            return await _fileManageGroupTranslationRepository.GetAsync(where, orderBy, include);
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<FileManageGroupTranslation, TResult>> selector = null, Expression<Func<FileManageGroupTranslation, bool>> where = null, Func<IQueryable<FileManageGroupTranslation>, IOrderedQueryable<FileManageGroupTranslation>> orderBy = null, Func<IQueryable<FileManageGroupTranslation>, IIncludableQueryable<FileManageGroupTranslation, object>> include = null, bool disableTracking = true)
        {
            return await _fileManageGroupTranslationRepository.GetWithIncludeAsync(selector, where, orderBy, include);
        }

        public async Task Insert(FileManageGroupTranslation fileManageGroupTranslation)
        {
            await _fileManageGroupTranslationRepository.Insert(fileManageGroupTranslation);
        }

        public async Task Save()
        {
            await _fileManageGroupTranslationRepository.Save();
        }

        public void Update(FileManageGroupTranslation fileManageGroupTranslation)
        {
            _fileManageGroupTranslationRepository.Update(fileManageGroupTranslation);
        }
    }
}
