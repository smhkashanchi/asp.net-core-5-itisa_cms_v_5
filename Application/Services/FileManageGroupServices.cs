﻿using Application.Interfaces;

using Domain.DomainClasses.FilesManage;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class FileManageGroupServices : IFileManageGroupServices
    {
        private IFileManageGroupRepository _fileManageGroupRepository;
        public FileManageGroupServices(IFileManageGroupRepository fileManageGroupRepository)
        {
            _fileManageGroupRepository = fileManageGroupRepository;
        }
        public void Delete(FileManageGroup fileManageGroup)
        {
            _fileManageGroupRepository.Delete(fileManageGroup);
        }

        public async Task<bool> ExistsAsync(Expression<Func<FileManageGroup, bool>> where = null, Func<IQueryable<FileManageGroup>, IIncludableQueryable<FileManageGroup, object>> include = null)
        {
            return await _fileManageGroupRepository.ExistsAsync(where, include);
        }

        public async Task<IEnumerable<FileManageGroup>> GetAllAsyn(Expression<Func<FileManageGroup, bool>> where = null, Func<IQueryable<FileManageGroup>, IOrderedQueryable<FileManageGroup>> orderBy = null, Func<IQueryable<FileManageGroup>, IIncludableQueryable<FileManageGroup, object>> include = null, bool tracking = false)
        {
            return await _fileManageGroupRepository.GetAllAsyn(where, orderBy, include);
        }

        public async Task<FileManageGroup> GetAsync(Expression<Func<FileManageGroup, bool>> where = null, Func<IQueryable<FileManageGroup>, IOrderedQueryable<FileManageGroup>> orderBy = null, Func<IQueryable<FileManageGroup>, IIncludableQueryable<FileManageGroup, object>> include = null, bool tracking = false)
        {
            return await _fileManageGroupRepository.GetAsync(where, orderBy, include);
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<FileManageGroup, TResult>> selector = null, Expression<Func<FileManageGroup, bool>> where = null, Func<IQueryable<FileManageGroup>, IOrderedQueryable<FileManageGroup>> orderBy = null, Func<IQueryable<FileManageGroup>, IIncludableQueryable<FileManageGroup, object>> include = null, bool disableTracking = true)
        {
            return await _fileManageGroupRepository.GetWithIncludeAsync(selector, where, orderBy, include);
        }

        public async Task Insert(FileManageGroup fileManageGroup)
        {
            await _fileManageGroupRepository.Insert(fileManageGroup);
        }

        public async Task Save()
        {
            await _fileManageGroupRepository.Save();
        }

        public void Update(FileManageGroup fileManageGroup)
        {
            _fileManageGroupRepository.Update(fileManageGroup);
        }
    }
}
