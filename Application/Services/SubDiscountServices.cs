﻿using Application.Interfaces;

using Domain.DomainClasses.Product;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class SubDiscountServices : ISubDiscountServices
    {
        private ISubDiscountRepository _subDiscountRepository;
        public SubDiscountServices(ISubDiscountRepository subDiscountRepository)
        {
            _subDiscountRepository = subDiscountRepository;
        }
        public void Delete(SubDiscount subDiscount)
        {
            _subDiscountRepository.Delete(subDiscount);
        }

        public void DeleteAll(IEnumerable<SubDiscount> subDiscounts)
        {
            _subDiscountRepository.DeleteAll(subDiscounts);
        }

        public async Task<bool> ExistsAsync(Expression<Func<SubDiscount, bool>> where = null, Func<IQueryable<SubDiscount>, IIncludableQueryable<SubDiscount, object>> include = null)
        {
            return await _subDiscountRepository.ExistsAsync(where, include);
        }

        public async Task<IEnumerable<SubDiscount>> GetAllAsyn(Expression<Func<SubDiscount, bool>> where = null, Func<IQueryable<SubDiscount>, IOrderedQueryable<SubDiscount>> orderBy = null, Func<IQueryable<SubDiscount>, IIncludableQueryable<SubDiscount, object>> include = null, bool tracking = false)
        {
            return await _subDiscountRepository.GetAllAsyn(where, orderBy, include);
        }

        public async Task<SubDiscount> GetAsync(Expression<Func<SubDiscount, bool>> where = null, Func<IQueryable<SubDiscount>, IOrderedQueryable<SubDiscount>> orderBy = null, Func<IQueryable<SubDiscount>, IIncludableQueryable<SubDiscount, object>> include = null, bool tracking = false)
        {
            return await _subDiscountRepository.GetAsync(where, orderBy, include);
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<SubDiscount, TResult>> selector = null, Expression<Func<SubDiscount, bool>> where = null, Func<IQueryable<SubDiscount>, IOrderedQueryable<SubDiscount>> orderBy = null, Func<IQueryable<SubDiscount>, IIncludableQueryable<SubDiscount, object>> include = null, bool disableTracking = true)
        {
            return await _subDiscountRepository.GetWithIncludeAsync(selector, where, orderBy, include);
        }

        public async Task Insert(SubDiscount subDiscount)
        {
            await _subDiscountRepository.Insert(subDiscount);
        }

        public async Task Save()
        {
            await _subDiscountRepository.Save();
        }

        public async Task Update(SubDiscount subDiscount)
        {
            await _subDiscountRepository.Update(subDiscount);
        }
    }
}
