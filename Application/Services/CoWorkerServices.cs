﻿using Application.Interfaces;

using Domain.DomainClasses.CoWorker;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class CoWorkerServices : ICoWorkerServices
    {
        private ICoWorkerRepository _coWorkerRepository;
        public CoWorkerServices(ICoWorkerRepository coWorkerRepository)
        {
            _coWorkerRepository = coWorkerRepository;
        }
        public void DeleteAsync(CoWorker coWorker)
        {
            _coWorkerRepository.DeleteAsync(coWorker);
        }

        public async Task<bool> ExistsAsync(Expression<Func<CoWorker, bool>> where = null, Func<IQueryable<CoWorker>, IIncludableQueryable<CoWorker, object>> include = null)
        {
            return await _coWorkerRepository.ExistsAsync(where, include);
        }

        public async Task<IEnumerable<CoWorker>> GetAllAsyn(Expression<Func<CoWorker, bool>> where = null, Func<IQueryable<CoWorker>, IOrderedQueryable<CoWorker>> orderBy = null, Func<IQueryable<CoWorker>, IIncludableQueryable<CoWorker, object>> include = null, bool tracking = false)
        {
            return await _coWorkerRepository.GetAllAsyn(where, orderBy, include);
        }

        public async Task<CoWorker> GetAsync(Expression<Func<CoWorker, bool>> where = null, Func<IQueryable<CoWorker>, IOrderedQueryable<CoWorker>> orderBy = null, Func<IQueryable<CoWorker>, IIncludableQueryable<CoWorker, object>> include = null, bool tracking = false)
        {
            return await _coWorkerRepository.GetAsync(where, orderBy, include);
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<CoWorker, TResult>> selector = null, Expression<Func<CoWorker, bool>> where = null, Func<IQueryable<CoWorker>, IOrderedQueryable<CoWorker>> orderBy = null, Func<IQueryable<CoWorker>, IIncludableQueryable<CoWorker, object>> include = null, bool disableTracking = true)
        {
            return await _coWorkerRepository.GetWithIncludeAsync(selector, where, orderBy, include);
        }

        public async Task Insert(CoWorker coWorker)
        {
            await _coWorkerRepository.Insert(coWorker);
        }

        public async Task Save()
        {
            await _coWorkerRepository.Save();
        }

        public async Task Update(CoWorker coWorker)
        {
            await _coWorkerRepository.Update(coWorker);
        }
    }
}
