﻿using Application.Interfaces;

using Domain.DomainClasses.Product;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class SuggestProductServices : ISuggestProductServices
    {
        private ISuggestProductRepository _suggestProductRepository;
        public SuggestProductServices(ISuggestProductRepository suggestProductRepository)
        {
            _suggestProductRepository = suggestProductRepository;
        }
        public void Delete(SuggestProduct suggestProduct)
        {
            _suggestProductRepository.Delete(suggestProduct);
        }

        public void DeleteAll(IEnumerable<SuggestProduct> suggestProducts)
        {
            _suggestProductRepository.DeleteAll(suggestProducts);
        }

        public async Task<bool> ExistsAsync(Expression<Func<SuggestProduct, bool>> where = null, Func<IQueryable<SuggestProduct>, IIncludableQueryable<SuggestProduct, object>> include = null)
        {
            return await _suggestProductRepository.ExistsAsync(where, include);
        }

        public async Task<IEnumerable<SuggestProduct>> GetAllAsyn(Expression<Func<SuggestProduct, bool>> where = null, Func<IQueryable<SuggestProduct>, IOrderedQueryable<SuggestProduct>> orderBy = null, Func<IQueryable<SuggestProduct>, IIncludableQueryable<SuggestProduct, object>> include = null, bool tracking = false)
        {
            return await _suggestProductRepository.GetAllAsyn(where, orderBy, include);
        }

        public async Task<SuggestProduct> GetAsync(Expression<Func<SuggestProduct, bool>> where = null, Func<IQueryable<SuggestProduct>, IOrderedQueryable<SuggestProduct>> orderBy = null, Func<IQueryable<SuggestProduct>, IIncludableQueryable<SuggestProduct, object>> include = null, bool tracking = false)
        {
            return await _suggestProductRepository.GetAsync(where, orderBy, include);
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<SuggestProduct, TResult>> selector = null, Expression<Func<SuggestProduct, bool>> where = null, Func<IQueryable<SuggestProduct>, IOrderedQueryable<SuggestProduct>> orderBy = null, Func<IQueryable<SuggestProduct>, IIncludableQueryable<SuggestProduct, object>> include = null, bool disableTracking = true)
        {
            return await _suggestProductRepository.GetWithIncludeAsync(selector, where, orderBy, include);
        }

        public async Task Insert(SuggestProduct suggestProduct)
        {
            await _suggestProductRepository.Insert(suggestProduct);
        }

        public async Task Save()
        {
            await _suggestProductRepository.Save();
        }

        public async Task Update(SuggestProduct suggestProduct)
        {
            await _suggestProductRepository.Update(suggestProduct);
        }
    }
}
