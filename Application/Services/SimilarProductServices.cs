﻿using Application.Interfaces;

using Domain.DomainClasses.Product;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class SimilarProductServices : ISimilarProductServices
    {
        private ISimilarProductRepository _similarProductRepository;
        public SimilarProductServices(ISimilarProductRepository similarProductRepository)
        {
            _similarProductRepository = similarProductRepository;
        }
        public void Delete(SimilarProduct similarProduct)
        {
            _similarProductRepository.Delete(similarProduct);
        }

        public void DeleteAll(IEnumerable<SimilarProduct> similarProducts)
        {
            _similarProductRepository.DeleteAll(similarProducts);
        }

        public async Task<bool> ExistsAsync(Expression<Func<SimilarProduct, bool>> where = null, Func<IQueryable<SimilarProduct>, IIncludableQueryable<SimilarProduct, object>> include = null)
        {
            return await _similarProductRepository.ExistsAsync(where, include);
        }

        public async Task<IEnumerable<SimilarProduct>> GetAllAsyn(Expression<Func<SimilarProduct, bool>> where = null, Func<IQueryable<SimilarProduct>, IOrderedQueryable<SimilarProduct>> orderBy = null, Func<IQueryable<SimilarProduct>, IIncludableQueryable<SimilarProduct, object>> include = null, bool tracking = false)
        {
            return await _similarProductRepository.GetAllAsyn(where, orderBy, include);
        }

        public async Task<SimilarProduct> GetAsync(Expression<Func<SimilarProduct, bool>> where = null, Func<IQueryable<SimilarProduct>, IOrderedQueryable<SimilarProduct>> orderBy = null, Func<IQueryable<SimilarProduct>, IIncludableQueryable<SimilarProduct, object>> include = null, bool tracking = false)
        {
            return await _similarProductRepository.GetAsync(where, orderBy, include);
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<SimilarProduct, TResult>> selector = null, Expression<Func<SimilarProduct, bool>> where = null, Func<IQueryable<SimilarProduct>, IOrderedQueryable<SimilarProduct>> orderBy = null, Func<IQueryable<SimilarProduct>, IIncludableQueryable<SimilarProduct, object>> include = null, bool disableTracking = true)
        {
            return await _similarProductRepository.GetWithIncludeAsync(selector, where, orderBy, include);
        }

        public async Task Insert(SimilarProduct similarProduct)
        {
            await _similarProductRepository.Insert(similarProduct);
        }

        public async Task Save()
        {
            await _similarProductRepository.Save();
        }

        public async Task Update(SimilarProduct similarProduct)
        {
            await _similarProductRepository.Update(similarProduct);
        }
    }
}
