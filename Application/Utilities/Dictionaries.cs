﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Utilities
{
    public static class Dictionaries
    {
        public static Dictionary<int, string> SendType_Dic=new Dictionary<int, string>();
        public static Dictionary<int,string> GetSendTypeDic()
        {
            SendType_Dic.Clear();
            SendType_Dic.Add(1, "ماشینی");
            SendType_Dic.Add(2, "موتوری");
            SendType_Dic.Add(3, "حضوری");
            SendType_Dic.Add(4, "پیشتاز");
            return SendType_Dic;
        }
    }
}
