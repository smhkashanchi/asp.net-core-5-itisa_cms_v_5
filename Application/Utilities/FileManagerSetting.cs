﻿using GleamTech.AspNet.UI;
using GleamTech.FileUltimate.AspNet.UI;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Utilities
{
    public static class FileManagerSetting
    {
        public static FileManager GetFileManagerModel()
        {

            var fileManager1 = new FileManager
            {
                Id = "fileManager1",
                Width = 800,
                Height = 400,
                Resizable = true,
                DisplayLanguage = "fa",
                Hidden = true,
                CollapseRibbon = true,
                DisplayMode = DisplayMode.Window,
                WindowOptions =
                {
                    Title = "Choose a file",
                    Modal = true
                },
                ClientEvents = new FileManagerClientEvents
                {
                    Chosen = "fileManagerChosen"
                },
                Chooser = true,
                ChooserMultipleSelection = true
            };
            fileManager1.RootFolders.Add(new FileManagerRootFolder
            {
                Name = "Files",
                Location = "~/Files"
            });
            fileManager1.RootFolders[0].AccessControls.Add(new FileManagerAccessControl
            {
                Path = @"\",
                AllowedPermissions = FileManagerPermissions.Full
            });

            return fileManager1;

        }
    }
}
