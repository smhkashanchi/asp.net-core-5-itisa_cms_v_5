﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Utilities
{
    public static class CulturesInfo
    {
        public static List<string> LanguageList=new ();
        
        public static List<string> GetList()
        {
            LanguageList.Add("fa-IR");
            LanguageList.Add("en-US");
            return LanguageList;
        }
        
           
    }
}
