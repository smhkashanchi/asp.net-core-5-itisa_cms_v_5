﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Utilities
{
    public static class FileManipulate
    {
        public enum FileSize{Medium,Thumb};

        public static void CreateDirectory(string path="", string webRootPath="", string thumbPath="",string mediumPath="")
        {
            string filePath = System.IO.Path.Combine(webRootPath, path);
            string thumbfilePath = System.IO.Path.Combine(webRootPath, thumbPath);
            string mediumfilePath = System.IO.Path.Combine(webRootPath, mediumPath);
            if (!System.IO.Directory.Exists(filePath))
            {
                System.IO.Directory.CreateDirectory(filePath);
            }
            if (thumbPath != "")
            {
                if (!System.IO.Directory.Exists(thumbfilePath))
                {
                    System.IO.Directory.CreateDirectory(thumbfilePath);
                }
            }
            if (mediumPath != "")
            {
                if (!System.IO.Directory.Exists(mediumfilePath))
                {
                    System.IO.Directory.CreateDirectory(mediumfilePath);
                }
            }
        }
        public static async Task<Tuple<string,string>> CreateFile(string webRootPath, IFormFile file,string path)
        {
            try
            {
                string FileNameCustom = file.FileName.Substring(0, file.FileName.IndexOf(".")) + "_" + Guid.NewGuid().ToString() + System.IO.Path.GetExtension(file.FileName);
                string filePath = System.IO.Path.Combine(webRootPath, path,FileNameCustom);

                using (var stream = new FileStream(filePath, FileMode.Create))
                {
                    await file.CopyToAsync(stream);
                }
                Tuple<string, string> tp = Tuple.Create(path, FileNameCustom);
                return tp;
            }
            catch (Exception err)
            {
                Tuple<string, string> tp = Tuple.Create(path, err.Message);
                return tp;
            }
          
        }
        public static Tuple<string, string,string> Combine(string webRootPath,string fileName,string filePath,string thumbFilePath = "",string mediumFilePath="")
        {
            string thumbFilePathCombined = "";
            string mediumFilePathCombined = "";
            string filePathCombined=System.IO.Path.Combine(webRootPath, filePath, fileName);
            if (!string.IsNullOrEmpty(thumbFilePath))
            {
                thumbFilePathCombined=System.IO.Path.Combine(webRootPath, thumbFilePath, fileName);
            }
            if (!string.IsNullOrEmpty(mediumFilePath))
            {
                mediumFilePathCombined = System.IO.Path.Combine(webRootPath, mediumFilePath, fileName);
            }
            Tuple<string, string,string> tp = Tuple.Create(filePathCombined, thumbFilePathCombined,mediumFilePathCombined);
            return tp;
        }
        public static async Task<Tuple<string,long>> CreateMultiFile(List<IFormFile> Files,string webRootPath,string filePath)
        {
            string fileName = "";
            string fn = "";
            Random rnd = new Random();
            long size = Files.Sum(f => f.Length);

            string path = Path.Combine(webRootPath, filePath);
            foreach (var formFile in Files)
            {
                if (formFile.Length > 0)
                {
                    fileName = rnd.Next(1, 99999).ToString() + formFile.FileName;
                    using (var stream = new FileStream(Path.Combine(path, fileName), FileMode.Create))
                    {
                        await formFile.CopyToAsync(stream);
                    }
                }
                fn += Path.Combine(filePath,fileName) + "&";
            }
            Tuple<string,long> tp=Tuple.Create(fn, size);
            return tp;
        }
        public static void DeleteFile(string webRootPath="",string filePath="",string fileName="")
        {
            //string filePath =Path.Combine(webRootPath, fileName);

            if (File.Exists(filePath))
            {
                File.Delete(filePath);
            }
        }

        /////// ImageManipulate
        public static async Task<string> CreateImagesWithoutResize(string webRootPath,string filePath, IFormFile uploadedImage)
        {//ساخت عکس همراه با کمباین کردن آدرس آن بدون تغییر سایز
            string FullImagePath = "";
            string targetPath = Path.Combine(webRootPath, filePath);

            FileManipulate.CreateDirectory(targetPath);

            ///For Combine :ImageName and ImageFullPath
            Tuple<string, string> ImagePath = await FileManipulate.CreateFile(webRootPath, uploadedImage, filePath);
            FullImagePath = Path.Combine(filePath, ImagePath.Item2);

            return FullImagePath;
        }
        public static void resizeImage(string inputPath, string outputPath, int size = 150, int quality = 75)
        {
            //const int size = 150;
            //const int quality = 75;

            using (var image = new Bitmap(System.Drawing.Image.FromFile(inputPath)))
            {
                int width, height;
                if (image.Width > image.Height)
                {
                    width = size;
                    height = Convert.ToInt32(image.Height * size / (double)image.Width);
                }
                else
                {
                    width = Convert.ToInt32(image.Width * size / (double)image.Height);
                    height = size;
                }
                var resized = new Bitmap(width, height);
                using (var graphics = Graphics.FromImage(resized))
                {
                    graphics.CompositingQuality = CompositingQuality.HighSpeed;
                    graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    graphics.CompositingMode = CompositingMode.SourceCopy;
                    graphics.DrawImage(image, 0, 0, width, height);
                    using (var output = System.IO.File.Open(outputPath, FileMode.Create))
                    {
                        var qualityParamId = System.Drawing.Imaging.Encoder.Quality;
                        var encoderParameters = new EncoderParameters(1);
                        encoderParameters.Param[0] = new EncoderParameter(qualityParamId, quality);
                        var codec = ImageCodecInfo.GetImageDecoders()
                            .FirstOrDefault(codecc => codecc.FormatID == ImageFormat.Jpeg.Guid);
                        resized.Save(output, codec, encoderParameters);
                        output.Dispose();
                    }
                    graphics.Dispose();
                }
                image.Dispose();
            }
        }
        public static void resizeImage2(string inputPath, string outputPath, int width = 150, int height = 75, int quality = 75)
        {
            //const int size = 150;
            //const int quality = 75;

            var image = new Bitmap(inputPath);

            //int width, height;
            //if (image.Width > image.Height)
            //{
            //    width = size;
            //    height = Convert.ToInt32(image.Height * size / (double)image.Width);
            //}
            //else
            //{
            //    width = Convert.ToInt32(image.Width * size / (double)image.Height);
            //    height = size;
            //}
            var resized = new Bitmap(width, height);
            using (var graphics = Graphics.FromImage(resized))
            {
                graphics.CompositingQuality = CompositingQuality.HighSpeed;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.CompositingMode = CompositingMode.SourceCopy;
                graphics.DrawImage(image, 0, 0, width, height);
                using (var output = System.IO.File.Open(outputPath, FileMode.Create))
                {
                    var qualityParamId = System.Drawing.Imaging.Encoder.Quality;
                    var encoderParameters = new EncoderParameters(1);
                    encoderParameters.Param[0] = new EncoderParameter(qualityParamId, quality);
                    var codec = ImageCodecInfo.GetImageDecoders()
                        .FirstOrDefault(codecc => codecc.FormatID == ImageFormat.Jpeg.Guid);
                    resized.Save(output, codec, encoderParameters);
                    output.Close();
                }
                graphics.Dispose();
            }
            image.Dispose();
        }
        public static void Image_resize(string input_Image_Path, string output_Image_Path, int new_Width)

        {

            //---------------< Image_resize() >---------------
            const long quality = 50L;

            Bitmap source_Bitmap = new Bitmap(input_Image_Path);



            double dblWidth_origial = source_Bitmap.Width;

            double dblHeigth_origial = source_Bitmap.Height;

            double relation_heigth_width = dblHeigth_origial / dblWidth_origial;

            int new_Height = (int)(new_Width * relation_heigth_width);



            //< create Empty Drawarea >

            var new_DrawArea = new Bitmap(new_Width, new_Height);

            //</ create Empty Drawarea >



            using (var graphic_of_DrawArea = Graphics.FromImage(new_DrawArea))

            {

                //< setup >

                graphic_of_DrawArea.CompositingQuality = CompositingQuality.HighSpeed;

                graphic_of_DrawArea.InterpolationMode = InterpolationMode.HighQualityBicubic;

                graphic_of_DrawArea.CompositingMode = CompositingMode.SourceCopy;

                //</ setup >



                //< draw into placeholder >

                //*imports the image into the drawarea

                graphic_of_DrawArea.DrawImage(source_Bitmap, 0, 0, new_Width, new_Height);

                //</ draw into placeholder >



                //--< Output as .Jpg >--

                using (var output = System.IO.File.Open(output_Image_Path, FileMode.Create))

                {

                    //< setup jpg >

                    var qualityParamId = System.Drawing.Imaging.Encoder.Quality;

                    var encoderParameters = new EncoderParameters(1);

                    encoderParameters.Param[0] = new EncoderParameter(qualityParamId, quality);

                    //</ setup jpg >



                    //< save Bitmap as Jpg >

                    var codec = ImageCodecInfo.GetImageDecoders().FirstOrDefault(c => c.FormatID == ImageFormat.Jpeg.Guid);

                    new_DrawArea.Save(output, codec, encoderParameters);

                    //resized_Bitmap.Dispose();

                    output.Close();

                    //</ save Bitmap as Jpg >

                }

                //--</ Output as .Jpg >--

                graphic_of_DrawArea.Dispose();

            }

            source_Bitmap.Dispose();
        }
        public static Tuple<string,string,string> CreateNewFileNameWithSizeInfo(string filePath,int thumbWidth=0,int thumbHeight=0, int mediumWidth = 0, int mediumHeight = 0)
        { ///ساخت اسم فایل جدید برای تصاویر کوچک و متوسط با اطلاعات اندازه عکس که از گروه گرفته میشود
            string directotyNam = Path.GetDirectoryName(filePath);
            string currentFileName = Path.GetFileName(filePath);
            string thumbNewFileName= thumbWidth.ToString() + "_" + thumbHeight.ToString() + "_" + currentFileName;
            string mediumNewFileName= mediumWidth.ToString() + "_" + mediumHeight.ToString() + "_" + currentFileName;

            string orginalFilePath = Path.Combine(directotyNam, currentFileName);
            string thumbFilePath = Path.Combine(directotyNam,FileSize.Thumb.ToString(), thumbNewFileName);
            string mediumFilePath = Path.Combine(directotyNam, FileSize.Medium.ToString(), mediumNewFileName);

            return Tuple.Create(orginalFilePath, mediumFilePath, thumbFilePath);
        }

    }
}
