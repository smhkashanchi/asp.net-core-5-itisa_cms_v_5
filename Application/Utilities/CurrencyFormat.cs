﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace Application.Utilities
{
    public static class CurrencyFormat
    {
        public static string  PersianCurrency(this decimal? price)
        {
            var cultureInfo = new CultureInfo("fa-Ir");
            cultureInfo.NumberFormat.CurrencyPositivePattern = 3;
            cultureInfo.NumberFormat.CurrencyNegativePattern = 3;

            var result = string.Format(cultureInfo, "{0:C0}", price);
            return result;
        }
        public static string PersianCurrency(this decimal price)
        {
            var cultureInfo = new CultureInfo("fa-Ir");
            cultureInfo.NumberFormat.CurrencyPositivePattern = 3;
            cultureInfo.NumberFormat.CurrencyNegativePattern = 3;

            var result = string.Format(cultureInfo, "{0:C0}", price);
            return result;
        }
    }
}
