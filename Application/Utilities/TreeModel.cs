﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Application.Utilities
{
	public class TreeModel
	{
		public IEnumerable<Node> TreeNodes { get; set; }
	}

	public class Node
	{
		public string text { get; set; }
		public string href { get; set; }
        public string tags { get; set; }
                                           //public eNodeType NodeType { get; set; }
        public IEnumerable<Node> nodes { get; set; }
	}
    public class ChildNode
    {
        public string text { get; set; }
        public string href { get; set; }
        public string tags { get; set; }
    }

	public enum eNodeType
	{
		Root,
		Folder,
		File
	}

}