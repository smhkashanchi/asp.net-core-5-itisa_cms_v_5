﻿using Application.Utilities;

using Domain.DomainClasses.Seo;

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;

namespace Utilities
{
    public static class MyExtensions
    {
        public static string GenerateRandomSlug()
        {
            while (true)
            {
                Random random = new Random();

                const string chars = "AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqReSsTtUuVvWwXxYyZz0123456789";
                var slug = new string(Enumerable.Repeat(chars, 6)
                  .Select(s => s[random.Next(s.Length)]).ToArray());

                return slug;
            }
        }

        public static string GetDateByLang(this DateTime date)
        {
            return CultureInfo.CurrentCulture.Name == "en" ? date.ToLongDateString() : date.ToShamsi();
        }
        public static string ChangeSiteMapLocation(this MainSiteMap siteMap)
        {
            //iran_carpet_1
            var spilit = siteMap.Location.Split("_");

            var new_loc = (Convert.ToInt32(spilit.LastOrDefault()) + 1).ToString(); // 3

            Array.Resize(ref spilit, spilit.Length - 1); // remove last child

            var text = string.Join("_", spilit); // => iran_carpet

            return $"{text}_{new_loc}";


        }

        public static string ToToman(this int val)
        {
            if (CultureInfo.CurrentCulture.Name == "fa")
                return val.ToString("#,0 تومان");
            else if (CultureInfo.CurrentCulture.Name == "en")
                return val.ToString("#,0 $");
            else
                return val.ToString("#,0 الریال");


        }
        public static string ToCurrency(this int val)
        {
            return val.ToString("#,0");
        }
        public static string ToCurrency(this object val)
        {
            var price = Convert.ToInt32(val);
            return (price).ToString("#,0");
        }
        public static string ToToman(this string val)
        {
            var price = Convert.ToInt32(val);

            if (CultureInfo.CurrentCulture.Name == "fa")
                return price.ToString("#,0 تومان");
            else if (CultureInfo.CurrentCulture.Name == "en")
                return price.ToString("#,0 toman");
            else
                return price.ToString("#,0 التومان");
        }
        public static string ToRial(this string val)
        {
            var price = Convert.ToUInt32(val) * 10;
            return price.ToString("#,0 ریال");
        }
        public static string ToShortText(this string text)
        {
            if (text == null) return "";
            else if (text.Length > 50) return text.Substring(0, 50) + " ...";
            else return text;
        }
        public static string PersianToEnglish(this string persianStr)
        {
            Dictionary<char, char> LettersDictionary = new Dictionary<char, char>
            {
                ['۰'] = '0',
                ['۱'] = '1',
                ['۲'] = '2',
                ['۳'] = '3',
                ['۴'] = '4',
                ['۵'] = '5',
                ['۶'] = '6',
                ['۷'] = '7',
                ['۸'] = '8',
                ['۹'] = '9'
            };

            foreach (var item in persianStr)
            {
                if (LettersDictionary.ContainsKey(item))
                    persianStr = persianStr.Replace(item, LettersDictionary[item]);
            }
            return persianStr;
        }
        public static string ToSlug(this string Value)
        {
            Regex pattern = new Regex("[*/+() ]");
            Value = pattern.Replace(Value, "-");
            return Value;
        }
        public static string ToTitle(this string Value)
        {
            return Value.Replace("-", " ");
        }
        public static string ToMB(this long Value)
        {
            long temp = Value % (1024 * 1024 * 1024);
            long MBs = temp / (1024 * 1024);
            return MBs.ToString() + "MB";
        }
        public static string GenerateDiscountCode(string keyStart,string type, int length=4)
        {
            Random random = new Random();
             string mix_chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
             string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
             string number = "0123456789";
             string typeRandom = "";
            if (type == "3")
                typeRandom = mix_chars;
            else if (type == "2")
                typeRandom = number;
            else if (type == "1")
                typeRandom = chars;

           string generated_code = new string(Enumerable.Repeat(typeRandom, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
            return keyStart + "_" + generated_code;
        }

    }
}
