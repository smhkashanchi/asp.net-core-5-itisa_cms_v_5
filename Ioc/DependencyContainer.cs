﻿using Application.Interfaces;
using Application.Services;

using Data.Repository;

using Domain.Interfaces;

using Microsoft.Extensions.DependencyInjection;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ioc
{
    public class DependencyContainer
    {
        public static void RegisterServices(IServiceCollection services)
        {
            services.AddScoped<ILanguageRepository, LanguageRepository>();
            services.AddScoped<IColorRepository, ColorRepository>();
            services.AddScoped<IColorTranslationRepository, ColorTranslationRepository>();
            services.AddScoped<IConnectionGroupRepository, ConnectionGroupRepository>();
            services.AddScoped<IConnectionRepository, ConnectionRepository>();
            services.AddScoped<IConnectionTranslationRepository,ConnectionTranslationRepository>();
            services.AddScoped<IContentGroupRepository,ContentGroupRepository>();
            services.AddScoped<IContentGroupTranslationRepository,ContentGroupTranslationRepository>();
            services.AddScoped<IMainSiteMapRepository,MainSiteMapRepository>();
            services.AddScoped<IContentRepository,ContentRepository>();
            services.AddScoped<IContentTranslationRepository,ContentTranslationRepository>();
            services.AddScoped<IMiddleContentGroupRepository,MiddleContentGroupRepository>();
            services.AddScoped<IGalleryGroupRepository,GalleryGroupRepository>();
            services.AddScoped<IGalleryGroupTranslationRepository,GalleryGroupTranslationRepository>();
            services.AddScoped<IGalleryRepository,GalleryRepository>();
            services.AddScoped<IGalleryTranslationRepository,GalleryTranslationRepository>();
            services.AddScoped<IGalleryPictureRepository,GalleryPictureRepository>();
            services.AddScoped<ISocialNetworkRepository,SocialNetworkRepository>();
            services.AddScoped<ISocialNetworkTranslationRepository,SocialNetworkTranslationRepository>();
            services.AddScoped<ISlideShowGroupRepository,SlideShowGroupRepository>();
            services.AddScoped<ISlideShowRepository,SlideShowRepository>();
            services.AddScoped<ISlideShowTranslationRepository,SlideShowTranslationRepository>();
            services.AddScoped<ISlideShowGroupTranslationRepository, SlideShowGroupTranslationRepository>();
            services.AddScoped<ISeoPageRepository,SeoPageRepository>();
            services.AddScoped<ISendTypeRepository,SendTypeRepository>();
            services.AddScoped<ISendTypeTranslationRepository,SendTypeTranslationRepository>();
            services.AddScoped<ICoWorkerGroupRepository,CoWorkerGroupRepository>();
            services.AddScoped<ICoWorkerGroupTranslationRepository,CoWorkerGroupTranslationRepository>();
            services.AddScoped<ICoWorkerRepository,CoWorkerRepository>();
            services.AddScoped<ICoWorkerTranslationRepository,CoWorkerTranslationRepository>();
            services.AddScoped<IFileManageGroupRepository,FileManageGroupRepository>();
            services.AddScoped<IFileManageGroupTranslationRepository,FileManageGroupTranslationRepository>();
            services.AddScoped<IFileManageRepository,FileManageRepository>();
            services.AddScoped<IFileManageTranslationRepository,FileManageTranslationRepository>();
            services.AddScoped<ICountryRepository,CountryRepository>();
            services.AddScoped<IStateRepository,StateRepository>();
            services.AddScoped<ICityRepository,CityRepository>();
            services.AddScoped<IDiscountRepository,DiscountRepository>();
            services.AddScoped<ISubDiscountRepository,SubDiscountRepository>();
            services.AddScoped<IFAQRepository,FAQRepository>();
            services.AddScoped<IProductGroupRepository,ProductGroupRepository>();
            services.AddScoped<IProductGroupTranslaionRepository,ProductGroupTranslationRepository>();
            services.AddScoped<IProductRepository,ProductRepository>();
            services.AddScoped<IProductTranslationRepository,ProductTranslationRepository>();
            services.AddScoped<IProductMiddleGroupRepository,ProductMiddleGroupRepository>();
            services.AddScoped<IFeatureGroupRepository,FeatureGroupRepository>();
            services.AddScoped<IFeatureGroupTranslationRepository,FeatureGroupTranslationRepository>();
            services.AddScoped<IFeatureRepository,FeatureRepository>();
            services.AddScoped<IFeatureTranslationRepository,FeatureTranslationRepository>();
            services.AddScoped<IFeatureReplyRepository,FeatureReplyRepository>();
            services.AddScoped<IFeatureReplyTranslationRepository,FeatureReplyTranslationRepository>();
            services.AddScoped<IProductGroupFeatureRepository, ProductGroupFeatureRepository>();
            services.AddScoped<IProductFeatureRepository, ProductFeatureRepository>();
            services.AddScoped<ISimilarProductRepository, SimilarProductRepository>();
            services.AddScoped<ISuggestProductRepository, SuggestProductRepository>();
            



            services.AddScoped<ILanguageServices, LanguageServices>();
            services.AddScoped<IColorServices, ColorServices>();
            services.AddScoped<IColorTranslationServices, ColorTranslationServices>();
            services.AddScoped<IConnectionGroupServices, ConnectionGroupServices>();
            services.AddScoped<IConnectionServices, ConnectionServices>();
            services.AddScoped<IConnectionTranslationServices, ConnectionTranslationServices>();
            services.AddScoped<IContentGroupServices, ContentGroupServices>();
            services.AddScoped<IContentGroupTranslationSrevices,ContentGroupTranslationSrevices>();
            services.AddScoped<IMainSiteMapServices,MainSiteMapServices>();
            services.AddScoped<IContentServices,ContentServices>();
            services.AddScoped<IContentTranslationServices,ContentTranslationServices>();
            services.AddScoped<IMiddleContentGroupServices,MiddleContentGroupServices>();
            services.AddScoped<IGalleryGroupServices,GalleryGroupServices>();
            services.AddScoped<IGalleryGroupTranslationServices,GalleryGroupTranslationServices>();
            services.AddScoped<IGalleryServices,GalleryServices>();
            services.AddScoped<IGalleryTranslationServices,GalleryTranslationServices>();
            services.AddScoped<IGalleryPictureServices,GalleryPictureServices>();
            services.AddScoped<ISocialNetworkServices,SocialNetworkServices>();
            services.AddScoped<ISocialNetworkTranslationServices,SocialNetworkTranslationServices>();
            services.AddScoped<ISlideShowGroupServices,SlideShowGroupServices>();
            services.AddScoped<ISlideShowServices,SlideShowServices>();
            services.AddScoped<ISlideShowTranslationServices,SlideShowTranslationServices>();
            services.AddScoped<ISlideShowGroupTranslationServices, SlideShowGroupTranslationServices>();
            services.AddScoped<ISeoPageServices,SeoPageServices>();
            services.AddScoped<ISendTypeServices,SendTypeServices>();
            services.AddScoped<ISendTypeTranslationServices,SendTypeTranslationServices>();
            services.AddScoped<ICoWorkerGroupServices,CoWorkerGroupServices>();
            services.AddScoped<ICoWorkerGroupTranslationServices,CoWorkerGroupTranslationServices>();
            services.AddScoped<ICoWorkerServices,CoWorkerServices>();
            services.AddScoped<ICoWorkerTranslationServices,CoWorkerTranslationServices>();
            services.AddScoped<IFileManageGroupServices,FileManageGroupServices>();
            services.AddScoped<IFileManageGroupTranslationServices,FileManageGroupTranslationServices>();
            services.AddScoped<IFileManageServices,FileManageServices>();
            services.AddScoped<IFileManageTranslationServices,FileManageTranslationServices>();
            services.AddScoped<ICountryServices,CountryServices>();
            services.AddScoped<IStateServices,StateServices>();
            services.AddScoped<ICityServices,CityServices>();
            services.AddScoped<IDiscountServices,DiscountServices>();
            services.AddScoped<ISubDiscountServices,SubDiscountServices>();
            services.AddScoped<IFAQServices,FAQServices>();
            services.AddScoped<IProductGroupServices,ProductGroupServices>();
            services.AddScoped<IProductGroupTranslationServices,ProductGroupTranslationServices>();
            services.AddScoped<IProductServices,ProductServices>();
            services.AddScoped<IProductTranslationServices,ProductTranslationServices>();
            services.AddScoped<IProductMiddleGroupServices,ProductMiddleGroupServices>();
            services.AddScoped<IFeatureGroupServices, FeatureGroupServices>();
            services.AddScoped<IFeatureGroupTranslationServices, FeatureGroupTranslationServices>();
            services.AddScoped<IFeatureServices, FeatureServices>();
            services.AddScoped<IFeatureTranslationServices, FeatureTranslationServices>();
            services.AddScoped<IFeatureReplyServices, FeatureReplyServices>();
            services.AddScoped<IFeatureReplyTranslationServices, FeatureReplyTranslationServices>();
            services.AddScoped<IProductGroupFeatureServices, ProductGroupFeatureServices>();
            services.AddScoped<IProductFeatureServices, ProductFeatureServices>();
            services.AddScoped<ISimilarProductServices, SimilarProductServices>();
            services.AddScoped<ISuggestProductServices, SuggestProductServices>();

            services.AddScoped<ISiteMapService, SiteMapService>();

        }
    }
}
