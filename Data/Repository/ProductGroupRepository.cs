﻿using Data.Context;

using Domain.DomainClasses.Product;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repository
{
    public class ProductGroupRepository : IProductGroupRepository
    {
        private ApplicationDbContext _db;
        public ProductGroupRepository(ApplicationDbContext db)
        {
            _db = db;
        }
        public void Delete(ProductGroup productGroup)
        {
            _db.ProductGroup_tb.Remove(productGroup);
        }

        public async Task<bool> ExistsAsync(Expression<Func<ProductGroup, bool>> where = null, Func<IQueryable<ProductGroup>, IIncludableQueryable<ProductGroup, object>> include = null)
        {
            IQueryable<ProductGroup> query = _db.Set<ProductGroup>();

            if (include != null)
            {
                query = include(query);
            }

            query = query.Where(where);

            if (await query.CountAsync() > 0)
                return true;
            else
                return false;
        }

        public async Task<IEnumerable<ProductGroup>> GetAllAsyn(Expression<Func<ProductGroup, bool>> where = null, Func<IQueryable<ProductGroup>, IOrderedQueryable<ProductGroup>> orderBy = null, Func<IQueryable<ProductGroup>, IIncludableQueryable<ProductGroup, object>> include = null, bool tracking = false)
        {
            IQueryable<ProductGroup> query = _db.Set<ProductGroup>();

            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }
            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().ToListAsync();
                else
                    return await orderBy(query).ToListAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().ToListAsync();
                else
                    return await query.ToListAsync();
            }
        }

        public async Task<ProductGroup> GetAsync(Expression<Func<ProductGroup, bool>> where = null, Func<IQueryable<ProductGroup>, IOrderedQueryable<ProductGroup>> orderBy = null, Func<IQueryable<ProductGroup>, IIncludableQueryable<ProductGroup, object>> include = null, bool tracking = false)
        {
            IQueryable<ProductGroup> query = _db.Set<ProductGroup>();
            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }


            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().FirstOrDefaultAsync();
                else
                    return await orderBy(query).FirstOrDefaultAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().FirstOrDefaultAsync();
                else
                    return await query.FirstOrDefaultAsync();
            }
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<ProductGroup, TResult>> selector = null, Expression<Func<ProductGroup, bool>> where = null, Func<IQueryable<ProductGroup>, IOrderedQueryable<ProductGroup>> orderBy = null, Func<IQueryable<ProductGroup>, IIncludableQueryable<ProductGroup, object>> include = null, bool disableTracking = true)
        {
            IQueryable<ProductGroup> query = _db.Set<ProductGroup>();
            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }

            if (orderBy != null)
            {
                return await orderBy(query).Select(selector).ToListAsync();
            }
            else
            {
                return await query.Select(selector).ToListAsync();
            }
        }

        public async Task Insert(ProductGroup productGroup)
        {
            await _db.ProductGroup_tb.AddAsync(productGroup);
        }

        public async Task Save()
        {
            await _db.SaveChangesAsync();
        }

        public void Update(ProductGroup productGroup)
        {
            _db.ProductGroup_tb.Update(productGroup);
        }
    }
}
