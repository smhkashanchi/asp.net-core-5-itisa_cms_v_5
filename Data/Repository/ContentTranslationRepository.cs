﻿using Data.Context;

using Domain.DomainClasses.Content;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repository
{
    public class ContentTranslationRepository : IContentTranslationRepository
    {
        private ApplicationDbContext _db;
        public ContentTranslationRepository(ApplicationDbContext db)
        {
            _db = db;
        }
        public void DeleteAllAsync(IEnumerable<ContentTranslation> contentTranslations)
        {
            _db.ContentTranslation_tb.RemoveRange(contentTranslations);
        }

        public void DeleteAsync(ContentTranslation contentTranslation)
        {
            _db.ContentTranslation_tb.Remove(contentTranslation);
        }

        public async Task Detach(ContentTranslation contentTranslation)
        {
            var local = _db.Set<ContentTranslation>().Local
                .FirstOrDefault(entry => entry.ContentTranslation_ID
                .Equals(contentTranslation.ContentTranslation_ID));

            if (local != null)
            {
                _db.Entry(local).State = EntityState.Detached;
            }
            _db.Entry(contentTranslation).State = EntityState.Modified;
        }

        public async Task<bool> ExistsAsync(Expression<Func<ContentTranslation, bool>> where = null, Func<IQueryable<ContentTranslation>, IIncludableQueryable<ContentTranslation, object>> include = null)
        {
            IQueryable<ContentTranslation> query = _db.Set<ContentTranslation>();

            if (include != null)
            {
                query = include(query);
            }

            query = query.Where(where);

            if (await query.CountAsync() > 0)
                return true;
            else
                return false;
        }

        public async Task<IEnumerable<ContentTranslation>> GetAllAsyn(Expression<Func<ContentTranslation, bool>> where = null, Func<IQueryable<ContentTranslation>, IOrderedQueryable<ContentTranslation>> orderBy = null, Func<IQueryable<ContentTranslation>, IIncludableQueryable<ContentTranslation, object>> include = null, bool tracking = false)
        {
            IQueryable<ContentTranslation> query = _db.Set<ContentTranslation>();

            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }
            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().ToListAsync();
                else
                    return await orderBy(query).ToListAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().ToListAsync();
                else
                    return await query.ToListAsync();
            }
        }

        public async Task<ContentTranslation> GetAsync(Expression<Func<ContentTranslation, bool>> where = null, Func<IQueryable<ContentTranslation>, IOrderedQueryable<ContentTranslation>> orderBy = null, Func<IQueryable<ContentTranslation>, IIncludableQueryable<ContentTranslation, object>> include = null, bool tracking = false)
        {
            IQueryable<ContentTranslation> query = _db.Set<ContentTranslation>();
            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }


            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().FirstOrDefaultAsync();
                else
                    return await orderBy(query).FirstOrDefaultAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().FirstOrDefaultAsync();
                else
                    return await query.FirstOrDefaultAsync();
            }
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<ContentTranslation, TResult>> selector = null, Expression<Func<ContentTranslation, bool>> where = null, Func<IQueryable<ContentTranslation>, IOrderedQueryable<ContentTranslation>> orderBy = null, Func<IQueryable<ContentTranslation>, IIncludableQueryable<ContentTranslation, object>> include = null, bool disableTracking = true)
        {
            IQueryable<ContentTranslation> query = _db.Set<ContentTranslation>();


            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }

            if (orderBy != null)
            {
                return await orderBy(query).Select(selector).ToListAsync();
            }
            else
            {
                return await query.AsNoTracking().Select(selector).ToListAsync();
            }
        }

        public async Task Insert(ContentTranslation contentTranslation)
        {
            await _db.ContentTranslation_tb.AddAsync(contentTranslation);
        }

        public async Task Save()
        {
            await _db.SaveChangesAsync();
        }

        public async Task Update(ContentTranslation contentTranslation)
        {
            await Detach(contentTranslation);
        }
    }
}
