﻿using Data.Context;

using Domain.DomainClasses.Product;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repository
{
    public class SubDiscountRepository : ISubDiscountRepository
    {
        private ApplicationDbContext _db;
        public SubDiscountRepository(ApplicationDbContext db)
        {
            _db = db;
        }
        public void Delete(SubDiscount subDiscount)
        {
            _db.SubDiscount_tb.Remove(subDiscount);
        }

        public void DeleteAll(IEnumerable<SubDiscount> subDiscounts)
        {
            _db.SubDiscount_tb.RemoveRange(subDiscounts);
        }

        public async Task<bool> ExistsAsync(Expression<Func<SubDiscount, bool>> where = null, Func<IQueryable<SubDiscount>, IIncludableQueryable<SubDiscount, object>> include = null)
        {
            IQueryable<SubDiscount> query = _db.Set<SubDiscount>();

            if (include != null)
            {
                query = include(query);
            }

            query = query.Where(where);

            if (await query.CountAsync() > 0)
                return true;
            else
                return false;
        }

        public async Task<IEnumerable<SubDiscount>> GetAllAsyn(Expression<Func<SubDiscount, bool>> where = null, Func<IQueryable<SubDiscount>, IOrderedQueryable<SubDiscount>> orderBy = null, Func<IQueryable<SubDiscount>, IIncludableQueryable<SubDiscount, object>> include = null, bool tracking = false)
        {
            IQueryable<SubDiscount> query = _db.Set<SubDiscount>();

            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }
            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().ToListAsync();
                else
                    return await orderBy(query).ToListAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().ToListAsync();
                else
                    return await query.ToListAsync();
            }
        }

        public async Task<SubDiscount> GetAsync(Expression<Func<SubDiscount, bool>> where = null, Func<IQueryable<SubDiscount>, IOrderedQueryable<SubDiscount>> orderBy = null, Func<IQueryable<SubDiscount>, IIncludableQueryable<SubDiscount, object>> include = null, bool tracking = false)
        {
            IQueryable<SubDiscount> query = _db.Set<SubDiscount>();
            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }


            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().FirstOrDefaultAsync();
                else
                    return await orderBy(query).FirstOrDefaultAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().FirstOrDefaultAsync();
                else
                    return await query.FirstOrDefaultAsync();
            }
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<SubDiscount, TResult>> selector = null, Expression<Func<SubDiscount, bool>> where = null, Func<IQueryable<SubDiscount>, IOrderedQueryable<SubDiscount>> orderBy = null, Func<IQueryable<SubDiscount>, IIncludableQueryable<SubDiscount, object>> include = null, bool disableTracking = true)
        {
            IQueryable<SubDiscount> query = _db.Set<SubDiscount>();


            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }

            if (orderBy != null)
            {
                return await orderBy(query).Select(selector).ToListAsync();
            }
            else
            {
                return await query.AsNoTracking().Select(selector).ToListAsync();
            }
        }

        public async Task Insert(SubDiscount subDiscount)
        {
            await _db.SubDiscount_tb.AddAsync(subDiscount);
        }

        public async Task Save()
        {
            await _db.SaveChangesAsync();
        }

        public async Task Update(SubDiscount subDiscount)
        {
            _db.SubDiscount_tb.Update(subDiscount);
        }
    }
}
