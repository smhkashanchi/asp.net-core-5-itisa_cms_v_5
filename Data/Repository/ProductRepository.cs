﻿using Data.Context;

using Domain.DomainClasses.Product;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repository
{
    public class ProductRepository : IProductRepository
    {
        private ApplicationDbContext _db;
        public ProductRepository(ApplicationDbContext db)
        {
            _db = db;
        }
        public async Task Delete(Product product)
        {
            // _db.Product_tb.Remove(product);
            var local = _db.Set<Product>().Local
               .FirstOrDefault(entry => entry.Product_ID
               .Equals(product.Product_ID));

            if (local != null)
            {
                _db.Product_tb.Attach(local);
            }

            _db.Entry(product).State = EntityState.Deleted;
        }

        public async Task<bool> ExistsAsync(Expression<Func<Product, bool>> where = null, Func<IQueryable<Product>, IIncludableQueryable<Product, object>> include = null)
        {
            IQueryable<Product> query = _db.Set<Product>();

            if (include != null)
            {
                query = include(query);
            }

            query = query.Where(where);

            if (await query.CountAsync() > 0)
                return true;
            else
                return false;
        }

        public async Task<IEnumerable<Product>> GetAllAsyn(Expression<Func<Product, bool>> where = null, Func<IQueryable<Product>, IOrderedQueryable<Product>> orderBy = null, Func<IQueryable<Product>, IIncludableQueryable<Product, object>> include = null, bool tracking = false)
        {
            IQueryable<Product> query = _db.Set<Product>();

            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }
            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().ToListAsync();
                else
                    return await orderBy(query).ToListAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().ToListAsync();
                else
                    return await query.ToListAsync();
            }
        }

        public async Task<Product> GetAsync(Expression<Func<Product, bool>> where = null, Func<IQueryable<Product>, IOrderedQueryable<Product>> orderBy = null, Func<IQueryable<Product>, IIncludableQueryable<Product, object>> include = null, bool tracking = false)
        {
            IQueryable<Product> query = _db.Set<Product>();
            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }


            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().FirstOrDefaultAsync();
                else
                    return await orderBy(query).FirstOrDefaultAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().FirstOrDefaultAsync();
                else
                    return await query.FirstOrDefaultAsync();
            }
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<Product, TResult>> selector = null, Expression<Func<Product, bool>> where = null, Func<IQueryable<Product>, IOrderedQueryable<Product>> orderBy = null, Func<IQueryable<Product>, IIncludableQueryable<Product, object>> include = null, bool disableTracking = true)
        {
            IQueryable<Product> query = _db.Set<Product>();


            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }

            if (orderBy != null)
            {
                return await orderBy(query).Select(selector).ToListAsync();
            }
            else
            {
                return await query.Select(selector).ToListAsync();
            }
        }

        public async Task Insert(Product product)
        {
            await _db.Product_tb.AddAsync(product);
        }

        public async Task Save()
        {
            await _db.SaveChangesAsync();
        }

        public void Update(Product product)
        {
            _db.Product_tb.Update(product);
        }
    }
}
