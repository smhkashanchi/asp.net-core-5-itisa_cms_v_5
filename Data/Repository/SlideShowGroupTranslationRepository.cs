﻿using Data.Context;

using Domain.DomainClasses.SlideShow;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repository
{
    public class SlideShowGroupTranslationRepository : ISlideShowGroupTranslationRepository
    {
        private ApplicationDbContext _db;
        public SlideShowGroupTranslationRepository(ApplicationDbContext db)
        {
            _db = db;
        }
        public void DeleteAsync(SlideShowGroupTranslation slideShowGroupTranslation)
        {
            _db.SlideShowGroupTranslation_tb.Remove(slideShowGroupTranslation);
        }

        public async Task<bool> ExistsAsync(Expression<Func<SlideShowGroupTranslation, bool>> where = null, Func<IQueryable<SlideShowGroupTranslation>, IIncludableQueryable<SlideShowGroupTranslation, object>> include = null)
        {

            IQueryable<SlideShowGroupTranslation> query = _db.Set<SlideShowGroupTranslation>();

            if (include != null)
            {
                query = include(query);
            }

            query = query.Where(where);

            if (await query.CountAsync() > 0)
                return true;
            else
                return false;
        }

        public async Task<IEnumerable<SlideShowGroupTranslation>> GetAllAsyn(Expression<Func<SlideShowGroupTranslation, bool>> where = null, Func<IQueryable<SlideShowGroupTranslation>, IOrderedQueryable<SlideShowGroupTranslation>> orderBy = null, Func<IQueryable<SlideShowGroupTranslation>, IIncludableQueryable<SlideShowGroupTranslation, object>> include = null, bool tracking = false)
        {
            IQueryable<SlideShowGroupTranslation> query = _db.Set<SlideShowGroupTranslation>();

            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }
            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().ToListAsync();
                else
                    return await orderBy(query).ToListAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().ToListAsync();
                else
                    return await query.ToListAsync();
            }
        }

        public async Task<SlideShowGroupTranslation> GetAsync(Expression<Func<SlideShowGroupTranslation, bool>> where = null, Func<IQueryable<SlideShowGroupTranslation>, IOrderedQueryable<SlideShowGroupTranslation>> orderBy = null, Func<IQueryable<SlideShowGroupTranslation>, IIncludableQueryable<SlideShowGroupTranslation, object>> include = null, bool tracking = false)
        {
            IQueryable<SlideShowGroupTranslation> query = _db.Set<SlideShowGroupTranslation>();
            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }


            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().FirstOrDefaultAsync();
                else
                    return await orderBy(query).FirstOrDefaultAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().FirstOrDefaultAsync();
                else
                    return await query.FirstOrDefaultAsync();
            }
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<SlideShowGroupTranslation, TResult>> selector = null, Expression<Func<SlideShowGroupTranslation, bool>> where = null, Func<IQueryable<SlideShowGroupTranslation>, IOrderedQueryable<SlideShowGroupTranslation>> orderBy = null, Func<IQueryable<SlideShowGroupTranslation>, IIncludableQueryable<SlideShowGroupTranslation, object>> include = null, bool disableTracking = true)
        {
            IQueryable<SlideShowGroupTranslation> query = _db.Set<SlideShowGroupTranslation>();


            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }

            if (orderBy != null)
            {
                return await orderBy(query).Select(selector).ToListAsync();
            }
            else
            {
                return await query.Select(selector).ToListAsync();
            }
        }

        public async Task Insert(SlideShowGroupTranslation slideShowGroupTranslation)
        {
            await _db.SlideShowGroupTranslation_tb.AddAsync(slideShowGroupTranslation);
        }

        public async Task Save()
        {
            await _db.SaveChangesAsync();
        }

        public async Task Update(SlideShowGroupTranslation slideShowGroupTranslation)
        {
            _db.SlideShowGroupTranslation_tb.Update(slideShowGroupTranslation);
        }
    }
}
