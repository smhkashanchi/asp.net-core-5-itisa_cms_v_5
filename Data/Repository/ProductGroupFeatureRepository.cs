﻿using Data.Context;

using Domain.DomainClasses.Product;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repository
{
    public class ProductGroupFeatureRepository : IProductGroupFeatureRepository
    {
        private ApplicationDbContext _db;
        public ProductGroupFeatureRepository(ApplicationDbContext db)
        {
            _db = db;
        }
        public void DeleteRange(List<ProductGroupFeature> productGroupFeatures)
        {
            _db.ProductGroupFeature_tb.RemoveRange(productGroupFeatures);
        }

        public async Task<bool> ExistsAsync(Expression<Func<ProductGroupFeature, bool>> where = null, Func<IQueryable<ProductGroupFeature>, IIncludableQueryable<ProductGroupFeature, object>> include = null)
        {
            IQueryable<ProductGroupFeature> query = _db.Set<ProductGroupFeature>();

            if (include != null)
            {
                query = include(query);
            }

            query = query.Where(where);

            if (await query.CountAsync() > 0)
                return true;
            else
                return false;
        }

        public async Task<IEnumerable<ProductGroupFeature>> GetAllAsyn(Expression<Func<ProductGroupFeature, bool>> where = null, Func<IQueryable<ProductGroupFeature>, IOrderedQueryable<ProductGroupFeature>> orderBy = null, Func<IQueryable<ProductGroupFeature>, IIncludableQueryable<ProductGroupFeature, object>> include = null, bool tracking = false)
        {
            IQueryable<ProductGroupFeature> query = _db.Set<ProductGroupFeature>();

            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }
            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().ToListAsync();
                else
                    return await orderBy(query).ToListAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().ToListAsync();
                else
                    return await query.ToListAsync();
            }
        }

        public async Task<ProductGroupFeature> GetAsync(Expression<Func<ProductGroupFeature, bool>> where = null, Func<IQueryable<ProductGroupFeature>, IOrderedQueryable<ProductGroupFeature>> orderBy = null, Func<IQueryable<ProductGroupFeature>, IIncludableQueryable<ProductGroupFeature, object>> include = null, bool tracking = false)
        {

            IQueryable<ProductGroupFeature> query = _db.Set<ProductGroupFeature>();
            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }


            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().FirstOrDefaultAsync();
                else
                    return await orderBy(query).FirstOrDefaultAsync();
            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().FirstOrDefaultAsync();
                else
                    return await query.FirstOrDefaultAsync();
            }
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<ProductGroupFeature, TResult>> selector = null, Expression<Func<ProductGroupFeature, bool>> where = null, Func<IQueryable<ProductGroupFeature>, IOrderedQueryable<ProductGroupFeature>> orderBy = null, Func<IQueryable<ProductGroupFeature>, IIncludableQueryable<ProductGroupFeature, object>> include = null, bool disableTracking = true)
        {
            IQueryable<ProductGroupFeature> query = _db.Set<ProductGroupFeature>();


            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }

            if (orderBy != null)
            {
                return await orderBy(query).Select(selector).ToListAsync();
            }
            else
            {
                return await query.Select(selector).ToListAsync();
            }
        }

        public async Task InsertRange(List<ProductGroupFeature> productGroupFeatures)
        {
            await _db.ProductGroupFeature_tb.AddRangeAsync(productGroupFeatures);
        }

        public async Task Save()
        {
            await _db.SaveChangesAsync();
        }
    }
}
