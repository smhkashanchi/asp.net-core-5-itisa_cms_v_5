﻿using Data.Context;

using Domain.DomainClasses.Gallery;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repository
{
    public class GalleryGroupRepository : IGalleryGroupRepository
    {
        private ApplicationDbContext _db;
        public GalleryGroupRepository(ApplicationDbContext db)
        {
            _db = db;
        }
        public void DeleteAsync(GalleryGroup galleryGroup)
        {
            _db.GalleryGroup_tb.Remove(galleryGroup);
        }

        public async Task<bool> ExistsAsync(Expression<Func<GalleryGroup, bool>> where = null, Func<IQueryable<GalleryGroup>, IIncludableQueryable<GalleryGroup, object>> include = null)
        {
            IQueryable<GalleryGroup> query = _db.Set<GalleryGroup>();

            if (include != null)
            {
                query = include(query);
            }

            query = query.Where(where);

            if (await query.CountAsync() > 0)
                return true;
            else
                return false;
        }

        public async Task<IEnumerable<GalleryGroup>> GetAllAsyn(Expression<Func<GalleryGroup, bool>> where = null, Func<IQueryable<GalleryGroup>, IOrderedQueryable<GalleryGroup>> orderBy = null, Func<IQueryable<GalleryGroup>, IIncludableQueryable<GalleryGroup, object>> include = null, bool tracking = false)
        {
            IQueryable<GalleryGroup> query = _db.Set<GalleryGroup>();

            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }
            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().ToListAsync();
                else
                    return await orderBy(query).ToListAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().ToListAsync();
                else
                    return await query.ToListAsync();
            }
        }

        public async Task<GalleryGroup> GetAsync(Expression<Func<GalleryGroup, bool>> where = null, Func<IQueryable<GalleryGroup>, IOrderedQueryable<GalleryGroup>> orderBy = null, Func<IQueryable<GalleryGroup>, IIncludableQueryable<GalleryGroup, object>> include = null, bool tracking = false)
        {
            IQueryable<GalleryGroup> query = _db.Set<GalleryGroup>();
            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }


            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().FirstOrDefaultAsync();
                else
                    return await orderBy(query).FirstOrDefaultAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().FirstOrDefaultAsync();
                else
                    return await query.FirstOrDefaultAsync();
            }
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<GalleryGroup, TResult>> selector = null, Expression<Func<GalleryGroup, bool>> where = null, Func<IQueryable<GalleryGroup>, IOrderedQueryable<GalleryGroup>> orderBy = null, Func<IQueryable<GalleryGroup>, IIncludableQueryable<GalleryGroup, object>> include = null, bool disableTracking = true)
        {
            IQueryable<GalleryGroup> query = _db.Set<GalleryGroup>();


            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }

            if (orderBy != null)
            {
                return await orderBy(query).Select(selector).ToListAsync();
            }
            else
            {
                return await query.Select(selector).ToListAsync();
            }
        }

        public async Task Insert(GalleryGroup galleryGroup)
        {
            await _db.GalleryGroup_tb.AddAsync(galleryGroup);
        }

        public async Task Save()
        {
            await _db.SaveChangesAsync();
        }

        public async Task Update(GalleryGroup galleryGroup)
        {
            _db.GalleryGroup_tb.Update(galleryGroup);
        }
    }
}
