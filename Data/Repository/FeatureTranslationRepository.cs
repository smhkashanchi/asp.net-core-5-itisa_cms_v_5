﻿using Data.Context;

using Domain.DomainClasses.Feature;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repository
{
    public class FeatureTranslationRepository : IFeatureTranslationRepository
    {
        private ApplicationDbContext _db;
        public FeatureTranslationRepository(ApplicationDbContext db)
        {
            _db = db;
        }
        public void Delete(FeatureTranslation featureTranslation)
        {
            _db.FeatureTranslation_tb.Remove(featureTranslation);
        }

        public async Task<bool> ExistsAsync(Expression<Func<FeatureTranslation, bool>> where = null, Func<IQueryable<FeatureTranslation>, IIncludableQueryable<FeatureTranslation, object>> include = null)
        {
            IQueryable<FeatureTranslation> query = _db.Set<FeatureTranslation>();

            if (include != null)
            {
                query = include(query);
            }

            query = query.Where(where);

            if (await query.CountAsync() > 0)
                return true;
            else
                return false;
        }

        public async Task<IEnumerable<FeatureTranslation>> GetAllAsyn(Expression<Func<FeatureTranslation, bool>> where = null, Func<IQueryable<FeatureTranslation>, IOrderedQueryable<FeatureTranslation>> orderBy = null, Func<IQueryable<FeatureTranslation>, IIncludableQueryable<FeatureTranslation, object>> include = null, bool tracking = false)
        {
            IQueryable<FeatureTranslation> query = _db.Set<FeatureTranslation>();

            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }
            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().ToListAsync();
                else
                    return await orderBy(query).ToListAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().ToListAsync();
                else
                    return await query.ToListAsync();
            }
        }

        public async Task<FeatureTranslation> GetAsync(Expression<Func<FeatureTranslation, bool>> where = null, Func<IQueryable<FeatureTranslation>, IOrderedQueryable<FeatureTranslation>> orderBy = null, Func<IQueryable<FeatureTranslation>, IIncludableQueryable<FeatureTranslation, object>> include = null, bool tracking = false)
        {
            IQueryable<FeatureTranslation> query = _db.Set<FeatureTranslation>();
            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }
            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().FirstOrDefaultAsync();
                else
                    return await orderBy(query).FirstOrDefaultAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().FirstOrDefaultAsync();
                else
                    return await query.FirstOrDefaultAsync();
            }
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<FeatureTranslation, TResult>> selector = null, Expression<Func<FeatureTranslation, bool>> where = null, Func<IQueryable<FeatureTranslation>, IOrderedQueryable<FeatureTranslation>> orderBy = null, Func<IQueryable<FeatureTranslation>, IIncludableQueryable<FeatureTranslation, object>> include = null, bool disableTracking = true)
        {
            IQueryable<FeatureTranslation> query = _db.Set<FeatureTranslation>();
            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }

            if (orderBy != null)
            {
                return await orderBy(query).Select(selector).ToListAsync();
            }
            else
            {
                return await query.Select(selector).ToListAsync();
            }
        }

        public async Task Insert(FeatureTranslation featureTranslation)
        {
            await _db.FeatureTranslation_tb.AddAsync(featureTranslation);
        }

        public async Task Save()
        {
            await _db.SaveChangesAsync();
        }

        public void Update(FeatureTranslation featureTranslation)
        {
            Detach(featureTranslation);
        }
        public void Detach(FeatureTranslation featureTranslation)
        {
            var local = _db.Set<FeatureTranslation>().Local
                .FirstOrDefault(entry => entry.FeatureTranslation_ID
                .Equals(featureTranslation.FeatureTranslation_ID));

            if (local != null)
            {
                _db.Entry(local).State = EntityState.Detached;
            }
            _db.Entry(featureTranslation).State = EntityState.Modified;
        }

        public void DeleteAll(IEnumerable<FeatureTranslation> featureTranslations)
        {
            _db.FeatureTranslation_tb.RemoveRange(featureTranslations);
        }
    }
}
