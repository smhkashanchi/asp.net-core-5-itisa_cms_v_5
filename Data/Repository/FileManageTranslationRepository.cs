﻿using Data.Context;

using Domain.DomainClasses.FilesManage;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repository
{
    public class FileManageTranslationRepository : IFileManageTranslationRepository
    {
        private ApplicationDbContext _db;
        public FileManageTranslationRepository(ApplicationDbContext db)
        {
            _db = db;
        }
        public void Delete(FileManageTranslation fileManageTranslation)
        {
            _db.FileManageTranslation_tb.Remove(fileManageTranslation);
        }

        public async Task<bool> ExistsAsync(Expression<Func<FileManageTranslation, bool>> where = null, Func<IQueryable<FileManageTranslation>, IIncludableQueryable<FileManageTranslation, object>> include = null)
        {
            IQueryable<FileManageTranslation> query = _db.Set<FileManageTranslation>();

            if (include != null)
            {
                query = include(query);
            }

            query = query.Where(where);

            if (await query.CountAsync() > 0)
                return true;
            else
                return false;
        }

        public async Task<IEnumerable<FileManageTranslation>> GetAllAsyn(Expression<Func<FileManageTranslation, bool>> where = null, Func<IQueryable<FileManageTranslation>, IOrderedQueryable<FileManageTranslation>> orderBy = null, Func<IQueryable<FileManageTranslation>, IIncludableQueryable<FileManageTranslation, object>> include = null, bool tracking = false)
        {
            IQueryable<FileManageTranslation> query = _db.Set<FileManageTranslation>();

            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }
            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().ToListAsync();
                else
                    return await orderBy(query).ToListAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().ToListAsync();
                else
                    return await query.ToListAsync();
            }
        }

        public async Task<FileManageTranslation> GetAsync(Expression<Func<FileManageTranslation, bool>> where = null, Func<IQueryable<FileManageTranslation>, IOrderedQueryable<FileManageTranslation>> orderBy = null, Func<IQueryable<FileManageTranslation>, IIncludableQueryable<FileManageTranslation, object>> include = null, bool tracking = false)
        {
            IQueryable<FileManageTranslation> query = _db.Set<FileManageTranslation>();
            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }
            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().FirstOrDefaultAsync();
                else
                    return await orderBy(query).FirstOrDefaultAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().FirstOrDefaultAsync();
                else
                    return await query.FirstOrDefaultAsync();
            }
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<FileManageTranslation, TResult>> selector = null, Expression<Func<FileManageTranslation, bool>> where = null, Func<IQueryable<FileManageTranslation>, IOrderedQueryable<FileManageTranslation>> orderBy = null, Func<IQueryable<FileManageTranslation>, IIncludableQueryable<FileManageTranslation, object>> include = null, bool disableTracking = true)
        {
            IQueryable<FileManageTranslation> query = _db.Set<FileManageTranslation>();
            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }

            if (orderBy != null)
            {
                return await orderBy(query).Select(selector).ToListAsync();
            }
            else
            {
                return await query.Select(selector).ToListAsync();
            }
        }

        public async Task Insert(FileManageTranslation fileManageTranslation)
        {
            await _db.FileManageTranslation_tb.AddAsync(fileManageTranslation);
        }

        public async Task Save()
        {
            await _db.SaveChangesAsync();
        }

        public void Update(FileManageTranslation fileManageTranslation)
        {
            Detach(fileManageTranslation);
        }

        public void Detach(FileManageTranslation fileManageTranslation)
        {
            var local = _db.Set<FileManageTranslation>().Local
                .FirstOrDefault(entry => entry.FileManageTranslation_ID
                .Equals(fileManageTranslation.FileManageTranslation_ID));

            // check if local is not null 
            if (local != null)
            {
                // detach
                _db.Entry(local).State = EntityState.Detached;
            }
            // set Modified flag in your entry
            _db.Entry(fileManageTranslation).State = EntityState.Modified;
        }

        public void DeleteAll(IEnumerable<FileManageTranslation> fileManageTranslations)
        {
            _db.FileManageTranslation_tb.RemoveRange(fileManageTranslations);
        }
    }
}
