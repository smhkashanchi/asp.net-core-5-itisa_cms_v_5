﻿using Data.Context;

using Domain.DomainClasses;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repository
{
    public class LanguageRepository : ILanguageRepository
    {
       private ApplicationDbContext _db;
        public LanguageRepository(ApplicationDbContext db)
        {
            _db = db;
        }
        public async Task<IEnumerable<Language>> GetAll(Expression<Func<Language, bool>> where = null, Func<IQueryable<Language>,
            IOrderedQueryable<Language>> orderBy = null, Func<IQueryable<Language>, IIncludableQueryable<Language, object>> include = null, bool tracking = false)
        {
            IQueryable<Language> query = _db.Set<Language>();

            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }
            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().ToListAsync();
                else
                    return await orderBy(query).ToListAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().ToListAsync();
                else
                    return await query.ToListAsync();
            }
        }
        public void Delete(Language language)
        {
            _db.Entry(language).State = EntityState.Deleted;
        }
        public async Task<IEnumerable<Language>> GetAllIsActive()
        {
            return await _db.Language_tb.Where(x => x.IsActive).ToListAsync();
        }

        public async Task<Language> GetOne(string Id)
        {
            return await _db.Language_tb.FindAsync(Id);
        }

        public async Task Insert(Language language)
        {
            await _db.Language_tb.AddAsync(language);
        }

        public async Task<bool> IsExist(string Id)
        {
            return await _db.Language_tb.AnyAsync(x => x.Id == Id);
        }

        public async Task Save()
        {
            await _db.SaveChangesAsync();
        }

        public void Update(Language language)
        {
            _db.Entry(language).State = EntityState.Modified;
        }
    }
}
