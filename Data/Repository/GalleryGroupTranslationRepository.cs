﻿using Data.Context;

using Domain.DomainClasses.Gallery;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repository
{
    public class GalleryGroupTranslationRepository : IGalleryGroupTranslationRepository
    {
        private ApplicationDbContext _db;
        public GalleryGroupTranslationRepository(ApplicationDbContext db)
        {
            _db = db;
        }
        public void Delete(GalleryGroupTranslation galleryGroupTranslation)
        {
            _db.GalleryGroupTranslation_tb.Remove(galleryGroupTranslation);
        }

        public async Task<bool> ExistsAsync(Expression<Func<GalleryGroupTranslation, bool>> where = null, Func<IQueryable<GalleryGroupTranslation>, IIncludableQueryable<GalleryGroupTranslation, object>> include = null)
        {
            IQueryable<GalleryGroupTranslation> query = _db.Set<GalleryGroupTranslation>();

            if (include != null)
            {
                query = include(query);
            }

            query = query.Where(where);

            if (await query.CountAsync() > 0)
                return true;
            else
                return false;
        }

        public async Task<IEnumerable<GalleryGroupTranslation>> GetAllAsyn(Expression<Func<GalleryGroupTranslation, bool>> where = null, Func<IQueryable<GalleryGroupTranslation>, IOrderedQueryable<GalleryGroupTranslation>> orderBy = null, Func<IQueryable<GalleryGroupTranslation>, IIncludableQueryable<GalleryGroupTranslation, object>> include = null, bool tracking = false)
        {
            IQueryable<GalleryGroupTranslation> query = _db.Set<GalleryGroupTranslation>();

            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }
            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().ToListAsync();
                else
                    return await orderBy(query).ToListAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().ToListAsync();
                else
                    return await query.ToListAsync();
            }
        }

        public async Task<GalleryGroupTranslation> GetAsync(Expression<Func<GalleryGroupTranslation, bool>> where = null, Func<IQueryable<GalleryGroupTranslation>, IOrderedQueryable<GalleryGroupTranslation>> orderBy = null, Func<IQueryable<GalleryGroupTranslation>, IIncludableQueryable<GalleryGroupTranslation, object>> include = null, bool tracking = false)
        {
            IQueryable<GalleryGroupTranslation> query = _db.Set<GalleryGroupTranslation>();
            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }
            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().FirstOrDefaultAsync();
                else
                    return await orderBy(query).FirstOrDefaultAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().FirstOrDefaultAsync();
                else
                    return await query.FirstOrDefaultAsync();
            }
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<GalleryGroupTranslation, TResult>> selector = null, Expression<Func<GalleryGroupTranslation, bool>> where = null, Func<IQueryable<GalleryGroupTranslation>, IOrderedQueryable<GalleryGroupTranslation>> orderBy = null, Func<IQueryable<GalleryGroupTranslation>, IIncludableQueryable<GalleryGroupTranslation, object>> include = null, bool disableTracking = true)
        {
            IQueryable<GalleryGroupTranslation> query = _db.Set<GalleryGroupTranslation>();


            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }

            if (orderBy != null)
            {
                return await orderBy(query).Select(selector).ToListAsync();
            }
            else
            {
                return await query.Select(selector).ToListAsync();
            }
        }

        public async Task Insert(GalleryGroupTranslation galleryGroupTranslation)
        {
            await _db.GalleryGroupTranslation_tb.AddAsync(galleryGroupTranslation);
        }

        public async Task Save()
        {
            await _db.SaveChangesAsync();
        }

        public async Task Update(GalleryGroupTranslation galleryGroupTranslation)
        {
            _db.GalleryGroupTranslation_tb.Update(galleryGroupTranslation);
        }
    }
}
