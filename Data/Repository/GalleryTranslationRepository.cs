﻿using Data.Context;

using Domain.DomainClasses.Gallery;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repository
{
    public class GalleryTranslationRepository : IGalleryTranslationRepository
    {
        private ApplicationDbContext _db;
        public GalleryTranslationRepository(ApplicationDbContext db)
        {
            _db = db;
        }
        public void Delete(GalleryTranslation galleryTranslation)
        {
            _db.GalleryTranslation_tb.Remove(galleryTranslation);
        }

        public void DeleteAll(IEnumerable<GalleryTranslation> galleryTranslation)
        {
            _db.GalleryTranslation_tb.RemoveRange(galleryTranslation);
        }

        public async Task<bool> ExistsAsync(Expression<Func<GalleryTranslation, bool>> where = null, Func<IQueryable<GalleryTranslation>, IIncludableQueryable<GalleryTranslation, object>> include = null)
        {
            IQueryable<GalleryTranslation> query = _db.Set<GalleryTranslation>();

            if (include != null)
            {
                query = include(query);
            }

            query = query.Where(where);

            if (await query.CountAsync() > 0)
                return true;
            else
                return false;
        }

        public async Task<IEnumerable<GalleryTranslation>> GetAll(Expression<Func<GalleryTranslation, bool>> where = null, Func<IQueryable<GalleryTranslation>, IOrderedQueryable<GalleryTranslation>> orderBy = null, Func<IQueryable<GalleryTranslation>, IIncludableQueryable<GalleryTranslation, object>> include = null, bool tracking = false)
        {
            IQueryable<GalleryTranslation> query = _db.Set<GalleryTranslation>();

            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }
            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().ToListAsync();
                else
                    return await orderBy(query).ToListAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().ToListAsync();
                else
                    return await query.ToListAsync();
            }
        }

        public async Task<GalleryTranslation> GetAsync(Expression<Func<GalleryTranslation, bool>> where = null, Func<IQueryable<GalleryTranslation>, IOrderedQueryable<GalleryTranslation>> orderBy = null, Func<IQueryable<GalleryTranslation>, IIncludableQueryable<GalleryTranslation, object>> include = null, bool tracking = false)
        {
            IQueryable<GalleryTranslation> query = _db.Set<GalleryTranslation>();
            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }


            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().FirstOrDefaultAsync();
                else
                    return await orderBy(query).FirstOrDefaultAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().FirstOrDefaultAsync();
                else
                    return await query.FirstOrDefaultAsync();
            }
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<GalleryTranslation, TResult>> selector = null, Expression<Func<GalleryTranslation, bool>> where = null, Func<IQueryable<GalleryTranslation>, IOrderedQueryable<GalleryTranslation>> orderBy = null, Func<IQueryable<GalleryTranslation>, IIncludableQueryable<GalleryTranslation, object>> include = null, bool disableTracking = true)
        {
            IQueryable<GalleryTranslation> query = _db.Set<GalleryTranslation>();


            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }

            if (orderBy != null)
            {
                return await orderBy(query).Select(selector).ToListAsync();
            }
            else
            {
                return await query.Select(selector).ToListAsync();
            }
        }

        public async Task Insert(GalleryTranslation galleryTranslation)
        {
            await _db.GalleryTranslation_tb.AddAsync(galleryTranslation);
        }

        public async Task Save()
        {
            await _db.SaveChangesAsync();
        }

        public void Update(GalleryTranslation galleryTranslation)
        {
            _db.GalleryTranslation_tb.Update(galleryTranslation);
        }
    }
}
