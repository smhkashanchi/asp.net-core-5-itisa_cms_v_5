﻿using Data.Context;

using Domain.DomainClasses.Product;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repository
{
    public class ProductGroupTranslationRepository : IProductGroupTranslaionRepository
    {
        private ApplicationDbContext _db;
        public ProductGroupTranslationRepository(ApplicationDbContext db)
        {
            _db = db;
        }
        public void Delete(ProductGroupTranslation productGroupTranslation)
        {
            var local = _db.Set<ProductGroupTranslation>().Local
                 .FirstOrDefault(entry => entry.ProductGroupTranslation_ID
                 .Equals(productGroupTranslation.ProductGroupTranslation_ID));

            if (local != null)
            {
                // detach
                _db.Entry(local).State = EntityState.Detached;
            }
            _db.Entry(productGroupTranslation).State = EntityState.Deleted;
        }

        public void DeleteAll(IEnumerable<ProductGroupTranslation> productGroupTranslations)
        {
            var local = _db.Set<ProductGroup>().Local
               .FirstOrDefault(entry => entry.ProductGroup_ID
               .Equals(productGroupTranslations.FirstOrDefault().ProductGroup_Id));

            if (local != null)
            {
                // detach
                _db.Entry(local).State = EntityState.Detached;
            }
            _db.ProductGroupTranslation_tb.RemoveRange(productGroupTranslations);
        }

        public async Task<bool> ExistsAsync(Expression<Func<ProductGroupTranslation, bool>> where = null, Func<IQueryable<ProductGroupTranslation>, IIncludableQueryable<ProductGroupTranslation, object>> include = null)
        {
            IQueryable<ProductGroupTranslation> query = _db.Set<ProductGroupTranslation>();

            if (include != null)
            {
                query = include(query);
            }

            query = query.Where(where);

            if (await query.CountAsync() > 0)
                return true;
            else
                return false;
        }

        public async Task<IEnumerable<ProductGroupTranslation>> GetAllAsyn(Expression<Func<ProductGroupTranslation, bool>> where = null, Func<IQueryable<ProductGroupTranslation>, IOrderedQueryable<ProductGroupTranslation>> orderBy = null, Func<IQueryable<ProductGroupTranslation>, IIncludableQueryable<ProductGroupTranslation, object>> include = null, bool tracking = false)
        {
            IQueryable<ProductGroupTranslation> query = _db.Set<ProductGroupTranslation>();

            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }
            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().ToListAsync();
                else
                    return await orderBy(query).ToListAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().ToListAsync();
                else
                    return await query.ToListAsync();
            }
        }

        public async Task<ProductGroupTranslation> GetAsync(Expression<Func<ProductGroupTranslation, bool>> where = null, Func<IQueryable<ProductGroupTranslation>, IOrderedQueryable<ProductGroupTranslation>> orderBy = null, Func<IQueryable<ProductGroupTranslation>, IIncludableQueryable<ProductGroupTranslation, object>> include = null, bool tracking = false)
        {
            IQueryable<ProductGroupTranslation> query = _db.Set<ProductGroupTranslation>();
            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }


            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().FirstOrDefaultAsync();
                else
                    return await orderBy(query).FirstOrDefaultAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().FirstOrDefaultAsync();
                else
                    return await query.FirstOrDefaultAsync();
            }
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<ProductGroupTranslation, TResult>> selector = null, Expression<Func<ProductGroupTranslation, bool>> where = null, Func<IQueryable<ProductGroupTranslation>, IOrderedQueryable<ProductGroupTranslation>> orderBy = null, Func<IQueryable<ProductGroupTranslation>, IIncludableQueryable<ProductGroupTranslation, object>> include = null, bool disableTracking = true)
        {
            IQueryable<ProductGroupTranslation> query = _db.Set<ProductGroupTranslation>();


            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }

            if (orderBy != null)
            {
                return await orderBy(query).Select(selector).ToListAsync();
            }
            else
            {
                return await query.AsNoTracking().Select(selector).ToListAsync();
            }
        }

        public async Task InsertRange(IEnumerable<ProductGroupTranslation> productGroupTranslations)
        {
            await _db.ProductGroupTranslation_tb.AddRangeAsync(productGroupTranslations);
        }

        public async Task Save()
        {
            await _db.SaveChangesAsync();
        }

        public void Update(ProductGroupTranslation productGroupTranslation) 
        {
            Detach(productGroupTranslation);
        }
        public void Detach(ProductGroupTranslation productGroupTranslation)
        {
            var local = _db.Set<ProductGroupTranslation>().Local
                .FirstOrDefault(entry => entry.ProductGroupTranslation_ID
                .Equals(productGroupTranslation.ProductGroupTranslation_ID));

            if (local != null)
            {
                // detach
                _db.Entry(local).State = EntityState.Detached;
            }
            _db.Entry(productGroupTranslation).State = EntityState.Modified;
        }

        public async Task Insert(ProductGroupTranslation productGroupTranslation)
        {
            await _db.ProductGroupTranslation_tb.AddAsync(productGroupTranslation);
        }
    }
}
