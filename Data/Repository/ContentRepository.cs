﻿using Data.Context;

using Domain.DomainClasses.Content;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repository
{
    public class ContentRepository : IContentRepository
    {
        private ApplicationDbContext _db;
        public ContentRepository(ApplicationDbContext db)
        {
            _db = db;
        }
        public void DeleteAsync(Content content)
        {
            _db.Content_tb.Remove(content);
        }

        public async Task<bool> ExistsAsync(Expression<Func<Content, bool>> where = null, Func<IQueryable<Content>, IIncludableQueryable<Content, object>> include = null)
        {
            IQueryable<Content> query = _db.Set<Content>();

            if (include != null)
            {
                query = include(query);
            }

            query = query.Where(where);

            if (await query.CountAsync() > 0)
                return true;
            else
                return false;
        }

        public async Task<IEnumerable<Content>> GetAllAsyn(Expression<Func<Content, bool>> where = null, Func<IQueryable<Content>, IOrderedQueryable<Content>> orderBy = null, Func<IQueryable<Content>, IIncludableQueryable<Content, object>> include = null, bool tracking = false)
        {
            IQueryable<Content> query = _db.Set<Content>();

            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }
            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().ToListAsync();
                else
                    return await orderBy(query).ToListAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().ToListAsync();
                else
                    return await query.ToListAsync();
            }
        }

        public async Task<Content> GetAsync(Expression<Func<Content, bool>> where = null, Func<IQueryable<Content>, IOrderedQueryable<Content>> orderBy = null, Func<IQueryable<Content>, IIncludableQueryable<Content, object>> include = null, bool tracking = false)
        {
            IQueryable<Content> query = _db.Set<Content>();
            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }


            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().FirstOrDefaultAsync();
                else
                    return await orderBy(query).FirstOrDefaultAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().FirstOrDefaultAsync();
                else
                    return await query.FirstOrDefaultAsync();
            }
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<Content, TResult>> selector = null, Expression<Func<Content, bool>> where = null, Func<IQueryable<Content>, IOrderedQueryable<Content>> orderBy = null, Func<IQueryable<Content>, IIncludableQueryable<Content, object>> include = null, bool disableTracking = true)
        {
            IQueryable<Content> query = _db.Set<Content>();


            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }

            if (orderBy != null)
            {
                return await orderBy(query).Select(selector).ToListAsync();
            }
            else
            {
                return await query.Select(selector).ToListAsync();
            }
        }

        public async Task Insert(Content Content)
        {
            await _db.Content_tb.AddAsync(Content);
        }

        public async Task Save()
        {
            await _db.SaveChangesAsync();
        }

        public async Task Update(Content content)
        {
            _db.Content_tb.Update(content);
        }
    }
}
