﻿using Data.Context;

using Domain.DomainClasses.Product;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repository
{
    public class SuggestProductRepository : ISuggestProductRepository
    {
        private ApplicationDbContext _db;
        public SuggestProductRepository(ApplicationDbContext db)
        {
            _db = db;
        }
        public void Delete(SuggestProduct suggestProduct)
        {
            _db.SuggestProduct_tb.Remove(suggestProduct);
        }

        public void DeleteAll(IEnumerable<SuggestProduct> suggestProducts)
        {
            _db.SuggestProduct_tb.RemoveRange(suggestProducts);
        }

        public async Task<bool> ExistsAsync(Expression<Func<SuggestProduct, bool>> where = null, Func<IQueryable<SuggestProduct>, IIncludableQueryable<SuggestProduct, object>> include = null)
        {
            IQueryable<SuggestProduct> query = _db.Set<SuggestProduct>();

            if (include != null)
            {
                query = include(query);
            }

            query = query.Where(where);

            if (await query.CountAsync() > 0)
                return true;
            else
                return false;
        }

        public async Task<IEnumerable<SuggestProduct>> GetAllAsyn(Expression<Func<SuggestProduct, bool>> where = null, Func<IQueryable<SuggestProduct>, IOrderedQueryable<SuggestProduct>> orderBy = null, Func<IQueryable<SuggestProduct>, IIncludableQueryable<SuggestProduct, object>> include = null, bool tracking = false)
        {
            IQueryable<SuggestProduct> query = _db.Set<SuggestProduct>();

            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }
            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().ToListAsync();
                else
                    return await orderBy(query).ToListAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().ToListAsync();
                else
                    return await query.ToListAsync();
            }
        }

        public async Task<SuggestProduct> GetAsync(Expression<Func<SuggestProduct, bool>> where = null, Func<IQueryable<SuggestProduct>, IOrderedQueryable<SuggestProduct>> orderBy = null, Func<IQueryable<SuggestProduct>, IIncludableQueryable<SuggestProduct, object>> include = null, bool tracking = false)
        {
            IQueryable<SuggestProduct> query = _db.Set<SuggestProduct>();
            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }


            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().FirstOrDefaultAsync();
                else
                    return await orderBy(query).FirstOrDefaultAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().FirstOrDefaultAsync();
                else
                    return await query.FirstOrDefaultAsync();
            }
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<SuggestProduct, TResult>> selector = null, Expression<Func<SuggestProduct, bool>> where = null, Func<IQueryable<SuggestProduct>, IOrderedQueryable<SuggestProduct>> orderBy = null, Func<IQueryable<SuggestProduct>, IIncludableQueryable<SuggestProduct, object>> include = null, bool disableTracking = true)
        {
            IQueryable<SuggestProduct> query = _db.Set<SuggestProduct>();


            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }

            if (orderBy != null)
            {
                return await orderBy(query).Select(selector).ToListAsync();
            }
            else
            {
                return await query.AsNoTracking().Select(selector).ToListAsync();
            }
        }

        public async Task Insert(SuggestProduct suggestProduct)
        {
            await _db.SuggestProduct_tb.AddAsync(suggestProduct);
        }

        public async Task Save()
        {
            await _db.SaveChangesAsync();
        }

        public async Task Update(SuggestProduct suggestProduct)
        {
            _db.SuggestProduct_tb.Update(suggestProduct);
        }
    }
}
