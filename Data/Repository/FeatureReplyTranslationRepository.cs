﻿using Data.Context;

using Domain.DomainClasses.Feature;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repository
{
    public class FeatureReplyTranslationRepository : IFeatureReplyTranslationRepository
    {
        private ApplicationDbContext _db;

        public FeatureReplyTranslationRepository(ApplicationDbContext db)
        {
            _db = db;
        }
        public void Delete(FeatureReplyTranslation featureReplyTranslation)
        {
            _db.FeatureReplyTranslation_tb.Remove(featureReplyTranslation);
        }

        public async Task<bool> ExistsAsync(Expression<Func<FeatureReplyTranslation, bool>> where = null, Func<IQueryable<FeatureReplyTranslation>, IIncludableQueryable<FeatureReplyTranslation, object>> include = null)
        {
            IQueryable<FeatureReplyTranslation> query = _db.Set<FeatureReplyTranslation>();

            if (include != null)
            {
                query = include(query);
            }

            query = query.Where(where);

            if (await query.CountAsync() > 0)
                return true;
            else
                return false;
        }

        public async Task<IEnumerable<FeatureReplyTranslation>> GetAllAsyn(Expression<Func<FeatureReplyTranslation, bool>> where = null, Func<IQueryable<FeatureReplyTranslation>, IOrderedQueryable<FeatureReplyTranslation>> orderBy = null, Func<IQueryable<FeatureReplyTranslation>, IIncludableQueryable<FeatureReplyTranslation, object>> include = null, bool tracking = false)
        {
            IQueryable<FeatureReplyTranslation> query = _db.Set<FeatureReplyTranslation>();

            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }
            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().ToListAsync();
                else
                    return await orderBy(query).ToListAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().ToListAsync();
                else
                    return await query.ToListAsync();
            }
        }

        public async Task<FeatureReplyTranslation> GetAsync(Expression<Func<FeatureReplyTranslation, bool>> where = null, Func<IQueryable<FeatureReplyTranslation>, IOrderedQueryable<FeatureReplyTranslation>> orderBy = null, Func<IQueryable<FeatureReplyTranslation>, IIncludableQueryable<FeatureReplyTranslation, object>> include = null, bool tracking = false)
        {
            IQueryable<FeatureReplyTranslation> query = _db.Set<FeatureReplyTranslation>();
            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }
            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().FirstOrDefaultAsync();
                else
                    return await orderBy(query).FirstOrDefaultAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().FirstOrDefaultAsync();
                else
                    return await query.FirstOrDefaultAsync();
            }
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<FeatureReplyTranslation, TResult>> selector = null, Expression<Func<FeatureReplyTranslation, bool>> where = null, Func<IQueryable<FeatureReplyTranslation>, IOrderedQueryable<FeatureReplyTranslation>> orderBy = null, Func<IQueryable<FeatureReplyTranslation>, IIncludableQueryable<FeatureReplyTranslation, object>> include = null, bool disableTracking = true)
        {
            IQueryable<FeatureReplyTranslation> query = _db.Set<FeatureReplyTranslation>();
            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }

            if (orderBy != null)
            {
                return await orderBy(query).Select(selector).ToListAsync();
            }
            else
            {
                return await query.Select(selector).ToListAsync();
            }
        }

        public async Task Insert(FeatureReplyTranslation featureReplyTranslation)
        {
            await _db.FeatureReplyTranslation_tb.AddAsync(featureReplyTranslation);
        }

        public async Task Save()
        {
            await _db.SaveChangesAsync();
        }

        public void Update(FeatureReplyTranslation featureReplyTranslation)
        {
            Detach(featureReplyTranslation);
        }
        public void Detach(FeatureReplyTranslation featureReplyTranslation)
        {
            var local = _db.Set<FeatureReplyTranslation>().Local
                .FirstOrDefault(entry => entry.FeatureReplyTranslation_ID
                .Equals(featureReplyTranslation.FeatureReplyTranslation_ID));

            if (local != null)
            {
                _db.Entry(local).State = EntityState.Detached;
            }
            _db.Entry(featureReplyTranslation).State = EntityState.Modified;
        }

        public void DeleteAll(IEnumerable<FeatureReplyTranslation> featureReplyTranslations)
        {
            _db.FeatureReplyTranslation_tb.RemoveRange(featureReplyTranslations);
        }
    }
}
