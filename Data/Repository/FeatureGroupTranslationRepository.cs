﻿using Data.Context;

using Domain.DomainClasses.Feature;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repository
{
    public class FeatureGroupTranslationRepository : IFeatureGroupTranslationRepository
    {
        private ApplicationDbContext _db;

        public FeatureGroupTranslationRepository(ApplicationDbContext db)
        {
            _db = db;
        }
        public void Delete(FeatureGroupTranslation featureGroupTranslation)
        {
            _db.FeatureGroupTranslation_tb.Remove(featureGroupTranslation);
        }

        public async Task<bool> ExistsAsync(Expression<Func<FeatureGroupTranslation, bool>> where = null, Func<IQueryable<FeatureGroupTranslation>, IIncludableQueryable<FeatureGroupTranslation, object>> include = null)
        {
            IQueryable<FeatureGroupTranslation> query = _db.Set<FeatureGroupTranslation>();

            if (include != null)
            {
                query = include(query);
            }

            query = query.Where(where);

            if (await query.CountAsync() > 0)
                return true;
            else
                return false;
        }

        public async Task<IEnumerable<FeatureGroupTranslation>> GetAllAsyn(Expression<Func<FeatureGroupTranslation, bool>> where = null, Func<IQueryable<FeatureGroupTranslation>, IOrderedQueryable<FeatureGroupTranslation>> orderBy = null, Func<IQueryable<FeatureGroupTranslation>, IIncludableQueryable<FeatureGroupTranslation, object>> include = null, bool tracking = false)
        {
            IQueryable<FeatureGroupTranslation> query = _db.Set<FeatureGroupTranslation>();

            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }
            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().ToListAsync();
                else
                    return await orderBy(query).ToListAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().ToListAsync();
                else
                    return await query.ToListAsync();
            }
        }

        public async Task<FeatureGroupTranslation> GetAsync(Expression<Func<FeatureGroupTranslation, bool>> where = null, Func<IQueryable<FeatureGroupTranslation>, IOrderedQueryable<FeatureGroupTranslation>> orderBy = null, Func<IQueryable<FeatureGroupTranslation>, IIncludableQueryable<FeatureGroupTranslation, object>> include = null, bool tracking = false)
        {
            IQueryable<FeatureGroupTranslation> query = _db.Set<FeatureGroupTranslation>();
            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }
            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().FirstOrDefaultAsync();
                else
                    return await orderBy(query).FirstOrDefaultAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().FirstOrDefaultAsync();
                else
                    return await query.FirstOrDefaultAsync();
            }
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<FeatureGroupTranslation, TResult>> selector = null, Expression<Func<FeatureGroupTranslation, bool>> where = null, Func<IQueryable<FeatureGroupTranslation>, IOrderedQueryable<FeatureGroupTranslation>> orderBy = null, Func<IQueryable<FeatureGroupTranslation>, IIncludableQueryable<FeatureGroupTranslation, object>> include = null, bool disableTracking = true)
        {
            IQueryable<FeatureGroupTranslation> query = _db.Set<FeatureGroupTranslation>();
            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }

            if (orderBy != null)
            {
                return await orderBy(query).Select(selector).ToListAsync();
            }
            else
            {
                return await query.Select(selector).ToListAsync();
            }
        }

        public async Task Insert(FeatureGroupTranslation featureGroupTranslation)
        {
            await _db.FeatureGroupTranslation_tb.AddAsync(featureGroupTranslation);
        }

        public async Task Save()
        {
            await _db.SaveChangesAsync();
        }

        public void Update(FeatureGroupTranslation featureGroupTranslation)
        {
            Detach(featureGroupTranslation);
        }
        public void Detach(FeatureGroupTranslation featureGroupTranslation)
        {
            var local = _db.Set<FeatureGroupTranslation>().Local
                .FirstOrDefault(entry => entry.FeatureGroupTranslation_ID
                .Equals(featureGroupTranslation.FeatureGroupTranslation_ID));
            if (local != null)
            {
                _db.Entry(local).State = EntityState.Detached;
            }
            _db.Entry(featureGroupTranslation).State = EntityState.Modified;
        }
    }
}
