﻿using Data.Context;

using Domain.DomainClasses.Connection;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repository
{
    public class ConnectionTranslationRepository:IConnectionTranslationRepository
    {
        private ApplicationDbContext _db;
        public ConnectionTranslationRepository(ApplicationDbContext db)
        {
            _db = db;
        }

        public void Delete(ConnectionTranslation connectionTranslation)
        {
            _db.ConnectionTranslation_tb.Remove(connectionTranslation);
        }

        public void DeleteAll(IEnumerable<ConnectionTranslation> connectionTranslation)
        {
            _db.ConnectionTranslation_tb.RemoveRange(connectionTranslation);
        }

        public async Task<bool> ExistsAsync(Expression<Func<ConnectionTranslation, bool>> where = null, Func<IQueryable<ConnectionTranslation>, IIncludableQueryable<ConnectionTranslation, object>> include = null)
        {
            IQueryable<ConnectionTranslation> query = _db.Set<ConnectionTranslation>();

            if (include != null)
            {
                query = include(query);
            }

            query = query.Where(where);

            if (await query.CountAsync() > 0)
                return true;
            else
                return false;
        }

        public async Task<Dictionary<string, int>> GetAllToDictionary(int Id)
        {
            return await _db.ConnectionTranslation_tb
                .Where(x => x.Id == (Int32)Id)
                .ToDictionaryAsync(x => x.LanguageId, x => (Int32)x.Id);
        }

        public async Task<ConnectionTranslation> GetAsync(Expression<Func<ConnectionTranslation, bool>> where = null, Func<IQueryable<ConnectionTranslation>, IOrderedQueryable<ConnectionTranslation>> orderBy = null, Func<IQueryable<ConnectionTranslation>,IIncludableQueryable<ConnectionTranslation, object>> include = null, bool tracking = false)
        {
            IQueryable<ConnectionTranslation> query = _db.Set<ConnectionTranslation>();
            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }


            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().FirstOrDefaultAsync();
                else
                    return await orderBy(query).FirstOrDefaultAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().FirstOrDefaultAsync();
                else
                    return await query.FirstOrDefaultAsync();
            }
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<ConnectionTranslation, TResult>> selector = null, Expression<Func<ConnectionTranslation, bool>> where = null, Func<IQueryable<ConnectionTranslation>, IOrderedQueryable<ConnectionTranslation>> orderBy = null, Func<IQueryable<ConnectionTranslation>, IIncludableQueryable<ConnectionTranslation, object>> include = null, bool disableTracking = true)
        {
            IQueryable<ConnectionTranslation> query = _db.Set<ConnectionTranslation>();


            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }

            if (orderBy != null)
            {
                return await orderBy(query).Select(selector).ToListAsync();
            }
            else
            {
                return await query.Select(selector).ToListAsync();
            }
        }

        public async Task Insert(ConnectionTranslation connectionTranslation)
        {
            await _db.ConnectionTranslation_tb.AddAsync(connectionTranslation);
        }

        public async Task Save()
        {
            await _db.SaveChangesAsync();
        }

        public void Update(ConnectionTranslation connectionTranslation)
        {
            _db.ConnectionTranslation_tb.Update(connectionTranslation);
        }
    }
}
