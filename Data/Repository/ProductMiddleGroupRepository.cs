﻿using Data.Context;

using Domain.DomainClasses.Product;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repository
{
    public class ProductMiddleGroupRepository : IProductMiddleGroupRepository
    {
        private ApplicationDbContext _db;
        public ProductMiddleGroupRepository(ApplicationDbContext db)
        {
            _db = db;
        }
        public void Delete(ProductMiddleGroup productMiddleGroup)
        {
            _db.ProductMiddleGroup_tb.Remove(productMiddleGroup);
        }

        public void DeleteAll(IEnumerable<ProductMiddleGroup> productMiddleGroups)
        {
            _db.ProductMiddleGroup_tb.RemoveRange(productMiddleGroups);
        }

        public async Task<bool> ExistsAsync(Expression<Func<ProductMiddleGroup, bool>> where = null, Func<IQueryable<ProductMiddleGroup>, IIncludableQueryable<ProductMiddleGroup, object>> include = null)
        {
            IQueryable<ProductMiddleGroup> query = _db.Set<ProductMiddleGroup>();
            if (include != null)
            {
                query = include(query);
            }

            query = query.Where(where);

            if (await query.CountAsync() > 0)
                return true;
            else
                return false;
        }

        public async Task<IEnumerable<ProductMiddleGroup>> GetAllAsyn(Expression<Func<ProductMiddleGroup, bool>> where = null, Func<IQueryable<ProductMiddleGroup>, IOrderedQueryable<ProductMiddleGroup>> orderBy = null, Func<IQueryable<ProductMiddleGroup>, IIncludableQueryable<ProductMiddleGroup, object>> include = null, bool tracking = false)
        {
            IQueryable<ProductMiddleGroup> query = _db.Set<ProductMiddleGroup>();

            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }
            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().ToListAsync();
                else
                    return await orderBy(query).ToListAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().ToListAsync();
                else
                    return await query.ToListAsync();
            }
        }

        public async Task<ProductMiddleGroup> GetAsync(Expression<Func<ProductMiddleGroup, bool>> where = null, Func<IQueryable<ProductMiddleGroup>, IOrderedQueryable<ProductMiddleGroup>> orderBy = null, Func<IQueryable<ProductMiddleGroup>, IIncludableQueryable<ProductMiddleGroup, object>> include = null, bool tracking = false)
        {
            IQueryable<ProductMiddleGroup> query = _db.Set<ProductMiddleGroup>();
            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }


            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().FirstOrDefaultAsync();
                else
                    return await orderBy(query).FirstOrDefaultAsync();
            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().FirstOrDefaultAsync();
                else
                    return await query.FirstOrDefaultAsync();
            }
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<ProductMiddleGroup, TResult>> selector = null, Expression<Func<ProductMiddleGroup, bool>> where = null, Func<IQueryable<ProductMiddleGroup>, IOrderedQueryable<ProductMiddleGroup>> orderBy = null, Func<IQueryable<ProductMiddleGroup>, IIncludableQueryable<ProductMiddleGroup, object>> include = null, bool disableTracking = true)
        {
            IQueryable<ProductMiddleGroup> query = _db.Set<ProductMiddleGroup>();


            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }

            if (orderBy != null)
            {
                return await orderBy(query).Select(selector).ToListAsync();
            }
            else
            {
                return await query.Select(selector).ToListAsync();
            }
        }

        public async Task Insert(ProductMiddleGroup productMiddleGroup)
        {
            await _db.ProductMiddleGroup_tb.AddAsync(productMiddleGroup);
        }

        public async Task InsertRange(IEnumerable<ProductMiddleGroup> productMiddleGroups)
        {
            await _db.ProductMiddleGroup_tb.AddRangeAsync(productMiddleGroups);
        }

        public async Task Save()
        {
            await _db.SaveChangesAsync();
        }

        public void Update(ProductMiddleGroup productMiddleGroup)
        {
            _db.ProductMiddleGroup_tb.Update(productMiddleGroup);
        }
    }
}
