﻿using Data.Context;

using Domain.DomainClasses.Region;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repository
{
    public class StateRepository : IStateRepository
    {
        private ApplicationDbContext _db;
        public StateRepository(ApplicationDbContext db)
        {
            _db = db;
        }
        public void Delete(State state)
        {
            _db.State_tb.Remove(state);
        }

        public void DeleteAll(IEnumerable<State> states)
        {
            _db.State_tb.RemoveRange(states);
        }

        public async Task<bool> ExistsAsync(Expression<Func<State, bool>> where = null, Func<IQueryable<State>, IIncludableQueryable<State, object>> include = null)
        {
            IQueryable<State> query = _db.Set<State>();

            if (include != null)
            {
                query = include(query);
            }

            query = query.Where(where);

            if (await query.CountAsync() > 0)
                return true;
            else
                return false;
        }

        public async Task<IEnumerable<State>> GetAllAsyn(Expression<Func<State, bool>> where = null, Func<IQueryable<State>, IOrderedQueryable<State>> orderBy = null, Func<IQueryable<State>, IIncludableQueryable<State, object>> include = null, bool tracking = false)
        {
            IQueryable<State> query = _db.Set<State>();

            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }
            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().ToListAsync();
                else
                    return await orderBy(query).ToListAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().ToListAsync();
                else
                    return await query.ToListAsync();
            }
        }

        public async Task<State> GetAsync(Expression<Func<State, bool>> where = null, Func<IQueryable<State>, IOrderedQueryable<State>> orderBy = null, Func<IQueryable<State>, IIncludableQueryable<State, object>> include = null, bool tracking = false)
        {
            IQueryable<State> query = _db.Set<State>();
            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }


            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().FirstOrDefaultAsync();
                else
                    return await orderBy(query).FirstOrDefaultAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().FirstOrDefaultAsync();
                else
                    return await query.FirstOrDefaultAsync();
            }
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<State, TResult>> selector = null, Expression<Func<State, bool>> where = null, Func<IQueryable<State>, IOrderedQueryable<State>> orderBy = null, Func<IQueryable<State>, IIncludableQueryable<State, object>> include = null, bool disableTracking = true)
        {
            IQueryable<State> query = _db.Set<State>();


            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }

            if (orderBy != null)
            {
                return await orderBy(query).Select(selector).ToListAsync();
            }
            else
            {
                return await query.AsNoTracking().Select(selector).ToListAsync();
            }
        }

        public async Task Insert(State state)
        {
            await _db.State_tb.AddAsync(state);
        }

        public async Task Save()
        {
            await _db.SaveChangesAsync();
        }

        public async Task Update(State state)
        {
             _db.State_tb.Update(state);
        }
    }
}
