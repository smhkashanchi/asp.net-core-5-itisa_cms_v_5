﻿using Data.Context;

using Domain.DomainClasses.CoWorker;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repository
{
    public class CoWorkerGroupTranslationRepository : ICoWorkerGroupTranslationRepository
    {
        private ApplicationDbContext _db;
        public CoWorkerGroupTranslationRepository(ApplicationDbContext db)
        {
            _db = db;
        }
        public void DeleteAsync(CoWorkerGroupTranslation coWorkerGroupTranslation)
        {
            _db.CoWorkerGroupTranslation_tb.Remove(coWorkerGroupTranslation);
        }

        public async Task<bool> ExistsAsync(Expression<Func<CoWorkerGroupTranslation, bool>> where = null, Func<IQueryable<CoWorkerGroupTranslation>, IIncludableQueryable<CoWorkerGroupTranslation, object>> include = null)
        {
            IQueryable<CoWorkerGroupTranslation> query = _db.Set<CoWorkerGroupTranslation>();

            if (include != null)
            {
                query = include(query);
            }

            query = query.Where(where);

            if (await query.CountAsync() > 0)
                return true;
            else
                return false;
        }

        public async Task<IEnumerable<CoWorkerGroupTranslation>> GetAllAsyn(Expression<Func<CoWorkerGroupTranslation, bool>> where = null, Func<IQueryable<CoWorkerGroupTranslation>, IOrderedQueryable<CoWorkerGroupTranslation>> orderBy = null, Func<IQueryable<CoWorkerGroupTranslation>, IIncludableQueryable<CoWorkerGroupTranslation, object>> include = null, bool tracking = false)
        {
            IQueryable<CoWorkerGroupTranslation> query = _db.Set<CoWorkerGroupTranslation>();

            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }
            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().ToListAsync();
                else
                    return await orderBy(query).ToListAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().ToListAsync();
                else
                    return await query.ToListAsync();
            }
        }

        public async Task<CoWorkerGroupTranslation> GetAsync(Expression<Func<CoWorkerGroupTranslation, bool>> where = null, Func<IQueryable<CoWorkerGroupTranslation>, IOrderedQueryable<CoWorkerGroupTranslation>> orderBy = null, Func<IQueryable<CoWorkerGroupTranslation>, IIncludableQueryable<CoWorkerGroupTranslation, object>> include = null, bool tracking = false)
        {
            IQueryable<CoWorkerGroupTranslation> query = _db.Set<CoWorkerGroupTranslation>();
            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }
            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().FirstOrDefaultAsync();
                else
                    return await orderBy(query).FirstOrDefaultAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().FirstOrDefaultAsync();
                else
                    return await query.FirstOrDefaultAsync();
            }
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<CoWorkerGroupTranslation, TResult>> selector = null, Expression<Func<CoWorkerGroupTranslation, bool>> where = null, Func<IQueryable<CoWorkerGroupTranslation>, IOrderedQueryable<CoWorkerGroupTranslation>> orderBy = null, Func<IQueryable<CoWorkerGroupTranslation>, IIncludableQueryable<CoWorkerGroupTranslation, object>> include = null, bool disableTracking = true)
        {
            IQueryable<CoWorkerGroupTranslation> query = _db.Set<CoWorkerGroupTranslation>();


            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }

            if (orderBy != null)
            {
                return await orderBy(query).Select(selector).ToListAsync();
            }
            else
            {
                return await query.Select(selector).ToListAsync();
            }
        }

        public async Task Insert(CoWorkerGroupTranslation coWorkerGroupTranslation)
        {
            await _db.CoWorkerGroupTranslation_tb.AddAsync(coWorkerGroupTranslation);
        }

        public async Task Save()
        {
            await _db.SaveChangesAsync();
        }

        public async Task Update(CoWorkerGroupTranslation coWorkerGroupTranslation)
        {
            _db.CoWorkerGroupTranslation_tb.Update(coWorkerGroupTranslation);
        }
    }
}
