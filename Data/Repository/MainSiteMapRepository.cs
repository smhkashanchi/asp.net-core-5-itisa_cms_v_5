﻿using Data.Context;

using Domain.DomainClasses.Seo;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repository
{
    public class MainSiteMapRepository : IMainSiteMapRepository
    {
        private ApplicationDbContext _db;
        public MainSiteMapRepository(ApplicationDbContext db)
        {
            _db = db;
        }
        public void DeleteAsync(MainSiteMap MainSiteMap)
        {
            _db.MainSiteMap_tb.Remove(MainSiteMap);
        }

        public async Task<bool> ExistsAsync(Expression<Func<MainSiteMap, bool>> where = null, Func<IQueryable<MainSiteMap>, IIncludableQueryable<MainSiteMap, object>> include = null)
        {
            IQueryable<MainSiteMap> query = _db.Set<MainSiteMap>();

            if (include != null)
            {
                query = include(query);
            }

            query = query.Where(where);

            if (await query.CountAsync() > 0)
                return true;
            else
                return false;
        }

        public async Task<IEnumerable<MainSiteMap>> GetAllAsyn(Expression<Func<MainSiteMap, bool>> where = null, Func<IQueryable<MainSiteMap>, IOrderedQueryable<MainSiteMap>> orderBy = null, Func<IQueryable<MainSiteMap>, IIncludableQueryable<MainSiteMap, object>> include = null, bool tracking = false)
        {
            IQueryable<MainSiteMap> query = _db.Set<MainSiteMap>();

            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }
            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().ToListAsync();
                else
                    return await orderBy(query).ToListAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().ToListAsync();
                else
                    return await query.ToListAsync();
            }
        }

        public async Task<MainSiteMap> GetAsync(Expression<Func<MainSiteMap, bool>> where = null, Func<IQueryable<MainSiteMap>, IOrderedQueryable<MainSiteMap>> orderBy = null, Func<IQueryable<MainSiteMap>, IIncludableQueryable<MainSiteMap, object>> include = null, bool tracking = false)
        {
            IQueryable<MainSiteMap> query = _db.Set<MainSiteMap>();
            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }


            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().FirstOrDefaultAsync();
                else
                    return await orderBy(query).FirstOrDefaultAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().FirstOrDefaultAsync();
                else
                    return await query.FirstOrDefaultAsync();
            }
        }

        public Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<MainSiteMap, TResult>> selector = null, Expression<Func<MainSiteMap, bool>> where = null, Func<IQueryable<MainSiteMap>, IOrderedQueryable<MainSiteMap>> orderBy = null, Func<IQueryable<MainSiteMap>, IIncludableQueryable<MainSiteMap, object>> include = null, bool disableTracking = true)
        {
            throw new NotImplementedException();
        }

        public async Task Insert(MainSiteMap MainSiteMap)
        {
            await _db.MainSiteMap_tb.AddAsync(MainSiteMap);
        }

        public async Task Save()
        {
            await _db.SaveChangesAsync();
        }

        public void Update(MainSiteMap MainSiteMap)
        {
            _db.MainSiteMap_tb.Update(MainSiteMap);
        }
    }
}
