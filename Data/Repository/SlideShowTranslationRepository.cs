﻿using Data.Context;

using Domain.DomainClasses.SlideShow;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repository
{
    public class SlideShowTranslationRepository : ISlideShowTranslationRepository
    {
        private ApplicationDbContext _db;
        public SlideShowTranslationRepository(ApplicationDbContext db)
        {
            _db = db;
        }
        public void Delete(SlideShowTranslation slideShowTranslation)
        {
            _db.SlideShowTranslation_tb.Remove(slideShowTranslation);
        }

        public void DeleteAll(IEnumerable<SlideShowTranslation> slideShowTranslation)
        {
            var local = _db.Set<SlideShowTranslation>().Local
              .FirstOrDefault(entry => entry.SlideShowTranslation_ID
              .Equals(slideShowTranslation.FirstOrDefault().SlideShowTranslation_ID));
            if (local != null)
            {
                _db.Entry(local).State = EntityState.Detached;
            }
            _db.SlideShowTranslation_tb.RemoveRange(slideShowTranslation);
        }

        public async Task<bool> ExistsAsync(Expression<Func<SlideShowTranslation, bool>> where = null, Func<IQueryable<SlideShowTranslation>, IIncludableQueryable<SlideShowTranslation, object>> include = null)
        {
            IQueryable<SlideShowTranslation> query = _db.Set<SlideShowTranslation>();

            if (include != null)
            {
                query = include(query);
            }

            query = query.Where(where);

            if (await query.CountAsync() > 0)
                return true;
            else
                return false;
        }

        public Task<Dictionary<string, int>> GetAllToDictionary(int Id)
        {
            throw new NotImplementedException();
        }

        public async Task<SlideShowTranslation> GetAsync(Expression<Func<SlideShowTranslation, bool>> where = null, Func<IQueryable<SlideShowTranslation>, IOrderedQueryable<SlideShowTranslation>> orderBy = null, Func<IQueryable<SlideShowTranslation>, IIncludableQueryable<SlideShowTranslation, object>> include = null, bool tracking = false)
        {
            IQueryable<SlideShowTranslation> query = _db.Set<SlideShowTranslation>();
            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }


            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().FirstOrDefaultAsync();
                else
                    return await orderBy(query).FirstOrDefaultAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().FirstOrDefaultAsync();
                else
                    return await query.FirstOrDefaultAsync();
            }
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<SlideShowTranslation, TResult>> selector = null, Expression<Func<SlideShowTranslation, bool>> where = null, Func<IQueryable<SlideShowTranslation>, IOrderedQueryable<SlideShowTranslation>> orderBy = null, Func<IQueryable<SlideShowTranslation>, IIncludableQueryable<SlideShowTranslation, object>> include = null, bool disableTracking = true)
        {
            IQueryable<SlideShowTranslation> query = _db.Set<SlideShowTranslation>();


            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }

            if (orderBy != null)
            {
                return await orderBy(query).Select(selector).ToListAsync();
            }
            else
            {
                return await query.Select(selector).ToListAsync();
            }
        }

        public async Task Insert(SlideShowTranslation slideShowTranslation)
        {
            await _db.SlideShowTranslation_tb.AddAsync(slideShowTranslation);
        }

        public async Task Save()
        {
            await _db.SaveChangesAsync();
        }

        public async Task Update(SlideShowTranslation slideShowTranslation)
        {
            _db.SlideShowTranslation_tb.Update(slideShowTranslation);
        }
    }
}
