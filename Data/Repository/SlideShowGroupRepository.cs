﻿using Data.Context;

using Domain.DomainClasses.SlideShow;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repository
{
    public class SlideShowGroupRepository : ISlideShowGroupRepository
    {
        private ApplicationDbContext _db;
        public SlideShowGroupRepository(ApplicationDbContext db)
        {
            _db = db;
        }
        public void DeleteAsync(SlideShowGroup slideShowGroup)
        {
            _db.SlideShowGroup_tb.Remove(slideShowGroup);
        }

        public async Task<bool> ExistsAsync(Expression<Func<SlideShowGroup, bool>> where = null, Func<IQueryable<SlideShowGroup>, IIncludableQueryable<SlideShowGroup, object>> include = null)
        {
            IQueryable<SlideShowGroup> query = _db.Set<SlideShowGroup>();

            if (include != null)
            {
                query = include(query);
            }

            query = query.Where(where);

            if (await query.CountAsync() > 0)
                return true;
            else
                return false;
        }

        public async Task<IEnumerable<SlideShowGroup>> GetAllAsyn(Expression<Func<SlideShowGroup, bool>> where = null, Func<IQueryable<SlideShowGroup>, IOrderedQueryable<SlideShowGroup>> orderBy = null, Func<IQueryable<SlideShowGroup>, IIncludableQueryable<SlideShowGroup, object>> include = null, bool tracking = false)
        {
            IQueryable<SlideShowGroup> query = _db.Set<SlideShowGroup>();

            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }
            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().ToListAsync();
                else
                    return await orderBy(query).ToListAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().ToListAsync();
                else
                    return await query.ToListAsync();
            }
        }

        public async Task<SlideShowGroup> GetAsync(Expression<Func<SlideShowGroup, bool>> where = null, Func<IQueryable<SlideShowGroup>, IOrderedQueryable<SlideShowGroup>> orderBy = null, Func<IQueryable<SlideShowGroup>, IIncludableQueryable<SlideShowGroup, object>> include = null, bool tracking = false)
        {
            IQueryable<SlideShowGroup> query = _db.Set<SlideShowGroup>();
            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }


            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().FirstOrDefaultAsync();
                else
                    return await orderBy(query).FirstOrDefaultAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().FirstOrDefaultAsync();
                else
                    return await query.FirstOrDefaultAsync();
            }
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<SlideShowGroup, TResult>> selector = null, Expression<Func<SlideShowGroup, bool>> where = null, Func<IQueryable<SlideShowGroup>, IOrderedQueryable<SlideShowGroup>> orderBy = null, Func<IQueryable<SlideShowGroup>, IIncludableQueryable<SlideShowGroup, object>> include = null, bool disableTracking = true)
        {
            IQueryable<SlideShowGroup> query = _db.Set<SlideShowGroup>();


            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }

            if (orderBy != null)
            {
                return await orderBy(query).Select(selector).ToListAsync();
            }
            else
            {
                return await query.Select(selector).ToListAsync();
            }
        }

        public async Task Insert(SlideShowGroup slideShowGroup)
        {
            await _db.SlideShowGroup_tb.AddAsync(slideShowGroup);
        }

        public async Task Save()
        {
            await _db.SaveChangesAsync();
        }

        public async Task Update(SlideShowGroup slideShowGroup)
        {
            _db.SlideShowGroup_tb.Update(slideShowGroup);
        }
    }
}
