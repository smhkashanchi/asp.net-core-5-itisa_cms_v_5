﻿using Data.Context;

using Domain.DomainClasses.Content;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repository
{
    public class MiddleContentGroupRepository : IMiddleContentGroupRepository
    {
        private ApplicationDbContext _db;
        public MiddleContentGroupRepository(ApplicationDbContext db)
        {
            _db = db;
        }
        public void Delete(ContentMiddleGroup ContentMiddleGroup)
        {
            _db.ContentMiddleGroup_tb.Remove(ContentMiddleGroup);
        }

        public async Task<bool> ExistsAsync(Expression<Func<ContentMiddleGroup, bool>> where = null, Func<IQueryable<ContentMiddleGroup>, IIncludableQueryable<ContentMiddleGroup, object>> include = null)
        {
            IQueryable<ContentMiddleGroup> query = _db.Set<ContentMiddleGroup>();
            if (include != null)
            {
                query = include(query);
            }

            query = query.Where(where);

            if (await query.CountAsync() > 0)
                return true;
            else
                return false;
        }

        public async Task<IEnumerable<ContentMiddleGroup>> GetAllAsyn(Expression<Func<ContentMiddleGroup, bool>> where = null, Func<IQueryable<ContentMiddleGroup>, IOrderedQueryable<ContentMiddleGroup>> orderBy = null, Func<IQueryable<ContentMiddleGroup>, IIncludableQueryable<ContentMiddleGroup, object>> include = null, bool tracking = false)
        {
            IQueryable<ContentMiddleGroup> query = _db.Set<ContentMiddleGroup>();

            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }
            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().ToListAsync();
                else
                    return await orderBy(query).ToListAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().ToListAsync();
                else
                    return await query.ToListAsync();
            }
        }

        public async Task<ContentMiddleGroup> GetAsync(Expression<Func<ContentMiddleGroup, bool>> where = null, Func<IQueryable<ContentMiddleGroup>, IOrderedQueryable<ContentMiddleGroup>> orderBy = null, Func<IQueryable<ContentMiddleGroup>, IIncludableQueryable<ContentMiddleGroup, object>> include = null, bool tracking = false)
        {

            IQueryable<ContentMiddleGroup> query = _db.Set<ContentMiddleGroup>();
            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }


            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().FirstOrDefaultAsync();
                else
                    return await orderBy(query).FirstOrDefaultAsync();
            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().FirstOrDefaultAsync();
                else
                    return await query.FirstOrDefaultAsync();
            }
        }
        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<ContentMiddleGroup, TResult>> selector = null, Expression<Func<ContentMiddleGroup, bool>> where = null, Func<IQueryable<ContentMiddleGroup>, IOrderedQueryable<ContentMiddleGroup>> orderBy = null, Func<IQueryable<ContentMiddleGroup>, IIncludableQueryable<ContentMiddleGroup, object>> include = null, bool disableTracking = true)
        {
            IQueryable<ContentMiddleGroup> query = _db.Set<ContentMiddleGroup>();


            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }

            if (orderBy != null)
            {
                return await orderBy(query).Select(selector).ToListAsync();
            }
            else
            {
                return await query.Select(selector).ToListAsync();
            }
        }

        public async Task Insert(ContentMiddleGroup ContentMiddleGroup)
        {
            await _db.ContentMiddleGroup_tb.AddAsync(ContentMiddleGroup);
        }

        public async Task Save()
        {
            await _db.SaveChangesAsync();
        }

        public async Task Update(ContentMiddleGroup contentMiddleGroup)
        {
            await Detach(contentMiddleGroup);
        }
        public async Task Detach(ContentMiddleGroup contentMiddleGroup)
        {
            var local = _db.Set<ContentMiddleGroup>().Local
                .FirstOrDefault(entry => entry.ID
                .Equals(contentMiddleGroup.ID));

            // check if local is not null 
            if (local != null)
            {
                // detach
                _db.Entry(local).State = EntityState.Detached;
            }
            // set Modified flag in your entry
            _db.Entry(contentMiddleGroup).State = EntityState.Modified;
        }

        public void DeleteAll(IEnumerable<ContentMiddleGroup> contentMiddleGroups)
        {
            _db.ContentMiddleGroup_tb.RemoveRange(contentMiddleGroups);
        }
    }
}
