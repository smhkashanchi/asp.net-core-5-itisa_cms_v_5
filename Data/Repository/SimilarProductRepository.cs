﻿using Data.Context;

using Domain.DomainClasses.Product;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repository
{
    public class SimilarProductRepository : ISimilarProductRepository
    {
        private ApplicationDbContext _db;
        public SimilarProductRepository(ApplicationDbContext db)
        {
            _db = db;
        }
        public void Delete(SimilarProduct similarProduct)
        {
            _db.SimilarProduct_tb.Remove(similarProduct);
        }

        public void DeleteAll(IEnumerable<SimilarProduct> similarProducts)
        {
            _db.SimilarProduct_tb.RemoveRange(similarProducts);
        }

        public async Task<bool> ExistsAsync(Expression<Func<SimilarProduct, bool>> where = null, Func<IQueryable<SimilarProduct>, IIncludableQueryable<SimilarProduct, object>> include = null)
        {
            IQueryable<SimilarProduct> query = _db.Set<SimilarProduct>();

            if (include != null)
            {
                query = include(query);
            }

            query = query.Where(where);

            if (await query.CountAsync() > 0)
                return true;
            else
                return false;
        }

        public async Task<IEnumerable<SimilarProduct>> GetAllAsyn(Expression<Func<SimilarProduct, bool>> where = null, Func<IQueryable<SimilarProduct>, IOrderedQueryable<SimilarProduct>> orderBy = null, Func<IQueryable<SimilarProduct>, IIncludableQueryable<SimilarProduct, object>> include = null, bool tracking = false)
        {
            IQueryable<SimilarProduct> query = _db.Set<SimilarProduct>();

            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }
            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().ToListAsync();
                else
                    return await orderBy(query).ToListAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().ToListAsync();
                else
                    return await query.ToListAsync();
            }
        }

        public async Task<SimilarProduct> GetAsync(Expression<Func<SimilarProduct, bool>> where = null, Func<IQueryable<SimilarProduct>, IOrderedQueryable<SimilarProduct>> orderBy = null, Func<IQueryable<SimilarProduct>, IIncludableQueryable<SimilarProduct, object>> include = null, bool tracking = false)
        {
            IQueryable<SimilarProduct> query = _db.Set<SimilarProduct>();
            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }


            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().FirstOrDefaultAsync();
                else
                    return await orderBy(query).FirstOrDefaultAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().FirstOrDefaultAsync();
                else
                    return await query.FirstOrDefaultAsync();
            }
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<SimilarProduct, TResult>> selector = null, Expression<Func<SimilarProduct, bool>> where = null, Func<IQueryable<SimilarProduct>, IOrderedQueryable<SimilarProduct>> orderBy = null, Func<IQueryable<SimilarProduct>, IIncludableQueryable<SimilarProduct, object>> include = null, bool disableTracking = true)
        {
            IQueryable<SimilarProduct> query = _db.Set<SimilarProduct>();


            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }

            if (orderBy != null)
            {
                return await orderBy(query).Select(selector).ToListAsync();
            }
            else
            {
                return await query.AsNoTracking().Select(selector).ToListAsync();
            }
        }

        public async Task Insert(SimilarProduct similarProduct)
        {
            await _db.SimilarProduct_tb.AddAsync(similarProduct);
        }

        public async Task Save()
        {
            await _db.SaveChangesAsync();
        }

        public async Task Update(SimilarProduct similarProduct)
        {
             _db.SimilarProduct_tb.Update(similarProduct);
        }
    }
}
