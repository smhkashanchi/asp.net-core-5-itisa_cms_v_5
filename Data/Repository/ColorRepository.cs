﻿using Data.Context;

using Domain.DomainClasses.Color;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repository
{
    public class ColorRepository : IColorRepository
    {
        private ApplicationDbContext _db;
        public ColorRepository(ApplicationDbContext db)
        {
            _db = db;
        }

        public void Delete(Color color)
        {
            _db.Color_tb.Remove(color);
        }

        public async Task<Color> GetAsync(Expression<Func<Color, bool>> where = null, Func<IQueryable<Color>, IOrderedQueryable<Color>> orderBy = null, Func<IQueryable<Color>, IIncludableQueryable<Color, object>> include = null, bool tracking = false)
        {
            IQueryable<Color> query = _db.Set<Color>();
            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }


            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().FirstOrDefaultAsync();
                else
                    return await orderBy(query).FirstOrDefaultAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().FirstOrDefaultAsync();
                else
                    return await query.FirstOrDefaultAsync();
            }
        }

        public async Task InsertColor(Color color)
        {
           await _db.Color_tb.AddAsync(color);
        }

        public async Task Save()
        {
           await _db.SaveChangesAsync();
        }

        public void UpdateColor(Color color)
        {
             _db.Color_tb.Update(color);
        }
    }
}
