﻿using Data.Context;

using Domain.DomainClasses.Region;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repository
{
    public class CountryRepository : ICountryRepository
    {
        private ApplicationDbContext _db;
        public CountryRepository(ApplicationDbContext db)
        {
            _db = db;
        }
        public void Delete(Country country)
        {
            _db.Country_tb.Remove(country);
        }

        public void DeleteAll(IEnumerable<Country> countries)
        {
            _db.Country_tb.RemoveRange(countries);
        }

        public async Task<bool> ExistsAsync(Expression<Func<Country, bool>> where = null, Func<IQueryable<Country>, IIncludableQueryable<Country, object>> include = null)
        {
            IQueryable<Country> query = _db.Set<Country>();

            if (include != null)
            {
                query = include(query);
            }

            query = query.Where(where);

            if (await query.CountAsync() > 0)
                return true;
            else
                return false;
        }

        public async Task<IEnumerable<Country>> GetAllAsyn(Expression<Func<Country, bool>> where = null, Func<IQueryable<Country>, IOrderedQueryable<Country>> orderBy = null, Func<IQueryable<Country>, IIncludableQueryable<Country, object>> include = null, bool tracking = false)
        {
            IQueryable<Country> query = _db.Set<Country>();

            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }
            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().ToListAsync();
                else
                    return await orderBy(query).ToListAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().ToListAsync();
                else
                    return await query.ToListAsync();
            }
        }

        public async Task<Country> GetAsync(Expression<Func<Country, bool>> where = null, Func<IQueryable<Country>, IOrderedQueryable<Country>> orderBy = null, Func<IQueryable<Country>, IIncludableQueryable<Country, object>> include = null, bool tracking = false)
        {
            IQueryable<Country> query = _db.Set<Country>();
            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }


            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().FirstOrDefaultAsync();
                else
                    return await orderBy(query).FirstOrDefaultAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().FirstOrDefaultAsync();
                else
                    return await query.FirstOrDefaultAsync();
            }
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<Country, TResult>> selector = null, Expression<Func<Country, bool>> where = null, Func<IQueryable<Country>, IOrderedQueryable<Country>> orderBy = null, Func<IQueryable<Country>, IIncludableQueryable<Country, object>> include = null, bool disableTracking = true)
        {
            IQueryable<Country> query = _db.Set<Country>();


            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }

            if (orderBy != null)
            {
                return await orderBy(query).Select(selector).ToListAsync();
            }
            else
            {
                return await query.AsNoTracking().Select(selector).ToListAsync();
            }
        }

        public async Task Insert(Country country)
        {
            await _db.Country_tb.AddAsync(country);
        }

        public async Task Save()
        {
            await _db.SaveChangesAsync();
        }

        public async Task Update(Country country)
        {
            _db.Country_tb.Update(country);
        }
    }
}
