﻿using Data.Context;

using Domain.DomainClasses.SendType;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repository
{
    public class SendTypeTranslationRepository : ISendTypeTranslationRepository
    {
        private ApplicationDbContext _db;
        public SendTypeTranslationRepository(ApplicationDbContext db)
        {
            _db = db;
        }
        public void Delete(SendTypeTranslation sendTypeTranslation)
        {
            _db.SendTypeTranslation_tb.Remove(sendTypeTranslation);
        }

        public void DeleteAll(IEnumerable<SendTypeTranslation> sendTypeTranslations)
        {
            _db.SendTypeTranslation_tb.RemoveRange(sendTypeTranslations);
        }

        public async Task<bool> ExistsAsync(Expression<Func<SendTypeTranslation, bool>> where = null, Func<IQueryable<SendTypeTranslation>, IIncludableQueryable<SendTypeTranslation, object>> include = null)
        {
            IQueryable<SendTypeTranslation> query = _db.Set<SendTypeTranslation>();

            if (include != null)
            {
                query = include(query);
            }

            query = query.Where(where);

            if (await query.CountAsync() > 0)
                return true;
            else
                return false;
        }

        public async Task<IEnumerable<SendTypeTranslation>> GetAllAsyn(Expression<Func<SendTypeTranslation, bool>> where = null, Func<IQueryable<SendTypeTranslation>, IOrderedQueryable<SendTypeTranslation>> orderBy = null, Func<IQueryable<SendTypeTranslation>, IIncludableQueryable<SendTypeTranslation, object>> include = null, bool tracking = false)
        {
            IQueryable<SendTypeTranslation> query = _db.Set<SendTypeTranslation>();

            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }
            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().ToListAsync();
                else
                    return await orderBy(query).ToListAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().ToListAsync();
                else
                    return await query.ToListAsync();
            }
        }

        public async Task<SendTypeTranslation> GetAsync(Expression<Func<SendTypeTranslation, bool>> where = null, Func<IQueryable<SendTypeTranslation>, IOrderedQueryable<SendTypeTranslation>> orderBy = null, Func<IQueryable<SendTypeTranslation>, IIncludableQueryable<SendTypeTranslation, object>> include = null, bool tracking = false)
        {
            IQueryable<SendTypeTranslation> query = _db.Set<SendTypeTranslation>();
            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }


            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().FirstOrDefaultAsync();
                else
                    return await orderBy(query).FirstOrDefaultAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().FirstOrDefaultAsync();
                else
                    return await query.FirstOrDefaultAsync();
            }
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<SendTypeTranslation, TResult>> selector = null, Expression<Func<SendTypeTranslation, bool>> where = null, Func<IQueryable<SendTypeTranslation>, IOrderedQueryable<SendTypeTranslation>> orderBy = null, Func<IQueryable<SendTypeTranslation>, IIncludableQueryable<SendTypeTranslation, object>> include = null, bool disableTracking = true)
        {
            IQueryable<SendTypeTranslation> query = _db.Set<SendTypeTranslation>();


            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }

            if (orderBy != null)
            {
                return await orderBy(query).Select(selector).ToListAsync();
            }
            else
            {
                return await query.Select(selector).ToListAsync();
            }
        }

        public async Task Insert(SendTypeTranslation sendTypeTranslation)
        {
            await _db.SendTypeTranslation_tb.AddAsync(sendTypeTranslation);
        }

        public async Task Save()
        {
            await _db.SaveChangesAsync();
        }

        public async Task Update(SendTypeTranslation sendTypeTranslation)
        {
            _db.SendTypeTranslation_tb.Update(sendTypeTranslation);
        }
    }
}
