﻿using Data.Context;

using Domain.DomainClasses.Feature;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repository
{
    public class FeatureRepository : IFeatureRepository
    {
        private ApplicationDbContext _db;
        public FeatureRepository(ApplicationDbContext db)
        {
            _db = db;
        }
        public void Delete(Feature feature)
        {
            _db.Feature_tb.Remove(feature);
        }

        public async Task<bool> ExistsAsync(Expression<Func<Feature, bool>> where = null, Func<IQueryable<Feature>, IIncludableQueryable<Feature, object>> include = null)
        {
            IQueryable<Feature> query = _db.Set<Feature>();

            if (include != null)
            {
                query = include(query);
            }

            query = query.Where(where);

            if (await query.CountAsync() > 0)
                return true;
            else
                return false;
        }

        public async Task<IEnumerable<Feature>> GetAllAsyn(Expression<Func<Feature, bool>> where = null, Func<IQueryable<Feature>, IOrderedQueryable<Feature>> orderBy = null, Func<IQueryable<Feature>, IIncludableQueryable<Feature, object>> include = null, bool tracking = false)
        {
            IQueryable<Feature> query = _db.Set<Feature>();

            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }
            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().ToListAsync();
                else
                    return await orderBy(query).ToListAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().ToListAsync();
                else
                    return await query.ToListAsync();
            }
        }

        public async Task<Feature> GetAsync(Expression<Func<Feature, bool>> where = null, Func<IQueryable<Feature>, IOrderedQueryable<Feature>> orderBy = null, Func<IQueryable<Feature>, IIncludableQueryable<Feature, object>> include = null, bool tracking = false)
        {
            IQueryable<Feature> query = _db.Set<Feature>();
            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }
            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().FirstOrDefaultAsync();
                else
                    return await orderBy(query).FirstOrDefaultAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().FirstOrDefaultAsync();
                else
                    return await query.FirstOrDefaultAsync();
            }
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<Feature, TResult>> selector = null, Expression<Func<Feature, bool>> where = null, Func<IQueryable<Feature>, IOrderedQueryable<Feature>> orderBy = null, Func<IQueryable<Feature>, IIncludableQueryable<Feature, object>> include = null, bool disableTracking = true)
        {
            IQueryable<Feature> query = _db.Set<Feature>();
            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }

            if (orderBy != null)
            {
                return await orderBy(query).Select(selector).ToListAsync();
            }
            else
            {
                return await query.Select(selector).ToListAsync();
            }
        }

        public async Task Insert(Feature feature)
        {
            await _db.Feature_tb.AddAsync(feature);
        }

        public async Task Save()
        {
            await _db.SaveChangesAsync();
        }

        public void Update(Feature feature)
        {
            _db.Feature_tb.Update(feature);
        }
    }
}
