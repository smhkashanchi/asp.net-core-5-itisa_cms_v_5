﻿using Data.Context;

using Domain.DomainClasses.CoWorker;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repository
{
    public class CoWorkerRepository : ICoWorkerRepository
    {
        private ApplicationDbContext _db;
        public CoWorkerRepository(ApplicationDbContext db)
        {
            _db = db;
        }
        public void DeleteAsync(CoWorker coWorker)
        {
            _db.CoWorker_tb.Remove(coWorker);
        }

        public async Task<bool> ExistsAsync(Expression<Func<CoWorker, bool>> where = null, Func<IQueryable<CoWorker>, IIncludableQueryable<CoWorker, object>> include = null)
        {
            IQueryable<CoWorker> query = _db.Set<CoWorker>();

            if (include != null)
            {
                query = include(query);
            }

            query = query.Where(where);

            if (await query.CountAsync() > 0)
                return true;
            else
                return false;
        }

        public async Task<IEnumerable<CoWorker>> GetAllAsyn(Expression<Func<CoWorker, bool>> where = null, Func<IQueryable<CoWorker>, IOrderedQueryable<CoWorker>> orderBy = null, Func<IQueryable<CoWorker>, IIncludableQueryable<CoWorker, object>> include = null, bool tracking = false)
        {
            IQueryable<CoWorker> query = _db.Set<CoWorker>();

            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }
            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().ToListAsync();
                else
                    return await orderBy(query).ToListAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().ToListAsync();
                else
                    return await query.ToListAsync();
            }
        }

        public async Task<CoWorker> GetAsync(Expression<Func<CoWorker, bool>> where = null, Func<IQueryable<CoWorker>, IOrderedQueryable<CoWorker>> orderBy = null, Func<IQueryable<CoWorker>, IIncludableQueryable<CoWorker, object>> include = null, bool tracking = false)
        {
            IQueryable<CoWorker> query = _db.Set<CoWorker>();
            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }


            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().FirstOrDefaultAsync();
                else
                    return await orderBy(query).FirstOrDefaultAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().FirstOrDefaultAsync();
                else
                    return await query.FirstOrDefaultAsync();
            }
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<CoWorker, TResult>> selector = null, Expression<Func<CoWorker, bool>> where = null, Func<IQueryable<CoWorker>, IOrderedQueryable<CoWorker>> orderBy = null, Func<IQueryable<CoWorker>, IIncludableQueryable<CoWorker, object>> include = null, bool disableTracking = true)
        {
            IQueryable<CoWorker> query = _db.Set<CoWorker>();


            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }

            if (orderBy != null)
            {
                return await orderBy(query).Select(selector).ToListAsync();
            }
            else
            {
                return await query.Select(selector).ToListAsync();
            }
        }

        public async Task Insert(CoWorker coWorker)
        {
            await _db.CoWorker_tb.AddAsync(coWorker);
        }

        public async Task Save()
        {
           await _db.SaveChangesAsync();
        }

        public async Task Update(CoWorker coWorker)
        {
            _db.CoWorker_tb.Update(coWorker);
        }
    }
}
