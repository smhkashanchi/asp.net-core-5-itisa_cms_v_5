﻿using Data.Context;

using Domain.DomainClasses.SocialNetwork;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repository
{
    public class SocialNetworkTranslationRepository : ISocialNetworkTranslationRepository
    {
        private ApplicationDbContext _db;
        public SocialNetworkTranslationRepository(ApplicationDbContext db)
        {
            _db = db;
        }

        public void DeleteAllAsync(IEnumerable<SocialNetworkTranslation> SocialNetworkTranslations)
        {
            var local = _db.Set<SocialNetworkTranslation>().Local
               .FirstOrDefault(entry => entry.SocialNetworkTranslation_ID
               .Equals(SocialNetworkTranslations.FirstOrDefault().SocialNetworkTranslation_ID));
            if (local != null)
            {
                _db.Entry(local).State = EntityState.Detached;
            }
            _db.SocialNetworkTranslation_tb.RemoveRange(SocialNetworkTranslations);
        }

        public void DeleteAsync(SocialNetworkTranslation SocialNetworkTranslation)
        {
            _db.SocialNetworkTranslation_tb.Remove(SocialNetworkTranslation);
        }

        public async Task<bool> ExistsAsync(Expression<Func<SocialNetworkTranslation, bool>> where = null, Func<IQueryable<SocialNetworkTranslation>, IIncludableQueryable<SocialNetworkTranslation, object>> include = null)
        {
            IQueryable<SocialNetworkTranslation> query = _db.Set<SocialNetworkTranslation>();

            if (include != null)
            {
                query = include(query);
            }

            query = query.Where(where);

            if (await query.CountAsync() > 0)
                return true;
            else
                return false;
        }

        public async Task<IEnumerable<SocialNetworkTranslation>> GetAllAsyn(Expression<Func<SocialNetworkTranslation, bool>> where = null, Func<IQueryable<SocialNetworkTranslation>, IOrderedQueryable<SocialNetworkTranslation>> orderBy = null, Func<IQueryable<SocialNetworkTranslation>, IIncludableQueryable<SocialNetworkTranslation, object>> include = null, bool tracking = false)
        {

            IQueryable<SocialNetworkTranslation> query = _db.Set<SocialNetworkTranslation>();

            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }
            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().ToListAsync();
                else
                    return await orderBy(query).ToListAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().ToListAsync();
                else
                    return await query.ToListAsync();
            }
        }

        public async Task<SocialNetworkTranslation> GetAsync(Expression<Func<SocialNetworkTranslation, bool>> where = null, Func<IQueryable<SocialNetworkTranslation>, IOrderedQueryable<SocialNetworkTranslation>> orderBy = null, Func<IQueryable<SocialNetworkTranslation>, IIncludableQueryable<SocialNetworkTranslation, object>> include = null, bool tracking = false)
        {

            IQueryable<SocialNetworkTranslation> query = _db.Set<SocialNetworkTranslation>();
            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }


            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().FirstOrDefaultAsync();
                else
                    return await orderBy(query).FirstOrDefaultAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().FirstOrDefaultAsync();
                else
                    return await query.FirstOrDefaultAsync();
            }
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<SocialNetworkTranslation, TResult>> selector = null, Expression<Func<SocialNetworkTranslation, bool>> where = null, Func<IQueryable<SocialNetworkTranslation>, IOrderedQueryable<SocialNetworkTranslation>> orderBy = null, Func<IQueryable<SocialNetworkTranslation>, IIncludableQueryable<SocialNetworkTranslation, object>> include = null, bool disableTracking = true)
        {
            IQueryable<SocialNetworkTranslation> query = _db.Set<SocialNetworkTranslation>();


            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }

            if (orderBy != null)
            {
                return await orderBy(query).Select(selector).ToListAsync();
            }
            else
            {
                return await query.AsNoTracking().Select(selector).ToListAsync();
            }
        }

        public async Task Insert(SocialNetworkTranslation SocialNetworkTranslation)
        {
            await _db.SocialNetworkTranslation_tb.AddAsync(SocialNetworkTranslation);
        }

        public async Task Save()
        {
            await _db.SaveChangesAsync();
        }

        public async Task Update(SocialNetworkTranslation SocialNetworkTranslation)
        {
            _db.SocialNetworkTranslation_tb.Update(SocialNetworkTranslation);
        }
    }
}
