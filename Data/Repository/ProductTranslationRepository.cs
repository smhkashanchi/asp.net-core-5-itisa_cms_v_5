﻿using Data.Context;

using Domain.DomainClasses.Product;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repository
{
    public class ProductTranslationRepository : IProductTranslationRepository
    {
        private ApplicationDbContext _db;
        public ProductTranslationRepository(ApplicationDbContext db)
        {
            _db = db;
        }
        public void Delete(ProductTranslation productTranslation)
        {
            var local = _db.Set<Product>().Local
               .FirstOrDefault(entry => entry.Product_ID
               .Equals(productTranslation.Product_Id));

            if (local != null)
            {
                _db.Entry(local).State = EntityState.Detached;
            }
            _db.Entry(productTranslation).State = EntityState.Deleted;

            //_db.ProductTranslation_tb.Remove(productTranslation);
        }

        public void DeleteAll(IEnumerable<ProductTranslation> productTranslations)
        {
            _db.ProductTranslation_tb.RemoveRange(productTranslations);
        }

        public async Task<bool> ExistsAsync(Expression<Func<ProductTranslation, bool>> where = null, Func<IQueryable<ProductTranslation>, IIncludableQueryable<ProductTranslation, object>> include = null)
        {
            IQueryable<ProductTranslation> query = _db.Set<ProductTranslation>();

            if (include != null)
            {
                query = include(query);
            }

            query = query.Where(where);

            if (await query.CountAsync() > 0)
                return true;
            else
                return false;
        }

        public async Task<IEnumerable<ProductTranslation>> GetAllAsyn(Expression<Func<ProductTranslation, bool>> where = null, Func<IQueryable<ProductTranslation>, IOrderedQueryable<ProductTranslation>> orderBy = null, Func<IQueryable<ProductTranslation>, IIncludableQueryable<ProductTranslation, object>> include = null, bool tracking = false)
        {
            IQueryable<ProductTranslation> query = _db.Set<ProductTranslation>();

            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }
            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().ToListAsync();
                else
                    return await orderBy(query).ToListAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().ToListAsync();
                else
                    return await query.ToListAsync();
            }
        }

        public async Task<ProductTranslation> GetAsync(Expression<Func<ProductTranslation, bool>> where = null, Func<IQueryable<ProductTranslation>, IOrderedQueryable<ProductTranslation>> orderBy = null, Func<IQueryable<ProductTranslation>, IIncludableQueryable<ProductTranslation, object>> include = null, bool tracking = false)
        {
            IQueryable<ProductTranslation> query = _db.Set<ProductTranslation>();
            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }


            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().FirstOrDefaultAsync();
                else
                    return await orderBy(query).FirstOrDefaultAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().FirstOrDefaultAsync();
                else
                    return await query.FirstOrDefaultAsync();
            }
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<ProductTranslation, TResult>> selector = null, Expression<Func<ProductTranslation, bool>> where = null, Func<IQueryable<ProductTranslation>, IOrderedQueryable<ProductTranslation>> orderBy = null, Func<IQueryable<ProductTranslation>, IIncludableQueryable<ProductTranslation, object>> include = null, bool disableTracking = true)
        {
            IQueryable<ProductTranslation> query = _db.Set<ProductTranslation>();


            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }

            if (orderBy != null)
            {
                return await orderBy(query).Select(selector).ToListAsync();
            }
            else
            {
                return await query.AsNoTracking().Select(selector).ToListAsync();
            }
        }

        public async Task Insert(ProductTranslation productTranslation)
        {
            await _db.ProductTranslation_tb.AddAsync(productTranslation);
        }

        public async Task Save()
        {
            await _db.SaveChangesAsync();
        }

        public void Update(ProductTranslation productTranslation)
        {
             Detach(productTranslation);
        }

        public void Detach(ProductTranslation productTranslation)
        {
            var local = _db.Set<ProductTranslation>().Local
                .FirstOrDefault(entry => entry.ProductTranslation_ID
                .Equals(productTranslation.ProductTranslation_ID));

            if (local != null)
            {
                _db.Entry(local).State = EntityState.Detached;
            }
            _db.Entry(productTranslation).State = EntityState.Modified;
        }

        public async Task InsertRange(IEnumerable<ProductTranslation> productTranslations)
        {
            await _db.ProductTranslation_tb.AddRangeAsync(productTranslations);
        }
    }
}
