﻿using Data.Context;

using Domain.DomainClasses.Product;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repository
{
    public class ProductFeatureRepository : IProductFeatureRepository
    {
        private ApplicationDbContext _db;
        public ProductFeatureRepository(ApplicationDbContext db)
        {
            _db = db;
        }
        public void DeleteRange(List<ProductFeature> productGroupFeatures)
        {
            _db.ProductFeature_tb.RemoveRange(productGroupFeatures);
        }

        public async Task<bool> ExistsAsync(Expression<Func<ProductFeature, bool>> where = null, Func<IQueryable<ProductFeature>, IIncludableQueryable<ProductFeature, object>> include = null)
        {
            IQueryable<ProductFeature> query = _db.Set<ProductFeature>();

            if (include != null)
            {
                query = include(query);
            }

            query = query.Where(where);

            if (await query.CountAsync() > 0)
                return true;
            else
                return false;
        }

        public async Task<IEnumerable<ProductFeature>> GetAllAsyn(Expression<Func<ProductFeature, bool>> where = null, Func<IQueryable<ProductFeature>, IOrderedQueryable<ProductFeature>> orderBy = null, Func<IQueryable<ProductFeature>, IIncludableQueryable<ProductFeature, object>> include = null, bool tracking = false)
        {
            IQueryable<ProductFeature> query = _db.Set<ProductFeature>();

            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }
            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().ToListAsync();
                else
                    return await orderBy(query).ToListAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().ToListAsync();
                else
                    return await query.ToListAsync();
            }
        }

        public async Task<ProductFeature> GetAsync(Expression<Func<ProductFeature, bool>> where = null, Func<IQueryable<ProductFeature>, IOrderedQueryable<ProductFeature>> orderBy = null, Func<IQueryable<ProductFeature>, IIncludableQueryable<ProductFeature, object>> include = null, bool tracking = false)
        {
            IQueryable<ProductFeature> query = _db.Set<ProductFeature>();
            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }
            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().FirstOrDefaultAsync();
                else
                    return await orderBy(query).FirstOrDefaultAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().FirstOrDefaultAsync();
                else
                    return await query.FirstOrDefaultAsync();
            }
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<ProductFeature, TResult>> selector = null, Expression<Func<ProductFeature, bool>> where = null, Func<IQueryable<ProductFeature>, IOrderedQueryable<ProductFeature>> orderBy = null, Func<IQueryable<ProductFeature>, IIncludableQueryable<ProductFeature, object>> include = null, bool disableTracking = true)
        {
            IQueryable<ProductFeature> query = _db.Set<ProductFeature>();
            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }

            if (orderBy != null)
            {
                return await orderBy(query).Select(selector).ToListAsync();
            }
            else
            {
                return await query.Select(selector).ToListAsync();
            }
        }

        public async Task InsertRange(List<ProductFeature> productGroupFeatures)
        {
            await _db.ProductFeature_tb.AddRangeAsync(productGroupFeatures);
        }

        public async Task Insert(ProductFeature productGroupFeature)
        {
            await _db.AddAsync(productGroupFeature);
        }

        public async Task Save()
        {
            await _db.SaveChangesAsync();
        }
    }
}
