﻿using Data.Context;

using Domain.DomainClasses.FAQ;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repository
{
    public class FAQRepository : IFAQRepository
    {
        private ApplicationDbContext _db;

        public FAQRepository(ApplicationDbContext db)
        {
            _db = db;
        }
        public void Delete(FAQ fAQ)
        {
            _db.FAQ_tb.Remove(fAQ);
        }

        public void DeleteAll(IEnumerable<FAQ> fAQs)
        {
            _db.FAQ_tb.RemoveRange(fAQs);
        }

        public async Task<bool> ExistsAsync(Expression<Func<FAQ, bool>> where = null, Func<IQueryable<FAQ>, IIncludableQueryable<FAQ, object>> include = null)
        {
            IQueryable<FAQ> query = _db.Set<FAQ>();

            if (include != null)
            {
                query = include(query);
            }

            query = query.Where(where);

            if (await query.CountAsync() > 0)
                return true;
            else
                return false;
        }

        public async Task<IEnumerable<FAQ>> GetAllAsyn(Expression<Func<FAQ, bool>> where = null, Func<IQueryable<FAQ>, IOrderedQueryable<FAQ>> orderBy = null, Func<IQueryable<FAQ>, IIncludableQueryable<FAQ, object>> include = null, bool tracking = false)
        {
            IQueryable<FAQ> query = _db.Set<FAQ>();

            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }
            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().ToListAsync();
                else
                    return await orderBy(query).ToListAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().ToListAsync();
                else
                    return await query.ToListAsync();
            }
        }

        public async Task<FAQ> GetAsync(Expression<Func<FAQ, bool>> where = null, Func<IQueryable<FAQ>, IOrderedQueryable<FAQ>> orderBy = null, Func<IQueryable<FAQ>, IIncludableQueryable<FAQ, object>> include = null, bool tracking = false)
        {
            IQueryable<FAQ> query = _db.Set<FAQ>();
            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }


            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().FirstOrDefaultAsync();
                else
                    return await orderBy(query).FirstOrDefaultAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().FirstOrDefaultAsync();
                else
                    return await query.FirstOrDefaultAsync();
            }
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<FAQ, TResult>> selector = null, Expression<Func<FAQ, bool>> where = null, Func<IQueryable<FAQ>, IOrderedQueryable<FAQ>> orderBy = null, Func<IQueryable<FAQ>, IIncludableQueryable<FAQ, object>> include = null, bool disableTracking = true)
        {
            IQueryable<FAQ> query = _db.Set<FAQ>();


            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }

            if (orderBy != null)
            {
                return await orderBy(query).Select(selector).ToListAsync();
            }
            else
            {
                return await query.AsNoTracking().Select(selector).ToListAsync();
            }
        }

        public async Task Insert(FAQ fAQ)
        {
            await _db.FAQ_tb.AddAsync(fAQ);
        }

        public async Task Save()
        {
            await _db.SaveChangesAsync();
        }

        public async Task Update(FAQ fAQ)
        {
            _db.FAQ_tb.Update(fAQ);
        }
    }
}
