﻿using Data.Context;

using Domain.DomainClasses.Product;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repository
{
    public class DiscountRepository : IDiscountRepository
    {
        private ApplicationDbContext _db;
        public DiscountRepository(ApplicationDbContext db)
        {
            _db = db;
        }
        public void Delete(Discount discount)
        {
            _db.Discount_tb.Remove(discount);
        }

        public void DeleteAll(IEnumerable<Discount> discounts)
        {
            _db.Discount_tb.RemoveRange(discounts);
        }

        public async Task<bool> ExistsAsync(Expression<Func<Discount, bool>> where = null, Func<IQueryable<Discount>, IIncludableQueryable<Discount, object>> include = null)
        {
            IQueryable<Discount> query = _db.Set<Discount>();

            if (include != null)
            {
                query = include(query);
            }

            query = query.Where(where);

            if (await query.CountAsync() > 0)
                return true;
            else
                return false;
        }

        public async Task<IEnumerable<Discount>> GetAllAsyn(Expression<Func<Discount, bool>> where = null, Func<IQueryable<Discount>, IOrderedQueryable<Discount>> orderBy = null, Func<IQueryable<Discount>, IIncludableQueryable<Discount, object>> include = null, bool tracking = false)
        {
            IQueryable<Discount> query = _db.Set<Discount>();

            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }
            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().ToListAsync();
                else
                    return await orderBy(query).ToListAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().ToListAsync();
                else
                    return await query.ToListAsync();
            }
        }

        public async Task<Discount> GetAsync(Expression<Func<Discount, bool>> where = null, Func<IQueryable<Discount>, IOrderedQueryable<Discount>> orderBy = null, Func<IQueryable<Discount>, IIncludableQueryable<Discount, object>> include = null, bool tracking = false)
        {
            IQueryable<Discount> query = _db.Set<Discount>();
            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }


            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().FirstOrDefaultAsync();
                else
                    return await orderBy(query).FirstOrDefaultAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().FirstOrDefaultAsync();
                else
                    return await query.FirstOrDefaultAsync();
            }
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<Discount, TResult>> selector = null, Expression<Func<Discount, bool>> where = null, Func<IQueryable<Discount>, IOrderedQueryable<Discount>> orderBy = null, Func<IQueryable<Discount>, IIncludableQueryable<Discount, object>> include = null, bool disableTracking = true)
        {
            IQueryable<Discount> query = _db.Set<Discount>();


            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }

            if (orderBy != null)
            {
                return await orderBy(query).Select(selector).ToListAsync();
            }
            else
            {
                return await query.AsNoTracking().Select(selector).ToListAsync();
            }
        }

        public async Task Insert(Discount discount)
        {
            await _db.Discount_tb.AddAsync(discount);
        }

        public async Task Save()
        {
            await _db.SaveChangesAsync();
        }

        public async Task Update(Discount discount)
        {
            _db.Discount_tb.Update(discount);
        }
    }
}
