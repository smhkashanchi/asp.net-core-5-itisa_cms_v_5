﻿using Data.Context;

using Domain.DomainClasses.Gallery;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repository
{
    public class GalleryPictureRepository : IGalleryPictureRepository
    {
        private ApplicationDbContext _db;
        public GalleryPictureRepository(ApplicationDbContext db)
        {
            _db = db;
        }
        public void DeleteAsync(GalleryPicture galleryPicture)
        {
            _db.GalleryPicture_tb.Remove(galleryPicture);
        }

        public async Task<bool> ExistsAsync(Expression<Func<GalleryPicture, bool>> where = null, Func<IQueryable<GalleryPicture>, IIncludableQueryable<GalleryPicture, object>> include = null)
        {
            IQueryable<GalleryPicture> query = _db.Set<GalleryPicture>();

            if (include != null)
            {
                query = include(query);
            }

            query = query.Where(where);

            if (await query.CountAsync() > 0)
                return true;
            else
                return false;
        }

        public async Task<IEnumerable<GalleryPicture>> GetAllAsyn(Expression<Func<GalleryPicture, bool>> where = null, Func<IQueryable<GalleryPicture>, IOrderedQueryable<GalleryPicture>> orderBy = null, Func<IQueryable<GalleryPicture>, IIncludableQueryable<GalleryPicture, object>> include = null, bool tracking = false)
        {
            IQueryable<GalleryPicture> query = _db.Set<GalleryPicture>();

            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }
            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().ToListAsync();
                else
                    return await orderBy(query).ToListAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().ToListAsync();
                else
                    return await query.ToListAsync();
            }
        }

        public async Task<GalleryPicture> GetAsync(Expression<Func<GalleryPicture, bool>> where = null, Func<IQueryable<GalleryPicture>, IOrderedQueryable<GalleryPicture>> orderBy = null, Func<IQueryable<GalleryPicture>, IIncludableQueryable<GalleryPicture, object>> include = null, bool tracking = false)
        {
            IQueryable<GalleryPicture> query = _db.Set<GalleryPicture>();
            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }


            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().FirstOrDefaultAsync();
                else
                    return await orderBy(query).FirstOrDefaultAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().FirstOrDefaultAsync();
                else
                    return await query.FirstOrDefaultAsync();
            }
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<GalleryPicture, TResult>> selector = null, Expression<Func<GalleryPicture, bool>> where = null, Func<IQueryable<GalleryPicture>, IOrderedQueryable<GalleryPicture>> orderBy = null, Func<IQueryable<GalleryPicture>, IIncludableQueryable<GalleryPicture, object>> include = null, bool disableTracking = true)
        {
            IQueryable<GalleryPicture> query = _db.Set<GalleryPicture>();


            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }

            if (orderBy != null)
            {
                return await orderBy(query).Select(selector).ToListAsync();
            }
            else
            {
                return await query.Select(selector).ToListAsync();
            }
        }

        public async Task Insert(GalleryPicture galleryPicture)
        {
            await _db.GalleryPicture_tb.AddAsync(galleryPicture);
        }

        public async Task Save()
        {
            await _db.SaveChangesAsync();
        }

        public void Update(GalleryPicture galleryPicture)
        {
            _db.GalleryPicture_tb.Update(galleryPicture);
        }
    }
}
