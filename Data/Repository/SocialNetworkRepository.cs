﻿using Data.Context;

using Domain.DomainClasses.SocialNetwork;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repository
{
    public class SocialNetworkRepository : ISocialNetworkRepository
    {
        private ApplicationDbContext _db;
        public SocialNetworkRepository(ApplicationDbContext db)
        {
            _db = db;
        }
        public void DeleteAsync(SocialNetwork socialNetwork)
        {
            _db.SocialNetwork_tb.Remove(socialNetwork);
        }

        public async Task<bool> ExistsAsync(Expression<Func<SocialNetwork, bool>> where = null, Func<IQueryable<SocialNetwork>, IIncludableQueryable<SocialNetwork, object>> include = null)
        {
            IQueryable<SocialNetwork> query = _db.Set<SocialNetwork>();

            if (include != null)
            {
                query = include(query);
            }

            query = query.Where(where);

            if (await query.CountAsync() > 0)
                return true;
            else
                return false;
        }

        public async Task<IEnumerable<SocialNetwork>> GetAllAsyn(Expression<Func<SocialNetwork, bool>> where = null, Func<IQueryable<SocialNetwork>, IOrderedQueryable<SocialNetwork>> orderBy = null, Func<IQueryable<SocialNetwork>, IIncludableQueryable<SocialNetwork, object>> include = null, bool tracking = false)
        {
            IQueryable<SocialNetwork> query = _db.Set<SocialNetwork>();

            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }
            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().ToListAsync();
                else
                    return await orderBy(query).ToListAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().ToListAsync();
                else
                    return await query.ToListAsync();
            }
        }

        public async Task<SocialNetwork> GetAsync(Expression<Func<SocialNetwork, bool>> where = null, Func<IQueryable<SocialNetwork>, IOrderedQueryable<SocialNetwork>> orderBy = null, Func<IQueryable<SocialNetwork>, IIncludableQueryable<SocialNetwork, object>> include = null, bool tracking = false)
        {
            IQueryable<SocialNetwork> query = _db.Set<SocialNetwork>();
            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }


            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().FirstOrDefaultAsync();
                else
                    return await orderBy(query).FirstOrDefaultAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().FirstOrDefaultAsync();
                else
                    return await query.FirstOrDefaultAsync();
            }
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<SocialNetwork, TResult>> selector = null, Expression<Func<SocialNetwork, bool>> where = null, Func<IQueryable<SocialNetwork>, IOrderedQueryable<SocialNetwork>> orderBy = null, Func<IQueryable<SocialNetwork>, IIncludableQueryable<SocialNetwork, object>> include = null, bool disableTracking = true)
        {
            IQueryable<SocialNetwork> query = _db.Set<SocialNetwork>();


            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }

            if (orderBy != null)
            {
                return await orderBy(query).Select(selector).ToListAsync();
            }
            else
            {
                return await query.Select(selector).ToListAsync();
            }
        }

        public async Task Insert(SocialNetwork socialNetwork)
        {
            await _db.SocialNetwork_tb.AddAsync(socialNetwork);
        }

        public async Task Save()
        {
            await _db.SaveChangesAsync();
        }

        public async Task Update(SocialNetwork socialNetwork)
        {
            _db.SocialNetwork_tb.Update(socialNetwork);
        }
    }
}
