﻿using Data.Context;

using Domain.DomainClasses.Connection;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repository
{
    public class ConnectionRepository : IConnectionRepository
    {
        private ApplicationDbContext _db;
        public ConnectionRepository(ApplicationDbContext db)
        {
            _db = db;
        }

        public void Delete(Connection connection)
        {
            _db.Connection_tb.Remove(connection);
        }

        public async Task<bool> ExistsAsync(Expression<Func<Connection, bool>> where = null, Func<IQueryable<Connection>, IIncludableQueryable<Connection, object>> include = null)
        {
            IQueryable<Connection> query = _db.Set<Connection>();

            if (include != null)
            {
                query = include(query);
            }

            query = query.Where(where);

            if (await query.CountAsync() > 0)
                return true;
            else
                return false;
        }

        public async Task<Connection> GetAsync(Expression<Func<Connection, bool>> where = null, Func<IQueryable<Connection>, IOrderedQueryable<Connection>> orderBy = null, Func<IQueryable<Connection>, IIncludableQueryable<Connection, object>> include = null, bool tracking = false)
        {
            IQueryable<Connection> query = _db.Set<Connection>();
            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }


            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().FirstOrDefaultAsync();
                else
                    return await orderBy(query).FirstOrDefaultAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().FirstOrDefaultAsync();
                else
                    return await query.FirstOrDefaultAsync();
            }
        }

        public async Task InsertConnection(Connection connection)
        {
            await _db.Connection_tb.AddAsync(connection);
        }

        public async Task Save()
        {
            await _db.SaveChangesAsync();
        }

        public void UpdateConnection(Connection connection)
        {
            _db.Connection_tb.Update(connection);
        }
    }
}
