﻿using Data.Context;

using Domain.DomainClasses.SendType;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repository
{
    public class SendTypeRepository : ISendTypeRepository
    {
        private ApplicationDbContext _db;
        public SendTypeRepository(ApplicationDbContext db)
        {
            _db = db;
        }
        public void DeleteAsync(SendType sendType)
        {
            _db.SendType_tb.Remove(sendType);
        }

        public async Task<bool> ExistsAsync(Expression<Func<SendType, bool>> where = null, Func<IQueryable<SendType>, IIncludableQueryable<SendType, object>> include = null)
        {
            IQueryable<SendType> query = _db.Set<SendType>();

            if (include != null)
            {
                query = include(query);
            }

            query = query.Where(where);

            if (await query.CountAsync() > 0)
                return true;
            else
                return false;
        }

        public async Task<IEnumerable<SendType>> GetAllAsyn(Expression<Func<SendType, bool>> where = null, Func<IQueryable<SendType>, IOrderedQueryable<SendType>> orderBy = null, Func<IQueryable<SendType>, IIncludableQueryable<SendType, object>> include = null, bool tracking = false)
        {
            IQueryable<SendType> query = _db.Set<SendType>();

            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }
            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().ToListAsync();
                else
                    return await orderBy(query).ToListAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().ToListAsync();
                else
                    return await query.ToListAsync();
            }
        }

        public async Task<SendType> GetAsync(Expression<Func<SendType, bool>> where = null, Func<IQueryable<SendType>, IOrderedQueryable<SendType>> orderBy = null, Func<IQueryable<SendType>, IIncludableQueryable<SendType, object>> include = null, bool tracking = false)
        {
            IQueryable<SendType> query = _db.Set<SendType>();
            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }


            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().FirstOrDefaultAsync();
                else
                    return await orderBy(query).FirstOrDefaultAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().FirstOrDefaultAsync();
                else
                    return await query.FirstOrDefaultAsync();
            }
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<SendType, TResult>> selector = null, Expression<Func<SendType, bool>> where = null, Func<IQueryable<SendType>, IOrderedQueryable<SendType>> orderBy = null, Func<IQueryable<SendType>, IIncludableQueryable<SendType, object>> include = null, bool disableTracking = true)
        {
            IQueryable<SendType> query = _db.Set<SendType>();


            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }

            if (orderBy != null)
            {
                return await orderBy(query).Select(selector).ToListAsync();
            }
            else
            {
                return await query.Select(selector).ToListAsync();
            }
        }

        public async Task Insert(SendType sendType)
        {
            await _db.SendType_tb.AddAsync(sendType);
        }

        public async Task Save()
        {
            await _db.SaveChangesAsync();
        }

        public async Task Update(SendType sendType)
        {
             _db.SendType_tb.Update(sendType);
        }
    }
}
