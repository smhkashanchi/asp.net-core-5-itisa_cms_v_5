﻿using Data.Context;

using Domain.DomainClasses.Feature;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repository
{
    public class FeatureReplyRepository : IFeatureReplyRepository
    {
        private ApplicationDbContext _db;
        public FeatureReplyRepository(ApplicationDbContext db)
        {
            _db = db;
        }
        public void Delete(FeatureReply featureReply)
        {
            _db.FeatureReply_tb.Remove(featureReply);
        }

        public async Task<bool> ExistsAsync(Expression<Func<FeatureReply, bool>> where = null, Func<IQueryable<FeatureReply>, IIncludableQueryable<FeatureReply, object>> include = null)
        {
            IQueryable<FeatureReply> query = _db.Set<FeatureReply>();

            if (include != null)
            {
                query = include(query);
            }

            query = query.Where(where);

            if (await query.CountAsync() > 0)
                return true;
            else
                return false;
        }

        public async Task<IEnumerable<FeatureReply>> GetAllAsyn(Expression<Func<FeatureReply, bool>> where = null, Func<IQueryable<FeatureReply>, IOrderedQueryable<FeatureReply>> orderBy = null, Func<IQueryable<FeatureReply>, IIncludableQueryable<FeatureReply, object>> include = null, bool tracking = false)
        {
            IQueryable<FeatureReply> query = _db.Set<FeatureReply>();

            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }
            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().ToListAsync();
                else
                    return await orderBy(query).ToListAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().ToListAsync();
                else
                    return await query.ToListAsync();
            }
        }

        public async Task<FeatureReply> GetAsync(Expression<Func<FeatureReply, bool>> where = null, Func<IQueryable<FeatureReply>, IOrderedQueryable<FeatureReply>> orderBy = null, Func<IQueryable<FeatureReply>, IIncludableQueryable<FeatureReply, object>> include = null, bool tracking = false)
        {
            IQueryable<FeatureReply> query = _db.Set<FeatureReply>();
            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }
            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().FirstOrDefaultAsync();
                else
                    return await orderBy(query).FirstOrDefaultAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().FirstOrDefaultAsync();
                else
                    return await query.FirstOrDefaultAsync();
            }
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<FeatureReply, TResult>> selector = null, Expression<Func<FeatureReply, bool>> where = null, Func<IQueryable<FeatureReply>, IOrderedQueryable<FeatureReply>> orderBy = null, Func<IQueryable<FeatureReply>, IIncludableQueryable<FeatureReply, object>> include = null, bool disableTracking = true)
        {
            IQueryable<FeatureReply> query = _db.Set<FeatureReply>();
            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }

            if (orderBy != null)
            {
                return await orderBy(query).Select(selector).ToListAsync();
            }
            else
            {
                return await query.Select(selector).ToListAsync();
            }
        }

        public async Task Insert(FeatureReply featureReply)
        {
            await _db.FeatureReply_tb.AddAsync(featureReply);
        }

        public async Task Save()
        {
            await _db.SaveChangesAsync();
        }

        public void Update(FeatureReply featureReply)
        {
            _db.FeatureReply_tb.Update(featureReply);
        }
    }
}
