﻿using Data.Context;

using Domain.DomainClasses.Color;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repository
{
    public class ColorTranslationRepository : IColorTranslationRepository
    {
        private ApplicationDbContext _db;
        public ColorTranslationRepository(ApplicationDbContext db)
        {
            _db = db;
        }

        public void DeleteAll(IEnumerable<ColorTranslation> colorsTranslation)
        {
            _db.ColorTranslation_tb.RemoveRange(colorsTranslation);
        }

        public void DeleteAsync(ColorTranslation colorTranslation)
        {
            _db.ColorTranslation_tb.Remove(colorTranslation);
        }

        public async Task<bool> ExistsAsync(Expression<Func<ColorTranslation, bool>> where = null, Func<IQueryable<ColorTranslation>, IIncludableQueryable<ColorTranslation, object>> include = null)
        {
            IQueryable<ColorTranslation> query = _db.Set<ColorTranslation>();

            if (include != null)
            {
                query = include(query);
            }

            query = query.Where(where);

            if (await query.CountAsync() > 0)
                return true;
            else
                return false;
        }

        public async Task<IEnumerable<ColorTranslation>> GetAllAsyn(Expression<Func<ColorTranslation, bool>> where = null, Func<IQueryable<ColorTranslation>, IOrderedQueryable<ColorTranslation>> orderBy = null, Func<IQueryable<ColorTranslation>, IIncludableQueryable<ColorTranslation, object>> include = null, bool tracking = false)
        {
            IQueryable<ColorTranslation> query = _db.Set<ColorTranslation>();

            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }
            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().ToListAsync();
                else
                    return await orderBy(query).ToListAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().ToListAsync();
                else
                    return await query.ToListAsync();
            }
        }

        public async Task<ColorTranslation> GetAsync(Expression<Func<ColorTranslation, bool>> where = null, Func<IQueryable<ColorTranslation>, IOrderedQueryable<ColorTranslation>> orderBy = null, Func<IQueryable<ColorTranslation>, IIncludableQueryable<ColorTranslation, object>> include = null, bool tracking = false)
        {
            IQueryable<ColorTranslation> query = _db.Set<ColorTranslation>();
            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }


            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().FirstOrDefaultAsync();
                else
                    return await orderBy(query).FirstOrDefaultAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().FirstOrDefaultAsync();
                else
                    return await query.FirstOrDefaultAsync();
            }
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<ColorTranslation, TResult>> selector = null, Expression<Func<ColorTranslation, bool>> where = null, Func<IQueryable<ColorTranslation>, IOrderedQueryable<ColorTranslation>> orderBy = null, Func<IQueryable<ColorTranslation>, IIncludableQueryable<ColorTranslation, object>> include = null, bool disableTracking = true)
        {
            IQueryable<ColorTranslation> query = _db.Set<ColorTranslation>();


            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }

            if (orderBy != null)
            {
                return await orderBy(query).Select(selector).ToListAsync();
            }
            else
            {
                return await query.Select(selector).ToListAsync();
            }
        }

        public async Task Insert(ColorTranslation colorTranslation)
        {
           await _db.ColorTranslation_tb.AddAsync(colorTranslation);
        }

        public async Task Save()
        {
           await _db.SaveChangesAsync();
        }

        public void Update(ColorTranslation colorTranslation)
        {
            _db.Entry(colorTranslation).State = EntityState.Modified;
        }
    }
}
