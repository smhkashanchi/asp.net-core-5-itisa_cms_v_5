﻿using Data.Context;

using Domain.DomainClasses.CoWorker;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repository
{
    public class CoWorkerGroupRepository : ICoWorkerGroupRepository
    {
        private ApplicationDbContext _db;
        public CoWorkerGroupRepository(ApplicationDbContext db)
        {
            _db = db;
        }
        public void DeleteAsync(CoWorkerGroup coWorkerGroup)
        {
            _db.CoWorkerGroup_tb.Remove(coWorkerGroup);
        }

        public async Task<bool> ExistsAsync(Expression<Func<CoWorkerGroup, bool>> where = null, Func<IQueryable<CoWorkerGroup>, IIncludableQueryable<CoWorkerGroup, object>> include = null)
        {
            IQueryable<CoWorkerGroup> query = _db.Set<CoWorkerGroup>();

            if (include != null)
            {
                query = include(query);
            }

            query = query.Where(where);

            if (await query.CountAsync() > 0)
                return true;
            else
                return false;
        }

        public async Task<IEnumerable<CoWorkerGroup>> GetAllAsyn(Expression<Func<CoWorkerGroup, bool>> where = null, Func<IQueryable<CoWorkerGroup>, IOrderedQueryable<CoWorkerGroup>> orderBy = null, Func<IQueryable<CoWorkerGroup>, IIncludableQueryable<CoWorkerGroup, object>> include = null, bool tracking = false)
        {
            IQueryable<CoWorkerGroup> query = _db.Set<CoWorkerGroup>();

            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }
            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().ToListAsync();
                else
                    return await orderBy(query).ToListAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().ToListAsync();
                else
                    return await query.ToListAsync();
            }
        }

        public async Task<CoWorkerGroup> GetAsync(Expression<Func<CoWorkerGroup, bool>> where = null, Func<IQueryable<CoWorkerGroup>, IOrderedQueryable<CoWorkerGroup>> orderBy = null, Func<IQueryable<CoWorkerGroup>, IIncludableQueryable<CoWorkerGroup, object>> include = null, bool tracking = false)
        {
            IQueryable<CoWorkerGroup> query = _db.Set<CoWorkerGroup>();
            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }


            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().FirstOrDefaultAsync();
                else
                    return await orderBy(query).FirstOrDefaultAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().FirstOrDefaultAsync();
                else
                    return await query.FirstOrDefaultAsync();
            }
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<CoWorkerGroup, TResult>> selector = null, Expression<Func<CoWorkerGroup, bool>> where = null, Func<IQueryable<CoWorkerGroup>, IOrderedQueryable<CoWorkerGroup>> orderBy = null, Func<IQueryable<CoWorkerGroup>, IIncludableQueryable<CoWorkerGroup, object>> include = null, bool disableTracking = true)
        {
            IQueryable<CoWorkerGroup> query = _db.Set<CoWorkerGroup>();


            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }

            if (orderBy != null)
            {
                return await orderBy(query).Select(selector).ToListAsync();
            }
            else
            {
                return await query.Select(selector).ToListAsync();
            }
        }

        public async Task Insert(CoWorkerGroup coWorkerGroup)
        {
            await _db.CoWorkerGroup_tb.AddAsync(coWorkerGroup);
        }

        public async Task Save()
        {
            await _db.SaveChangesAsync();
        }

        public async Task Update(CoWorkerGroup coWorkerGroup)
        {
            _db.CoWorkerGroup_tb.Update(coWorkerGroup);
        }
    }
}
