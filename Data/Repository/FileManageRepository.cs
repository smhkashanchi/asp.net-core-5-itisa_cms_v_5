﻿using Data.Context;

using Domain.DomainClasses.FilesManage;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repository
{
    public class FileManageRepository : IFileManageRepository
    {
        private ApplicationDbContext _db;
        public FileManageRepository(ApplicationDbContext db)
        {
            _db = db;
        }
        public void Delete(FileManage fileManage)
        {
            _db.FileManage_tb.Remove(fileManage);
        }

        public async Task<bool> ExistsAsync(Expression<Func<FileManage, bool>> where = null, Func<IQueryable<FileManage>, IIncludableQueryable<FileManage, object>> include = null)
        {
            IQueryable<FileManage> query = _db.Set<FileManage>();

            if (include != null)
            {
                query = include(query);
            }

            query = query.Where(where);

            if (await query.CountAsync() > 0)
                return true;
            else
                return false;
        }

        public async Task<IEnumerable<FileManage>> GetAllAsyn(Expression<Func<FileManage, bool>> where = null, Func<IQueryable<FileManage>, IOrderedQueryable<FileManage>> orderBy = null, Func<IQueryable<FileManage>, IIncludableQueryable<FileManage, object>> include = null, bool tracking = false)
        {
            IQueryable<FileManage> query = _db.Set<FileManage>();

            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }
            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().ToListAsync();
                else
                    return await orderBy(query).ToListAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().ToListAsync();
                else
                    return await query.ToListAsync();
            }
        }

        public async Task<FileManage> GetAsync(Expression<Func<FileManage, bool>> where = null, Func<IQueryable<FileManage>, IOrderedQueryable<FileManage>> orderBy = null, Func<IQueryable<FileManage>, IIncludableQueryable<FileManage, object>> include = null, bool tracking = false)
        {
            IQueryable<FileManage> query = _db.Set<FileManage>();
            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }
            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().FirstOrDefaultAsync();
                else
                    return await orderBy(query).FirstOrDefaultAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().FirstOrDefaultAsync();
                else
                    return await query.FirstOrDefaultAsync();
            }
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<FileManage, TResult>> selector = null, Expression<Func<FileManage, bool>> where = null, Func<IQueryable<FileManage>, IOrderedQueryable<FileManage>> orderBy = null, Func<IQueryable<FileManage>, IIncludableQueryable<FileManage, object>> include = null, bool disableTracking = true)
        {
            IQueryable<FileManage> query = _db.Set<FileManage>();
            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }

            if (orderBy != null)
            {
                return await orderBy(query).Select(selector).ToListAsync();
            }
            else
            {
                return await query.Select(selector).ToListAsync();
            }
        }

        public async Task Insert(FileManage fileManage)
        {
            await _db.FileManage_tb.AddAsync(fileManage);
        }

        public async Task Save()
        {
            await _db.SaveChangesAsync();
        }

        public void Update(FileManage fileManage)
        {
            _db.FileManage_tb.Update(fileManage);
        }
    }
}
