﻿using Data.Context;

using Domain.DomainClasses.CoWorker;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repository
{
    public class CoWorkerTranslationRepository : ICoWorkerTranslationRepository
    {
        private ApplicationDbContext _db;
        public CoWorkerTranslationRepository(ApplicationDbContext db)
        {
            _db = db;
        }
        public void Delete(CoWorkerTranslation coWorkerTranslation)
        {
            _db.CoWorkerTranslation_tb.Remove(coWorkerTranslation);
        }

        public void DeleteAll(IEnumerable<CoWorkerTranslation> coWorkerTranslations)
        {
            _db.CoWorkerTranslation_tb.RemoveRange(coWorkerTranslations);
        }

        public async Task<bool> ExistsAsync(Expression<Func<CoWorkerTranslation, bool>> where = null, Func<IQueryable<CoWorkerTranslation>, IIncludableQueryable<CoWorkerTranslation, object>> include = null)
        {
            IQueryable<CoWorkerTranslation> query = _db.Set<CoWorkerTranslation>();

            if (include != null)
            {
                query = include(query);
            }

            query = query.Where(where);

            if (await query.CountAsync() > 0)
                return true;
            else
                return false;
        }

        public async Task<IEnumerable<CoWorkerTranslation>> GetAllAsyn(Expression<Func<CoWorkerTranslation, bool>> where = null, Func<IQueryable<CoWorkerTranslation>, IOrderedQueryable<CoWorkerTranslation>> orderBy = null, Func<IQueryable<CoWorkerTranslation>, IIncludableQueryable<CoWorkerTranslation, object>> include = null, bool tracking = false)
        {
            IQueryable<CoWorkerTranslation> query = _db.Set<CoWorkerTranslation>();

            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }
            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().ToListAsync();
                else
                    return await orderBy(query).ToListAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().ToListAsync();
                else
                    return await query.ToListAsync();
            }
        }

        public async Task<CoWorkerTranslation> GetAsync(Expression<Func<CoWorkerTranslation, bool>> where = null, Func<IQueryable<CoWorkerTranslation>, IOrderedQueryable<CoWorkerTranslation>> orderBy = null, Func<IQueryable<CoWorkerTranslation>, IIncludableQueryable<CoWorkerTranslation, object>> include = null, bool tracking = false)
        {
            IQueryable<CoWorkerTranslation> query = _db.Set<CoWorkerTranslation>();
            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }


            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().FirstOrDefaultAsync();
                else
                    return await orderBy(query).FirstOrDefaultAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().FirstOrDefaultAsync();
                else
                    return await query.FirstOrDefaultAsync();
            }
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<CoWorkerTranslation, TResult>> selector = null, Expression<Func<CoWorkerTranslation, bool>> where = null, Func<IQueryable<CoWorkerTranslation>, IOrderedQueryable<CoWorkerTranslation>> orderBy = null, Func<IQueryable<CoWorkerTranslation>, IIncludableQueryable<CoWorkerTranslation, object>> include = null, bool disableTracking = true)
        {
            IQueryable<CoWorkerTranslation> query = _db.Set<CoWorkerTranslation>();
            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }

            if (orderBy != null)
            {
                return await orderBy(query).Select(selector).ToListAsync();
            }
            else
            {
                return await query.Select(selector).ToListAsync();
            }
        }

        public async Task Insert(CoWorkerTranslation coWorkerTranslation)
        {
            await _db.CoWorkerTranslation_tb.AddAsync(coWorkerTranslation);
        }

        public async Task Save()
        {
            await _db.SaveChangesAsync();
        }

        public async Task Update(CoWorkerTranslation coWorkerTranslation)
        {
            _db.CoWorkerTranslation_tb.Update(coWorkerTranslation);
        }
    }
}
