﻿using Data.Context;

using Domain.DomainClasses.FilesManage;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repository
{
    public class FileManageGroupTranslationRepository : IFileManageGroupTranslationRepository
    {
        private ApplicationDbContext _db;
        public FileManageGroupTranslationRepository(ApplicationDbContext db)
        {
            _db = db;
        }
        public void Delete(FileManageGroupTranslation fileManageGroupTranslation)
        {
            _db.FileManageGroupTranslation_tb.Remove(fileManageGroupTranslation);
        }

        public async Task<bool> ExistsAsync(Expression<Func<FileManageGroupTranslation, bool>> where = null, Func<IQueryable<FileManageGroupTranslation>, IIncludableQueryable<FileManageGroupTranslation, object>> include = null)
        {
            IQueryable<FileManageGroupTranslation> query = _db.Set<FileManageGroupTranslation>();

            if (include != null)
            {
                query = include(query);
            }

            query = query.Where(where);

            if (await query.CountAsync() > 0)
                return true;
            else
                return false;
        }

        public async Task<IEnumerable<FileManageGroupTranslation>> GetAllAsyn(Expression<Func<FileManageGroupTranslation, bool>> where = null, Func<IQueryable<FileManageGroupTranslation>, IOrderedQueryable<FileManageGroupTranslation>> orderBy = null, Func<IQueryable<FileManageGroupTranslation>, IIncludableQueryable<FileManageGroupTranslation, object>> include = null, bool tracking = false)
        {
            IQueryable<FileManageGroupTranslation> query = _db.Set<FileManageGroupTranslation>();

            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }
            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().ToListAsync();
                else
                    return await orderBy(query).ToListAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().ToListAsync();
                else
                    return await query.ToListAsync();
            }
        }

        public async Task<FileManageGroupTranslation> GetAsync(Expression<Func<FileManageGroupTranslation, bool>> where = null, Func<IQueryable<FileManageGroupTranslation>, IOrderedQueryable<FileManageGroupTranslation>> orderBy = null, Func<IQueryable<FileManageGroupTranslation>, IIncludableQueryable<FileManageGroupTranslation, object>> include = null, bool tracking = false)
        {
            IQueryable<FileManageGroupTranslation> query = _db.Set<FileManageGroupTranslation>();
            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }
            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().FirstOrDefaultAsync();
                else
                    return await orderBy(query).FirstOrDefaultAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().FirstOrDefaultAsync();
                else
                    return await query.FirstOrDefaultAsync();
            }
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<FileManageGroupTranslation, TResult>> selector = null, Expression<Func<FileManageGroupTranslation, bool>> where = null, Func<IQueryable<FileManageGroupTranslation>, IOrderedQueryable<FileManageGroupTranslation>> orderBy = null, Func<IQueryable<FileManageGroupTranslation>, IIncludableQueryable<FileManageGroupTranslation, object>> include = null, bool disableTracking = true)
        {
            IQueryable<FileManageGroupTranslation> query = _db.Set<FileManageGroupTranslation>();
            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }

            if (orderBy != null)
            {
                return await orderBy(query).Select(selector).ToListAsync();
            }
            else
            {
                return await query.Select(selector).ToListAsync();
            }
        }

        public async Task Insert(FileManageGroupTranslation fileManageGroupTranslation)
        {
            await _db.FileManageGroupTranslation_tb.AddAsync(fileManageGroupTranslation);
        }

        public async Task Save()
        {
            await _db.SaveChangesAsync();
        }

        public void Update(FileManageGroupTranslation fileManageGroupTranslation) 
        {
             Detach(fileManageGroupTranslation);
        }
        public void Detach(FileManageGroupTranslation fileManageGroupTranslation)
        {
            var local = _db.Set<FileManageGroupTranslation>().Local
                .FirstOrDefault(entry => entry.FileManageGroupTranslation_ID
                .Equals(fileManageGroupTranslation.FileManageGroupTranslation_ID));

            // check if local is not null 
            if (local != null)
            {
                // detach
                _db.Entry(local).State = EntityState.Detached;
            }
            // set Modified flag in your entry
            _db.Entry(fileManageGroupTranslation).State = EntityState.Modified;
        }
    }
}
