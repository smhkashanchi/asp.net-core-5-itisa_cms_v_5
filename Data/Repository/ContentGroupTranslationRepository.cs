﻿using Data.Context;

using Domain.DomainClasses.Content;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repository
{
    public class ContentGroupTranslationRepository : IContentGroupTranslationRepository
    {
        private ApplicationDbContext _db;
        public ContentGroupTranslationRepository(ApplicationDbContext db)
        {
            _db = db;
        }
        public void DeleteAllAsync(IEnumerable<ContentGroupTranslation> ContentGroupTranslations)
        {
            _db.ContentGroupTranslation_tb.RemoveRange(ContentGroupTranslations);
        }

        public void DeleteAsync(ContentGroupTranslation ContentGroupTranslation)
        {
            _db.ContentGroupTranslation_tb.Remove(ContentGroupTranslation);
        }

        public async Task<bool> ExistsAsync(Expression<Func<ContentGroupTranslation, bool>> where = null, Func<IQueryable<ContentGroupTranslation>, IIncludableQueryable<ContentGroupTranslation, object>> include = null)
        {
            IQueryable<ContentGroupTranslation> query = _db.Set<ContentGroupTranslation>();

            if (include != null)
            {
                query = include(query);
            }

            query = query.Where(where);

            if (await query.CountAsync() > 0)
                return true;
            else
                return false;
        }

        public async Task<IEnumerable<ContentGroupTranslation>> GetAllAsyn(Expression<Func<ContentGroupTranslation, bool>> where = null, Func<IQueryable<ContentGroupTranslation>, IOrderedQueryable<ContentGroupTranslation>> orderBy = null, Func<IQueryable<ContentGroupTranslation>, IIncludableQueryable<ContentGroupTranslation, object>> include = null, bool tracking = false)
        {
            IQueryable<ContentGroupTranslation> query = _db.Set<ContentGroupTranslation>();

            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }
            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().ToListAsync();
                else
                    return await orderBy(query).ToListAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().ToListAsync();
                else
                    return await query.ToListAsync();
            }
        }

        public async Task<ContentGroupTranslation> GetAsync(Expression<Func<ContentGroupTranslation, bool>> where = null, Func<IQueryable<ContentGroupTranslation>, IOrderedQueryable<ContentGroupTranslation>> orderBy = null, Func<IQueryable<ContentGroupTranslation>, IIncludableQueryable<ContentGroupTranslation, object>> include = null, bool tracking = false)
        {
            IQueryable<ContentGroupTranslation> query = _db.Set<ContentGroupTranslation>();
            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }


            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().FirstOrDefaultAsync();
                else
                    return await orderBy(query).FirstOrDefaultAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().FirstOrDefaultAsync();
                else
                    return await query.FirstOrDefaultAsync();
            }
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<ContentGroupTranslation, TResult>> selector = null, Expression<Func<ContentGroupTranslation, bool>> where = null, Func<IQueryable<ContentGroupTranslation>, IOrderedQueryable<ContentGroupTranslation>> orderBy = null, Func<IQueryable<ContentGroupTranslation>, IIncludableQueryable<ContentGroupTranslation, object>> include = null, bool disableTracking = true)
        {
            IQueryable<ContentGroupTranslation> query = _db.Set<ContentGroupTranslation>();


            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }

            if (orderBy != null)
            {
                return await orderBy(query).Select(selector).ToListAsync();
            }
            else
            {
                return await query.AsNoTracking().Select(selector).ToListAsync();
            }
        }

        public async Task Insert(ContentGroupTranslation contentGroupTranslation)
        {
            await _db.ContentGroupTranslation_tb.AddAsync(contentGroupTranslation);
        }

        public async Task Save()
        {
            await _db.SaveChangesAsync();
        }
        public async Task Update(ContentGroupTranslation contentGroupTranslation)
        {
            await Detach(contentGroupTranslation);
        }
        public async Task Detach(ContentGroupTranslation contentGroupTranslation)
        {
            var local = _db.Set<ContentGroupTranslation>().Local
                .FirstOrDefault(entry => entry.ContentGroupTranslation_ID
                .Equals(contentGroupTranslation.ContentGroupTranslation_ID));

            // check if local is not null 
            if (local != null)
            {
                // detach
                _db.Entry(local).State = EntityState.Detached;
            }
            // set Modified flag in your entry
            _db.Entry(contentGroupTranslation).State = EntityState.Modified;
        }
    }
}
