﻿using Data.Context;

using Domain.DomainClasses.Feature;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repository
{
    public class FeatureGroupRepository : IFeatureGroupRepository
    {
        private ApplicationDbContext _db;
        public FeatureGroupRepository(ApplicationDbContext db)
        {
            _db = db;
        }
        public void Delete(FeatureGroup featureGroup)
        {
            _db.FeatureGroup_tb.Remove(featureGroup);
        }

        public async Task<bool> ExistsAsync(Expression<Func<FeatureGroup, bool>> where = null, Func<IQueryable<FeatureGroup>, IIncludableQueryable<FeatureGroup, object>> include = null)
        {
            IQueryable<FeatureGroup> query = _db.Set<FeatureGroup>();

            if (include != null)
            {
                query = include(query);
            }

            query = query.Where(where);

            if (await query.CountAsync() > 0)
                return true;
            else
                return false;
        }

        public async Task<IEnumerable<FeatureGroup>> GetAllAsyn(Expression<Func<FeatureGroup, bool>> where = null, Func<IQueryable<FeatureGroup>, IOrderedQueryable<FeatureGroup>> orderBy = null, Func<IQueryable<FeatureGroup>, IIncludableQueryable<FeatureGroup, object>> include = null, bool tracking = false)
        {
            IQueryable<FeatureGroup> query = _db.Set<FeatureGroup>();

            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }
            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().ToListAsync();
                else
                    return await orderBy(query).ToListAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().ToListAsync();
                else
                    return await query.ToListAsync();
            }
        }

        public async Task<FeatureGroup> GetAsync(Expression<Func<FeatureGroup, bool>> where = null, Func<IQueryable<FeatureGroup>, IOrderedQueryable<FeatureGroup>> orderBy = null, Func<IQueryable<FeatureGroup>, IIncludableQueryable<FeatureGroup, object>> include = null, bool tracking = false)
        {
            IQueryable<FeatureGroup> query = _db.Set<FeatureGroup>();
            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }
            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().FirstOrDefaultAsync();
                else
                    return await orderBy(query).FirstOrDefaultAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().FirstOrDefaultAsync();
                else
                    return await query.FirstOrDefaultAsync();
            }
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<FeatureGroup, TResult>> selector = null, Expression<Func<FeatureGroup, bool>> where = null, Func<IQueryable<FeatureGroup>, IOrderedQueryable<FeatureGroup>> orderBy = null, Func<IQueryable<FeatureGroup>, IIncludableQueryable<FeatureGroup, object>> include = null, bool disableTracking = true)
        {
            IQueryable<FeatureGroup> query = _db.Set<FeatureGroup>();
            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }

            if (orderBy != null)
            {
                return await orderBy(query).Select(selector).ToListAsync();
            }
            else
            {
                return await query.Select(selector).ToListAsync();
            }
        }

        public async Task Insert(FeatureGroup featureGroup)
        {
            await _db.FeatureGroup_tb.AddAsync(featureGroup);
        }

        public async Task Save()
        {
            await _db.SaveChangesAsync(); 
        }

        public void Update(FeatureGroup featureGroup)
        {
            _db.FeatureGroup_tb.Update(featureGroup);
        }
    }
}
