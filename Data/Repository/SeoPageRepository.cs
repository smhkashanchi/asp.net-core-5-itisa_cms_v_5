﻿using Data.Context;

using Domain.DomainClasses.Seo;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repository
{
    public class SeoPageRepository : ISeoPageRepository
    {
        private ApplicationDbContext _db;
        public SeoPageRepository(ApplicationDbContext db)
        {
            _db = db;
        }
        public void DeleteAsync(SeoPage SeoPage)
        {
            _db.SeoPage_tb.Remove(SeoPage);
        }

        public async Task<bool> ExistsAsync(Expression<Func<SeoPage, bool>> where = null, Func<IQueryable<SeoPage>, IIncludableQueryable<SeoPage, object>> include = null)
        {
            IQueryable<SeoPage> query = _db.Set<SeoPage>();

            if (include != null)
            {
                query = include(query);
            }

            query = query.Where(where);

            if (await query.CountAsync() > 0)
                return true;
            else
                return false;
        }

        public async Task<IEnumerable<SeoPage>> GetAllAsyn(Expression<Func<SeoPage, bool>> where = null, Func<IQueryable<SeoPage>, IOrderedQueryable<SeoPage>> orderBy = null, Func<IQueryable<SeoPage>, IIncludableQueryable<SeoPage, object>> include = null, bool tracking = false)
        {
            IQueryable<SeoPage> query = _db.Set<SeoPage>();

            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }
            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().ToListAsync();
                else
                    return await orderBy(query).ToListAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().ToListAsync();
                else
                    return await query.ToListAsync();
            }
        }

        public async Task<SeoPage> GetAsync(Expression<Func<SeoPage, bool>> where = null, Func<IQueryable<SeoPage>, IOrderedQueryable<SeoPage>> orderBy = null, Func<IQueryable<SeoPage>, IIncludableQueryable<SeoPage, object>> include = null, bool tracking = false)
        {
            IQueryable<SeoPage> query = _db.Set<SeoPage>();
            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }


            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().FirstOrDefaultAsync();
                else
                    return await orderBy(query).FirstOrDefaultAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().FirstOrDefaultAsync();
                else
                    return await query.FirstOrDefaultAsync();
            }
        }

        public async Task Insert(SeoPage SeoPage)
        {
            await _db.SeoPage_tb.AddAsync(SeoPage);
        }

        public async Task Save()
        {
            await _db.SaveChangesAsync();
        }

        public async Task Update(SeoPage SeoPage)
        {
            _db.SeoPage_tb.Update(SeoPage);
        }
    }
}
