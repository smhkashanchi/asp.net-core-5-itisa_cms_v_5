﻿using Data.Context;

using Domain.DomainClasses.Content;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repository
{
    public class ContentGroupRepository : IContentGroupRepository
    {
        private ApplicationDbContext _db;
        public ContentGroupRepository(ApplicationDbContext db)
        {
            _db = db;
        }
        public void DeleteAsync(ContentGroup contentGroup)
        {
            _db.ContentGroup_tb.Remove(contentGroup);
        }

        public async Task<bool> ExistsAsync(Expression<Func<ContentGroup, bool>> where = null, Func<IQueryable<ContentGroup>, IIncludableQueryable<ContentGroup, object>> include = null)
        {
            IQueryable<ContentGroup> query = _db.Set<ContentGroup>();

            if (include != null)
            {
                query = include(query);
            }

            query = query.Where(where);

            if (await query.CountAsync() > 0)
                return true;
            else
                return false;
        }

        public async Task<IEnumerable<ContentGroup>> GetAllAsyn(Expression<Func<ContentGroup, bool>> where = null, Func<IQueryable<ContentGroup>, IOrderedQueryable<ContentGroup>> orderBy = null, Func<IQueryable<ContentGroup>, IIncludableQueryable<ContentGroup, object>> include = null, bool tracking = false)
        {
            IQueryable<ContentGroup> query = _db.Set<ContentGroup>();

            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }
            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().ToListAsync();
                else
                    return await orderBy(query).ToListAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().ToListAsync();
                else
                    return await query.ToListAsync();
            }
        }

        public async Task<ContentGroup> GetAsync(Expression<Func<ContentGroup, bool>> where = null, Func<IQueryable<ContentGroup>, IOrderedQueryable<ContentGroup>> orderBy = null, Func<IQueryable<ContentGroup>, IIncludableQueryable<ContentGroup, object>> include = null, bool tracking = false)
        {
            IQueryable<ContentGroup> query = _db.Set<ContentGroup>();
            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }


            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().FirstOrDefaultAsync();
                else
                    return await orderBy(query).FirstOrDefaultAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().FirstOrDefaultAsync();
                else
                    return await query.FirstOrDefaultAsync();
            }
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<ContentGroup, TResult>> selector = null, Expression<Func<ContentGroup, bool>> where = null, Func<IQueryable<ContentGroup>, IOrderedQueryable<ContentGroup>> orderBy = null, Func<IQueryable<ContentGroup>, IIncludableQueryable<ContentGroup, object>> include = null, bool disableTracking = true)
        {
            IQueryable<ContentGroup> query = _db.Set<ContentGroup>();
            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }

            if (orderBy != null)
            {
                return await orderBy(query).Select(selector).ToListAsync();
            }
            else
            {
                return await query.Select(selector).ToListAsync();
            }
        }

        public async Task Insert(ContentGroup contentGroup)
        {
            await _db.ContentGroup_tb.AddAsync(contentGroup);
        }

        public async Task Save()
        {
            await _db.SaveChangesAsync();
        }

        public void Update(ContentGroup contentGroup)
        {
            _db.ContentGroup_tb.Update(contentGroup);
        }
    }
}
