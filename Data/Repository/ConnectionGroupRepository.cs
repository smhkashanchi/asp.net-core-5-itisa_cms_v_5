﻿using Data.Context;

using Domain.DomainClasses.Connection;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repository
{
    public class ConnectionGroupRepository : IConnectionGroupRepository
    {
        private ApplicationDbContext _db;
        public ConnectionGroupRepository(ApplicationDbContext db)
        {
            _db = db;
        }
        public void DeleteAsync(ConnectionGroup ConnectionGroup)
        {
            _db.ConnectionGroup_tb.Remove(ConnectionGroup);
        }

        public async Task<bool> ExistsAsync(Expression<Func<ConnectionGroup, bool>> where = null, Func<IQueryable<ConnectionGroup>, IIncludableQueryable<ConnectionGroup, object>> include = null)
        {
            IQueryable<ConnectionGroup> query = _db.Set<ConnectionGroup>();

            if (include != null)
            {
                query = include(query);
            }

            query = query.Where(where);

            if (await query.CountAsync() > 0)
                return true;
            else
                return false;
        }

        public async Task<IEnumerable<ConnectionGroup>> GetAllAsyn(Expression<Func<ConnectionGroup, bool>> where = null, Func<IQueryable<ConnectionGroup>, IOrderedQueryable<ConnectionGroup>> orderBy = null, Func<IQueryable<ConnectionGroup>, IIncludableQueryable<ConnectionGroup, object>> include = null, bool tracking = false)
        {
            IQueryable<ConnectionGroup> query = _db.Set<ConnectionGroup>();

            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }
            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().ToListAsync();
                else
                    return await orderBy(query).ToListAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().ToListAsync();
                else
                    return await query.ToListAsync();
            }
        }

        public async Task<ConnectionGroup> GetAsync(Expression<Func<ConnectionGroup, bool>> where = null, Func<IQueryable<ConnectionGroup>, IOrderedQueryable<ConnectionGroup>> orderBy = null, Func<IQueryable<ConnectionGroup>, IIncludableQueryable<ConnectionGroup, object>> include = null, bool tracking = false)
        {
            IQueryable<ConnectionGroup> query = _db.Set<ConnectionGroup>();
            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }


            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().FirstOrDefaultAsync();
                else
                    return await orderBy(query).FirstOrDefaultAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().FirstOrDefaultAsync();
                else
                    return await query.FirstOrDefaultAsync();
            }
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<ConnectionGroup, TResult>> selector = null, Expression<Func<ConnectionGroup, bool>> where = null, Func<IQueryable<ConnectionGroup>, IOrderedQueryable<ConnectionGroup>> orderBy = null, Func<IQueryable<ConnectionGroup>, IIncludableQueryable<ConnectionGroup, object>> include = null, bool disableTracking = true)
        {
            IQueryable<ConnectionGroup> query = _db.Set<ConnectionGroup>();


            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }

            if (orderBy != null)
            {
                return await orderBy(query).Select(selector).ToListAsync();
            }
            else
            {
                return await query.Select(selector).ToListAsync();
            }
        }

        public async Task Insert(ConnectionGroup ConnectionGroup)
        {
           await _db.ConnectionGroup_tb.AddAsync(ConnectionGroup);
        }

        public async Task Save()
        {
            await _db.SaveChangesAsync();
        }

        public void Update(ConnectionGroup ConnectionGroup)
        {
            _db.ConnectionGroup_tb.Update(ConnectionGroup);
        }
    }
}
