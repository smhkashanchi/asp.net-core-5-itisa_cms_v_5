﻿using Data.Context;

using Domain.DomainClasses.Region;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repository
{
    public class CityRepository : ICityRepository
    {
        private ApplicationDbContext _db;
        public CityRepository(ApplicationDbContext db)
        {
            _db = db;
        }
        public void Delete(City city)
        {
            _db.City_tb.Remove(city);
        }

        public void DeleteAll(IEnumerable<City> cities)
        {
            _db.City_tb.RemoveRange(cities);
        }

        public async Task<bool> ExistsAsync(Expression<Func<City, bool>> where = null, Func<IQueryable<City>, IIncludableQueryable<City, object>> include = null)
        {
            IQueryable<City> query = _db.Set<City>();

            if (include != null)
            {
                query = include(query);
            }

            query = query.Where(where);

            if (await query.CountAsync() > 0)
                return true;
            else
                return false;
        }

        public async Task<IEnumerable<City>> GetAllAsyn(Expression<Func<City, bool>> where = null, Func<IQueryable<City>, IOrderedQueryable<City>> orderBy = null, Func<IQueryable<City>, IIncludableQueryable<City, object>> include = null, bool tracking = false)
        {
            IQueryable<City> query = _db.Set<City>();

            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }
            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().ToListAsync();
                else
                    return await orderBy(query).ToListAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().ToListAsync();
                else
                    return await query.ToListAsync();
            }
        }

        public async Task<City> GetAsync(Expression<Func<City, bool>> where = null, Func<IQueryable<City>, IOrderedQueryable<City>> orderBy = null, Func<IQueryable<City>, IIncludableQueryable<City, object>> include = null, bool tracking = false)
        {
            IQueryable<City> query = _db.Set<City>();
            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }


            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().FirstOrDefaultAsync();
                else
                    return await orderBy(query).FirstOrDefaultAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().FirstOrDefaultAsync();
                else
                    return await query.FirstOrDefaultAsync();
            }
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<City, TResult>> selector = null, Expression<Func<City, bool>> where = null, Func<IQueryable<City>, IOrderedQueryable<City>> orderBy = null, Func<IQueryable<City>, IIncludableQueryable<City, object>> include = null, bool disableTracking = true)
        {
            IQueryable<City> query = _db.Set<City>();


            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }

            if (orderBy != null)
            {
                return await orderBy(query).Select(selector).ToListAsync();
            }
            else
            {
                return await query.AsNoTracking().Select(selector).ToListAsync();
            }
        }

        public async Task Insert(City city)
        {
            await _db.City_tb.AddAsync(city);
        }

        public async Task Save()
        {
            await _db.SaveChangesAsync();
        }

        public async Task Update(City city)
        {
            _db.City_tb.Update(city);
        }
    }
}
