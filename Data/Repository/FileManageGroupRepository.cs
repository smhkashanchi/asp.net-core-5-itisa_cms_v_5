﻿using Data.Context;

using Domain.DomainClasses.FilesManage;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repository
{
    public class FileManageGroupRepository : IFileManageGroupRepository
    {
        private ApplicationDbContext _db;
        public FileManageGroupRepository(ApplicationDbContext db)
        {
            _db = db;
        }
        public void Delete(FileManageGroup fileManageGroup)
        {
            _db.FileManageGroup_tb.Remove(fileManageGroup);
        }

        public async Task<bool> ExistsAsync(Expression<Func<FileManageGroup, bool>> where = null, Func<IQueryable<FileManageGroup>, IIncludableQueryable<FileManageGroup, object>> include = null)
        {
            IQueryable<FileManageGroup> query = _db.Set<FileManageGroup>();

            if (include != null)
            {
                query = include(query);
            }

            query = query.Where(where);

            if (await query.CountAsync() > 0)
                return true;
            else
                return false;
        }

        public async Task<IEnumerable<FileManageGroup>> GetAllAsyn(Expression<Func<FileManageGroup, bool>> where = null, Func<IQueryable<FileManageGroup>, IOrderedQueryable<FileManageGroup>> orderBy = null, Func<IQueryable<FileManageGroup>, IIncludableQueryable<FileManageGroup, object>> include = null, bool tracking = false)
        {
            IQueryable<FileManageGroup> query = _db.Set<FileManageGroup>();

            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }
            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().ToListAsync();
                else
                    return await orderBy(query).ToListAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().ToListAsync();
                else
                    return await query.ToListAsync();
            }
        }

        public async Task<FileManageGroup> GetAsync(Expression<Func<FileManageGroup, bool>> where = null, Func<IQueryable<FileManageGroup>, IOrderedQueryable<FileManageGroup>> orderBy = null, Func<IQueryable<FileManageGroup>, IIncludableQueryable<FileManageGroup, object>> include = null, bool tracking = false)
        {
            IQueryable<FileManageGroup> query = _db.Set<FileManageGroup>();
            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }
            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().FirstOrDefaultAsync();
                else
                    return await orderBy(query).FirstOrDefaultAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().FirstOrDefaultAsync();
                else
                    return await query.FirstOrDefaultAsync();
            }
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<FileManageGroup, TResult>> selector = null, Expression<Func<FileManageGroup, bool>> where = null, Func<IQueryable<FileManageGroup>, IOrderedQueryable<FileManageGroup>> orderBy = null, Func<IQueryable<FileManageGroup>, IIncludableQueryable<FileManageGroup, object>> include = null, bool disableTracking = true)
        {
            IQueryable<FileManageGroup> query = _db.Set<FileManageGroup>();
            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }

            if (orderBy != null)
            {
                return await orderBy(query).Select(selector).ToListAsync();
            }
            else
            {
                return await query.Select(selector).ToListAsync();
            }
        }

        public async Task Insert(FileManageGroup fileManageGroup)
        {
            await _db.FileManageGroup_tb.AddAsync(fileManageGroup);
        }

        public async Task Save()
        {
            await _db.SaveChangesAsync();
        }

        public void Update(FileManageGroup fileManageGroup)
        {
            _db.FileManageGroup_tb.Update(fileManageGroup);
        }
    }
}
