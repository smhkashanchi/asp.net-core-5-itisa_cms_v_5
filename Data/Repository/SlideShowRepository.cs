﻿using Data.Context;

using Domain.DomainClasses.SlideShow;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repository
{
    public class SlideShowRepository : ISlideShowRepository
    {
        private ApplicationDbContext _db;
        public SlideShowRepository(ApplicationDbContext db)
        {
            _db = db;
        }
        public void DeleteAsync(SlideShow slideShow)
        {
            _db.SlideShow_tb.Remove(slideShow);
        }

        public async Task<bool> ExistsAsync(Expression<Func<SlideShow, bool>> where = null, Func<IQueryable<SlideShow>, IIncludableQueryable<SlideShow, object>> include = null)
        {
            IQueryable<SlideShow> query = _db.Set<SlideShow>();

            if (include != null)
            {
                query = include(query);
            }

            query = query.Where(where);

            if (await query.CountAsync() > 0)
                return true;
            else
                return false;
        }

        public async Task<IEnumerable<SlideShow>> GetAllAsyn(Expression<Func<SlideShow, bool>> where = null, Func<IQueryable<SlideShow>, IOrderedQueryable<SlideShow>> orderBy = null, Func<IQueryable<SlideShow>, IIncludableQueryable<SlideShow, object>> include = null, bool tracking = false)
        {
            IQueryable<SlideShow> query = _db.Set<SlideShow>();

            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }
            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().ToListAsync();
                else
                    return await orderBy(query).ToListAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().ToListAsync();
                else
                    return await query.ToListAsync();
            }
        }

        public async Task<SlideShow> GetAsync(Expression<Func<SlideShow, bool>> where = null, Func<IQueryable<SlideShow>, IOrderedQueryable<SlideShow>> orderBy = null, Func<IQueryable<SlideShow>, IIncludableQueryable<SlideShow, object>> include = null, bool tracking = false)
        {
            IQueryable<SlideShow> query = _db.Set<SlideShow>();
            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }


            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().FirstOrDefaultAsync();
                else
                    return await orderBy(query).FirstOrDefaultAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().FirstOrDefaultAsync();
                else
                    return await query.FirstOrDefaultAsync();
            }
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<SlideShow, TResult>> selector = null, Expression<Func<SlideShow, bool>> where = null, Func<IQueryable<SlideShow>, IOrderedQueryable<SlideShow>> orderBy = null, Func<IQueryable<SlideShow>, IIncludableQueryable<SlideShow, object>> include = null, bool disableTracking = true)
        {

            IQueryable<SlideShow> query = _db.Set<SlideShow>();


            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }

            if (orderBy != null)
            {
                return await orderBy(query).Select(selector).ToListAsync();
            }
            else
            {
                return await query.Select(selector).ToListAsync();
            }
        }

        public async Task Insert(SlideShow slideShow)
        {
            await _db.SlideShow_tb.AddAsync(slideShow);
        }

        public async Task Save()
        {
            await _db.SaveChangesAsync();
        }

        public async Task Update(SlideShow slideShow)
        {
            _db.SlideShow_tb.Update(slideShow);
        }
    }
}
