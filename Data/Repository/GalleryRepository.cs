﻿using Data.Context;

using Domain.DomainClasses.Gallery;
using Domain.Interfaces;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repository
{
    public class GalleryRepository : IGalleryRepository
    {
        private ApplicationDbContext _db;
        public GalleryRepository(ApplicationDbContext db)
        {
            _db = db;
        }

        public void Delete(Gallery gallery)
        {
            _db.Gallery_tb.Remove(gallery);
        }

        public async Task<bool> ExistsAsync(Expression<Func<Gallery, bool>> where = null, Func<IQueryable<Gallery>, IIncludableQueryable<Gallery, object>> include = null)
        {
            IQueryable<Gallery> query = _db.Set<Gallery>();

            if (include != null)
            {
                query = include(query);
            }

            query = query.Where(where);

            if (await query.CountAsync() > 0)
                return true;
            else
                return false;
        }

        public async Task<Gallery> GetAsync(Expression<Func<Gallery, bool>> where = null, Func<IQueryable<Gallery>, IOrderedQueryable<Gallery>> orderBy = null, Func<IQueryable<Gallery>, IIncludableQueryable<Gallery, object>> include = null, bool tracking = false)
        {
            IQueryable<Gallery> query = _db.Set<Gallery>();
            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }


            if (orderBy != null)
            {
                if (tracking)
                    return await orderBy(query).AsNoTracking().FirstOrDefaultAsync();
                else
                    return await orderBy(query).FirstOrDefaultAsync();

            }
            else
            {
                if (tracking)
                    return await query.AsNoTracking().FirstOrDefaultAsync();
                else
                    return await query.FirstOrDefaultAsync();
            }
        }

        public async Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<Gallery, TResult>> selector = null, Expression<Func<Gallery, bool>> where = null, Func<IQueryable<Gallery>, IOrderedQueryable<Gallery>> orderBy = null, Func<IQueryable<Gallery>, IIncludableQueryable<Gallery, object>> include = null, bool disableTracking = true)
        {
            IQueryable<Gallery> query = _db.Set<Gallery>();


            if (include != null)
            {
                query = include(query);
            }

            if (where != null)
            {
                query = query.Where(where);
            }

            if (orderBy != null)
            {
                return await orderBy(query).Select(selector).ToListAsync();
            }
            else
            {
                return await query.Select(selector).ToListAsync();
            }
        }

        public async Task InsertGallery(Gallery gallery)
        {
            await _db.Gallery_tb.AddAsync(gallery);
        }

        public async Task Save()
        {
            await _db.SaveChangesAsync();
        }

        public async Task UpdateGallery(Gallery gallery)
        {
            _db.Gallery_tb.Update(gallery);
        }
    }
}
