﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Data.Migrations
{
    public partial class initDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    UserName = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    Email = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(type: "bit", nullable: false),
                    PasswordHash = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SecurityStamp = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ConcurrencyStamp = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PhoneNumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(type: "bit", nullable: false),
                    TwoFactorEnabled = table.Column<bool>(type: "bit", nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true),
                    LockoutEnabled = table.Column<bool>(type: "bit", nullable: false),
                    AccessFailedCount = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Color_tb",
                columns: table => new
                {
                    ColorId = table.Column<short>(type: "smallint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ColorCode = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Color_tb", x => x.ColorId);
                });

            migrationBuilder.CreateTable(
                name: "ConnectionGroup_tb",
                columns: table => new
                {
                    Id = table.Column<byte>(type: "tinyint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    Slug = table.Column<string>(type: "nvarchar(300)", maxLength: 300, nullable: false),
                    IsActive = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ConnectionGroup_tb", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ContentGroup_tb",
                columns: table => new
                {
                    ContentGroup_ID = table.Column<byte>(type: "tinyint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ParentId = table.Column<byte>(type: "tinyint", nullable: true),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    ContentImageWidth = table.Column<string>(type: "nvarchar(5)", maxLength: 5, nullable: true),
                    ContentImageHeight = table.Column<string>(type: "nvarchar(5)", maxLength: 5, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContentGroup_tb", x => x.ContentGroup_ID);
                    table.ForeignKey(
                        name: "FK_ContentGroup_tb_ContentGroup_tb_ParentId",
                        column: x => x.ParentId,
                        principalTable: "ContentGroup_tb",
                        principalColumn: "ContentGroup_ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Country_tb",
                columns: table => new
                {
                    Country_ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Code = table.Column<string>(type: "varchar(10)", nullable: true),
                    IsActive = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Country_tb", x => x.Country_ID);
                });

            migrationBuilder.CreateTable(
                name: "CoWorker_tb",
                columns: table => new
                {
                    CoWorker_ID = table.Column<short>(type: "smallint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Image = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CoWorker_tb", x => x.CoWorker_ID);
                });

            migrationBuilder.CreateTable(
                name: "CoWorkerGroup_tb",
                columns: table => new
                {
                    CoWorkerGroup_ID = table.Column<byte>(type: "tinyint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ImageWidth = table.Column<string>(type: "nvarchar(5)", maxLength: 5, nullable: false),
                    ImageHeight = table.Column<string>(type: "nvarchar(5)", maxLength: 5, nullable: false),
                    IsActive = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CoWorkerGroup_tb", x => x.CoWorkerGroup_ID);
                });

            migrationBuilder.CreateTable(
                name: "Currency_tb",
                columns: table => new
                {
                    Currency_ID = table.Column<byte>(type: "tinyint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Currency_tb", x => x.Currency_ID);
                });

            migrationBuilder.CreateTable(
                name: "FeatureGroup_tb",
                columns: table => new
                {
                    FeatureGroup_ID = table.Column<short>(type: "smallint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FeatureGroup_tb", x => x.FeatureGroup_ID);
                });

            migrationBuilder.CreateTable(
                name: "FileManage_tb",
                columns: table => new
                {
                    FileManage_ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Type = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: false),
                    FileData = table.Column<string>(type: "nvarchar(3000)", maxLength: 3000, nullable: true),
                    FileSize = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false),
                    DownloadCount = table.Column<short>(type: "smallint", nullable: false),
                    CreateDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    IsActive = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FileManage_tb", x => x.FileManage_ID);
                });

            migrationBuilder.CreateTable(
                name: "FileManageGroup_tb",
                columns: table => new
                {
                    FileManageGroup_ID = table.Column<short>(type: "smallint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Parent_Id = table.Column<short>(type: "smallint", nullable: true),
                    IsActive = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FileManageGroup_tb", x => x.FileManageGroup_ID);
                    table.ForeignKey(
                        name: "FK_FileManageGroup_tb_FileManageGroup_tb_Parent_Id",
                        column: x => x.Parent_Id,
                        principalTable: "FileManageGroup_tb",
                        principalColumn: "FileManageGroup_ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Gallery_tb",
                columns: table => new
                {
                    Gallery_ID = table.Column<short>(type: "smallint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Image = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Gallery_tb", x => x.Gallery_ID);
                });

            migrationBuilder.CreateTable(
                name: "GalleryGroup_tb",
                columns: table => new
                {
                    GalleryGroup_ID = table.Column<byte>(type: "tinyint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    LargPicWidth = table.Column<string>(type: "nvarchar(5)", maxLength: 5, nullable: true),
                    MediumPicWidth = table.Column<string>(type: "nvarchar(5)", maxLength: 5, nullable: true),
                    SmallPicWidth = table.Column<string>(type: "nvarchar(5)", maxLength: 5, nullable: true),
                    LargPicHeight = table.Column<string>(type: "nvarchar(5)", maxLength: 5, nullable: true),
                    MediumPicHeight = table.Column<string>(type: "nvarchar(5)", maxLength: 5, nullable: true),
                    SmallPicHeight = table.Column<string>(type: "nvarchar(5)", maxLength: 5, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GalleryGroup_tb", x => x.GalleryGroup_ID);
                });

            migrationBuilder.CreateTable(
                name: "Language_tb",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: false),
                    Name = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: false),
                    IsActive = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Language_tb", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MainSiteMap_tb",
                columns: table => new
                {
                    SiteMap_ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Type = table.Column<short>(type: "smallint", nullable: false),
                    Location = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Title = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    CreateDate = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MainSiteMap_tb", x => x.SiteMap_ID);
                });

            migrationBuilder.CreateTable(
                name: "SendType_tb",
                columns: table => new
                {
                    SendType_ID = table.Column<byte>(type: "tinyint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Type = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SendType_tb", x => x.SendType_ID);
                });

            migrationBuilder.CreateTable(
                name: "SeoPage_tb",
                columns: table => new
                {
                    SeoPage_ID = table.Column<short>(type: "smallint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PageUrl = table.Column<string>(type: "nvarchar(max)", maxLength: 5000, nullable: false),
                    MetaTitle = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true),
                    MetaKeyword = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true),
                    MetaDescription = table.Column<string>(type: "nvarchar(max)", maxLength: 5000, nullable: true),
                    MetaOther = table.Column<string>(type: "nvarchar(max)", maxLength: 5000, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SeoPage_tb", x => x.SeoPage_ID);
                });

            migrationBuilder.CreateTable(
                name: "SlideShow_tb",
                columns: table => new
                {
                    SlideShow_ID = table.Column<short>(type: "smallint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Image = table.Column<string>(type: "nvarchar(max)", maxLength: 5000, nullable: true),
                    MediumImage = table.Column<string>(type: "nvarchar(max)", maxLength: 5000, nullable: true),
                    SmallImage = table.Column<string>(type: "nvarchar(max)", maxLength: 5000, nullable: true),
                    SimpleUpload = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SlideShow_tb", x => x.SlideShow_ID);
                });

            migrationBuilder.CreateTable(
                name: "SlideShowGroup_tb",
                columns: table => new
                {
                    SlideShowGroup_ID = table.Column<byte>(type: "tinyint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    ImageWidth = table.Column<string>(type: "nvarchar(5)", maxLength: 5, nullable: true),
                    ImageHeight = table.Column<string>(type: "nvarchar(5)", maxLength: 5, nullable: true),
                    SmallPicWidth = table.Column<string>(type: "nvarchar(5)", maxLength: 5, nullable: true),
                    SmallPicHeight = table.Column<string>(type: "nvarchar(5)", maxLength: 5, nullable: true),
                    MediumPicHeight = table.Column<string>(type: "nvarchar(5)", maxLength: 5, nullable: true),
                    MediumPicWidth = table.Column<string>(type: "nvarchar(5)", maxLength: 5, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SlideShowGroup_tb", x => x.SlideShowGroup_ID);
                });

            migrationBuilder.CreateTable(
                name: "SocialNetwork_tb",
                columns: table => new
                {
                    SocialNetwork_ID = table.Column<short>(type: "smallint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Link = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    Type = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SocialNetwork_tb", x => x.SocialNetwork_ID);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RoleId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    ClaimType = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ClaimValue = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    ClaimType = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ClaimValue = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    ProviderKey = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    ProviderDisplayName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UserId = table.Column<string>(type: "nvarchar(450)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    RoleId = table.Column<string>(type: "nvarchar(450)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    LoginProvider = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Value = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Connection_tb",
                columns: table => new
                {
                    Id = table.Column<byte>(type: "tinyint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    GroupId = table.Column<byte>(type: "tinyint", nullable: false),
                    Type = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Image = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Connection_tb", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Connection_tb_ConnectionGroup_tb_GroupId",
                        column: x => x.GroupId,
                        principalTable: "ConnectionGroup_tb",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "State_tb",
                columns: table => new
                {
                    State_ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Code = table.Column<string>(type: "varchar(10)", nullable: true),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    CountryId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_State_tb", x => x.State_ID);
                    table.ForeignKey(
                        name: "FK_State_tb_Country_tb_CountryId",
                        column: x => x.CountryId,
                        principalTable: "Country_tb",
                        principalColumn: "Country_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PriceList_tb",
                columns: table => new
                {
                    PriceList_ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Currency_Id = table.Column<byte>(type: "tinyint", nullable: false),
                    Title = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PriceList_tb", x => x.PriceList_ID);
                    table.ForeignKey(
                        name: "FK_PriceList_tb_Currency_tb_Currency_Id",
                        column: x => x.Currency_Id,
                        principalTable: "Currency_tb",
                        principalColumn: "Currency_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Feature_tb",
                columns: table => new
                {
                    Feature_ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FeatureGroup_Id = table.Column<short>(type: "smallint", nullable: false),
                    UseFilter = table.Column<bool>(type: "bit", nullable: false),
                    IsVariable = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Feature_tb", x => x.Feature_ID);
                    table.ForeignKey(
                        name: "FK_Feature_tb_FeatureGroup_tb_FeatureGroup_Id",
                        column: x => x.FeatureGroup_Id,
                        principalTable: "FeatureGroup_tb",
                        principalColumn: "FeatureGroup_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Content_tb",
                columns: table => new
                {
                    Content_ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Image = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SmallImage = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreateDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ReleaseDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    Gallery_Id = table.Column<short>(type: "smallint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Content_tb", x => x.Content_ID);
                    table.ForeignKey(
                        name: "FK_Content_tb_Gallery_tb_Gallery_Id",
                        column: x => x.Gallery_Id,
                        principalTable: "Gallery_tb",
                        principalColumn: "Gallery_ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "GalleryPicture_tb",
                columns: table => new
                {
                    GalleryPic_ID = table.Column<string>(type: "nvarchar(450)", maxLength: 450, nullable: false),
                    Gallery_Id = table.Column<short>(type: "smallint", nullable: false),
                    Image = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GalleryPicture_tb", x => x.GalleryPic_ID);
                    table.ForeignKey(
                        name: "FK_GalleryPicture_tb_Gallery_tb_Gallery_Id",
                        column: x => x.Gallery_Id,
                        principalTable: "Gallery_tb",
                        principalColumn: "Gallery_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Product_tb",
                columns: table => new
                {
                    Product_ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Code = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    IsExist = table.Column<bool>(type: "bit", nullable: false),
                    MostVisited = table.Column<bool>(type: "bit", nullable: false),
                    BestSelling = table.Column<bool>(type: "bit", nullable: false),
                    CreateDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Gallery_Id = table.Column<short>(type: "smallint", nullable: true),
                    IsPreview = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Product_tb", x => x.Product_ID);
                    table.ForeignKey(
                        name: "FK_Product_tb_Gallery_tb_Gallery_Id",
                        column: x => x.Gallery_Id,
                        principalTable: "Gallery_tb",
                        principalColumn: "Gallery_ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ColorTranslation_tb",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ColorId = table.Column<short>(type: "smallint", nullable: false),
                    Title = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    LanguageId = table.Column<string>(type: "nvarchar(10)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ColorTranslation_tb", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ColorTranslation_tb_Color_tb_ColorId",
                        column: x => x.ColorId,
                        principalTable: "Color_tb",
                        principalColumn: "ColorId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ColorTranslation_tb_Language_tb_LanguageId",
                        column: x => x.LanguageId,
                        principalTable: "Language_tb",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CoWorkerGroupTranslation_tb",
                columns: table => new
                {
                    CoWorkerGroupTranslation_ID = table.Column<byte>(type: "tinyint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CoWorkerGroup_Id = table.Column<byte>(type: "tinyint", nullable: false),
                    Title = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Language_Id = table.Column<string>(type: "nvarchar(10)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CoWorkerGroupTranslation_tb", x => x.CoWorkerGroupTranslation_ID);
                    table.ForeignKey(
                        name: "FK_CoWorkerGroupTranslation_tb_CoWorkerGroup_tb_CoWorkerGroup_Id",
                        column: x => x.CoWorkerGroup_Id,
                        principalTable: "CoWorkerGroup_tb",
                        principalColumn: "CoWorkerGroup_ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CoWorkerGroupTranslation_tb_Language_tb_Language_Id",
                        column: x => x.Language_Id,
                        principalTable: "Language_tb",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Discount_tb",
                columns: table => new
                {
                    Discount_ID = table.Column<short>(type: "smallint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    Percent = table.Column<byte>(type: "tinyint", nullable: false),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    StartDate = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true),
                    EndDate = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true),
                    UnLimitTime = table.Column<bool>(type: "bit", nullable: false),
                    UnLimitUse = table.Column<bool>(type: "bit", nullable: false),
                    IsRandom = table.Column<bool>(type: "bit", nullable: false),
                    CountOff = table.Column<short>(type: "smallint", nullable: true),
                    CountUse = table.Column<short>(type: "smallint", nullable: false),
                    Language_Id = table.Column<string>(type: "nvarchar(10)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Discount_tb", x => x.Discount_ID);
                    table.ForeignKey(
                        name: "FK_Discount_tb_Language_tb_Language_Id",
                        column: x => x.Language_Id,
                        principalTable: "Language_tb",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "FAQ_tb",
                columns: table => new
                {
                    FAQ_ID = table.Column<short>(type: "smallint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Question = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: false),
                    Answer = table.Column<string>(type: "nvarchar(max)", maxLength: 9000, nullable: false),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    Language_Id = table.Column<string>(type: "nvarchar(10)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FAQ_tb", x => x.FAQ_ID);
                    table.ForeignKey(
                        name: "FK_FAQ_tb_Language_tb_Language_Id",
                        column: x => x.Language_Id,
                        principalTable: "Language_tb",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "FeatureGroupTranslation_tb",
                columns: table => new
                {
                    FeatureGroupTranslation_ID = table.Column<short>(type: "smallint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FeatureGroup_Id = table.Column<short>(type: "smallint", nullable: false),
                    Title = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Language_Id = table.Column<string>(type: "nvarchar(10)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FeatureGroupTranslation_tb", x => x.FeatureGroupTranslation_ID);
                    table.ForeignKey(
                        name: "FK_FeatureGroupTranslation_tb_FeatureGroup_tb_FeatureGroup_Id",
                        column: x => x.FeatureGroup_Id,
                        principalTable: "FeatureGroup_tb",
                        principalColumn: "FeatureGroup_ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_FeatureGroupTranslation_tb_Language_tb_Language_Id",
                        column: x => x.Language_Id,
                        principalTable: "Language_tb",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "GalleryGroupTranslation_tb",
                columns: table => new
                {
                    GalleryGroupTranslation_ID = table.Column<short>(type: "smallint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    GalleryGroup_Id = table.Column<byte>(type: "tinyint", nullable: false),
                    Title = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Language_Id = table.Column<string>(type: "nvarchar(10)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GalleryGroupTranslation_tb", x => x.GalleryGroupTranslation_ID);
                    table.ForeignKey(
                        name: "FK_GalleryGroupTranslation_tb_GalleryGroup_tb_GalleryGroup_Id",
                        column: x => x.GalleryGroup_Id,
                        principalTable: "GalleryGroup_tb",
                        principalColumn: "GalleryGroup_ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_GalleryGroupTranslation_tb_Language_tb_Language_Id",
                        column: x => x.Language_Id,
                        principalTable: "Language_tb",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ContentGroupTranslation_tb",
                columns: table => new
                {
                    ContentGroupTranslation_ID = table.Column<short>(type: "smallint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ContentGroup_Id = table.Column<byte>(type: "tinyint", nullable: false),
                    Title = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    Slug = table.Column<string>(type: "nvarchar(300)", maxLength: 300, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(2000)", maxLength: 2000, nullable: true),
                    Language_Id = table.Column<string>(type: "nvarchar(10)", nullable: true),
                    SiteMap_Id = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContentGroupTranslation_tb", x => x.ContentGroupTranslation_ID);
                    table.ForeignKey(
                        name: "FK_ContentGroupTranslation_tb_ContentGroup_tb_ContentGroup_Id",
                        column: x => x.ContentGroup_Id,
                        principalTable: "ContentGroup_tb",
                        principalColumn: "ContentGroup_ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ContentGroupTranslation_tb_Language_tb_Language_Id",
                        column: x => x.Language_Id,
                        principalTable: "Language_tb",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ContentGroupTranslation_tb_MainSiteMap_tb_SiteMap_Id",
                        column: x => x.SiteMap_Id,
                        principalTable: "MainSiteMap_tb",
                        principalColumn: "SiteMap_ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "FileManageGroupTranslation_tb",
                columns: table => new
                {
                    FileManageGroupTranslation_ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FileManageGroup_Id = table.Column<short>(type: "smallint", nullable: false),
                    Title = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Slug = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    Language_Id = table.Column<string>(type: "nvarchar(10)", nullable: true),
                    SiteMap_Id = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FileManageGroupTranslation_tb", x => x.FileManageGroupTranslation_ID);
                    table.ForeignKey(
                        name: "FK_FileManageGroupTranslation_tb_FileManageGroup_tb_FileManageGroup_Id",
                        column: x => x.FileManageGroup_Id,
                        principalTable: "FileManageGroup_tb",
                        principalColumn: "FileManageGroup_ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_FileManageGroupTranslation_tb_Language_tb_Language_Id",
                        column: x => x.Language_Id,
                        principalTable: "Language_tb",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_FileManageGroupTranslation_tb_MainSiteMap_tb_SiteMap_Id",
                        column: x => x.SiteMap_Id,
                        principalTable: "MainSiteMap_tb",
                        principalColumn: "SiteMap_ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ProductGroup_tb",
                columns: table => new
                {
                    ProductGroup_ID = table.Column<short>(type: "smallint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ParentId = table.Column<short>(type: "smallint", nullable: true),
                    Image = table.Column<string>(type: "nvarchar(900)", maxLength: 900, nullable: true),
                    ImageWidth = table.Column<string>(type: "nvarchar(5)", maxLength: 5, nullable: true),
                    ImageHeight = table.Column<string>(type: "nvarchar(5)", maxLength: 5, nullable: true),
                    SmallImageWidth = table.Column<string>(type: "nvarchar(5)", maxLength: 5, nullable: true),
                    SmallImageHeight = table.Column<string>(type: "nvarchar(5)", maxLength: 5, nullable: true),
                    SiteMap_Id = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductGroup_tb", x => x.ProductGroup_ID);
                    table.ForeignKey(
                        name: "FK_ProductGroup_tb_MainSiteMap_tb_SiteMap_Id",
                        column: x => x.SiteMap_Id,
                        principalTable: "MainSiteMap_tb",
                        principalColumn: "SiteMap_ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ProductGroup_tb_ProductGroup_tb_ParentId",
                        column: x => x.ParentId,
                        principalTable: "ProductGroup_tb",
                        principalColumn: "ProductGroup_ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SendTypeTranslation_tb",
                columns: table => new
                {
                    SendTypeTranslation_ID = table.Column<short>(type: "smallint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SendType_Id = table.Column<byte>(type: "tinyint", nullable: false),
                    Title = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(max)", maxLength: 9000, nullable: true),
                    Price = table.Column<int>(type: "int", nullable: true),
                    Language_Id = table.Column<string>(type: "nvarchar(10)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SendTypeTranslation_tb", x => x.SendTypeTranslation_ID);
                    table.ForeignKey(
                        name: "FK_SendTypeTranslation_tb_Language_tb_Language_Id",
                        column: x => x.Language_Id,
                        principalTable: "Language_tb",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SendTypeTranslation_tb_SendType_tb_SendType_Id",
                        column: x => x.SendType_Id,
                        principalTable: "SendType_tb",
                        principalColumn: "SendType_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SlideShowGroupTranslation_tb",
                columns: table => new
                {
                    SlideShowGroupTranslation_ID = table.Column<byte>(type: "tinyint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SlideShowGroup_Id = table.Column<byte>(type: "tinyint", nullable: false),
                    Title = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Language_Id = table.Column<string>(type: "nvarchar(10)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SlideShowGroupTranslation_tb", x => x.SlideShowGroupTranslation_ID);
                    table.ForeignKey(
                        name: "FK_SlideShowGroupTranslation_tb_Language_tb_Language_Id",
                        column: x => x.Language_Id,
                        principalTable: "Language_tb",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SlideShowGroupTranslation_tb_SlideShowGroup_tb_SlideShowGroup_Id",
                        column: x => x.SlideShowGroup_Id,
                        principalTable: "SlideShowGroup_tb",
                        principalColumn: "SlideShowGroup_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SocialNetworkTranslation_tb",
                columns: table => new
                {
                    SocialNetworkTranslation_ID = table.Column<short>(type: "smallint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SocialNetwork_Id = table.Column<short>(type: "smallint", nullable: false),
                    Title = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    Language_Id = table.Column<string>(type: "nvarchar(10)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SocialNetworkTranslation_tb", x => x.SocialNetworkTranslation_ID);
                    table.ForeignKey(
                        name: "FK_SocialNetworkTranslation_tb_Language_tb_Language_Id",
                        column: x => x.Language_Id,
                        principalTable: "Language_tb",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SocialNetworkTranslation_tb_SocialNetwork_tb_SocialNetwork_Id",
                        column: x => x.SocialNetwork_Id,
                        principalTable: "SocialNetwork_tb",
                        principalColumn: "SocialNetwork_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ConnectionTranslation_tb",
                columns: table => new
                {
                    Id = table.Column<short>(type: "smallint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ConnectionId = table.Column<byte>(type: "tinyint", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Value = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    LanguageId = table.Column<string>(type: "nvarchar(10)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ConnectionTranslation_tb", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ConnectionTranslation_tb_Connection_tb_ConnectionId",
                        column: x => x.ConnectionId,
                        principalTable: "Connection_tb",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ConnectionTranslation_tb_Language_tb_LanguageId",
                        column: x => x.LanguageId,
                        principalTable: "Language_tb",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "City_tb",
                columns: table => new
                {
                    City_ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Code = table.Column<string>(type: "varchar(10)", nullable: true),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    State_Id = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_City_tb", x => x.City_ID);
                    table.ForeignKey(
                        name: "FK_City_tb_State_tb_State_Id",
                        column: x => x.State_Id,
                        principalTable: "State_tb",
                        principalColumn: "State_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "FeatureReply_tb",
                columns: table => new
                {
                    FeatureReply_ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Feature_Id = table.Column<int>(type: "int", nullable: false),
                    IsDefault = table.Column<bool>(type: "bit", nullable: false),
                    ColorCode = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FeatureReply_tb", x => x.FeatureReply_ID);
                    table.ForeignKey(
                        name: "FK_FeatureReply_tb_Feature_tb_Feature_Id",
                        column: x => x.Feature_Id,
                        principalTable: "Feature_tb",
                        principalColumn: "Feature_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "FeatureTranslation_tb",
                columns: table => new
                {
                    FeatureTranslation_ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Feature_Id = table.Column<int>(type: "int", nullable: false),
                    Title = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    Language_Id = table.Column<string>(type: "nvarchar(10)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FeatureTranslation_tb", x => x.FeatureTranslation_ID);
                    table.ForeignKey(
                        name: "FK_FeatureTranslation_tb_Feature_tb_Feature_Id",
                        column: x => x.Feature_Id,
                        principalTable: "Feature_tb",
                        principalColumn: "Feature_ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_FeatureTranslation_tb_Language_tb_Language_Id",
                        column: x => x.Language_Id,
                        principalTable: "Language_tb",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ContentTranslation_tb",
                columns: table => new
                {
                    ContentTranslation_ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Content_Id = table.Column<int>(type: "int", nullable: false),
                    Title = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    Slug = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    Summery = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true),
                    ContentText = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    MetaTitle = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true),
                    MetaKeword = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true),
                    MetaDescription = table.Column<string>(type: "nvarchar(max)", maxLength: 5000, nullable: true),
                    MetaOther = table.Column<string>(type: "nvarchar(max)", maxLength: 5000, nullable: true),
                    Tag = table.Column<string>(type: "nvarchar(1000)", maxLength: 1000, nullable: true),
                    VisitCount = table.Column<short>(type: "smallint", nullable: false),
                    LikeCount = table.Column<short>(type: "smallint", nullable: false),
                    Author = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    Votes = table.Column<short>(type: "smallint", nullable: false),
                    SumVotes = table.Column<short>(type: "smallint", nullable: false),
                    AvgVotes = table.Column<short>(type: "smallint", nullable: false),
                    Language_Id = table.Column<string>(type: "nvarchar(10)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContentTranslation_tb", x => x.ContentTranslation_ID);
                    table.ForeignKey(
                        name: "FK_ContentTranslation_tb_Content_tb_Content_Id",
                        column: x => x.Content_Id,
                        principalTable: "Content_tb",
                        principalColumn: "Content_ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ContentTranslation_tb_Language_tb_Language_Id",
                        column: x => x.Language_Id,
                        principalTable: "Language_tb",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "GalleryPictureTranslation_tb",
                columns: table => new
                {
                    GalleryPicTranslation_ID = table.Column<string>(type: "nvarchar(450)", maxLength: 450, nullable: false),
                    GalleryPic_Id = table.Column<string>(type: "nvarchar(450)", maxLength: 450, nullable: true),
                    Title = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GalleryPictureTranslation_tb", x => x.GalleryPicTranslation_ID);
                    table.ForeignKey(
                        name: "FK_GalleryPictureTranslation_tb_GalleryPicture_tb_GalleryPic_Id",
                        column: x => x.GalleryPic_Id,
                        principalTable: "GalleryPicture_tb",
                        principalColumn: "GalleryPic_ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SimilarProduct_tb",
                columns: table => new
                {
                    ID = table.Column<string>(type: "nvarchar(450)", maxLength: 450, nullable: false),
                    Product_Id = table.Column<int>(type: "int", nullable: false),
                    ProductSimilar_Id = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SimilarProduct_tb", x => x.ID);
                    table.ForeignKey(
                        name: "FK_SimilarProduct_tb_Product_tb_Product_Id",
                        column: x => x.Product_Id,
                        principalTable: "Product_tb",
                        principalColumn: "Product_ID");
                    table.ForeignKey(
                        name: "FK_SimilarProduct_tb_Product_tb_ProductSimilar_Id",
                        column: x => x.ProductSimilar_Id,
                        principalTable: "Product_tb",
                        principalColumn: "Product_ID");
                });

            migrationBuilder.CreateTable(
                name: "SuggestProduct_tb",
                columns: table => new
                {
                    ID = table.Column<string>(type: "nvarchar(450)", maxLength: 450, nullable: false),
                    Product_Id = table.Column<int>(type: "int", nullable: false),
                    ProductSuggest_Id = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SuggestProduct_tb", x => x.ID);
                    table.ForeignKey(
                        name: "FK_SuggestProduct_tb_Product_tb_Product_Id",
                        column: x => x.Product_Id,
                        principalTable: "Product_tb",
                        principalColumn: "Product_ID");
                    table.ForeignKey(
                        name: "FK_SuggestProduct_tb_Product_tb_ProductSuggest_Id",
                        column: x => x.ProductSuggest_Id,
                        principalTable: "Product_tb",
                        principalColumn: "Product_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CoWorkerTranslation_tb",
                columns: table => new
                {
                    CoWorkerTranslation_ID = table.Column<short>(type: "smallint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CoWorker_Id = table.Column<short>(type: "smallint", nullable: false),
                    TranslationGroup_Id = table.Column<byte>(type: "tinyint", nullable: false),
                    Title = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true),
                    Link = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    Language_Id = table.Column<string>(type: "nvarchar(10)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CoWorkerTranslation_tb", x => x.CoWorkerTranslation_ID);
                    table.ForeignKey(
                        name: "FK_CoWorkerTranslation_tb_CoWorker_tb_CoWorker_Id",
                        column: x => x.CoWorker_Id,
                        principalTable: "CoWorker_tb",
                        principalColumn: "CoWorker_ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CoWorkerTranslation_tb_CoWorkerGroupTranslation_tb_TranslationGroup_Id",
                        column: x => x.TranslationGroup_Id,
                        principalTable: "CoWorkerGroupTranslation_tb",
                        principalColumn: "CoWorkerGroupTranslation_ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CoWorkerTranslation_tb_Language_tb_Language_Id",
                        column: x => x.Language_Id,
                        principalTable: "Language_tb",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ProductTranslation_tb",
                columns: table => new
                {
                    ProductTranslation_ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Product_Id = table.Column<int>(type: "int", nullable: false),
                    Discount_Id = table.Column<short>(type: "smallint", nullable: true),
                    Title = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    Slug = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    MetaTitle = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true),
                    MetaKeword = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true),
                    MetaDescription = table.Column<string>(type: "nvarchar(max)", maxLength: 5000, nullable: true),
                    MetaOther = table.Column<string>(type: "nvarchar(max)", maxLength: 5000, nullable: true),
                    Tag = table.Column<string>(type: "nvarchar(1000)", maxLength: 1000, nullable: true),
                    Language_Id = table.Column<string>(type: "nvarchar(10)", nullable: true),
                    IsActive = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductTranslation_tb", x => x.ProductTranslation_ID);
                    table.ForeignKey(
                        name: "FK_ProductTranslation_tb_Discount_tb_Discount_Id",
                        column: x => x.Discount_Id,
                        principalTable: "Discount_tb",
                        principalColumn: "Discount_ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ProductTranslation_tb_Language_tb_Language_Id",
                        column: x => x.Language_Id,
                        principalTable: "Language_tb",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ProductTranslation_tb_Product_tb_Product_Id",
                        column: x => x.Product_Id,
                        principalTable: "Product_tb",
                        principalColumn: "Product_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SubDiscount_tb",
                columns: table => new
                {
                    SubDiscount_ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Discount_Id = table.Column<short>(type: "smallint", nullable: false),
                    Title = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    IsUse = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubDiscount_tb", x => x.SubDiscount_ID);
                    table.ForeignKey(
                        name: "FK_SubDiscount_tb_Discount_tb_Discount_Id",
                        column: x => x.Discount_Id,
                        principalTable: "Discount_tb",
                        principalColumn: "Discount_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "GalleryTranslation_tb",
                columns: table => new
                {
                    GalleryTranslation_ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Gallery_Id = table.Column<short>(type: "smallint", nullable: false),
                    GroupTranslation_Id = table.Column<short>(type: "smallint", nullable: false),
                    Title = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Slug = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    Language_Id = table.Column<string>(type: "nvarchar(10)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GalleryTranslation_tb", x => x.GalleryTranslation_ID);
                    table.ForeignKey(
                        name: "FK_GalleryTranslation_tb_Gallery_tb_Gallery_Id",
                        column: x => x.Gallery_Id,
                        principalTable: "Gallery_tb",
                        principalColumn: "Gallery_ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_GalleryTranslation_tb_GalleryGroupTranslation_tb_GroupTranslation_Id",
                        column: x => x.GroupTranslation_Id,
                        principalTable: "GalleryGroupTranslation_tb",
                        principalColumn: "GalleryGroupTranslation_ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_GalleryTranslation_tb_Language_tb_Language_Id",
                        column: x => x.Language_Id,
                        principalTable: "Language_tb",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "FileManageTranslation_tb",
                columns: table => new
                {
                    FileManageTranslation_ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FileManage_Id = table.Column<int>(type: "int", nullable: false),
                    TranslationGroup_Id = table.Column<int>(type: "int", nullable: false),
                    Title = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Slug = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    MetaTitle = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true),
                    MetaKeword = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true),
                    MetaDescription = table.Column<string>(type: "nvarchar(max)", maxLength: 5000, nullable: true),
                    MetaOther = table.Column<string>(type: "nvarchar(max)", maxLength: 5000, nullable: true),
                    Language_Id = table.Column<string>(type: "nvarchar(10)", nullable: true),
                    SiteMap_Id = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FileManageTranslation_tb", x => x.FileManageTranslation_ID);
                    table.ForeignKey(
                        name: "FK_FileManageTranslation_tb_FileManage_tb_FileManage_Id",
                        column: x => x.FileManage_Id,
                        principalTable: "FileManage_tb",
                        principalColumn: "FileManage_ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_FileManageTranslation_tb_FileManageGroupTranslation_tb_TranslationGroup_Id",
                        column: x => x.TranslationGroup_Id,
                        principalTable: "FileManageGroupTranslation_tb",
                        principalColumn: "FileManageGroupTranslation_ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_FileManageTranslation_tb_Language_tb_Language_Id",
                        column: x => x.Language_Id,
                        principalTable: "Language_tb",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_FileManageTranslation_tb_MainSiteMap_tb_SiteMap_Id",
                        column: x => x.SiteMap_Id,
                        principalTable: "MainSiteMap_tb",
                        principalColumn: "SiteMap_ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ProductGroupFeature_tb",
                columns: table => new
                {
                    ProductGroupFeature_ID = table.Column<string>(type: "nvarchar(450)", maxLength: 450, nullable: false),
                    Feature_Id = table.Column<int>(type: "int", nullable: false),
                    ProductGroup_Id = table.Column<short>(type: "smallint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductGroupFeature_tb", x => x.ProductGroupFeature_ID);
                    table.ForeignKey(
                        name: "FK_ProductGroupFeature_tb_Feature_tb_Feature_Id",
                        column: x => x.Feature_Id,
                        principalTable: "Feature_tb",
                        principalColumn: "Feature_ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProductGroupFeature_tb_ProductGroup_tb_ProductGroup_Id",
                        column: x => x.ProductGroup_Id,
                        principalTable: "ProductGroup_tb",
                        principalColumn: "ProductGroup_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProductGroupTranslation_tb",
                columns: table => new
                {
                    ProductGroupTranslation_ID = table.Column<short>(type: "smallint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ProductGroup_Id = table.Column<short>(type: "smallint", nullable: false),
                    Discount_Id = table.Column<short>(type: "smallint", nullable: true),
                    Title = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    Slug = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    Language_Id = table.Column<string>(type: "nvarchar(10)", nullable: true),
                    MainSiteMapSiteMap_ID = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductGroupTranslation_tb", x => x.ProductGroupTranslation_ID);
                    table.ForeignKey(
                        name: "FK_ProductGroupTranslation_tb_Discount_tb_Discount_Id",
                        column: x => x.Discount_Id,
                        principalTable: "Discount_tb",
                        principalColumn: "Discount_ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ProductGroupTranslation_tb_Language_tb_Language_Id",
                        column: x => x.Language_Id,
                        principalTable: "Language_tb",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ProductGroupTranslation_tb_MainSiteMap_tb_MainSiteMapSiteMap_ID",
                        column: x => x.MainSiteMapSiteMap_ID,
                        principalTable: "MainSiteMap_tb",
                        principalColumn: "SiteMap_ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ProductGroupTranslation_tb_ProductGroup_tb_ProductGroup_Id",
                        column: x => x.ProductGroup_Id,
                        principalTable: "ProductGroup_tb",
                        principalColumn: "ProductGroup_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProductMiddleGroup_tb",
                columns: table => new
                {
                    ID = table.Column<string>(type: "nvarchar(450)", maxLength: 450, nullable: false),
                    ProductGroup_Id = table.Column<short>(type: "smallint", nullable: false),
                    Product_Id = table.Column<int>(type: "int", nullable: false),
                    SiteMap_Id = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductMiddleGroup_tb", x => x.ID);
                    table.ForeignKey(
                        name: "FK_ProductMiddleGroup_tb_MainSiteMap_tb_SiteMap_Id",
                        column: x => x.SiteMap_Id,
                        principalTable: "MainSiteMap_tb",
                        principalColumn: "SiteMap_ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProductMiddleGroup_tb_Product_tb_Product_Id",
                        column: x => x.Product_Id,
                        principalTable: "Product_tb",
                        principalColumn: "Product_ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProductMiddleGroup_tb_ProductGroup_tb_ProductGroup_Id",
                        column: x => x.ProductGroup_Id,
                        principalTable: "ProductGroup_tb",
                        principalColumn: "ProductGroup_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SlideShowTranslation_tb",
                columns: table => new
                {
                    SlideShowTranslation_ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SlideShow_Id = table.Column<short>(type: "smallint", nullable: false),
                    TranslationGroup_Id = table.Column<byte>(type: "tinyint", nullable: false),
                    Title = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    Text = table.Column<string>(type: "nvarchar(900)", maxLength: 900, nullable: true),
                    Link = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    Language_Id = table.Column<string>(type: "nvarchar(10)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SlideShowTranslation_tb", x => x.SlideShowTranslation_ID);
                    table.ForeignKey(
                        name: "FK_SlideShowTranslation_tb_Language_tb_Language_Id",
                        column: x => x.Language_Id,
                        principalTable: "Language_tb",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SlideShowTranslation_tb_SlideShow_tb_SlideShow_Id",
                        column: x => x.SlideShow_Id,
                        principalTable: "SlideShow_tb",
                        principalColumn: "SlideShow_ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SlideShowTranslation_tb_SlideShowGroupTranslation_tb_TranslationGroup_Id",
                        column: x => x.TranslationGroup_Id,
                        principalTable: "SlideShowGroupTranslation_tb",
                        principalColumn: "SlideShowGroupTranslation_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "FeatureReplyTranslation_tb",
                columns: table => new
                {
                    FeatureReplyTranslation_ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FeatureReply_Id = table.Column<int>(type: "int", nullable: false),
                    Title = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    Language_Id = table.Column<string>(type: "nvarchar(10)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FeatureReplyTranslation_tb", x => x.FeatureReplyTranslation_ID);
                    table.ForeignKey(
                        name: "FK_FeatureReplyTranslation_tb_FeatureReply_tb_FeatureReply_Id",
                        column: x => x.FeatureReply_Id,
                        principalTable: "FeatureReply_tb",
                        principalColumn: "FeatureReply_ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_FeatureReplyTranslation_tb_Language_tb_Language_Id",
                        column: x => x.Language_Id,
                        principalTable: "Language_tb",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProductFeature_tb",
                columns: table => new
                {
                    ProductFeature_ID = table.Column<string>(type: "nvarchar(450)", maxLength: 450, nullable: false),
                    Feature_Id = table.Column<int>(type: "int", nullable: false),
                    Product_Id = table.Column<int>(type: "int", nullable: false),
                    FeatureReply_Id = table.Column<int>(type: "int", nullable: false),
                    IsVariable = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductFeature_tb", x => x.ProductFeature_ID);
                    table.ForeignKey(
                        name: "FK_ProductFeature_tb_Feature_tb_Feature_Id",
                        column: x => x.Feature_Id,
                        principalTable: "Feature_tb",
                        principalColumn: "Feature_ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProductFeature_tb_FeatureReply_tb_FeatureReply_Id",
                        column: x => x.FeatureReply_Id,
                        principalTable: "FeatureReply_tb",
                        principalColumn: "FeatureReply_ID");
                    table.ForeignKey(
                        name: "FK_ProductFeature_tb_Product_tb_Product_Id",
                        column: x => x.Product_Id,
                        principalTable: "Product_tb",
                        principalColumn: "Product_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ContentMiddleGroup_tb",
                columns: table => new
                {
                    ID = table.Column<string>(type: "nvarchar(450)", maxLength: 450, nullable: false),
                    ContentGroupTranslation_Id = table.Column<short>(type: "smallint", nullable: false),
                    ContentTranslation_Id = table.Column<int>(type: "int", nullable: false),
                    SiteMap_Id = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContentMiddleGroup_tb", x => x.ID);
                    table.ForeignKey(
                        name: "FK_ContentMiddleGroup_tb_ContentGroupTranslation_tb_ContentGroupTranslation_Id",
                        column: x => x.ContentGroupTranslation_Id,
                        principalTable: "ContentGroupTranslation_tb",
                        principalColumn: "ContentGroupTranslation_ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ContentMiddleGroup_tb_ContentTranslation_tb_ContentTranslation_Id",
                        column: x => x.ContentTranslation_Id,
                        principalTable: "ContentTranslation_tb",
                        principalColumn: "ContentTranslation_ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ContentMiddleGroup_tb_MainSiteMap_tb_SiteMap_Id",
                        column: x => x.SiteMap_Id,
                        principalTable: "MainSiteMap_tb",
                        principalColumn: "SiteMap_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Variation_tb",
                columns: table => new
                {
                    Variation_ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ProductTranslation_Id = table.Column<int>(type: "int", nullable: false),
                    Title = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Variation_tb", x => x.Variation_ID);
                    table.ForeignKey(
                        name: "FK_Variation_tb_ProductTranslation_tb_ProductTranslation_Id",
                        column: x => x.ProductTranslation_Id,
                        principalTable: "ProductTranslation_tb",
                        principalColumn: "ProductTranslation_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "FeatureReplyVariation_tb",
                columns: table => new
                {
                    FeatureReplyVariation_ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FeatureReply_Id = table.Column<int>(type: "int", nullable: false),
                    Variation_Id = table.Column<int>(type: "int", nullable: false),
                    Feature_Id = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FeatureReplyVariation_tb", x => x.FeatureReplyVariation_ID);
                    table.ForeignKey(
                        name: "FK_FeatureReplyVariation_tb_Feature_tb_Feature_Id",
                        column: x => x.Feature_Id,
                        principalTable: "Feature_tb",
                        principalColumn: "Feature_ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_FeatureReplyVariation_tb_FeatureReply_tb_FeatureReply_Id",
                        column: x => x.FeatureReply_Id,
                        principalTable: "FeatureReply_tb",
                        principalColumn: "FeatureReply_ID");
                    table.ForeignKey(
                        name: "FK_FeatureReplyVariation_tb_Variation_tb_Variation_Id",
                        column: x => x.Variation_Id,
                        principalTable: "Variation_tb",
                        principalColumn: "Variation_ID");
                });

            migrationBuilder.CreateTable(
                name: "ProductPriceList_tb",
                columns: table => new
                {
                    ProductPriceList_ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PriceList_Id = table.Column<int>(type: "int", nullable: false),
                    ProductTranslation_Id = table.Column<int>(type: "int", nullable: false),
                    Variation_Id = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductPriceList_tb", x => x.ProductPriceList_ID);
                    table.ForeignKey(
                        name: "FK_ProductPriceList_tb_PriceList_tb_PriceList_Id",
                        column: x => x.PriceList_Id,
                        principalTable: "PriceList_tb",
                        principalColumn: "PriceList_ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProductPriceList_tb_ProductTranslation_tb_ProductTranslation_Id",
                        column: x => x.ProductTranslation_Id,
                        principalTable: "ProductTranslation_tb",
                        principalColumn: "ProductTranslation_ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProductPriceList_tb_Variation_tb_Variation_Id",
                        column: x => x.Variation_Id,
                        principalTable: "Variation_tb",
                        principalColumn: "Variation_ID");
                });

            migrationBuilder.CreateTable(
                name: "ProductPriceListHistory_tb",
                columns: table => new
                {
                    ProductPriceListHistory_ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ProductPriceList_ID = table.Column<int>(type: "int", nullable: false),
                    PriceList_Id = table.Column<int>(type: "int", nullable: false),
                    ProductTranslation_Id = table.Column<int>(type: "int", nullable: false),
                    Variation_Id = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductPriceListHistory_tb", x => x.ProductPriceListHistory_ID);
                    table.ForeignKey(
                        name: "FK_ProductPriceListHistory_tb_PriceList_tb_PriceList_Id",
                        column: x => x.PriceList_Id,
                        principalTable: "PriceList_tb",
                        principalColumn: "PriceList_ID");
                    table.ForeignKey(
                        name: "FK_ProductPriceListHistory_tb_ProductPriceList_tb_ProductPriceList_ID",
                        column: x => x.ProductPriceList_ID,
                        principalTable: "ProductPriceList_tb",
                        principalColumn: "ProductPriceList_ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProductPriceListHistory_tb_ProductTranslation_tb_ProductTranslation_Id",
                        column: x => x.ProductTranslation_Id,
                        principalTable: "ProductTranslation_tb",
                        principalColumn: "ProductTranslation_ID");
                    table.ForeignKey(
                        name: "FK_ProductPriceListHistory_tb_Variation_tb_Variation_Id",
                        column: x => x.Variation_Id,
                        principalTable: "Variation_tb",
                        principalColumn: "Variation_ID");
                });

            migrationBuilder.InsertData(
                table: "ContentGroup_tb",
                columns: new[] { "ContentGroup_ID", "ContentImageHeight", "ContentImageWidth", "IsActive", "ParentId" },
                values: new object[] { (byte)1, "0", "0", true, null });

            migrationBuilder.InsertData(
                table: "FeatureGroup_tb",
                column: "FeatureGroup_ID",
                value: (short)1);

            migrationBuilder.InsertData(
                table: "FileManageGroup_tb",
                columns: new[] { "FileManageGroup_ID", "IsActive", "Parent_Id" },
                values: new object[] { (short)1, true, null });

            migrationBuilder.InsertData(
                table: "Language_tb",
                columns: new[] { "Id", "IsActive", "Name" },
                values: new object[,]
                {
                    { "fa-IR", true, "زبان فارسی" },
                    { "en-US", true, "English" }
                });

            migrationBuilder.InsertData(
                table: "ProductGroup_tb",
                columns: new[] { "ProductGroup_ID", "Image", "ImageHeight", "ImageWidth", "ParentId", "SiteMap_Id", "SmallImageHeight", "SmallImageWidth" },
                values: new object[] { (short)1, null, "0", "0", null, null, "0", "0" });

            migrationBuilder.InsertData(
                table: "ContentGroupTranslation_tb",
                columns: new[] { "ContentGroupTranslation_ID", "ContentGroup_Id", "Description", "Language_Id", "SiteMap_Id", "Slug", "Title" },
                values: new object[,]
                {
                    { (short)1, (byte)1, null, "fa-IR", null, "گروه-پیش-فرض", "گروه پیش فرض" },
                    { (short)2, (byte)1, null, "en-US", null, "Default-Group", "Default-Group" }
                });

            migrationBuilder.InsertData(
                table: "FeatureGroupTranslation_tb",
                columns: new[] { "FeatureGroupTranslation_ID", "FeatureGroup_Id", "Language_Id", "Title" },
                values: new object[,]
                {
                    { (short)1, (short)1, "fa-IR", "مشخصات کلی" },
                    { (short)2, (short)1, "en-US", "Genral Features" }
                });

            migrationBuilder.InsertData(
                table: "Feature_tb",
                columns: new[] { "Feature_ID", "FeatureGroup_Id", "IsVariable", "UseFilter" },
                values: new object[] { 1, (short)1, true, true });

            migrationBuilder.InsertData(
                table: "FileManageGroupTranslation_tb",
                columns: new[] { "FileManageGroupTranslation_ID", "FileManageGroup_Id", "Language_Id", "SiteMap_Id", "Slug", "Title" },
                values: new object[,]
                {
                    { 1, (short)1, "fa-IR", null, "گروه-پیش-فرض", "گروه پیش فرض" },
                    { 2, (short)1, "en-US", null, "Default-Group", "Default-Group" }
                });

            migrationBuilder.InsertData(
                table: "ProductGroupTranslation_tb",
                columns: new[] { "ProductGroupTranslation_ID", "Description", "Discount_Id", "IsActive", "Language_Id", "MainSiteMapSiteMap_ID", "ProductGroup_Id", "Slug", "Title" },
                values: new object[,]
                {
                    { (short)2, null, null, true, "en-US", null, (short)1, "Products-Group", "Products-Group" },
                    { (short)1, null, null, true, "fa-IR", null, (short)1, "گروه-محصولات", "گروه محصولات" }
                });

            migrationBuilder.InsertData(
                table: "FeatureTranslation_tb",
                columns: new[] { "FeatureTranslation_ID", "Feature_Id", "Language_Id", "Title" },
                values: new object[] { 2, 1, "en-US", "Colors" });

            migrationBuilder.InsertData(
                table: "FeatureTranslation_tb",
                columns: new[] { "FeatureTranslation_ID", "Feature_Id", "Language_Id", "Title" },
                values: new object[] { 1, 1, "fa-IR", "رنگ ها" });

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true,
                filter: "[NormalizedName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true,
                filter: "[NormalizedUserName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_City_tb_State_Id",
                table: "City_tb",
                column: "State_Id");

            migrationBuilder.CreateIndex(
                name: "IX_ColorTranslation_tb_ColorId",
                table: "ColorTranslation_tb",
                column: "ColorId");

            migrationBuilder.CreateIndex(
                name: "IX_ColorTranslation_tb_LanguageId",
                table: "ColorTranslation_tb",
                column: "LanguageId");

            migrationBuilder.CreateIndex(
                name: "IX_Connection_tb_GroupId",
                table: "Connection_tb",
                column: "GroupId");

            migrationBuilder.CreateIndex(
                name: "IX_ConnectionTranslation_tb_ConnectionId",
                table: "ConnectionTranslation_tb",
                column: "ConnectionId");

            migrationBuilder.CreateIndex(
                name: "IX_ConnectionTranslation_tb_LanguageId",
                table: "ConnectionTranslation_tb",
                column: "LanguageId");

            migrationBuilder.CreateIndex(
                name: "IX_Content_tb_Gallery_Id",
                table: "Content_tb",
                column: "Gallery_Id");

            migrationBuilder.CreateIndex(
                name: "IX_ContentGroup_tb_ParentId",
                table: "ContentGroup_tb",
                column: "ParentId");

            migrationBuilder.CreateIndex(
                name: "IX_ContentGroupTranslation_tb_ContentGroup_Id",
                table: "ContentGroupTranslation_tb",
                column: "ContentGroup_Id");

            migrationBuilder.CreateIndex(
                name: "IX_ContentGroupTranslation_tb_Language_Id",
                table: "ContentGroupTranslation_tb",
                column: "Language_Id");

            migrationBuilder.CreateIndex(
                name: "IX_ContentGroupTranslation_tb_SiteMap_Id",
                table: "ContentGroupTranslation_tb",
                column: "SiteMap_Id");

            migrationBuilder.CreateIndex(
                name: "IX_ContentMiddleGroup_tb_ContentGroupTranslation_Id",
                table: "ContentMiddleGroup_tb",
                column: "ContentGroupTranslation_Id");

            migrationBuilder.CreateIndex(
                name: "IX_ContentMiddleGroup_tb_ContentTranslation_Id",
                table: "ContentMiddleGroup_tb",
                column: "ContentTranslation_Id");

            migrationBuilder.CreateIndex(
                name: "IX_ContentMiddleGroup_tb_SiteMap_Id",
                table: "ContentMiddleGroup_tb",
                column: "SiteMap_Id");

            migrationBuilder.CreateIndex(
                name: "IX_ContentTranslation_tb_Content_Id",
                table: "ContentTranslation_tb",
                column: "Content_Id");

            migrationBuilder.CreateIndex(
                name: "IX_ContentTranslation_tb_Language_Id",
                table: "ContentTranslation_tb",
                column: "Language_Id");

            migrationBuilder.CreateIndex(
                name: "IX_CoWorkerGroupTranslation_tb_CoWorkerGroup_Id",
                table: "CoWorkerGroupTranslation_tb",
                column: "CoWorkerGroup_Id");

            migrationBuilder.CreateIndex(
                name: "IX_CoWorkerGroupTranslation_tb_Language_Id",
                table: "CoWorkerGroupTranslation_tb",
                column: "Language_Id");

            migrationBuilder.CreateIndex(
                name: "IX_CoWorkerTranslation_tb_CoWorker_Id",
                table: "CoWorkerTranslation_tb",
                column: "CoWorker_Id");

            migrationBuilder.CreateIndex(
                name: "IX_CoWorkerTranslation_tb_Language_Id",
                table: "CoWorkerTranslation_tb",
                column: "Language_Id");

            migrationBuilder.CreateIndex(
                name: "IX_CoWorkerTranslation_tb_TranslationGroup_Id",
                table: "CoWorkerTranslation_tb",
                column: "TranslationGroup_Id");

            migrationBuilder.CreateIndex(
                name: "IX_Discount_tb_Language_Id",
                table: "Discount_tb",
                column: "Language_Id");

            migrationBuilder.CreateIndex(
                name: "IX_FAQ_tb_Language_Id",
                table: "FAQ_tb",
                column: "Language_Id");

            migrationBuilder.CreateIndex(
                name: "IX_Feature_tb_FeatureGroup_Id",
                table: "Feature_tb",
                column: "FeatureGroup_Id");

            migrationBuilder.CreateIndex(
                name: "IX_FeatureGroupTranslation_tb_FeatureGroup_Id",
                table: "FeatureGroupTranslation_tb",
                column: "FeatureGroup_Id");

            migrationBuilder.CreateIndex(
                name: "IX_FeatureGroupTranslation_tb_Language_Id",
                table: "FeatureGroupTranslation_tb",
                column: "Language_Id");

            migrationBuilder.CreateIndex(
                name: "IX_FeatureReply_tb_Feature_Id",
                table: "FeatureReply_tb",
                column: "Feature_Id");

            migrationBuilder.CreateIndex(
                name: "IX_FeatureReplyTranslation_tb_FeatureReply_Id",
                table: "FeatureReplyTranslation_tb",
                column: "FeatureReply_Id");

            migrationBuilder.CreateIndex(
                name: "IX_FeatureReplyTranslation_tb_Language_Id",
                table: "FeatureReplyTranslation_tb",
                column: "Language_Id");

            migrationBuilder.CreateIndex(
                name: "IX_FeatureReplyVariation_tb_Feature_Id",
                table: "FeatureReplyVariation_tb",
                column: "Feature_Id");

            migrationBuilder.CreateIndex(
                name: "IX_FeatureReplyVariation_tb_FeatureReply_Id",
                table: "FeatureReplyVariation_tb",
                column: "FeatureReply_Id");

            migrationBuilder.CreateIndex(
                name: "IX_FeatureReplyVariation_tb_Variation_Id",
                table: "FeatureReplyVariation_tb",
                column: "Variation_Id");

            migrationBuilder.CreateIndex(
                name: "IX_FeatureTranslation_tb_Feature_Id",
                table: "FeatureTranslation_tb",
                column: "Feature_Id");

            migrationBuilder.CreateIndex(
                name: "IX_FeatureTranslation_tb_Language_Id",
                table: "FeatureTranslation_tb",
                column: "Language_Id");

            migrationBuilder.CreateIndex(
                name: "IX_FileManageGroup_tb_Parent_Id",
                table: "FileManageGroup_tb",
                column: "Parent_Id");

            migrationBuilder.CreateIndex(
                name: "IX_FileManageGroupTranslation_tb_FileManageGroup_Id",
                table: "FileManageGroupTranslation_tb",
                column: "FileManageGroup_Id");

            migrationBuilder.CreateIndex(
                name: "IX_FileManageGroupTranslation_tb_Language_Id",
                table: "FileManageGroupTranslation_tb",
                column: "Language_Id");

            migrationBuilder.CreateIndex(
                name: "IX_FileManageGroupTranslation_tb_SiteMap_Id",
                table: "FileManageGroupTranslation_tb",
                column: "SiteMap_Id");

            migrationBuilder.CreateIndex(
                name: "IX_FileManageTranslation_tb_FileManage_Id",
                table: "FileManageTranslation_tb",
                column: "FileManage_Id");

            migrationBuilder.CreateIndex(
                name: "IX_FileManageTranslation_tb_Language_Id",
                table: "FileManageTranslation_tb",
                column: "Language_Id");

            migrationBuilder.CreateIndex(
                name: "IX_FileManageTranslation_tb_SiteMap_Id",
                table: "FileManageTranslation_tb",
                column: "SiteMap_Id");

            migrationBuilder.CreateIndex(
                name: "IX_FileManageTranslation_tb_TranslationGroup_Id",
                table: "FileManageTranslation_tb",
                column: "TranslationGroup_Id");

            migrationBuilder.CreateIndex(
                name: "IX_GalleryGroupTranslation_tb_GalleryGroup_Id",
                table: "GalleryGroupTranslation_tb",
                column: "GalleryGroup_Id");

            migrationBuilder.CreateIndex(
                name: "IX_GalleryGroupTranslation_tb_Language_Id",
                table: "GalleryGroupTranslation_tb",
                column: "Language_Id");

            migrationBuilder.CreateIndex(
                name: "IX_GalleryPicture_tb_Gallery_Id",
                table: "GalleryPicture_tb",
                column: "Gallery_Id");

            migrationBuilder.CreateIndex(
                name: "IX_GalleryPictureTranslation_tb_GalleryPic_Id",
                table: "GalleryPictureTranslation_tb",
                column: "GalleryPic_Id");

            migrationBuilder.CreateIndex(
                name: "IX_GalleryTranslation_tb_Gallery_Id",
                table: "GalleryTranslation_tb",
                column: "Gallery_Id");

            migrationBuilder.CreateIndex(
                name: "IX_GalleryTranslation_tb_GroupTranslation_Id",
                table: "GalleryTranslation_tb",
                column: "GroupTranslation_Id");

            migrationBuilder.CreateIndex(
                name: "IX_GalleryTranslation_tb_Language_Id",
                table: "GalleryTranslation_tb",
                column: "Language_Id");

            migrationBuilder.CreateIndex(
                name: "IX_PriceList_tb_Currency_Id",
                table: "PriceList_tb",
                column: "Currency_Id");

            migrationBuilder.CreateIndex(
                name: "IX_Product_tb_Gallery_Id",
                table: "Product_tb",
                column: "Gallery_Id");

            migrationBuilder.CreateIndex(
                name: "IX_ProductFeature_tb_Feature_Id",
                table: "ProductFeature_tb",
                column: "Feature_Id");

            migrationBuilder.CreateIndex(
                name: "IX_ProductFeature_tb_FeatureReply_Id",
                table: "ProductFeature_tb",
                column: "FeatureReply_Id");

            migrationBuilder.CreateIndex(
                name: "IX_ProductFeature_tb_Product_Id",
                table: "ProductFeature_tb",
                column: "Product_Id");

            migrationBuilder.CreateIndex(
                name: "IX_ProductGroup_tb_ParentId",
                table: "ProductGroup_tb",
                column: "ParentId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductGroup_tb_SiteMap_Id",
                table: "ProductGroup_tb",
                column: "SiteMap_Id");

            migrationBuilder.CreateIndex(
                name: "IX_ProductGroupFeature_tb_Feature_Id",
                table: "ProductGroupFeature_tb",
                column: "Feature_Id");

            migrationBuilder.CreateIndex(
                name: "IX_ProductGroupFeature_tb_ProductGroup_Id",
                table: "ProductGroupFeature_tb",
                column: "ProductGroup_Id");

            migrationBuilder.CreateIndex(
                name: "IX_ProductGroupTranslation_tb_Discount_Id",
                table: "ProductGroupTranslation_tb",
                column: "Discount_Id");

            migrationBuilder.CreateIndex(
                name: "IX_ProductGroupTranslation_tb_Language_Id",
                table: "ProductGroupTranslation_tb",
                column: "Language_Id");

            migrationBuilder.CreateIndex(
                name: "IX_ProductGroupTranslation_tb_MainSiteMapSiteMap_ID",
                table: "ProductGroupTranslation_tb",
                column: "MainSiteMapSiteMap_ID");

            migrationBuilder.CreateIndex(
                name: "IX_ProductGroupTranslation_tb_ProductGroup_Id",
                table: "ProductGroupTranslation_tb",
                column: "ProductGroup_Id");

            migrationBuilder.CreateIndex(
                name: "IX_ProductMiddleGroup_tb_Product_Id",
                table: "ProductMiddleGroup_tb",
                column: "Product_Id");

            migrationBuilder.CreateIndex(
                name: "IX_ProductMiddleGroup_tb_ProductGroup_Id",
                table: "ProductMiddleGroup_tb",
                column: "ProductGroup_Id");

            migrationBuilder.CreateIndex(
                name: "IX_ProductMiddleGroup_tb_SiteMap_Id",
                table: "ProductMiddleGroup_tb",
                column: "SiteMap_Id");

            migrationBuilder.CreateIndex(
                name: "IX_ProductPriceList_tb_PriceList_Id",
                table: "ProductPriceList_tb",
                column: "PriceList_Id");

            migrationBuilder.CreateIndex(
                name: "IX_ProductPriceList_tb_ProductTranslation_Id",
                table: "ProductPriceList_tb",
                column: "ProductTranslation_Id");

            migrationBuilder.CreateIndex(
                name: "IX_ProductPriceList_tb_Variation_Id",
                table: "ProductPriceList_tb",
                column: "Variation_Id");

            migrationBuilder.CreateIndex(
                name: "IX_ProductPriceListHistory_tb_PriceList_Id",
                table: "ProductPriceListHistory_tb",
                column: "PriceList_Id");

            migrationBuilder.CreateIndex(
                name: "IX_ProductPriceListHistory_tb_ProductPriceList_ID",
                table: "ProductPriceListHistory_tb",
                column: "ProductPriceList_ID");

            migrationBuilder.CreateIndex(
                name: "IX_ProductPriceListHistory_tb_ProductTranslation_Id",
                table: "ProductPriceListHistory_tb",
                column: "ProductTranslation_Id");

            migrationBuilder.CreateIndex(
                name: "IX_ProductPriceListHistory_tb_Variation_Id",
                table: "ProductPriceListHistory_tb",
                column: "Variation_Id");

            migrationBuilder.CreateIndex(
                name: "IX_ProductTranslation_tb_Discount_Id",
                table: "ProductTranslation_tb",
                column: "Discount_Id");

            migrationBuilder.CreateIndex(
                name: "IX_ProductTranslation_tb_Language_Id",
                table: "ProductTranslation_tb",
                column: "Language_Id");

            migrationBuilder.CreateIndex(
                name: "IX_ProductTranslation_tb_Product_Id",
                table: "ProductTranslation_tb",
                column: "Product_Id");

            migrationBuilder.CreateIndex(
                name: "IX_SendTypeTranslation_tb_Language_Id",
                table: "SendTypeTranslation_tb",
                column: "Language_Id");

            migrationBuilder.CreateIndex(
                name: "IX_SendTypeTranslation_tb_SendType_Id",
                table: "SendTypeTranslation_tb",
                column: "SendType_Id");

            migrationBuilder.CreateIndex(
                name: "IX_SimilarProduct_tb_Product_Id",
                table: "SimilarProduct_tb",
                column: "Product_Id");

            migrationBuilder.CreateIndex(
                name: "IX_SimilarProduct_tb_ProductSimilar_Id",
                table: "SimilarProduct_tb",
                column: "ProductSimilar_Id");

            migrationBuilder.CreateIndex(
                name: "IX_SlideShowGroupTranslation_tb_Language_Id",
                table: "SlideShowGroupTranslation_tb",
                column: "Language_Id");

            migrationBuilder.CreateIndex(
                name: "IX_SlideShowGroupTranslation_tb_SlideShowGroup_Id",
                table: "SlideShowGroupTranslation_tb",
                column: "SlideShowGroup_Id");

            migrationBuilder.CreateIndex(
                name: "IX_SlideShowTranslation_tb_Language_Id",
                table: "SlideShowTranslation_tb",
                column: "Language_Id");

            migrationBuilder.CreateIndex(
                name: "IX_SlideShowTranslation_tb_SlideShow_Id",
                table: "SlideShowTranslation_tb",
                column: "SlideShow_Id");

            migrationBuilder.CreateIndex(
                name: "IX_SlideShowTranslation_tb_TranslationGroup_Id",
                table: "SlideShowTranslation_tb",
                column: "TranslationGroup_Id");

            migrationBuilder.CreateIndex(
                name: "IX_SocialNetworkTranslation_tb_Language_Id",
                table: "SocialNetworkTranslation_tb",
                column: "Language_Id");

            migrationBuilder.CreateIndex(
                name: "IX_SocialNetworkTranslation_tb_SocialNetwork_Id",
                table: "SocialNetworkTranslation_tb",
                column: "SocialNetwork_Id");

            migrationBuilder.CreateIndex(
                name: "IX_State_tb_CountryId",
                table: "State_tb",
                column: "CountryId");

            migrationBuilder.CreateIndex(
                name: "IX_SubDiscount_tb_Discount_Id",
                table: "SubDiscount_tb",
                column: "Discount_Id");

            migrationBuilder.CreateIndex(
                name: "IX_SuggestProduct_tb_Product_Id",
                table: "SuggestProduct_tb",
                column: "Product_Id");

            migrationBuilder.CreateIndex(
                name: "IX_SuggestProduct_tb_ProductSuggest_Id",
                table: "SuggestProduct_tb",
                column: "ProductSuggest_Id");

            migrationBuilder.CreateIndex(
                name: "IX_Variation_tb_ProductTranslation_Id",
                table: "Variation_tb",
                column: "ProductTranslation_Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "City_tb");

            migrationBuilder.DropTable(
                name: "ColorTranslation_tb");

            migrationBuilder.DropTable(
                name: "ConnectionTranslation_tb");

            migrationBuilder.DropTable(
                name: "ContentMiddleGroup_tb");

            migrationBuilder.DropTable(
                name: "CoWorkerTranslation_tb");

            migrationBuilder.DropTable(
                name: "FAQ_tb");

            migrationBuilder.DropTable(
                name: "FeatureGroupTranslation_tb");

            migrationBuilder.DropTable(
                name: "FeatureReplyTranslation_tb");

            migrationBuilder.DropTable(
                name: "FeatureReplyVariation_tb");

            migrationBuilder.DropTable(
                name: "FeatureTranslation_tb");

            migrationBuilder.DropTable(
                name: "FileManageTranslation_tb");

            migrationBuilder.DropTable(
                name: "GalleryPictureTranslation_tb");

            migrationBuilder.DropTable(
                name: "GalleryTranslation_tb");

            migrationBuilder.DropTable(
                name: "ProductFeature_tb");

            migrationBuilder.DropTable(
                name: "ProductGroupFeature_tb");

            migrationBuilder.DropTable(
                name: "ProductGroupTranslation_tb");

            migrationBuilder.DropTable(
                name: "ProductMiddleGroup_tb");

            migrationBuilder.DropTable(
                name: "ProductPriceListHistory_tb");

            migrationBuilder.DropTable(
                name: "SendTypeTranslation_tb");

            migrationBuilder.DropTable(
                name: "SeoPage_tb");

            migrationBuilder.DropTable(
                name: "SimilarProduct_tb");

            migrationBuilder.DropTable(
                name: "SlideShowTranslation_tb");

            migrationBuilder.DropTable(
                name: "SocialNetworkTranslation_tb");

            migrationBuilder.DropTable(
                name: "SubDiscount_tb");

            migrationBuilder.DropTable(
                name: "SuggestProduct_tb");

            migrationBuilder.DropTable(
                name: "AspNetRoles");

            migrationBuilder.DropTable(
                name: "AspNetUsers");

            migrationBuilder.DropTable(
                name: "State_tb");

            migrationBuilder.DropTable(
                name: "Color_tb");

            migrationBuilder.DropTable(
                name: "Connection_tb");

            migrationBuilder.DropTable(
                name: "ContentGroupTranslation_tb");

            migrationBuilder.DropTable(
                name: "ContentTranslation_tb");

            migrationBuilder.DropTable(
                name: "CoWorker_tb");

            migrationBuilder.DropTable(
                name: "CoWorkerGroupTranslation_tb");

            migrationBuilder.DropTable(
                name: "FileManage_tb");

            migrationBuilder.DropTable(
                name: "FileManageGroupTranslation_tb");

            migrationBuilder.DropTable(
                name: "GalleryPicture_tb");

            migrationBuilder.DropTable(
                name: "GalleryGroupTranslation_tb");

            migrationBuilder.DropTable(
                name: "FeatureReply_tb");

            migrationBuilder.DropTable(
                name: "ProductGroup_tb");

            migrationBuilder.DropTable(
                name: "ProductPriceList_tb");

            migrationBuilder.DropTable(
                name: "SendType_tb");

            migrationBuilder.DropTable(
                name: "SlideShow_tb");

            migrationBuilder.DropTable(
                name: "SlideShowGroupTranslation_tb");

            migrationBuilder.DropTable(
                name: "SocialNetwork_tb");

            migrationBuilder.DropTable(
                name: "Country_tb");

            migrationBuilder.DropTable(
                name: "ConnectionGroup_tb");

            migrationBuilder.DropTable(
                name: "ContentGroup_tb");

            migrationBuilder.DropTable(
                name: "Content_tb");

            migrationBuilder.DropTable(
                name: "CoWorkerGroup_tb");

            migrationBuilder.DropTable(
                name: "FileManageGroup_tb");

            migrationBuilder.DropTable(
                name: "GalleryGroup_tb");

            migrationBuilder.DropTable(
                name: "Feature_tb");

            migrationBuilder.DropTable(
                name: "MainSiteMap_tb");

            migrationBuilder.DropTable(
                name: "PriceList_tb");

            migrationBuilder.DropTable(
                name: "Variation_tb");

            migrationBuilder.DropTable(
                name: "SlideShowGroup_tb");

            migrationBuilder.DropTable(
                name: "FeatureGroup_tb");

            migrationBuilder.DropTable(
                name: "Currency_tb");

            migrationBuilder.DropTable(
                name: "ProductTranslation_tb");

            migrationBuilder.DropTable(
                name: "Discount_tb");

            migrationBuilder.DropTable(
                name: "Product_tb");

            migrationBuilder.DropTable(
                name: "Language_tb");

            migrationBuilder.DropTable(
                name: "Gallery_tb");
        }
    }
}
