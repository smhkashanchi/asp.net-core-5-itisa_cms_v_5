﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Data.Migrations
{
    public partial class modifySimilarProduct : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "SimilarProduct_tb",
                columns: table => new
                {
                    ID = table.Column<string>(type: "nvarchar(450)", maxLength: 450, nullable: false),
                    Product_Id = table.Column<int>(type: "int", nullable: true),
                    ProductSimilar_Id = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SimilarProduct_tb", x => x.ID);
                    table.ForeignKey(
                        name: "FK_SimilarProduct_tb_Product_tb_Product_Id",
                        column: x => x.Product_Id,
                        principalTable: "Product_tb",
                        principalColumn: "Product_ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SimilarProduct_tb_Product_tb_ProductSimilar_Id",
                        column: x => x.ProductSimilar_Id,
                        principalTable: "Product_tb",
                        principalColumn: "Product_ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SuggestProduct_tb",
                columns: table => new
                {
                    ID = table.Column<string>(type: "nvarchar(450)", maxLength: 450, nullable: false),
                    Product_Id = table.Column<int>(type: "int", nullable: true),
                    ProductSuggest_Id = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SuggestProduct_tb", x => x.ID);
                    table.ForeignKey(
                        name: "FK_SuggestProduct_tb_Product_tb_Product_Id",
                        column: x => x.Product_Id,
                        principalTable: "Product_tb",
                        principalColumn: "Product_ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SuggestProduct_tb_Product_tb_ProductSuggest_Id",
                        column: x => x.ProductSuggest_Id,
                        principalTable: "Product_tb",
                        principalColumn: "Product_ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_SimilarProduct_tb_Product_Id",
                table: "SimilarProduct_tb",
                column: "Product_Id");

            migrationBuilder.CreateIndex(
                name: "IX_SimilarProduct_tb_ProductSimilar_Id",
                table: "SimilarProduct_tb",
                column: "ProductSimilar_Id");

            migrationBuilder.CreateIndex(
                name: "IX_SuggestProduct_tb_Product_Id",
                table: "SuggestProduct_tb",
                column: "Product_Id");

            migrationBuilder.CreateIndex(
                name: "IX_SuggestProduct_tb_ProductSuggest_Id",
                table: "SuggestProduct_tb",
                column: "ProductSuggest_Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SimilarProduct_tb");

            migrationBuilder.DropTable(
                name: "SuggestProduct_tb");
        }
    }
}
