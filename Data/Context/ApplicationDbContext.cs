﻿using Domain.DomainClasses;
using Domain.DomainClasses.Color;
using Domain.DomainClasses.Connection;
using Domain.DomainClasses.Content;
using Domain.DomainClasses.CoWorker;
using Domain.DomainClasses.FAQ;
using Domain.DomainClasses.Feature;
using Domain.DomainClasses.FilesManage;
using Domain.DomainClasses.Gallery;
using Domain.DomainClasses.Product;
using Domain.DomainClasses.Region;
using Domain.DomainClasses.SendType;
using Domain.DomainClasses.Seo;
using Domain.DomainClasses.SlideShow;
using Domain.DomainClasses.SocialNetwork;
using Domain.DomainClasses.Variation;

using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Context
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public DbSet<Language> Language_tb { get; set; }
        public DbSet<Color> Color_tb { get; set; }
        public DbSet<ColorTranslation> ColorTranslation_tb { get; set; }
        public DbSet<ConnectionGroup> ConnectionGroup_tb { get; set; }
        public DbSet<Connection> Connection_tb { get; set; }
        public DbSet<ConnectionTranslation> ConnectionTranslation_tb { get; set; }
        public DbSet<MainSiteMap> MainSiteMap_tb { get; set; }
        public DbSet<ContentGroup> ContentGroup_tb { get; set; }
        public DbSet<ContentGroupTranslation> ContentGroupTranslation_tb { get; set; }
        public DbSet<Content> Content_tb { get; set; }
        public DbSet<ContentTranslation> ContentTranslation_tb { get; set; }
        public DbSet<ContentMiddleGroup> ContentMiddleGroup_tb { get; set; }
        public DbSet<GalleryGroup> GalleryGroup_tb { get; set; }
        public DbSet<GalleryGroupTranslation> GalleryGroupTranslation_tb { get; set; }
        public DbSet<Gallery> Gallery_tb { get; set; }
        public DbSet<GalleryTranslation> GalleryTranslation_tb { get; set; }
        public DbSet<GalleryPicture> GalleryPicture_tb { get; set; }
        public DbSet<GalleryPictureTranslation> GalleryPictureTranslation_tb { get; set; }
        public DbSet<SocialNetwork> SocialNetwork_tb { get; set; }
        public DbSet<SocialNetworkTranslation> SocialNetworkTranslation_tb { get; set; }
        public DbSet<SlideShowGroup> SlideShowGroup_tb { get; set; }
        public DbSet<SlideShowGroupTranslation> SlideShowGroupTranslation_tb { get; set; }
        public DbSet<SlideShow> SlideShow_tb { get; set; }
        public DbSet<SlideShowTranslation> SlideShowTranslation_tb { get; set; }
        public DbSet<SeoPage> SeoPage_tb { get; set; }
        public DbSet<SendType> SendType_tb { get; set; }
        public DbSet<SendTypeTranslation> SendTypeTranslation_tb { get; set; }
        public DbSet<CoWorkerGroup> CoWorkerGroup_tb { get; set; }
        public DbSet<CoWorkerGroupTranslation> CoWorkerGroupTranslation_tb { get; set; }
        public DbSet<CoWorker> CoWorker_tb { get; set; }
        public DbSet<CoWorkerTranslation> CoWorkerTranslation_tb { get; set; }
        public DbSet<FileManageGroup> FileManageGroup_tb { get; set; }
        public DbSet<FileManageGroupTranslation> FileManageGroupTranslation_tb { get; set; }
        public DbSet<FileManage> FileManage_tb { get; set; }
        public DbSet<FileManageTranslation> FileManageTranslation_tb { get; set; }
        public DbSet<Country> Country_tb { get; set; }
        public DbSet<State> State_tb { get; set; }
        public DbSet<City> City_tb { get; set; }
        public DbSet<Discount> Discount_tb { get; set; }
        public DbSet<SubDiscount> SubDiscount_tb { get; set; }
        public DbSet<ProductGroup> ProductGroup_tb { get; set; }
        public DbSet<ProductGroupTranslation> ProductGroupTranslation_tb { get; set; }
        public DbSet<Product> Product_tb { get; set; }
        public DbSet<ProductTranslation> ProductTranslation_tb { get; set; }
        public DbSet<ProductMiddleGroup> ProductMiddleGroup_tb { get; set; }
        public DbSet<FAQ> FAQ_tb { get; set; }


        public DbSet<FeatureGroup> FeatureGroup_tb { get; set; }
        public DbSet<FeatureGroupTranslation> FeatureGroupTranslation_tb { get; set; }
        public DbSet<FeatureTranslation> FeatureTranslation_tb { get; set; }
        public DbSet<Feature> Feature_tb { get; set; }
        public DbSet<FeatureReply> FeatureReply_tb { get; set; }
        public DbSet<FeatureReplyTranslation> FeatureReplyTranslation_tb { get; set; }
        public DbSet<ProductFeature> ProductFeature_tb { get; set; }
        public DbSet<ProductGroupFeature> ProductGroupFeature_tb { get; set; }

        public DbSet<Variation> Variation_tb { get; set; }
        public DbSet<FeatureReplyVariation> FeatureReplyVariation_tb { get; set; }

        public DbSet<Currency> Currency_tb { get; set; }
        public DbSet<PriceList> PriceList_tb { get; set; }
        public DbSet<ProductPriceList> ProductPriceList_tb { get; set; }
        public DbSet<ProductPriceListHistory> ProductPriceListHistory_tb { get; set; }
        public DbSet<SimilarProduct> SimilarProduct_tb { get; set; }
        public DbSet<SuggestProduct> SuggestProduct_tb { get; set; }



        protected override void OnModelCreating(ModelBuilder builder)
        {
            #region Language_SeedData
            builder.Entity<Language>().HasData( ///مقدار اولیه جدول بانک
                new Language
                {
                    Id = "fa-IR",
                    Name = "زبان فارسی",
                    IsActive = true
                },
                    new Language
                    {
                        Id = "en-US",
                        Name = "English",
                        IsActive = true
                    });
            #endregion

            #region ContentGroup_SeedData
            builder.Entity<ContentGroup>().HasData( ///مقدار اولیه جدول بانک
                new ContentGroup
                {
                    ContentGroup_ID = 1,
                    ParentId = null,
                    IsActive = true,
                    ContentImageHeight = "0",
                    ContentImageWidth = "0"
                });

            string title = "گروه پیش فرض";
            string title2 = "Default-Group";
            builder.Entity<ContentGroupTranslation>().HasData(
                new ContentGroupTranslation
                {
                    ContentGroupTranslation_ID = 2,
                    ContentGroup_Id = 1,
                    Title = title2,
                    Slug = title2.Trim().Replace(" ", "-"),
                    Language_Id = "en-US",
                    SiteMap_Id = null,
                },
                new ContentGroupTranslation
                {
                    ContentGroupTranslation_ID = 1,
                    ContentGroup_Id = 1,
                    Title = title,
                    Slug = title.Trim().Replace(" ", "-"),
                    Language_Id = "fa-IR",
                    SiteMap_Id = null,
                });
            #endregion

            #region FileManage_SeedData

            builder.Entity<FileManageGroup>().HasData(
                new FileManageGroup
                {
                    FileManageGroup_ID = 1,
                    Parent_Id = null,
                    IsActive = true,
                });
            string title_fileManage = "گروه پیش فرض";
            string title2_fileManage = "Default-Group";
            builder.Entity<FileManageGroupTranslation>().HasData(
                new FileManageGroupTranslation
                {
                    FileManageGroupTranslation_ID = 2,
                    FileManageGroup_Id = 1,
                    Title = title2_fileManage,
                    Slug = title2_fileManage.Trim().Replace(" ", "-"),
                    Language_Id = "en-US",
                    SiteMap_Id = null,
                },
                new FileManageGroupTranslation
                {
                    FileManageGroupTranslation_ID = 1,
                    FileManageGroup_Id = 1,
                    Title = title_fileManage,
                    Slug = title_fileManage.Trim().Replace(" ", "-"),
                    Language_Id = "fa-IR",
                    SiteMap_Id = null,
                });
            #endregion

            #region ProductGroup_SeedData
            builder.Entity<ProductGroup>().HasData( ///مقدار اولیه جدول بانک
                new ProductGroup
                {
                    ProductGroup_ID = 1,
                    ParentId = null,
                    Image = null,
                    ImageHeight = "0",
                    ImageWidth = "0",
                    SmallImageHeight = "0",
                    SmallImageWidth = "0",
                });

            string title_product = "گروه محصولات";
            string title2_product = "Products-Group";
            builder.Entity<ProductGroupTranslation>().HasData(
                new ProductGroupTranslation
                {
                    ProductGroupTranslation_ID = 2,
                    ProductGroup_Id = 1,
                    Discount_Id = null,
                    Title = title2_product,
                    Slug = title2_product.Trim().Replace(" ", "-"),
                    IsActive = true,
                    Language_Id = "en-US",
                },
                new ProductGroupTranslation
                {
                    ProductGroupTranslation_ID = 1,
                    ProductGroup_Id = 1,
                    Discount_Id = null,
                    Title = title_product,
                    Slug = title_product.Trim().Replace(" ", "-"),
                    IsActive = true,
                    Language_Id = "fa-IR",
                });
            #endregion

            #region FeatureGroup_SeedData
            builder.Entity<FeatureGroup>().HasData(
                new FeatureGroup
                {
                    FeatureGroup_ID = 1,  // برای رنگ
                });

            string title_featureGroup = "مشخصات کلی";
            string title2_featureGroup = "Genral Features";
            builder.Entity<FeatureGroupTranslation>().HasData(
                new FeatureGroupTranslation
                {
                    FeatureGroupTranslation_ID = 2,
                    FeatureGroup_Id = 1,
                    Title = title2_featureGroup,
                    Language_Id = "en-US",
                },
                new FeatureGroupTranslation
                {
                    FeatureGroupTranslation_ID = 1,
                    FeatureGroup_Id = 1,
                    Title = title_featureGroup,
                    Language_Id = "fa-IR",
                });
            #endregion

            #region Feature_SeedData
            builder.Entity<Feature>().HasData(
                new Feature
                {
                    Feature_ID = 1,  // برای رنگ
                    FeatureGroup_Id = 1,
                    IsVariable = true,
                    UseFilter = true
                });

            string title_feature = "رنگ ها";
            string title2_feature = "Colors";
            builder.Entity<FeatureTranslation>().HasData(
                new FeatureTranslation
                {
                    FeatureTranslation_ID = 2,
                    Feature_Id = 1,
                    Title = title2_feature,
                    Language_Id = "en-US",
                },
                new FeatureTranslation
                {
                    FeatureTranslation_ID = 1,
                    Feature_Id = 1,
                    Title = title_feature,
                    Language_Id = "fa-IR",
                });
            #endregion

            //#region GalleryGroup_SeedData
            //builder.Entity<GalleryGroup>().HasData(
            //    new GalleryGroup
            //    {
            //        GalleryGroup_ID = 1,  
            //        LargPicHeight ="0",
            //        LargPicWidth ="0",
            //        MediumPicWidth ="0",
            //        MediumPicHeight ="0",
            //        SmallPicWidth ="0",
            //        SmallPicHeight ="0",
            //    });

            //string title_galleryGroup = "گروه پیش فرض";
            //builder.Entity<GalleryGroupTranslation>().HasData(
            //    new GalleryGroupTranslation
            //    {
            //        GalleryGroupTranslation_ID = 1,
            //        GalleryGroup_Id = 1,
            //        Title = title_galleryGroup,
            //        Language_Id = "fa-IR",
            //    });
            //#endregion

            // builder.Entity<MainSiteMap>().HasMany(p => p.ContentGroupTranslations)
            //     .WithOne(b => b.MainSiteMap)
            // .HasForeignKey(p => p.SiteMap_Id)
            //  .OnDelete(DeleteBehavior.Cascade);


            builder.Entity<ContentMiddleGroup>()
                  .HasOne(x => x.ContentGroupTranslation)
                  .WithMany(x => x.ContentMiddleGroups)
                  .HasForeignKey(x => x.ContentGroupTranslation_Id)
                  .IsRequired()
                  .OnDelete(DeleteBehavior.Cascade);

            builder.Entity<ContentMiddleGroup>()
                  .HasOne(x => x.ContentTranslation)
                  .WithMany(x => x.ContentMiddleGroups)
                  .HasForeignKey(x => x.ContentTranslation_Id)
                  .IsRequired()
                  .OnDelete(DeleteBehavior.Cascade);

            builder.Entity<ProductPriceList>()
                 .HasOne(x => x.Variation)
                 .WithMany(x => x.ProductPriceLists)
                 .HasForeignKey(x => x.Variation_Id)
                 .IsRequired()
                 .OnDelete(DeleteBehavior.NoAction);

            builder.Entity<FeatureReplyVariation>()
                 .HasOne(x => x.Variation)
                 .WithMany(x => x.FeatureReplyVariations)
                 .HasForeignKey(x => x.Variation_Id)
                 .IsRequired()
                 .OnDelete(DeleteBehavior.NoAction);

            builder.Entity<FeatureReplyVariation>()
               .HasOne(x => x.FeatureReply)
               .WithMany(x => x.FeatureReplyVariations)
               .HasForeignKey(x => x.FeatureReply_Id)
               .IsRequired()
               .OnDelete(DeleteBehavior.NoAction);

            builder.Entity<ProductPriceListHistory>()
                .HasOne(x => x.Variation)
                .WithMany(x => x.ProductPriceListHistories)
                .HasForeignKey(x => x.Variation_Id)
                .IsRequired()
                .OnDelete(DeleteBehavior.NoAction);

            builder.Entity<ProductPriceListHistory>()
                .HasOne(x => x.PriceList)
                .WithMany(x => x.ProductPriceListHistories)
                .HasForeignKey(x => x.PriceList_Id)
                .IsRequired()
                .OnDelete(DeleteBehavior.NoAction);

            builder.Entity<ProductPriceListHistory>()
               .HasOne(x => x.ProductTranslation)
               .WithMany(x => x.ProductPriceListHistories)
               .HasForeignKey(x => x.ProductTranslation_Id)
               .IsRequired()
               .OnDelete(DeleteBehavior.NoAction);

            builder.Entity<ProductFeature>()
             .HasOne(x => x.FeatureReply)
             .WithMany(x => x.ProductFeatures)
             .HasForeignKey(x => x.FeatureReply_Id)
             .IsRequired()
             .OnDelete(DeleteBehavior.NoAction);

           // builder.Entity<SimilarProduct>()
           // .HasOne(u => u.Product)
           // .WithMany(u => u.SimilarProduct)
           // .IsRequired()
           // .OnDelete(DeleteBehavior.Cascade);


           // builder.Entity<SimilarProduct>()
           //.HasOne(pt => pt.Similar_Product)
           //.WithMany(p => p.SimilarProduct2)
           //.HasForeignKey(pt => pt.ProductSimilar_Id).OnDelete(DeleteBehavior.Cascade);

           // builder.Entity<SuggestProduct>()
           // .HasOne(u => u.Product)
           // .WithMany(u => u.SuggestProducts)
           // .IsRequired()
           // .OnDelete(DeleteBehavior.Cascade);


            base.OnModelCreating(builder);
        }
    }
}
